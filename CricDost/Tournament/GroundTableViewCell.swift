//
//  GroundTableViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 8/29/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class GroundTableViewCell: UITableViewCell {

    @IBOutlet weak var groundLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
