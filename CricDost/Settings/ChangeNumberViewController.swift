//
//  ChangeNumberViewController.swift
//  CricDost
//
//  Created by Jit Goel on 7/2/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import IQKeyboardManager

class ChangeNumberViewController: UIViewController, UITextFieldDelegate, ShowsAlert,UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    
    @IBOutlet weak var countryCodeTextFieldWidth: NSLayoutConstraint!
    @IBOutlet weak var countryCodeTextField: UITextField!
    @IBOutlet weak var countryCodeLineView: UIView!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var sendOtpButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var mobileView: UIView!
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var mobileNumber = ""
    @IBOutlet weak var titleLabel: UILabel!
    var countryPicker = UIPickerView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [mobileView])
        
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(self.keyboardNotification(notification:)),
//                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
//                                               object: nil)
//        self.addDoneButtonOnKeyboard()
         IQKeyboardManager.shared().isEnabled = true
        countryPicker.delegate = self
        countryPicker.dataSource = self
        
        countryCodeTextField.inputView = countryPicker
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == countryPicker {
            print(CountryCodes.countryCodes.count)
            return CountryCodes.countryCodes.count
        }
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == countryPicker {
            let text = "\(CountryCodes.countryCodes[row].first!.value)"
            return text
        }
        return "error"
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == countryPicker {
            let country = CountryCodes.countryCodes[row].first!.key
            countryCodeTextField.text = country
        }
    }

    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendOtpButtonClick(_ sender: Any) {
        if sendOtpButton.titleLabel?.text == "Send OTP" {
            if !mobileNumberTextField.isReallyEmpty {
                if (mobileNumberTextField.text?.count)! < 6 || (mobileNumberTextField.text?.count)! > 15 {
                    showAlert(title: "Invalid Phone Number", message: "Please check the number you have entered")
                    self.mobileView.shake()
                    
                } else {
                    sendOTP()
                }
            } else {
                self.mobileView.shake()
                showAlert(title: "Missing Phone Number", message: "Please enter a phone number")
            }
        } else if sendOtpButton.titleLabel?.text == "Update" {
            if !mobileNumberTextField.isReallyEmpty {
                checkOTP()
            } else {
                self.mobileView.shake()
                showAlert(title: "Missing OTP", message: "Please enter the OTP send to you mobile")
            }
        }
    }
    
    func sendOTP(){
            activityIndicator.startAnimating()
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MOBILE_NUMBER\":\"\(mobileNumberTextField.text!)\",\"COUNTRY_CODE\":\"\(countryCodeTextField.text!)\"}", mod: "Settings", actionType: "change-mobile-number") { (response) in
                if response as? String != "error" {
                    print(response)
                    self.sendOtpButton.setTitle("Update", for: .normal)
                    self.mobileNumber = self.mobileNumberTextField.text!
                    self.mobileNumberTextField.text = ""
                    self.mobileNumberTextField.placeholder = "Enter OTP"
                    self.titleLabel.text = "Enter OTP"
                    self.showAlert(title: "OTP", message: "Otp was send to the mobile number")
                    self.countryCodeTextFieldWidth.constant = 0
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
            }
    }
    
    func checkOTP(){
        
        activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MOBILE_NUMBER\":\"\(mobileNumber)\",\"OTP\":\"\(mobileNumberTextField.text!)\"}", mod: "Settings", actionType: "change-mobile-number-validate-otp") { (response) in
            if response as? String != "error" {
                print(response)
                
                UserDefaults.standard.set(((response as! NSDictionary)["XSCData"] as! NSDictionary)["MOBILE_NUMBER"] as! String, forKey: CommonUtil.MOBILE_NUMBER)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getUserSettingsDetail"), object: nil)
                let alert = UIAlertController(title: "Message", message: (response as! NSDictionary)["XSCMessage"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        return newLength <= 15
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant = 0.0
            } else {
                self.keyboardHeightLayoutConstraint?.constant = -(endFrame?.size.height ?? 0.0)/2
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.mobileNumberTextField.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.mobileNumberTextField.resignFirstResponder()
    }
}
