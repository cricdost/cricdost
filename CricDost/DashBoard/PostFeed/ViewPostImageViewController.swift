//
//  ViewPostImageViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 11/1/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import ImageScrollView
import SDWebImage

class ViewPostImageViewController: UIViewController {

    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var scrollviewForImageView: ImageScrollView!
    var myImage: UIImage?
    var imageUrl: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let x = SDImageCache.shared().imageFromCache(forKey: URL(string: "\(CommonUtil.BASE_URL)\(imageUrl!)")!.absoluteString) {
            myImage = x
        } else {
            myImage = nil
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scrollviewForImageView.display(image: myImage!)
    }
    
    @IBAction func closeButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
