//
//  BackgroundImageView.swift
//  CricDost
//
//  Created by Jit Goel on 10/8/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class BackgroundImageView: UIView, UIDropInteractionDelegate{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    private func setup() {
        if #available(iOS 11.0, *) {
            addInteraction(UIDropInteraction(delegate: self))
        } else {
            // Fallback on earlier versions
        }
    }
    @available(iOS 11.0, *)
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: UIImage.self)
    }
    @available(iOS 11.0, *)
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        return UIDropProposal(operation: .copy)
    }
    @available(iOS 11.0, *)
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        for item in session.items {
            let itemProvider = item.itemProvider
            guard itemProvider.canLoadObject(ofClass: UIImage.self)
                else {continue}
            print("drop item")
            itemProvider.loadObject(ofClass: UIImage.self, completionHandler: { (object, error) in
                if let image = object as? UIImage {
                    DispatchQueue.main.async {
                        for subview in self.subviews {
                            CommonUtil.imageRoundedCorners(imageviews: [subview as! UIImageView])
                        }
                        for subview in self.subviews {
                            let imageView = (subview as! UIImageView)
                            if  imageView.image == nil {
                                imageView.image = image
                                break
                            }
                        }
                        
                    }
                }
            })
        }
//        session.loadObjects(ofClass: UIImage.self) { (providers) in
//            print("drop item",providers)
//            (self.subviews[0] as! UIImageView).image = providers as? UIImage
//        }
    }
    
    var backgroundImage: UIImage? { didSet {setNeedsDisplay() }  }
    override func draw(_ rect: CGRect) {
        backgroundImage?.draw(in: bounds)
    }
    
    
}
