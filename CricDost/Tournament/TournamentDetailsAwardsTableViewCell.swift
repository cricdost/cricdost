//
//  TournamentDetailsAwardsTableViewCell.swift
//  CricDost
//
//  Created by JIT GOEL on 9/7/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class TournamentDetailsAwardsTableViewCell: UITableViewCell {

    @IBOutlet weak var awardNameLabel: UILabel!
    @IBOutlet weak var prizeAmountLabel: UILabel!
    @IBOutlet weak var trophyImageView: UIImageView!
    @IBOutlet weak var rupeesImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   
}
