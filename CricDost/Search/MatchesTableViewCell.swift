//
//  MatchesTableViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 7/9/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class MatchesTableViewCell: UITableViewCell {

    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var teamAImage: UIImageView!
    @IBOutlet weak var teamBImage: UIImageView!
    @IBOutlet weak var teamAName: UILabel!
    @IBOutlet weak var teamBName: UILabel!
    @IBOutlet weak var joinMatchButton: UIButton!
    @IBOutlet weak var challengeButton: UIButton!
    @IBOutlet weak var numOfChallengers: UILabel!
    @IBOutlet weak var exitButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        roundedCorner(buttons: [joinMatchButton, challengeButton, exitButton])
        CommonUtil.imageRoundedCorners(imageviews: [teamAImage, teamBImage])
    }
    
    func roundedCorner(buttons: [UIButton]) {
        for button in buttons {
            button.layer.cornerRadius = 5
            button.clipsToBounds = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }

}
