//
//  TeamProfileViewController.swift
//  CricDost
//
//  Created by Jit Goel on 6/7/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import AnimatedCollectionViewLayout

class TeamProfileViewController: PullUpController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ShowsAlert, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var teamCollectionView: UICollectionView!
    @IBOutlet weak var matchesCollectionView: UICollectionView!
    @IBOutlet weak var playersCollectionView: UICollectionView!
    @IBOutlet weak var viewHolder: UIView!
    var currentAddressID: String?
    var currentMatchID: String?
    var isCalledFromPlayerProfile = false
    var isCalledFromSearch = false
    var teamDetails: Any?
    var address: String?
    var currentTeamId: String?
    @IBOutlet weak var requestsTableView: UITableView!
    @IBOutlet weak var noMatchesLabel: UILabel!
    @IBOutlet weak var noTeamsLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    var teamList: NSArray = []
    var playerList: NSArray = []
    var receivedList: NSArray = []
    var sentList: NSArray = []
    @IBOutlet weak var addPlayerButton: UIButton!
    @IBOutlet weak var icon: UIButton!
    var matchesList: NSArray = []
    @IBOutlet weak var mainLoaderActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var preventUserInteractionLoadingView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    let layout = AnimatedCollectionViewLayout()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(dismissTeamProfileFunction), name: NSNotification.Name(rawValue: "dismissTeamProfileFunction"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTeamMacthListFunction), name: NSNotification.Name(rawValue: "reloadTeamMacthListFunction"), object: nil)
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [viewHolder])
        CommonUtil.buttonRoundedCorners(buttons: [addPlayerButton])
        print(teamDetails)
        
        layout.animator = LinearCardAttributesAnimator(minAlpha: 1.0, itemSpacing: 0.15, scaleRate: 0.92)
        layout.scrollDirection = .horizontal
        matchesCollectionView.collectionViewLayout = layout
        matchesCollectionView.isPagingEnabled = true
        
        didMoveToStickyPoint = { [weak self] point in
            if point == -100.0 {
                NotificationCenter.default.post(name: NSNotification.Name("hideShowNavBar"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name("dismissBlackScreen"), object: nil)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        CommonUtil.updateGATracker(screenName: "Team Profile")
        teamList = (teamDetails as! NSDictionary)["XSCData"] as! NSArray
        if (teamList.count > 0) {
            currentTeamId = (teamList[0] as! NSDictionary)["TEAM_ID"] as? String
            getTeamPlayerList(teamID: (teamList[0] as! NSDictionary)["TEAM_ID"] as! String)
            if (teamList[0] as! NSDictionary)["IS_ADMIN"] as! Int == 1 {
                addPlayerButton.isHidden = false
            } else {
                addPlayerButton.isHidden = true
            }
        }
        teamCollectionView.reloadData()
    }
    
    @IBAction func addPlayersButtonClick(_ sender: Any) {
        if addPlayerButton.titleLabel?.text == "Add Player" {
            let invitePlayerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateTeamViewController") as! CreateTeamViewController
            invitePlayerVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            invitePlayerVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            invitePlayerVC.open_InvitePlayersOnly = true
            invitePlayerVC.recentTeamID = Int(currentTeamId!)
            self.present(invitePlayerVC, animated: true, completion: nil)
        } else {
            if CommonUtil.NewCreateMatchFlow {
                
                let newCreateMatchVC = UIStoryboard(name: "NewCreateMatch", bundle: nil).instantiateViewController(withIdentifier: "NewCreateMatchViewController") as? NewCreateMatchViewController
                newCreateMatchVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                newCreateMatchVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                newCreateMatchVC?.isOnBoardUser = false
                present(newCreateMatchVC!, animated: true, completion: nil)
                
            } else {
                let createMatchVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateMatchViewController") as? CreateMatchViewController
                createMatchVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                createMatchVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                createMatchVC?.isOnBoardUser = false
                present(createMatchVC!, animated: true, completion: nil)
            }
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(60)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat(0)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionCell = tableView.dequeueReusableCell(withIdentifier: "sectionCell") as! SectionRequestsTableViewCell
        
        if section == 0 {
            sectionCell.sectionNameLabel.text = "\(receivedList.count) Received Requests"
//            if receivedList.count == 0 {
//                sectionCell.sectionNameLabel.text = "0 Received Requests"
//            }
        } else {
            sectionCell.sectionNameLabel.text = "\(sentList.count) Sent Requests"
//            if sentList.count == 0 {
//                sectionCell.sectionNameLabel.text = "0 Sent Requests"
//            }
        }
        return sectionCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return receivedList.count
        } else {
            return sentList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "requestCell", for: indexPath) as! RequestsTableViewCell
        
        CommonUtil.imageRoundedCorners(imageviews: [cell.profileImageView])
        CommonUtil.buttonRoundedCorners(buttons: [cell.declineButton, cell.acceptButton])
        
        if indexPath.section == 0 {
            let request = receivedList[indexPath.row] as! NSDictionary
            
            cell.nameLabel.text = request["REQUEST_NAME"] as? String
            cell.detailLabel.text = request["REQUEST_SUB_DATA"] as? String
            cell.profileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(request["IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "profile_pic_default"))
            if indexPath.row == receivedList.count - 1 {
                cell.lineView.isHidden = true
            } else {
                cell.lineView.isHidden = false
            }
            cell.acceptButton.isHidden = false
            cell.declineButton.setTitle("Decline", for: .normal)
            
            cell.declineButton.addTarget(self, action: #selector(declineButtonRequestTapped), for: UIControlEvents.touchUpInside)
            cell.declineButton.tag = indexPath.row
            
            cell.acceptButton.addTarget(self, action: #selector(acceptButtonRequestTapped), for: UIControlEvents.touchUpInside)
            cell.acceptButton.tag = indexPath.row
            
        } else {
            let request = sentList[indexPath.row] as! NSDictionary
            
            cell.nameLabel.text = request["FULL_NAME"] as? String
            cell.detailLabel.text = request["SKILLS"] as? String
            cell.profileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(request["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
            
            if indexPath.row == sentList.count - 1 {
                cell.lineView.isHidden = true
            } else {
                cell.lineView.isHidden = false
            }
            
            cell.acceptButton.isHidden = true
            cell.declineButton.setTitle("Cancel", for: .normal)
            
            cell.declineButton.addTarget(self, action: #selector(declineButtonRequestTapped), for: UIControlEvents.touchUpInside)
            cell.declineButton.tag = indexPath.row

        }
        
        return cell
    }
    
    @IBAction func declineButtonRequestTapped(_ sender: UIButton) {
        
        if sender.titleLabel?.text == "Decline" {
            let request = receivedList[sender.tag] as! NSDictionary
            print("Decline")
            let alert = UIAlertController(title: "Decline Request", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Decline", style: UIAlertActionStyle.default, handler: { action in
                self.replyToTeamRequest(requestSupportId: request["REQUEST_SUPPORT_ID"] as! String, accept: 0)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:{ action in
                
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            let request = sentList[sender.tag] as! NSDictionary
            print("Cancel")
            let alert = UIAlertController(title: "Cancel Request", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                self.cancelTeamRequestSentToPlayer(teamPlayerId: request["TEAM_PLAYER_ID"] as! String)
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ action in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func acceptButtonRequestTapped(_ sender: UIButton) {
        print("ACCEPT")
        let request = receivedList[sender.tag] as! NSDictionary
        let alert = UIAlertController(title: "Accept Request", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Accept", style: UIAlertActionStyle.default, handler: { action in
            self.replyToTeamRequest(requestSupportId: request["REQUEST_SUPPORT_ID"] as! String, accept: 1)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:{ action in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == teamCollectionView {
            return teamList.count
        }  else if collectionView == matchesCollectionView {
            return matchesList.count
        } else if collectionView == playersCollectionView {
            return playerList.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == teamCollectionView {
            let team = teamList[indexPath.section] as! NSDictionary
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "teamProfile", for: indexPath) as! TeamProfileCollectionViewCell
            
            cell.nameAgeLabel.text = "\(team["TEAM_NAME"] as! String)"
            cell.addressLabel.text = self.address
            cell.ratingLabel.text = "\(team["RATTING"] as! String)"
//            cell.follwerLabel.text = "FOLLOWERS \(team["FOLLOWERS"] as! Int) \u{2022} FOLLOWING \(team["FOLLOWING"] as! Int)"
            cell.follwerLabel.text = "FOLLOWERS \(team["FOLLOWERS"] as! Int)"
            cell.profileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(team["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            cell.matchesLabel.text = "\(team["MATCHES"] as! Int)"
            cell.winnerLabel.text = "\(team["WINNER"] as! Int)"
            cell.runnerLabel.text = "\(team["RUNNER"] as! Int)"
            
            if team["CAN_FOLLOW"] as! Int == 1 {
                cell.followButton.setTitle("Follow", for: .normal)
            } else {
                cell.followButton.setTitle("Unfollow", for: .normal)
            }
            
            let tapped = UITapGestureRecognizer(target: self, action: #selector(ImageTappedFunction))
            cell.profileImageView.addGestureRecognizer(tapped)
            cell.profileImageView.tag = indexPath.section
            
            cell.followButton.addTarget(self, action: #selector(followButtonTapped), for: UIControlEvents.touchUpInside)
            cell.followButton.tag = indexPath.section
            
            cell.oneStar.addTarget(self, action: #selector(oneStarButtonTapped), for: UIControlEvents.touchUpInside)
            cell.oneStar.tag = indexPath.section
            
            cell.twoStar.addTarget(self, action: #selector(twoStarButtonTapped), for: UIControlEvents.touchUpInside)
            cell.twoStar.tag = indexPath.section
            
            cell.threeStar.addTarget(self, action: #selector(threeStarButtonTapped), for: UIControlEvents.touchUpInside)
            cell.threeStar.tag = indexPath.section
            
            cell.fourStar.addTarget(self, action: #selector(fourStarButtonTapped), for: UIControlEvents.touchUpInside)
            cell.fourStar.tag = indexPath.section
            
            cell.fiveStar.addTarget(self, action: #selector(fiveStarButtonTapped), for: UIControlEvents.touchUpInside)
            cell.fiveStar.tag = indexPath.section
            
            cell.requestButton.addTarget(self, action: #selector(requestButtonTapped), for: UIControlEvents.touchUpInside)
            cell.requestButton.tag = indexPath.section
            
            cell.matchesButton.addTarget(self, action: #selector(matchesButtonTapped), for: UIControlEvents.touchUpInside)
            cell.matchesButton.tag = indexPath.section
            
            cell.playersButton.addTarget(self, action: #selector(playersButtonTapped), for: UIControlEvents.touchUpInside)
            cell.playersButton.tag = indexPath.section
            
            cell.matchesButton.setImage(#imageLiteral(resourceName: "match_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            cell.matchesButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            cell.matchesButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
            
            if team["CAN_RATE"] as! Int == 1 {
                cell.oneStar.isEnabled = true
                cell.twoStar.isEnabled = true
                cell.threeStar.isEnabled = true
                cell.fourStar.isEnabled = true
                cell.fiveStar.isEnabled = true
            } else {
                cell.oneStar.isEnabled = false
                cell.twoStar.isEnabled = false
                cell.threeStar.isEnabled = false
                cell.fourStar.isEnabled = false
                cell.fiveStar.isEnabled = false
                cell.setRating(rating: team["MY_RATING"] as! String)
            }
            cell.messageButton.isHidden = true
            cell.optionMenuButton.isHidden = true
            if (team["IS_JOINED"] as! String == "true") {
                cell.requestButton.setTitle("Exit", for: .normal)
                cell.requestButton.setImage(#imageLiteral(resourceName: "cancel_black_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
                cell.messageButton.isHidden = false
                if team["IS_ADMIN"] as! Int == 1 {
                    cell.requestButton.setTitle("Requests", for: .normal)
                    cell.requestButton.setImage(#imageLiteral(resourceName: "cancel_black_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
                    cell.optionMenuButton.isHidden = false
                }
            } else if (team["IS_REQUEST_SENT"] as! String == "true") {
                cell.requestButton.setTitle("Respond", for: .normal)
                cell.requestButton.setImage(#imageLiteral(resourceName: "cancel_black_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            } else if (team["IS_REQUEST_RECEIVED"] as! String == "true") {
                cell.requestButton.setTitle("Cancel", for: .normal)
                cell.requestButton.setImage(#imageLiteral(resourceName: "cancel_black_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            } else {
                cell.requestButton.setTitle("Request", for: .normal)
                cell.requestButton.setImage(#imageLiteral(resourceName: "request_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            }
            
            cell.requestButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            cell.requestButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
            
            if team["IS_ADMIN"] as! Int == 1 {
                cell.followButton.isHidden = true
                cell.oneStar.isHidden = true
                cell.twoStar.isHidden = true
                cell.threeStar.isHidden = true
                cell.fourStar.isHidden = true
                cell.fiveStar.isHidden = true
                UserDefaults.standard.set(true, forKey: CommonUtil.ISTEAMADMIN)
            } else {
                cell.followButton.isHidden = false
                cell.oneStar.isHidden = false
                cell.twoStar.isHidden = false
                cell.threeStar.isHidden = false
                cell.fourStar.isHidden = false
                cell.fiveStar.isHidden = false
                UserDefaults.standard.set(false, forKey: CommonUtil.ISTEAMADMIN)
            }
            
            cell.messageButton.addTarget(self, action: #selector(messageTeamButtonTapped), for: UIControlEvents.touchUpInside)
            cell.messageButton.tag = indexPath.section
            
            cell.optionMenuButton.addTarget(self, action: #selector(optionMenuButtonTapped), for: UIControlEvents.touchUpInside)
            cell.optionMenuButton.tag = indexPath.section
            
            return cell
        } else if collectionView == playersCollectionView {
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "playerCell", for: indexPath) as! TeamPlayersCollectionViewCell
            let player = playerList[indexPath.section] as! NSDictionary
            
            cell1.playerName.text = player["FULL_NAME"] as? String
            cell1.skillLabel.text = player["SKILLS"] as? String
            cell1.profileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(player["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
            cell1.callButton.addTarget(self, action: #selector(callButtonTapped), for: UIControlEvents.touchUpInside)
            cell1.callButton.tag = indexPath.section
            
            cell1.optionButton.addTarget(self, action: #selector(optionButtonoptionButton), for: UIControlEvents.touchUpInside)
            cell1.optionButton.tag = indexPath.section
            
            cell1.messageBUtton.addTarget(self, action: #selector(messageButtonTapped), for: UIControlEvents.touchUpInside)
            cell1.messageBUtton.tag = indexPath.section
            
            if player["IS_TEAM_OWNER"] as! String == "true" {
                cell1.adminLabel.isHidden = false
                cell1.optionButton.isHidden = true
            } else {
                cell1.adminLabel.isHidden = true
            }
            
            if player["IS_CURRENT_PLAYER"] as! String == "true" {
                cell1.playerName.text = "You"
                cell1.callButton.isHidden = true
                cell1.messageBUtton.isHidden = true
                cell1.lineView.isHidden = true
            } else {
                if UserDefaults.standard.object(forKey: CommonUtil.ISTEAMADMIN) as? Bool ?? false {
                    cell1.optionButton.isHidden = false
                } else {
                    cell1.optionButton.isHidden = true
                }
                cell1.playerName.text = player["FULL_NAME"] as? String
                cell1.callButton.isHidden = false
                cell1.messageBUtton.isHidden = false
                cell1.lineView.isHidden = false
            }
            
            return cell1
        } else {
            let match = (matchesList[indexPath.section] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
            let score = (matchesList[indexPath.section] as! NSDictionary)["SCORE_DETAILS"] as! NSDictionary
            let teamA = match["TEAM_A"] as! NSDictionary
            let teamB = match["TEAM_B"] as! NSDictionary
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "matchesCell", for: indexPath) as! PlayerProfileMatchesCollectionViewCell
            cell2.dateLabel.text = match["DATE"] as? String
            cell2.timeLabel.text = match["TIME"] as? String
            cell2.teamAName.text = teamA["TEAM_NAME"] as? String
            cell2.numOfPlayers.text = "\(match["AVAILABLE_PLAYERS"] as? String ?? String(match["AVAILABLE_PLAYERS"] as! Int) )/\(match["TOTAL_PLAYERS"] as? String ?? String(match["TOTAL_PLAYERS"] as! Int)) Players"
            cell2.teamAImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamA["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            if match["IS_ADMIN"] as! Int == 0 {
                cell2.adminButton.isHidden = true
            } else {
                cell2.adminButton.isHidden = false
            }
            cell2.addressLabel.text = match["ADDRESS"] as? String
            if  teamB["POSITION_STATUS"] as! Int == 0 {
                cell2.teamBImageView.image = nil
                cell2.teamBName.text = "Challengers"
                cell2.numOfChallengers.isHidden = false
                cell2.numOfChallengers.text = match["INTEREST_COUNT"] as? String
            } else {
                cell2.numOfChallengers.isHidden = true
                cell2.teamBImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamB["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
                cell2.teamBName.text = teamB["TEAM_NAME"] as? String
            }
            if match["CAN_CHALLENGE"] as! Int == 1 {
                cell2.challengeMatchButton.isHidden = false
            } else {
                cell2.challengeMatchButton.isHidden = true
            }
            
            if match["CAN_JOIN"] as! Int == 1 {
                cell2.joinMatchButton.isHidden = false
            } else {
                cell2.joinMatchButton.isHidden = true
            }
            
            cell2.joinMatchButton.addTarget(self, action: #selector(joinMatchBUttonTapped), for: UIControlEvents.touchUpInside)
            cell2.challengeMatchButton.addTarget(self, action: #selector(challengeButtonTapped), for: UIControlEvents.touchUpInside)
            cell2.joinMatchButton.tag = indexPath.section
            cell2.challengeMatchButton.tag = indexPath.section
            
            if score["STATUS"] as! Int == 1 {
                cell2.joinMatchButton.isHidden = true
                cell2.challengeMatchButton.isHidden = true
                cell2.livescoreView.isHidden = false
                cell2.teamAScore.text = (score["TEAM_A"] as! NSDictionary)["RUN_STR"] as? String
                cell2.teamBScore.text = (score["TEAM_B"] as! NSDictionary)["RUN_STR"] as? String
                cell2.numOfPlayers.isHidden = true
                cell2.addressLabel.isHidden = true
            } else {
                cell2.livescoreView.isHidden = true
            }
            
            return cell2
        }
    }
    
    @objc func ImageTappedFunction(gesture: UITapGestureRecognizer) {
        let team = teamList[(gesture.view?.tag)!] as! NSDictionary
        if team["IMAGE_URL"] as! String != "" {
            let imageVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewProfileImageViewController") as? ViewProfileImageViewController
            imageVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            imageVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            imageVC?.imageUrl = team["IMAGE_URL"] as? String
            self.present(imageVC!, animated: true, completion: nil)
        }
    }
    
    @IBAction func optionButtonoptionButton(_ sender: UIButton) {
        let player = playerList[sender.tag] as! NSDictionary
        print(player)
        let teamPlayerID = player["TEAM_PLAYER_ID"] as! String
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .popover
        alert.addAction(UIAlertAction(title: "Remove Player", style: .default, handler: { _ in
            print("remove player")
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_PLAYER_ID\":\"\(teamPlayerID)\"}", mod: "Team", actionType: "remove-team-player") { (response: Any) in
                print(response)
                if response as? String != "error" {
                    if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                        self.getTeamPlayerList(teamID: self.currentTeamId!)
                    } else {
                        self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                    }
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.mainLoaderActivityIndicator.stopAnimating()
                self.preventUserInteractionLoadingView.isHidden = true
            }
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            print("IPAD")
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            alert.popoverPresentationController?.permittedArrowDirections = .down
            
        default:
            break
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func optionMenuButtonTapped(_ sender: UIButton) {
        let team = teamList[sender.tag] as! NSDictionary
        print(team)
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .popover
        alert.addAction(UIAlertAction(title: "Edit Profile", style: .default, handler: { _ in
            print("Go to edit team")
            let editTeamVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditTeamProfileViewController") as! EditTeamProfileViewController
            editTeamVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            editTeamVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            editTeamVC.teamName = team["TEAM_NAME"] as? String
            editTeamVC.teamID = team["TEAM_ID"] as? String
            editTeamVC.location = team["ADDRESS"] as? String
            editTeamVC.profileURL = team["IMAGE_URL"] as? String
//            self.pullUpControllerMoveToVisiblePoint(self.pullUpControllerMiddleStickyPoints[1], completion: nil)
            self.present(editTeamVC, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            print("IPAD")
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            alert.popoverPresentationController?.permittedArrowDirections = .down
            
        default:
            break
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func messageTeamButtonTapped(_ sender: UIButton) {
        let team = teamList[sender.tag] as! NSDictionary
        
        print("chat", team)
        
        //        pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[1], completion: nil)
        let groupChatVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GroupChatViewController") as? GroupChatViewController
        groupChatVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        groupChatVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        groupChatVC?.groupJIDPrefix = team["OPENFIRE_USERNAME"] as? String
        groupChatVC?.teamName = team["TEAM_NAME"] as? String
        self.present(groupChatVC!, animated: true, completion: nil)
    }
    
    @IBAction func callButtonTapped(_ sender: UIButton) {
        let player = playerList[sender.tag] as! NSDictionary
        
        if player["IS_ALLOW_USER_TO_CALL"] as! String == "1" {
            guard let number = URL(string: "telprompt://\(player["MOBILE_NUMBER"] as! String)") else { return }
            UIApplication.shared.open(number)
        } else {
            showAlert(message: "You are not allowed to call these player")
        }
    }
    
    @IBAction func messageButtonTapped(_ sender: UIButton) {
        let player = playerList[sender.tag] as! NSDictionary
        
        if player["IS_ALLOW_USER_TO_CHAT"] as! String == "1" {
            print("Message")
            let chatVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
            chatVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            chatVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            chatVC.playerUserName = player["OPENFIRE_USERNAME"] as? String
            self.present(chatVC, animated: true, completion: nil)
        } else {
            showAlert(message: "You are not allowed to send message to these player")
        }
    }
    
    @IBAction func matchesButtonTapped(_ sender: UIButton) {
        if let collectionView = self.teamCollectionView {
            let indexPath = IndexPath(row: 0, section: sender.tag)
            let cell = collectionView.cellForItem(at: indexPath) as? TeamProfileCollectionViewCell
            cell?.playersButton.setImage(#imageLiteral(resourceName: "player-24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            cell?.playersButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            cell?.playersButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
            cell?.matchesButton.setImage(#imageLiteral(resourceName: "match_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            cell?.matchesButton.tintColor = CommonUtil.themeRed
            cell?.matchesButton.setTitleColor(CommonUtil.themeRed, for: .normal)
            cell?.requestButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            cell?.requestButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
            cell?.layoutSubviews()
            cell?.layoutIfNeeded()
        }
        print("Matches Tapped")
        icon.setImage(#imageLiteral(resourceName: "match_icon_map"), for: .normal)
        titleLabel.text = "Matches"
        addPlayerButton.setTitle("Create Match", for: .normal)
        noTeamsLabel.isHidden = true
        self.playersCollectionView.isHidden = true
        self.matchesCollectionView.isHidden = false
        self.requestsTableView.isHidden = true
        let team = teamList[sender.tag] as! NSDictionary
        pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[2], completion: nil)
        currentTeamId = team["TEAM_ID"] as? String
        getTeamMatchesList(teamId: team["TEAM_ID"] as! String)
    }
    
    @IBAction func playersButtonTapped(_ sender: UIButton) {
        if let collectionView = self.teamCollectionView {
            let indexPath = IndexPath(row: 0, section: sender.tag)
            let cell = collectionView.cellForItem(at: indexPath) as? TeamProfileCollectionViewCell
            cell?.playersButton.setImage(#imageLiteral(resourceName: "player-24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            cell?.playersButton.tintColor = UIColor(red:0.56, green:0.65, blue:0.27, alpha:1.0)
            cell?.playersButton.setTitleColor(UIColor(red:0.56, green:0.65, blue:0.27, alpha:1.0), for: .normal)
            cell?.matchesButton.setImage(#imageLiteral(resourceName: "match_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            cell?.matchesButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            cell?.matchesButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
            cell?.requestButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            cell?.requestButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
            cell?.layoutSubviews()
            cell?.layoutIfNeeded()
        }
        print("Players")
        icon.setImage(#imageLiteral(resourceName: "my-players-24x24"), for: .normal)
        titleLabel.text = "My Players"
        addPlayerButton.setTitle("Add Player", for: .normal)
        noMatchesLabel.isHidden = true
        self.playersCollectionView.isHidden = false
        self.matchesCollectionView.isHidden = true
        self.requestsTableView.isHidden = true
        let team = teamList[sender.tag] as! NSDictionary
        pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[2], completion: nil)
        getTeamPlayerList(teamID: team["TEAM_ID"] as! String)
    }
    
    @objc func reloadTeamMacthListFunction() {
        self.getTeamMatchesList(teamId: currentTeamId!)
    }
    
    @IBAction func joinMatchBUttonTapped(_ sender: UIButton) {
        let match = (matchesList[sender.tag] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        currentMatchID = match["MATCH_ID"] as? String
        activityIndicator.startAnimating()
        preventUserInteractionLoadingView.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\": \"\(currentMatchID!)\"}", mod: "Match", actionType: "join-match", callback: { (response: Any) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.getTeamMatchesList(teamId: self.currentTeamId!)
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        })
    }
    
    @IBAction func challengeButtonTapped(_ sender: UIButton) {
        let match = (matchesList[sender.tag] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        currentMatchID = match["MATCH_ID"] as? String
        activityIndicator.startAnimating()
        preventUserInteractionLoadingView.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Match", actionType: "create-match-team-list") { (response: Any) in
            if response as? String != "error" {
                print(response)
                
                let teams = (response as! NSDictionary)["XSCData"] as! NSArray
                
                if teams.count == 0 {
                    let alert = UIAlertController(title: "No Teams", message: "You don't own a team. Create a team now?", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Create", style: UIAlertActionStyle.default, handler: { action in
                        self.pullUpControllerMoveToVisiblePoint(self.pullUpControllerMiddleStickyPoints[1], completion: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "createTeamVCFunction"), object: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let myTeamVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyTeamsViewController") as? MyTeamsViewController
                    myTeamVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    myTeamVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    myTeamVC?.teamList = teams
                    myTeamVC?.reloadTeamMatch = true
                    myTeamVC?.currentMatchID = self.currentMatchID
                    self.present(myTeamVC!, animated: true, completion: nil)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
        
    }
    
    @IBAction func oneStarButtonTapped(_ sender: UIButton) {
        let team = teamList[sender.tag] as! NSDictionary
        print("oneStar")
        currentTeamId = team["TEAM_ID"] as? String
        let alert = UIAlertController(title: "Rate Team", message: "Rate 1 star to team. Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            self.preventUserInteractionLoadingView.isHidden = false
            self.mainLoaderActivityIndicator.startAnimating()
            self.inputRating(teamID: team["TEAM_ID"] as! String, rating: 1)
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ action in
            print("No")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func twoStarButtonTapped(_ sender: UIButton) {
        let team = teamList[sender.tag] as! NSDictionary
        print("twoStar")
        currentTeamId = team["TEAM_ID"] as? String
        let alert = UIAlertController(title: "Rate Team", message: "Rate 2 star to team. Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            self.preventUserInteractionLoadingView.isHidden = false
            self.mainLoaderActivityIndicator.startAnimating()
            self.inputRating(teamID: team["TEAM_ID"] as! String, rating: 2)
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ action in
            print("No")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func threeStarButtonTapped(_ sender: UIButton) {
        let team = teamList[sender.tag] as! NSDictionary
        print("threeStar")
        currentTeamId = team["TEAM_ID"] as? String
        let alert = UIAlertController(title: "Rate Team", message: "Rate 3 star to team. Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            self.preventUserInteractionLoadingView.isHidden = false
            self.mainLoaderActivityIndicator.startAnimating()
            self.inputRating(teamID: team["TEAM_ID"] as! String, rating: 3)
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ action in
            print("No")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func fourStarButtonTapped(_ sender: UIButton) {
        let team = teamList[sender.tag] as! NSDictionary
        print("fourStar")
        currentTeamId = team["TEAM_ID"] as? String
        let alert = UIAlertController(title: "Rate Team", message: "Rate 4 star to team. Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            self.preventUserInteractionLoadingView.isHidden = false
            self.mainLoaderActivityIndicator.startAnimating()
            self.inputRating(teamID: team["TEAM_ID"] as! String, rating: 4)
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ action in
            print("No")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func fiveStarButtonTapped(_ sender: UIButton) {
        let team = teamList[sender.tag] as! NSDictionary
        print("fiveStar")
        currentTeamId = team["TEAM_ID"] as? String
        let alert = UIAlertController(title: "Rate Team", message: "Rate 5 star to team. Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            self.preventUserInteractionLoadingView.isHidden = false
            self.mainLoaderActivityIndicator.startAnimating()
            self.inputRating(teamID: team["TEAM_ID"] as! String, rating: 5)
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ action in
            print("No")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func followButtonTapped(_ sender: UIButton) {
        print("follow")
        let team = teamList[sender.tag] as! NSDictionary
        let indexPath = IndexPath(row: 0, section: sender.tag)
        let cell = teamCollectionView.cellForItem(at: indexPath) as? TeamProfileCollectionViewCell
        let fol = (cell?.follwerLabel.text)?.split{$0 == " "}.map(String.init)
        sender.isEnabled = false
        if sender.titleLabel?.text == "Follow" {
            followUnfollowTeam(teamID: team["TEAM_ID"] as! String, follow: 1, sender)
            cell?.followButton.setTitle("Unfollow", for: .normal)
            cell?.follwerLabel.text = "FOLLOWERS \(Int(fol![1])! + 1) \u{2022} FOLLOWING \(team["FOLLOWING"] as! Int)"
        } else {
            followUnfollowTeam(teamID: team["TEAM_ID"] as! String, follow: 0, sender)
            cell?.followButton.setTitle("Follow", for: .normal)
            cell?.follwerLabel.text = "FOLLOWERS \(Int(fol![1])! - 1) \u{2022} FOLLOWING \(team["FOLLOWING"] as! Int)"
        }
        cell?.layoutSubviews()
        cell?.layoutIfNeeded()
    }
    
    @IBAction func requestButtonTapped(_ sender: UIButton) {
        if let collectionView = self.teamCollectionView {
            let indexPath = IndexPath(row: 0, section: sender.tag)
            let cell = collectionView.cellForItem(at: indexPath) as? TeamProfileCollectionViewCell
            cell?.playersButton.setImage(#imageLiteral(resourceName: "player-24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            cell?.playersButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            cell?.playersButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
            cell?.matchesButton.setImage(#imageLiteral(resourceName: "match_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            cell?.matchesButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            cell?.matchesButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
            cell?.requestButton.tintColor = UIColor.black
            cell?.requestButton.setTitleColor(UIColor.black, for: .normal)
            cell?.layoutSubviews()
            cell?.layoutIfNeeded()
        }
        print("Request")
        let team = teamList[sender.tag] as! NSDictionary
        currentTeamId = team["TEAM_ID"] as? String
        switch sender.titleLabel?.text {
        case "Request":
            let alert = UIAlertController(title: "Join Team Request", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                self.preventUserInteractionLoadingView.isHidden = false
                self.mainLoaderActivityIndicator.startAnimating()
                self.joinTeamRequest(teamID: team["TEAM_ID"] as! String, sender)
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ action in
                print("No")
            }))
            self.present(alert, animated: true, completion: nil)
            break
        case "Cancel":
            let alert = UIAlertController(title: "Cancel Request", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                self.preventUserInteractionLoadingView.isHidden = false
                self.mainLoaderActivityIndicator.startAnimating()
                self.cancelTeamRequest(teamPlayerID: team["TEAM_PLAYER_ID"] as! String, sender)
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ action in
                print("No")
            }))
            self.present(alert, animated: true, completion: nil)
            break
        case "Exit":
            let alert = UIAlertController(title: "Exit Team", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                self.preventUserInteractionLoadingView.isHidden = false
                self.mainLoaderActivityIndicator.startAnimating()
                self.exitTeam(teamID: team["TEAM_ID"] as! String, sender)
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ action in
                print("No")
            }))
            self.present(alert, animated: true, completion: nil)
            break
        case "Respond":
            let alert = UIAlertController(title: "Respond Team Request", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Accept", style: UIAlertActionStyle.default, handler: { action in
                self.preventUserInteractionLoadingView.isHidden = false
                self.mainLoaderActivityIndicator.startAnimating()
                self.replyTeamRequest(teamPlayerID: team["TEAM_PLAYER_ID"] as! String, sender, accept: 1)
            }))
            alert.addAction(UIAlertAction(title: "Decline", style: UIAlertActionStyle.cancel, handler:{ action in
                self.preventUserInteractionLoadingView.isHidden = false
                self.mainLoaderActivityIndicator.startAnimating()
                self.replyTeamRequest(teamPlayerID: team["TEAM_PLAYER_ID"] as! String, sender, accept: 0)
            }))
            self.present(alert, animated: true, completion: nil)
            break
        case "Requests": icon.setImage(#imageLiteral(resourceName: "match_icon_map"), for: .normal)
                        titleLabel.text = "Requests Received"
                        self.playersCollectionView.isHidden = true
                        self.matchesCollectionView.isHidden = true
                        self.requestsTableView.isHidden = false
                        self.pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[2], completion: nil)
                        self.fetchMyRequests()
            break
        default: print("Invalid Input")
            break
        }
    }
    
    func fetchMyRequests() {
        activityIndicator.startAnimating()
        preventUserInteractionLoadingView.isHidden = true
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_ID\":\"\(currentTeamId!)\"}", mod: "Team", actionType: "requests") { (response: Any) in
            print(response)
            if response as? String != "error" {
                let requests = (response as! NSDictionary)["XSCData"] as! NSDictionary
                
                self.receivedList = requests["RECEIVED"] as! NSArray
                self.sentList = requests["SENT"] as! NSArray
                
                self.requestsTableView.reloadData()
//                if self.matchesList.count == 0 {
//                    self.noMatchesLabel.isHidden = false
//                } else {
//                    self.noMatchesLabel.isHidden = true
//                }
//                self.noTeamsLabel.isHidden = true
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    func joinTeamRequest(teamID: String,_ sender: UIButton) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"REQUEST_TYPE\":\"Received\",\"TEAM_ID\":\"\(teamID)\"}", mod: "Team", actionType: "send-request") { (response: Any) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    sender.setTitle("Cancel", for: .normal)
                    sender.setImage(#imageLiteral(resourceName: "cancel_black_24x24"), for: .normal)
                    self.getTeamProfile(addressID: self.currentAddressID!, address: self.address!, isCalledFromPlayerProfile: self.isCalledFromPlayerProfile)
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.mainLoaderActivityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    func cancelTeamRequest(teamPlayerID: String,_ sender: UIButton) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_PLAYER_ID\":\"\(teamPlayerID)\"}", mod: "Team", actionType: "cancel-request") { (response: Any) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    sender.setTitle("Request", for: .normal)
                    sender.setImage(#imageLiteral(resourceName: "request_24x24"), for: .normal)
                    self.getTeamProfile(addressID: self.currentAddressID!, address: self.address!, isCalledFromPlayerProfile: self.isCalledFromPlayerProfile)
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.mainLoaderActivityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    func replyTeamRequest(teamPlayerID: String,_ sender: UIButton, accept: Int) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_PLAYER_ID\":\"\(teamPlayerID)\",\"STATUS\":\"\(accept)\",\"REQUEST_TYPE\": \"Received\"}", mod: "Team", actionType: "request-reply") { (response: Any) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    if accept == 1 {
                        sender.setTitle("Exit", for: .normal)
                        sender.setImage(#imageLiteral(resourceName: "cancel_black_24x24"), for: .normal)
                    } else {
                        sender.setTitle("Request", for: .normal)
                        sender.setImage(#imageLiteral(resourceName: "request_24x24"), for: .normal)
                    }
                    self.getTeamProfile(addressID: self.currentAddressID!, address: self.address!, isCalledFromPlayerProfile: self.isCalledFromPlayerProfile)
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.mainLoaderActivityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    func exitTeam(teamID: String,_ sender: UIButton) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_ID\":\"\(teamID)\"}", mod: "Team", actionType: "exit-team") { (response: Any) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    sender.setTitle("Request", for: .normal)
                    sender.setImage(#imageLiteral(resourceName: "request_24x24"), for: .normal)
                    self.getTeamProfile(addressID: self.currentAddressID!, address: self.address!, isCalledFromPlayerProfile: self.isCalledFromPlayerProfile)
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.mainLoaderActivityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == playersCollectionView {
            if isCalledFromSearch {
                print("Open player profile from search")
                let player = playerList[indexPath.section] as! NSDictionary
                print(player)
                if player["USER_ID"] as! String == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String {
                    pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[1], completion: nil)
                    print("Show my profile")
                } else {
                    let encodedPlayerData = NSKeyedArchiver.archivedData(withRootObject: player)
                    UserDefaults.standard.set(encodedPlayerData, forKey: CommonUtil.SELECTEDPLAYERFROMTEAMPROFILE)
                    pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[1], completion: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getPlayerProfileFromSearch"), object: nil)
                }
            } else {
                let player = playerList[indexPath.section] as! NSDictionary
                print(player)
                if player["USER_ID"] as! String == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String {
                    pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[1], completion: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showMyProfile"), object: nil)
                } else {
                    let encodedPlayerData = NSKeyedArchiver.archivedData(withRootObject: player)
                    UserDefaults.standard.set(encodedPlayerData, forKey: CommonUtil.SELECTEDPLAYERFROMTEAMPROFILE)
                    pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[1], completion: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openPlayerVCFromTeamProfile"), object: nil)
                }
            }
        } else if collectionView == matchesCollectionView {
            let score = (matchesList[indexPath.section] as! NSDictionary)["SCORE_DETAILS"] as! NSDictionary
            let match = (matchesList[indexPath.section] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
            currentMatchID = match["MATCH_ID"] as? String
            if score["STATUS"] as! Int == 1 {
                if score["IS_SCORER"] as! Int == 1 {
                    let scorerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScorerViewController") as? ScorerViewController
                    scorerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    scorerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    scorerVC?.MATCH_ID = match["MATCH_ID"] as? String
                    self.present(scorerVC!, animated: true, completion: nil)
                } else {
                    if CommonUtil.NewLiveMatchFlow {
                        print("Go to non scorer activity")
                        let non_scorerVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "TeamScorerViewController") as? TeamScorerViewController
                        non_scorerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        non_scorerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        non_scorerVC?.MATCH_ID = match["MATCH_ID"] as? String
                        self.present(non_scorerVC!, animated: true, completion: nil)
                    } else {
                        print("Go to non scorer activity")
                        let non_scorerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NonScorerViewController") as? NonScorerViewController
                        non_scorerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        non_scorerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        non_scorerVC?.MATCH_ID = match["MATCH_ID"] as? String
                        self.present(non_scorerVC!, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == teamCollectionView {
            return CGSize(width: self.teamCollectionView.frame.width, height: self.teamCollectionView.frame.height)
        } else if collectionView == playersCollectionView {
            let screensize = playersCollectionView.bounds.size
            var cellwidth = floor(screensize.width) / 2
            let cellheight = floor(playersCollectionView.bounds.height * 0.9)
            
            if UIDevice.current.userInterfaceIdiom == .pad {
                cellwidth = floor(screensize.width) / 3
            }
            
            let layout = playersCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.itemSize = CGSize(width: cellwidth, height: cellheight)
            return layout.itemSize
        } else {
            
            return CGSize(width: collectionView.bounds.width / CGFloat(1), height: collectionView.bounds.height / CGFloat(1))

//            let screensize = matchesCollectionView.bounds.size
//            var cellwidth = floor(screensize.width * 0.8)
//            let cellheight = floor(matchesCollectionView.bounds.height * 0.95)
//
//            if UIDevice.current.userInterfaceIdiom == .pad {
//                cellwidth = floor(screensize.width * 0.6)
//            }
//
//            let insetX = (matchesCollectionView.bounds.width - cellwidth) / 2.0
//            let insetY = (matchesCollectionView.bounds.height - cellheight) / 2.0
//
//            let layout = matchesCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
//            layout.itemSize = CGSize(width: cellwidth, height: cellheight)
//            matchesCollectionView.contentInset = UIEdgeInsetsMake(insetY, insetX, insetY, insetX)
//            return layout.itemSize
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    @objc func dismissTeamProfileFunction() {
        pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[1], completion: nil)
    }
    
    override var pullUpControllerPreferredSize: CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 600)
    }
    
    override var pullUpControllerPreviewOffset: CGFloat {
        return pullUpControllerMiddleStickyPoints[0]
    }
    
    override var pullUpControllerMiddleStickyPoints: [CGFloat] {
        return [320,-100,600]
    }
    
    override var pullUpControllerIsBouncingEnabled: Bool {
        return true
    }
    
    override var pullUpControllerPreferredLandscapeFrame: CGRect {
        return CGRect(x: 5, y: 5, width: 280, height: UIScreen.main.bounds.height - 10)
    }
    
    func followUnfollowTeam(teamID: String, follow: Int, _ sender: UIButton) {
//        mainLoaderActivityIndicator.startAnimating()
//        preventUserInteractionLoadingView.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"TEAM\",\"REFERENCE_ID\":\"\(teamID)\",\"IS_FOLLOW\":\(follow)}", mod: "Connections", actionType: "update-following") { (response: Any) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    
                } else {
                    if sender.titleLabel?.text == "Follow" {
                        sender.setTitle("Unfollow", for: .normal)
                    } else {
                        sender.setTitle("Follow", for: .normal)
                    }
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            sender.isEnabled = true
//            self.mainLoaderActivityIndicator.stopAnimating()
//            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print(Int(scrollView.contentOffset.x) / Int(scrollView.frame.width))
        let section = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        if scrollView == teamCollectionView {
            currentTeamId = (teamList[section] as! NSDictionary)["TEAM_ID"] as? String
            if (teamList[section] as! NSDictionary)["IS_ADMIN"] as! Int == 1 {
                addPlayerButton.isHidden = false
            } else {
                addPlayerButton.isHidden = true
            }
            icon.setImage(#imageLiteral(resourceName: "my-players-24x24"), for: .normal)
            titleLabel.text = "My Players"
            noMatchesLabel.isHidden = true
            self.playersCollectionView.isHidden = false
            self.matchesCollectionView.isHidden = true
            self.requestsTableView.isHidden = true
            let team = teamList[section] as! NSDictionary
            currentTeamId = team["TEAM_ID"] as? String
            pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[0], completion: nil)
            getTeamPlayerList(teamID: team["TEAM_ID"] as! String)
        }
    }
    
    func inputRating(teamID: String, rating: Int) {
        preventUserInteractionLoadingView.isHidden = true
        currentTeamId = teamID
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"REFERENCE_ID\":\"\(teamID)\",\"TYPE\":\"TEAM\",\"RATING\":\"\(rating)\"}", mod: "Connections", actionType: "update-rating") { (response: Any) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.getTeamProfile(addressID: self.currentAddressID!, address: self.address!, isCalledFromPlayerProfile: self.isCalledFromPlayerProfile)
                } else {
                    self.showAlert(title: "Oops!", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.mainLoaderActivityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    func getTeamProfile(addressID: String, address: String, isCalledFromPlayerProfile: Bool) {
        if isCalledFromPlayerProfile {
            let selectedTeam = UserDefaults.standard.object(forKey: CommonUtil.SELECTEDTEAMFROMPLAYERPROFILE) as! Data
            let decodedselectedTeam = NSKeyedUnarchiver.unarchiveObject(with: selectedTeam) as! NSDictionary
            currentAddressID = addressID
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"TEAM\",\"TEAM_ID\":\"\(decodedselectedTeam["TEAM_ID"] as! String)\"}", mod: "Team", actionType: "dashborad-team-profile") { (response: Any) in
                print(response)
                if response as? String != "error" {
                    self.teamDetails = response
                    self.teamList = (self.teamDetails as! NSDictionary)["XSCData"] as! NSArray
                    self.teamCollectionView.reloadData()
                    self.getTeamPlayerList(teamID: decodedselectedTeam["TEAM_ID"] as! String)
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.mainLoaderActivityIndicator.stopAnimating()
                self.preventUserInteractionLoadingView.isHidden = true
            }
        } else {
            currentAddressID = addressID
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"TEAM\",\"ADDRESS_ID\":\"\(addressID)\"}", mod: "Map", actionType: "venue-items") { (response: Any) in
                print(response)
                if response as? String != "error" {
                    self.teamDetails = response
                    self.teamList = (self.teamDetails as! NSDictionary)["XSCData"] as! NSArray
                    self.teamCollectionView.reloadData()
                    self.getTeamPlayerList(teamID: self.currentTeamId!)
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.mainLoaderActivityIndicator.stopAnimating()
                self.preventUserInteractionLoadingView.isHidden = true
            }
        }
    }
    
    func getTeamMatchesList(teamId: String) {
        activityIndicator.startAnimating()
        preventUserInteractionLoadingView.isHidden = true
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_ID\":\"\(teamId)\"}", mod: "Match", actionType: "team-match-list") { (response: Any) in
            print(response)
            if response as? String != "error" {
                self.matchesList = (response as! NSDictionary)["XSCData"] as! NSArray
                self.matchesCollectionView.reloadData()
                if self.matchesList.count == 0 {
                    if self.teamCollectionView.isHidden {
                        self.noMatchesLabel.isHidden = false
                    }
                } else {
                    self.noMatchesLabel.isHidden = true
                }
                self.noTeamsLabel.isHidden = true
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    func getTeamPlayerList(teamID: String) {
        activityIndicator.startAnimating()
        self.preventUserInteractionLoadingView.isHidden = true
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_ID\":\"\(teamID)\"}", mod: "Team", actionType: "team-player-list") { (response: Any) in
            print(response)
            if response as? String != "error" {
                self.playerList = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["PLAYERS"] as! NSArray
                self.playersCollectionView.reloadData()
                if self.playerList.count == 0 {
                    if self.matchesCollectionView.isHidden {
                        self.noTeamsLabel.isHidden = false
                    }
                } else {
                    self.noTeamsLabel.isHidden = true
                }
                self.noMatchesLabel.isHidden = true
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    func replyToTeamRequest(requestSupportId: String, accept: Int) {
        activityIndicator.startAnimating()
        self.preventUserInteractionLoadingView.isHidden = true
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_PLAYER_ID\":\"\(requestSupportId)\",\"STATUS\":\"\(accept)\",\"REQUEST_TYPE\": \"Sent\"}", mod: "Team", actionType: "request-reply") { (response: Any) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.fetchMyRequests()
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    func cancelTeamRequestSentToPlayer(teamPlayerId: String) {
        activityIndicator.startAnimating()
        self.preventUserInteractionLoadingView.isHidden = true
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_PLAYER_ID\":\"\(teamPlayerId)\"}", mod: "Team", actionType: "cancel-request") { (response: Any) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.fetchMyRequests()
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
}























