//
//  ScorerViewController.swift
//  CricDost
//
//  Created by Jit Goel on 6/25/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ScorerViewController: UIViewController, ShowsAlert {
    
    var MATCH_ID: String?
    @IBOutlet weak var navBar: UINavigationBar!
    var matchDetial: NSDictionary?
    var teamA: NSDictionary?
    var teamB: NSDictionary?
    @IBOutlet weak var preventUserInteraction: UIView!
    @IBOutlet weak var teamAName: UILabel!
    @IBOutlet weak var teamBName: UILabel!
    @IBOutlet weak var teamAScore: UILabel!
    @IBOutlet weak var teamBScore: UILabel!
    @IBOutlet weak var matchStatusLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var scoreKeyView: UIView!
    @IBOutlet weak var batsman1Name: UIButton!
    @IBOutlet weak var batsman2Name: UIButton!
    @IBOutlet weak var C1_batsman1: UILabel!
    @IBOutlet weak var C2_batsman1: UILabel!
    @IBOutlet weak var C3_batsman1: UILabel!
    @IBOutlet weak var C4_batsman1: UILabel!
    @IBOutlet weak var C5_batsman1: UILabel!
    @IBOutlet weak var C1_batsman2: UILabel!
    @IBOutlet weak var C2_batsman2: UILabel!
    @IBOutlet weak var C3_batsman2: UILabel!
    @IBOutlet weak var C4_batsman2: UILabel!
    @IBOutlet weak var C5_batsman2: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var undoStackView: UIStackView!
    var playersList: NSMutableArray = []
    
    @IBOutlet weak var bowlerName: UIButton!
    @IBOutlet weak var C1_Bowler: UILabel!
    @IBOutlet weak var C2_Bowler: UILabel!
    @IBOutlet weak var C3_Bowler: UILabel!
    @IBOutlet weak var C4_Bowler: UILabel!
    @IBOutlet weak var C5_Bowler: UILabel!
    var nextBall: Double?
    
    @IBOutlet weak var scoreAdderButton: UIButton!
    @IBOutlet weak var undoButton: UIButton!
    @IBOutlet weak var zeroButton: UIButton!
    @IBOutlet weak var oneButton: UIButton!
    @IBOutlet weak var twoButton: UIButton!
    @IBOutlet weak var threeButton: UIButton!
    @IBOutlet weak var fourButton: UIButton!
    @IBOutlet weak var fiveButton: UIButton!
    @IBOutlet weak var sixButton: UIButton!
    @IBOutlet weak var sevenButton: UIButton!
    @IBOutlet weak var wideButton: UIButton!
    @IBOutlet weak var noBallButton: UIButton!
    @IBOutlet weak var wicketButton: UIButton!
    @IBOutlet weak var bysButton: UIButton!
    @IBOutlet weak var legBysButton: UIButton!
    @IBOutlet weak var reballButton: UIButton!
    
    var selectedRun = 0
    var runs = 0
    var extraRuns = 0
    var extraType = ""
    var extraType_str = ""
    var isWicket = false
    var currentBatsMan = "0"
    var currentNonStriker = "0"
    var currentBowler = "0"
    var currentFielder = "0"
    var whoIsOut = "0"
    var howOutStr = ""
    var currentMatchState = ""
    
    var score = true
    var undo = true
    var wide = true
    var noball = true
    var wicket = true
    var bys = true
    var legbys = true
    var reball = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if let statusbar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
            statusbar.backgroundColor = CommonUtil.themeRed
        }
        NotificationCenter.default.addObserver(self, selector: #selector(refreshScorerPage), name: NSNotification.Name(rawValue: "refreshScorerPage"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissScorerView), name: NSNotification.Name(rawValue: "dismissScorerView"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(wicketAddScore), name: NSNotification.Name(rawValue: "wicketAddScore"), object: nil)
        
        navBarElevation()
        getScore()
        reballButton.isEnabled = false
        undoButton.isHidden = true
        undoStackView.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    @objc func dismissScorerView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func refreshScorerPage() {
        getScore()
    }
    
    @IBAction func refreshButtonClick(_ sender: Any) {
        getScore()
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "scorer_fullscorecard" {
            UserDefaults.standard.set(MATCH_ID, forKey: CommonUtil.CURRENTSCOREMATCHID)
        }
    }
    
    @IBAction func menuOptionsClick(_ sender: Any) {
        let alert = UIAlertController(title: "Options", message: "Choose an action", preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .popover
        alert.addAction(UIAlertAction(title: "Full Score Card", style: .default, handler: { _ in
            print("FULL SCORE")
            self.performSegue(withIdentifier: "scorer_fullscorecard", sender: self)
        }))
        if matchDetial!["status_str"] as! String == "LIVE" {
            alert.addAction(UIAlertAction(title: "Change Scorer", style: .default, handler: { _ in
                print("Change Scorer")
                let changeScorerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChangeScorerViewController") as! ChangeScorerViewController
                changeScorerVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                changeScorerVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                changeScorerVC.playerList = self.playersList
                changeScorerVC.MATCH_ID = self.MATCH_ID
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissMatchDetailView"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissMyProfileFunction"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissTeamProfileFunction"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissPlayerProfileFunction"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissFixedMatchesFunction"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissOpenMatchesFunction"), object: nil)
                self.present(changeScorerVC, animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Break", style: .default, handler: { _ in
                print("Break")
                let breakVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BreakViewController") as! BreakViewController
                breakVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                breakVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                breakVC.abandon = false
                breakVC.editOver = false
                breakVC.MATCH_ID = self.MATCH_ID
                self.present(breakVC, animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Abandon", style: .default, handler: { _ in
                print("Abandon")
                let breakVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BreakViewController") as! BreakViewController
                breakVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                breakVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                breakVC.abandon = true
                breakVC.editOver = false
                breakVC.MATCH_ID = self.MATCH_ID
                self.present(breakVC, animated: true, completion: nil)
            }))
            if matchDetial!["current_inning"] as! String == "1" {
                alert.addAction(UIAlertAction(title: "Edit Over", style: .default, handler: { _ in
                    print("Edit Over")
                    let breakVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BreakViewController") as! BreakViewController
                    breakVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    breakVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    breakVC.abandon = false
                    breakVC.editOver = true
//                    breakVC.nextBall = self.matchDetial!["next_ball"] as! Double
                    breakVC.nextBall = self.nextBall!
                    breakVC.over = self.matchDetial!["overs"] as! String
                    breakVC.MATCH_ID = self.MATCH_ID
                    self.present(breakVC, animated: true, completion: nil)
                }))
            }
        }
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            print("IPAD")
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            alert.popoverPresentationController?.permittedArrowDirections = .down
            
        default:
            break
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func getScore() {
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\":\"\(MATCH_ID!)\"}", mod: "MatchScore", actionType: "match-score-details") { (response) in
            if response as? String != "error" {
                print(response)
                self.matchDetial = (response as! NSDictionary)["XSCData"] as? NSDictionary
                self.refreshScoreScreen(matchDetail: self.matchDetial!)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        }
    }
    
    func refreshScoreScreen(matchDetail: NSDictionary) {
        teamA = (matchDetial!["team"] as! NSDictionary)["teamA"] as? NSDictionary
        teamB = (matchDetial!["team"] as! NSDictionary)["teamB"] as? NSDictionary
        
        teamAName.text = (teamA!["name"] as? String)?.uppercased()
        teamBName.text = (teamB!["name"] as? String)?.uppercased()
        
        nextBall = matchDetial!["next_ball"] as? Double
        
        UserDefaults.standard.set(teamA!["name"] as! String, forKey: CommonUtil.CURRENT_MATCH_TEAM_A_NAME)
        UserDefaults.standard.set((teamB!["name"] as! String), forKey: CommonUtil.CURRENT_MATCH_TEAM_B_NAME)
        
        teamAScore.text = "\(teamA!["run_str"] as! String) \(teamA!["over_str"] as! String)"
        teamBScore.text = "\(teamB!["run_str"] as! String) \(teamB!["over_str"] as! String)"
        
        matchStatusLabel.text = matchDetail["result"] as? String
        playersList = []
        for player in (self.teamA!["players"] as? NSArray ?? []) {
            playersList.add(player)
        }
        
        for player in (self.teamB!["players"] as? NSArray ?? []) {
            playersList.add(player)
        }
        
        print(matchDetail["batsman"] as! NSArray)
        print(matchDetail["bowler"] as! NSArray)
        
        if (matchDetail["bowler"] as! NSArray).count == 1 {
            let bowler1 = (matchDetail["bowler"] as! NSArray)[0] as! NSDictionary
            
            bowlerName.setTitle(bowler1["FULL_NAME"] as? String, for: .normal)
            C1_Bowler.text = bowler1["OVERS"] as? String
            C2_Bowler.text = bowler1["MAIDEN"] as? String
            C3_Bowler.text = bowler1["RUNS"] as? String
            C4_Bowler.text = bowler1["WICKETS"] as? String
            C5_Bowler.text = bowler1["ECONOMY"] as? String
            
            currentBowler = bowler1["BOWLER"] as! String
        } else {
            bowlerName.setTitle("", for: .normal)
            C1_Bowler.text = ""
            C2_Bowler.text = ""
            C3_Bowler.text = ""
            C4_Bowler.text = ""
            C5_Bowler.text = ""
            
            currentBowler = ""
        }
        
        if (matchDetail["batsman"] as! NSArray).count == 1 {
            let batsman1 = (matchDetail["batsman"] as! NSArray)[0] as! NSDictionary
            
            batsman1Name.setTitle(batsman1["FULL_NAME"] as? String, for: .normal)
            C1_batsman1.text = batsman1["RUNS"] as? String
            C2_batsman1.text = batsman1["BALLS"] as? String
            C3_batsman1.text = batsman1["4S"] as? String
            C4_batsman1.text = batsman1["6S"] as? String
            C5_batsman1.text = batsman1["STRIKE_RATE"] as? String
            
            if batsman1["STRIKER"] as! String == "1" {
                batsman1Name.setImage(#imageLiteral(resourceName: "bat_24"), for: .normal)
                currentBatsMan = batsman1["BATSMAN"] as! String
            } else {
                batsman1Name.setImage(#imageLiteral(resourceName: "grey-bat_24"), for: .normal)
                currentNonStriker = batsman1["BATSMAN"] as! String
            }
            
        } else if (matchDetail["batsman"] as! NSArray).count == 2 {
            let batsman1 = (matchDetail["batsman"] as! NSArray)[0] as! NSDictionary
            let batsman2 = (matchDetail["batsman"] as! NSArray)[1] as! NSDictionary
            
            batsman1Name.setTitle(batsman1["FULL_NAME"] as? String, for: .normal)
            C1_batsman1.text = batsman1["RUNS"] as? String
            C2_batsman1.text = batsman1["BALLS"] as? String
            C3_batsman1.text = batsman1["4S"] as? String
            C4_batsman1.text = batsman1["6S"] as? String
            C5_batsman1.text = batsman1["STRIKE_RATE"] as? String
            
            batsman2Name.setTitle(batsman2["FULL_NAME"] as? String, for: .normal)
            C1_batsman2.text = batsman2["RUNS"] as? String
            C2_batsman2.text = batsman2["BALLS"] as? String
            C3_batsman2.text = batsman2["4S"] as? String
            C4_batsman2.text = batsman2["6S"] as? String
            C5_batsman2.text = batsman2["STRIKE_RATE"] as? String
            
            if batsman1["STRIKER"] as! String == "1" {
                batsman1Name.setImage(#imageLiteral(resourceName: "bat_24"), for: .normal)
                currentBatsMan = batsman1["BATSMAN"] as! String
            } else {
                batsman1Name.setImage(#imageLiteral(resourceName: "grey-bat_24"), for: .normal)
                currentNonStriker = batsman1["BATSMAN"] as! String
            }
            
            if batsman2["STRIKER"] as! String == "1" {
                batsman2Name.setImage(#imageLiteral(resourceName: "bat_24"), for: .normal)
                currentBatsMan = batsman2["BATSMAN"] as! String
            } else {
                batsman2Name.setImage(#imageLiteral(resourceName: "grey-bat_24"), for: .normal)
                currentNonStriker = batsman2["BATSMAN"] as! String
            }
        }
        
        switch matchDetail["status_str"] as! String {
        case "STRIKER": scoreKeyView.isHidden = true
        matchStatusLabel.isHidden = false
        lineView.isHidden = false
        actionButton.isHidden = false
        actionButton.setTitle("Choose Striker", for: .normal)
            break
        case "NON-STRIKER": scoreKeyView.isHidden = true
        matchStatusLabel.isHidden = false
        lineView.isHidden = false
        actionButton.isHidden = false
        actionButton.setTitle("Choose non striker", for: .normal)
            break
        case "BOWLER":  scoreKeyView.isHidden = true
        matchStatusLabel.isHidden = false
        lineView.isHidden = false
        actionButton.isHidden = false
        actionButton.setTitle("Choose Bowler", for: .normal)
            break
        case "INNINGS-BREAK":   scoreKeyView.isHidden = true
        matchStatusLabel.isHidden = false
        lineView.isHidden = false
        actionButton.isHidden = false
        actionButton.setTitle("Start match (inning break)", for: .normal)
            break
        case "DELAYED": scoreKeyView.isHidden = true
        matchStatusLabel.isHidden = false
        lineView.isHidden = false
        actionButton.isHidden = false
        actionButton.setTitle("Start match (delayed)", for: .normal)
            break
        case "BREAK":   scoreKeyView.isHidden = true
        matchStatusLabel.isHidden = false
        lineView.isHidden = false
        actionButton.isHidden = false
        actionButton.setTitle("Resume (break)", for: .normal)
            break
        case "LIVE":    scoreKeyView.isHidden = false
        matchStatusLabel.isHidden = true
        lineView.isHidden = true
        actionButton.isHidden = true
            break
        case "SYNC":    scoreKeyView.isHidden = true
        matchStatusLabel.isHidden = false
        lineView.isHidden = false
        actionButton.isHidden = false
        actionButton.setTitle("COMPLETED", for: .normal)
            break
        case "COMPLETED":   scoreKeyView.isHidden = true
        matchStatusLabel.isHidden = false
        lineView.isHidden = false
        actionButton.isHidden = false
        actionButton.setTitle("COMPLETED", for: .normal)
        
        if CommonUtil.NewLiveMatchFlow {
            let non_scorerVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "TeamScorerViewController") as? TeamScorerViewController
            non_scorerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            non_scorerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            non_scorerVC?.MATCH_ID = self.MATCH_ID
            self.present(non_scorerVC!, animated: true, completion: nil)
        } else {
            let resultVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WinnerCupResultViewController") as? WinnerCupResultViewController
            resultVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            resultVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            resultVC?.result = matchDetail["result"] as? String
            self.present(resultVC!, animated: true, completion: nil)
        }
            break
        case "ABANDONED":   scoreKeyView.isHidden = true
        matchStatusLabel.isHidden = false
        lineView.isHidden = false
        actionButton.isHidden = false
        actionButton.setTitle("ABANDONED", for: .normal)
            break
        default:
            break
        }
        navBar.topItem?.title = "\((teamA!["name"] as! String).uppercased()) vs \((teamB!["name"] as! String).uppercased())"
        activityIndicator.stopAnimating()
    }
    
    @IBAction func actionButtonClick(_ sender: Any) {
        
        switch (actionButton.titleLabel?.text)! {
        case "Choose Striker": print("STRIKER")
        chooseStriker(striker: "1")
            break
        case "Choose non striker": print("NON-STRIKER")
        chooseStriker(striker: "0")
            break
        case "Choose Bowler": print("BOWLER")
        chooseBowler()
            break
        case "Start match (inning break)": print("INNINGS-BREAK")
        resumeMatch(status: "PLAY-ONGOING", info: "")
            break
        case "Start match (delayed)": print("DELAYED")
        resumeMatch(status: "PLAY-ONGOING", info: "")
            break
        case "Resume (break)": print("BREAK")
        resumeMatch(status: "PLAY-ONGOING", info: "")
            break
        case "COMPLETED": print("COMPLETED")
            break
        case "ABANDONED": print("ABANDONED")
        resumeMatch(status: "PLAY-ONGOING", info: "")
            break
        default:
            break
        }
    }
    
    func chooseStriker(striker: String) {
        teamA = (matchDetial!["team"] as! NSDictionary)["teamA"] as? NSDictionary
        teamB = (matchDetial!["team"] as! NSDictionary)["teamB"] as? NSDictionary
        var batsmanList: NSArray = []
        if matchDetial!["current_inning"] as! String == "1" {
            print("Team A is batting")
            batsmanList = teamA!["players"] as! NSArray
        } else {
            print("Team B is batting")
            batsmanList = teamB!["players"] as! NSArray
        }
        let chooseStrikerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseStrikerViewController") as? ChooseStrikerViewController
        chooseStrikerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        chooseStrikerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        chooseStrikerVC?.batsmanList = batsmanList
        chooseStrikerVC?.striker = striker
        self.present(chooseStrikerVC!, animated: true, completion: nil)
    }
    
    func chooseBowler() {
        teamA = (matchDetial!["team"] as! NSDictionary)["teamA"] as? NSDictionary
        teamB = (matchDetial!["team"] as! NSDictionary)["teamB"] as? NSDictionary
        var bowlerList: NSArray = []
        if matchDetial!["current_inning"] as! String == "2" {
            print("Team A is bowling")
            bowlerList = teamA!["players"] as! NSArray
        } else {
            print("Team B is bowling")
            bowlerList = teamB!["players"] as! NSArray
        }
        let chooseStrikerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseBowlerViewController") as? ChooseBowlerViewController
        chooseStrikerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        chooseStrikerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        chooseStrikerVC?.bowlerList = bowlerList
        self.present(chooseStrikerVC!, animated: true, completion: nil)
    }
    
    func resumeMatch(status: String, info: String) {
        activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\":\"\(MATCH_ID!)\", \"STATUS\":\"\(status)\",\"ADDITIONAL_INFO\":\"\(info)\"}", mod: "MatchScore", actionType: "update-match-status") { (response) in
            if response as? String != "error" {
                print(response)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshScorerPage"), object: nil)
                SocketConnectionsclass.matchScoreUpdateEmit(match_id: self.MATCH_ID!)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
        }
    }
    
    func navBarElevation() {
        
        navBar.layer.shadowColor = UIColor.black.cgColor
        navBar.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        navBar.layer.shadowRadius = 2.0
        navBar.layer.shadowOpacity = 0.5
        navBar.layer.masksToBounds = false
    }
    
    
    @IBAction func scoreButtonClick(_ sender: Any) {
        
    }
    
    @IBAction func undoButtonClick(_ sender: Any) {
        clearButtons()
    }
    
    func clearButtons() {
        
        scoreAdderButton.setTitle("", for: .normal)
        runs = 0
        extraRuns = 0
        selectedRun = 0
        extraType = ""
        extraType_str = ""
        isWicket = false
        wicket = true
        sliptOver()
        reballButton.isEnabled = false
        undoButton.isHidden = true
        undoStackView.isHidden = true
        scoreAdderButton.isHidden = true
        if wicketButton.backgroundColor == CommonUtil.themeRed {
            print("white color====")
            wicketButton.backgroundColor = UIColor.white
        }
        zeroButton.backgroundColor = UIColor.white
        oneButton.backgroundColor = UIColor.white
        twoButton.backgroundColor = UIColor.white
        threeButton.backgroundColor = UIColor.white
        fourButton.backgroundColor = UIColor.white
        fiveButton.backgroundColor = UIColor.white
        sixButton.backgroundColor = UIColor.white
        sevenButton.backgroundColor = UIColor.white
        wideButton.backgroundColor = UIColor.white
        noBallButton.backgroundColor = UIColor.white
        bysButton.backgroundColor = UIColor.white
        legBysButton.backgroundColor = UIColor.white
    }
    
    @IBAction func zeroButtonClick(_ sender: Any) {
        if zeroButton.backgroundColor == CommonUtil.themeRed {
            zeroButton.backgroundColor = UIColor.white
            selectedRun = 0
        } else {
            zeroButton.backgroundColor = CommonUtil.themeRed
            oneButton.backgroundColor = UIColor.white
            twoButton.backgroundColor = UIColor.white
            threeButton.backgroundColor = UIColor.white
            fourButton.backgroundColor = UIColor.white
            fiveButton.backgroundColor = UIColor.white
            sixButton.backgroundColor = UIColor.white
            sevenButton.backgroundColor = UIColor.white
            selectedRun = 0
        }
        generateDataForRunInfo()
    }
    @IBAction func oneButtonClick(_ sender: Any) {
        if oneButton.backgroundColor == CommonUtil.themeRed {
            oneButton.backgroundColor = UIColor.white
            selectedRun = 0
        } else {
            zeroButton.backgroundColor = UIColor.white
            oneButton.backgroundColor = CommonUtil.themeRed
            twoButton.backgroundColor = UIColor.white
            threeButton.backgroundColor = UIColor.white
            fourButton.backgroundColor = UIColor.white
            fiveButton.backgroundColor = UIColor.white
            sixButton.backgroundColor = UIColor.white
            sevenButton.backgroundColor = UIColor.white
            selectedRun = 1
        }
        generateDataForRunInfo()
    }
    @IBAction func twoButtonClick(_ sender: Any) {
        if twoButton.backgroundColor == CommonUtil.themeRed {
            twoButton.backgroundColor = UIColor.white
            selectedRun = 0
        } else {
            zeroButton.backgroundColor = UIColor.white
            oneButton.backgroundColor = UIColor.white
            threeButton.backgroundColor = UIColor.white
            fourButton.backgroundColor = UIColor.white
            fiveButton.backgroundColor = UIColor.white
            sixButton.backgroundColor = UIColor.white
            sevenButton.backgroundColor = UIColor.white
            twoButton.backgroundColor = CommonUtil.themeRed
            selectedRun = 2
        }
        generateDataForRunInfo()
    }
    @IBAction func threeButtonClick(_ sender: Any) {
        if threeButton.backgroundColor == CommonUtil.themeRed {
            threeButton.backgroundColor = UIColor.white
            selectedRun = 0
        } else {
            zeroButton.backgroundColor = UIColor.white
            oneButton.backgroundColor = UIColor.white
            twoButton.backgroundColor = UIColor.white
            fourButton.backgroundColor = UIColor.white
            fiveButton.backgroundColor = UIColor.white
            sixButton.backgroundColor = UIColor.white
            sevenButton.backgroundColor = UIColor.white
            threeButton.backgroundColor = CommonUtil.themeRed
            selectedRun = 3
        }
        generateDataForRunInfo()
    }
    
    @IBAction func fourButtonClick(_ sender: Any) {
        if fourButton.backgroundColor == CommonUtil.themeRed {
            fourButton.backgroundColor = UIColor.white
            selectedRun = 0
        } else {
            zeroButton.backgroundColor = UIColor.white
            oneButton.backgroundColor = UIColor.white
            twoButton.backgroundColor = UIColor.white
            threeButton.backgroundColor = UIColor.white
            fiveButton.backgroundColor = UIColor.white
            sixButton.backgroundColor = UIColor.white
            sevenButton.backgroundColor = UIColor.white
            fourButton.backgroundColor = CommonUtil.themeRed
            selectedRun = 4
        }
        generateDataForRunInfo()
    }
    
    @IBAction func fiveButtonClick(_ sender: Any) {
        if fiveButton.backgroundColor == CommonUtil.themeRed {
            fiveButton.backgroundColor = UIColor.white
            selectedRun = 0
        } else {
            zeroButton.backgroundColor = UIColor.white
            oneButton.backgroundColor = UIColor.white
            twoButton.backgroundColor = UIColor.white
            threeButton.backgroundColor = UIColor.white
            fourButton.backgroundColor = UIColor.white
            sixButton.backgroundColor = UIColor.white
            sevenButton.backgroundColor = UIColor.white
            fiveButton.backgroundColor = CommonUtil.themeRed
            selectedRun = 5
        }
        generateDataForRunInfo()
    }
    
    @IBAction func sixButtonClick(_ sender: Any) {
        if sixButton.backgroundColor == CommonUtil.themeRed {
            sixButton.backgroundColor = UIColor.white
            selectedRun = 0
        } else {
            zeroButton.backgroundColor = UIColor.white
            oneButton.backgroundColor = UIColor.white
            twoButton.backgroundColor = UIColor.white
            threeButton.backgroundColor = UIColor.white
            fourButton.backgroundColor = UIColor.white
            fiveButton.backgroundColor = UIColor.white
            sevenButton.backgroundColor = UIColor.white
            sixButton.backgroundColor = CommonUtil.themeRed
            selectedRun = 6
        }
        generateDataForRunInfo()
    }
    
    @IBAction func sevenButtonClick(_ sender: Any) {
        if sevenButton.backgroundColor == CommonUtil.themeRed {
            sevenButton.backgroundColor = UIColor.white
            selectedRun = 0
        } else {
            zeroButton.backgroundColor = UIColor.white
            oneButton.backgroundColor = UIColor.white
            twoButton.backgroundColor = UIColor.white
            threeButton.backgroundColor = UIColor.white
            fourButton.backgroundColor = UIColor.white
            fiveButton.backgroundColor = UIColor.white
            sixButton.backgroundColor = UIColor.white
            sevenButton.backgroundColor = CommonUtil.themeRed
        
            selectedRun = 7
        }
        generateDataForRunInfo()
    }
    
    @IBAction func wideButtonClick(_ sender: Any) {
        if wideButton.backgroundColor == CommonUtil.themeRed {
            wideButton.backgroundColor = UIColor.white
            extraType = ""
        } else {
            wideButton.backgroundColor = CommonUtil.themeRed
            noBallButton.backgroundColor = UIColor.white
            bysButton.backgroundColor = UIColor.white
            legBysButton.backgroundColor = UIColor.white
            extraType = "wd"
        }
        generateDataForRunInfo()
    }
    
    @IBAction func noBallButtonClick(_ sender: Any) {
        if noBallButton.backgroundColor == CommonUtil.themeRed {
            noBallButton.backgroundColor = UIColor.white
            extraType = ""
        } else {
            wideButton.backgroundColor = UIColor.white
            noBallButton.backgroundColor = CommonUtil.themeRed
            bysButton.backgroundColor = UIColor.white
            legBysButton.backgroundColor = UIColor.white
            extraType = "nb"
        }
        generateDataForRunInfo()
    }
    
    @IBAction func wicketsButtonClick(_ sender: Any) {
        if wicket {
            print("red color")
            wicketButton.backgroundColor = CommonUtil.themeRed
            isWicket = true
        } else {
            print("clear color color")
            wicketButton.backgroundColor = UIColor.white
            isWicket = false
        }
        wicket = !wicket
        generateDataForRunInfo()
    }
    
    @IBAction func bysButtonClick(_ sender: Any) {
        if bysButton.backgroundColor == CommonUtil.themeRed {
            bysButton.backgroundColor = UIColor.white
            extraType = ""
        } else {
            wideButton.backgroundColor = UIColor.white
            noBallButton.backgroundColor = UIColor.white
            bysButton.backgroundColor = CommonUtil.themeRed
            legBysButton.backgroundColor = UIColor.white
            extraType = "b"
        }
        generateDataForRunInfo()
    }
    
    @IBAction func legBysButtonClick(_ sender: Any) {
        if legBysButton.backgroundColor == CommonUtil.themeRed {
            legBysButton.backgroundColor = UIColor.white
            extraType = ""
        } else {
            wideButton.backgroundColor = UIColor.white
            noBallButton.backgroundColor = UIColor.white
            bysButton.backgroundColor = UIColor.white
            legBysButton.backgroundColor = CommonUtil.themeRed
            extraType = "lb"
        }
        generateDataForRunInfo()
    }
    
    @IBAction func reballButtonClick(_ sender: Any) {
        teamA = (matchDetial!["team"] as! NSDictionary)["teamA"] as? NSDictionary
        teamB = (matchDetial!["team"] as! NSDictionary)["teamB"] as? NSDictionary
        reballButton.isEnabled = false
        if isWicket {
            print("Reason for wicket")
            var bowlerList: NSArray = []
            if matchDetial!["current_inning"] as! String == "2" {
                print("Team A is bowling")
                bowlerList = teamA!["players"] as! NSArray
            } else {
                print("Team B is bowling")
                bowlerList = teamB!["players"] as! NSArray
            }
            let wicketVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WicketViewController") as? WicketViewController
            wicketVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            wicketVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            wicketVC?.batsmanList = matchDetial!["batsman"] as! NSArray
            wicketVC?.fielderList = bowlerList
            
            self.present(wicketVC!, animated: true, completion: nil)
        } else {
            addScore()
        }
    }
    
    func generateDataForRunInfo() {
        var currentBallText = ""
        runs = selectedRun
        if extraType == "" {
            extraRuns = 0
            extraType_str = ""
            
        } else {
            switch extraType {
            case "p":   extraRuns = 5
                        runs = 0
                        extraType_str = "PENALTY"
                        currentBallText = "p"
                break
            case "wd":  extraRuns = runs + 1
                        extraType_str = "WIDE"
                        currentBallText = "wd + \(runs)"
                        runs = -1
                        reballButton.setTitle("Re-ball", for: .normal)
                break
            case "nb":  extraRuns = 1
                        extraType_str = "NO BALL"
                        currentBallText = "nb +"
                        reballButton.setTitle("Re-ball", for: .normal)
                break
            case "b":   extraRuns = runs
                        extraType_str = "BYE"
                        currentBallText = "b+\(runs)"
                        runs = -1
                        sliptOver()
                break
            case "lb":  extraRuns = runs
                        extraType_str = "LEG BYE"
                        currentBallText = "lb+\(runs)"
                        runs = -1
                        sliptOver()
                break
            default:
                break
            }
        }
        if runs >= 0 {
            currentBallText = "\(currentBallText) \(runs)"
        }
        if runs == -1 {
            runs = 0
        }
        
        if isWicket {
            if currentBallText == "" {
                currentBallText = "wkt"
            } else {
                currentBallText = "\(currentBallText) + wkt"
            }
        }
        if currentBallText == "" {
            currentBallText = "0"
        }
        
        scoreAdderButton.setTitle(currentBallText, for: .normal)
        if currentBallText == "" {
            undoButton.isHidden = true
            undoStackView.isHidden = true
            scoreAdderButton.isHidden = true
        } else {
            undoButton.isHidden = false
            undoStackView.isHidden = false
            scoreAdderButton.isHidden = false
        }
        reballButton.isEnabled = true
    }
    
    
    func sliptOver() {
        let over = String(self.matchDetial!["next_ball"] as! Double).last
        if over == "6" {
            reballButton.setTitle("Next Over", for: .normal)
        } else {
            reballButton.setTitle("Next Ball", for: .normal)
        }
    }
    
    @objc func wicketAddScore(_ notification: NSNotification) {
        whoIsOut = notification.userInfo![AnyHashable("who_out")] as! String
        howOutStr = notification.userInfo![AnyHashable("how_out")] as! String
        currentFielder = notification.userInfo![AnyHashable("fielder")] as! String
//        matchDetial!["next_ball"] as! Double
        activityIndicator.startAnimating()
        let wicketInfo = isWicket ? 1 : 0
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\":\"\(MATCH_ID!)\",\"INNING\":\"\(matchDetial!["current_inning"] as! String)\",\"OVER\":\"\(nextBall!)\",\"RUNS\":\"\(runs)\",\"EXTRAS\":\"\(extraRuns)\",\"EXTRAS_TYPE\":\"\(extraType_str)\",\"BATSMAN\":\"\(currentBatsMan)\",\"BOWLER\":\"\(currentBowler)\",\"FIELDER\":\"\(currentFielder)\",\"HOW_OUT\":\"\(howOutStr)\",\"WHO_OUT\":\"\(whoIsOut)\",\"NON_STRIKER\":\"\(currentNonStriker)\",\"WICKET\":\"\(wicketInfo)\"}", mod: "MatchScore", actionType: "ball-update") { (response) in
            if response as? String != "error" {
                print("SCORED UPATDED DATA", response)
                let data = (response as! NSDictionary)["XSCData"] as! NSDictionary
                self.onScoreUpdatedMethod(data: data)
                SocketConnectionsclass.matchScoreUpdateEmit(match_id: self.MATCH_ID!)
//                if self.isWicket || self.currentMatchState != "LIVE" {
//                    print("Wicket")
//                    return
//                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
        }
        
    }
    
    func addScore() {
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        let wicketInfo = isWicket ? 1 : 0
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\":\"\(MATCH_ID!)\",\"INNING\":\"\(matchDetial!["current_inning"] as! String)\",\"OVER\":\"\(nextBall!)\",\"RUNS\":\"\(runs)\",\"EXTRAS\":\"\(extraRuns)\",\"EXTRAS_TYPE\":\"\(extraType_str)\",\"BATSMAN\":\"\(currentBatsMan)\",\"BOWLER\":\"\(currentBowler)\",\"FIELDER\":\"\(currentFielder)\",\"HOW_OUT\":\"\(howOutStr)\",\"WHO_OUT\":\"\(whoIsOut)\",\"NON_STRIKER\":\"\(currentNonStriker)\",\"WICKET\":\"\(wicketInfo)\"}", mod: "MatchScore", actionType: "ball-update") { (response) in
            if response as? String != "error" {
                print("SCORED UPATDED DATA", response)
                if let data = (response as! NSDictionary)["XSCData"] as? NSDictionary {
                    self.onScoreUpdatedMethod(data: data)
                    SocketConnectionsclass.matchScoreUpdateEmit(match_id: self.MATCH_ID!)
                } else {
                    self.showAlert(title: "Alert", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
//                if self.isWicket || self.currentMatchState != "LIVE" {
//                    print("Wicket")
//                    return
//                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.preventUserInteraction.isHidden = true
            self.activityIndicator.stopAnimating()
        }
    }
    
    func onScoreUpdatedMethod(data: NSDictionary) {
        
        teamA = (data["team"] as! NSDictionary)["teamA"] as? NSDictionary
        teamB = (data["team"] as! NSDictionary)["teamB"] as? NSDictionary
        
        teamAScore.text = "\(teamA!["run_str"] as! String) \(teamA!["over_str"] as! String)"
        teamBScore.text = "\(teamB!["run_str"] as! String) \(teamB!["over_str"] as! String)"
        
        nextBall = data["next_ball"] as? Double
        
        matchStatusLabel.text = data["result"] as? String
        if (data["bowler"] as? NSArray) != nil {
            if (data["bowler"] as! NSArray).count == 1 {
                let bowler1 = (data["bowler"] as! NSArray)[0] as! NSDictionary
                
                bowlerName.setTitle(bowler1["FULL_NAME"] as? String, for: .normal)
                C1_Bowler.text = bowler1["OVERS"] as? String
                C2_Bowler.text = bowler1["MAIDEN"] as? String
                C3_Bowler.text = bowler1["RUNS"] as? String
                C4_Bowler.text = bowler1["WICKETS"] as? String
                C5_Bowler.text = bowler1["ECONOMY"] as? String
                
                currentBowler = bowler1["BOWLER"] as! String
            } else {
                bowlerName.setTitle("", for: .normal)
                C1_Bowler.text = ""
                C2_Bowler.text = ""
                C3_Bowler.text = ""
                C4_Bowler.text = ""
                C5_Bowler.text = ""
                
                currentBowler = "0"
            }
        }
        
        if (data["batsman"] as? NSArray) != nil {
            if (data["batsman"] as! NSArray).count == 1 {
                let batsman1 = (data["batsman"] as! NSArray)[0] as! NSDictionary
                
                batsman1Name.setTitle(batsman1["FULL_NAME"] as? String, for: .normal)
                C1_batsman1.text = batsman1["RUNS"] as? String
                C2_batsman1.text = batsman1["BALLS"] as? String
                C3_batsman1.text = batsman1["4S"] as? String
                C4_batsman1.text = batsman1["6S"] as? String
                C5_batsman1.text = batsman1["STRIKE_RATE"] as? String
                
                if batsman1["STRIKER"] as! String == "1" {
                    batsman1Name.setImage(#imageLiteral(resourceName: "bat_24"), for: .normal)
                    currentBatsMan = batsman1["BATSMAN"] as! String
                    batsman2Name.setImage(#imageLiteral(resourceName: "grey-bat_24"), for: .normal)
                } else {
                    batsman1Name.setImage(#imageLiteral(resourceName: "grey-bat_24"), for: .normal)
                    currentNonStriker = batsman1["BATSMAN"] as! String
                    batsman2Name.setImage(#imageLiteral(resourceName: "bat_24"), for: .normal)
                }
                
                batsman2Name.setTitle("Player 2", for: .normal)
                C1_batsman2.text = "0"
                C2_batsman2.text = "0"
                C3_batsman2.text = "0"
                C4_batsman2.text = "0"
                C5_batsman2.text = "0.00"
                
            } else if (data["batsman"] as! NSArray).count == 2 {
                let batsman1 = (data["batsman"] as! NSArray)[0] as! NSDictionary
                let batsman2 = (data["batsman"] as! NSArray)[1] as! NSDictionary
                
                batsman1Name.setTitle(batsman1["FULL_NAME"] as? String, for: .normal)
                C1_batsman1.text = batsman1["RUNS"] as? String
                C2_batsman1.text = batsman1["BALLS"] as? String
                C3_batsman1.text = batsman1["4S"] as? String
                C4_batsman1.text = batsman1["6S"] as? String
                C5_batsman1.text = batsman1["STRIKE_RATE"] as? String
                
                batsman2Name.setTitle(batsman2["FULL_NAME"] as? String, for: .normal)
                C1_batsman2.text = batsman2["RUNS"] as? String
                C2_batsman2.text = batsman2["BALLS"] as? String
                C3_batsman2.text = batsman2["4S"] as? String
                C4_batsman2.text = batsman2["6S"] as? String
                C5_batsman2.text = batsman2["STRIKE_RATE"] as? String
                
                if batsman1["STRIKER"] as! String == "1" {
                    batsman1Name.setImage(#imageLiteral(resourceName: "bat_24"), for: .normal)
                    currentBatsMan = batsman1["BATSMAN"] as! String
                } else {
                    batsman1Name.setImage(#imageLiteral(resourceName: "grey-bat_24"), for: .normal)
                    currentNonStriker = batsman1["BATSMAN"] as! String
                }
                
                if batsman2["STRIKER"] as! String == "1" {
                    batsman2Name.setImage(#imageLiteral(resourceName: "bat_24"), for: .normal)
                    currentBatsMan = batsman2["BATSMAN"] as! String
                } else {
                    batsman2Name.setImage(#imageLiteral(resourceName: "grey-bat_24"), for: .normal)
                    currentNonStriker = batsman2["BATSMAN"] as! String
                }
            } else {
                
                batsman1Name.setTitle("Player 1", for: .normal)
                C1_batsman1.text = "0"
                C2_batsman1.text = "0"
                C3_batsman1.text = "0"
                C4_batsman1.text = "0"
                C5_batsman1.text = "0"
                
                batsman2Name.setTitle("Player 2", for: .normal)
                C1_batsman2.text = "0"
                C2_batsman2.text = "0"
                C3_batsman2.text = "0"
                C4_batsman2.text = "0"
                C5_batsman2.text = "0.00"
            }
        }
        
        
        switch data["status_str"] as! String {
        case "STRIKER": scoreKeyView.isHidden = true
        matchStatusLabel.isHidden = false
        lineView.isHidden = false
        actionButton.isHidden = false
        actionButton.setTitle("Choose Striker", for: .normal)
            break
        case "NON-STRIKER": scoreKeyView.isHidden = true
        matchStatusLabel.isHidden = false
        lineView.isHidden = false
        actionButton.isHidden = false
        actionButton.setTitle("Choose non striker", for: .normal)
            break
        case "BOWLER":  scoreKeyView.isHidden = true
        matchStatusLabel.isHidden = false
        lineView.isHidden = false
        actionButton.isHidden = false
        actionButton.setTitle("Choose Bowler", for: .normal)
            break
        case "INNINGS-BREAK":   scoreKeyView.isHidden = true
        matchStatusLabel.isHidden = false
        lineView.isHidden = false
        actionButton.isHidden = false
        actionButton.setTitle("Start match (inning break)", for: .normal)
            break
        case "DELAYED": scoreKeyView.isHidden = true
        matchStatusLabel.isHidden = false
        lineView.isHidden = false
        actionButton.isHidden = false
        actionButton.setTitle("Start match (delayed)", for: .normal)
            break
        case "BREAK":   scoreKeyView.isHidden = true
        matchStatusLabel.isHidden = false
        lineView.isHidden = false
        actionButton.isHidden = false
        actionButton.setTitle("Resume (break)", for: .normal)
            break
        case "LIVE":    scoreKeyView.isHidden = false
        matchStatusLabel.isHidden = true
        lineView.isHidden = true
        actionButton.isHidden = true
            break
        case "SYNC":    scoreKeyView.isHidden = true
        matchStatusLabel.isHidden = false
        lineView.isHidden = false
        actionButton.isHidden = false
        actionButton.setTitle("COMPLETED", for: .normal)
            break
        case "COMPLETED":   scoreKeyView.isHidden = true
        matchStatusLabel.isHidden = false
        lineView.isHidden = false
        actionButton.isHidden = false
        actionButton.setTitle("COMPLETED", for: .normal)
            break
        case "ABANDONED":   scoreKeyView.isHidden = true
        matchStatusLabel.isHidden = false
        lineView.isHidden = false
        actionButton.isHidden = false
        actionButton.setTitle("ABANDONED", for: .normal)
            break
        default:
            break
        }
        clearButtons()
//        refreshScorerPage()
    }
}














