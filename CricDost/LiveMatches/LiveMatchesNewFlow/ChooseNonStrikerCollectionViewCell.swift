//
//  ChooseNonStrikerCollectionViewCell.swift
//  CricDost
//
//  Created by JIT GOEL on 9/18/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ChooseNonStrikerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nonStrikerImageView: UIImageView!
    @IBOutlet weak var nonStrikerNameLabel: UILabel!
    @IBOutlet weak var nonStrikerTickButton: UIButton!
    @IBOutlet weak var isStrikerImageView: UIImageView!

    override func layoutSubviews() {
        super.layoutSubviews()
        CommonUtil.imageRoundedCorners(imageviews: [nonStrikerImageView])
        CommonUtil.buttonRoundedCorners(buttons: [nonStrikerTickButton])
    }
}
