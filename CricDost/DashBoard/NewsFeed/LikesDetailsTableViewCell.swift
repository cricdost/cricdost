//
//  LikesDetailsTableViewCell.swift
//  CricDost
//
//  Created by JIT GOEL on 10/29/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class LikesDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var likesProfileImageView: UIImageView!
    @IBOutlet weak var likesProfileNameLabel: UILabel!
    @IBOutlet weak var likesAddressLabel: UILabel!
    @IBOutlet weak var followAndUnfollowButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        CommonUtil.imageRoundedCorners(imageviews: [likesProfileImageView])
        CommonUtil.buttonRoundedCorners(buttons: [followAndUnfollowButton])
        followAndUnfollowButton.layer.shadowRadius = 1
        followAndUnfollowButton.layer.shadowOpacity = 0.5
        followAndUnfollowButton.clipsToBounds = false
        followAndUnfollowButton.layer.shadowOffset = CGSize(width: 0, height: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
