//
//  LocationViewController.swift
//  CricDost
//
//  Created by Jit Goel on 5/26/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import CoreLocation
import LTMorphingLabel

class LocationViewController: UIViewController, CLLocationManagerDelegate, ShowsAlert, LTMorphingLabelDelegate{
    
    @IBOutlet weak var letsPlayButton: UIButton!
    @IBOutlet weak var frameView: UIView!
    var manager: CLLocationManager?
    var latitude = 0.00
    var longitude = 0.00
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressLoadIndicator: UIActivityIndicatorView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadingViewPreventUserInteraction: UIView!
    @IBOutlet weak var heyIsThisYourHomeLabel: LTMorphingLabel!
    @IBOutlet weak var confirmYourHomeLocationLabel: LTMorphingLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonUtil.buttonRoundedCorners(buttons: [letsPlayButton])
        
        frameView.layer.cornerRadius = 5;
        frameView.layer.masksToBounds = true;
        
        self.setLabelMorph(labels: [self.heyIsThisYourHomeLabel], effect: .anvil)
        setLabelMorph(labels: [confirmYourHomeLocationLabel], effect: .evaporate)
        
        let addressTap = UITapGestureRecognizer(target: self, action: #selector(addressTapFunction))
        addressLabel.addGestureRecognizer(addressTap)
        
        UserDefaults.standard.set(false, forKey: CommonUtil.USERSELECTEDLOCATION)
        
        OperationQueue.main.addOperation{
            self.manager = CLLocationManager()
            self.manager?.delegate = self
            self.manager?.desiredAccuracy = kCLLocationAccuracyBest
            self.manager?.requestWhenInUseAuthorization()
            self.manager?.startUpdatingLocation()
        }
    }
  
    func setLabelMorph(labels: [LTMorphingLabel], effect: LTMorphingEffect) {
        for label in labels {
            label.delegate = self
            label.morphingEffect = effect
            label.start()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        UserDefaults.standard.set(false, forKey: CommonUtil.USERSELECTEDLOCATION)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
//        heyIsThisYourHomeLabel.text = "Hey is this your home?"
//        confirmYourHomeLocationLabel.text = "Confirm your home location"
        if (UserDefaults.standard.object(forKey: CommonUtil.USERSELECTEDLOCATION) as? Bool ?? false) {
            addressLoadIndicator.startAnimating()
            latitude = UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double
            longitude = UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double
            getAddressFromLatLon(pdblLatitude: UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double, withLongitude: UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double)
        }
    }
    
    @objc func addressTapFunction(sender:UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "location_locationPicker", sender: self)
    }
    
    @IBAction func letplayButtonClick(_ sender: Any) {
        activityIndicator.startAnimating()
        loadingViewPreventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"LATITUDE\":\"\(latitude)\",\"LONGITUDE\":\"\(longitude)\",\"ADDRESS\":\"\(removeSpecialCharsFromString(text: addressLabel.text!))\"}", mod: "Player", actionType: "update-player-location", callback: {
            (response: Any) -> Void in
            print(response)
            if response as? String != "error" {
                if((response as! NSDictionary)["XSCStatus"] as! Int == 0) {
                    UserDefaults.standard.set(((response as! NSDictionary)["XSCData"] as! NSDictionary)["OPENFIRE_PASSWORD"] as! String, forKey: CommonUtil.OPENFIRE_PASSWORD)
                    UserDefaults.standard.set(((response as! NSDictionary)["XSCData"] as! NSDictionary)["OPENFIRE_USERNAME"] as! String, forKey: CommonUtil.OPENFIRE_USERNAME)
                    if CommonUtil.LandingPageCricSpace {
                        self.performSegue(withIdentifier: "location_cricspace", sender: self)
                    } else {
                        self.performSegue(withIdentifier: "location_dashbord", sender: self)
                    }
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.loadingViewPreventUserInteraction.isHidden = true
        })
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_")
        return String(text.filter {okayChars.contains($0) })
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.manager?.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            latitude = location.coordinate.latitude
            longitude = location.coordinate.longitude
            if (UserDefaults.standard.object(forKey: CommonUtil.USERSELECTEDLOCATION) as? Bool ?? false) {
                addressLoadIndicator.startAnimating()
                latitude = UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double
                longitude = UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double
                getAddressFromLatLon(pdblLatitude: UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double, withLongitude: UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double)
            } else {
                getAddressFromLatLon(pdblLatitude: location.coordinate.latitude, withLongitude: location.coordinate.longitude)
            }
        }
        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
        let lat: Double = pdblLatitude
        
        let lon: Double = pdblLongitude
        
        
        CommonUtil.getAddressForLatLng(viewcontroller: self, latitude: String(lat), longitude: String(lon), callback: {
            (address: String) -> Void in
            print("GEOCODING \(address)")
            UIView.transition(with: self.addressLabel, duration: 0.4, options: .curveEaseOut, animations: {
                
                self.addressLabel.text = address
            }, completion: nil)
            self.addressLoadIndicator.stopAnimating()
            self.letsPlayButton.isEnabled = true
        })
    }
}
