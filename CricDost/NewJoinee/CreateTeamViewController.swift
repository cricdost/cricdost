//
//  CreateTeamViewController.swift
//  CricDost
//
//  Created by Jit Goel on 5/29/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import CoreLocation
import LTMorphingLabel
import Branch

class CreateTeamViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ShowsAlert, UITextFieldDelegate, CLLocationManagerDelegate, UICollectionViewDataSource, UICollectionViewDelegate ,LTMorphingLabelDelegate {
    
    @IBOutlet weak var createTeamImageView: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var createTeamCardView: UIView!
    @IBOutlet weak var teamLocationCardView: UIView!
    @IBOutlet weak var teamCreatedCardView: UIView!
    @IBOutlet weak var teamImageView: UIImageView!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressLoadIndicator: UIActivityIndicatorView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var teamNameLabel: LTMorphingLabel!
    @IBOutlet weak var inviteNearestPlayerCardView: UIView!
    @IBOutlet weak var invitePlayerCollectionView: UICollectionView!
    @IBOutlet weak var searchPlayerTextField: UITextField!
    @IBOutlet weak var loadingViewPreventUserInteraction: UIView!
    var isOnBoardUser = true
    var imagePicker = UIImagePickerController()
    var imageData:Data?
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    var manager: CLLocationManager?
    var latitude = 0.00
    var longitude = 0.00
    var recentTeamID: Int?
    var playerList: NSArray = []
    var searchPlayerList: NSMutableArray = []
    var _selectedPlayers : [String] = []
    @IBOutlet weak var numOfSelectedPlayersLabel: UILabel!
    @IBOutlet weak var clearSelectedPlayersList: UILabel!
    @IBOutlet weak var inviteButton: UIButton!
    @IBOutlet weak var noplayerFoundLabel: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var createTeamLabel: LTMorphingLabel!
    @IBOutlet weak var confirmYourTeamLocationLabel: LTMorphingLabel!
    @IBOutlet weak var teamCreatedImageView: UIImageView!
    @IBOutlet weak var yourTeamLabel: LTMorphingLabel!
    @IBOutlet weak var teamCreatedSuccessfullyLabel: LTMorphingLabel!
    @IBOutlet weak var inviteNearestPlayerLabel: LTMorphingLabel!
    var open_InvitePlayersOnly = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        createTeamLabel.text = "Hey!Dost!!"
//        confirmYourTeamLocationLabel.text = "Hey!Dost!!"
        yourTeamLabel.text = "..."
        teamCreatedSuccessfullyLabel.text = "..."
        inviteNearestPlayerLabel.text = "..."
        
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [createTeamCardView, teamLocationCardView, teamCreatedCardView, inviteNearestPlayerCardView])
        CommonUtil.imageRoundedCorners(imageviews: [createTeamImageView])
        CommonUtil.buttonRoundedCorners(buttons: [nextButton, createButton, okButton, inviteButton])
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(gesture:)))
        teamImageView.addGestureRecognizer(tapGesture)
        
        imagePicker.delegate = self
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)
        
        let addressTap = UITapGestureRecognizer(target: self, action: #selector(addressTapFunction))
        addressLabel.addGestureRecognizer(addressTap)
        
        let outerViewTap = UITapGestureRecognizer(target: self, action: #selector(outerViewFunction))
        outerView.addGestureRecognizer(outerViewTap)
        
        UserDefaults.standard.set(false, forKey: CommonUtil.USERSELECTEDLOCATION)
        
        setCollectionViewLayout(card: teamCreatedCardView, collectionView: invitePlayerCollectionView, center: false)
        
        invitePlayerCollectionView.allowsMultipleSelection = true
        
        let clearAll = UITapGestureRecognizer(target: self, action: #selector(clearAllTapFunction))
        clearSelectedPlayersList.addGestureRecognizer(clearAll)
        
        outerView.isHidden = isOnBoardUser
        
        if open_InvitePlayersOnly {
            createTeamCardView.isHidden = true
            getPlayersNearBy(search: "")
        }
        setLabelMorph(labels: [inviteNearestPlayerLabel,createTeamLabel,confirmYourTeamLocationLabel,yourTeamLabel,teamCreatedSuccessfullyLabel,inviteNearestPlayerLabel], effect: .evaporate)
        setLabelMorph(labels: [teamNameLabel], effect: .burn)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        createTeamLabel.text = "Create Team"
        inviteNearestPlayerLabel.text = "Invite Nearest Players"
        
        if (UserDefaults.standard.object(forKey: CommonUtil.USERSELECTEDLOCATION) as? Bool ?? false) {
            addressLoadIndicator.startAnimating()
            latitude = UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double
            longitude = UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double
            getAddressFromLatLon(pdblLatitude: UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double, withLongitude: UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double)
        }
    }
    
    func setLabelMorph(labels: [LTMorphingLabel], effect: LTMorphingEffect) {
        for label in labels {
            label.delegate = self
            label.morphingEffect = effect
            label.start()
        }
    }
    
    @IBAction func clearAllTapFunction(_ sender: Any) {
        print("Clear All")
        _selectedPlayers.removeAll()
        invitePlayerCollectionView.reloadData()
        numOfSelectedPlayersLabel.text = "\(_selectedPlayers.count) Selected"
    }
    
    @objc func addressTapFunction(sender:UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "createTeam_Location", sender: self)
    }
    
    @objc func outerViewFunction(sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == searchPlayerTextField {
            getPlayersNearBy(search: textField.text!)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == searchPlayerTextField {
            searchPlayerList.removeAllObjects()
            for profile in playerList {
                let player = profile as! NSDictionary
                if (player["FULL_NAME"] as! String).containsIgnoringCase(find: textField.text!) {
                    searchPlayerList.add(player)
                }
            }
            invitePlayerCollectionView.reloadData()
            if searchPlayerList.count == 0 {
                noplayerFoundLabel.isHidden = false
            } else {
                noplayerFoundLabel.isHidden = true
            }
        }
        return true
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant = 0.0
            } else {
                self.keyboardHeightLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    @objc func imageTapped(gesture: UIGestureRecognizer) {
        print("Image Tapped")
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .popover
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            print("IPAD")
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            alert.popoverPresentationController?.permittedArrowDirections = .down
            
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func createTeamNextButton(_ sender: Any) {
        if nameTextField.isReallyEmpty {
            self.createTeamCardView.shake()
            self.showAlert(title: "Missing Info", message: "Please enter a team name")
        } else {
            UIView.transition(with: self.createTeamCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.createTeamCardView.isHidden = true
            })
            UIView.transition(with: self.teamLocationCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.teamLocationCardView.isHidden = false
            }) { (bool) in
//                self.confirmYourTeamLocationLabel.text = "Confirm your team location"
            }
            
            OperationQueue.main.addOperation {
                self.manager = CLLocationManager()
                self.manager?.delegate = self
                self.manager?.desiredAccuracy = kCLLocationAccuracyBest
                self.manager?.requestWhenInUseAuthorization()
                self.manager?.startUpdatingLocation()
            }
        }
    }
    
    @IBAction func createTeamButtonClick(_ sender: Any) {
       
        self.activityIndicator.startAnimating()
        self.loadingViewPreventUserInteraction.isHidden = false
        if imageData != nil {
            myImageUploadRequest(image: imageData!, name: nameTextField.text!, address: addressLabel.text!, lat: latitude, lon: longitude)
        } else {
            noImageSelectedUpload(name: nameTextField.text!, address: addressLabel.text!, lat: latitude, lon: longitude)
        }
    }
    
    @IBAction func okButtonClicked(_ sender: Any) {
        getPlayersNearBy(search: "")
        self.inviteNearestPlayerLabel.text = "..."
        self.inviteNearestPlayerLabel.text = "Invite Nearest Players"
    }
    
    @IBAction func skipButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            if !self.open_InvitePlayersOnly {
                if self.isOnBoardUser {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "joinMatchVCFunction"), object: nil)
                }
            }
        })
    }
    
    @IBAction func inviteButtonClick(_ sender: Any) {
        self.activityIndicator.startAnimating()
        self.loadingViewPreventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYERS_ID\": \(_selectedPlayers),\"TEAM_ID\":\"\(recentTeamID!)\"}", mod: "Team", actionType: "send-team-player-request") { (response: Any) in
            if response as? String != "error" {
                print(response)
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.dismiss(animated: true, completion: {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshMapView"), object: nil)
                        if !self.open_InvitePlayersOnly {
                            if self.isOnBoardUser {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "joinMatchVCFunction"), object: nil)
                            }
                        }
                    })
                } else {
                    self.showAlert(title: "Oops!", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.loadingViewPreventUserInteraction.isHidden = true
        }
    }
    
    func getPlayersNearBy(search: String) {
        self.activityIndicator.startAnimating()
        self.loadingViewPreventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"SEARCH_KEY\": \"\(search)\",\"TEAM_ID\":\"\(recentTeamID!)\"}", mod: "Player", actionType: "players-for-team-invite") { (response: Any) in
            if response as? String != "error" {
                let players = (response as! NSDictionary)["XSCData"] as! NSArray
                print(players)
                self.playerList = players
                self.searchPlayerList.removeAllObjects()
                for player in self.playerList {
                    self.searchPlayerList.add(player)
                }
                self.invitePlayerCollectionView.reloadData()
                if self.searchPlayerList.count == 0 {
                    self.noplayerFoundLabel.isHidden = false
                } else {
                    self.noplayerFoundLabel.isHidden = true
                }
                
                UIView.transition(with: self.teamCreatedCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                    self.teamCreatedCardView.isHidden = true
                })
                UIView.transition(with: self.inviteNearestPlayerCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                    self.inviteNearestPlayerCardView.isHidden = false
                })
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.loadingViewPreventUserInteraction.isHidden = true
        }
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            self.createTeamCardView.shake()
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image_data = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageData = UIImageJPEGRepresentation(image_data.updateImageOrientionUpSide()!, 0.025)
            self.dismiss(animated: true, completion: nil)
            UIView.transition(with: self.teamImageView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.teamImageView.image = image_data
                self.teamImageView.layer.cornerRadius = self.teamImageView.frame.height/2
                self.teamImageView.clipsToBounds = true
            })
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            latitude = location.coordinate.latitude
            longitude = location.coordinate.longitude
            if (UserDefaults.standard.object(forKey: CommonUtil.USERSELECTEDLOCATION) as? Bool ?? false) {
                addressLoadIndicator.startAnimating()
                latitude = UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double
                longitude = UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double
                getAddressFromLatLon(pdblLatitude: UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double, withLongitude: UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double)
            } else {
                getAddressFromLatLon(pdblLatitude: location.coordinate.latitude, withLongitude: location.coordinate.longitude)
            }
        }
        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return searchPlayerList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "invitePlayerNearbyCell", for: indexPath) as! InviteNearestPlayerCollectionViewCell
        
        let player = searchPlayerList[indexPath.section] as! NSDictionary
        
        CommonUtil.imageRoundedCorners(imageviews: [cell.playerImageView])
        CommonUtil.buttonRoundedCorners(buttons: [cell.tickButton])
        
        cell.playerNameLabel.text = player["FULL_NAME"] as? String
        cell.distanceLabel.text = "\(player["DISTANCE"] as! String) KM"
        cell.playerImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(player["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
        if _selectedPlayers.contains(player["PLAYER_ID"] as! String) {
            cell.isSelected=true
            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
            cell.tickButton.isHidden=false
        } else{
            cell.isSelected=false
            cell.tickButton.isHidden=true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let player = searchPlayerList[indexPath.section] as! NSDictionary
        print(player)
        if _selectedPlayers.count < 25 {
            _selectedPlayers.append(player["PLAYER_ID"] as! String)
            collectionView.reloadItems(at: [indexPath])
            numOfSelectedPlayersLabel.text = "\(_selectedPlayers.count) Selected"
        } else {
            self.showAlert(message: "Maximum number of players selected")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let player = searchPlayerList[indexPath.section] as! NSDictionary
        print(player)
        _selectedPlayers = _selectedPlayers.filter{$0 != player["PLAYER_ID"] as! String}
        collectionView.reloadItems(at: [indexPath])
        numOfSelectedPlayersLabel.text = "\(_selectedPlayers.count) Selected"
        
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
//        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude
        //21.228124
        let lon: Double = pdblLongitude
        //72.833770
        
        CommonUtil.getAddressForLatLng(viewcontroller: self, latitude: String(lat), longitude: String(lon), callback: {
            (address: String) -> Void in
            print("GEOCODING \(address)")
            UIView.transition(with: self.addressLabel, duration: 0.4, options: .curveEaseOut, animations: {
                self.addressLabel.text = address
            }, completion: nil)
            self.addressLoadIndicator.stopAnimating()
        })
    }
    
    func noImageSelectedUpload(name: String, address: String, lat: Double, lon: Double) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_NAME\":\"\(name)\",\"ADDRESS\":\"\(removeSpecialCharsFromString(text: address))\",\"LATITUDE\":\"\(lat)\",\"LONGITUDE\":\"\(lon)\"}", mod: "Team", actionType: "create-team", callback: {(response: Any) -> Void in
            if response as? String != "error" {
                if ((response as! NSDictionary)["XSCStatus"] as! Int) == 0 {
                    self.recentTeamID = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["TEAM_ID"] as? Int
                    self.teamNameLabel.text = name
                    UIView.transition(with: self.teamCreatedCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.teamCreatedCardView.isHidden = false
                        CommonUtil.animateImage(image: self.teamCreatedImageView)
                        self.yourTeamLabel.text = "Your team"
                        self.teamCreatedSuccessfullyLabel.text = "Created successfully"
                        Branch.getInstance()?.userCompletedAction("team_created")
                    })
                    UIView.transition(with: self.teamLocationCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.teamLocationCardView.isHidden = true
                    })
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCStatus"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.loadingViewPreventUserInteraction.isHidden = true
        })
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_")
        return String(text.filter {okayChars.contains($0) })
    }
    
    func myImageUploadRequest(image: Data, name: String, address: String, lat: Double, lon: Double)
    {
        let myUrl = NSURL(string: CommonUtil.BASE_URL);
        //let myUrl = NSURL(string: "http://www.boredwear.com/utils/postImage.php");
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        
        let param = [
            "app" : "CRICDOST",
            "mod": "Team",
            "actionType" : "create-team",
            "subAction": "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_NAME\":\"\(name)\",\"ADDRESS\":\"\(removeSpecialCharsFromString(text: address))\",\"LATITUDE\":\"\(lat)\",\"LONGITUDE\":\"\(lon)\"}"
            ] as [String : Any]
        print(param)
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        if(imageData==nil)  { return; }
        
        request.httpBody = createBodyWithParameters(parameters: param , filePathKey: "IMAGE", imageDataKey: image as NSData, boundary: boundary) as Data
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                self.showAlert(title: "Error", message: "Request TimeOut")
                self.activityIndicator.stopAnimating()
                self.loadingViewPreventUserInteraction.isHidden = true
                print("error=\(String(describing: error))")
                return
            }
            
            // You can print out response object
            print("******* response = \(String(describing: response))")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                print(json as Any)
                DispatchQueue.main.async {
                    if json!["XSCStatus"] as! Int == 0 {
                        self.recentTeamID = (json!["XSCData"] as! NSDictionary)["TEAM_ID"] as? Int
                        self.teamNameLabel.text = name
                        UIView.transition(with: self.teamCreatedCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                            self.teamCreatedCardView.isHidden = false
                            CommonUtil.animateImage(image: self.teamCreatedImageView)
                            self.yourTeamLabel.text = "Your team"
                            self.teamCreatedSuccessfullyLabel.text = "Created successfully"
                            Branch.getInstance()?.userCompletedAction("team_created")
                        })
                        UIView.transition(with: self.teamLocationCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                            self.teamLocationCardView.isHidden = true
                        })
                    } else {
                        self.showAlert(title: "Oops", message: json!["XSCMessage"] as! String)
                    }
                    self.activityIndicator.stopAnimating()
                    self.loadingViewPreventUserInteraction.isHidden = true
                }
            }catch
            {
                print(error)
            }
            
        }
        
        task.resume()
    }
    
    func createBodyWithParameters(parameters: [String: Any]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func setCollectionViewLayout(card: UIView, collectionView: UICollectionView, center: Bool) {
        
        if center {
            let screensize = card.bounds.size
            let cellwidth = floor(screensize.width * 0.8)
            let cellheight = floor(collectionView.bounds.height * 0.9)
            
            let insetX = (card.bounds.width - cellwidth) / 2.0
            let insetY = (collectionView.bounds.height - cellheight) / 2.0
            
            let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.itemSize = CGSize(width: cellwidth, height: cellheight)
            collectionView.contentInset = UIEdgeInsetsMake(insetY, insetX, insetY, insetX)
        } else {
            let screensize1 = card.bounds.size
            let cellwidth1 = floor(screensize1.width/3)
            let cellheight1 = floor(collectionView.bounds.height)
            
            
            let layout1 = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout1.itemSize = CGSize(width: cellwidth1, height: cellheight1)
            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }
    }
    
}
