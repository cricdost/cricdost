//
//  EditTeamProfileViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 8/1/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import SDWebImage

class EditTeamProfileViewController: UIViewController , UIImagePickerControllerDelegate , UINavigationControllerDelegate, ShowsAlert {
    @IBOutlet weak var cameraImage: UIImageView!
    @IBOutlet weak var teamProfileImage: UIImageView!
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    @IBOutlet weak var teamNameLabel: UILabel!
    @IBOutlet weak var teamLocationLabel: UILabel!
    var imagePicker = UIImagePickerController()
    var imageData: Data?
    var teamName: String?
    var teamID: String?
    var location: String?
    var profileURL: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonUtil.imageRoundedCorners(imageviews: [teamProfileImage])
        teamProfileImage.layer.borderWidth=5
        teamProfileImage.layer.borderColor = UIColor.white.cgColor
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = CommonUtil.themeRed
        }
//        UIApplication.shared.statusBarStyle = .lightContent
        
        let teamNameGesture = UITapGestureRecognizer(target: self, action: #selector(teamNameTapped))
        teamNameLabel.isUserInteractionEnabled = true
        teamNameLabel.addGestureRecognizer(teamNameGesture)
        
        let locationGesture = UITapGestureRecognizer(target: self, action: #selector(locationTapped))
        teamLocationLabel.isUserInteractionEnabled = true
        teamLocationLabel.addGestureRecognizer(locationGesture)
        
        let clickCamera = UITapGestureRecognizer(target: self, action: #selector(cameraTapped))
        cameraImage.isUserInteractionEnabled = true
        cameraImage.addGestureRecognizer(clickCamera)
        
        imagePicker.delegate = self

        teamNameLabel.text = teamName
        teamLocationLabel.text = location
        teamProfileImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(profileURL!)"), placeholderImage: #imageLiteral(resourceName: "no_team_image_icon"))
        
        NotificationCenter.default.addObserver(self, selector: #selector(setLocationForEditTeam), name: NSNotification.Name(rawValue: "setLocationForEditTeam"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(setTeamName), name: NSNotification.Name(rawValue: "setTeamName"), object: nil)
        
        print(teamID!)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func teamNameTapped() {
        print("edit team name clciked")
        
        let editTeamNameVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UpdateTeamNameViewController") as! UpdateTeamNameViewController
        editTeamNameVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        editTeamNameVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        editTeamNameVC.curTeamName = teamNameLabel.text
        self.present(editTeamNameVC, animated: true, completion: nil)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        UserDefaults.standard.set(true, forKey: CommonUtil.EDITTEAMLOCATIONACTIVATE)
    }
    
    @objc func setLocationForEditTeam() {
        print("Location added", UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double,UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double)
        let lat = UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double
        let lon = UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double
        
        getAddressFromLatLon(pdblLatitude: lat, withLongitude: lon)
    }
    
    @objc func locationTapped() {
        print("edit team location clciked")
        self.performSegue(withIdentifier: "editTeam_location", sender: self)
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
        //        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude
        //21.228124
        let lon: Double = pdblLongitude
        //72.833770
        
        CommonUtil.getAddressForLatLng(viewcontroller: self, latitude: String(lat), longitude: String(lon), callback: {
            (address: String) -> Void in
            print("GEOCODING \(address)")
            UIView.transition(with: self.teamLocationLabel, duration: 0.4, options: .curveEaseOut, animations: {
                self.teamLocationLabel.text = address
            }, completion: nil)
            self.updateTeamLocation(teamId: self.teamID!, address: address, lat: lat, lon: lon)
        })
    }
    
    @objc func setTeamName(){
        print("team name")
        let name = UserDefaults.standard.object(forKey: "teamNameText") as! String
        self.teamNameLabel.text = name
        updateTeamName(teamId: teamID!, teamName: name)
    }
    
    @objc func cameraTapped(gesture : UITapGestureRecognizer){
        print("camera Clicked")
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .popover
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        alert.addAction(UIAlertAction(title: "Remove Profile Image", style: .default, handler: { _ in
            self.removeProfileImage(teamId: self.teamID!)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera() {
        print("Opened Camera")
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery() {
        print("Opened Gallery")
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image_data = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageData = UIImageJPEGRepresentation(image_data.updateImageOrientionUpSide()!, 0.025)
            self.dismiss(animated: true, completion: nil)
            UIView.transition(with: self.teamProfileImage, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.teamProfileImage.image = image_data
                self.teamProfileImage.layer.cornerRadius = self.teamProfileImage.frame.height/2
                self.teamProfileImage.clipsToBounds = true
            })
            if imageData != nil{
                print("ImageData",imageData!)
                myImageUploadRequest(image: imageData!, teamId: teamID!)
            }
        }
    }
    
    func removeProfileImage(teamId: String) {
        print("Removed Profile Image")
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(String(describing: UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY)))\",\"TEAM_ID\":\"\(teamId)\"}", mod: "Team", actionType: "remove-team-image") { (response) in
            if response as? String != "error" {
                print(response)
                self.showAlert(title: "Message", message: (response as! NSDictionary)["XSCMessage"] as! String)
                self.teamProfileImage.image = #imageLiteral(resourceName: "no_team_image_icon")
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
        }
    }
    
    func updateTeamName(teamId: String, teamName: String) {
        
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_ID\":\"\(teamId)\",\"TEAM_NAME\":\"\(teamName)\"}", mod: "Team", actionType: "edit-team-name") { (response) in
            if response as? String != "error" {
                print(response)
                self.showAlert(title: "Message", message: (response as! NSDictionary)["XSCMessage"] as! String)
            } else{
                self.showAlert(title: "Oops!", message: "Something went worng! Please try again")
            }
        }
        
    }
    
    func updateTeamLocation(teamId: String, address: String, lat: Double, lon: Double) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_ID\":\"\(teamId)\",\"TEAM_ADDRESS\":\"\(address)\",\"TEAM_LATITUDE\":\"\(lat)\",\"TEAM_LONGITUDE\":\"\(lon)\"}", mod: "Team", actionType: "edit-team-address") { (response) in
            if response as? String != "error" {
                print(response)
                self.showAlert(title: "Message", message: (response as! NSDictionary)["XSCMessage"] as! String)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            
        }
    }
    
    func myImageUploadRequest(image: Data, teamId: String)
    {
        let myUrl = NSURL(string: CommonUtil.BASE_URL);
        //let myUrl = NSURL(string: "http://www.boredwear.com/utils/postImage.php");
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        
        request.httpMethod = "POST";
        
        let param = [
            "app" : "CRICDOST",
            "mod": "Team",
            "actionType" : "edit-team-image",
            "subAction": "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_ID\":\"\(teamId)\"}"
            ] as [String : Any]
        print(param)
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        if(imageData==nil)  { return; }
        
        request.httpBody = createBodyWithParameters(parameters: param , filePathKey: "IMAGE", imageDataKey: image as NSData, boundary: boundary) as Data
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                self.showAlert(title: "Error", message: "Request TimeOut")
                print("error=\(String(describing: error))")
                return
            }
            
            // You can print out response object
            print("******* response = \(String(describing: response))")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                print(json as Any)
                DispatchQueue.main.async {
                    if json!["XSCStatus"] as! Int == 0 {
                        let alert = UIAlertController(title: "Message", message: json!["XSCMessage"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        self.showAlert(title: "Oops", message: json!["XSCMessage"] as! String)
                    }
                }
            }catch
            {
                print(error)
            }
            
        }
        
        task.resume()
    }
    
    func createBodyWithParameters(parameters: [String: Any]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    
    
}
