//
//  CommonUtil.swift
//  CricDost
//
//  Created by Jit Goel on 5/23/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import SystemConfiguration
import GoogleMobileAds

class CommonUtil {
    
    static var LANGUAGE = "LANGUAGE"
    static let DeveloperMode = true
    static let AdMobIsOn = false
    static let NewLiveMatchFlow = true
    static let NewCreateMatchFlow = true
    static let LandingPageCricSpace = true
    static var BASE_URL = ""
    static var CHAT_AUTH_KEY = ""
    static var OTP_ENABLED_FLAG = true
    static var OPENINVITEPAGE = "OPENINVITEPAGE"
    static var MYPOINTS = "MYPOINTS"
    static let CHAT_AUTH_KEY_SHARED = "CHAT_AUTH_KEY_SHARED"
    static let OTP_ENABLED = "OTP_ENABLED"
    static let CHAT_CLIENT_API_SHARED = "CHAT_CLIENT_API_SHARED"
    static let CHAT_CLIENT_PORT_SHARED = "CHAT_CLIENT_PORT_SHARED"
    static let cricdost_group_chat_domain_name_SHARED = "cricdost_group_chat_domain_name_SHARED"
    static let cricdost_chat_domain_name_SHARED = "cricdost_chat_domain_name_SHARED"
    static let cricdost_chat_domain_SHARED = "cricdost_chat_domain_SHARED"
    static let SOCKET_URL_SHARED = "SOCKET_URL_SHARED"
    static let BASE_URL_SHARED = "BASE_URL_SHARED"
    static let SHAKE = "SHAKE"
    static let PayUMoney_KEY = "KVA3m4wr" //live
    static let PayUMoney_MERCHANT_ID = "6389193" //live
    static let PayUMoney_SALT = "WvdvqpsyIW" // live
    static var CHAT_CLIENT_API = ""
    static var CHAT_CLIENT_PORT = ""
    static var cricdost_group_chat_domain_name = ""
    static var cricdost_chat_domain_name = ""
    static var cricdost_chat_domain = ""
    static var cricdost_socket_api = ""
    static var cricdost_api_key = ""
    static let DEVICE_KEY = "DEVICE_KEY"
    static let SWIPEDEMO = "SWIPEDEMO"
    static let USER_ID = "USER_ID"
    static let ADDRESS = "ADDRESS"
    static let SKILLS = "SKILLS"
    static let USERLIST = "USERLIST"
    static let GROUNDSELECTEDLOCATION = "GROUNDSELECTEDLOCATION"
    static let TEMPGROUNDSELECTED = "TEMPGROUNDSELECTED"
    static let MATCHDETAILUPDATELOCATION = "MATCHDETAILUPDATELOCATION"
    static let CHATROOMLIST = "CHATROOMLIST"
    static let IMAGE_URL = "IMAGE_URL"
    static let FULL_NAME = "FULL_NAME"
    static let COUNTRY_CODE = "COUNTRY_CODE"
    static let MOBILE_NUMBER = "MOBILE_NUMBER"
    static let TEAM_COUNT = "TEAM_COUNT"
    static let FB_TOKEN = "FB_TOKEN"
    static let IS_NOTIFICATION_RECEIVED = "IS_NOTIFICATION_RECEIVED"
    static let NewNotification = "NewNotification"
    static let ALERTVIBRATION = "alertVibration"
    static let STEPNAME = "STEPNAME"
    static let TEMPTEAMID = "TEMPTEAMID"
    static let EDITPROFILESEGUE = "EDITPROFILESEGUE"
    static let USERSELECTEDLOCATION = "USERSELECTEDLOCATION"
    static let EDITTEAMLOCATIONACTIVATE = "EDITTEAMLOCATIONACTIVATE"
    static let PRODUCT_USERLOCATION = "PRODUCT_USERLOCATION"
    static let TEMPLATITUDE = "TEMPLATITUDE"
    static let TEMPLONGITUDE = "TEMPLONGITUDE"
    static let CREATEMATCHSEGUE = "CREATEMATCHSEGUE"
    static let SELECTEDTEAMFROMPLAYERPROFILE = "SELECTEDTEAMFROMPLAYERPROFILE"
    static let SELECTEDPLAYERFROMTEAMPROFILE = "SELECTEDPLAYERFROMTEAMPROFILE"
    static let CURRENTSCOREMATCHID = "CURRENTSCOREMATCHID"
    static let CURRENT_MATCH_TEAM_A_NAME = "CURRENT_MATCH_TEAM_A_NAME"
    static let CURRENT_MATCH_TEAM_B_NAME = "CURRENT_MATCH_TEAM_B_NAME"
    static let OPENFIRE_USERNAME = "OPENFIRE_USERNAME"
    static let OPENFIRE_PASSWORD = "OPENFIRE_PASSWORD"
    static let READMESSAGESLIST = "READMESSAGESLIST"
    static let CURRENTCHAT = "CURRENTCHAT"
    static let PRESENCE = "PRESENCE"
    static let MYMARKERS = "MYMARKERS"
    static let ISTEAMADMIN = "ISTEAMADMIN"
    static let SearchOpenMatchesDetails = "SearchOpenMatchesDetails"
    
    static let baseUrl = "https://maps.googleapis.com/maps/api/geocode/json?"
    static let apikey = "AIzaSyASZ1qo1MWK8YwuR-Alt5p0lT98vBt5rSg"
    static let themeRed = UIColor(red:0.92, green:0.28, blue:0.22, alpha:1.0)
    

    public static var SOCKET_URL = "" //testing
    
    public static func buttonRoundedCorners(buttons: [UIButton]) {
        for button in buttons {
            button.layer.cornerRadius = button.frame.height/2
            button.clipsToBounds = true
        }
    }
    
   
    public static func viewRoundedCorners(uiviews: [UIView]) {
        for view in uiviews {
            view.layer.cornerRadius = view.frame.height/2
            view.clipsToBounds = true
        }
    }
    
    public static func newButtonRoundedCorners(buttons: [UIButton]) {
        for button in buttons {
            button.layer.cornerRadius = 5
            button.clipsToBounds = true
            button.layer.masksToBounds = true
            button.layer.shadowRadius = 3
            button.layer.shadowOpacity = 0.5
        }
    }
    
    public static func roundedUIViewCornersWithShade(uiviews: [UIView]) {
        for view in uiviews {
            view.layer.cornerRadius = 5
            view.layer.masksToBounds = true
            view.layer.shadowRadius = 3
            view.layer.shadowOpacity = 0.5
            view.layer.shadowOffset = CGSize(width: 0, height: 0)
            view.clipsToBounds = false
        }
    }
    
    public static func roundedTextFieldCorners(textFields: [UITextField]) {
        for textField in textFields {
            textField.layer.masksToBounds = false
            textField.layer.cornerRadius = textField.frame.height/2
            textField.clipsToBounds = true
        }
    }
    
    public static func imageRoundedCorners(imageviews: [UIImageView]) {
        for imageview in imageviews {
            imageview.layer.masksToBounds = false
            imageview.layer.cornerRadius = imageview.frame.height/2
            imageview.clipsToBounds = true
        }
    }
    
    public static func addRightBorder(borderColor: UIColor, borderWidth: CGFloat, buttons: [UIButton]) {
        for button in buttons {
            let border = CALayer()
            border.backgroundColor = borderColor.cgColor
            border.frame = CGRect(x: button.frame.size.width - borderWidth,y: 0, width:borderWidth, height: button.frame.size.height)
            button.layer.addSublayer(border)
        }
    }
    
    public static func addLeftBorder(color: UIColor, width: CGFloat, buttons: [UIButton]) {
        for button in buttons {
            let border = CALayer()
            border.backgroundColor = color.cgColor
            border.frame = CGRect(x:0, y:0, width:width, height: button.frame.size.height)
            button.layer.addSublayer(border)
        }
    }
    
    public static func buttonImageColorchange(buttons: [UIButton], imageName: String, color: UIColor ) {
        for button in buttons {
            let origImage = UIImage(named: imageName)
            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
            button.setImage(tintedImage, for: .normal)
            button.tintColor = color
        }
    }
    
    public static func updateGATracker(screenName: String) {
        if !DeveloperMode {
            guard let tracker = GAI.sharedInstance().defaultTracker else { return }
            tracker.set(kGAIScreenName, value: screenName)
            
            guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
            tracker.send(builder.build() as? [AnyHashable: Any])
        }
    }
    
    public static func convertDateFormater(_ date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date!)
    }
    
    public static func convertStringDateFormater(_ date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd MMM yyyy"
        return  dateFormatter.string(from: date!)
    }
    
    public static func animateImage(image: UIImageView) {
        let endFrame:CGRect = image.frame;
        // Animate drop
        let delay = 0 * Double(1)
        UIView.animate(withDuration: 0.3, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations:{() in
            image.frame = endFrame
            image.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            // Animate squash
        }, completion:{(Bool) in
            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations:{() in
                image.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            }, completion: {(Bool) in
                UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations:{() in
                    image.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                }, completion: {(Bool) in
                    UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations:{() in
                        image.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                    }, completion: {(Bool) in
                        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations:{() in
                            image.transform = CGAffineTransform(scaleX: 1, y: 1)
                        }, completion: nil)
                    })
                })
            })
        })
    }
    
    public static func convertDateToStringFormater(_ date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return  dateFormatter.string(from: date!)
    }
    
    public static func timeAgoSinceDate(date:NSDate, numericDates:Bool) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = NSDate()
        let earliest = now.earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now
        let components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest as Date)
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }
    
    public class func getAddressForLatLng(viewcontroller: UIViewController, latitude: String, longitude: String, callback:@escaping (String) -> Void) {
        if (Reachability.isConnectedToNetwork()) {
            let url = NSURL(string: "\(baseUrl)latlng=\(latitude),\(longitude)&key=\(apikey)")
            if let data = NSData(contentsOf: url! as URL) {
                let json = try! JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
                var street = ""
                if let result = json["results"] as? NSArray {
                    print("ADRESSSSS ARRAY", result.count)
                    if result.count != 0 {
                        if let address = (result[0] as! NSDictionary)["address_components"] as? NSArray {
                            for add in address {
                                street = "\(street) \((add as! NSDictionary)["short_name"] as! String),"
                            }
                            DispatchQueue.main.async {
                                callback(street)
                            }
                        }
                    }
                }
            }
        } else {
            print("NO NETWORK AVAILABLE")
            let alert = UIAlertController(title: "Network Error", message: "Please check your network connection", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { action in
                getAddressForLatLng(viewcontroller: viewcontroller, latitude: latitude, longitude: longitude, callback: { (street) in
                    callback(street)
                })
            }))
            viewcontroller.present(alert, animated: true, completion: nil)
        }
    }
    
    public static func userActivityDetected(viewController: UIViewController ,activityType:String, status:String) {
        print(activityType,status)
        CommonUtil.makeRestAPICall(viewcontroller: viewController, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"USER_ID\":\"\(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String)\",\"ACTIVITY_TYPE\":\"\(activityType)\",\"STATUS\":\"\(status)\"}", mod: "UserActivity", actionType: "add-user-activity") { (response) in
            print(response)
        }
    }
    
    public class func makeRestAPICall(viewcontroller: UIViewController,subAction: String ,mod: String,actionType: String, callback:@escaping (Any) -> Void) {
        if (Reachability.isConnectedToNetwork()) {
            let url = URL(string: BASE_URL)!
            var request = URLRequest(url: url)
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            let postString = "app=CRICDOST&mod=\(mod)&actionType=\(actionType)&subAction=\(subAction)&sessionId=&subMod="
            print(postString)
            request.httpBody = postString.data(using: .utf8)
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    DispatchQueue.main.async {
                        //                    callback("error")
                        print("ATTEMPTING RELOADING -- OOPS")
                        makeRestAPICall(viewcontroller: viewcontroller, subAction: subAction, mod: mod, actionType: actionType, callback: { (response) in
                            callback(response)
                        })
                    }
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                    // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(String(describing: response))")
                    callback("error")
                }
                
                let responseString = String(data: data, encoding: .utf8)
                print("responseString = \(String(describing: responseString))")
                
                do {
                    //create json object from data
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                        //                    print(json)
                        DispatchQueue.main.async {
                            callback(json)
                        }
                    }
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            print("NO NETWORK AVAILABLE")
            let alert = UIAlertController(title: "Network Error", message: "Please check your network connection", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: { action in
                makeRestAPICall(viewcontroller: viewcontroller, subAction: subAction, mod: mod, actionType: actionType, callback: { (response) in
                    callback(response)
                })
            }))
            viewcontroller.present(alert, animated: true, completion: nil)
        }
    }
    
    public class func getJID(callback:@escaping (Any) -> Void) {
        if (Reachability.isConnectedToNetwork()) {
            
            //create the url with URL
            let url = URL(string:"\(CommonUtil.CHAT_CLIENT_API)/plugins/restapi/v1/users")
            //create the URLRequest object using the url object
            var request = URLRequest(url: url!)
            
            //set headers
            request.addValue(CommonUtil.CHAT_AUTH_KEY, forHTTPHeaderField: "Authorization")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                (data, response, error) in
                
                if(error != nil){
                    print("ERROR1")
                }else{
                    
                    if let resp = response as? HTTPURLResponse{
                        if(resp.statusCode == 200){
                            do {
                                //create json object from data
                                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary {
                                    print("OPENFIRE Roster extracted")
                                    callback(json)
                                }
                            } catch let error {
                                print(error.localizedDescription)
                            }
                        }else{
                            print("ERROR2")
                        }
                    }
                }
                
            }
            task.resume()
        } else {
            print("NO NETWORK AVAILABLE")
        }
    }
    
    public class func getChatRooomsJID(callback:@escaping (Any) -> Void) {
        if (Reachability.isConnectedToNetwork()) {
            
            //create the url with URL
            let url = URL(string:"\(CommonUtil.CHAT_CLIENT_API)/plugins/restapi/v1/chatrooms")
            //create the URLRequest object using the url object
            var request = URLRequest(url: url!)
            
            //set headers
            request.addValue(CommonUtil.CHAT_AUTH_KEY, forHTTPHeaderField: "Authorization")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                (data, response, error) in
                
                if(error != nil){
                    print("ERROR1")
                }else{
                    
                    if let resp = response as? HTTPURLResponse{
                        if(resp.statusCode == 200){
                            do {
                                //create json object from data
                                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary {
                                    callback(json)
                                }
                            } catch let error {
                                print(error.localizedDescription)
                            }
                        }else{
                            print("ERROR2")
                        }
                    }
                }
                
            }
            task.resume()
            
        } else {
            print("NO NETWORK AVAILABLE")
        }
    }
}

public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
    }
}
