//
//  CreateTournamentViewController.swift
//  CricDost
//
//  Created by Jit Goel on 8/20/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import CoreLocation
import IQKeyboardManager

class CreateTournamentViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate , ShowsAlert, UITextFieldDelegate, CLLocationManagerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource{
    
    
    
    @IBOutlet weak var dissmissViewHolder: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var preventUserInteractionView: UIView!
    
    @IBOutlet weak var tourNameView: UIView!
    @IBOutlet weak var tourImageUploadImageView: UIImageView!
    @IBOutlet weak var tourNameTextField: UITextField!
    @IBOutlet weak var tourNameNextButton: UIButton!
    
    var imagePicker = UIImagePickerController()
    var imageData:Data?
    var tournamentName: String?
    @IBOutlet weak var keyboardHeightLayoutConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tourStartDateTextField: UITextField!
    @IBOutlet weak var tourEndDateTextField: UITextField!
    @IBOutlet weak var tourWhenView: UIView!
    @IBOutlet weak var tourDateNextButton: UIButton!
    //@IBOutlet weak var keyboardHeightTourWhenView: NSLayoutConstraint!
    
    var from_date_picker: UIDatePicker?
    var to_date_picker: UIDatePicker?
    var tourStartDate: String?
    var tourEndDate: String?
    
    @IBOutlet weak var tourLocationNextButton: UIButton!
    @IBOutlet weak var tourLocationLabel: UILabel!
    @IBOutlet weak var tourLocationLabelView: UIView!
    @IBOutlet weak var tourLocationView: UIView!
    
    var manager: CLLocationManager?
    var tourLocation: String?
    
    @IBOutlet weak var tourTeamsView: UIView!
    @IBOutlet weak var tourTeamsNumberTF: UITextField!
    @IBOutlet weak var tourTeamsNextButton: UIButton!
    
    var tourTeamsPickerView: UIPickerView?
    let tourTeamsPickerNumber = ["4", "8", "16", "32", "64"]
    var tourTeams: String?
    
    @IBOutlet weak var tourOversView: UIView!
    @IBOutlet weak var tourOverNumberTF: UITextField!
    @IBOutlet weak var tourOversNextButton: UIButton!
    @IBOutlet weak var keyboardTourOverHeightView: NSLayoutConstraint!
    
    var tourOvers: String?
    
    
    @IBOutlet weak var tourSetRulesView: UIView!
    @IBOutlet weak var tourSetRuleTitleView: UIView!
    @IBOutlet weak var tourSelectRulesFromListTF: UITextField!
    @IBOutlet weak var tourSelectedRulesFromListButton: UIButton!
    @IBOutlet weak var selectedRulesTableView: UITableView!
    @IBOutlet weak var tourSetRulesContinueButton: UIButton!
    
    var tourSelectRulesPickerView: UIPickerView?
    var tourSelectRulesOptionPickerView: UIPickerView?
    @IBOutlet weak var optionTextfield: UITextField!
    var tourSelectRulesVar: String?
    var rulesReceived: NSArray = []
    var UnSelectedRules: NSMutableArray = []
    var selectedRules: NSMutableArray = []
    var optionList: NSArray = []
    var tempCurrentRuleID: String?
    var tempCurrentRuleIDOption: String?
    var tempCurrentRuleOption: NSDictionary?
    var groundID: String?
    
    @IBOutlet weak var numberofPlayersView: UIView!
    @IBOutlet weak var numberOfPlayerNextButton: UIButton!
    @IBOutlet weak var numberOfPlayersTextField: UITextField!
    var numberOfPlayers: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        IQKeyboardManager.shared().isEnabled = true

        CommonUtil.roundedUIViewCornersWithShade(uiviews: [tourNameView,tourWhenView,tourLocationView,tourTeamsView,tourOversView,tourSetRulesView,numberofPlayersView])
        
        CommonUtil.viewRoundedCorners(uiviews: [tourLocationLabelView])
        
        let dismissGesture = UITapGestureRecognizer(target: self, action: #selector(dissmissViewHolderTapped))
        dissmissViewHolder.isUserInteractionEnabled = true
        dissmissViewHolder.addGestureRecognizer(dismissGesture)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tourCreateImageTapped))
        tourImageUploadImageView.isUserInteractionEnabled = true
        tourImageUploadImageView.addGestureRecognizer(tapGesture)
        
        let tourLocationGesture = UITapGestureRecognizer(target: self, action: #selector(tourLocationTapped))
        tourLocationLabel.isUserInteractionEnabled = true
        tourLocationLabel.addGestureRecognizer(tourLocationGesture)
        
        //        self.tourLocationNextButton.isEnabled = false
        
        imagePicker.delegate = self
        
        //        NotificationCenter.default.addObserver(self,
        //                                               selector: #selector(self.keyboardNotification(notification:)),
        //                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
        //                                               object: nil)
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let curentDate = Date()
        from_date_picker = UIDatePicker()
        from_date_picker?.minimumDate = curentDate
        tourStartDateTextField.inputView = from_date_picker
        //        tourStartDateTextField.text = dateFormatter.string(from: Date())
        tourStartDateTextField.text = "Start Date"
        tourStartDateTextField.tintColor = .clear
        from_date_picker?.addTarget(self, action: #selector(handleStartDatePicker), for: UIControlEvents.valueChanged)
        from_date_picker?.datePickerMode = .date
        
        
        //        tourEndDateTextField.text = dateFormatter.string(from: Date())
        tourEndDateTextField.text = "End Date"
        
        let startDateToolBar = UIToolbar().ToolbarPiker(mySelect: #selector(startDateDismissPicker))
        tourStartDateTextField.inputAccessoryView = startDateToolBar
        
        let endDateToolBar = UIToolbar().ToolbarPiker(mySelect: #selector(endDateDismissPicker))
        tourEndDateTextField.inputAccessoryView = endDateToolBar
        
        tourTeamsPickerView = UIPickerView()
        
        tourTeamsPickerView?.dataSource = self
        tourTeamsPickerView?.delegate = self
        
        tourTeamsNumberTF.inputView = tourTeamsPickerView
        //        tourTeamsNumberTF.text = tourTeamsPickerNumber[0]
        
        let tourTeamsToolBar = UIToolbar().ToolbarPiker(mySelect: #selector(tourTeamsDismissPicker))
        tourTeamsNumberTF.inputAccessoryView = tourTeamsToolBar
        
        tourSelectRulesPickerView = UIPickerView()
        tourSelectRulesPickerView?.dataSource = self
        tourSelectRulesPickerView?.delegate = self
        tourSelectRulesFromListTF.inputView = tourSelectRulesPickerView
        tourSelectRulesFromListTF.text = "Select rule from list"
        
        tourSelectRulesOptionPickerView = UIPickerView()
        tourSelectRulesOptionPickerView?.dataSource = self
        tourSelectRulesOptionPickerView?.delegate = self
        optionTextfield.inputView = tourSelectRulesOptionPickerView
        
        let tourSelectRulesOptionToolBar = UIToolbar().ToolbarPiker(mySelect: #selector(tourSelectRulesOptionDismissPicker))
        optionTextfield.inputAccessoryView = tourSelectRulesOptionToolBar
        
        NotificationCenter.default.addObserver(self, selector: #selector(setLocationForTournamentHost), name: NSNotification.Name(rawValue: "setLocationForTournamentHost"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(gotoSelectFromMap), name: NSNotification.Name(rawValue: "gotoSelectFromMap"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(setLocationForTournamentHostFromExistingGround), name: NSNotification.Name(rawValue: "setLocationForTournamentHostFromExistingGround"), object: nil)
        
        let tourSelectRulesToolBar = UIToolbar().ToolbarPiker(mySelect: #selector(tourSelectRulesDismissPicker))
        tourSelectRulesFromListTF.inputAccessoryView = tourSelectRulesToolBar
        
        getRuleList()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        CommonUtil.buttonRoundedCorners(buttons: [tourNameNextButton,tourDateNextButton, tourLocationNextButton,tourTeamsNextButton,tourOversNextButton,tourSetRulesContinueButton, numberOfPlayerNextButton])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tourSetRuleTitleView.roundCornersView([.topLeft,.topRight], radius: 5)
    }
    
    @objc func dissmissViewHolderTapped(){
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openNearByTournament"), object: nil)
        })
    }
    
    @objc func tourCreateImageTapped() {
        print("Image Tapped")
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .popover
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            print("IPAD")
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            alert.popoverPresentationController?.permittedArrowDirections = .down
            
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            self.tourNameView.shake()
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image_data = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageData = UIImageJPEGRepresentation(image_data.updateImageOrientionUpSide()!, 0.025)
            self.dismiss(animated: true, completion: nil)
            self.tourImageUploadImageView.image = image_data
            self.tourImageUploadImageView.layer.cornerRadius = self.tourImageUploadImageView.frame.height/2
            self.tourImageUploadImageView.clipsToBounds = true
        }
    }
    
    //Tournament create name next button
    @IBAction func tourNameCreateNextButtonTapped(_ sender: Any) {
        view.endEditing(true)
        if tourNameTextField.isReallyEmpty {
            self.tourNameView.shake()
            self.showAlert(title: "Missing Info", message: "Please enter a team name")
        } else {
            tournamentName = tourNameTextField.text
            UIView.transition(with: self.tourNameView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tourNameView.isHidden = true
            })
            UIView.transition(with: self.tourWhenView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tourWhenView.isHidden = false
            })
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == tourOverNumberTF {
            let maxLength = 2
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else if textField == numberOfPlayersTextField {
            let maxLength = 2
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else {
            return true
        }
    }
    
    //Keyboard Methods
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant = 0.0
                self.keyboardTourOverHeightView?.constant = 0.0
            } else {
                self.keyboardHeightLayoutConstraint?.constant = -(endFrame?.size.height ?? 0.0) / 2
                self.keyboardTourOverHeightView?.constant = -(endFrame?.size.height ?? 0.0) / 2
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tourEndDateTextField {
            print("Helloooo")
            if tourStartDateTextField.text == "Start Date" {
                textField.resignFirstResponder()
                self.showAlert(title: "Start Date", message: "Please select a start date first")
            }
        }
    }
    
    //date format for from date
    @objc func handleStartDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        tourStartDateTextField.text = dateFormatter.string(from: (from_date_picker?.date)!)
        
        to_date_picker = UIDatePicker()
        to_date_picker?.minimumDate = from_date_picker?.date
        tourEndDateTextField.tintColor = .clear
        to_date_picker?.addTarget(self , action: #selector(handleEndDatePicker), for: UIControlEvents.valueChanged)
        to_date_picker?.datePickerMode = .date
        tourEndDateTextField.inputView = to_date_picker
    }
    
    @objc func handleEndDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        tourEndDateTextField.text = dateFormatter.string(from: (to_date_picker?.date)!)
        from_date_picker?.maximumDate = to_date_picker?.date
    }
    
    //date format for from date
    @objc func handleLastDateEntry() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
    }
    
    @objc func lastDateDismissPicker(){
        view.endEditing(true)
    }
    
    //From date set and get latitude & longitude
    @objc func startDateDismissPicker() {
        view.endEditing(true)
    }
    
    //To date set and get latitude & longitude
    @objc func endDateDismissPicker() {
        view.endEditing(true)
    }
       //Tournament start date and end date next button
    @IBAction func tourStartEndDateNextButton(_ sender: Any) {
        if tourStartDateTextField.text == "Start Date" || tourEndDateTextField.text == "End Date" {
            self.tourWhenView.shake()
            self.showAlert(title: "Invalid info", message: "Please set a start date and end date")
        } else {
            tourStartDate = tourStartDateTextField.text
            tourEndDate = tourEndDateTextField.text
            UIView.transition(with: self.tourWhenView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tourWhenView.isHidden = true
            })
            UIView.transition(with: self.tourLocationView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tourLocationView.isHidden = false
                CommonUtil.viewRoundedCorners(uiviews: [self.tourLocationLabelView])
                self.tourLocationLabelView.backgroundColor = UIColor.init(red: 0.80, green: 0.80, blue: 0.80, alpha: 1.0)
            })
        }
    }
    
    @objc func tourLocationTapped(){
        let groundVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseGroundViewController") as? ChooseGroundViewController
        groundVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        groundVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(groundVC!, animated: true, completion: nil)
    }
    // Tournament location next button
    @IBAction func tourLocationNextButtonTapped(_ sender: Any) {
        print("Tournamet Location clicked")
        if tourLocationLabel.text == "Enter the place" {
            self.tourLocationView.shake()
            self.showAlert(title: "Missing Value", message: "Please Select the tournament location")
        } else {
            UIView.transition(with: self.tourLocationView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tourLocationView.isHidden = true
            })
            UIView.transition(with: self.tourTeamsView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tourTeamsView.isHidden = false
            })
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == tourTeamsPickerView {
            return tourTeamsPickerNumber.count
        }  else if pickerView == tourSelectRulesPickerView {
            return UnSelectedRules.count
        } else if pickerView == tourSelectRulesOptionPickerView {
            return optionList.count
        } else {
            return UnSelectedRules.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case tourTeamsPickerView:
            return tourTeamsPickerNumber[row]
        //break
        case tourSelectRulesPickerView:
            return (UnSelectedRules[row] as! NSDictionary)["RULE"] as? String
            
        case tourSelectRulesOptionPickerView:
            return (optionList[row] as! NSDictionary)["RULE_OPTION"] as? String
        //break
        default:
            return (UnSelectedRules[row] as! NSDictionary)["RULE"] as? String
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if pickerView == tourTeamsPickerView {
            tourTeamsNumberTF.text = tourTeamsPickerNumber[row]
        } else if pickerView == tourSelectRulesOptionPickerView {
            print(optionList[row])
            tempCurrentRuleOption = optionList[row] as? NSDictionary
            UserDefaults.standard.set(tempCurrentRuleOption, forKey: tempCurrentRuleIDOption!)
            UIView.transition(with: self.selectedRulesTableView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.selectedRulesTableView.reloadData()
            })
        } else {
            if UnSelectedRules.count > 0 {
                tempCurrentRuleID = (UnSelectedRules[row] as! NSDictionary)["RULE_ID"] as? String
                tourSelectRulesFromListTF.text = "Select rule from list"
            }
            
        }
    }
    
    @objc func tourSelectRulesOptionDismissPicker() {
        UserDefaults.standard.set(tempCurrentRuleOption, forKey: tempCurrentRuleIDOption!)
        UIView.transition(with: self.selectedRulesTableView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.selectedRulesTableView.reloadData()
        })
        view.endEditing(true)
    }
    
    @objc func tourTeamsDismissPicker(){
        view.endEditing(true)
    }
    //Tournament team select next button
    @IBAction func tourTeamsNextButtonClicked(_ sender: Any) {
        print("team button clicked")
        if tourTeamsNumberTF.isReallyEmpty {
            self.tourTeamsView.shake()
            self.showAlert(title: "Number of Teams", message: "Please select the number of teams?")
        } else {
            tourTeams = tourTeamsNumberTF.text
            UIView.transition(with: self.tourTeamsView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tourTeamsView.isHidden = true
            })
            UIView.transition(with: self.tourOversView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tourOversView.isHidden = false
            })
        }
        
    }
    //Tournament match over next button
    @IBAction func tourOversNextButtonClick(_ sender: Any) {
        view.endEditing(true)
        if tourOverNumberTF.isReallyEmpty {
            self.tourOversView.shake()
            self.showAlert(title: "Missing Info", message: "Please enter Overs")
        } else {
            if Int(tourOverNumberTF.text!)! <= 50 {
                tourOvers = tourOverNumberTF.text
                UIView.transition(with: self.tourOversView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                    self.tourOversView.isHidden = true
                })
                UIView.transition(with: self.numberofPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                    self.numberofPlayersView.isHidden = false
                })
            } else {
                self.showAlert(title: "Alert", message: "Maximum number of overs is 50")
            }
        }
    }
    //Tournament number of player select next button
    @IBAction func tourNumberofPlayerNextButtonClick(_ sender: Any) {
        view.endEditing(true)
        if numberOfPlayersTextField.isReallyEmpty {
            self.numberofPlayersView.shake()
            self.showAlert(title: "Missing Info", message: "Please enter number of players")
        } else {
            if Int(numberOfPlayersTextField.text!)! >= 3 && Int(numberOfPlayersTextField.text!)! <= 16 {
                numberOfPlayers = numberOfPlayersTextField.text
                UIView.transition(with: self.numberofPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                    self.numberofPlayersView.isHidden = true
                })
                UIView.transition(with: self.tourSetRulesView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                    self.tourSetRulesView.isHidden = false
                })
            } else {
                self.showAlert(title: "Alert", message: "Minimum number of players is 3 and maximum number of players is 16")
            }
        }
    }
    
    @objc func tourSelectRulesDismissPicker(){
        if UnSelectedRules.count > 0 {
            
            
            let unselectedRuleTemp = UnSelectedRules
            
            var removable: NSDictionary?
            
            for rule in unselectedRuleTemp {
                if (rule as! NSDictionary)["RULE_ID"] as? String == tempCurrentRuleID {
                    let optionList = (rule as! NSDictionary)["RULE_OPTIONS"] as! NSArray
                    tempCurrentRuleOption = optionList[0] as? NSDictionary
                    tempCurrentRuleIDOption = (rule as! NSDictionary)["RULE_ID"] as? String
                    UserDefaults.standard.set(tempCurrentRuleOption, forKey: tempCurrentRuleIDOption!)
                    selectedRules.add(rule)
                    removable = rule as? NSDictionary
                }
            }
            
            if removable != nil {
                UnSelectedRules.remove(removable!)
            }
            
            UIView.transition(with: self.selectedRulesTableView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.selectedRulesTableView.reloadData()
            })
            tourSelectRulesPickerView?.reloadAllComponents()
            print("selectedRule",selectedRules)
        }
        view.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == selectedRulesTableView {
            return selectedRules.count
        } else {
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == selectedRulesTableView {
            let rule = selectedRules[indexPath.row] as! NSDictionary
            let selectedRulesCell = tableView.dequeueReusableCell(withIdentifier: "selectedRulesFromList", for: indexPath) as! SelectedRulesTableViewCell
            selectedRulesCell.ruleNameLabel.text = rule["RULE"] as? String
            
            if (rule["RULE_OPTIONS"] as! NSArray).count > 0 {
                selectedRulesCell.ruleTypeButton.isHidden = false
                selectedRulesCell.ruleTypeButton.addTarget(self, action: #selector(ruleTypeButtonTapped), for: UIControlEvents.touchUpInside)
                selectedRulesCell.ruleTypeButton.tag = indexPath.row
            } else {
                selectedRulesCell.ruleTypeButton.isHidden = true
            }
            if let option = UserDefaults.standard.object(forKey: rule["RULE_ID"] as! String) as? NSDictionary {
                print("Selected Option", option)
                selectedRulesCell.ruleTypeButton.setTitle(option["RULE_OPTION"] as? String, for: .normal)
            }
            selectedRulesCell.ruleRemoveButton.addTarget(self, action: #selector(ruleRemoveButtonTapped), for: UIControlEvents.touchUpInside)
            selectedRulesCell.ruleRemoveButton.tag = indexPath.row
            return selectedRulesCell
        } else {
            let selectedAwardsCell = tableView.dequeueReusableCell(withIdentifier: "awardsAndPrizeTableViewCell", for: indexPath) as! AwardsPrizesTableViewCell
            return selectedAwardsCell
        }
    }
    
    @IBAction func ruleTypeButtonTapped(_ sender: UIButton) {
        let rule = selectedRules[sender.tag] as! NSDictionary
        tempCurrentRuleIDOption = rule["RULE_ID"] as? String
        optionList = rule["RULE_OPTIONS"] as! NSArray
        print(optionList)
        tempCurrentRuleOption = optionList[0] as? NSDictionary
        UserDefaults.standard.set(tempCurrentRuleOption, forKey: tempCurrentRuleIDOption!)
        tourSelectRulesOptionPickerView?.reloadAllComponents()
        optionTextfield.becomeFirstResponder()
    }
    
    @IBAction func ruleRemoveButtonTapped(_ sender: UIButton) {
        let rul = selectedRules[sender.tag] as! NSDictionary
        
        for rule in selectedRules {
            if rul["RULE_ID"] as? String == (rule as! NSDictionary)["RULE_ID"] as? String {
                UnSelectedRules.add(rule)
                selectedRules.remove(rule)
                break
            }
        }
        tourSelectRulesPickerView?.reloadAllComponents()
        UIView.transition(with: self.selectedRulesTableView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.selectedRulesTableView.reloadData()
        })
    }
    
    @IBAction func tourRuleDropDownButtonClick(_ sender: Any) {
        tourSelectRulesFromListTF.becomeFirstResponder()
    }
    
    // To dismiss Picker for done button
    @objc func tourSelectAwardsDismissPicker() {
        view.endEditing(true)
    }
    
    @objc func setLocationForTournamentHostFromExistingGround() {
        print("Setting location from existing ground")
        let ground = UserDefaults.standard.object(forKey: CommonUtil.TEMPGROUNDSELECTED) as? NSDictionary ?? [:]
        self.tourLocationLabel.text = ground["ADDRESS"] as? String
        self.groundID = ground["GROUND_ID"] as? String
        self.tourLocationLabelView.backgroundColor = UIColor.white
        //        self.tourLocationNextButton.isEnabled = true
    }
    
    @objc func setLocationForTournamentHost() {
        print("Setting location on view")
        getAddressFromLatLon(pdblLatitude: UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double, withLongitude: UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double)
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
        let lat: Double = pdblLatitude
        
        let lon: Double = pdblLongitude
        print(lat,lon)
        CommonUtil.getAddressForLatLng(viewcontroller: self, latitude: String(lat), longitude: String(lon), callback: {
            (address: String) -> Void in
            print("GEOCODING \(address)")
            UIView.transition(with: self.tourLocationLabel, duration: 0.4, options: .curveEaseOut, animations: {
                self.tourLocationLabel.text = address
                self.tourLocationLabelView.backgroundColor = UIColor.white
            }, completion: nil)
            self.activityIndicator.startAnimating()
            
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"ADDRESS\":\"\(self.removeSpecialCharsFromString(text: address))\",\"LATITUDE\":\"\(lat)\",\"LONGITUDE\":\"\(lon)\",\"ITEMS_PER_PAGE\":\"5\",\"USER_ID\":\"\(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String)\",\"GROUND_NAME\":\"\",\"GROUND_DESCRIPTION\":\"\",\"IS_PAID\":\"\"}", mod: "Ground", actionType: "add-ground", callback: { (response) in
                if response as? String != "error" {
                    if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                        print(response)
                        self.groundID = (response as! NSDictionary)["XSCData"] as? String ?? "\((response as! NSDictionary)["XSCData"] as! Int)"
                        self.tourLocationNextButton.isEnabled = true
                    } else {
//                        self.showAlert(title: "Oops", message: (response as! NSDictionary)["XSCMessage"] as! String)
                    }
                } else {
                    self.showAlert(title: "Oops", message: "Something went wrong")
                }
                self.activityIndicator.stopAnimating()
            })
            
        })
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_")
        return String(text.filter {okayChars.contains($0) })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        UserDefaults.standard.set(true, forKey: CommonUtil.GROUNDSELECTEDLOCATION)
    }
    
    @objc func gotoSelectFromMap() {
        self.performSegue(withIdentifier: "createTournament_LocationPicker", sender: self)
    }
    
    @IBAction func tourSelectRulesContinueButton(_ sender: Any) {
        print("select Rules button clicked")
        
        let rules: NSMutableArray = []
        for rule in selectedRules {
            let option = UserDefaults.standard.object(forKey: (rule as! NSDictionary)["RULE_ID"] as! String) as? NSDictionary ?? [:]
            let keyvalue = ["RULE_ID":"\((rule as! NSDictionary)["RULE_ID"] as! String)","MATCH_RULE_OPTION_ID":"\(option["MATCH_RULE_OPTION_ID"] as? String ?? "")"]
            rules.add(keyvalue)
        }
        for rule in rulesReceived {
            UserDefaults.standard.removeObject(forKey: (rule as! NSDictionary)["RULE_ID"] as! String)
        }
        print(rules)
        if selectedRules.count > 0 {
            activityIndicator.startAnimating()
            preventUserInteractionView.isHidden = false
            if imageData != nil {
                print("Image is available")
                myImageUploadRequest(image: imageData!, rule_id: rules)
            } else {
                continueToCreatePin(rule_id: rules)
            }
        } else {
            self.tourSetRulesView.shake()
            self.showAlert(title: "Missing Info", message: "Please select atleast one rule ")
        }
    }
    
    func getRuleList() {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "", mod: "Tournament", actionType: "get-tournament-rule-list") { (response) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    print(response)
                    self.rulesReceived = (response as! NSDictionary)["XSCData"] as! NSArray
                    self.UnSelectedRules = []
                    self.selectedRules = []
                    for rule in self.rulesReceived {
                        self.UnSelectedRules.add(rule)
                    }
                    self.tourSelectRulesPickerView?.reloadAllComponents()
                } else {
                    print((response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                print("Something went wrong! Please try again")
            }
            
        }
    }
    
    func continueToCreatePin(rule_id:NSArray) {
        do {
            let data = try JSONSerialization.data(withJSONObject: rule_id)
            let dataString = String(data: data, encoding: .utf8)!
            print(dataString)
            tourStartDate = CommonUtil.convertDateFormater(tourStartDate!)
            tourEndDate = CommonUtil.convertDateFormater(tourEndDate!)
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"NAME\":\"\(tournamentName!)\",\"NO_OF_TEAMS\":\"\(tourTeams!)\",\"START_DATE\":\"\(tourStartDate!)\",\"END_DATE\":\"\(tourEndDate!)\",\"NO_OF_OVERS\":\"\(tourOvers!)\",\"RULE_ID\":\(dataString),\"LOCATION_CITY\":\"\(groundID!)\",\"PLAYERS_PER_TEAM\":\"\(numberOfPlayers!)\"}", mod: "Tournament", actionType: "create-tournament") { (response) in
                print(response)
                if response as? String != "error" {
                    if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                        self.dissmissViewHolder.isHidden = false
                        self.tourSetRulesView.isHidden = true
                        let tourCreate = (response as! NSDictionary)["XSCData"] as! NSDictionary
                        let tourCreateKey: [String: Any] = ["tourCreate": tourCreate, "maxiEntryDate":self.from_date_picker!.date, "tourTeams": self.tourTeams!]
                        
                        self.dismiss(animated: true, completion: {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "gotoTournamentPaymentDetails"), object: nil, userInfo: tourCreateKey)
                        })
                        //                        let tourCreate = (response as! NSDictionary)["XSCData"] as! NSDictionary
                        //                        let tourPay = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TournamentPaymentDetailsViewController") as? TournamentPaymentDetailsViewController
                        //                        tourPay?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        //                        tourPay?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        //                        if tourCreate["IS_PIN_EXIST"] as! Int == 0 {
                        //                            tourPay?.hasPin = false
                        //                        } else {
                        //                            tourPay?.hasPin = true
                        //                        }
                        //                        tourPay?.tournament_ID = tourCreate["TOURNAMENT_ID"] as? String ?? "\(tourCreate["TOURNAMENT_ID"] as! Int)"
                        //                        tourPay?.maxiEntryDate = self.from_date_picker?.date
                        //                        tourPay?.numberOfTeams = self.tourTeams!
                        //                        self.present(tourPay!, animated: true, completion: nil)
                    } else {
                        self.showAlert(title: "Oops", message: (response as! NSDictionary)["XSCMessage"] as! String)
                    }
                } else {
                    print("Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
                self.preventUserInteractionView.isHidden = true
            }
        } catch {
            print("JSON serialization failed: ", error)
        }
    }
    
    func myImageUploadRequest(image: Data, rule_id:NSArray)
    {
        let myUrl = NSURL(string: CommonUtil.BASE_URL);
        //let myUrl = NSURL(string: "http://www.boredwear.com/utils/postImage.php");
        var param: [String: Any]?
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        do {
            tourStartDate = CommonUtil.convertDateFormater(tourStartDate!)
            tourEndDate = CommonUtil.convertDateFormater(tourEndDate!)
            let data = try JSONSerialization.data(withJSONObject: rule_id)
            let dataString = String(data: data, encoding: .utf8)!
            print(dataString)
            
            param = [
                "app" : "CRICDOST",
                "mod": "Tournament",
                "actionType" : "create-tournament",
                "subAction": "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"NAME\":\"\(tournamentName!)\",\"NO_OF_TEAMS\":\"\(tourTeams!)\",\"START_DATE\":\"\(tourStartDate!)\",\"END_DATE\":\"\(tourEndDate!)\",\"NO_OF_OVERS\":\"\(tourOvers!)\",\"RULE_ID\":\(dataString),\"LOCATION_CITY\":\"\(groundID!)\",\"PLAYERS_PER_TEAM\":\"\(numberOfPlayers!)\"}"
                ] as [String : Any]
            
        } catch {
            print("JSON serialization failed: ", error)
        }
        print(param)
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        if(imageData==nil)  { return; }
        
        request.httpBody = createBodyWithParameters(parameters: param , filePathKey: "IMAGE", imageDataKey: image as NSData, boundary: boundary) as Data
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                self.showAlert(title: "Error", message: "Request TimeOut")
                print("error=\(String(describing: error))")
                return
            }
            
            // You can print out response object
            print("******* response = \(String(describing: response))")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                print(json as Any)
                DispatchQueue.main.async {
                    if json!["XSCStatus"] as! Int == 0 {
                        self.tourSetRulesView.isHidden = true
                        
                        let tourCreate = json!["XSCData"] as! NSDictionary
                        let tourCreateKey: [String: Any] = ["tourCreate": tourCreate, "maxiEntryDate":self.from_date_picker!.date, "tourTeams": self.tourTeams!]
                        
                        self.dismiss(animated: true, completion: {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "gotoTournamentPaymentDetails"), object: nil, userInfo: tourCreateKey)
                        })
                        //                        let tourCreate = json!["XSCData"] as! NSDictionary
                        //                        let tourPay = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TournamentPaymentDetailsViewController") as? TournamentPaymentDetailsViewController
                        //                        tourPay?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        //                        tourPay?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        //                        if tourCreate["IS_PIN_EXIST"] as! Int == 0 {
                        //                            tourPay?.hasPin = false
                        //                        } else {
                        //                            tourPay?.hasPin = true
                        //                        }
                        //                        tourPay?.tournament_ID = tourCreate["TOURNAMENT_ID"] as? String ?? "\(tourCreate["TOURNAMENT_ID"] as! Int)"
                        //                        tourPay?.maxiEntryDate = self.from_date_picker?.date
                        //                        self.present(tourPay!, animated: true, completion: nil)
                    } else {
                        self.showAlert(title: "Oops", message: json!["XSCMessage"] as! String)
                    }
                }
            }catch
            {
                print(error)
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteractionView.isHidden = true
        }
        
        task.resume()
    }
    
    func createBodyWithParameters(parameters: [String: Any]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    @IBAction func tourNameBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openNearByTournament"), object: nil)
        })
    }
    @IBAction func tourWhenBackButton(_ sender: Any) {
        //        tourNameTextField.text = ""
        tourStartDateTextField.text = "Start Date"
        tourEndDateTextField.text = "End Date"
        self.from_date_picker?.minimumDate = Date()
        self.to_date_picker?.minimumDate = Date()
        self.from_date_picker?.maximumDate = nil
        
        self.from_date_picker?.setDate(Date(), animated: true)
        self.to_date_picker?.setDate(Date(), animated: true)
        UIView.transition(with: self.tourNameView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tourNameView.isHidden = false
        })
        UIView.transition(with: self.tourWhenView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tourWhenView.isHidden = true
        })
    }
    @IBAction func tourLocationBackButton(_ sender: Any) {
        //        tourStartDateTextField.text = "Start Date"
        //        tourEndDateTextField.text = "End Date"
        tourLocationLabel.text = "Enter the place"
        self.groundID?.removeAll()
        
        UIView.transition(with: self.tourWhenView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tourWhenView.isHidden = false
        })
        UIView.transition(with: self.tourLocationView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tourLocationView.isHidden = true
        })
    }
    @IBAction func tourTeamBackButton(_ sender: Any) {
        //        tourLocationLabel.text = "Enter the place"
        self.groundID?.removeAll()
        tourTeamsNumberTF.text = ""
        UIView.transition(with: self.tourLocationView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tourLocationView.isHidden = false
        })
        UIView.transition(with: self.tourTeamsView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tourTeamsView.isHidden = true
        })
    }
    @IBAction func tourOverBackButton(_ sender: Any) {
        tourOverNumberTF.text = ""
        //        tourTeamsNumberTF.text = "4"
        UIView.transition(with: self.tourTeamsView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tourTeamsView.isHidden = false
        })
        UIView.transition(with: self.tourOversView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tourOversView.isHidden = true
        })
    }
    
    @IBAction func tourPlayersBackButton(_ sender: Any) {
        //        tourOverNumberTF.text = ""
        numberOfPlayersTextField.text = ""
        
        UIView.transition(with: self.tourOversView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tourOversView.isHidden = false
        })
        UIView.transition(with: self.numberofPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.numberofPlayersView.isHidden = true
        })
        
    }
    
    @IBAction func tourSetRulesBackButton(_ sender: Any) {
        //         numberOfPlayersTextField.text = ""
        self.UnSelectedRules.removeAllObjects()
        self.selectedRules.removeAllObjects()
        for rule in self.rulesReceived {
            self.UnSelectedRules.add(rule)
        }
        self.optionList = []
        self.selectedRulesTableView.reloadData()
        self.tourSelectRulesPickerView?.reloadAllComponents()
        UIView.transition(with: self.numberofPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.numberofPlayersView.isHidden = false
        })
        UIView.transition(with: self.tourSetRulesView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tourSetRulesView.isHidden = true
        })
        
    }
    
}
