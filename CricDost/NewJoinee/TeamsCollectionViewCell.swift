//
//  TeamsCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 5/28/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class TeamsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var teamImage: UIImageView!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var teamDistance: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        self.layer.cornerRadius = 3.0
//        layer.shadowRadius = 2
//        layer.shadowOpacity = 0.3
//        layer.shadowOffset = CGSize(width: 2, height: 2)
//        self.clipsToBounds = false
    }
}
