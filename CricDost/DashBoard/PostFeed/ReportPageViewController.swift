//
//  ReportPageViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 10/24/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ReportPageViewController: UIViewController, ShowsAlert {

    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var reportTheUserView: UIView!
    @IBOutlet weak var inappropriateMessages: RadioButton!
    @IBOutlet weak var inappropriatePhotos: RadioButton!
    @IBOutlet weak var badBehavior: RadioButton!
    @IBOutlet weak var feelsLikeSpam: RadioButton!
    @IBOutlet weak var others: RadioButton!
    @IBOutlet weak var othersTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    
    var report: String?
    var postID: String?
    var commentID: String?
    var reportCommentFlag = false
    var additionalInfo: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        CommonUtil.roundedUIViewCornersWithShade(uiviews: [reportTheUserView])
        
        
        let dismissViewGesture = UITapGestureRecognizer(target: self, action: #selector(dismissViewTapped))
        dismissView.addGestureRecognizer(dismissViewGesture)
        
        print("report post id" ,postID!)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        submitButton.roundCornersButton([.bottomLeft,.bottomRight], radius: 5)
    }
    
    @objc func dismissViewTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitButtonClick(_ sender: Any) {
        if report != nil {
            if report == "OTHER" && othersTextField.isReallyEmpty {
                self.reportTheUserView.shake()
                self.showAlert(title: "Invalid info", message: "Please enter additional info")
            } else {
                if reportCommentFlag {
                    reportComment()
                } else {
                    reportPost()
                }
            }
        } else {
            self.reportTheUserView.shake()
            self.showAlert(title: "Invalid info", message: "Please select a report type")
        }
    }
    
    @IBAction func inappropriateMessageButtonClick(_ sender: Any) {
        othersTextField.resignFirstResponder()
        othersTextField.text = ""
        othersTextField.isEnabled = false
        if (sender as! RadioButton) == inappropriateMessages {
            report = "MESSAGE"
            setRadioButtonSelected(radio: inappropriateMessages)
            setRadioButtonDeSelected(radioButtons: [inappropriatePhotos,badBehavior,feelsLikeSpam,others])
        }
    }
    
    @IBAction func inappropriatePhotosButtonClick(_ sender: Any) {
        othersTextField.resignFirstResponder()
        othersTextField.text = ""
        othersTextField.isEnabled = false
        if (sender as! RadioButton) == inappropriatePhotos {
            report = "PHOTO"
            setRadioButtonSelected(radio: inappropriatePhotos)
            setRadioButtonDeSelected(radioButtons: [inappropriateMessages,badBehavior,feelsLikeSpam,others])
        }
    }
    
    @IBAction func badBehaviorButtonClick(_ sender: Any) {
        othersTextField.resignFirstResponder()
        othersTextField.text = ""
        othersTextField.isEnabled = false
        if (sender as! RadioButton) == badBehavior {
            report = "BEHAVIOUR"
            setRadioButtonSelected(radio: badBehavior)
            setRadioButtonDeSelected(radioButtons: [inappropriatePhotos,inappropriateMessages,feelsLikeSpam,others])
        }
    }
    
    @IBAction func feelsLikeSpamButtonClick(_ sender: Any) {
        othersTextField.resignFirstResponder()
        othersTextField.text = ""
        othersTextField.isEnabled = false
        if (sender as! RadioButton) == feelsLikeSpam {
            report = "SPAM"
            setRadioButtonSelected(radio: feelsLikeSpam)
            setRadioButtonDeSelected(radioButtons: [inappropriatePhotos,inappropriateMessages,badBehavior,others])
        }
    }
    
    @IBAction func othersButtonClick(_ sender: Any) {
        othersTextField.becomeFirstResponder()
        othersTextField.isEnabled = true
        if (sender as! RadioButton) == others {
            report = "OTHER"
            setRadioButtonSelected(radio: others)
            setRadioButtonDeSelected(radioButtons: [inappropriatePhotos,inappropriateMessages,badBehavior,feelsLikeSpam])
        }
    }
    
    func setRadioButtonSelected(radio: RadioButton) {
        radio.isSelected = true
        if radio.isSelected {
            radio.outerCircleColor = UIColor.init(red: 0.55, green: 0.66, blue: 0.25, alpha: 1.0)
            radio.innerCircleCircleColor = UIColor.init(red: 0.55, green: 0.66, blue: 0.25, alpha: 1.0)
        } else {
            radio.outerCircleColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.5)
        }
    }
    
    func setRadioButtonDeSelected(radioButtons: [RadioButton]) {
        for radio in radioButtons {
            radio.isSelected = false
            if radio.isSelected {
                radio.outerCircleColor = UIColor.init(red: 0.55, green: 0.66, blue: 0.25, alpha: 1.0)
                radio.innerCircleCircleColor = UIColor.init(red: 0.55, green: 0.66, blue: 0.25, alpha: 1.0)
            } else {
                radio.outerCircleColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.5)
            }
        }
    }
    
    func reportPost() {
        if report == "OTHER" {
            additionalInfo = othersTextField.text!
        }
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"REPORT_STATUS\":\"\(report!)\",\"POST_ID\":\"\(postID!)\",\"ADDL_INFO\":\"\(additionalInfo)\"}", mod: "CricSpace", actionType: "report-post") { (response) in
            print("report post",response )
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    
                    let alert = UIAlertController(title: "Message", message: (response as! NSDictionary)["XSCMessage"] as! String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                        self.dismiss(animated: true, completion: {
                            let post:[String: Any] = ["post":self.postID!]
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeReportedPost"), object: nil, userInfo: post)
                        })
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    self.showAlert(title: "Report", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops", message: "Something went wrong! Please try again")
            }
        }

    }
    
    func reportComment() {
        
        if report == "OTHER" {
            additionalInfo = othersTextField.text!
        }
        
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"POST_ID\":\"\(postID!)\",\"COMMENT_ID\":\"\(commentID!)\",\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"REPORT_STATUS\":\"\(report!)\",\"ADDL_INFO\":\"\(additionalInfo)\"}", mod: "CricSpace", actionType: "report-post-comment") { (response) in
            print("report comment" , response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    let alert = UIAlertController(title: "Message", message: (response as! NSDictionary)["XSCMessage"] as! String, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                        self.dismiss(animated: true, completion: {
                            let post:[String: Any] = ["post":self.postID!]
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeReportedComment"), object: nil, userInfo: post)
                            
                            let comment:[String: Any] = ["commentRemove":self.commentID!]
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reportCommentRefreshData"), object: nil, userInfo: comment)
                        })
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    self.showAlert(title: "Report", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops", message: "Something went wrong! Please try again")
            }
        }
    }
    
}
