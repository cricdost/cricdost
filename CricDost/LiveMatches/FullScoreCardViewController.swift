//
//  FullScoreCardViewController.swift
//  CricDost
//
//  Created by Jit Goel on 6/27/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class FullScoreCardViewController: ButtonBarPagerTabStripViewController, ShowsAlert {
    
    let purpleInspireColor = UIColor(red:0, green:150/255, blue:136/255, alpha:1.0)
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var barButton: ButtonBarView!
    var MATCH_ID: String?
    var teamAinfo:[String: Any]?
    var teamBinfo:[String: Any]?
    @IBOutlet weak var refreshButton: UIBarButtonItem!
    
    var matchTemp: NSDictionary?
    var teamATemp: NSDictionary?
    var teamBTemp: NSDictionary?
    
    var isCalledFromInternational = false
    
    override func viewDidLoad() {
        
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = purpleInspireColor
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 13)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0

        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .black
            newCell?.label.textColor = CommonUtil.themeRed
            
            if (self?.isCalledFromInternational)! {
                if self?.teamAinfo != nil {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshTeamAScore1"), object: nil, userInfo: self?.teamAinfo)
                }
                if self?.teamBinfo != nil {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshTeamBScore1"), object: nil, userInfo: self?.teamBinfo)
                }
            } else {
                if self?.teamAinfo != nil {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshTeamAScore"), object: nil, userInfo: self?.teamAinfo)
                }
                if self?.teamBinfo != nil {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshTeamBScore"), object: nil, userInfo: self?.teamBinfo)
                }
            }
        }
        super.viewDidLoad()
        MATCH_ID = UserDefaults.standard.object(forKey: CommonUtil.CURRENTSCOREMATCHID) as? String
        let TitleLabel = UILabel()
        TitleLabel.text = "FULL SCORE CARD"
        TitleLabel.font = UIFont.boldSystemFont(ofSize: 17)
        TitleLabel.textColor = UIColor.white
        TitleLabel.sizeToFit()
        let leftItem = UIBarButtonItem(customView: TitleLabel)
        self.navigationItem.leftBarButtonItem = leftItem
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshScore), name: NSNotification.Name(rawValue: "refreshScore"), object: nil)
        
        refreshScore()
        if isCalledFromInternational {
            refreshButton.tintColor = UIColor.clear
            refreshButton.isEnabled = !isCalledFromInternational
        }
    }
    
    
    @objc func refreshScore() {
        if isCalledFromInternational {
            print("Is called from international")
            self.teamAinfo = ["teamAinfo": teamATemp! ]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshTeamAScore1"), object: nil, userInfo: self.teamAinfo)
            
            self.teamBinfo = ["teamBinfo": teamBTemp! ]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshTeamBScore1"), object: nil, userInfo: self.teamBinfo)
        } else {
            activityIndicator.startAnimating()
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\":\"\(MATCH_ID!)\"}", mod: "MatchScore", actionType: "full-scorecard") { (response) in
                if response as? String != "error" {
                    let matchinfo = (response as! NSDictionary)["XSCData"] as! NSDictionary
                    let teamA = (matchinfo["team"] as! NSDictionary)["teamA"] as! NSDictionary
                    let teamB = (matchinfo["team"] as! NSDictionary)["teamB"] as! NSDictionary
                    
                    self.teamAinfo = ["teamAinfo": teamA ]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshTeamAScore"), object: nil, userInfo: self.teamAinfo)
                    
                    self.teamBinfo = ["teamBinfo": teamB ]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshTeamBScore"), object: nil, userInfo: self.teamBinfo)
                    
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let teamA = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TeamAScoreCardViewController") as! TeamAScoreCardViewController
        let teamB = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TeamBScoreCardViewController") as! TeamBScoreCardViewController
        return [teamA, teamB]
    }
    
    @IBAction func closeButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func refreshButtonClick(_ sender: Any) {
        self.refreshScore()
    }
    
}
