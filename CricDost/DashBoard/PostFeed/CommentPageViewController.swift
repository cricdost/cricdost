//
//  CommentPageViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 10/16/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import SDWebImage
class CommentPageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, ShowsAlert {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var commentProfileImageView: UIImageView!
    @IBOutlet weak var commentNameLabel: UILabel!
    @IBOutlet weak var commentAddressTimeLabel: UILabel!
    @IBOutlet weak var commentPostImageView: UIImageView!
    @IBOutlet weak var postImageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var commentPostLabel: UILabel!
    @IBOutlet weak var commentListTableView: UITableView!
    @IBOutlet weak var commentTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var writeCommentTextView: UITextView!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var sendCommentButton: UIButton!
    @IBOutlet weak var viewPreviousCommentsLabel: UILabel!
    var rightButton  : UIButton?
    var postID: String?
    var post: NSDictionary?

    var myComments: NSArray = []
    var postDetails: NSDictionary?
    var commentList: String?
    var mycommentMutable: NSMutableArray = []
    var nextPage = 1
    var isfetchingComment = true
    var tempCommentID = "0"
    var commentEdit = false
    var commentToEdit: NSDictionary?
    
    @IBOutlet weak var sixButton: UIButton!
    @IBOutlet weak var sixLabel: UILabel!
    @IBOutlet weak var fourButton: UIButton!
    @IBOutlet weak var fourLabel: UILabel!
    @IBOutlet weak var outButton: UIButton!
    @IBOutlet weak var outLabel: UILabel!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    
    var editPost = false
    var tempPostID = "0"
    var offSet = CGPoint.zero
    var newContentHeight: CGFloat = 0
//    var commentPage = false
    
    
    var aspectConstraint: NSLayoutConstraint? {
        
        willSet {
            
            if((aspectConstraint) != nil) {
                self.commentPostImageView.removeConstraint(self.aspectConstraint!)
            }
        }
        
        didSet {
            
            if(aspectConstraint != nil) {
                self.commentPostImageView.addConstraint(self.aspectConstraint!)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        CommonUtil.imageRoundedCorners(imageviews: [commentProfileImageView])
        CommonUtil.buttonRoundedCorners(buttons: [sendCommentButton])
        
        writeCommentTextView.text = "Write a comment"
        writeCommentTextView.textColor = UIColor.lightGray
        writeCommentTextView.layer.cornerRadius = 5.0
        
        self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "baseline_arrow_back_white_24pt_1x.png")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "baseline_arrow_back_white_24pt_1x.png")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        
        rightButton?.setBackgroundImage(#imageLiteral(resourceName: "baseline_more_vert_white_24pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        rightButton?.tintColor = UIColor.white
        rightButton?.addTarget(self, action: #selector(moreOption), for: .touchUpInside)
        let rightBarButtomItem = UIBarButtonItem(customView: rightButton!)
        navigationItem.rightBarButtonItem = rightBarButtomItem
        
        print("POST ID", postID!)
        print("Postsss" ,post!)
//        let comments = post
        
        

        self.commentListTableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.commentListTableView.rowHeight = UITableViewAutomaticDimension
        self.commentListTableView.estimatedSectionHeaderHeight = 100
        self.commentListTableView.estimatedRowHeight = 100
        
        [commentProfileImageView,commentNameLabel,commentPostImageView,commentAddressTimeLabel,commentPostLabel].forEach { (object) in
                    object?.showAnimatedGradientSkeleton()
                }
        getMyComments(pageNo: nextPage)
        getPostDetails()
        initializeListeners()
        
        commentListTableView.transform = CGAffineTransform(rotationAngle: -(.pi))
        
        
        let viewPreviousComment = UITapGestureRecognizer(target: self, action: #selector(viewPreviousCommentTapped))
        viewPreviousCommentsLabel.isUserInteractionEnabled = true
        viewPreviousCommentsLabel.addGestureRecognizer(viewPreviousComment)
        
        let postImageGesture = UITapGestureRecognizer(target: self, action: #selector(postImageTappedFuction))
        commentPostImageView.isUserInteractionEnabled = true
        commentPostImageView.addGestureRecognizer(postImageGesture)
        
    }

    override func viewDidAppear(_ animated: Bool) {
        if commentToEdit != [:] {
            commentEdit = true
            tempCommentID = commentToEdit!["COMMENT_ID"] as? String ?? "\(commentToEdit!["COMMENT_ID"] as! Int)"
            writeCommentTextView.text = commentToEdit!["COMMENT"] as? String
            writeCommentTextView.textColor = UIColor.black
        }
    }
    
    func initializeListeners() {
        NotificationCenter.default.addObserver(self, selector: #selector(editPostRefreshData), name: NSNotification.Name(rawValue: "editPostRefreshData"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reportCommentRefreshData), name: NSNotification.Name(rawValue: "reportCommentRefreshData"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(blockedUserForComments), name: NSNotification.Name(rawValue: "blockedUserForComments"), object: nil)
    }
    
    @objc func viewPreviousCommentTapped() {
        if !isfetchingComment {
//            offSet = CGPoint(x: 0, y: self.scrollView.contentOffset.y)
            isfetchingComment = true
            getMyComments(pageNo: nextPage)
        }
    }
    
    @objc func postImageTappedFuction(gesture: UITapGestureRecognizer) {
//        let postImage = postDetails?[gesture.view!.tag] as! NSDictionary
        if postDetails?["POST_IMAGE_URL"] as! String != "" {
            let imageVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewPostImageViewController") as? ViewPostImageViewController
            imageVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            imageVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            imageVC?.imageUrl = postDetails?["POST_IMAGE_URL"] as? String
            self.present(imageVC!, animated: true, completion: nil)
        }
    }
    
    @objc func editPostRefreshData(_ notification: NSNotification) {
        let postid =  notification.userInfo![AnyHashable("commentPageEdit")] as! String
//        editPostDetail(postID: postid)
        let post:[String: Any] = ["post":postid]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "editPostDetailRefreshData"), object: nil, userInfo: post)
        getPostDetails()
    }
    
    @objc func reportCommentRefreshData(_ notification: NSNotification) {
        let commentID =  notification.userInfo![AnyHashable("commentRemove")] as! String
        getPostDetails()
        removeReportFunction(commentID: commentID)
    }
    
    func removeReportFunction(commentID: String) {
        let commentList = self.mycommentMutable
        var x = 0
        repeat {
            let comment = commentList[x]
            if (comment as! NSDictionary)["COMMENT_ID"] as? String ?? "\((comment as! NSDictionary)["COMMENT_ID"] as! Int)" == commentID {
                self.mycommentMutable.remove(comment)
            }
            x = x + 1
        } while(x < commentList.count)
        self.reloadAndResizeTable()
    }
    
    @objc func blockedUserForComments(_ notification: NSNotification) {
        let userid =  notification.userInfo![AnyHashable("userid")] as! String
        self.showAlert(title: "Block", message: "User has been blocked, you wont see any other posts from this user and also this user won't be able to contact you or see your posts")
        blockedUserFunctionComments(UserID: userid)
    }
    
    func blockedUserFunctionComments(UserID: String) {
        print("user id" , UserID)
        
    }
    
    @objc func moreOption() {
        print("more option tapped")
        let alert = UIAlertController(title: "Choose Action", message: nil, preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .popover
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        if postDetails?["USER_ID"] as? String ?? "\(postDetails?["USER_ID"] as! Int)" == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as? String  {
            
            alert.addAction(UIAlertAction(title: "Edit Post", style: .default, handler: { _ in
                self.editPost = true
                self.performSegue(withIdentifier: "commentPage_postnow", sender: self)
            }))
            
            alert.addAction(UIAlertAction(title: "Delete Post", style: .default, handler: { _ in
                self.deletePost()
            }))
            
        } else {
            
            alert.addAction(UIAlertAction(title: "Report Post", style: .default, handler: { _ in
                self.reportPost()
            }))
            alert.addAction(UIAlertAction(title: "Block User", style: .default, handler: { _ in
                let alertController = UIAlertController(title: "Block User", message: "Are you sure you want to block this user?", preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title: "Block", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    self.navigationController?.popToRootViewController(animated: true)
                    self.blockPost(USER_ID: self.postDetails?["USER_ID"] as? String ?? "\(self.postDetails?["USER_ID"] as! Int)", postID: self.postID!)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                }
                // Add the actions
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
            }))
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func editPostDetail(postID: String) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"POST_ID\":\"\(postID)\",\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "CricSpace", actionType: "get-post-details") { (response) in
            print("post details" ,response)
            
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    let postDetails = (response as! NSDictionary)["XSCData"] as! NSDictionary
                    let postList = self.mycommentMutable
                    var x = 0
                    repeat {
                        let post = postList[x]
                        if (post as! NSDictionary)["POST_ID"] as? String ?? "\((post as! NSDictionary)["POST_ID"] as! Int)" == postID {
                            self.mycommentMutable.remove(post)
                            self.mycommentMutable.insert(postDetails, at: x)
                        }
                        x = x + 1
                    } while(x < postList.count)
                    self.reloadAndResizeTable()
                } else {
                    print((response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                print("Something went wrong! Please try again")
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "commentPage_postnow" {
            let postVC = segue.destination as! PostNowViewController
            postVC.openCam = false
            postVC.commentPage = true
            postVC.editPost = editPost
            postVC.postID = postID!
            print("KKKK",self.postID!)
        }
    }
    
    func deletePost() {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"POST_ID\":\"\(postID!)\",\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "CricSpace", actionType: "delete-post") { (response) in
            print("delete post" ,response)
            
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.getPostDetails()
                    let post:[String: Any] = ["post":self.postID!]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deletePostFromCommentPage"), object: nil, userInfo: post)
                    self.navigationController?.popToRootViewController(animated: true)
                } else {
                    self.showAlert(title: "comment", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops", message: "Something went wrong! Please try again")
            }
            
        }
    }
    
    func reportPost() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let reportVC = storyBoard.instantiateViewController(withIdentifier: "ReportPageViewController") as! ReportPageViewController
        reportVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        reportVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        reportVC.postID = postID
        reportVC.commentID = tempCommentID
        self.present(reportVC, animated: true, completion: nil)
    }
    
    func blockPost(USER_ID: String, postID: String) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"IS_BLOCK\":\"1\",\"REFERENCE_ID\":\"\(USER_ID)\"}", mod: "Connections", actionType: "update-blocking") { (response) in
            print("block post",response )
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.navigationController?.popToRootViewController(animated: true)
                    let userid:[String: Any] = ["userid":USER_ID]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "blockedUserFunctionFromCommentPage"), object: nil, userInfo: userid)
                } else {
                    self.showAlert(title: "Block", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops", message: "Something went wrong! Please try again")
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.commentTableViewHeight?.constant = self.commentListTableView.contentSize.height
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let maxHeight: CGFloat = 100.0
        let minHeight: CGFloat = 30.0
        textViewHeight.constant = min(maxHeight, max(minHeight, textView.contentSize.height))
        textView.isScrollEnabled = true
        self.view.layoutIfNeeded()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write a comment"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return mycommentMutable.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let comment = mycommentMutable[section] as! NSDictionary
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell") as! CommentTableViewCell
        cell.profileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(comment["USER_IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "wellplayed"))
        cell.nameLabel.text = (comment["COMMENTED_BY"] as? String)?.capitalized
//        cell.addressTimeLabel.text = "Chennai \u{2022} \(comment["COMMENTED_ON"] as! String)"
        if let _ = comment["ADDRESS"] as? String {
        let addressList = (comment["ADDRESS"] as! String).split(separator: ",")
        if addressList.count > 3 && addressList.count < 9 {
            cell.addressTimeLabel.text = "\(addressList[addressList.endIndex - 4].trimmingCharacters(in: .whitespaces)) \u{2022} \(comment["COMMENTED_ON"] as! String)"
        } else {
            cell.addressTimeLabel.text = "\(addressList[addressList.endIndex - 2].trimmingCharacters(in: .whitespaces)) \u{2022} \(comment["COMMENTED_ON"] as! String)"
        }
        }
        cell.commentLabel.text = comment["COMMENT"] as? String
        cell.optionMenuButton.addTarget(self, action: #selector(commentOptionButtonTapped), for: UIControlEvents.touchUpInside)
        cell.optionMenuButton.tag = section
        
        cell.contentView.transform = CGAffineTransform(rotationAngle: .pi)
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as! CommentTableViewCell
        
        return cell
    }
    
    @IBAction func commentOptionButtonTapped(_ sender: UIButton) {
        let comments = mycommentMutable[sender.tag] as! NSDictionary
        print("comment option button click")
        let alert = UIAlertController(title: "Choose Action", message: nil, preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .popover
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        if comments["USER_ID"] as? String ?? "\(comments["USER_ID"] as! Int)" == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as? String  {
            alert.addAction(UIAlertAction(title: "Edit comment", style: .default, handler: { _ in
                self.editComment(commentID: comments["COMMENT_ID"] as? String ?? "\(comments["COMMENT_ID"] as! Int)", comment: (comments["COMMENT"] as? String)!)
                self.tempCommentID = comments["COMMENT_ID"] as? String ?? "\(comments["COMMENT_ID"] as! Int)"
            }))
            alert.addAction(UIAlertAction(title: "Delete comment", style: .default, handler: { _ in
                self.deleteComment(commentID: comments["COMMENT_ID"] as? String ?? "\(comments["COMMENT_ID"] as! Int)" )
            }))
        } else {
            alert.addAction(UIAlertAction(title: "Report comment", style: .default, handler: { _ in
                self.reportComment(commentID: comments["COMMENT_ID"] as? String ?? "\(comments["COMMENT_ID"] as! Int)", postID: self.postID!)
            }))
            alert.addAction(UIAlertAction(title: "Block User", style: .default, handler: { _ in
                let alertController = UIAlertController(title: "Block User", message: "Are you sure you want to block this user?", preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title: "Block", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    print("user id====",comments["USER_ID"] as? String ?? "\(comments["USER_ID"] as! Int)")
                    print("comment id===",comments["COMMENT_ID"] as? String ?? "\(comments["COMMENT_ID"] as! Int)")
                    self.blockComment(USER_ID: comments["USER_ID"] as? String ?? "\(comments["USER_ID"] as! Int)", commentID: comments["COMMENT_ID"] as? String ?? "\(comments["COMMENT_ID"] as! Int)")
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                }
                // Add the actions
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
            }))
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func editComment(commentID: String , comment: String) {
        print("edit comments")
        self.writeCommentTextView.text = comment
        self.writeCommentTextView.textColor = UIColor.black
        commentEdit = true
    }
    
    func deleteComment(commentID: String) {
        
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"POST_ID\":\"\(postID!)\",\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"COMMENT_ID\":\"\(commentID)\"}", mod: "CricSpace", actionType: "delete-comment") { (response) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    let post:[String: Any] = ["post":self.postID!]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "editPostDetailRefreshData"), object: nil, userInfo: post)
                    let postList = self.mycommentMutable
                    var x = 0
                    repeat {
                        let post = postList[x]
                        if (post as! NSDictionary)["COMMENT_ID"] as? String ?? "\((post as! NSDictionary)["COMMENT_ID"] as! Int)" == commentID {
                            self.mycommentMutable.remove(post)
                        }
                        x = x + 1
                    } while(x < postList.count)
                    self.reloadAndResizeTable()
                    self.writeCommentTextView.text = "Write a comment"
                    self.writeCommentTextView.textColor = UIColor.lightGray
                    self.writeCommentTextView.resignFirstResponder()
                    let maxHeight: CGFloat = 100.0
                    let minHeight: CGFloat = 30.0
                    self.textViewHeight.constant = min(maxHeight, max(minHeight, self.writeCommentTextView.contentSize.height))
                    
                    self.getMyComments(pageNo: 1)
                } else {
                    self.showAlert(title: "comment", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops", message: "Something went wrong! Please try again")
            }
        }
    }
    
    func reportComment(commentID: String, postID: String) {

        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let reportVC = storyBoard.instantiateViewController(withIdentifier: "ReportPageViewController") as! ReportPageViewController
        reportVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        reportVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        reportVC.postID = postID
        reportVC.commentID = commentID
        reportVC.reportCommentFlag = true
        self.present(reportVC, animated: true, completion: nil)
        
    }
    func blockComment(USER_ID: String, commentID: String) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"IS_BLOCK\":\"1\",\"REFERENCE_ID\":\"\(USER_ID)\"}", mod: "Connections", actionType: "update-blocking") { (response) in
            print("block comment and users",response )
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.mycommentMutable.removeAllObjects()
                    if USER_ID == self.postDetails?["USER_ID"] as? String ?? "\(self.postDetails?["USER_ID"] as! Int)" {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                    let userid:[String: Any] = ["userid":USER_ID]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "blockedUserFunctionFromCommentPage"), object: nil, userInfo: userid)
                    self.getMyComments(pageNo: 1)
                } else {
                    self.showAlert(title: "Block", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops", message: "Something went wrong! Please try again")
            }
        }
    }
    
    @IBAction func commentSendButtonClick(_ sender: Any) {
        
        if writeCommentTextView.text.trimmingCharacters(in: .whitespacesAndNewlines) != "" && writeCommentTextView.text != "Write a comment" {
            if commentEdit {
                print("tempcommentid" , tempCommentID)
                editCommentSubmit(commentId: tempCommentID)
            } else {
                newContentHeight = self.writeCommentTextView.contentSize.height
                commentSubmit()
            }
        }
        
        writeCommentTextView.text = "Write a comment"
        writeCommentTextView.textColor = UIColor.lightGray
        writeCommentTextView.resignFirstResponder()
        let maxHeight: CGFloat = 100.0
        let minHeight: CGFloat = 30.0
        textViewHeight.constant = min(maxHeight, max(minHeight, writeCommentTextView.contentSize.height))
        
        writeCommentTextView.resignFirstResponder()
    }
    
    @IBAction func sixButtonClick(_ sender: UIButton) {
        if postDetails!["USER_STATUS"] as? String ?? "liked post" != "" {
            let myLikes = postDetails!["USER_STATUS"] as! NSDictionary
            if myLikes["IS_SIX"] as! String == "true" {
                addLikes(postID: postID!, six: 0, four: 0, out: 0)
            } else {
                addLikes(postID: postID!, six: 1, four: 0, out: 0)
            }
        } else {
            addLikes(postID: postID!, six: 1, four: 0, out: 0)
        }
    }
    
    @IBAction func fourButtonClick(_ sender: UIButton) {
        print("add like", postDetails!)
        if postDetails!["USER_STATUS"] as? String ?? "liked post" != "" {
            let myLikes = postDetails!["USER_STATUS"] as! NSDictionary
            if myLikes["IS_FOUR"] as! String == "true" {
                addLikes(postID: postID!, six: 0, four: 0, out: 0)
            } else {
                addLikes(postID: postID!, six: 0, four: 1, out: 0)
            }
        } else {
            addLikes(postID: postID!, six: 0, four: 1, out: 0)
        }
    }
    
    @IBAction func outButtonClick(_ sender: UIButton) {
        if postDetails!["USER_STATUS"] as? String ?? "liked post" != "" {
            let myLikes = postDetails!["USER_STATUS"] as! NSDictionary
            if myLikes["IS_WICKET"] as! String == "true" {
                addLikes(postID: postID!, six: 0, four: 0, out: 0)
            } else {
                addLikes(postID: postID!, six: 0, four: 0, out: 1)
            }
        } else {
            addLikes(postID: postID!, six: 0, four: 0, out: 1)
        }
    }
    
    @IBAction func shareButton(_ sender: Any) {
        var items: [Any] = []
        print("comment label" , self.commentPostLabel.text)
        if self.commentPostLabel.text != nil {
                items.append(self.commentPostLabel.text!)
        }
        if self.commentPostImageView.image != nil {
            items.append(self.commentPostImageView.image!)
        }

        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: items, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        let currentOffset = scrollView.contentOffset.y
//        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
//        // Change 10.0 to adjust the distance from bottom
//        if maximumOffset - currentOffset <= 2.0 {
//            print("reached bottom")
//            if !isfetchingComment {
//                isfetchingComment = true
//                getMyComments(pageNo: nextPage)
//            }
//        }
//    }
    
    func commentSubmit() {
        print(writeCommentTextView)
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"POST_ID\":\"\(postID!)\",\"COMMENT\":\"\(writeCommentTextView.text!)\",\"PARENT_COMMENT_ID\":\"\"}", mod: "CricSpace", actionType: "add-comments-for-post") { (response) in
            
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    
                    if self.scrollView.contentSize.height - self.scrollView.bounds.size.height > 0 && self.mycommentMutable.count <= 10 {
                        print("scroll to position zero")
                        self.offSet = CGPoint(x: 0, y: self.scrollView.contentSize.height - self.scrollView.bounds.size.height + self.newContentHeight)
                        self.newContentHeight = 0
                    }
                    self.mycommentMutable.removeAllObjects()
                    self.getMyComments(pageNo: 1)
                    self.writeCommentTextView.text = "Write a comment"
                    self.writeCommentTextView.textColor = UIColor.lightGray
                    self.writeCommentTextView.resignFirstResponder()
                    let post:[String: Any] = ["post":self.postID!]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "editPostDetailRefreshData"), object: nil, userInfo: post)
                } else {
                    self.showAlert(title: "comment", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops", message: "Something went wrong! Please try again")
            }
        }
    }
    
    func editCommentSubmit(commentId: String) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"POST_ID\":\"\(postID!)\",\"COMMENT\":\"\(writeCommentTextView.text!)\",\"PARENT_COMMENT_ID\":\"\",\"COMMENT_ID\":\"\(commentId)\"}", mod: "CricSpace", actionType: "add-comments-for-post") { (response) in
            
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    let commentList = self.mycommentMutable
                    var x = 0
                    repeat {
                        let comment = commentList[x]
                        if (comment as! NSDictionary)["COMMENT_ID"] as? String ?? "\((comment as! NSDictionary)["COMMENT_ID"] as! Int)" == commentId {
                            self.mycommentMutable.remove(comment)
                        }
                        x = x + 1
                    } while(x < commentList.count)
                    self.getMyComments(pageNo: 1)
                    self.commentEdit = false
                    self.writeCommentTextView.text = ""
                    let post:[String: Any] = ["post":self.postID!]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "editPostDetailRefreshData"), object: nil, userInfo: post)
                } else {
                    self.showAlert(title: "comment", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops", message: "Something went wrong! Please try again")
            }
        }
    }
    
    func getMyComments(pageNo: Int) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"CURRENT_PAGE\":\"\(pageNo)\",\"POST_ID\":\"\(postID!)\"}", mod: "CricSpace", actionType: "get-all-comments") { (response) in
            print("get my comments", response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {

                   let getComments = ((response as! NSDictionary)["XSCData"] as! NSDictionary)
                    self.myComments = getComments["COMMENTS"] as! NSArray
                    if getComments["NEXT_PAGE"] as? Int ?? 0 != 0 {
                        self.nextPage = getComments["NEXT_PAGE"] as? Int ?? Int(getComments["NEXT_PAGE"] as! String)!
                    } else {
                        self.viewPreviousCommentsLabel.isHidden = true
                    }
//                    self.offSet = CGPoint(x: 0, y: self.scrollView.contentSize.height - self.scrollView.bounds.size.height)
                    for comment in self.myComments {
                        if !self.mycommentMutable.contains(comment) {
                            self.mycommentMutable.add(comment)
                            self.mycommentMutable = (self.mycommentMutable.sorted(by: { ($0 as! NSDictionary)["UPDATED_TIME"] as! String > ($1 as! NSDictionary)["UPDATED_TIME"] as! String}) as NSArray).mutableCopy() as! NSMutableArray
                            self.mycommentMutable = (self.mycommentMutable.filterDuplicates { ($0 as! NSDictionary)["COMMENT_ID"] as! String == ($1 as! NSDictionary)["COMMENT_ID"] as! String } as NSArray).mutableCopy() as! NSMutableArray
                        }
                    }
                    self.reloadAndResizeTable()
                    self.getPostDetails()
                    print("mycommnebtyfk",self.mycommentMutable.count)
                } else {
                    print((response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                print("Something went wrong! Please try again")
            }
        }
    }
    
    
    func getPostDetails() {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"POST_ID\":\"\(postID!)\",\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "CricSpace", actionType: "get-post-details") { (response) in
            print("post details" ,response)
            
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    [self.commentProfileImageView,self.commentNameLabel,self.commentAddressTimeLabel,self.commentPostImageView,self.commentPostLabel].forEach { (object) in
                        object?.hideSkeleton()
                    }
                    self.postDetails = ((response as! NSDictionary)["XSCData"] as! NSDictionary)
                    self.commentProfileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(self.postDetails?["USER_IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "profile_pic_default.png"))
                    self.commentNameLabel.text = (self.postDetails?["POSTED_BY"] as? String)?.capitalized
//                    self.commentAddressTimeLabel.text = "Chennai \u{2022} \(self.postDetails?["CREATED_DATE"] as! String)"
                    if let _ = self.postDetails!["ADDRESS"] as? String {
                    let addressList = (self.postDetails!["ADDRESS"] as! String).split(separator: ",")
                    if addressList.count > 3 && addressList.count < 9 {
                        self.commentAddressTimeLabel.text = "\(addressList[addressList.endIndex - 4].trimmingCharacters(in: .whitespaces)) \u{2022} \(self.postDetails!["CREATED_DATE"] as! String)"
                    } else {
                        self.commentAddressTimeLabel.text = "\(addressList[addressList.endIndex - 2].trimmingCharacters(in: .whitespaces)) \u{2022} \(self.postDetails!["CREATED_DATE"] as! String)"
                    }
                    }
                    
                    self.commentPostLabel.text = self.postDetails?["POST"] as? String
                    if self.postDetails?["POST_IMAGE_URL"] as! String != "" {
//                        self.commentPostImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(self.postDetails?["POST_IMAGE_URL"] as! String)"), placeholderImage: nil)
                        
                        self.commentPostImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(self.postDetails?["POST_IMAGE_URL"] as! String)"), placeholderImage: nil, options: [SDWebImageOptions.delayPlaceholder], completed: {(image, error, cacheType, imageURL) in
                            // Perform operation.
                            if error != nil {
                               self.postImageViewHeight.constant = 0
                            } else {
                                let aspect = (image?.size.width)! / (image?.size.height)!
                                self.aspectConstraint = NSLayoutConstraint(item: self.commentPostImageView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: self.commentPostImageView, attribute: NSLayoutAttribute.height, multiplier: aspect, constant: 0.0)
                            }
                        })
                        
                    } else {
                        self.postImageViewHeight.constant = 0
                    }
                    if self.postDetails?["SIX"] as? String ?? "\(self.postDetails?["SIX"] as! Int)" == "0" || self.postDetails?["SIX"] as? String ?? "\(self.postDetails?["SIX"] as! Int)" == "1" {
                         self.sixLabel.text = "\(self.postDetails?["SIX"] as? String ?? "\(self.postDetails?["SIX"] as! Int)") six"
                    } else {
                         self.sixLabel.text = "\(self.postDetails?["SIX"] as? String ?? "\(self.postDetails?["SIX"] as! Int)") sixes"
                    }
                    
                    if self.postDetails?["FOUR"] as? String ?? "\(self.postDetails?["FOUR"] as! Int)" == "0" || self.postDetails?["FOUR"] as? String ?? "\(self.postDetails?["FOUR"] as! Int)" == "1" {
                        self.fourLabel.text = "\(self.postDetails?["FOUR"] as? String ?? "\(self.postDetails?["FOUR"] as! Int)") four"
                    } else {
                        self.fourLabel.text = "\(self.postDetails?["FOUR"] as? String ?? "\(self.postDetails?["FOUR"] as! Int)") fours"
                    }
                    
                    if self.postDetails?["WICKET"] as? String ?? "\(self.postDetails?["WICKET"] as! Int)" == "0" || self.postDetails?["WICKET"] as? String ?? "\(self.postDetails?["WICKET"] as! Int)" == "1" {
                        self.outLabel.text = "\(self.postDetails?["WICKET"] as? String ?? "\(self.postDetails?["WICKET"] as! Int)") out"
                    } else {
                        self.outLabel.text = "\(self.postDetails?["WICKET"] as? String ?? "\(self.postDetails?["WICKET"] as! Int)") outs"
                    }
                   
                    if self.postDetails?["COMMENT"] as? String ?? "\(self.postDetails?["COMMENT"] as! Int)" == "0" || self.postDetails?["COMMENT"] as? String ?? "\(self.postDetails?["COMMENT"] as! Int)" == "1" {
                        self.commentLabel.text = "\(self.postDetails?["COMMENT"] as? String ?? "\(self.postDetails?["COMMENT"] as! Int)") comment"
                    } else {
                        self.commentLabel.text = "\(self.postDetails?["COMMENT"] as? String ?? "\(self.postDetails?["COMMENT"] as! Int)") comments"
                    }
                    
                    
                    if self.postDetails?["USER_STATUS"] as? String ?? "liked post" != "" {
                        let myLikes = self.postDetails?["USER_STATUS"] as! NSDictionary
                        
                        if myLikes["IS_SIX"] as! String == "true" {
                            self.sixButton.setImage(#imageLiteral(resourceName: "6-color.png"), for: .normal)
                        } else {
                            self.sixButton.setImage(#imageLiteral(resourceName: "6-bw.png"), for: .normal)
                        }
                        
                        if myLikes["IS_FOUR"] as! String == "true" {
                            self.fourButton.setImage(#imageLiteral(resourceName: "4-color"), for: .normal)
                        } else {
                            self.fourButton.setImage(#imageLiteral(resourceName: "4-bw"), for: .normal)
                        }
                        
                        if myLikes["IS_WICKET"] as! String == "true" {
                            self.outButton.setImage(#imageLiteral(resourceName: "out-color"), for: .normal)
                        } else {
                            self.outButton.setImage(#imageLiteral(resourceName: "out-bw"), for: .normal)
                        }
                    }
                } else {
                    print((response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                print("Something went wrong! Please try again")
            }
        }
    }
    
    func addLikes(postID: String,six: Int, four:Int, out:Int) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"POST_ID\":\"\(postID)\",\"SIX\":\"\(six)\",\"FOUR\":\"\(four)\",\"WICKET\":\"\(out)\"}", mod: "CricSpace", actionType: "add-post-like") { (response) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                
                    let likeDetails = (response as! NSDictionary)["XSCData"] as! NSDictionary
                    self.postDetails = likeDetails
                    if likeDetails["USER_STATUS"] as? String ?? "liked post" != "" {
                        let myLikes = likeDetails["USER_STATUS"] as! NSDictionary
                        
                        if myLikes["IS_SIX"] as! String == "true" {
                            self.sixButton.setImage(#imageLiteral(resourceName: "6-color.png"), for: .normal)
                        } else {
                            self.sixButton.setImage(#imageLiteral(resourceName: "6-bw.png"), for: .normal)
                        }
                        
                        if myLikes["IS_FOUR"] as! String == "true" {
                            self.fourButton.setImage(#imageLiteral(resourceName: "4-color"), for: .normal)
                        } else {
                            self.fourButton.setImage(#imageLiteral(resourceName: "4-bw"), for: .normal)
                        }
                        
                        if myLikes["IS_WICKET"] as! String == "true" {
                            self.outButton.setImage(#imageLiteral(resourceName: "out-color"), for: .normal)

                        } else {
                            self.outButton.setImage(#imageLiteral(resourceName: "out-bw"), for: .normal)
                        }
                    }
                    
                    if likeDetails["SIX"] as? String ?? "\(likeDetails["SIX"] as! Int)" == "0" || likeDetails["SIX"] as? String ?? "\(likeDetails["SIX"] as! Int)" == "1" {
                        self.sixLabel.text = "\(likeDetails["SIX"] as? String ?? "\(likeDetails["SIX"] as! Int)") six"
                    } else {
                        self.sixLabel.text = "\(likeDetails["SIX"] as? String ?? "\(likeDetails["SIX"] as! Int)") sixes"
                    }
                    
                    if likeDetails["FOUR"] as? String ?? "\(likeDetails["FOUR"] as! Int)" == "0" || likeDetails["FOUR"] as? String ?? "\(likeDetails["FOUR"] as! Int)" == "1" {
                        self.fourLabel.text = "\(likeDetails["FOUR"] as? String ?? "\(likeDetails["FOUR"] as! Int)") four"
                    } else {
                        self.fourLabel.text = "\(likeDetails["FOUR"] as? String ?? "\(likeDetails["FOUR"] as! Int)") fours"
                    }
                    
                    if likeDetails["WICKET"] as? String ?? "\(likeDetails["WICKET"] as! Int)" == "0" || likeDetails["WICKET"] as? String ?? "\(likeDetails["WICKET"] as! Int)" == "1" {
                        self.outLabel.text = "\(likeDetails["WICKET"] as? String ?? "\(likeDetails["WICKET"] as! Int)") out"
                    } else {
                        self.outLabel.text = "\(likeDetails["WICKET"] as? String ?? "\(likeDetails["WICKET"] as! Int)") outs"
                    }
                    
                    if likeDetails["COMMENT"] as? String ?? "\(likeDetails["COMMENT"] as! Int)" == "0" || likeDetails["COMMENT"] as? String ?? "\(likeDetails["COMMENT"] as! Int)" == "1" {
                        self.commentLabel.text = "\(likeDetails["COMMENT"] as? String ?? "\(likeDetails["COMMENT"] as! Int)") comments"
                    } else {
                        self.commentLabel.text = "\(likeDetails["COMMENT"] as? String ?? "\(likeDetails["COMMENT"] as! Int)") comments"
                    }
                    
                    
                    let post:[String: Any] = ["post":self.postID!]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "editPostDetailRefreshData"), object: nil, userInfo: post)
                } else {
                    print((response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                print("Something went wrong! Please try again")
            }
        }
    }
    
    func reloadAndResizeTable() {
        self.commentListTableView.reloadData()
            self.commentTableViewHeight.constant = self.commentListTableView.contentSize.height
                self.view.layoutIfNeeded()
                self.isfetchingComment = false
        self.scrollView.setContentOffset(offSet, animated: true)
        offSet = CGPoint(x: 0, y: self.scrollView.contentOffset.y)
    }
}
