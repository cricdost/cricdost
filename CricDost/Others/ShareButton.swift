//
//  ShareButton.swift
//  CricDost
//
//  Created by Jit Goel on 10/22/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ShareButtonCustom: UIButton {
    var postImage: UIImageView?
    var postLabel: UILabel?
    var postTitle: UILabel?
    var extraDetails: Int = 0
    
    
    convenience init(postImage: UIImageView, postLabel: UILabel,postTitle: UILabel, extraDetails: Int) {
        self.init(frame:CGRect.zero)
        self.postImage = postImage
        self.postLabel = postLabel
        self.postTitle = postTitle
        self.extraDetails = extraDetails
    }
}
