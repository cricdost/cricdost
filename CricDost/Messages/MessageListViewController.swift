//
//  MessageListViewController.swift
//  CricDost
//
//  Created by Jit Goel on 7/19/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import XMPPFramework

class MessageListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var chatList: [String] = []
    @IBOutlet weak var messageTableView: UITableView!
    var xmppController: XMPPController?
    var stream: XMPPStream?
    var password: String?
    var userName: String?
    var users: NSArray = []
    var chatrooms: NSArray = []
    var chatArray: NSMutableArray = []
    var chatHistory: NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userName = UserDefaults.standard.object(forKey: CommonUtil.OPENFIRE_USERNAME) as? String
        password = UserDefaults.standard.object(forKey: CommonUtil.OPENFIRE_PASSWORD) as? String
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = CommonUtil.themeRed
        }
//        UIApplication.shared.statusBarStyle = .lightContent
        
//        print(UserDefaults.standard.object(forKey: UserDefaults.standard.object(forKey: CommonUtil.OPENFIRE_USERNAME) as! String))
        
        chatList = UserDefaults.standard.object(forKey: UserDefaults.standard.object(forKey: CommonUtil.OPENFIRE_USERNAME) as! String) as? [String] ?? []
        let userlist = UserDefaults.standard.object(forKey: CommonUtil.USERLIST) as! Data
        let decodedData = NSKeyedUnarchiver.unarchiveObject(with: userlist) as! NSDictionary

        users = decodedData["users"] as? NSArray ?? []
        
        let chatRoomlist = UserDefaults.standard.object(forKey: CommonUtil.CHATROOMLIST) as! Data
        let decodedData1 = NSKeyedUnarchiver.unarchiveObject(with: chatRoomlist) as! NSDictionary
        
        chatrooms = decodedData1["chatRooms"] as? NSArray ?? []
        print("CHATLIST", chatList)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshChatHome), name: NSNotification.Name(rawValue: "refreshChatHome"), object: nil)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func refreshChatHome(_ notification: NSNotification) {
        print("ReFreshing Chat List")
        chatArray.removeAllObjects()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd, MMM hh:mm a"
        
        chatList = UserDefaults.standard.object(forKey: UserDefaults.standard.object(forKey: CommonUtil.OPENFIRE_USERNAME) as! String) as? [String] ?? []
        
        for chat in chatList {
            for user in users {
                if chat == (user as! NSDictionary)["username"] as! String {
                    if let msg = UserDefaults.standard.object(forKey: chat) as? NSArray {
                        let message = (msg[0] as! NSDictionary)["message"] as! String
                        let time = dateFormatter.date(from: (msg[0] as! NSDictionary)["time"] as! String)
                        let nickName = (user as! NSDictionary)["name"] as! String
                        let imageUrl = (msg[0] as! NSDictionary)["image"] as? String ?? ""
                        let chatDetail: NSDictionary = ["Name":chat, "message": message, "time": time!,"nickName":nickName,"image":imageUrl]

                        chatArray.insert(chatDetail, at: 0)
                    }
                }
            }
            
            if chat.contains(find: "t_") || chat.contains(find: "m_") {
                var chatDetail: NSDictionary?
                if let msg = UserDefaults.standard.object(forKey: chat) as? NSArray {
                    var message = ""
                    var time = Date()
                    var imageUrl = ""
                    if msg.count != 0 {
                        message = (msg[0] as! NSDictionary)["message"] as! String
                        time = dateFormatter.date(from: (msg[0] as! NSDictionary)["time"] as! String)!
                        imageUrl = (msg[0] as! NSDictionary)["image"] as? String ?? ""
                    }
                    for room in chatrooms {
                        if (room as! NSDictionary)["roomName"] as! String == chat {
                            chatDetail = ["Name":chat, "message": message, "time": time,"nickName":(room as! NSDictionary)["naturalName"] as! String, "image":imageUrl]
                        }
                    }
//                    if chatArray.count == 0 {
//                        chatArray[0] = chatDetail!
//                    } else {
                        chatArray.insert(chatDetail!, at: 0)
//                    }
                }
                
            }
        }
        chatHistory = (chatArray.sorted(by: { (($0 as! NSDictionary)["time"] as! Date).compare(($1 as! NSDictionary)["time"] as! Date) == .orderedDescending })) as NSArray
        print(chatHistory)
        UIView.transition(with: messageTableView, duration: 0.5, options: .transitionCrossDissolve, animations: {self.messageTableView.reloadData()}, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        chatArray.removeAllObjects()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd, MMM hh:mm a"
        
        for chat in chatList {
            for user in users {
                if chat == (user as! NSDictionary)["username"] as! String {
                    if let msg = UserDefaults.standard.object(forKey: chat) as? NSArray {
                        let message = (msg[0] as! NSDictionary)["message"] as! String
                        let time = dateFormatter.date(from: (msg[0] as! NSDictionary)["time"] as! String)
                        let nickName = (user as! NSDictionary)["name"] as! String
                        let imageUrl = (msg[0] as! NSDictionary)["image"] as? String ?? ""
                        let chatDetail: NSDictionary = ["Name":chat, "message": message, "time": time!,"nickName":nickName, "image":imageUrl]
                        chatArray.insert(chatDetail, at: 0)
                    }
                }
            }
            if chat.contains(find: "t_") || chat.contains(find: "m_") {
                var chatDetail: NSDictionary?
                if let msg = UserDefaults.standard.object(forKey: chat) as? NSArray {
                    var message = ""
                    var time = Date()
                    var imageUrl = ""
                    if msg.count != 0 {
                        message = (msg[0] as! NSDictionary)["message"] as! String
                        time = dateFormatter.date(from: (msg[0] as! NSDictionary)["time"] as! String)!
                        imageUrl = (msg[0] as! NSDictionary)["image"] as? String ?? ""
                    }
                    
                    for room in chatrooms {
                        if (room as! NSDictionary)["roomName"] as! String == chat {
                            chatDetail = ["Name":chat, "message": message, "time": time,"nickName":(room as! NSDictionary)["naturalName"] as! String, "image":imageUrl]
                        }
                    }
                    print(chatArray)
//                    if chatArray.count == 0 {
//                        chatArray[0] = chatDetail!
//                    } else {
                        chatArray.insert(chatDetail!, at: 0)
//                    }
                    
                }
            }
        }
//        print(chatArray)
        chatHistory = (chatArray.sorted(by: { (($0 as! NSDictionary)["time"] as! Date).compare(($1 as! NSDictionary)["time"] as! Date) == .orderedDescending })) as NSArray
        print(chatHistory)
        UIView.transition(with: messageTableView, duration: 0.5, options: .transitionCrossDissolve, animations: {self.messageTableView.reloadData()}, completion: nil)
        
    }
    
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let chat = chatHistory[indexPath.row] as! NSDictionary
        print(chat)
        let cell = tableView.dequeueReusableCell(withIdentifier: "messageListCell", for: indexPath) as! MessageListTableViewCell
//        cell.nameLabel.text = chat
        
        if chat["image"] as? String ?? "" != "" {
            print("\(CommonUtil.BASE_URL)\(chat["image"] as? String ?? "")")
            cell.profileImage.image = #imageLiteral(resourceName: "profile_pic_default")
            let url = UserDefaults.standard.object(forKey: "\(chat["Name"] as! String)_IMAGEURL") as? String ?? ""
            if url != "" {
                cell.profileImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(url)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
            }
        }
        
        for user in users {
            if (chat["Name"] as! String) == (user as! NSDictionary)["username"] as! String {
                cell.nameLabel.text = chat["nickName"] as? String
            }
        }
        if (chat["Name"] as! String).contains(find: "t_") || (chat["Name"] as! String).contains(find: "m_") {
            cell.nameLabel.text = chat["nickName"] as? String
            cell.profileImage.image = #imageLiteral(resourceName: "no_team_image_icon")
            let url = UserDefaults.standard.object(forKey: "\(chat["Name"] as! String)_IMAGEURL") as? String ?? ""
            if url != "" {
                cell.profileImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(url)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            }
        }

        if let msg = UserDefaults.standard.object(forKey: chat["Name"] as! String) as? NSArray {
            if msg.count != 0 {
                cell.messageLabel.text = (msg[0] as! NSDictionary)["message"] as? String
                cell.timeLabel.text = (msg[0] as! NSDictionary)["time"] as? String
            } else {
                cell.messageLabel.text = ""
                cell.timeLabel.text = ""
            }
        }
        
        if (UserDefaults.standard.object(forKey: "\((chat["Name"] as! String))_\(userName!)") as? Int ?? 0) != 0 {
            cell.unReadLabel.isHidden = false
            cell.unReadLabel.text = "\(UserDefaults.standard.object(forKey: "\((chat["Name"] as! String))_\(userName!)") as! Int)"
        } else {
            cell.unReadLabel.isHidden = true
        }
        if chat["Name"] as! String == "cricdost" {
            cell.profileImage.image = #imageLiteral(resourceName: "cd_logo_version")
            cell.profileImage.contentMode = .scaleAspectFit
            cell.profileImage.layer.masksToBounds = false
            cell.profileImage.layer.cornerRadius = 0
            cell.profileImage.clipsToBounds = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chat = chatHistory[indexPath.row] as! NSDictionary
        if (chat["Name"] as! String).contains(find: "t_") || (chat["Name"] as! String).contains(find: "m_") {
            print("Go to group Chat")
            let groupChatVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GroupChatViewController") as? GroupChatViewController
            groupChatVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            groupChatVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            groupChatVC?.groupJIDPrefix = chat["Name"] as? String
            groupChatVC?.teamName = chat["nickName"] as? String
            self.present(groupChatVC!, animated: true, completion: nil)
        } else {
            let chatVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
            chatVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            chatVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            chatVC.playerUserName = chat["Name"] as? String
            self.present(chatVC, animated: true, completion: nil)
        }
        
    }
}
