//
//  FixedMatchesCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 6/26/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class FixedMatchesCollectionViewCell: UICollectionViewCell {
        
        @IBOutlet weak var containerView: UIView!
        @IBOutlet weak var adminButton: UIButton!
        @IBOutlet weak var dateLabel: UILabel!
        @IBOutlet weak var timeLabel: UILabel!
        @IBOutlet weak var teamAImageView: UIImageView!
        @IBOutlet weak var teamBImageView: UIImageView!
        @IBOutlet weak var challengersCount: UILabel!
        @IBOutlet weak var teamAName: UILabel!
        @IBOutlet weak var teamBName: UILabel!
        @IBOutlet weak var addressLabel: UILabel!
        @IBOutlet weak var numberOfPlayers: UILabel!
        @IBOutlet weak var joinButton: UIButton!
        @IBOutlet weak var challengeBUtton: UIButton!
        @IBOutlet weak var chatButton: UIButton!
        @IBOutlet weak var chatButton1: UIButton!
        @IBOutlet weak var exitButton: UIButton!
        @IBOutlet weak var tossButton: UIButton!
        @IBOutlet weak var teamAPlayerImage: UIImageView!
        @IBOutlet weak var teamBPlayerImage: UIImageView!
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            containerView.layer.cornerRadius = 5.0
            containerView.layer.shadowRadius = 3
            containerView.layer.shadowOpacity = 0.5
            containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
            containerView.clipsToBounds = false
            
            CommonUtil.buttonRoundedCorners(buttons: [adminButton,joinButton,challengeBUtton, chatButton, exitButton, tossButton, chatButton1])
            CommonUtil.imageRoundedCorners(imageviews: [teamAImageView, teamBImageView, teamAPlayerImage, teamBPlayerImage])
        }
    
}
