//
//  JoinTournamentViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 8/29/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import PlugNPlay

class JoinTournamentViewController: UIViewController ,UITextFieldDelegate , ShowsAlert , UICollectionViewDelegate, UICollectionViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var preventUserInteractionView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var dismissViewHolder: UIView!
    @IBOutlet weak var joinTourLoginView: UIView!
    @IBOutlet weak var joinTourEnterPinTextFeild: UITextField!
    @IBOutlet weak var joinTourForgotPasswordLabel: UILabel!
    @IBOutlet weak var joinTourAgreeButton: UIButton!
    @IBOutlet weak var joinTourTermsAndCondLabel: UILabel!
    @IBOutlet weak var joinTourNextButton: UIButton!
    @IBOutlet weak var paymentFailedView: UIView!
    @IBOutlet weak var paymentSuccessView: UIView!
    @IBOutlet weak var paymentFailedLabel: UILabel!
    @IBOutlet weak var tryAgainButton: UIButton!
    @IBOutlet weak var paymentSuccessOkButton: UIButton!
    
    @IBOutlet weak var joinTourCreatePinView: UIView!
    @IBOutlet weak var joinTourNewPinTextField: UITextField!
    @IBOutlet weak var joinTourConfirmPinTextField: UITextField!
    @IBOutlet weak var joinTourCreatePinAgreeButton: UIButton!
    @IBOutlet weak var joinTourCreatePinTermCondLabel: UILabel!
    @IBOutlet weak var joinTourCreatePinNextButton: UIButton!
    
    @IBOutlet weak var joinTourCreateTeamToSelectPlayer: UIView!
    @IBOutlet weak var joinTourSelectPlayersCollectionView: UICollectionView!
    @IBOutlet weak var joinTourSelectPlayersContinueButton: UIButton!
    
    @IBOutlet weak var joinTourChoosePlayersView: UIView!
    @IBOutlet weak var joinTourChoosePlayerSearchBarTextField: UITextField!
    @IBOutlet weak var joinTourChoosePlayerFilterButton: UIButton!
    @IBOutlet weak var joinTourChoosePlayerCollectionView: UICollectionView!
    @IBOutlet weak var joinTourChoosrPlayerProgressView: UIProgressView!
    @IBOutlet weak var joinTourChoosePlayerClearAllLAbel: UILabel!
    @IBOutlet weak var joinTourChoosePlayerSelectedLabel: UILabel!
    @IBOutlet weak var joinTounChoosePlayerBackButton: UIButton!
    @IBOutlet weak var joinTourChoosePlayerContinueButton: UIButton!
    
    @IBOutlet weak var confirmPlayersView: UIView!
    @IBOutlet weak var tournamentNametextField: UITextField!
    @IBOutlet weak var tournamentTeamImage: UIImageView!
    @IBOutlet weak var dropDownButtonTeamNames: UIButton!
    @IBOutlet weak var confirmPlayerCollectionView: UICollectionView!
    @IBOutlet weak var confirmPlayerContinueButton: UIButton!
    
    @IBOutlet weak var proceedToPaymentView: UIView!
    @IBOutlet weak var proceedPaymentButton: UIButton!
    var receivedTeam_ID = ""
    
    var tourDetail: NSDictionary?
    var selectedTournament: NSDictionary?
    var hasPin: Int?
    var teamList: NSArray = []
    var playerList: NSArray = []
    var playerListMutable: NSMutableArray = []
    var selectedPlayersList: NSMutableArray = []
    var selectedTeam: String?
    var selectedTeamsIDs : [String] = []
    var _selectedCells : NSMutableArray = []
    var _selectedPlayers : [String] = []
    var teamID: String?
    var isInTournamentDetailPage = false
    
    @IBOutlet weak var playerTypeTextField: UITextField!
    var playerTypePicker: UIPickerView?
    let playerTypes = ["All","Batsman", "Bowler", "Keeper", "All Rounder"]
    var selectedPlayerType = ""
    @IBOutlet weak var tournamentTeamNameDummy: UITextField!
    var teamPicker: UIPickerView?
    let teams: NSMutableArray = []
    var selectTeam: NSDictionary = [:]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [joinTourLoginView,joinTourCreatePinView,joinTourCreateTeamToSelectPlayer,joinTourChoosePlayersView, confirmPlayersView,proceedToPaymentView,paymentFailedView,paymentSuccessView])
        CommonUtil.buttonRoundedCorners(buttons: [joinTourNextButton,proceedPaymentButton,joinTourCreatePinNextButton,joinTourSelectPlayersContinueButton,joinTourChoosePlayerContinueButton, confirmPlayerContinueButton,paymentSuccessOkButton,tryAgainButton])
        setCollectionViewLayout(card: joinTourChoosePlayersView, collectionView: joinTourChoosePlayerCollectionView, center: false)
        setCollectionViewLayout(card: joinTourCreateTeamToSelectPlayer, collectionView: joinTourSelectPlayersCollectionView, center: false)
        setCollectionViewLayout(card: confirmPlayersView, collectionView: confirmPlayerCollectionView, center: false)
        
        CommonUtil.newButtonRoundedCorners(buttons: [joinTourChoosePlayerFilterButton])
        
        CommonUtil.imageRoundedCorners(imageviews: [tournamentTeamImage])
        
        let dismissViewGesture = UITapGestureRecognizer(target: self, action: #selector(dissmissViewHolderTapped))
        dismissViewHolder.isUserInteractionEnabled = true
        dismissViewHolder.addGestureRecognizer(dismissViewGesture)
        
        let forgotPasswordGesture = UITapGestureRecognizer(target: self, action: #selector(forgotPasswordTapped))
        joinTourForgotPasswordLabel.isUserInteractionEnabled = true
        joinTourForgotPasswordLabel.addGestureRecognizer(forgotPasswordGesture)
        
        let joinTourTermsAndConditionGesture = UITapGestureRecognizer(target: self, action: #selector(joinTourTermsAndConditionTapped))
        joinTourTermsAndCondLabel.isUserInteractionEnabled = true
        joinTourTermsAndCondLabel.addGestureRecognizer(joinTourTermsAndConditionGesture)
        
        let joinTourCreatePinTermsAndConditionGesture = UITapGestureRecognizer(target: self, action: #selector(joinTourCreatePinTermsAndConditionTapped))
        joinTourCreatePinTermCondLabel.isUserInteractionEnabled = true
        joinTourCreatePinTermCondLabel.addGestureRecognizer(joinTourCreatePinTermsAndConditionGesture)
        
        let joinTourClearAll = UITapGestureRecognizer(target: self, action: #selector(clearAllTapFunction))
        joinTourChoosePlayerClearAllLAbel.isUserInteractionEnabled = true
        joinTourChoosePlayerClearAllLAbel.addGestureRecognizer(joinTourClearAll)
        
        joinTourAgreeButton.backgroundColor = .clear
        joinTourAgreeButton.layer.cornerRadius = 0
        joinTourAgreeButton.layer.borderWidth = 1
        joinTourAgreeButton.layer.borderColor = UIColor.black.cgColor
        
        joinTourCreatePinAgreeButton.backgroundColor = .clear
        joinTourCreatePinAgreeButton.layer.cornerRadius = 0
        joinTourCreatePinAgreeButton.layer.borderWidth = 1
        joinTourCreatePinAgreeButton.layer.borderColor = UIColor.black.cgColor
        
        teamList = tourDetail!["TEAM_LIST"] as! NSArray
        
        playerTypePicker = UIPickerView()
        playerTypePicker?.dataSource = self
        playerTypePicker?.delegate = self
        playerTypeTextField.inputView = playerTypePicker
        
        teamPicker = UIPickerView()
        teamPicker?.dataSource = self
        teamPicker?.delegate = self
        tournamentTeamNameDummy.inputView = teamPicker
        
        let playerTypeToolBar = UIToolbar().ToolbarPiker(mySelect: #selector(playerTypeDismissPicker))
        playerTypeTextField.inputAccessoryView = playerTypeToolBar
        
        let teamsToolBar = UIToolbar().ToolbarPiker(mySelect: #selector(teamsDismissPicker))
        tournamentTeamNameDummy.inputAccessoryView = teamsToolBar
        
        getPinStatus()
        //getJoinTourMyTeams()
        joinTourNextButton.isEnabled = false
        joinTourNextButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        joinTourCreatePinNextButton.isEnabled = false
        joinTourCreatePinNextButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        proceedPaymentButton.setTitle("Proceed to Pay \u{20B9}\(self.selectedTournament!["ENTRANCE_FEE"] as! String)", for: .normal)
    }
    
   
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == teamPicker {
            return teams.count
        } else {
            return playerTypes.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == teamPicker {
            return (teams[row] as! NSDictionary)["TEAM_NAME"] as? String
        } else {
            return playerTypes[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
        if pickerView == teamPicker {
            tournamentNametextField.text = (teams[row] as! NSDictionary)["TEAM_NAME"] as? String
            selectTeam = teams[row] as! NSDictionary
            tournamentTeamImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\((teams[row] as! NSDictionary)["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
        } else {
            playerTypeTextField.text = playerTypes[row]
            selectedPlayerType = playerTypeTextField.text!
            joinTourChoosePlayerFilterButton.setTitle(selectedPlayerType, for: .normal)
            playerListMutable.removeAllObjects()
            for profile in playerList {
                let player = profile as! NSDictionary
                if (player["SKILLS"] as! String).containsIgnoringCase(find: selectedPlayerType){
                    playerListMutable.add(player)
                } else if selectedPlayerType == "All" {
                    playerListMutable.add(player)
                }
//                if selectedPlayerType == "Batsman" || selectedPlayerType == "Bowler" {
//                    if (player["SKILLS"] as! String).containsIgnoringCase(find: "All Rounder") {
//                        playerListMutable.add(player)
//                    }
//                }
            }
            if playerTypeTextField.text == "" {
                playerListMutable = playerList.mutableCopy() as! NSMutableArray
            }
            joinTourChoosePlayerCollectionView.reloadData()
        }
    }
    
    @IBAction func selectPlayerTypeButtonClick(_ sender: Any) {
        playerTypeTextField.becomeFirstResponder()
    }
    
    @IBAction func dropdownButtonClick(_ sender: Any) {
        tournamentTeamNameDummy.becomeFirstResponder()
    }
    
    @objc func teamsDismissPicker() {
        tournamentTeamNameDummy.resignFirstResponder()
    }
    
    @objc func playerTypeDismissPicker() {
        selectedPlayerType = playerTypeTextField.text!
        playerListMutable.removeAllObjects()
        for profile in playerList {
            let player = profile as! NSDictionary
            if (player["SKILLS"] as! String).containsIgnoringCase(find: selectedPlayerType) {
                playerListMutable.add(player)
            } else if selectedPlayerType == "All" {
                playerListMutable.add(player)
            }
        }
        if playerTypeTextField.text == "" {
            playerListMutable = playerList.mutableCopy() as! NSMutableArray
        }
        joinTourChoosePlayerCollectionView.reloadData()
        playerTypeTextField.resignFirstResponder()
    }
    
    @objc func dissmissViewHolderTapped() {
        let alert = UIAlertController(title: "Close", message: "Are you sure you want to close? All your entered info will be lost", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            self.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openNearByTournament"), object: nil)
            })
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == joinTourEnterPinTextFeild {
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else  if textField == joinTourNewPinTextField || textField   == joinTourConfirmPinTextField {
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else if textField == joinTourChoosePlayerSearchBarTextField {
            playerListMutable.removeAllObjects()
            for profile in playerList {
                let player = profile as! NSDictionary
                if (player["FULL_NAME"] as! String).containsIgnoringCase(find: textField.text!) {
                    playerListMutable.add(player)
                }
            }
            if textField.text == "" {
                playerListMutable = playerList.mutableCopy() as! NSMutableArray
            }
            joinTourChoosePlayerCollectionView.reloadData()
            return true
        } else if textField == tournamentNametextField {
            selectTeam = [:]
            return true
        } else {
            return true
        }
    }
    
    @IBAction func joinTourAgreeButtonClick(_ sender: Any) {
        print("Agree button clicked")
        if joinTourAgreeButton.backgroundColor == .clear  {
            joinTourNextButton.isEnabled = true
            joinTourNextButton.backgroundColor = .black
            joinTourAgreeButton.backgroundColor = CommonUtil.themeRed
            joinTourAgreeButton.layer.cornerRadius = 0
            joinTourAgreeButton.layer.borderWidth = 1
            joinTourAgreeButton.layer.borderColor = UIColor.red.cgColor
            joinTourAgreeButton.setImage(#imageLiteral(resourceName: "baseline_done_white_24pt_1x"), for: UIControlState.normal)
        } else if joinTourAgreeButton.backgroundColor == CommonUtil.themeRed {
            joinTourNextButton.isEnabled = false
            joinTourNextButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            joinTourAgreeButton.backgroundColor = .clear
            joinTourAgreeButton.layer.cornerRadius = 0
            joinTourAgreeButton.layer.borderWidth = 1
            joinTourAgreeButton.layer.borderColor = UIColor.black.cgColor
        }
    }
    
    @IBAction func joinTourCreatePinButtonClick(_ sender: Any) {
        print("Create pin agree button clicked")
        if joinTourCreatePinAgreeButton.backgroundColor == .clear  {
            joinTourCreatePinNextButton.isEnabled = true
            joinTourCreatePinNextButton.backgroundColor = .black
            joinTourCreatePinAgreeButton.backgroundColor = CommonUtil.themeRed
            joinTourCreatePinAgreeButton.layer.cornerRadius = 0
            joinTourCreatePinAgreeButton.layer.borderWidth = 1
            joinTourCreatePinAgreeButton.layer.borderColor = UIColor.red.cgColor
            joinTourCreatePinAgreeButton.setImage(#imageLiteral(resourceName: "baseline_done_white_24pt_1x"), for: UIControlState.normal)
        } else if joinTourCreatePinAgreeButton.backgroundColor == CommonUtil.themeRed {
            joinTourCreatePinNextButton.isEnabled = false
            joinTourCreatePinNextButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            joinTourCreatePinAgreeButton.backgroundColor = .clear
            joinTourCreatePinAgreeButton.layer.cornerRadius = 0
            joinTourCreatePinAgreeButton.layer.borderWidth = 1
            joinTourCreatePinAgreeButton.layer.borderColor = UIColor.black.cgColor
        }
    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let verticalIndicator = scrollView.subviews.last as? UIImageView
////        verticalIndicator?.backgroundColor = CommonUtil.themeRed
//        verticalIndicator?.tintColor = CommonUtil.themeRed
//    }
    
    @objc func forgotPasswordTapped(){
        print("Forgot password click")
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"USER_ID\":\"\(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String)\"}", mod: "Tournament", actionType: "forget-password") { (response) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    print(response)
                    self.showAlert(title: "Message", message: "Tournament PIN has been sent to your register mobile number")
                } else {
                    print((response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                print("Something went wrong! Please try again")
            }
        }
    }
    
    @objc func joinTourTermsAndConditionTapped(){
        print("Terms and condition Clicked")
        UIApplication.shared.open(NSURL(string:"http://www.cricdost.com/terms.html")! as URL)
    }
    @objc func joinTourCreatePinTermsAndConditionTapped() {
        print("Terms and condition Clicked")
        UIApplication.shared.open(NSURL(string:"http://www.cricdost.com/terms.html")! as URL)
    }
    
    func getPinStatus(){
        
        hasPin = tourDetail!["IS_PIN_EXIST"] as? Int
        
        if hasPin == 1 {
            
            UIView.transition(with: self.joinTourCreatePinView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.joinTourCreatePinView.isHidden = true
            })
            UIView.transition(with: self.joinTourLoginView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.joinTourLoginView.isHidden = false
            })
        } else {
            
            UIView.transition(with: self.joinTourLoginView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.joinTourLoginView.isHidden = true
            })
            UIView.transition(with: self.joinTourCreatePinView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.joinTourCreatePinView.isHidden = false
            })
        }
        
    }
    
    @IBAction func joinTourCreatePinNextButtonClick(_ sender: Any) {
        view.endEditing(true)
        print("create pin button clicked")
        if joinTourNewPinTextField.isReallyEmpty && joinTourConfirmPinTextField.isReallyEmpty {
            self.joinTourCreatePinView.shake()
            self.showAlert(title: "Missing Info", message: "Please enter CDT Pin")
        } else if (joinTourNewPinTextField.text != joinTourConfirmPinTextField.text) {
            self.joinTourCreatePinView.shake()
            self.showAlert(title: "Mismatch Password", message: "Please enter correct Password ")
        } else {
            self.activityIndicator.startAnimating()
            self.preventUserInteractionView.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"USER_ID\":\"\(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String)\",\"PIN\":\"\(joinTourNewPinTextField.text!)\"}", mod: "Tournament", actionType: "create-password") { (response) in
                if response as? String != "error" {
                    if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                        print(response)
                        UIView.transition(with: self.joinTourCreatePinView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                            self.joinTourCreatePinView.isHidden = true
                        })
                        UIView.transition(with: self.joinTourCreateTeamToSelectPlayer, duration: 0.4, options: .transitionCrossDissolve, animations: {
                            self.joinTourCreateTeamToSelectPlayer.isHidden = false
                        })
                        self.dismissViewHolder.isHidden = true
                    } else {
                        print((response as! NSDictionary)["XSCMessage"] as! String)
                    }
                } else {
                    print("Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
                self.preventUserInteractionView.isHidden = true
            }
        }
    }
    
    @IBAction func joinTourLoginNextButtonClick(_ sender: Any) {
        view.endEditing(true)
        if joinTourEnterPinTextFeild.isReallyEmpty {
            self.joinTourLoginView.shake()
            self.showAlert(title: "Missing Info", message: "Please enter CDT Pin")
        } else {
            activityIndicator.startAnimating()
            preventUserInteractionView.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"USER_ID\":\"\(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String)\",\"PIN\":\"\(joinTourEnterPinTextFeild.text!)\"}", mod: "Tournament", actionType: "password-verification") { (response) in
                if response as? String != "error" {
                    if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                        print(response)
                        UIView.transition(with: self.joinTourLoginView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                            self.joinTourLoginView.isHidden = true
                        })
                        UIView.transition(with: self.joinTourCreateTeamToSelectPlayer, duration: 0.4, options: .transitionCrossDissolve, animations: {
                            self.joinTourCreateTeamToSelectPlayer.isHidden = false
                        })
                        self.dismissViewHolder.isHidden = true
                    } else {
                        self.showAlert(title: "Oops", message: (response as! NSDictionary)["XSCMessage"] as! String)
                    }
                } else {
                    print("Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
                self.preventUserInteractionView.isHidden = true
            }
        }
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == joinTourSelectPlayersCollectionView{
            return teamList.count
        } else if collectionView == confirmPlayerCollectionView {
            return selectedPlayersList.count
        } else {
            return playerListMutable.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == joinTourSelectPlayersCollectionView {
            let selectTeamCell = collectionView.dequeueReusableCell(withReuseIdentifier: "joinTourCreateTeamsToSelectPlayersCollectionViewCell", for: indexPath) as! JoinTourCreateTeamsToSelectPlayersCollectionViewCell
            let team = teamList[indexPath.section] as! NSDictionary
            selectTeamCell.joinTourSelectTeamNamesLabel.text = team["TEAM_NAME"] as? String
            selectTeamCell.joinTourSelectPlayersImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(team["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            if selectedTeamsIDs.contains(team["TEAM_ID"] as! String) {
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
                selectTeamCell.joinTourSelectPlayersTickButton.isHidden=false
            } else{
                selectTeamCell.joinTourSelectPlayersTickButton.isHidden=true
            }
            return selectTeamCell
        } else if collectionView == confirmPlayerCollectionView {
            let player = selectedPlayersList[indexPath.section] as! NSDictionary
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "confirmPlayerCell", for: indexPath) as! ConfirmPlayersCollectionViewCell
            cell.playerProfileImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(player["IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "profile_pic_default"))
            cell.playerName.text = player["FULL_NAME"] as? String
            
            return cell
        } else {
            let player = playerListMutable[indexPath.section] as! NSDictionary
            let choosePlayersCell = collectionView.dequeueReusableCell(withReuseIdentifier: "joinTourChoosePlayersCollectionViewCell", for: indexPath) as! JoinTourChoosePlayersCollectionViewCell
            choosePlayersCell.joinTourChoosePlayerProfileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(player["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
            choosePlayersCell.joinTourPlayerNameLabel.text = player["FULL_NAME"] as? String
            choosePlayersCell.joinTourPlayerSkillsLabel.text = player["SKILLS"] as? String
            choosePlayersCell.joinTourPlayersTeamNameLabel.text = player["TEAM_NAME"] as? String
            
            if _selectedPlayers.contains(player["PLAYER_ID"] as! String) {
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
                choosePlayersCell.joinTourGreenTickImage.isHidden=false
            } else{
                choosePlayersCell.joinTourGreenTickImage.isHidden=true
            }
            
            return choosePlayersCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == joinTourSelectPlayersCollectionView {
            let team = teamList[indexPath.section] as! NSDictionary
            if selectedTeamsIDs.contains(team["TEAM_ID"] as! String) {
                selectedTeamsIDs = selectedTeamsIDs.filter { $0 != (team["TEAM_ID"] as! String) }
            } else {
                selectedTeamsIDs.append(team["TEAM_ID"] as! String)
            }
            selectedTeamsIDs = Array(Set(selectedTeamsIDs))
            collectionView.reloadItems(at: [indexPath])
            print(selectedTeamsIDs)
        }
        if collectionView == joinTourChoosePlayerCollectionView {
            let player = playerListMutable[indexPath.section] as! NSDictionary
            print(player)
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "joinTourChoosePlayersCollectionViewCell", for: indexPath) as! JoinTourChoosePlayersCollectionViewCell
            if _selectedPlayers.count < Int(selectedTournament!["PLAYERS_PER_TEAM"] as! String)! {
                if _selectedPlayers.contains(player["PLAYER_ID"] as! String) {
                    _selectedPlayers = _selectedPlayers.filter { $0 != (player["PLAYER_ID"] as! String) }
                } else {
                    _selectedPlayers.append(player["PLAYER_ID"] as! String)
                }
                _selectedPlayers = Array(Set(_selectedPlayers))
                collectionView.reloadItems(at: [indexPath])
                let remainingPlayers = Int(selectedTournament!["PLAYERS_PER_TEAM"] as! String)! - _selectedPlayers.count
                joinTourChoosePlayerSelectedLabel.text = "\(remainingPlayers) Remaining"
                print(_selectedPlayers)

            } else {
                if _selectedPlayers.contains(player["PLAYER_ID"] as! String) {
                    _selectedPlayers = _selectedPlayers.filter { $0 != (player["PLAYER_ID"] as! String) }
                    _selectedPlayers = Array(Set(_selectedPlayers))
                    collectionView.reloadItems(at: [indexPath])
                    let remainingPlayers = Int(selectedTournament!["PLAYERS_PER_TEAM"] as! String)! - _selectedPlayers.count
                    joinTourChoosePlayerSelectedLabel.text = "\(remainingPlayers) Remaining"
                } else {
                    self.joinTourChoosePlayersView.shake()
                    self.showAlert(message: "maximum number of players reached")
                }
            }
        }
    }
    
    @objc func clearAllTapFunction() {
        print("Clear All")
        print(_selectedPlayers)
        _selectedPlayers.removeAll()
        joinTourChoosePlayerCollectionView.reloadData()
        joinTourChoosePlayerSelectedLabel.text = "\(_selectedPlayers.count) Selected"
    }
    
    @IBAction func selectTeamNextButtonClick(_ sender: Any) {
        teams.removeAllObjects()
        if selectedTeamsIDs.count > 0 {
            var teamIDs = selectedTeamsIDs[0]
            for id in selectedTeamsIDs {
                
                if id != teamIDs {
                    teamIDs = "\(teamIDs),\(id)"
                }
                
                for team in teamList {
                    if (team as! NSDictionary)["TEAM_ID"] as! String == id {
                        teams.add(team)
                    }
                }
            }
            teamPicker?.reloadAllComponents()
            print(teamIDs)
            activityIndicator.startAnimating()
            preventUserInteractionView.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_ID\": \"\(teamIDs)\", \"TOURNAMENT_ID\":\"\(selectedTournament!["TOURNAMENT_ID"] as! String)\",\"IS_BATSMAN\":\"0\",\"IS_BOWLER\":\"0\",\"IS_ALL_ROUNDER\":\"0\",\"IS_KEEPER\":\"0\"}", mod: "Tournament", actionType: "tournament-team-players") { (response) in
                print(response)
                if response as? String != "error" {
                    
                    if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                        self.playerList = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["PLAYERS"] as! NSArray
                       
                        self.playerListMutable = self.playerList.mutableCopy() as! NSMutableArray
                        self.joinTourChoosePlayerCollectionView.reloadData()
                        
                        UIView.transition(with: self.joinTourChoosePlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                            self.joinTourChoosePlayersView.isHidden = false
                        })
                        UIView.transition(with: self.joinTourCreateTeamToSelectPlayer, duration: 0.4, options: .transitionCrossDissolve, animations: {
                            self.joinTourCreateTeamToSelectPlayer.isHidden = true
                        })
                    } else {
                        self.showAlert(title: "Oops", message: (response as! NSDictionary)["XSCMessage"] as! String)
                    }
                    
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
                self.preventUserInteractionView.isHidden = true
            }
        } else {
            self.joinTourCreateTeamToSelectPlayer.shake()
            self.showAlert(title: "Teams", message: "No teams selected, please select atleast one team")
        }
    }
    
    @IBAction func selectPlayerBackButtonClicked(_ sender: Any) {
        self.playerList = []
        self._selectedPlayers.removeAll()
        self.playerListMutable.removeAllObjects()
        self.selectedPlayersList.removeAllObjects()
        self.joinTourChoosePlayerSelectedLabel.text = "0 Selected"
        UIView.transition(with: self.joinTourChoosePlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.joinTourChoosePlayersView.isHidden = true
        })
        UIView.transition(with: self.joinTourCreateTeamToSelectPlayer, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.joinTourCreateTeamToSelectPlayer.isHidden = false
        })
    }
    
    @IBAction func playerSelectedContinueButtonClick(_ sender: Any) {
        selectedPlayersList.removeAllObjects()
        for player in _selectedPlayers {
            for play in playerList {
                if player == (play as! NSDictionary)["PLAYER_ID"] as? String {
                    selectedPlayersList.add(play)
                }
            }
        }
        print("Selected Players", selectedPlayersList.count ,"==", Int(selectedTournament!["PLAYERS_PER_TEAM"] as! String)!)
        
//        if selectedPlayersList.count < Int(selectedTournament!["PLAYERS_PER_TEAM"] as! String)! {
//            self.showAlert(title: "Requirement", message: "\(selectedTournament!["PLAYERS_PER_TEAM"] as! String) players is required to join this torunament")
//        } else {

        confirmPlayerCollectionView.reloadData()
            UIView.transition(with: self.joinTourChoosePlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.joinTourChoosePlayersView.isHidden = true
            })
            UIView.transition(with: self.confirmPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.confirmPlayersView.isHidden = false
            })
//        }
    }
    
    @IBAction func confirmBackButtonClick(_ sender: Any) {
        tournamentNametextField.text = ""
        selectedPlayersList.removeAllObjects()
        UIView.transition(with: self.joinTourChoosePlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.joinTourChoosePlayersView.isHidden = false
        })
        UIView.transition(with: self.confirmPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.confirmPlayersView.isHidden = true
        })
    }
    
    @IBAction func confirmPlayerContinueButtonClick(_ sender: Any) {
        if selectTeam != [:] {
            submitPlayerListForTournament(newTeam: 0, teamName: selectTeam["TEAM_NAME"] as! String, teamId: selectTeam["TEAM_ID"] as! String, teamIcon: selectTeam["TEAM_ICON"] as! String, locationID: selectTeam["LOCATION"] as! String)
        } else {
            if tournamentNametextField.isReallyEmpty {
                self.confirmPlayersView.shake()
                self.showAlert(title: "Missing Info", message: "Please enter or select your team name for the tournament")
            } else {
                submitPlayerListForTournament(newTeam: 1, teamName: tournamentNametextField.text!, teamId: "", teamIcon: (teams[0] as! NSDictionary)["TEAM_ICON"] as! String, locationID: (teams[0] as! NSDictionary)["LOCATION"] as! String)
            }
        }
    }
    
    @IBAction func paymentButtonClick(_ sender: Any) {
        
        UIView.transition(with: self.proceedToPaymentView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.proceedToPaymentView.isHidden = true
        })
        openPayUMoneyInterface()
    }
    
    func submitPlayerListForTournament(newTeam: Int, teamName: String, teamId: String, teamIcon: String, locationID: String) {
        self.activityIndicator.startAnimating()
        self.preventUserInteractionView.isHidden = false
        
        let playersChoosed:NSMutableArray = []
        
        for player in selectedPlayersList {
            let playerDetail = ["TEAM_ID":"","PLAYER_ID":(player as! NSDictionary)["PLAYER_ID"] as! String]
            playersChoosed.add(playerDetail)
        }
        do {
        let data = try JSONSerialization.data(withJSONObject: playersChoosed)
        let dataString = String(data: data, encoding: .utf8)!
        print(dataString)
        
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TOURNAMENT_ID\":\"\(selectedTournament!["TOURNAMENT_ID"] as! String)\",\"TEAM_ID\":\"\(teamId)\",\"NEW_TOURNAMENT_TEAM\":\"\(newTeam)\",\"PLAYERS\":\(dataString),\"TOURNAMENT_TEAM_ID\":\"\",\"TEAM_NAME\":\"\(teamName)\",\"TEAM_ICON\":\"\(teamIcon)\",\"LOCATION\":\"\(locationID)\"}", mod: "Tournament", actionType: "add-tournament-team-players") { (response) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.receivedTeam_ID = (response as! NSDictionary)["XSCData"] as? String ?? "\((response as! NSDictionary)["XSCData"] as! Int)"
                    UIView.transition(with: self.proceedToPaymentView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.proceedToPaymentView.isHidden = false
                        self.dismissViewHolder.isHidden = false
                    })
                    UIView.transition(with: self.confirmPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.confirmPlayersView.isHidden = true
                    })
                } else {
                    self.showAlert(title: "Oops", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteractionView.isHidden = true
        }
        } catch {
            print("JSON serialization failed: ", error)
        }
    }
    
    @IBAction func selectTeamBackButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openNearByTournament"), object: nil)
        })
    }
    
    func setCollectionViewLayout(card: UIView, collectionView: UICollectionView, center: Bool) {
        if center {
            let screensize = card.bounds.size
            let cellwidth = floor(screensize.width * 0.8)
            let cellheight = floor(collectionView.bounds.height * 0.9)
            
            let insetX = (card.bounds.width - cellwidth) / 2.0
            let insetY = (collectionView.bounds.height - cellheight) / 2.0
            
            let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.itemSize = CGSize(width: cellwidth, height: cellheight)
            collectionView.contentInset = UIEdgeInsetsMake(insetY, insetX, insetY, insetX)
        } else {
            let screensize1 = card.bounds.size
            let cellwidth1 = floor(screensize1.width/3)
            let cellheight1 = floor(collectionView.bounds.height)
            
            let layout1 = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout1.itemSize = CGSize(width: cellwidth1, height: cellheight1)
            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }
    }
    
    func openPayUMoneyInterface() {
        
        let txnParam = PUMTxnParam()

        //Set the parameters
        txnParam.phone = "9944640144"//UserDefaults.standard.object(forKey: CommonUtil.MOBILE_NUMBER) as! String
        txnParam.email = "karthik@xcelcorp.com"
        txnParam.amount = "\(self.selectedTournament!["ENTRANCE_FEE"] as! String)"
        txnParam.environment = .test
        txnParam.firstname = UserDefaults.standard.object(forKey: CommonUtil.FULL_NAME) as! String
        txnParam.key = CommonUtil.PayUMoney_KEY
        txnParam.merchantid = CommonUtil.PayUMoney_MERCHANT_ID
        txnParam.txnID = "\(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String)\(Int64(Date().timeIntervalSince1970 * 1000))"
        txnParam.surl = "http://www.cricdost.com/success.html"
        txnParam.furl = "http://www.cricdost.com/failed.html"
        txnParam.productInfo = "iPhone5C"
        txnParam.udf1 = "udf1"
        txnParam.udf2 = "udf2"
        txnParam.udf3 = "udf3"
        txnParam.udf4 = "udf4"
        txnParam.udf5 = "udf5"
        txnParam.udf6 = "udf6"
        txnParam.udf7 = "udf7"
        txnParam.udf8 = "udf8"
        txnParam.udf9 = "udf9"
        txnParam.udf10 = "udf10"
        let salt = CommonUtil.PayUMoney_SALT
        
        txnParam.hashValue = generateHash(txnParam, salt: salt)
        
        PlugNPlay.setDisableCompletionScreen(true)
        PlugNPlay.setButtonColor(CommonUtil.themeRed)
        PlugNPlay.setTopBarColor(CommonUtil.themeRed)
        PlugNPlay.setButtonTextColor(UIColor.white)
        PlugNPlay.setReturning(self)
        PlugNPlay.setTopTitleTextColor(UIColor.white)
        PlugNPlay.setMerchantDisplayName("Join Tournament")
        
        PlugNPlay.presentPaymentViewController(withTxnParams: txnParam, on: self, withCompletionBlock: { paymentResponse, error, extraParam in
            print("paymentResponse",paymentResponse)
            self.activityIndicator.startAnimating()
            if error != nil {
                CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_ID\":\"\(self.receivedTeam_ID)\",\"TOURNAMENT_ID\":\"\(self.selectedTournament!["TOURNAMENT_ID"] as! String)\"}", mod: "Tournament", actionType: "payment-cancel", callback: { (response) in
                    print(response)
                    if response as? String != "error" {
                        if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
//                            UIView.transition(with: self.proceedToPaymentView, duration: 0.4, options: .transitionCrossDissolve, animations: {
//                                self.proceedToPaymentView.isHidden = true
//                            })
//                            UIView.transition(with: self.confirmPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
//                                self.paymentFailedView.isHidden = false
//                            })
                                UIUtility.toastMessage(onScreen: error?.localizedDescription)
                            self.dismiss(animated: true, completion: nil)
//                            self.paymentFailedLabel.text = error?.localizedDescription
                        } else {
                            self.showAlert(title: "Oops", message: (response as! NSDictionary)["XSCMessage"] as! String)
                        }
                    } else {
                        self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                    }
                })
//                UIUtility.toastMessage(onScreen: error?.localizedDescription)
            } else {
                var message = ""
                if paymentResponse?["result"] != nil && (paymentResponse?["result"] is [AnyHashable : Any]) {
                    message = (paymentResponse?["result"] as! [AnyHashable : Any])["error_Message"] as? String ?? ""
                    if message.isEqual(NSNull()) || message.count == 0 || (message == "No Error") {
                        message = (paymentResponse?["result"] as! [AnyHashable : Any])["status"] as? String ?? ""
                    }
                } else {
                    message = paymentResponse?["status"] as? String ?? ""
                }
                if message == "success" {
                    CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_ID\":\"\(self.receivedTeam_ID)\",\"TOURNAMENT_ID\":\"\(self.selectedTournament!["TOURNAMENT_ID"] as! String)\",\"AMOUNT\":\"\(self.selectedTournament!["ENTRANCE_FEE"] as! String)\",\"PAYMENT_STATUS\":\"16\",\"TRANSACTION_ID\":\"\((paymentResponse?["result"] as! [AnyHashable : Any])["txnid"] as? String ?? "")\"}", mod: "Tournament", actionType: "update-tournament-payment", callback: { (response) in
                        print(response)
                        if response as? String != "error" {
                            if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                                UIView.transition(with: self.proceedToPaymentView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                                    self.proceedToPaymentView.isHidden = true
                                })
                                UIView.transition(with: self.confirmPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                                    self.paymentSuccessView.isHidden = false
                                })
                            } else {
                                self.showAlert(title: "Oops", message: (response as! NSDictionary)["XSCMessage"] as! String)
                            }
                        } else {
                            self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                        }
                    })
                } else {
                    CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_ID\":\"\(self.receivedTeam_ID)\",\"TOURNAMENT_ID\":\"\(self.selectedTournament!["TOURNAMENT_ID"] as! String)\",\"AMOUNT\":\"\(self.selectedTournament!["ENTRANCE_FEE"] as! String)\",\"PAYMENT_STATUS\":\"17\",\"TRANSACTION_ID\":\"\(txnParam.txnID)\"}", mod: "Tournament", actionType: "update-tournament-payment", callback: { (response) in
                        print(response)
                        if response as? String != "error" {
                            if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                                UIView.transition(with: self.proceedToPaymentView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                                    self.proceedToPaymentView.isHidden = true
                                })
                                UIView.transition(with: self.confirmPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                                    self.paymentFailedView.isHidden = false
                                })
                                //                    UIUtility.toastMessage(onScreen: message)
                                self.paymentFailedLabel.text = message
                            } else {
                                self.showAlert(title: "Oops", message: (response as! NSDictionary)["XSCMessage"] as! String)
                            }
                        } else {
                            self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                        }
                    })
                }
            }
            self.activityIndicator.stopAnimating()
        })
    }
    
    @IBAction func tryAgainButtonClick(_ sender: Any) {
        openPayUMoneyInterface()
    }
    
    @IBAction func paymentSuccessOkButtonClick(_ sender: Any) {
        let alert = UIAlertController(title: "Joined Tournament", message: "Your team have been added in tournament successfully", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            if self.isInTournamentDetailPage {
                    self.dismiss(animated: true, completion: {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getTournamentDetails"), object: nil)
                    })
            } else {
                self.dismiss(animated: true, completion: {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openNearByTournament"), object: nil)
                })
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func generateHash(_ txnParam: PUMTxnParam, salt: String) -> String {
        let hashSequence = "\(txnParam.key!)|\(txnParam.txnID!)|\(txnParam.amount!)|\(txnParam.productInfo!)|\(txnParam.firstname!)|\(txnParam.email!)|\(txnParam.udf1!)|\(txnParam.udf2!)|\(txnParam.udf3!)|\(txnParam.udf4!)|\(txnParam.udf5!)|\(txnParam.udf6!)|\(txnParam.udf7!)|\(txnParam.udf8!)|\(txnParam.udf9!)|\(txnParam.udf10!)|\(salt)"
        
        let hash = createSHA512(hashSequence).replacingOccurrences(of: "<", with: "").replacingOccurrences(of: ">", with: "").replacingOccurrences(of: " ", with: "")
        
        return hash
    }
    
    func createSHA512(_ string: String) -> String {
        var digest = [UInt8](repeating: 0, count: Int(CC_SHA512_DIGEST_LENGTH))
        if let data = string.data(using: String.Encoding.utf8) {
            let value =  data as NSData
            CC_SHA512(value.bytes, CC_LONG(data.count), &digest)
            
        }
        var digestHex = ""
        for index in 0..<Int(CC_SHA512_DIGEST_LENGTH) {
            digestHex += String(format: "%02x", digest[index])
        }
        
        return digestHex.replacingOccurrences(of: "<", with: "").replacingOccurrences(of: ">", with: "").replacingOccurrences(of: " ", with: "")
    }
    
}
