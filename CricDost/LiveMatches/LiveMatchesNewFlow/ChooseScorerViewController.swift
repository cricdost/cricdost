//
//  ChooseScorerViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 9/17/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ChooseScorerViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource, ShowsAlert {
    
    @IBOutlet weak var scorerViewHolder: UIView!
    @IBOutlet weak var scorerCollectionView: UICollectionView!
    @IBOutlet weak var scorerNextButton: UIButton!
    var playerlist: NSArray?
    var selected_Player = ""
    var striker: String?
    var nonStriker: String?
    var matchID: String?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var priventUserInteraction: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [scorerViewHolder])
        CommonUtil.buttonRoundedCorners(buttons: [scorerNextButton])
    }

    @IBAction func scorerNextButtonClick(_ sender: Any) {
        
        print("Scorer next button clicked")
        if selected_Player != "" {
            var scoreId = ""
            for player in playerlist! {
                let play = player as! NSDictionary
                if selected_Player == play["MATCH_TEAM_PLAYER_ID"] as? String ?? "\(play["MATCH_TEAM_PLAYER_ID"] as! Int)" {
                    scoreId = play["USER_ID"] as? String ?? "\(play["USER_ID"] as! Int)"
                }
            }
            activityIndicator.startAnimating()
            priventUserInteraction.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"SCORER\":\"\(scoreId)\",\"MATCH_ID\":\"\(matchID!)\"}", mod: "MatchScore", actionType: "change-scorer") { (response) in
                if response as? String != "error" {
                    print(response)
                    let data: [String: Any] = ["match_id": self.matchID!,
                                               "user_id": UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String,
                                               "step_name":"BAT_USER_ID",
                                               "won_team_id":"",
                                               "won_team_name": "",
                                               "bat_or_bowl":"",
                                               "won_team_object":""]
                    SocketConnectionsclass.matchStatusUpdate_NewFlow(param: data)
                    
                    self.dismiss(animated: true, completion: {
                        let playerList:[String: Any] = ["match_id":self.matchID!]
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openTeamScoreViewController"), object: nil, userInfo: playerList)
                    })
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
                self.priventUserInteraction.isHidden = true
            }
        } else {
            self.scorerViewHolder.shake()
            self.showAlert(title: "Scorer", message: "Please select a scorer")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return playerlist!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let player = playerlist![indexPath.row] as! NSDictionary
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseScorerCollectionViewCell", for: indexPath) as! ChooseScorerCollectionViewCell
        cell.scorerImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(player["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
        cell.scorerNameLabel.text = player["FULL_NAME"] as? String
        
        if selected_Player == player["MATCH_TEAM_PLAYER_ID"] as? String ?? "\(player["MATCH_TEAM_PLAYER_ID"] as! Int)" {
            cell.scorerTickButton.isHidden = false
        } else {
            cell.scorerTickButton.isHidden = true
        }
        
        if player["MATCH_TEAM_PLAYER_ID"] as? String ?? "\(player["MATCH_TEAM_PLAYER_ID"] as! Int)" == striker {
            cell.strikerNonStrikerImageView.isHidden = false
            cell.strikerNonStrikerImageView.setImage(#imageLiteral(resourceName: "bat_24"), for: .normal)
            cell.scorerTickButton.isHidden = true
        } else if player["MATCH_TEAM_PLAYER_ID"] as? String ?? "\(player["MATCH_TEAM_PLAYER_ID"] as! Int)" == nonStriker {
            cell.strikerNonStrikerImageView.isHidden = false
            cell.strikerNonStrikerImageView.setImage(#imageLiteral(resourceName: "grey-bat_24"), for: .normal)
            cell.scorerTickButton.isHidden = true
        } else {
            cell.strikerNonStrikerImageView.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let player = playerlist![indexPath.row] as! NSDictionary
        if player["MATCH_TEAM_PLAYER_ID"] as? String ?? "\(player["MATCH_TEAM_PLAYER_ID"] as! Int)" == striker {
            self.scorerViewHolder.shake()
            self.showAlert(title: "Scorer", message: "You can't select striker as scorer")
        } else if player["MATCH_TEAM_PLAYER_ID"] as? String ?? "\(player["MATCH_TEAM_PLAYER_ID"] as! Int)" == nonStriker {
            self.scorerViewHolder.shake()
            self.showAlert(title: "Scorer", message: "You can't select non-striker as scorer")
        } else {
            selected_Player = player["MATCH_TEAM_PLAYER_ID"] as? String ?? "\(player["MATCH_TEAM_PLAYER_ID"] as! Int)"
            collectionView.reloadData()
        }
    }
    
}










