//
//  SettingsPageViewController.swift
//  CricDost
//
//  Created by Jit Goel on 7/2/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class SettingsPageViewController: UIViewController, ShowsAlert {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var notificationSwitch: UISwitch!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var privacyView: UIView!
    @IBOutlet weak var preventUserInteraction: UIView!
    @IBOutlet weak var helpView: UIView!
    @IBOutlet weak var cricDostView: UIView!
    @IBOutlet weak var changeNumberView: UIView!
    @IBOutlet weak var deactivateView: UIView!
    @IBOutlet weak var signOutView: UIView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var alertVibrateImageView: UIImageView!
    @IBOutlet weak var alertVibrateSwitch: UISwitch!
    
    var IS_ALLOW_USER_TO_CALL = 1
    var IS_ALLOW_USER_TO_CHAT = 1
    var IS_PUSH_NOTIFICATION = 1
    var IS_VISIBLE = 1
    var IS_VISIBLE_PROFILE_PICTURE = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notificationSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        alertVibrateSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        phoneNumberLabel.text = UserDefaults.standard.object(forKey: CommonUtil.MOBILE_NUMBER) as? String
        
        NotificationCenter.default.addObserver(self, selector: #selector(getUserSettingsDetail), name: NSNotification.Name(rawValue: "getUserSettingsDetail"), object: nil)
        
        let privacyViewTaped = UITapGestureRecognizer(target: self, action: #selector(privacyViewTapFunction))
        privacyView.addGestureRecognizer(privacyViewTaped)
        
        let helpViewTaped = UITapGestureRecognizer(target: self, action: #selector(helpViewTapFuntion))
        helpView.addGestureRecognizer(helpViewTaped)
        
        let cricDostViewTaped = UITapGestureRecognizer(target: self, action: #selector(cricDostViewTapFuntion))
        cricDostView.addGestureRecognizer(cricDostViewTaped)
        
        let changeNumberViewTaped = UITapGestureRecognizer(target: self, action: #selector(changeNumberViewTapFuntion))
        changeNumberView.addGestureRecognizer(changeNumberViewTaped)
        
        let deactivateViewTaped = UITapGestureRecognizer(target: self, action: #selector(deactivateViewTapFuntion))
        deactivateView.addGestureRecognizer(deactivateViewTaped)
        
        let signOutViewTaped = UITapGestureRecognizer(target: self, action: #selector(signOutViewTapFuntion))
        signOutView.addGestureRecognizer(signOutViewTaped)
        
        let profileViewTaped = UITapGestureRecognizer(target: self, action: #selector(profileViewTapFuntion))
        profileView.addGestureRecognizer(profileViewTaped)
        
        alertVibrateImageView.image = #imageLiteral(resourceName: "baseline_vibration_black_24pt_1x").withRenderingMode(.alwaysTemplate)
        alertVibrateImageView.tintColor = UIColor.init(red: 0.56, green: 0.56, blue: 0.56, alpha: 1.0)
        if UserDefaults.standard.object(forKey: CommonUtil.ALERTVIBRATION) as? Bool ?? true {
            print("alert switch is on")
            self.alertVibrateSwitch.isOn = true
        } else {
            self.alertVibrateSwitch.isOn = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getUserSettingsDetail()
       
    }
    
    @objc func profileViewTapFuntion() {
        let changeNumberVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        changeNumberVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        changeNumberVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(changeNumberVC, animated: true, completion: nil)
    }
    
    @objc func signOutViewTapFuntion() {
        let alert = UIAlertController(title: "Sign Out", message: "Are you sure? you want to Sign Out?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            self.signOut()
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func deactivateViewTapFuntion() {
        let alert = UIAlertController(title: "De-Activate Account", message: "Are you sure? you want to de-activate your account?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            self.deActivateAccount()
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func changeNumberViewTapFuntion() {
        let changeNumberVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChangeNumberViewController") as! ChangeNumberViewController
        changeNumberVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        changeNumberVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(changeNumberVC, animated: true, completion: nil)
    }
    
    @objc func cricDostViewTapFuntion() {
        UIApplication.shared.open(NSURL(string:"http://www.cricdost.com/")! as URL)
    }
    
    @objc func helpViewTapFuntion() {
        UIApplication.shared.open(NSURL(string:"http://www.cricdost.com/blog/faq/")! as URL)
    }
    
    @objc func privacyViewTapFunction() {
        let privacyVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PrivacyViewController") as! PrivacyViewController
        privacyVC.IS_VISIBLE_PROFILE_PICTURE = IS_VISIBLE_PROFILE_PICTURE
        privacyVC.IS_VISIBLE = IS_VISIBLE
        privacyVC.IS_ALLOW_USER_TO_CHAT = IS_ALLOW_USER_TO_CHAT
        privacyVC.IS_ALLOW_USER_TO_CALL = IS_ALLOW_USER_TO_CALL
        self.navigationController?.pushViewController(privacyVC, animated: true)
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func notificationSwitchToggled(_ sender: Any) {
        if notificationSwitch.isOn {
            let alert = UIAlertController(title: "Notification", message: "Are you sure you want to turn ON push notification?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                self.updateNotificationSettings(actionType: "set-push-notification", flag: "1")
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
                if self.IS_PUSH_NOTIFICATION == 1 {
                    self.notificationSwitch.isOn = true
                } else {
                    self.notificationSwitch.isOn = false
                }
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Notification", message: "Are you sure you want to turn OFF push notification?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                self.updateNotificationSettings(actionType: "set-push-notification", flag: "0")
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
                if self.IS_PUSH_NOTIFICATION == 1 {
                    self.notificationSwitch.isOn = true
                } else {
                    self.notificationSwitch.isOn = false
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func alertVibrateSwitchToggled(_ sender: Any) {
        
        if self.alertVibrateSwitch.isOn {
            UserDefaults.standard.set(false, forKey: CommonUtil.ALERTVIBRATION)
            print("alert switch on")
            self.alertVibrateSwitch.isOn = false
        } else {
            print("alert switch offff")
            UserDefaults.standard.set(true, forKey: CommonUtil.ALERTVIBRATION)
            self.alertVibrateSwitch.isOn = true
        }
    }
    func deActivateAccount() {
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Settings", actionType: "deactivate-account") { (response) in
            if response as? String != "error" {
                print(response)
                UserDefaults.standard.set(true, forKey: "ONBOARD")
                self.performSegue(withIdentifier: "settings_splash", sender: self)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
        }
        activityIndicator.stopAnimating()
        preventUserInteraction.isHidden = true
    }
    
    func signOut() {
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "AA", actionType: "logout") { (response) in
            if response as? String != "error" {
                print(response)
                UserDefaults.standard.set(true, forKey: "ONBOARD")
                UserDefaults.standard.set(true, forKey: CommonUtil.NewNotification)
                UserDefaults.standard.set(true, forKey: CommonUtil.SHAKE)
                UserDefaults.standard.set(true, forKey: CommonUtil.SWIPEDEMO)
                self.performSegue(withIdentifier: "settings_splash", sender: self)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
        }
        activityIndicator.stopAnimating()
        preventUserInteraction.isHidden = true
    }
    
    func updateNotificationSettings(actionType: String, flag: String) {
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"FLAG\":\"\(flag)\"}", mod: "Settings", actionType: actionType) { (response) in
            if response as? String != "error" {
                print(response)
                self.showAlert(title: "Message", message: (response as! NSDictionary)["XSCMessage"] as! String)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        }
    }
    
    @objc func getUserSettingsDetail() {
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Settings", actionType: "get-settings") { (response) in
            if response as? String != "error" {
                let settings = (response as! NSDictionary)["XSCData"] as! NSDictionary
                self.IS_ALLOW_USER_TO_CALL = settings["IS_ALLOW_USER_TO_CALL"] as! Int
                self.IS_ALLOW_USER_TO_CHAT = settings["IS_ALLOW_USER_TO_CHAT"] as! Int
                self.IS_PUSH_NOTIFICATION = settings["IS_PUSH_NOTIFICATION"] as! Int
                self.IS_VISIBLE = settings["IS_VISIBLE"] as! Int
                self.IS_VISIBLE_PROFILE_PICTURE = settings["IS_VISIBLE_PROFILE_PICTURE"] as! Int
                self.phoneNumberLabel.text = UserDefaults.standard.object(forKey: CommonUtil.MOBILE_NUMBER) as? String
                
                if self.IS_PUSH_NOTIFICATION == 1 {
                    self.notificationSwitch.isOn = true
                } else {
                    self.notificationSwitch.isOn = false
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        }
    }
    
}
