//
//  ChooseABowlerCollectionViewCell.swift
//  CricDost
//
//  Created by JIT GOEL on 9/18/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ChooseABowlerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bowlerImageView: UIImageView!
    @IBOutlet weak var bowlerNameLabel: UILabel!
    @IBOutlet weak var bowlerTickButton: UIButton!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        CommonUtil.imageRoundedCorners(imageviews: [bowlerImageView])
        CommonUtil.buttonRoundedCorners(buttons: [bowlerTickButton])
    }
}
