//
//  JoinTeamViewController.swift
//  CricDost
//
//  Created by Jit Goel on 5/28/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import CoreLocation
import LTMorphingLabel

class JoinTeamViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, CLLocationManagerDelegate, ShowsAlert,LTMorphingLabelDelegate {
    
    @IBOutlet weak var teamsCollectionView: UICollectionView!
    @IBOutlet weak var createTeamButton: UIButton!
    @IBOutlet weak var teamProfileImage: UIImageView!
    @IBOutlet weak var teamProfileCard: UIView!
    @IBOutlet weak var numbPlayers: UILabel!
    @IBOutlet weak var teamAddress: UILabel!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var numbMatches: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var joinTeamButton: UIButton!
    @IBOutlet weak var loadingViewPreventUserInteraction: UIView!
    var team: NSDictionary?
    var manager: CLLocationManager?
    var latitude = 0.00
    var longitude = 0.00
    var locationSet = true
    var teamList: NSArray = []
    @IBOutlet weak var createTeamCard: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var joinYourDostTeamLabel: LTMorphingLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        joinYourDostTeamLabel.text = "..."
        CommonUtil.buttonRoundedCorners(buttons: [createTeamButton, joinTeamButton, cancelButton])
        CommonUtil.imageRoundedCorners(imageviews: [teamProfileImage])
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [createTeamCard,teamProfileCard])
        setLabelMorph(labels: [joinYourDostTeamLabel], effect: .evaporate)
        OperationQueue.main.addOperation{
            self.manager = CLLocationManager()
            self.manager?.delegate = self
            self.manager?.desiredAccuracy = kCLLocationAccuracyBest
            self.manager?.requestWhenInUseAuthorization()
            self.manager?.startUpdatingLocation()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        joinYourDostTeamLabel.text = "Join your dost's team"
    }
    
    func setLabelMorph(labels: [LTMorphingLabel], effect: LTMorphingEffect) {
        for label in labels {
            label.delegate = self
            label.morphingEffect = effect
            label.start()
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return teamList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TeamsCollectionViewCell", for: indexPath) as! TeamsCollectionViewCell
        let team = teamList[indexPath.section] as! NSDictionary
        cell.teamImage.layer.masksToBounds = false
        cell.teamImage.layer.cornerRadius = cell.teamImage.frame.height/2
        cell.teamImage.clipsToBounds = true
        cell.teamImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(team["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
        cell.teamDistance.text = "\(team["DISTANCE"] as! String) KM"
        cell.teamName.text = team["TEAM_NAME"] as? String
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        team = teamList[indexPath.section] as? NSDictionary
        activityIndicator.startAnimating()
        loadingViewPreventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_ID\": \"\(team!["TEAM_ID"] as! String)\"}", mod: "Team", actionType: "get-team-details", callback: {(response: Any) -> Void in
            if response as? String != "error" {
                let team = (response as! NSDictionary)["XSCData"] as! NSDictionary
                print(team)
                self.teamProfileImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(team["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
                self.teamName.text = team["TEAM_NAME"] as? String
                self.teamAddress.text = team["ADDRESS"] as? String
                self.numbMatches.text = team["MATCHES_COUNT"] as? String
                self.numbPlayers.text = team["PLAYERS_COUNT"] as? String
                
                UIView.transition(with: self.createTeamCard, duration: 0.4, options: .transitionCrossDissolve, animations: {
                    self.createTeamCard.isHidden = true
                }, completion: nil)
                UIView.transition(with: self.teamProfileCard, duration: 0.4, options: .transitionCrossDissolve, animations: {
                    self.teamProfileCard.isHidden = false
                }, completion: nil)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.loadingViewPreventUserInteraction.isHidden = true
        })
        
    }
    
    @IBAction func cancelButtonClick(_ sender: Any) {
        UIView.transition(with: self.createTeamCard, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.createTeamCard.isHidden = false
        }, completion: nil)
        UIView.transition(with: self.teamProfileCard, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.teamProfileCard.isHidden = true
        }, completion: nil)
    }
    
    @IBAction func skipButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "joinMatchVCFunction"), object: nil)
        })
    }
    
    @IBAction func createTeamButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "createTeamVCFunction"), object: nil)
        })
    }
    
    @IBAction func joinTeamButtonClick(_ sender: Any) {
        activityIndicator.startAnimating()
        loadingViewPreventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_ID\": \"\(team!["TEAM_ID"] as! String)\", \"REQUEST_TYPE\":\"Received\"}", mod: "Team", actionType: "send-request", callback: {
            (response: Any) -> Void in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.dismiss(animated: true, completion: {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "joinMatchVCFunction"), object: nil)
                    })
                    
                } else {
                    self.showAlert(title: "Oops", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
                
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.loadingViewPreventUserInteraction.isHidden = true
        })
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print(Int(scrollView.contentOffset.x) / Int(scrollView.frame.width))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.teamsCollectionView.frame.width/3, height: self.teamsCollectionView.frame.height)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.manager?.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            latitude = location.coordinate.latitude
            longitude = location.coordinate.longitude
            if (locationSet) {
                activityIndicator.startAnimating()
                loadingViewPreventUserInteraction.isHidden = false
                CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"LATITUDE\": \"\(latitude)\",\"LONGITUDE\":\"\(longitude)\"}", mod: "Team", actionType: "onboarding-team-list", callback:{ (response: Any) -> Void in
                    if response as? String != "error" {
                        let teams = (response as! NSDictionary)["XSCData"] as! NSArray
                        print(teams)
                        self.teamList = teams
                        self.teamsCollectionView.reloadData()
                    } else {
                        self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                        self.locationSet = true
                        self.manager?.requestLocation()
                    }
                    self.activityIndicator.stopAnimating()
                    self.loadingViewPreventUserInteraction.isHidden = true
                })
                self.locationSet = false
            }
        }
        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}
