//
//  FeedBackViewController.swift
//  CricDost
//
//  Created by Jit Goel on 7/2/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class FeedBackViewController: UIViewController, UITextFieldDelegate, ShowsAlert {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var feedbackTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        feedbackTextField.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendButtonClick(_ sender: Any) {
        if !feedbackTextField.isReallyEmpty {
            activityIndicator.startAnimating()
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"FEEDBACK\": \"\(feedbackTextField.text!)\"}", mod: "Settings", actionType: "submit-feedback") { (response) in
                if response as? String != "error" {
                    self.showAlert(title: "Feedback", message: (response as! NSDictionary)["XSCMessage"] as! String)
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
            }
        } else {
            showAlert(title: "Field Empty", message: "Please enter your feedback")
        }
    }
}
