//
//  SentMessageTableViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 7/18/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class SentMessageTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var curlImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        CommonUtil.imageRoundedCorners(imageviews: [profileImage])
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [viewHolder])
        
        profileImage.layer.borderWidth=1.5
        profileImage.layer.borderColor = CommonUtil.themeRed.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
