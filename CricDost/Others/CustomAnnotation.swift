//
//  CustomAnnotation.swift
//  CricDost
//
//  Created by Jit Goel on 6/1/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import MapKit

class CustomAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var address: String?
    var title: String?
    var subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
}
