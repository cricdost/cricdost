//
//  ChooseStrikerCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 6/28/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ChooseStrikerCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var greenTick: UIImageView!
    @IBOutlet weak var playerImage: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var strikerLabel: UILabel!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var status: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        CommonUtil.imageRoundedCorners(imageviews: [playerImage, greenTick])
    }
}
