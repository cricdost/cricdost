//
//  NewCreateMatchViewController.swift
//  CricDost
//
//  Created by Jit Goel on 10/5/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import IQKeyboardManager
import LTMorphingLabel
import Branch

class NewCreateMatchViewController: UIViewController, UITextFieldDelegate, LTMorphingLabelDelegate, ShowsAlert, UIPickerViewDelegate, UIPickerViewDataSource, UISearchBarDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIDropInteractionDelegate, UICollectionViewDropDelegate, UICollectionViewDragDelegate {
    
    var isGroundIdAvailable = false
    var isOnBoardUser = true
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var activityInidicator: UIActivityIndicatorView!
    
    //ToSubmitVariables
    var numberOfPlayer: String?
    var matchDate: String?
    var matchTime: String?
    var groundID: String?
    
    //Number of Players View
    @IBOutlet weak var numberOfPlayersView: UIView!
    @IBOutlet weak var numberOfPlayersTextField: UITextField!
    @IBOutlet weak var numberOfPlayerNextButton: UIButton!
    @IBOutlet weak var numberOfPlayerLabel: LTMorphingLabel!
    //confirm players
    var teamNamePicker = UIPickerView()
    @IBOutlet weak var dummyTextField: UITextField!
    
    //Select Players View
    @IBOutlet weak var selectPlayersView: UIView!
    @IBOutlet weak var playerTypeTextField: UITextField!
    var playerTypePicker = UIPickerView()
    @IBOutlet weak var dropDownImageView: UIImageView!
    let playerTypes: [String] = ["Choose players from CricDost       ","Choose players from MyTeam       "]
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var playerCollectionView: UICollectionView! {
        didSet {
            
            if #available(iOS 11.0, *) {
                playerCollectionView.dragDelegate = self
//                playerCollectionView.dragInteractionEnabled = true
            } else {
                // Fallback on earlier versions
            }
        }
    }
    @IBOutlet weak var clearAllLabel: UILabel!
    @IBOutlet weak var remainingLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var selectedPlayerBackButton: UIButton!
    var playerList: NSArray = []
    @IBOutlet weak var noPlayerFound: UILabel!
    var cricDostPlayers = true
    @IBOutlet weak var dropZone: UIView! {
        didSet {
            if #available(iOS 11.0, *) {
                dropZone.addInteraction(UIDropInteraction(delegate: self))
            } else {
                // Fallback on earlier versions
            }
        }
    }
    @IBOutlet weak var backgroundImageView: BackgroundImageView!
    var imageFetcher: ImageFetcher!
    var playerListMutable: NSMutableArray = []
    var selectedPlayersList: NSMutableArray = []
    var _selectedPlayers : [String] = []
    var _selectedPlayersTemp : [NSDictionary] = []
    var remainingPlayers = 0
    
    //Players image view
    @IBOutlet weak var player1: UIImageView!
    @IBOutlet weak var player2: UIImageView!
    @IBOutlet weak var player3: UIImageView!
    @IBOutlet weak var player4: UIImageView!
    @IBOutlet weak var player5: UIImageView!
    @IBOutlet weak var player6: UIImageView!
    @IBOutlet weak var player7: UIImageView!
    @IBOutlet weak var player8: UIImageView!
    @IBOutlet weak var player9: UIImageView!
    @IBOutlet weak var player10: UIImageView!
    @IBOutlet weak var player11: UIImageView!
    @IBOutlet weak var player12: UIImageView!
    @IBOutlet weak var player13: UIImageView!
    @IBOutlet weak var player14: UIImageView!
    @IBOutlet weak var player15: UIImageView!
    @IBOutlet weak var player16: UIImageView!
    var playerImageArray: [UIImageView] = []
    var playerNameArray: [UILabel] = []
    
    @IBOutlet weak var playerLabel1: UILabel!
    @IBOutlet weak var playerLabel2: UILabel!
    @IBOutlet weak var playerLabel3: UILabel!
    @IBOutlet weak var playerLabel4: UILabel!
    @IBOutlet weak var playerLabel5: UILabel!
    @IBOutlet weak var playerLabel6: UILabel!
    @IBOutlet weak var playerLabel7: UILabel!
    @IBOutlet weak var playerLabel8: UILabel!
    @IBOutlet weak var playerLabel9: UILabel!
    @IBOutlet weak var playerLabel10: UILabel!
    @IBOutlet weak var playerLabel11: UILabel!
    @IBOutlet weak var playerLabel12: UILabel!
    @IBOutlet weak var playerLabel13: UILabel!
    @IBOutlet weak var playerLabel14: UILabel!
    @IBOutlet weak var playerLabel15: UILabel!
    @IBOutlet weak var playerLabel16: UILabel!
    
    //When View
    @IBOutlet weak var whenView: UIView!
    @IBOutlet weak var whenTitleLabel: LTMorphingLabel!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var timeTextField: UITextField!
    @IBOutlet weak var whenNextButton: UIButton!
    var date_picker:UIDatePicker?
    var time_picker:UIDatePicker?
    
    //Location View
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var locationTitleLabel: LTMorphingLabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var locationLabelView: UIView!
    @IBOutlet weak var locationNextButton: UIButton!
    
    //Match Created View
    @IBOutlet weak var matchCreatedView: UIView!
    @IBOutlet weak var createMatchesImageView: UIImageView!
    @IBOutlet weak var yourMatchCreatedSuccessfullyLabel: LTMorphingLabel!
    @IBOutlet weak var inviteSentForYourTeamPlayersLabel: LTMorphingLabel!
    @IBOutlet weak var okButton: UIButton!
    
    //Confirm selected Players
    @IBOutlet weak var confirmSelectedPlayersView: UIView!
    @IBOutlet weak var confirmPlayersLabel: LTMorphingLabel!
    @IBOutlet weak var matchTeamNameTextField: UITextField!
    @IBOutlet weak var matchTeamImageView: UIImageView!
    @IBOutlet weak var matchTeamDropDownListButton: UIButton!
    @IBOutlet weak var confirmPlayersCollectionView: UICollectionView!
    @IBOutlet weak var confirmPlayersCreateButton: UIButton!
    @IBOutlet weak var confirmPlayersBackButton: UIButton!

    var teams:NSArray = []
    var teamId = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        IQKeyboardManager.shared().isEnabled = true
        
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [numberOfPlayersView,selectPlayersView, whenView, locationView,matchCreatedView,confirmSelectedPlayersView])
        CommonUtil.buttonRoundedCorners(buttons: [numberOfPlayerNextButton, whenNextButton,locationNextButton,okButton, continueButton,confirmPlayersCreateButton])
        CommonUtil.viewRoundedCorners(uiviews: [locationLabelView])
        
        setLabelMorph(labels: [numberOfPlayerLabel, whenTitleLabel,locationTitleLabel,yourMatchCreatedSuccessfullyLabel,inviteSentForYourTeamPlayersLabel,confirmPlayersLabel], effect: .evaporate)
        
        let clearAll = UITapGestureRecognizer(target: self, action: #selector(clearAllFunction))
        clearAllLabel.addGestureRecognizer(clearAll)
        
        let outerViewTap = UITapGestureRecognizer(target: self, action: #selector(outerViewFunction))
        dismissView.addGestureRecognizer(outerViewTap)
        
        playerImageArray = [player1,player2,player3,player4,player5,player6,player7,player8,player9,player10,player11,player12,player13,player14,player15]
        playerNameArray = [playerLabel1,playerLabel2,playerLabel3,playerLabel4,playerLabel5,playerLabel6,playerLabel7,playerLabel8,playerLabel9,playerLabel10,playerLabel11,playerLabel12,playerLabel13,playerLabel14,playerLabel15]
       
        CommonUtil.imageRoundedCorners(imageviews:[player1,player2,player3,player4,player5,player6,player7,player8,player9,player10,player11,player12,player13,player14,player15,player16,matchTeamImageView])
        
        for img in playerImageArray {
            img.layer.shadowRadius = 3
            img.layer.shadowOpacity = 0.3
            img.layer.shadowOffset = CGSize(width: 2, height: 2)
        }
        
        dismissView.isHidden = isOnBoardUser
        
        let now = Date()
        date_picker = UIDatePicker()
        date_picker?.minimumDate = now
        dateTextField.inputView = date_picker
        dateTextField.tintColor = .clear
        date_picker?.addTarget(self, action: #selector(handleDatePicker), for: UIControlEvents.valueChanged)
        date_picker?.datePickerMode = .date
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        dateTextField.text = formatter.string(from: now)
        
        time_picker = UIDatePicker()
        time_picker?.minimumDate = now.addingTimeInterval(30.0 * 60.0)
        timeTextField.inputView = time_picker
        timeTextField.tintColor = .clear
        time_picker?.addTarget(self, action: #selector(handleTimePicker), for: UIControlEvents.valueChanged)
        time_picker?.datePickerMode = .time
        
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "hh:mm a"
        timeTextField.text = formatter1.string(from: now.addingTimeInterval(30.0 * 60.0))
        
        let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
        dateTextField.inputAccessoryView = toolBar
        timeTextField.inputAccessoryView = toolBar
        
        let tourLocationGesture = UITapGestureRecognizer(target: self, action: #selector(locationLabelTapped))
        locationLabel.isUserInteractionEnabled = true
        locationLabel.addGestureRecognizer(tourLocationGesture)
        
        NotificationCenter.default.addObserver(self, selector: #selector(setLocationForCreateMatchFromExistingGround), name: NSNotification.Name(rawValue: "setLocationForCreateMatchFromExistingGround"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(gotoSelectFromMap), name: NSNotification.Name(rawValue: "gotoSelectFromMap"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setLocationForTournamentHost), name: NSNotification.Name(rawValue: "setLocationForTournamentHost"), object: nil)
        
        getMyTeams()
        getCricDostPlayers(searchKey: "")
        
        dropDownImageView.image = #imageLiteral(resourceName: "baseline_arrow_drop_down_white_24pt_1x.png").withRenderingMode(.alwaysTemplate)
        dropDownImageView.tintColor = UIColor.black
        
        playerTypePicker.delegate = self
        playerTypePicker.dataSource = self
        playerTypeTextField.inputView = playerTypePicker
        
        teamNamePicker.delegate = self
        teamNamePicker.dataSource = self
        dummyTextField.inputView = teamNamePicker
        
        for view in searchBar.subviews {
            for subview in view.subviews {
                if subview.isKind(of: UITextField.self) {
                    let textField: UITextField = subview as! UITextField
                    textField.backgroundColor = UIColor(red:0.92, green:0.93, blue:0.96, alpha:1.0)
                    textField.textColor = UIColor.black
                }
            }
        }
        UIBarButtonItem.appearance(whenContainedInInstancesOf:[UISearchBar.self]).tintColor = UIColor.black
        searchBar.backgroundImage = UIImage()
        
        setTapGestureForImageView()
        
        player16.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(UserDefaults.standard.object(forKey: CommonUtil.IMAGE_URL) as! String)"), placeholderImage: #imageLiteral(resourceName: "profile_pic_default"))
        playerLabel16.text = "You"
    }
    
    @IBAction func selectedPlayerBackButtonClick(_ sender: Any) {
        _selectedPlayers.removeAll()
        _selectedPlayersTemp.removeAll()
        selectedPlayersList.removeAllObjects()
        playerListMutable = self.playerList.mutableCopy() as! NSMutableArray
        playerCollectionView.reloadData()
        for selectedPlayers in playerImageArray {
            selectedPlayers.image = nil
        }
        for player in playerNameArray {
            player.text = ""
        }
        remainingLabel.text = "\(numberOfPlayer!) Remaining"
        
        UIView.transition(with: self.numberOfPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.numberOfPlayersView.isHidden = false
        })
        UIView.transition(with: self.selectPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.selectPlayersView.isHidden = true
        })
    }
    
    @IBAction func confirmPlayerBackButton(_ sender: Any) {
        matchTeamNameTextField.text = ""
        UIView.transition(with: self.selectPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.selectPlayersView.isHidden = false
        })
        UIView.transition(with: self.confirmSelectedPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.confirmSelectedPlayersView.isHidden = true
        })
    }
    @objc func clearAllFunction() {
        print("clearAll")
        _selectedPlayers.removeAll()
        _selectedPlayersTemp.removeAll()
        selectedPlayersList.removeAllObjects()
        playerListMutable = self.playerList.mutableCopy() as! NSMutableArray
        playerCollectionView.reloadData()
        for selectedPlayers in playerImageArray {
            selectedPlayers.image = nil
        }
        for player in playerNameArray {
            player.text = ""
        }
        remainingLabel.text = "\(numberOfPlayer!) Remaining"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        UserDefaults.standard.set(true, forKey: CommonUtil.GROUNDSELECTEDLOCATION)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        whenTitleLabel.text = "When?"
        bottomView.roundCornersView([.bottomLeft,.bottomRight], radius: 5)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        if cricDostPlayers {
            getCricDostPlayers(searchKey: searchText)
        } else {
            getMyTeamsPlayers(searchKey: searchText)
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        if cricDostPlayers {
            getCricDostPlayers(searchKey: searchBar.text ?? "")
        } else {
            getMyTeamsPlayers(searchKey: searchBar.text ?? "")
        }
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if cricDostPlayers {
            getCricDostPlayers(searchKey: searchBar.text ?? "")
        } else {
            getMyTeamsPlayers(searchKey: searchBar.text ?? "")
        }
        searchBar.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == numberOfPlayersTextField {
            let maxLength = 2
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else if textField == matchTeamNameTextField {
            teamId = ""
            return true
        } else {
            
            return true
        }
    }
    
    // Number of player functions
    @IBAction func numberOfPlayerNextButton(_ sender: Any) {
        if numberOfPlayersTextField.isReallyEmpty {
            self.numberOfPlayersView.shake()
            self.showAlert(title: "Number of players", message: "Please enter the number of player in match")
        } else {
            if Int(numberOfPlayersTextField.text!)! >= 3 && Int(numberOfPlayersTextField.text!)! <= 16 {
                numberOfPlayer = "\(Int(numberOfPlayersTextField.text!)! - 1)"
                UIView.transition(with: self.numberOfPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                    self.numberOfPlayersView.isHidden = true
                })
                UIView.transition(with: self.selectPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                    self.selectPlayersView.isHidden = false
                }, completion: { (bool) in
//                    self.selectPlayerTitleLabel.text = "Select Players"
                    self.remainingLabel.text = "\(self.numberOfPlayer!) Remaining"
                })
                if self.playerList.count > 0 {
                    var x = 0
                    repeat {
                        if self._selectedPlayers.contains((self.playerList[x] as! NSDictionary)["PLAYER_ID"] as! String) || (self.playerList[x] as! NSDictionary)["USER_ID"] as! String == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String {
                            self.playerListMutable.remove(self.playerList[x])
                            
                            print(self._selectedPlayers,"removed",(self.playerList[x] as! NSDictionary)["PLAYER_ID"] as! String)
                        }
                        x = x + 1
                    } while(x < self.playerList.count)
                }
                self.playerCollectionView.reloadData()
                if self.playerListMutable.count > 0 {
                    self.noPlayerFound.isHidden = true
                } else {
                    self.noPlayerFound.isHidden = false
                }
            } else {
                self.numberOfPlayersView.shake()
                self.showAlert(title: "Number of players", message: "Maximum number of player is 16 and minimum is 3 players for a match")
            }
        }
    }
    
    //Select Players Function
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == playerTypePicker {
            return 2
        }else if pickerView == teamNamePicker {
            return teams.count
        }
        else {
           return 0
        }
       
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == playerTypePicker {
            let text = playerTypes[row]
            return text
        } else if pickerView == teamNamePicker {
            return (teams[row] as! NSDictionary)["TEAM_NAME"] as? String
        } else {
            return "error"
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == playerTypePicker {
            let country = playerTypes[row]
            playerTypeTextField.text = country
            searchBar.text = ""
            if row == 0 {
                cricDostPlayers = true
                getCricDostPlayers(searchKey: "")
            } else {
                cricDostPlayers = false
                getMyTeamsPlayers(searchKey: "")
            }
        } else {
           
            matchTeamNameTextField.text = (teams[row] as! NSDictionary)["TEAM_NAME"] as? String
           let selectTeams = teams[row] as! NSDictionary
            teamId = selectTeams["TEAM_ID"] as? String ?? "\(selectTeams["TEAM_ID"] as! Int)"
            matchTeamImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\((selectTeams)["IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "no_team_image_icon.jpg"))
            
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == playerCollectionView {
            return playerListMutable.count
        } else {
             return selectedPlayersList.count + 1
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == playerCollectionView {
            let player = playerListMutable[indexPath.section] as! NSDictionary
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "playerCreateMatchCell", for: indexPath) as! NewCreateMatchPlayerCollectionViewCell
            cell.playerImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(player["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
            cell.playerNameLabel.text = player["FULL_NAME"] as? String
            cell.playerSkillLabel.text = player["SKILLS"] as? String
            return cell
        } else {
            let confirmCell = collectionView.dequeueReusableCell(withReuseIdentifier: "matchConfirmPlayersCollectionViewCell", for: indexPath) as! MatchConfirmPlayersCollectionViewCell
            if indexPath.section == selectedPlayersList.count {
                confirmCell.playersProfileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(UserDefaults.standard.object(forKey: CommonUtil.IMAGE_URL) as! String)"), placeholderImage: #imageLiteral(resourceName: "profile_pic_default"))
                confirmCell.playersNameLabel.text = UserDefaults.standard.object(forKey: CommonUtil.FULL_NAME) as! String
            } else {
                let player = selectedPlayersList[indexPath.section] as! NSDictionary
                
                confirmCell.playersProfileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(player["IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "profile_pic_default"))
                confirmCell.playersNameLabel.text = player["FULL_NAME"] as? String
            }
            return confirmCell
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == playerCollectionView {
            let player = playerListMutable[indexPath.section] as! NSDictionary
            
            if _selectedPlayers.count < Int(numberOfPlayer ?? "0") ?? 0 {
                if _selectedPlayers.contains(player["PLAYER_ID"] as! String) {
                    print("already selected")
//                    _selectedPlayers =  _selectedPlayers.filter { $0 != (player["PLAYER_ID"] as! String) }
                } else {
                    print("selecting")
                    for selecteredPlayer in playerImageArray {
                        if selecteredPlayer.image == nil {
                            selecteredPlayer.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(player["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
                            selecteredPlayer.tag = Int(player["PLAYER_ID"] as! String) ?? 0
                            break
                        }
                    }
                    for plyr in playerNameArray {
                        if plyr.text == "" {
                            plyr.text = player["FULL_NAME"] as? String
                            break
                        }
                    }
                    _selectedPlayers.append(player["PLAYER_ID"] as! String)
                    _selectedPlayersTemp.append(player)
                }
                _selectedPlayers = Array(Set(_selectedPlayers))
                _selectedPlayersTemp = Array(Set(_selectedPlayersTemp))
                
                remainingPlayers = Int(numberOfPlayer ?? "0")! - _selectedPlayers.count
                remainingLabel.text = "\(remainingPlayers) Remaining"
                print("Selected Players",_selectedPlayers)
                playerListMutable.removeObject(at: indexPath.section)
                playerCollectionView.reloadData()
            } else {
                self.showAlert(title: "Players", message: "Maximum number of player reached")
            }
            
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.playerCollectionView.frame.width/4, height: self.playerCollectionView.frame.height - 10)
    }
    
    @available(iOS 11.0, *)
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        session.localContext = collectionView
        return dragItems(at: indexPath)
    }
    
    
    @available(iOS 11.0, *)
    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem] {
        return dragItems(at: indexPath)
    }
    
    @available(iOS 11.0, *)
    private func dragItems(at indexPath: IndexPath) -> [UIDragItem] {
        if let dragImage = (playerCollectionView.cellForItem(at: indexPath) as? NewCreateMatchPlayerCollectionViewCell)?.playerImageView.image {
            let dragItem = UIDragItem(itemProvider: NSItemProvider(object: dragImage as NSItemProviderWriting))
            dragItem.localObject = dragImage
           return [dragItem]
        } else {
            return []
        }
    }
    
    
    @available(iOS 11.0, *)
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        print("drooooppppp")
        
        let destinationIndexPath = coordinator.destinationIndexPath ?? IndexPath(item: 0, section: 0)
        for item in coordinator.items {
            if let sourceIndexPath = item.sourceIndexPath {
                if let image = item.dragItem.localObject as? UIImage {
                    collectionView.performBatchUpdates({
//                        imageList.remove(at: sourceIndexPath.item)
//                        imageList.insert(image, at: destinationIndexPath.item)
                        collectionView.deleteItems(at: [sourceIndexPath])
                        collectionView.insertItems(at: [destinationIndexPath])
                    })
                    coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
                }
                
            }
        }
    }
    
    @available(iOS 11.0, *)
    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: UIImage.self)
    }
    @available(iOS 11.0, *)
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: NSURL.self) && session.canLoadObjects(ofClass: UIImage.self)
    }
    @available(iOS 11.0, *)
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        let isself = (session.localDragSession?.localContext as? UICollectionView) == collectionView
        return UICollectionViewDropProposal(operation: isself ? .move : .copy, intent: .insertAtDestinationIndexPath)
    }
    
    
    @available(iOS 11.0, *)
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        return UIDropProposal(operation: .copy)
    }
    
    @available(iOS 11.0, *)
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        print("Droooopppingljkhfkjd")
        
        
        imageFetcher = ImageFetcher() { (url,image) in
            DispatchQueue.main.async {
                self.backgroundImageView.backgroundImage = image
            }
        }
        session.loadObjects(ofClass: NSURL.self) { (nsurls) in
            if let url = nsurls.first as? URL {
                self.imageFetcher.fetch(url)
            }
        }
        session.loadObjects(ofClass: UIImage.self) { (images) in
            if let image = images.first as? UIImage {
                self.imageFetcher.backup = image
            }
            
        }
    }
    
    @IBAction func selectPlayerNextButtonClick(_ sender: Any) {
        print("selected player list", _selectedPlayers)
        selectedPlayersList.removeAllObjects()
        for player in _selectedPlayers {
            for plays in _selectedPlayersTemp {
                if player == (plays as! NSDictionary)["PLAYER_ID"] as? String ?? "\((plays as! NSDictionary)["PLAYER_ID"] as! Int)" {
                    print("selected",plays)
                    selectedPlayersList.add(plays)
                }
                
            }
        }
        confirmPlayersCollectionView.reloadData()
        print("selectedpleyerlistksjdhf",selectedPlayersList)
//        if selectedPlayersList.count < Int(numberOfPlayer ?? "0")! {
//            self.selectPlayersView.shake()
//            self.showAlert(title: "Requirement", message: "\(remainingLabel.text!) players is required to create this match")
//        } else {
        UIView.transition(with: self.selectPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.selectPlayersView.isHidden = true
        })
        UIView.transition(with: self.confirmSelectedPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.confirmSelectedPlayersView.isHidden = false
        }, completion: { (bool) in
              self.confirmPlayersLabel.text = "Confirm selected Players"
        })
//        }
    }
    
    @IBAction func dropDownButtonClick(_ sender: Any) {
        print("drop down clicked")
        if teams.count == 0 {
            self.showAlert(title: "No teams", message: "You don't have teams to select")
        } else {
            dummyTextField.becomeFirstResponder()
        }
    }
    
    
    //When View Functions
    @objc func dismissPicker() {
        view.endEditing(true)
    }
    
    @objc func handleDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateTextField.text = dateFormatter.string(from: (date_picker?.date)!)
    }
    
    @objc func handleTimePicker() {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        timeTextField.text = formatter.string(from: (time_picker?.date)!)
    }
    
    @IBAction func whenNextButtonClickEvent(_ sender: Any) {
        if dateTextField.text == "DD/MM/YY" || timeTextField.text == "00:00 am" {
            self.whenView.shake()
            self.showAlert(title: "Invalid info", message: "Please set a date and time")
        } else {
            matchDate = dateTextField.text!
            matchTime = timeTextField.text!
            UIView.transition(with: self.locationView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.locationView.isHidden = false
            })
            UIView.transition(with: self.whenView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.whenView.isHidden = true
            }, completion: { (bool) in
                self.locationTitleLabel.text = "Where?"
            })
            print("DATE",matchDate!, "TIME",matchTime!)
        }
    }
    
    
    //Location View Function
    @objc func locationLabelTapped(){
        let groundVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChooseGroundViewController") as? ChooseGroundViewController
        groundVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        groundVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(groundVC!, animated: true, completion: nil)
    }
    
    @objc func setLocationForCreateMatchFromExistingGround() {
        print("Setting location from existing ground")
        let ground = UserDefaults.standard.object(forKey: CommonUtil.TEMPGROUNDSELECTED) as? NSDictionary ?? [:]
        self.locationLabel.text = ground["ADDRESS"] as? String
        self.groundID = ground["GROUND_ID"] as? String
        self.locationLabelView.backgroundColor = UIColor.white
        isGroundIdAvailable = true
    }
    
    @objc func setLocationForTournamentHost() {
        print("Setting location on view")
        isGroundIdAvailable = false
        groundID = ""
        getAddressFromLatLon(pdblLatitude: UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double, withLongitude: UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double)
    }
    
    @objc func gotoSelectFromMap() {
        self.performSegue(withIdentifier: "createMatchNew_LocationPicker", sender: self)
    }
    
    @IBAction func locationNextButtonClick(_ sender: Any) {
        if locationLabel.text == "Enter the place" {
            self.locationView.shake()
            self.showAlert(title: "Missing Value", message: "Please select the match location")
        } else {
            UIView.transition(with: self.locationView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.locationView.isHidden = true
            })
            UIView.transition(with: self.numberOfPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.numberOfPlayersView.isHidden = false
            }, completion: { (bool) in
                self.numberOfPlayerLabel.text = "Number of players"
            })
        }
    }
    @IBAction func createButtonClick(_ sender: Any) {
        if matchTeamNameTextField.isReallyEmpty {
            self.confirmSelectedPlayersView.shake()
            self.showAlert(title: "Missing Info", message: "Please enter the team name")
        } else {
            var subAction = ""
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let myDate = dateFormatter.date(from: matchDate!)!
            dateFormatter.dateFormat = "yyyy/MM/dd"
            matchDate = dateFormatter.string(from: myDate)
            if isGroundIdAvailable {
                subAction = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_ID\":\"\(teamId)\",\"MATCH_DATE\":\"\(matchDate!)\",\"MATCH_TIME\":\"\(matchTime!)\",\"LATITUDE\":\"\",\"LONGITUDE\":\"\",\"ADDRESS\":\"\(locationLabel.text!)\",\"TOTAL_PLAYERS\":\"\(Int(numberOfPlayer!)! + 1)\",\"BALL_TYPE\":\"Soft Tennis ball\",\"PLAYERS\":\(_selectedPlayers),\"TEAM_NAME\":\"\(matchTeamNameTextField.text!)\",\"GROUND_ID\":\"\(groundID!)\"}"
            } else {
                subAction = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_ID\":\"\(teamId)\",\"MATCH_DATE\":\"\(matchDate!)\",\"MATCH_TIME\":\"\(matchTime!)\",\"LATITUDE\":\"\(UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double)\",\"LONGITUDE\":\"\(UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double)\",\"ADDRESS\":\"\(locationLabel.text!)\",\"TOTAL_PLAYERS\":\"\(Int(numberOfPlayer!)! + 1)\",\"BALL_TYPE\":\"Soft Tennis ball\",\"PLAYERS\":\(_selectedPlayers),\"TEAM_NAME\":\"\(matchTeamNameTextField.text!)\",\"GROUND_ID\":\"\"}"
            }
            activityInidicator.startAnimating()
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: subAction , mod: "Match", actionType: "create-match-new") { (response) in
                print(response)
                if response as? String != "error"  {
                    if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                        print(response)
                        UIView.transition(with: self.confirmSelectedPlayersView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                            self.confirmSelectedPlayersView.isHidden = true
                        })
                        UIView.transition(with: self.matchCreatedView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                            self.matchCreatedView.isHidden = false
                        }, completion: { (bool) in
                            CommonUtil.animateImage(image: self.createMatchesImageView)
                            self.yourMatchCreatedSuccessfullyLabel.text = "Your match created successfully"
                            self.inviteSentForYourTeamPlayersLabel.text = "Invite sent for your team players"
                            Branch.getInstance()?.userCompletedAction("match_created")
                        })
                    } else {
                        self.showAlert(title: "", message: (response as! NSDictionary)["XSCMessage"] as! String)
                    }
                } else {
                    self.showAlert(title: "Oops", message: "Something went wrong")
                }
                self.activityInidicator.stopAnimating()
                }
            }
       
    }
    
    //Match Created Button
    @IBAction func okBUttonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshMapView"), object: nil)
        })
    }
    
    
    //API CAllS and Other functions
    func getMyTeamsPlayers(searchKey: String) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_NAME\":\"\(searchKey)\"}", mod: "Match", actionType: "create-match-team-list-new") { (response: Any) in
            if response as? String != "error" {
                let players = (response as! NSDictionary)["XSCData"] as! NSDictionary
                print(players)
                self.playerList = players["PLAYERS"] as! NSArray
                self.playerListMutable = self.playerList.mutableCopy() as! NSMutableArray
                if self.playerList.count > 0 {
                    var x = 0
                    repeat {
                        if self._selectedPlayers.contains((self.playerList[x] as! NSDictionary)["PLAYER_ID"] as! String) || (self.playerList[x] as! NSDictionary)["USER_ID"] as! String == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String {
                            self.playerListMutable.remove(self.playerList[x])
                            
                            print(self._selectedPlayers,"removed",(self.playerList[x] as! NSDictionary)["PLAYER_ID"] as! String)
                        }
                        x = x + 1
                    } while(x < self.playerList.count)
                }
                self.playerCollectionView.reloadData()
                if self.playerListMutable.count > 0 {
                    self.noPlayerFound.isHidden = true
                } else {
                    self.noPlayerFound.isHidden = false
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
        }
    }
    
    func getCricDostPlayers(searchKey: String) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_NAME\":\"\(searchKey)\",\"USER_ID\":\"\"}", mod: "Match", actionType: "get-other-players") { (response: Any) in
            if response as? String != "error" {
                let players = (response as! NSDictionary)["XSCData"] as! NSDictionary
                print(players)
                self.playerList = players["PLAYERS"] as! NSArray
                self.playerListMutable = self.playerList.mutableCopy() as! NSMutableArray
                
                if self.playerList.count > 0 {
                    var x = 0
                    repeat {
                        if self._selectedPlayers.contains((self.playerList[x] as! NSDictionary)["PLAYER_ID"] as! String) || (self.playerList[x] as! NSDictionary)["USER_ID"] as! String == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String {
                            self.playerListMutable.remove(self.playerList[x])
                            print(self._selectedPlayers,"removed",(self.playerList[x] as! NSDictionary)["PLAYER_ID"] as! String)
                        }
                        x = x + 1
                    } while(x < self.playerList.count)
                }
                self.playerCollectionView.reloadData()
                if self.playerListMutable.count > 0 {
                    self.noPlayerFound.isHidden = true
                } else {
                    self.noPlayerFound.isHidden = false
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
        }
    }
    
    func getMyTeams(){
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Match", actionType: "create-match-team-list") { (response: Any) in
            if response as? String != "error" {
                print("team name",response)
                self.teams = (response as! NSDictionary)["XSCData"] as! NSArray
                print(self.teams)
                
//                if self.teams.count == 0 {
//                    self.whenView.shake()
//                    let alert = UIAlertController(title: "No Teams", message: "You don't own a team. Create a team now?", preferredStyle: UIAlertControllerStyle.alert)
//                    alert.addAction(UIAlertAction(title: "Create", style: UIAlertActionStyle.default, handler: { action in
//                        self.dismiss(animated: true, completion: {
//                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "createTeamVCFunction"), object: nil)
//                        })
//                    }))
//                    self.present(alert, animated: true, completion: nil)
//                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
        }
    }
    
    @objc func outerViewFunction(sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setLabelMorph(labels: [LTMorphingLabel], effect: LTMorphingEffect) {
        for label in labels {
            label.delegate = self
            label.morphingEffect = effect
            label.start()
        }
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
        let lat: Double = pdblLatitude
        
        let lon: Double = pdblLongitude
        print(lat,lon)
        
        CommonUtil.getAddressForLatLng(viewcontroller: self, latitude: String(lat), longitude: String(lon), callback: {
            (address: String) -> Void in
            print("GEOCODING \(address)")
            UIView.transition(with: self.locationLabel, duration: 0.4, options: .curveEaseOut, animations: {
                self.locationLabel.text = address
                self.locationLabelView.backgroundColor = UIColor.white
            }, completion: nil)
            self.activityInidicator.startAnimating()
            
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"ADDRESS\":\"\(self.removeSpecialCharsFromString(text: address))\",\"LATITUDE\":\"\(lat)\",\"LONGITUDE\":\"\(lon)\",\"ITEMS_PER_PAGE\":\"5\",\"USER_ID\":\"\(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String)\",\"GROUND_NAME\":\"\",\"GROUND_DESCRIPTION\":\"\",\"IS_PAID\":\"\"}", mod: "Ground", actionType: "add-ground", callback: { (response) in
                if response as? String != "error" {
                    if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                        print(response)
                        self.groundID = (response as! NSDictionary)["XSCData"] as? String ?? "\((response as! NSDictionary)["XSCData"] as! Int)"
                        self.locationNextButton.isEnabled = true
                    } else {
//                        self.showAlert(title: "", message: (response as! NSDictionary)["XSCMessage"] as! String)
                    }
                } else {
                    self.showAlert(title: "Oops", message: "Something went wrong")
                }
                self.activityInidicator.stopAnimating()
            })
            
        })
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_")
        return String(text.filter {okayChars.contains($0) })
    }
    
    func setTapGestureForImageView() {
        let player1Tap = UITapGestureRecognizer(target: self, action: #selector(player1TapFunction))
        player1.isUserInteractionEnabled = true
        player1.addGestureRecognizer(player1Tap)
        
        let player2Tap = UITapGestureRecognizer(target: self, action: #selector(player2TapFunction))
        player2.isUserInteractionEnabled = true
        player2.addGestureRecognizer(player2Tap)
        
        let player3Tap = UITapGestureRecognizer(target: self, action: #selector(player3TapFunction))
        player3.isUserInteractionEnabled = true
        player3.addGestureRecognizer(player3Tap)
        
        let player4Tap = UITapGestureRecognizer(target: self, action: #selector(player4TapFunction))
        player4.isUserInteractionEnabled = true
        player4.addGestureRecognizer(player4Tap)
        
        let player5Tap = UITapGestureRecognizer(target: self, action: #selector(player5TapFunction))
        player5.isUserInteractionEnabled = true
        player5.addGestureRecognizer(player5Tap)
        
        let player6Tap = UITapGestureRecognizer(target: self, action: #selector(player6TapFunction))
        player6.isUserInteractionEnabled = true
        player6.addGestureRecognizer(player6Tap)
        
        let player7Tap = UITapGestureRecognizer(target: self, action: #selector(player7TapFunction))
        player7.isUserInteractionEnabled = true
        player7.addGestureRecognizer(player7Tap)
        
        let player8Tap = UITapGestureRecognizer(target: self, action: #selector(player8TapFunction))
        player8.isUserInteractionEnabled = true
        player8.addGestureRecognizer(player8Tap)
        
        let player9Tap = UITapGestureRecognizer(target: self, action: #selector(player9TapFunction))
        player9.isUserInteractionEnabled = true
        player9.addGestureRecognizer(player9Tap)
        
        let player10Tap = UITapGestureRecognizer(target: self, action: #selector(player10TapFunction))
        player10.isUserInteractionEnabled = true
        player10.addGestureRecognizer(player10Tap)
        
        let player11Tap = UITapGestureRecognizer(target: self, action: #selector(player11TapFunction))
        player11.isUserInteractionEnabled = true
        player11.addGestureRecognizer(player11Tap)
        
        let player12Tap = UITapGestureRecognizer(target: self, action: #selector(player12TapFunction))
        player12.isUserInteractionEnabled = true
        player12.addGestureRecognizer(player12Tap)
        
        let player13Tap = UITapGestureRecognizer(target: self, action: #selector(player13TapFunction))
        player13.isUserInteractionEnabled = true
        player13.addGestureRecognizer(player13Tap)
        
        let player14Tap = UITapGestureRecognizer(target: self, action: #selector(player14TapFunction))
        player14.isUserInteractionEnabled = true
        player14.addGestureRecognizer(player14Tap)
        
        let player15Tap = UITapGestureRecognizer(target: self, action: #selector(player15TapFunction))
        player15.isUserInteractionEnabled = true
        player15.addGestureRecognizer(player15Tap)
        
//        let player16Tap = UITapGestureRecognizer(target: self, action: #selector(player16TapFunction))
//        player16.isUserInteractionEnabled = true
//        player16.addGestureRecognizer(player16Tap)
        
    }
    
    func removePlayer(playerId: Int, playerImageView: UIImageView, playerName: UILabel) {
        print("removePlayer",playerId)
        
        for player in playerList {
            let plyr = (player as! NSDictionary)["PLAYER_ID"] as? String ?? "\((player as! NSDictionary)["PLAYER_ID"] as! Int)"
            if plyr == "\(playerId)" {
                _selectedPlayers =  _selectedPlayers.filter { $0 != "\(playerId)" }
                playerListMutable.add(player)
                playerCollectionView.reloadData()
                playerImageView.image = nil
                playerName.text = ""
                remainingLabel.text = "\(Int(numberOfPlayer!)! - _selectedPlayers.count) Remaining"
            } else {
                _selectedPlayers =  _selectedPlayers.filter { $0 != "\(playerId)" }
                playerImageView.image = nil
                playerName.text = ""
                remainingLabel.text = "\(Int(numberOfPlayer!)! - _selectedPlayers.count) Remaining"
            }
        }
        print(_selectedPlayers)
    }
    
    @objc func player1TapFunction() {
        if player1.tag != 0 {
            removePlayer(playerId: player1?.tag ?? 0, playerImageView: player1, playerName: playerLabel1)
        }
    }
    @objc func player2TapFunction() {
        if player2.tag != 0 {
            removePlayer(playerId: player2?.tag ?? 0, playerImageView: player2, playerName: playerLabel2)
        }
    }
    @objc func player3TapFunction() {
        if player3.tag != 0 {
            removePlayer(playerId: player3?.tag ?? 0, playerImageView: player3, playerName: playerLabel3)
        }
    }
    @objc func player4TapFunction() {
        if player4.tag != 0 {
            removePlayer(playerId: player4?.tag ?? 0, playerImageView: player4, playerName: playerLabel4)
        }
    }
    @objc func player5TapFunction() {
        if player5.tag != 0 {
            removePlayer(playerId: player5?.tag ?? 0, playerImageView: player5, playerName: playerLabel5)
        }
    }
    @objc func player6TapFunction() {
        if player6.tag != 0 {
            removePlayer(playerId: player6?.tag ?? 0, playerImageView: player6, playerName: playerLabel6)
        }
    }
    @objc func player7TapFunction() {
        if player7.tag != 0 {
            removePlayer(playerId: player7?.tag ?? 0, playerImageView: player7, playerName: playerLabel7)
        }
    }
    @objc func player8TapFunction() {
        if player8.tag != 0 {
            removePlayer(playerId: player8?.tag ?? 0, playerImageView: player8, playerName: playerLabel8)
        }
    }
    @objc func player9TapFunction() {
        if player9.tag != 0 {
            removePlayer(playerId: player9?.tag ?? 0, playerImageView: player9, playerName: playerLabel9)
        }
    }
    @objc func player10TapFunction() {
        if player10.tag != 0 {
            removePlayer(playerId: player10?.tag ?? 0, playerImageView: player10, playerName: playerLabel10)
        }
    }
    @objc func player11TapFunction() {
        if player11.tag != 0 {
            removePlayer(playerId: player11?.tag ?? 0, playerImageView: player11, playerName: playerLabel11)
        }
    }
    @objc func player12TapFunction() {
        if player12.tag != 0 {
            removePlayer(playerId: player12?.tag ?? 0, playerImageView: player12, playerName: playerLabel12)
        }
    }
    @objc func player13TapFunction() {
        if player13.tag != 0 {
            removePlayer(playerId: player13?.tag ?? 0, playerImageView: player13, playerName: playerLabel13)
        }
    }
    @objc func player14TapFunction() {
        if player14.tag != 0 {
            removePlayer(playerId: player14?.tag ?? 0, playerImageView: player14, playerName: playerLabel14)
        }
    }
    @objc func player15TapFunction() {
        if player15.tag != 0 {
            removePlayer(playerId: player15?.tag ?? 0, playerImageView: player15, playerName: playerLabel15)
        }
    }
    @objc func player16TapFunction() {
//        if player16.tag != 0 {
//            removePlayer(playerId: player16?.tag ?? 0, playerImageView: player16)
//        }
    }
    
    
}
