//
//  ChooseABowlerViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 9/18/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ChooseABowlerViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource, ShowsAlert {
   
    @IBOutlet weak var chooseBowlerViewHolder: UIView!
    @IBOutlet weak var bowlerCollectionView: UICollectionView!
    @IBOutlet weak var bowlerNextButton: UIButton!
    var matchID: String?
    var adminDetail: NSDictionary?
    var playerList:NSArray?
    var selected_Player = ""
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var preventUserInteraction: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        CommonUtil.roundedUIViewCornersWithShade(uiviews: [chooseBowlerViewHolder])
        CommonUtil.buttonRoundedCorners(buttons: [bowlerNextButton])
        
        print(adminDetail)
    }

    
    @IBAction func bowlerNextButtonClick(_ sender: Any) {
        print("Bowler next button clicked")
        if selected_Player != "" {
            activityIndicator.startAnimating()
            preventUserInteraction.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"BOWLER\":\"\(selected_Player)\",\"MATCH_ID\":\"\(matchID!)\"}", mod: "MatchScore", actionType: "create-bowler") { (response) in
                if response as? String != "error" {
                    print(response)
                    let data: [String: Any] = ["match_id": self.matchID!,
                                               "user_id": UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String,
                                               "step_name":"BAT_USER_ID",
                                               "won_team_id":"",
                                               "won_team_name": "",
                                               "bat_or_bowl":"",
                                               "won_team_object":""]
                    SocketConnectionsclass.matchStatusUpdate_NewFlow(param: data)
                    self.dismiss(animated: true, completion: {
                        let playerList:[String: Any] = ["match_id":self.matchID!]
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openTeamScoreViewController"), object: nil, userInfo: playerList)
                    })
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
                self.preventUserInteraction.isHidden = true

            }
        } else {
            self.chooseBowlerViewHolder.shake()
            self.showAlert(title: "Bowler", message: "Please select a bowler")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return playerList!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let player = playerList![indexPath.row] as! NSDictionary
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseABowlerCollectionViewCell", for: indexPath) as! ChooseABowlerCollectionViewCell
        cell.bowlerImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(player["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
        cell.bowlerNameLabel.text = player["FULL_NAME"] as? String
        
        if selected_Player == player["MATCH_TEAM_PLAYER_ID"] as? String ?? "\(player["MATCH_TEAM_PLAYER_ID"] as! Int)" {
            cell.bowlerTickButton.isHidden = false
        } else {
            cell.bowlerTickButton.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let player = playerList![indexPath.row] as! NSDictionary
        print(player["MATCH_TEAM_PLAYER_ID"] as? String ?? "\(player["MATCH_TEAM_PLAYER_ID"] as! Int)")
        selected_Player = player["MATCH_TEAM_PLAYER_ID"] as? String ?? "\(player["MATCH_TEAM_PLAYER_ID"] as! Int)"
        print(selected_Player)
        collectionView.reloadData()
    }
    
}







