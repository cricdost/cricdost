//
//  ClaimNowViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 11/19/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ClaimNowViewController: UIViewController {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var addClaimAddressView: UIView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var addressLine1Label: UITextField!
    @IBOutlet weak var addressLine2Label: UITextField!
    @IBOutlet weak var cityLabel: UITextField!
    @IBOutlet weak var stateLabel: UITextField!
    @IBOutlet weak var pincodeLabel: UITextField!
    @IBOutlet weak var contactLabel: UITextField!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        CommonUtil.roundedUIViewCornersWithShade(uiviews: [addClaimAddressView])
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        titleView.roundCornersView([.topRight,.topLeft], radius: 5)
    }

}
