//
//  ChangeScorerCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 6/29/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ChangeScorerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var greenTick: UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        CommonUtil.imageRoundedCorners(imageviews: [profileImage, greenTick])
    }
}
