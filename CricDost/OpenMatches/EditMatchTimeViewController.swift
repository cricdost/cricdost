//
//  EditMatchTimeViewController.swift
//  CricDost
//
//  Created by Jit Goel on 11/15/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import IQKeyboardManager
import LTMorphingLabel

class EditMatchTimeViewController: UIViewController, LTMorphingLabelDelegate, ShowsAlert  {
    
    //When View
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var whenView: UIView!
    @IBOutlet weak var whenTitleLabel: LTMorphingLabel!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var timeTextField: UITextField!
    @IBOutlet weak var whenUpdateButton: UIButton!
    var date_picker:UIDatePicker?
    var time_picker:UIDatePicker?
    var dateString: String?
    var timeString: String?
    var matchID: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [whenView])
        CommonUtil.buttonRoundedCorners(buttons: [whenUpdateButton])
        
        setLabelMorph(labels: [whenTitleLabel], effect: .evaporate)
        
        let dismissViewGesture = UITapGestureRecognizer(target: self, action: #selector(dismissViewTapped))
        dismissView.addGestureRecognizer(dismissViewGesture)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy hh:mm a" //Your date format
//        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
        //according to date format your date string
        guard let date = dateFormatter.date(from: "\(dateString!) \(timeString!)") else {
            fatalError()
        }
        print(date)
        
        let now = date
        date_picker = UIDatePicker()
        dateTextField.inputView = date_picker
        date_picker?.minimumDate = Date()
        dateTextField.tintColor = .clear
        date_picker?.addTarget(self, action: #selector(handleDatePicker), for: UIControlEvents.valueChanged)
        date_picker?.datePickerMode = .date
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        dateTextField.text = formatter.string(from: now)
        
        time_picker = UIDatePicker()
        time_picker?.minimumDate = Date()
        timeTextField.inputView = time_picker
        timeTextField.tintColor = .clear
        time_picker?.addTarget(self, action: #selector(handleTimePicker), for: UIControlEvents.valueChanged)
        time_picker?.datePickerMode = .time
        
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "hh:mm a"
        timeTextField.text = formatter1.string(from: now)
        
        let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
        dateTextField.inputAccessoryView = toolBar
        timeTextField.inputAccessoryView = toolBar
    }
    
    //When View Functions
    @objc func dismissPicker() {
        view.endEditing(true)
    }
    
    @objc func handleDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateTextField.text = dateFormatter.string(from: (date_picker?.date)!)
    }
    
    @objc func handleTimePicker() {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        timeTextField.text = formatter.string(from: (time_picker?.date)!)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        whenTitleLabel.text = "When?"
    }
    
    @objc func dismissViewTapped(sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updateButtonClick(_ sender: Any) {
        if dateTextField.text == "DD/MM/YY" || timeTextField.text == "00:00 am" {
            self.whenView.shake()
            self.showAlert(title: "Invalid info", message: "Please set a date and time")
        } else {
            print("Update")
            updateMatchTime()
        }
    }
    
    func updateMatchTime() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let myDate = dateFormatter.date(from: dateTextField.text!)!
        dateFormatter.dateFormat = "yyyy/MM/dd"
        let matchDate = dateFormatter.string(from: myDate)
        
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\":\"\(matchID!)\",\"DATE\":\"\(matchDate)\",\"TIME\":\"\(timeTextField.text!)\"}", mod: "Match", actionType: "update-match-datetime") { (response) in
            print(response)
            if response as? String != "error" {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadOpenMacthDetailListFunction"), object: nil)
                let alertView = UIAlertController(title: "Match", message: (response as! NSDictionary)["XSCMessage"] as? String , preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    self.dismiss(animated: true, completion: nil)
                })
                alertView.addAction(action)
                self.present(alertView, animated: true, completion: nil)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
        }
    }
    
    func setLabelMorph(labels: [LTMorphingLabel], effect: LTMorphingEffect) {
        for label in labels {
            label.delegate = self
            label.morphingEffect = effect
            label.start()
        }
    }
    
}
