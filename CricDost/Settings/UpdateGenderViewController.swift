//
//  UpdateGenderViewController.swift
//  CricDost
//
//  Created by Jit Goel on 7/3/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class UpdateGenderViewController: UIViewController, ShowsAlert {
    
    @IBOutlet weak var maleButton: RadioButton!
    @IBOutlet weak var femaleButton: RadioButton!
    @IBOutlet weak var otherButton: RadioButton!
    var gender: String?
    @IBOutlet weak var updateGenderView: UIView!
    @IBOutlet weak var activityIIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [updateGenderView])
        setDefaultSelected()
    }
    
    func setDefaultSelected() {
        switch gender! {
        case "MALE":    setRadioButtonSelected(radio: maleButton)
                        setRadioButtonDeSelected(radioButtons: [femaleButton,otherButton])
            break
        case "FEMALE":  setRadioButtonSelected(radio: femaleButton)
                        setRadioButtonDeSelected(radioButtons: [maleButton,otherButton])
            break
        case "OTHER":   setRadioButtonSelected(radio: otherButton)
                        setRadioButtonDeSelected(radioButtons: [maleButton,femaleButton])
            break
        default:
            break
        }
    }
    
    @IBAction func cancelButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updateButtonClick(_ sender: Any) {
        updateGender()
    }
    
    @IBAction func maleButtonClick(_ sender: Any) {
        if (sender as! RadioButton) == maleButton {
            gender = "MALE"
            setRadioButtonSelected(radio: maleButton)
            setRadioButtonDeSelected(radioButtons: [femaleButton,otherButton])
        }
    }
    
    @IBAction func femaleButtonClick(_ sender: Any) {
        if (sender as! RadioButton) == femaleButton {
            gender = "FEMALE"
            setRadioButtonSelected(radio: femaleButton)
            setRadioButtonDeSelected(radioButtons: [maleButton,otherButton])
        }
    }
    @IBAction func otherButtonClick(_ sender: Any) {
        if (sender as! RadioButton) == otherButton {
            gender = "OTHER"
            setRadioButtonSelected(radio: otherButton)
            setRadioButtonDeSelected(radioButtons: [maleButton,femaleButton])
        }
    }
    
    func setRadioButtonSelected(radio: RadioButton) {
        radio.isSelected = true
        if radio.isSelected {
            radio.outerCircleColor = CommonUtil.themeRed
            radio.innerCircleCircleColor = CommonUtil.themeRed
        } else{
            radio.outerCircleColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.5)
        }
    }
    
    func setRadioButtonDeSelected(radioButtons: [RadioButton]) {
        for radio in radioButtons {
            radio.isSelected = false
            if radio.isSelected {
                radio.outerCircleColor = CommonUtil.themeRed
                radio.innerCircleCircleColor = CommonUtil.themeRed
            } else{
                radio.outerCircleColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.5)
            }
        }
    }
    
    func updateGender() {
        activityIIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"GENDER\":\"\(gender!)\"}", mod: "Users", actionType: "update-user-gender") { (response) in
            if response as? String != "error" {
                print(response)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getPlayerProfileEdit"), object: nil)
                let alert = UIAlertController(title: "Message", message: (response as! NSDictionary)["XSCMessage"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIIndicator.stopAnimating()
        }
    }
}
