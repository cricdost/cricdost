//
//  GoForTaskViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 9/17/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class GoForTaskViewController: UIViewController, ShowsAlert {

    @IBOutlet weak var goForTossViewHolder: UIView!
    @IBOutlet weak var matchTimerViewHolder: UIView!
    @IBOutlet weak var matchTimeLabel: UILabel!
    @IBOutlet weak var goForTossButton: UIButton!
    @IBOutlet weak var matchSecondsLabel: UILabel!
    
    var prodSeconds = 0 // This value is set in a different view controller
    lazy var intProdSeconds = Int(prodSeconds)
    var timer = Timer()
    var matchID: String?
    var isTimerRunning = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        CommonUtil.roundedUIViewCornersWithShade(uiviews: [goForTossViewHolder])
        CommonUtil.buttonRoundedCorners(buttons: [goForTossButton])
        CommonUtil.viewRoundedCorners(uiviews: [matchTimerViewHolder])
        
        if isTimerRunning == false {
            runProdTimer()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(dismissGoForTossView), name: NSNotification.Name(rawValue: "dismissGoForTossView"), object: nil)
    }
    
    func runProdTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(updateProdTimer)), userInfo: nil, repeats: true)
        isTimerRunning = true
    }
    
    @objc func updateProdTimer() {
        if intProdSeconds < 1 {
            timer.invalidate()
            matchTimeLabel.text = "00"
            matchSecondsLabel.text = "00"
        } else {
            intProdSeconds -= 1
            matchTimeLabel.text = prodTimeString(time: TimeInterval(intProdSeconds))
            matchSecondsLabel.text = prodTimeSecondsString(time: TimeInterval(intProdSeconds))
        }
    }
    
    func prodTimeString(time: TimeInterval) -> String {
        let prodMinutes = Int(time) / 60 % 60
        return String(format: "%02d", prodMinutes)
    }
    
    func prodTimeSecondsString(time: TimeInterval) -> String {
        let prodSeconds = Int(time) % 60
        return String(format: "%02d", prodSeconds)
    }
    
    @objc func dismissGoForTossView() {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func goForTossButtonClick(_ sender: Any) {
        print("go for toss button clicked")
        
        let data: [String: Any] = ["match_id": matchID!,
                                   "user_id": UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String,
                                   "step_name":"toss_match",
                                   "won_team_id":"",
                                   "won_team_name":"",
                                   "bat_or_bowl":"",
                                   "won_team_object":""]
        
        SocketConnectionsclass.matchStatusUpdate_NewFlow(param: data)
        self.dismiss(animated: true, completion: nil)
    }
    

}
