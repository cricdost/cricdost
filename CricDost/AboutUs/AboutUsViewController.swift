//
//  AboutUsViewController.swift
//  CricDost
//
//  Created by Jit Goel on 7/2/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import FRHyperLabel

class AboutUsViewController: UIViewController {

    @IBOutlet weak var cricDostLabel: FRHyperLabel!
    @IBOutlet weak var facebookImageview: UIImageView!
    @IBOutlet weak var twitterImageView: UIImageView!
    @IBOutlet weak var instagramImageView: UIImageView!
    @IBOutlet weak var youtubeImageView: UIImageView!
    @IBOutlet weak var googleplusImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = CommonUtil.themeRed
        }
//        UIApplication.shared.statusBarStyle = .lightContent
        
        cricDostLabel.numberOfLines = 0
        
        let re_send = "© 2018 CricDost All Rights Reversed."
        
        let attributes = [NSAttributedStringKey.foregroundColor: UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)]
        
        cricDostLabel.attributedText = NSAttributedString(string: re_send, attributes: attributes)
        cricDostLabel.linkAttributeHighlight = [NSAttributedStringKey.foregroundColor: CommonUtil.themeRed]
        cricDostLabel.linkAttributeDefault = [NSAttributedStringKey.foregroundColor: CommonUtil.themeRed]
        
        let handler = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
        }
        cricDostLabel.setLinksForSubstrings(["CricDost"], withLinkHandler: handler)
        
        let cricDostLabelTaped = UITapGestureRecognizer(target: self, action: #selector(cricDostLabelTapFunction))
        cricDostLabel.addGestureRecognizer(cricDostLabelTaped)
        
        let facebookImageviewTaped = UITapGestureRecognizer(target: self, action: #selector(facebookImageviewTapFunction))
        facebookImageview.addGestureRecognizer(facebookImageviewTaped)
        let twitterImageViewTaped = UITapGestureRecognizer(target: self, action: #selector(twitterImageViewTapFunction))
        twitterImageView.addGestureRecognizer(twitterImageViewTaped)
        let instagramImageViewTaped = UITapGestureRecognizer(target: self, action: #selector(instagramImageViewTapFunction))
        instagramImageView.addGestureRecognizer(instagramImageViewTaped)
        let youtubeImageViewTaped = UITapGestureRecognizer(target: self, action: #selector(youtubeImageViewTapFunction))
        youtubeImageView.addGestureRecognizer(youtubeImageViewTaped)
        let googleplusImageViewTaped = UITapGestureRecognizer(target: self, action: #selector(googleplusImageViewTapFunction))
        googleplusImageView.addGestureRecognizer(googleplusImageViewTaped)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func cricDostLabelTapFunction(sender:UITapGestureRecognizer) {
        UIApplication.shared.open(NSURL(string:"http://www.cricdost.com/")! as URL)
    }
    
    @objc func facebookImageviewTapFunction(sender:UITapGestureRecognizer) {
        UIApplication.shared.open(NSURL(string:"https://www.facebook.com/iamCricDost/")! as URL)
    }
    
    @objc func twitterImageViewTapFunction(sender:UITapGestureRecognizer) {
        UIApplication.shared.open(NSURL(string:"https://twitter.com/CricDost")! as URL)
    }
    
    @objc func instagramImageViewTapFunction(sender:UITapGestureRecognizer) {
        UIApplication.shared.open(NSURL(string:"https://www.instagram.com/cricdost/")! as URL)
    }
    
    @objc func youtubeImageViewTapFunction(sender:UITapGestureRecognizer) {
        UIApplication.shared.open(NSURL(string:"https://www.youtube.com/channel/UCbRdYMqFRj2e5_661GqLsvA")! as URL)
    }
    
    @objc func googleplusImageViewTapFunction(sender:UITapGestureRecognizer) {
        UIApplication.shared.open(NSURL(string:"https://plus.google.com/u/0/118433968369118734147")! as URL)
    }
    
    @IBAction func feedBackClick(_ sender: Any) {
        self.performSegue(withIdentifier: "aboutus_feedback", sender: self)
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
