//
//  ImagePreviewViewController.swift
//  CricDost
//
//  Created by Jit Goel on 5/25/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ImagePreviewViewController: UIViewController, ShowsAlert {
    
    var image: UIImage!
    var imageData: Data!
    var name: String?

    @IBOutlet weak var photoPreviewImageView: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadingViewPreventUserInteraction: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonUtil.buttonRoundedCorners(buttons: [nextButton])
        CommonUtil.imageRoundedCorners(imageviews: [photoPreviewImageView])
        
        photoPreviewImageView.image = image
        print(name)
        
        let imageTap = UITapGestureRecognizer(target: self, action: #selector(imageTapFunction))
        photoPreviewImageView.addGestureRecognizer(imageTap)
    }
    
    @objc func imageTapFunction(sender:UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextButtonClick(_ sender: Any) {
        activityIndicator.startAnimating()
        loadingViewPreventUserInteraction.isHidden = false
        myImageUploadRequest(image: imageData)
    }
    
    func myImageUploadRequest(image: Data)
    {
        
        let myUrl = NSURL(string: CommonUtil.BASE_URL);
        //let myUrl = NSURL(string: "http://www.boredwear.com/utils/postImage.php");
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        
        //        let param = "app=CRICDOST&mod=User&actionType=update-user-profile&subAction={\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY)!)\",\"FULL_NAME\":\"\(nameTextField.text!)\"}&sessionId=&subMod="
        let param = [
            "app" : "CRICDOST",
            "mod": "Users",
            "actionType" : "update-user-profile",
            "subAction": "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}"
            ] as [String : Any]
        print(param)
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        if(imageData==nil)  { return; }
        
        request.httpBody = createBodyWithParameters(parameters: param , filePathKey: "IMAGE", imageDataKey: image as NSData, boundary: boundary) as Data
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                self.showAlert(title: "Error", message: "Request TimeOut")
                self.activityIndicator.stopAnimating()
                self.loadingViewPreventUserInteraction.isHidden = true
                print("error=\(String(describing: error))")
                return
            }
            
            // You can print out response object
            print("******* response = \(String(describing: response))")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                
                print(json)
                
                DispatchQueue.main.async {
                    if (json!["XSCStatus"] as! Int == 0) {
                        let profile = json!["XSCData"] as! NSDictionary
                        print(profile)
                        
                        UserDefaults.standard.set(profile["PROFILE_PIC_URL"] as? String ?? "", forKey: CommonUtil.IMAGE_URL)
                        
                        switch(profile["STEP_NAME"] as! String) {
                            
                        case "GENDER":  self.performSegue(withIdentifier: "preview_gender", sender: self)
                            break
                        case "DOB": self.performSegue(withIdentifier: "preview_dob", sender: self)
                            break
                        case "PLAYER_STATUS": self.performSegue(withIdentifier: "preview_skill", sender: self)
                            break
                        case "LOCATION": self.performSegue(withIdentifier: "preview_location", sender: self)
                            break
                        case "DASHBOARD":
                            if CommonUtil.LandingPageCricSpace {
                                self.performSegue(withIdentifier: "preview_cricspace", sender: self)
                            } else {
                                self.performSegue(withIdentifier: "preview_dashboard", sender: self)
                            }
                            break
                        default:
                            print("DEFAULT CASE")
                        }
                    } else {
                        self.showAlert(title: "Something went wrong", message: json!["XSCMessage"] as! String)
                    }
                    self.activityIndicator.stopAnimating()
                    self.loadingViewPreventUserInteraction.isHidden = true
                }
                
            }catch
            {
                print(error)
                self.activityIndicator.stopAnimating()
                self.loadingViewPreventUserInteraction.isHidden = true
            }
        }
        
        task.resume()
    }
    
    func createBodyWithParameters(parameters: [String: Any]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
}
