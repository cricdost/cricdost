//
//  CricSpaceViewController.swift
//  CricDost
//
//  Created by Jit Goel on 10/11/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import FirebaseDatabase
import SDWebImage
import XMPPFramework
import AnimatedCollectionViewLayout
import SkeletonView
import Lottie
import UserNotifications
import Branch

class CricSpaceViewController: UIViewController, DKNavDrawerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource, ShowsAlert, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UNUserNotificationCenterDelegate {
    
    //XSC-Drawer
    var rootNav: DKNavDrawer?
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var backgroundView: UIView!
    
    //XSC-Notifications
    var rightButton  : UIButton?
    var notificationCount : Int = 0
    var notilabel: UILabel = UILabel(frame: CGRect(x: 10, y: -5, width: 16, height: 16))
    
    //Post View
    @IBOutlet weak var postView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var postLabel: UILabel!
    @IBOutlet weak var uploadImageButton: UIButton!
    
    //LiveScores
    var ref: DatabaseReference!
    var databaseHandleAdded: DatabaseHandle!
    var databaseHandleRemoved: DatabaseHandle!
    var databaseHandleChanged: DatabaseHandle!
    var matchesInfo: [Any] = []
    var liveMatches: [Any] = []
    var completedMatches: [Any] = []
    var removeElements: [Int] = []
    var scrollingTimer = Timer()
    var swipeTimer = Timer()
    @IBOutlet weak var liveMatchCollectionView: UICollectionView!
    @IBOutlet weak var pagecontrolLive: UIPageControl!
    var rowIndex = 0
    var matchTemp: NSDictionary?
    var teamATemp: NSDictionary?
    var teamBTemp: NSDictionary?
    
    //NewsFeed
    @IBOutlet weak var newsFeedTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var newsFeedTableView: UITableView!
    var postListMutable: NSMutableArray = []
    var nextPage = 1
    var tempPostID = "0"
    var tempPost: NSDictionary?
    var isfetchingPost = true
    internal var selectedSection: Int? {
        didSet{
            //(own internal logic removed)
            self.newsFeedTableView.beginUpdates()
            self.newsFeedTableView.reloadSections(IndexSet.init(integer: selectedSection ?? 0), with: .none)
            self.newsFeedTableView.endUpdates()
            DispatchQueue.main.async{
                UIView.animate(withDuration: 0.4) {
                    self.newsFeedTableViewHeight?.constant = self.newsFeedTableView.contentSize.height
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    var openCamera = false
    var editPost = false
    var currentAdressID: String?
    var timeLeftForMatch = 0
    var alertNonScorer: UIAlertController?
    weak var timer: Timer?
    var imagePicker = UIImagePickerController()
    var imageData:Data?
    var image: UIImage?
    @IBOutlet weak var loadingDataActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadingDataActivityIndicatorTop: UIActivityIndicatorView!
    
    //XMPP
    var xmppController: XMPPController?
    var stream: XMPPStream?
    var xmppRoom: XMPPRoom?
    
    var password: String?
    var userName: String?
    var commentToEdit: NSDictionary = [:]
    var commentEdit = false
    var likeType: String?
    
    //Collectionview animation
    let layout = AnimatedCollectionViewLayout()
    let animationView = LOTAnimationView(name: "swipeanimation")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newsFeedTableView.delegate = self
        newsFeedTableView.dataSource = self
        self.navigationController?.navigationBar.layer.zPosition = -1
        imagePicker.delegate = self
        
        CommonUtil.imageRoundedCorners(imageviews: [profileImageView])
        
        rootNav = (navigationController as? DKNavDrawer)
        rootNav?.dkNavDrawerDelegate = self
        
        let postNowGesture = UITapGestureRecognizer(target: self, action: #selector(postNowTapped))
        postLabel.isUserInteractionEnabled = true
        postLabel.addGestureRecognizer(postNowGesture)
        
        let profileImageGesture = UITapGestureRecognizer(target: self, action: #selector(profileImageViewTapped))
        profileImageView.isUserInteractionEnabled = true
        profileImageView.addGestureRecognizer(profileImageGesture)
        
        let outerViewGesture = UITapGestureRecognizer(target: self, action: #selector(outerViewFunctionTapped))
        backgroundView.addGestureRecognizer(outerViewGesture)
        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.maximumNumberOfTouches = 1
        edgePan.minimumNumberOfTouches = 1
        edgePan.edges = .right
        view.addGestureRecognizer(edgePan)
        
        rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        rightButton?.setBackgroundImage(#imageLiteral(resourceName: "baseline_notifications_white_24pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        rightButton?.tintColor = UIColor.white
        rightButton?.addTarget(self, action: #selector(navigateToNotification), for: .touchUpInside)
        let rightBarButtomItem = UIBarButtonItem(customView: rightButton!)
        navigationItem.rightBarButtonItem = rightBarButtomItem
        
        postView.layer.shadowRadius = 1
        postView.layer.shadowOpacity = 0.3
        postView.layer.shadowOffset = CGSize(width: 0, height: 1)
        postView.clipsToBounds = false
        
        newsFeedTableView.layer.shadowRadius = 1
        newsFeedTableView.layer.shadowOpacity = 0.3
        newsFeedTableView.layer.shadowOffset = CGSize(width: 0, height: -1)
        newsFeedTableView.clipsToBounds = false
        
        SocketConnectionsclass.connectSocket(userId: UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String, deviceKey: UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)
        
        registerNotificationCenterListeners()
        
        //Live Scores
        initializeLiveScores()
        
        profileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(UserDefaults.standard.object(forKey: CommonUtil.IMAGE_URL) as? String ?? "")"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default.png"))
        
        self.newsFeedTableView.sectionHeaderHeight = UITableViewAutomaticDimension
        self.newsFeedTableView.rowHeight = UITableViewAutomaticDimension
        self.newsFeedTableView.estimatedSectionHeaderHeight = 200
        self.newsFeedTableView.estimatedRowHeight = 100
        
        self.getAllPosts(pageNo: 1)
        
        initializeListeners()
        
        userName = UserDefaults.standard.object(forKey: CommonUtil.OPENFIRE_USERNAME) as? String
        password = UserDefaults.standard.object(forKey: CommonUtil.OPENFIRE_PASSWORD) as? String
        
        try! self.xmppController = XMPPController(hostName: CommonUtil.cricdost_chat_domain, userJIDString: "\(userName!)\(CommonUtil.cricdost_chat_domain_name)/\(UserDefaults.standard.object(forKey: CommonUtil.FULL_NAME) as! String)", hostPort: UInt16(CommonUtil.CHAT_CLIENT_PORT)!, password: password!)
        self.xmppController?.connect()
        
        stream = self.xmppController?.xmppStream
        
        swipeTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.swipeTimer(theTimer:)), userInfo: rowIndex, repeats: false)
        
        Branch.getInstance().setIdentity(UserDefaults.standard.object(forKey: CommonUtil.MOBILE_NUMBER) as? String)
        Branch.getInstance()?.userCompletedAction("signup_completed")
    }
    
    func initializeListeners() {
        NotificationCenter.default.addObserver(self, selector: #selector(newPostedAddedRefreshData), name: NSNotification.Name(rawValue: "newPostedAddedRefreshData"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(editPostDetailRefreshData), name: NSNotification.Name(rawValue: "editPostDetailRefreshData"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(joinMatchVCFunction), name: NSNotification.Name(rawValue: "joinMatchVCFunction"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateNotificationCount), name: NSNotification.Name(rawValue: "updateNotificationCount"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(UpcomingMatch), name: NSNotification.Name(rawValue: "UpcomingMatch"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(matchAdminDetail), name: NSNotification.Name(rawValue: "matchAdminDetail"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openTeamVCFromPlayerProfile), name: NSNotification.Name(rawValue: "openTeamVCFromPlayerProfile"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openPlayerVCFromTeamProfile), name: NSNotification.Name(rawValue: "openPlayerVCFromTeamProfile"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showMyProfile), name: NSNotification.Name(rawValue: "showMyProfile"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openMatchesDetailVC), name: NSNotification.Name(rawValue: "openMatchesDetailVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ShowTossVCFunction), name: NSNotification.Name(rawValue: "ShowTossVCFunction"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(sendMessage), name: NSNotification.Name(rawValue: "sendMessage"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(sendComposing), name: NSNotification.Name(rawValue: "sendComposing"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(joinGroupChat), name: NSNotification.Name(rawValue: "joinGroupChat"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(sendMessageGroup), name: NSNotification.Name(rawValue: "sendMessageGroup"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(disconnectXMPP), name: NSNotification.Name(rawValue: "disconnectXMPP"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(connectXMPP), name: NSNotification.Name(rawValue: "connectXMPP"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshDrawerMsgCount), name: NSNotification.Name(rawValue: "refreshDrawerMsgCount"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openMatchOverViewController), name: NSNotification.Name(rawValue: "openMatchOverViewController"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openNonStrikerViewController), name: NSNotification.Name(rawValue: "openNonStrikerViewController"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openChooseScorerViewController), name: NSNotification.Name(rawValue: "openChooseScorerViewController"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openTeamScoreViewController), name: NSNotification.Name(rawValue: "openTeamScoreViewController"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openScorerViewController), name: NSNotification.Name(rawValue: "openScorerViewController"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openNotificationPage), name: NSNotification.Name(rawValue: "openNotificationPage"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(removeReportedPost), name: NSNotification.Name(rawValue: "removeReportedPost"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshMyProfileImage), name: NSNotification.Name(rawValue: "refreshMyProfileImage"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(removeReportedComment), name: NSNotification.Name(rawValue: "removeReportedComment"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(blockedUserFunctionFromCommentPage), name: NSNotification.Name(rawValue: "blockedUserFunctionFromCommentPage"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(deletePostFromCommentPage), name: NSNotification.Name(rawValue: "deletePostFromCommentPage"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(dismissBlackScreen), name: NSNotification.Name(rawValue: "dismissBlackScreen"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(gotoInvitePage), name: NSNotification.Name(rawValue: "gotoInvitePage"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(hideShowNavBar), name: NSNotification.Name(rawValue: "hideShowNavBar"), object: nil)
    }
    
    @objc func hideShowNavBar() {
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    @objc func dismissBlackScreen() {
        backgroundView.isHidden = true
    }
    
    
    @objc func refreshMyProfileImage() {
        
        profileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(UserDefaults.standard.object(forKey: CommonUtil.IMAGE_URL) as? String ?? "")"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default.png"))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        CommonUtil.updateGATracker(screenName: "CricSpace")
        if UserDefaults.standard.object(forKey: "ONBOARD") as? Bool ?? true {

            if UserDefaults.standard.object(forKey: CommonUtil.NewNotification) as? Bool ?? true {
                UserDefaults.standard.set(true, forKey: CommonUtil.NewNotification)
                print("true,notification can access")
                createNotification()
                setWelcomeNotification()
            } else {
                print("false, sorry can not access")
                
            }
            UserDefaults.standard.set(false, forKey: "ONBOARD")
//            UserDefaults.standard.set(false, forKey: CommonUtil.NewNotification)
        }
        
        if UserDefaults.standard.object(forKey: CommonUtil.NewNotification) as? Bool ?? true {
            UserDefaults.standard.set(true, forKey: CommonUtil.NewNotification)
            setWelcomeNotification()
        }
        
        updateNotificationCount()
        updateFCMToken()
    }
    
    @objc func outerViewFunctionTapped() {
        backgroundView.isHidden = true
        navigationController?.setNavigationBarHidden(false, animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissMatchDetailView"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissMyProfileFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissTeamProfileFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissPlayerProfileFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissFixedMatchesFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissOpenMatchesFunction"), object: nil)
    }
    
    @objc func swipeTimer(theTimer: Timer) {
        if UserDefaults.standard.object(forKey: CommonUtil.SWIPEDEMO) as? Bool ?? true {
            UserDefaults.standard.set(true, forKey: "ONBOARD")
            UserDefaults.standard.set(false, forKey: CommonUtil.SWIPEDEMO)
            animationView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height/2)
            animationView?.center = self.view.center
            animationView?.contentMode = .scaleAspectFill
            animationView?.loopAnimation = true
            view.addSubview(animationView!)
            animationView?.play()
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.newsFeedTableViewHeight?.constant = self.newsFeedTableView.contentSize.height
    }
    
    @IBAction func uploadImageButtonClick(_ sender: Any) {
        print("comment clciked")
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .popover
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCam()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            print("IPAD")
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            alert.popoverPresentationController?.permittedArrowDirections = .down
            
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCam()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image_data = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageData = UIImageJPEGRepresentation(image_data.updateImageOrientionUpSide()!, 0.025)
            self.dismiss(animated: true, completion: nil)
            image = image_data
            openCamera = true
            editPost = false
            self.performSegue(withIdentifier: "cricspace_postnow", sender: self)
        }
    }
    
    @objc func postNowTapped() {
        print("post now clciked")
        openCamera = false
        editPost = false
        self.performSegue(withIdentifier: "cricspace_postnow", sender: self)
    }
    
    @objc func profileImageViewTapped() {
        backgroundView.isHidden = true
        print("profile image view tapped")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissMatchDetailView"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissMyProfileFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissTeamProfileFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissPlayerProfileFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissFixedMatchesFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissOpenMatchesFunction"), object: nil)
        showMyProfile()
    }
    
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .began {
            print("Screen edge began")
            animationView?.pause()
            animationView?.removeFromSuperview()
            self.performSegue(withIdentifier: "cricspace_mapView", sender: self)
        }
        if recognizer.state == .recognized {
            print("Screen edge swiped!")
        }
        if recognizer.state == .ended {
            print("Screen edge ended")
        }
    }
    
    // XSC-Drawer
    @IBAction func menuButtonClick(_ sender: Any) {
        rootNav?.drawerToggle()
    }
    
    func dkNavDrawerSelection(_ selectionIndex: Int) {
        print(selectionIndex)
        switch selectionIndex {
        case 0: NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissMyProfileFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissMatchDetailView"), object: nil)
        showMyProfile()
            break
        case 1: self.performSegue(withIdentifier: "cricspace_message", sender: self)
            break
        case 2: self.performSegue(withIdentifier: "cricspace_livescores", sender: self)
            break
        case 3: self.performSegue(withIdentifier: "cricspace_settings", sender: self)
            break
        case 4: UIApplication.shared.open(URL(string: "https://itunes.apple.com/app/id1367161611")!)
            break
        case 5: self.performSegue(withIdentifier: "cricspace_aboutus", sender: self)
            break
        case 6: UserDefaults.standard.set(true, forKey: CommonUtil.OPENINVITEPAGE)
            break
        default:
            break
        }
    }
    
    @objc func gotoInvitePage() {
        self.performSegue(withIdentifier: "cricspace_inviteFriend", sender: self)
    }
    
    //XSC-Notifications
    @objc func navigateToNotification() {
        setNeedsStatusBarAppearanceUpdate()
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let notificationViewController = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        notificationViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        notificationViewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        self.present(notificationViewController, animated: true, completion: nil)
    }
    
    @objc func updateNotificationCount() {
        print("=======notification")
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Notification", actionType: "notification-count") { (response) in
            print(response)
            if response as? String != "error" {
                print(response)
                self.notificationCount = (response as! NSDictionary)["XSCData"] as! Int
                self.createBadgeNotification()
            }
        }
    }
    
    func createBadgeNotification()
    {
        print("notification count=======")
        notilabel.layer.borderColor = UIColor.clear.cgColor
        notilabel.layer.borderWidth = 2
        notilabel.layer.cornerRadius = notilabel.bounds.size.height / 2
        notilabel.textAlignment = .center
        notilabel.layer.masksToBounds = true
        notilabel.font = UIFont.systemFont(ofSize: 10)
        notilabel.textColor = .white
        notilabel.backgroundColor = .clear
        notilabel.text = ""
        
        // create subview for notification bar button item
        
        if(notificationCount > 0)
        {
            notilabel.text = String(notificationCount)
            if UserDefaults.standard.object(forKey: CommonUtil.NewNotification) as? Bool ?? false {
                notilabel.text = String(notificationCount + 1)
            }
            if(notificationCount > 99)
            {
                notilabel.text = "99+"
            }
            notilabel.backgroundColor = .black
            
        }
        if notificationCount == 0 {
            notilabel.text = ""
            notilabel.backgroundColor = .clear
        }
        rightButton?.addSubview(notilabel)
        UIApplication.shared.applicationIconBadgeNumber = notificationCount
        if UserDefaults.standard.object(forKey: CommonUtil.NewNotification) as? Bool ?? false {
            UIApplication.shared.applicationIconBadgeNumber = notificationCount + 1
        }
    }
    
    //XSC-NEWSFEED
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if postListMutable.count != 0 {
            let post = postListMutable[section] as! NSDictionary
            if post["IS_TYPE"] as! String == "POST" {
                if post["COMMENT"] as? String ?? "\(post["COMMENT"] as! Int)" == "0" {
                    return 0
                } else {
                    let comments = post["COMMENTS"] as! NSArray
                    return comments.count + 1
                }
            } else {
                return 0
            }
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == self.selectedSection {
            return UITableViewAutomaticDimension
        }else{
            let size = 0
            return CGFloat(size)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if postListMutable.count != 0 {
            return postListMutable.count
        } else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "newsfeedcell") as! NewsFeedTableViewCell
        let cricNews = tableView.dequeueReusableCell(withIdentifier: "CricNewsFeedTableViewCell") as! CricNewsFeedTableViewCell
        if section == 0 {
            cell.spaceViewHeightConstraint.constant = 0
            cricNews.spaceHeightConstraint.constant = 0
        }
        
        if postListMutable.count != 0 {
            let post = postListMutable[section] as! NSDictionary
            cell.hideAnimation()
            if post["IS_TYPE"] as! String == "NEWS" {
                if post["NEWS_IMAGE_URL"] as! String != "" {
                    print("news image view",post["NEWS_IMAGE_URL"] as! String)
                    cricNews.cricNewsImageView.sd_setImage(with: URL(string: post["NEWS_IMAGE_URL"] as! String), placeholderImage: nil)
                    cricNews.imageHeightConstraint.constant = 250
                } else {
                    cricNews.imageHeightConstraint.constant = 0
                }
                cricNews.cricNewsTitleLabel.text = post["TITLE"] as? String
                cricNews.cricNewsContentLabel.text = post["DESCRIPTION"] as? String
                cricNews.newsSourceLabel.text = "Source - \(post["SOURCE_NAME"] as! String)"
                cricNews.cricNewsShareButton.addTarget(self, action: #selector(cricNewsShareButtonTapped), for: UIControlEvents.touchUpInside)
                cricNews.shareButton.addTarget(self, action: #selector(cricNewsShareButtonTapped), for: UIControlEvents.touchUpInside)
                cricNews.shareButton.postLabel = cricNews.cricNewsContentLabel
                cricNews.shareButton.postImage = cricNews.cricNewsImageView
                cricNews.shareButton.postTitle = cricNews.cricNewsTitleLabel
                cricNews.shareButton.tag = section
                cricNews.cricNewsShareButton.postLabel = cricNews.cricNewsContentLabel
                cricNews.cricNewsShareButton.postImage = cricNews.cricNewsImageView
                cricNews.cricNewsShareButton.postTitle = cricNews.cricNewsTitleLabel
                cricNews.cricNewsShareButton.tag = section
                
                cricNews.contentView.tag = section
                let headerTapped = UITapGestureRecognizer (target: self, action:#selector(newsHeaderTapped))
                cricNews.contentView.addGestureRecognizer(headerTapped)
                
                return cricNews.contentView
                
            } else {
                
                cell.postContentLabel.numberOfLines = 3
                
                cell.profileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(post["USER_IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default.png"))
                cell.profileNameLabel.text = (post["POSTED_BY"] as? String)?.capitalized
                //             cell.addressTimeLabel.text = "Chennai \u{2022} \(post["CREATED_DATE"] as! String)"
                if let _ = post["ADDRESS"] as? String {
                    let addressList = (post["ADDRESS"] as! String).split(separator: ",")
                    if addressList.count > 3 && addressList.count < 9 {
                        cell.addressTimeLabel.text = "\(addressList[addressList.endIndex - 4].trimmingCharacters(in: .whitespaces)) \u{2022} \(post["CREATED_DATE"] as! String)"
                    } else {
                        cell.addressTimeLabel.text = "\(addressList[addressList.endIndex - 2].trimmingCharacters(in: .whitespaces)) \u{2022} \(post["CREATED_DATE"] as! String)"
                    }
                }
                
                
                //                getAddressFromLatLon(pdblLatitude: Double(post["LATITUDE"] as! String) ?? 0.0, withLongitude: Double(post["LONGITUDE"] as! String) ?? 0.0, label: cell.addressTimeLabel, postDate: post["CREATED_DATE"] as! String)
                
                cell.postContentLabel.text = post["POST"] as? String
                if post["POST_IMAGE_URL"] as! String != "" {
                    cell.oneImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(post["POST_IMAGE_URL"] as! String)"), placeholderImage:nil)
                    cell.imageHeightConstraint.constant = 250
                } else {
                    cell.imageHeightConstraint.constant = 0
                }
                
                if post["SIX"] as? String ?? "\(post["SIX"] as! Int)" == "0" || post["SIX"] as? String ?? "\(post["SIX"] as! Int)" == "1" {
                    cell.sixCountLabel.text = "\(post["SIX"] as? String ?? "\(post["SIX"] as! Int)") six"
                } else {
                    cell.sixCountLabel.text = "\(post["SIX"] as? String ?? "\(post["SIX"] as! Int)") sixes"
                }
                
                if post["FOUR"] as? String ?? "\(post["FOUR"] as! Int)" == "0" || post["FOUR"] as? String ?? "\(post["FOUR"] as! Int)" == "1" {
                    cell.fourCountLabel.text = "\(post["FOUR"] as? String ?? "\(post["FOUR"] as! Int)") four"
                } else {
                    cell.fourCountLabel.text = "\(post["FOUR"] as? String ?? "\(post["FOUR"] as! Int)") fours"
                }
                
                if post["WICKET"] as? String ?? "\(post["WICKET"] as! Int)" == "0" || post["WICKET"] as? String ?? "\(post["WICKET"] as! Int)" == "1" {
                    cell.outCountLabel.text = "\(post["WICKET"] as? String ?? "\(post["WICKET"] as! Int)") out"
                } else {
                    cell.outCountLabel.text = "\(post["WICKET"] as? String ?? "\(post["WICKET"] as! Int)") outs"
                }
                
                if post["COMMENT"] as? String ?? "\(post["COMMENT"] as! Int)" == "0" || post["COMMENT"] as? String ?? "\(post["COMMENT"] as! Int)" == "1" {
                    cell.commentsCountLabel.text = "\(post["COMMENT"] as? String ?? "\(post["COMMENT"] as! Int)") comment"
                } else {
                    cell.commentsCountLabel.text = "\(post["COMMENT"] as? String ?? "\(post["COMMENT"] as! Int)") comments"
                }
                
                if self.selectedSection == section {
                    cell.commentButton.setImage(#imageLiteral(resourceName: "comment-color"), for: .normal)
                } else {
                    cell.commentButton.setImage(#imageLiteral(resourceName: "color-bw"), for: .normal)
                }
                
                if post["COMMENT"] as? String ?? "\(post["COMMENT"] as! Int)" == "0" {
                    cell.commentButton.setImage(#imageLiteral(resourceName: "color-bw"), for: .normal)
                }
                
                cell.commentButton.addTarget(self, action: #selector(commentButtonTapped), for: UIControlEvents.touchUpInside)
                cell.commentButton.tag = section
                
                cell.sixButton.addTarget(self, action: #selector(sixButtonTapped), for: UIControlEvents.touchUpInside)
                cell.sixButton.tag = section
                
                //like six click
                cell.sixCountLabel.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(sixCountLabelTapped)))
                cell.sixCountLabel.isUserInteractionEnabled = true
                cell.sixCountLabel.tag = section
                
                cell.fourButton.addTarget(self, action: #selector(fourButtonTapped), for: UIControlEvents.touchUpInside)
                cell.fourButton.tag = section
                
                //like four click
                cell.fourCountLabel.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(fourCountLabelTapped)))
                cell.fourCountLabel.isUserInteractionEnabled = true
                cell.fourCountLabel.tag = section
                
                cell.outButton.addTarget(self, action: #selector(outButtonTapped), for: UIControlEvents.touchUpInside)
                cell.outButton.tag = section
                
                //like out click
                cell.outCountLabel.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(outCountLabelTapped)))
                cell.outCountLabel.isUserInteractionEnabled = true
                cell.outCountLabel.tag = section
                
                
                cell.shareButton.addTarget(self, action: #selector(shareButtonTapped), for: UIControlEvents.touchUpInside)
                cell.shareButton.postLabel = cell.postContentLabel
                cell.shareButton.postImage = cell.oneImageView
                cell.shareButton.tag = section
                
                cell.optionButton.addTarget(self, action: #selector(optionButtonTapped), for: UIControlEvents.touchUpInside)
                cell.optionButton.tag = section
                
                //                cell.contentView.tag = section
                //                let headerTapped = UITapGestureRecognizer (target: self, action:#selector(sectionHeaderTapped))
                //                cell.contentView.addGestureRecognizer(headerTapped)
                
                cell.oneImageView.tag = section
                let oneImageTapped = UITapGestureRecognizer (target: self, action:#selector(sectionHeaderTapped))
                cell.oneImageView.addGestureRecognizer(oneImageTapped)
                
                cell.postContentLabel.tag = section
                let postContentTapped = UITapGestureRecognizer (target: self, action:#selector(sectionHeaderTapped))
                cell.postContentLabel.addGestureRecognizer(postContentTapped)
                
                
                
                if post["USER_STATUS"] as? String ?? "liked post" != "" {
                    let myLikes = post["USER_STATUS"] as! NSDictionary
                    
                    if myLikes["IS_SIX"] as! String == "true" {
                        cell.sixButton.setImage(#imageLiteral(resourceName: "6-color.png"), for: .normal)
                    } else {
                        cell.sixButton.setImage(#imageLiteral(resourceName: "6-bw.png"), for: .normal)
                    }
                    
                    if myLikes["IS_FOUR"] as! String == "true" {
                        cell.fourButton.setImage(#imageLiteral(resourceName: "4-color"), for: .normal)
                    } else {
                        cell.fourButton.setImage(#imageLiteral(resourceName: "4-bw"), for: .normal)
                    }
                    
                    if myLikes["IS_WICKET"] as! String == "true" {
                        cell.outButton.setImage(#imageLiteral(resourceName: "out-color"), for: .normal)
                    } else {
                        cell.outButton.setImage(#imageLiteral(resourceName: "out-bw"), for: .normal)
                    }
                }
                
            }
        } else {
            cell.oneImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)"), placeholderImage:nil)
            cell.postContentLabel.numberOfLines = 3
        }
        
        
        
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let post = postListMutable[indexPath.section] as! NSDictionary
        
        if indexPath.row == (post["COMMENTS"] as! NSArray).count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "showmorecell", for: indexPath) as! newsFeedShowMoreTableViewCell
            cell.showMoreButton.addTarget(self, action: #selector(showMoreButtonTapped), for: UIControlEvents.touchUpInside)
            cell.showMoreButton.tag = indexPath.section
            return cell
        } else {
            let comment = (post["COMMENTS"] as! NSArray)[indexPath.row] as! NSDictionary
            let cell = tableView.dequeueReusableCell(withIdentifier: "newsfeedcomments", for: indexPath) as! NewFeedCommentsTableViewCell
            cell.commentsLabel.text = comment["COMMENT"] as? String
            cell.commentsProfileImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(comment["USER_IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
            cell.commentsProfileName.text = (comment["COMMENTED_BY"] as? String)?.capitalized
            //            cell.commentsaddressTimeLabel.text = "Chennai \u{2022} \(post["CREATED_DATE"] as! String)"
            if let _ = post["ADDRESS"] as? String {
            let addressList = (comment["ADDRESS"] as! String).split(separator: ",")
            if addressList.count > 3 && addressList.count < 9 {
                cell.commentsaddressTimeLabel.text = "\(addressList[addressList.endIndex - 4].trimmingCharacters(in: .whitespaces)) \u{2022} \(comment["COMMENTED_ON"] as! String)"
            } else {
                cell.commentsaddressTimeLabel.text = "\(addressList[addressList.endIndex - 2].trimmingCharacters(in: .whitespaces)) \u{2022} \(comment["COMMENTED_ON"] as! String)"
            }
            }
            
            cell.commentsOptionButton.addTarget(self, action: #selector(commentOptionMenuTapped), for: UIControlEvents.touchUpInside)
            cell.commentsOptionButton.tag = indexPath.section
            cell.commentsOptionButton.extraDetails = indexPath.row
            return cell
        }
    }
    
    @objc func sectionHeaderTapped(tapped: UITapGestureRecognizer){
        print("header section view in newsfeed")
        let section = tapped.view?.tag
        let post = postListMutable[section ?? 0] as! NSDictionary
        tempPostID = post["POST_ID"] as? String ?? "\(post["POST_ID"] as! Int)"
        tempPost = post
        commentToEdit = [:]
        commentEdit = false
        self.performSegue(withIdentifier: "cricspace_comment", sender: self)
    }
    
    @objc func newsHeaderTapped(tapped: UITapGestureRecognizer) {
        let section = tapped.view?.tag
        let post = postListMutable[section ?? 0] as! NSDictionary
        if let link = post["LINK"] as? String {
            UIApplication.shared.open(NSURL(string: link)! as URL)
        }
    }
    
    @IBAction func commentOptionMenuTapped(_ sender: ShareButtonCustom) {
        let post = postListMutable[sender.tag] as! NSDictionary
        let postId = post["POST_ID"] as? String ?? "\(post["POST_ID"] as! Int)"
        let comments = post["COMMENTS"] as! NSArray
        let comment = comments[sender.extraDetails] as! NSDictionary
        print("comment option button click")
        let alert = UIAlertController(title: "Choose Action", message: nil, preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .popover
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        if comment["USER_ID"] as? String ?? "\(comment["USER_ID"] as! Int)" == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as? String  {
            alert.addAction(UIAlertAction(title: "Edit comment", style: .default, handler: { _ in
                self.commentToEdit = comment
                self.commentEdit = true
                self.performSegue(withIdentifier: "cricspace_comment", sender: self)
            }))
            alert.addAction(UIAlertAction(title: "Delete comment", style: .default, handler: { _ in
                self.deleteComment(commentID: comment["COMMENT_ID"] as? String ?? "\(comment["COMMENT_ID"] as! Int)", postID: postId)
            }))
        } else {
            alert.addAction(UIAlertAction(title: "Report comment", style: .default, handler: { _ in
                self.reportComment(commentID: comment["COMMENT_ID"] as? String ?? "\(comment["COMMENT_ID"] as! Int)", postID: postId)
            }))
            alert.addAction(UIAlertAction(title: "Block User", style: .default, handler: { _ in
                self.blockComment(UserID: comment["USER_ID"] as? String ?? "\(comment["USER_ID"] as! Int)", postID: postId)
            }))
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func optionButtonTapped(_ sender: UIButton) {
        let post = postListMutable[sender.tag] as! NSDictionary
        let postId = post["POST_ID"] as? String ?? "\(post["POST_ID"] as! Int)"
        let alert = UIAlertController(title: "Choose Action", message: nil, preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .popover
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        if post["USER_ID"] as? String ?? "\(post["USER_ID"] as! Int)" == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as? String  {
            alert.addAction(UIAlertAction(title: "Edit Post", style: .default, handler: { _ in
                self.editPost = true
                self.tempPostID = postId
                self.performSegue(withIdentifier: "cricspace_postnow", sender: self)
            }))
            alert.addAction(UIAlertAction(title: "Delete post", style: .default, handler: { _ in
                self.deletePost(postID: postId)
            }))
        } else {
            alert.addAction(UIAlertAction(title: "Report post", style: .default, handler: { _ in
                self.reportPost(postID: postId)
            }))
            alert.addAction(UIAlertAction(title: "Block User", style: .default, handler: { _ in
                let alertController = UIAlertController(title: "Block User", message: "Are you sure you want to block this user?", preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title: "Block", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    self.blockPost(USER_ID: post["USER_ID"] as? String ?? "\(post["USER_ID"] as! Int)", postID: postId)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                }
                // Add the actions
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
                
            }))
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func sixButtonTapped(_ sender: UIButton) {
        let post = postListMutable[sender.tag] as! NSDictionary
        let postId = post["POST_ID"] as? String ?? "\(post["POST_ID"] as! Int)"
        if post["USER_STATUS"] as? String ?? "liked post" != "" {
            let myLikes = post["USER_STATUS"] as! NSDictionary
            if myLikes["IS_SIX"] as! String == "true" {
                addLikes(postID: postId, six: 0, four: 0, out: 0, post: post)
            } else {
                addLikes(postID: postId, six: 1, four: 0, out: 0, post: post)
            }
        } else {
            addLikes(postID: postId, six: 1, four: 0, out: 0, post: post)
        }
        
    }
    
    @IBAction func sixCountLabelTapped(tapped: UITapGestureRecognizer) {
        let section = tapped.view?.tag
        let post = postListMutable[section ?? 0] as! NSDictionary
        tempPostID = post["POST_ID"] as? String ?? "\(post["POST_ID"] as! Int)"
        tempPost = post
        likeType = "SIX"
        self.performSegue(withIdentifier: "cricspace_likesDetails", sender: self)
    }
    
    @IBAction func fourCountLabelTapped(tapped: UITapGestureRecognizer) {
        let section = tapped.view?.tag
        let post = postListMutable[section ?? 0] as! NSDictionary
        tempPostID = post["POST_ID"] as? String ?? "\(post["POST_ID"] as! Int)"
        tempPost = post
        likeType = "FOUR"
        self.performSegue(withIdentifier: "cricspace_likesDetails", sender: self)
    }
    
    @IBAction func outCountLabelTapped(tapped: UITapGestureRecognizer) {
        let section = tapped.view?.tag
        let post = postListMutable[section ?? 0] as! NSDictionary
        tempPostID = post["POST_ID"] as? String ?? "\(post["POST_ID"] as! Int)"
        tempPost = post
        likeType = "OUT"
        self.performSegue(withIdentifier: "cricspace_likesDetails", sender: self)
    }
    
    @IBAction func fourButtonTapped(_ sender: UIButton) {
        let post = postListMutable[sender.tag] as! NSDictionary
        let postId = post["POST_ID"] as? String ?? "\(post["POST_ID"] as! Int)"
        if post["USER_STATUS"] as? String ?? "liked post" != "" {
            let myLikes = post["USER_STATUS"] as! NSDictionary
            if myLikes["IS_FOUR"] as! String == "true" {
                addLikes(postID: postId, six: 0, four: 0, out: 0, post: post)
            } else {
                addLikes(postID: postId, six: 0, four: 1, out: 0, post: post)
            }
        } else {
            addLikes(postID: postId, six: 0, four: 1, out: 0, post: post)
        }
    }
    
    @IBAction func outButtonTapped(_ sender: UIButton) {
        let post = postListMutable[sender.tag] as! NSDictionary
        let postId = post["POST_ID"] as? String ?? "\(post["POST_ID"] as! Int)"
        if post["USER_STATUS"] as? String ?? "liked post" != "" {
            let myLikes = post["USER_STATUS"] as! NSDictionary
            if myLikes["IS_WICKET"] as! String == "true" {
                addLikes(postID: postId, six: 0, four: 0, out: 0, post: post)
            } else {
                addLikes(postID: postId, six: 0, four: 0, out: 1, post: post)
            }
        } else {
            addLikes(postID: postId, six: 0, four: 0, out: 1, post: post)
        }
        
    }
    
    @IBAction func shareButtonTapped(_ sender: ShareButtonCustom) {
        //        let post = postListMutable[sender.tag] as! NSDictionary
        var items: [Any] = []
        if sender.postLabel?.text != nil {
            items.append(sender.postLabel?.text! as Any)
        }
        if sender.postImage?.image != nil {
            items.append(sender.postImage?.image! as Any)
        }
        
        let link = "\n\n\nClick here to Download the App\nhttp://www.cricdost.com/"
        items.append(link)
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: items, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func cricNewsShareButtonTapped(_ sender: ShareButtonCustom) {
        let post = postListMutable[sender.tag] as! NSDictionary
        
        var newsItems: [Any] = []
        if sender.postTitle!.text != nil {
            newsItems.append(sender.postTitle?.text! as Any)
        }
        
        if sender.postImage?.image != nil {
            newsItems.append(sender.postImage?.image! as Any)
        }
        
        if sender.postLabel!.text != nil {
            newsItems.append(sender.postLabel?.text! as Any)
        }
        
        if let link = post["LINK"] as? String {
            newsItems.append(link)
        }
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: newsItems, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func showMoreButtonTapped(_ sender: UIButton) {
        print("show more button in table view")
        let post = postListMutable[sender.tag] as! NSDictionary
        tempPostID = post["POST_ID"] as? String ?? "\(post["POST_ID"] as! Int)"
        tempPost = post
        commentToEdit = [:]
        commentEdit = false
        self.performSegue(withIdentifier: "cricspace_comment", sender: self)
    }
    
    @IBAction func commentButtonTapped(_ sender: UIButton) {
        print("comment button in table view",sender.tag)
        let post = postListMutable[sender.tag] as! NSDictionary
        tempPostID = post["POST_ID"] as? String ?? "\(post["POST_ID"] as! Int)"
        tempPost = post
        commentToEdit = [:]
        commentEdit = false
        if post["COMMENT"] as? String ?? "\(post["COMMENT"] as! Int)" == "0" {
            self.performSegue(withIdentifier: "cricspace_comment", sender: self)
        } else {
            if sender.tag == self.selectedSection {
                print("grey")
                self.selectedSection = nil
            } else {
                print("yellow")
                self.selectedSection = sender.tag
            }
            newsFeedTableView.reloadSections(IndexSet.init(integer: sender.tag), with: .none)
            sender.layoutIfNeeded()
            sender.layoutSubviews()
        }
    }
    
    func updateFCMToken() {
        print("Updating FBTOKEN",UserDefaults.standard.object(forKey: CommonUtil.FB_TOKEN) as? String ?? "")
        let mod = "AA"
        let actionType =  "save-token"
        let subAction = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as? String ?? "abc")\",\"DEVICE_TOKEN\":\"\(UserDefaults.standard.object(forKey: CommonUtil.FB_TOKEN) as? String ?? "")\"}"
        if (Reachability.isConnectedToNetwork()) {
            let url = URL(string: CommonUtil.BASE_URL)!
            var request = URLRequest(url: url)
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            let postString = "app=CRICDOST&mod=\(mod)&actionType=\(actionType)&subAction=\(subAction)&sessionId=&subMod="
            print(postString)
            request.httpBody = postString.data(using: .utf8)
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {                                                 // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    DispatchQueue.main.async {
                        //callback("error")
                        print("ATTEMPTING RELOADING -- OOPS")
                    }
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(String(describing: response))")
                }
                
                let responseString = String(data: data, encoding: .utf8)
                print("responseString = \(String(describing: responseString))")
                
                do {
                    //create json object from data
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                        //                    print(json)
                        DispatchQueue.main.async {
                            print("FCM send successfully")
                        }
                    }
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            print("NO NETWORK AVAILABLE")
        }
    }
    
    
    func registerNotificationCenterListeners() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateNotificationCount), name: NSNotification.Name(rawValue: "updateNotificationCount"), object: nil)
    }
    
    func initializeLiveScores() {
        
        ref = Database.database().reference()
        
        databaseHandleAdded = ref.child("live_scores").observe(.childAdded, with: { (snapshot) in
            let response = snapshot.value
            
            if let data = response {
                self.matchesInfo.append(data)
            }
            
            self.refreshData()
        })
        
        databaseHandleChanged = ref.child("live_scores").observe(.childChanged, with: { (snapshot) in
            let response = snapshot.value
            
            if let data = response {
                //                print("RECEIVED CHANGES IN SCORE", data)
                for i in 0...(self.matchesInfo.count - 1) {
                    print(i)
                    let item = self.matchesInfo[i]
                    if (item as! NSDictionary)["id"] as! Int == (data as! NSDictionary)["id"] as! Int {
                        self.matchesInfo[i] = data
                    }
                }
                
                self.refreshData()
            }
        })
        
        databaseHandleRemoved = ref.child("live_scores").observe(.childRemoved, with: { (snapshot) in
            let response = snapshot.value
            
            if let data = response {
                //                print("DATA Removed", data)
                print("RECEIVED REMOVED IN SCORE")
                self.removeElements = []
                for i in 0...(self.matchesInfo.count - 1) {
                    let item = self.matchesInfo[i]
                    if (item as! NSDictionary)["id"] as! Int == (data as! NSDictionary)["id"] as! Int {
                        print("ID Removed", (item as! NSDictionary)["id"] as! Int)
                        self.removeElements.append(i)
                    }
                }
                
                for index in self.removeElements {
                    self.matchesInfo.remove(at: index)
                }
                self.refreshData()
            }
        })
        
        scrollingTimer = Timer.scheduledTimer(timeInterval: 6.0, target: self, selector: #selector(self.startTimer(theTimer:)), userInfo: rowIndex, repeats: true)
    }
    
    @objc func startTimer(theTimer: Timer) {
        //        let numberOfRecords = self.liveMatches.count - 1
        let numberOfRecords = 500 - 1
        if rowIndex < numberOfRecords {
            rowIndex = rowIndex + 1
        } else {
            rowIndex = 0
        }
        if self.liveMatches.count != 0 {
            if rowIndex == 0 {
                self.liveMatchCollectionView.scrollToItem(at: IndexPath(row: rowIndex, section: 0), at: .centeredHorizontally, animated: true)
            } else {
                self.liveMatchCollectionView.scrollToItem(at: IndexPath(row: rowIndex, section: 0), at: .centeredHorizontally, animated: true)
            }
            pagecontrolLive.currentPage = rowIndex % self.liveMatches.count
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == liveMatchCollectionView {
            if self.liveMatches.count != 0 {
                print(Int(scrollView.contentOffset.x) / Int(scrollView.frame.width))
                let section = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
                pagecontrolLive.currentPage = section % self.liveMatches.count
                rowIndex = section
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView != liveMatchCollectionView {
            let section = Int(scrollView.contentOffset.y) / Int(scrollView.frame.height)
            rowIndex = section
//            if self.postListMutable.count > 0 {
//                let lastIndex = self.postListMutable.index(of: self.postListMutable.lastObject!)
//                if lastIndex - 5 == rowIndex {
//                    print("fetching new Posts",rowIndex)
//                    if !isfetchingPost {
//                        isfetchingPost = true
//                        getAllPosts(pageNo: nextPage)
//                    }
//                }
//            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView != liveMatchCollectionView {
            let currentOffset = scrollView.contentOffset.y
            let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
            // Change 10.0 to adjust the distance from bottom
            if maximumOffset - currentOffset <= 2.0 {
                print("reached bottom")
                if !isfetchingPost {
                    isfetchingPost = true
                    loadingDataActivityIndicator.startAnimating()
                    getAllPosts(pageNo: nextPage)
                }
            } else if currentOffset < 0 {
                print("reached top")
                if !isfetchingPost {
                    isfetchingPost = true
                    self.postListMutable.removeAllObjects()
                    loadingDataActivityIndicatorTop.startAnimating()
                    getAllPosts(pageNo: 1)
                }
            }
        }
    }
    
    func refreshData() {
        liveMatches.removeAll()
        completedMatches.removeAll()
        for item in matchesInfo {
            //            if (item as! NSDictionary)["status"] as! Int == 3 {
            //                completedMatches.append(item)
            //            } else {
            liveMatches.append(item)
            //            }
        }
        pagecontrolLive.numberOfPages = liveMatches.count
        liveMatchCollectionView.reloadData()
    }
    
    func setCollectionViewLayout(collectionViews: [UICollectionView]) {
        for collectionView in collectionViews {
            let screensize1 = view.bounds.size
            let cellwidth1 = floor(screensize1.width)
            let cellheight1 = floor(collectionView.bounds.height)
            
            let layout1 = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout1.itemSize = CGSize(width: cellwidth1, height: cellheight1)
            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if liveMatches.count != 0 {
            //            return liveMatches.count
            return 1000
        } else {
            return 1
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "livescoresCell", for: indexPath) as! LiveScoresCollectionViewCell
        if liveMatches.count != 0 {
            let match = liveMatches[indexPath.row % liveMatches.count] as! NSDictionary
            let teamA = (match["team"] as! NSDictionary)["teamA"] as! NSDictionary
            let teamB = (match["team"] as! NSDictionary)["teamB"] as! NSDictionary
            
            cell.venueLabel.hideSkeleton()
            cell.matchDetails.hideSkeleton()
            cell.teamAImageView.hideSkeleton()
            cell.teamBImageView.hideSkeleton()
            cell.teamAName.hideSkeleton()
            cell.teamBName.hideSkeleton()
            cell.teamAscore.hideSkeleton()
            cell.teamBscore.hideSkeleton()
            cell.venueLabel.text = match["type_str"] as? String
            cell.venueLabel.textAlignment = .left
            cell.matchDetails.text = "\(match["trophy"] as! String), \(match["title"] as! String)"
            cell.matchDetails.textAlignment = .left
            
            cell.teamAImageView.sd_setImage(with: URL(string: teamA["logo"] as! String), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            cell.teamBImageView.sd_setImage(with: URL(string: teamB["logo"] as! String), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            
            cell.teamAName.text = teamA["name"] as? String
            cell.teamBName.text = teamB["name"] as? String
            
            if match["status"] as! Int == 3 {
                cell.liveButton.setImage(#imageLiteral(resourceName: "baseline_fiber_manual_record_white_18pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
                cell.liveButton.setTitle("completed matches", for: .normal)
                cell.liveButton.tintColor = CommonUtil.themeRed
            } else {
                cell.liveButton.setImage(#imageLiteral(resourceName: "baseline_fiber_manual_record_white_18pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
                cell.liveButton.setTitle("live matches", for: .normal)
                cell.liveButton.tintColor = UIColor(red:0.07, green:0.67, blue:0.24, alpha:1.0)
            }
            
            if teamA["innings1"] as? NSDictionary != nil {
                if (teamA["innings1"] as! NSDictionary)["status"] as! Int == 1 || (teamA["innings1"] as! NSDictionary)["status"] as! Int == 2 {
                    var score = "\((teamA["innings1"] as! NSDictionary)["run_str"] as! String)"
                    if (teamA["innings2"] as! NSDictionary)["status"] as! Int == 1 || (teamA["innings2"] as! NSDictionary)["status"] as! Int == 2 {
                        score = "\(score) & \((teamA["innings2"] as! NSDictionary)["run_str"] as! String)"
                    }
                    cell.teamAscore.text = score
                } else {
                    cell.teamAscore.text = "-"
                }
            }
            
            if teamB["innings1"] as? NSDictionary != nil {
                if (teamB["innings1"] as! NSDictionary)["status"] as! Int == 1 {
                    var score = "\((teamB["innings1"] as! NSDictionary)["run_str"] as! String)"
                    if (teamB["innings2"] as! NSDictionary)["status"] as! Int == 1 {
                        score = "\(score) & \((teamB["innings2"] as! NSDictionary)["run_str"] as! String)"
                    }
                    cell.teamBscore.text = score
                } else {
                    cell.teamBscore.text = "-"
                }
            }
            cell.resultLabel.text = match["result"] as? String
            cell.resultLabel.textColor = UIColor.white
        } else {
            cell.venueLabel.showAnimatedGradientSkeleton()
            cell.matchDetails.showAnimatedGradientSkeleton()
            cell.teamAImageView.showAnimatedGradientSkeleton()
            cell.teamAName.showAnimatedGradientSkeleton()
            cell.teamBImageView.showAnimatedGradientSkeleton()
            cell.teamBName.showAnimatedGradientSkeleton()
            cell.teamAscore.showAnimatedGradientSkeleton()
            cell.teamBscore.showAnimatedGradientSkeleton()
        }
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "cricSpace_live_fullscore" {
            let fullScore = segue.destination as! UINavigationController
            let fullScoreVC = fullScore.topViewController as? FullScoreCardViewController
            fullScoreVC?.isCalledFromInternational = true
            fullScoreVC?.matchTemp = matchTemp
            fullScoreVC?.teamATemp = teamATemp
            fullScoreVC?.teamBTemp = teamBTemp
        } else if segue.identifier == "cricspace_comment" {
            let commentVC = segue.destination as! CommentPageViewController
            commentVC.postID = tempPostID
            commentVC.post = tempPost
            commentVC.commentToEdit = commentToEdit
            commentVC.commentEdit = commentEdit
        } else if segue.identifier == "cricspace_postnow" {
            let postVC = segue.destination as! PostNowViewController
            postVC.openCam = openCamera
            if openCamera {
                postVC.image = image
                postVC.imageData = imageData
            }
            postVC.editPost = editPost
            postVC.postID = tempPostID
        } else if segue.identifier == "cricspace_likesDetails" {
            let likeVC = segue.destination as! LikesListViewController
            likeVC.postID = tempPostID
            likeVC.post = tempPost
            likeVC.likeType = likeType
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if liveMatches.count != 0 {
            let match = liveMatches[indexPath.row % liveMatches.count] as! NSDictionary
            let teamA = (match["team"] as! NSDictionary)["teamA"] as! NSDictionary
            let teamB = (match["team"] as! NSDictionary)["teamB"] as! NSDictionary
            
            matchTemp = match
            teamATemp = teamA
            teamBTemp = teamB
            
            UserDefaults.standard.set(teamA["name"] as! String, forKey: CommonUtil.CURRENT_MATCH_TEAM_A_NAME)
            UserDefaults.standard.set(teamB["name"] as! String, forKey: CommonUtil.CURRENT_MATCH_TEAM_B_NAME)
            
            self.performSegue(withIdentifier: "cricSpace_live_fullscore", sender: self)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width / CGFloat(1), height: collectionView.bounds.height / CGFloat(1))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    func addLikes(postID: String,six: Int, four:Int, out:Int, post: NSDictionary) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"POST_ID\":\"\(postID)\",\"SIX\":\"\(six)\",\"FOUR\":\"\(four)\",\"WICKET\":\"\(out)\"}", mod: "CricSpace", actionType: "add-post-like") { (response) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    let xscData = ((response as! NSDictionary)["XSCData"] as! NSDictionary)
                    
                    print(xscData)
                    
                    DispatchQueue.main.async{
                        self.editPostDetail(postID: postID)
                    }
                } else {
                    print((response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                print("Something went wrong! Please try again")
            }
            
        }
    }
    
    func getAllPosts(pageNo: Int) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"CURRENT_PAGE\":\"\(pageNo)\"}", mod: "CricSpace", actionType: "get-all-posts") { (response) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    let xscData = ((response as! NSDictionary)["XSCData"] as! NSDictionary)
                    
                    print(xscData)
                    
                    let posts = xscData["POST"] as? NSArray
                    if xscData["NEXT_PAGE"] as? Int ?? 0 != 0 {
                        self.nextPage = xscData["NEXT_PAGE"] as? Int ?? Int(xscData["NEXT_PAGE"] as! String)!
                        
                        if pageNo == 1 {
                            self.postListMutable.addObjects(from: posts as! [Any])
                            self.reloadAndResizeTable()
                            
                        } else {
                            self.reloadAndResizeTableOneByOne(posts: posts ?? [])
                            
                        }
                        
                    }
                } else {
                    print((response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                print("Something went wrong! Please try again")
            }
            self.loadingDataActivityIndicator.stopAnimating()
            self.loadingDataActivityIndicatorTop.stopAnimating()
        }
    }
    
    func reloadAndResizeTable() {
        self.newsFeedTableView.reloadData()
        self.newsFeedTableViewHeight?.constant = self.newsFeedTableView.contentSize.height
        self.view.layoutIfNeeded()
        self.isfetchingPost = false
        self.scrollview.setContentOffset(CGPoint(x: 0, y: self.scrollview.contentOffset.y), animated: false)
    }
    
    func reloadAndResizeTableOneByOne(posts: NSArray) {
        print("Mutable Array Before",self.postListMutable.count,"Post count",posts.count)
        
        //        self.postListMutable.addObjects(from: posts as! [Any])
        for post in posts {
            self.newsFeedTableView.beginUpdates()
            self.postListMutable.add(post)
            self.newsFeedTableView.insertSections(IndexSet.init(integer: self.postListMutable.count - 1), with: .bottom)
            self.newsFeedTableViewHeight?.constant = self.newsFeedTableView.contentSize.height
            self.view.layoutIfNeeded()
            self.newsFeedTableView.endUpdates()
        }
        self.isfetchingPost = false
        print("Mutable Array After",self.postListMutable.count)
        self.scrollview.setContentOffset(CGPoint(x: 0, y: self.scrollview.contentOffset.y), animated: true)
    }
    
    @objc func showMyProfile(){
        backgroundView.isHidden = false
        print("Showing my profile in cricSpace")
        navigationController?.setNavigationBarHidden(true, animated: true)
        guard
            let myProfileVC = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "MyProfileViewController") as? MyProfileViewController
            else { return }
        addPullUpController(myProfileVC)
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double, label: UILabel, postDate: String) {
        let lat: Double = pdblLatitude
        var area = ""
        let lon: Double = pdblLongitude
        if (Reachability.isConnectedToNetwork()) {
            let url = NSURL(string: "\(CommonUtil.baseUrl)latlng=\(lat),\(lon)&key=\(CommonUtil.apikey)")
            let data = NSData(contentsOf: url! as URL)
            let json = try! JSONSerialization.jsonObject(with: data! as Data, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
            
            if let result = json["results"] as? NSArray {
                print("ADRESSSSS ARRAY", result)
                if result.count != 0 {
                    if let address = (result[0] as! NSDictionary)["address_components"] as? NSArray {
                        DispatchQueue.main.async {
                            for add in address {
                                let locality = (add as! NSDictionary)["types"] as? NSArray ?? []
                                for local in locality {
                                    if (local as? String) == "locality" {
                                        area = (add as! NSDictionary)["short_name"] as? String ?? ""
                                        break
                                    } else if (local as? String) == "administrative_area_level_2" {
                                        area = (add as! NSDictionary)["short_name"] as? String ?? ""
                                        break
                                    } else if (local as? String) == "administrative_area_level_1" {
                                        area = (add as! NSDictionary)["long_name"] as? String ?? ""
                                        break
                                    } else if (local as? String) == "country" {
                                        area = (add as! NSDictionary)["long_name"] as? String ?? ""
                                        break
                                    }
                                }
                            }
                            label.text = "\(area) \u{2022} \(postDate)"
                        }
                    }
                }
            }
        }
    }
    
    @objc func newPostedAddedRefreshData(_ notification: NSNotification) {
        let postDetail =  notification.userInfo![AnyHashable("post")] as? NSDictionary
        let postid = postDetail?["POST_ID"] as? String ?? "\(postDetail?["POST_ID"] as! Int)"
        addPostDetails(postID: postid)
    }
    
    @objc func editPostDetailRefreshData(_ notification: NSNotification) {
        let postid =  notification.userInfo![AnyHashable("post")] as! String
        commentToEdit = [:]
        editPostDetail(postID: postid)
    }
    
    @objc func removeReportedPost(_ notification: NSNotification) {
        let postid =  notification.userInfo![AnyHashable("post")] as! String
        removeReportFunction(postID: postid)
    }
    
    @objc func removeReportedComment(_ notification: NSNotification) {
        let postid =  notification.userInfo![AnyHashable("post")] as! String
        editPostDetail(postID: postid)
    }
    
    @objc func deletePostFromCommentPage(_ notification: NSNotification) {
        let postid =  notification.userInfo![AnyHashable("post")] as! String
        removeReportFunction(postID: postid)
    }
    
    
    @objc func blockedUserFunctionFromCommentPage(_ notification: NSNotification) {
        let userid =  notification.userInfo![AnyHashable("userid")] as! String
        self.showAlert(title: "Block", message: "User has been blocked, you wont see any other posts from this user and also this user won't be able to contact you or see your posts")
        blockedUserFunction(UserID: userid)
    }
    
    
    func blockedUserFunction(UserID: String) {
        //        self.postListMutable.removeAllObjects()
        //        self.newsFeedTableView.reloadData()
        //        getAllPosts(pageNo: 1)
        print("User blocked")
        let postList = self.postListMutable
        var x = 0
        repeat {
            let post = postList[x]
            if (post as! NSDictionary)["IS_TYPE"] as! String == "POST" {
                if (post as! NSDictionary)["USER_ID"] as? String ?? "\((post as! NSDictionary)["USER_ID"] as! Int)" == UserID {
                    self.postListMutable.remove(post)
                }
            }
            x = x + 1
        } while(x < postList.count)
        self.reloadAndResizeTable()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    
    func removeReportFunction(postID: String) {
        let postList = self.postListMutable
        var x = 0
        repeat {
            let post = postList[x]
            if (post as! NSDictionary)["IS_TYPE"] as! String == "POST" {
                if (post as! NSDictionary)["POST_ID"] as? String ?? "\((post as! NSDictionary)["POST_ID"] as! Int)" == postID {
                    self.postListMutable.remove(post)
                }
            }
            x = x + 1
        } while(x < postList.count)
        self.reloadAndResizeTable()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func addPostDetails(postID: String) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"POST_ID\":\"\(postID)\",\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "CricSpace", actionType: "get-post-details") { (response) in
            print("post details" ,response)
            
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    let postDetails = (response as! NSDictionary)["XSCData"] as! NSDictionary
                    self.postListMutable.insert(postDetails, at: 0)
                    self.reloadAndResizeTable()
                } else {
                    print((response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                print("Something went wrong! Please try again")
            }
        }
    }
    
    func editPostDetail(postID: String) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"POST_ID\":\"\(postID)\",\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "CricSpace", actionType: "get-post-details") { (response) in
            print("post details" ,response)
            
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    let postDetails = (response as! NSDictionary)["XSCData"] as! NSDictionary
                    let postList = self.postListMutable
                    var x = 0
                    repeat {
                        let post = postList[x]
                        if (post as! NSDictionary)["IS_TYPE"] as! String == "POST" {

                        if (post as! NSDictionary)["POST_ID"] as? String ?? "\((post as! NSDictionary)["POST_ID"] as! Int)" == postID {
                            self.postListMutable.remove(post)
                            self.postListMutable.insert(postDetails, at: x)
                            self.newsFeedTableView.reloadSections(IndexSet.init(integer: x), with: .none)
                        }
                        }
                        x = x + 1
                    } while(x < postList.count)
//                    self.reloadAndResizeTable()
                } else {
                    print((response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                print("Something went wrong! Please try again")
            }
        }
    }
    
    func deletePost(postID: String) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"POST_ID\":\"\(postID)\",\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "CricSpace", actionType: "delete-post") { (response) in
            print("delete post" ,response)
            
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    let postList = self.postListMutable
                    var x = 0
                    repeat {
                        let post = postList[x]
                        if (post as! NSDictionary)["IS_TYPE"] as! String == "POST" {
                            if (post as! NSDictionary)["POST_ID"] as? String ?? "\((post as! NSDictionary)["POST_ID"] as! Int)" == postID {
                                self.postListMutable.remove(post)
                            }
                        }
                        x = x + 1
                    } while(x < postList.count)
                    self.reloadAndResizeTable()
                } else {
                    self.showAlert(title: "Post", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops", message: "Something went wrong! Please try again")
            }
        }
    }
    
    func reportPost(postID: String) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let reportVC = storyBoard.instantiateViewController(withIdentifier: "ReportPageViewController") as! ReportPageViewController
        reportVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        reportVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        reportVC.postID = postID
        self.present(reportVC, animated: true, completion: nil)
    }
    
    func blockPost(USER_ID: String, postID: String) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"IS_BLOCK\":\"1\",\"REFERENCE_ID\":\"\(USER_ID)\"}", mod: "Connections", actionType: "update-blocking") { (response) in
            print("block post",response )
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.showAlert(title: "Block", message: "User has been blocked, you wont see any other posts from this user and also this user won't be able to contact you or see your posts")
                    self.blockedUserFunction(UserID: USER_ID)
                } else {
                    self.showAlert(title: "Block", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops", message: "Something went wrong! Please try again")
            }
        }
    }
    
    @objc func openTeamVCFromPlayerProfile() {
        let selectedTeam = UserDefaults.standard.object(forKey: CommonUtil.SELECTEDTEAMFROMPLAYERPROFILE) as! Data
        let decodedselectedTeam = NSKeyedUnarchiver.unarchiveObject(with: selectedTeam) as! NSDictionary
        getTeamProfile(addressID: decodedselectedTeam["ADDRESS_ID"] as! String, address: decodedselectedTeam["ADDRESS"] as! String, isCalledFromPlayerProfile: true)
    }
    
    func getTeamProfile(addressID: String, address: String, isCalledFromPlayerProfile: Bool) {
        if isCalledFromPlayerProfile {
            let selectedTeam = UserDefaults.standard.object(forKey: CommonUtil.SELECTEDTEAMFROMPLAYERPROFILE) as! Data
            let decodedselectedTeam = NSKeyedUnarchiver.unarchiveObject(with: selectedTeam) as! NSDictionary
            currentAdressID = addressID
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"TEAM\",\"TEAM_ID\":\"\(decodedselectedTeam["TEAM_ID"] as! String)\"}", mod: "Team", actionType: "dashborad-team-profile") { (response: Any) in
                print(response)
                if response as? String != "error" {
                    self.showTeamProfile(response: response, address: address, isCalledFromPlayerProfile: true)
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
            }
        } else {
            currentAdressID = addressID
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"TEAM\",\"ADDRESS_ID\":\"\(addressID)\"}", mod: "Map", actionType: "venue-items") { (response: Any) in
                print(response)
                if response as? String != "error" {
                    self.showTeamProfile(response: response, address: address, isCalledFromPlayerProfile: false)
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
            }
        }
    }
    
    private func showTeamProfile(response: Any, address: String, isCalledFromPlayerProfile: Bool) {
        backgroundView.isHidden = false
        guard
            let teamVC = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "TeamProfileViewController") as? TeamProfileViewController
            else { return }
        teamVC.teamDetails = response
        teamVC.address = address
        teamVC.currentAddressID = currentAdressID
        teamVC.isCalledFromPlayerProfile = isCalledFromPlayerProfile
        addPullUpController(teamVC)
    }
    
    @objc func ShowTossVCFunction(_ notification: NSNotification) {
        if CommonUtil.NewLiveMatchFlow {
            let showTossVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LiveMatchViewController") as? LiveMatchViewController
            showTossVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            showTossVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            showTossVC?.matchInfo = notification.userInfo![AnyHashable("matchInfo")]
            showTossVC?.MATCH_ID = notification.userInfo![AnyHashable("Match_ID")] as? String
            showTossVC?.isFromDashBoard = false
            self.present(showTossVC!, animated: true, completion: nil)
        } else {
            let showTossVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LiveMatchViewController") as? LiveMatchViewController
            showTossVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            showTossVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            showTossVC?.matchInfo = notification.userInfo![AnyHashable("matchInfo")]
            self.present(showTossVC!, animated: true, completion: nil)
        }
    }
    
    @objc func joinMatchVCFunction() {
        let joinMatchVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JoinMatchViewController") as? JoinMatchViewController
        joinMatchVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        joinMatchVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(joinMatchVC!, animated: true, completion: nil)
    }
    
    @objc func UpcomingMatch(_ notification: NSNotification) {
        let upcomingMatchinfo = (notification.userInfo![AnyHashable("upcoming")] as! NSArray)[0] as! NSDictionary
        print("Upcoming Match info", upcomingMatchinfo)
        
        let dateFormatter = DateFormatter()
        let userCalendar = Calendar.current
        let requestedComponent: Set<Calendar.Component> = [.month,.day,.hour,.minute,.second]
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //        dateFormatter.dateFormat = "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'"
        //        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let startTime = Date()
        let endTime = dateFormatter.date(from: "\(upcomingMatchinfo["DATE"] as! String) \(upcomingMatchinfo["TIME"] as! String)")
        //        let endTime = dateFormatter.date(from: "\(upcomingMatchinfo["isodate"] as! String)")
        let timeDifference = userCalendar.dateComponents(requestedComponent, from: startTime, to: endTime!)
        
        print("time difference", timeDifference.month,timeDifference.day,timeDifference.hour,timeDifference.minute)
        if CommonUtil.NewLiveMatchFlow {
            let dateFormatter =  DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let date1 = dateFormatter.date(from: upcomingMatchinfo["CURRENT_TIME"] as! String)
            // return the timeZone of your device i.e. America/Los_angeles
            let timeZone = TimeZone.autoupdatingCurrent.identifier as String
            dateFormatter.timeZone = TimeZone(identifier: timeZone)
            let date2 = dateFormatter.string(from: date1!)
            print("LOCAL TIME", date2)
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSzzz"
            let date = dateFormatter1.date(from: date2)
            
            let startTime = date
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let endTime = dateFormatter2.date(from: "\(upcomingMatchinfo["DATE"] as! String) \(upcomingMatchinfo["TIME"] as! String)")
            //        let endTime = dateFormatter.date(from: "\(upcomingMatchinfo["isodate"] as! String)")
            
            let timeDifference = userCalendar.dateComponents(requestedComponent, from: startTime!, to: endTime!)
            
            print("time difference", timeDifference.month,timeDifference.day,timeDifference.hour,timeDifference.minute)
            timeLeftForMatch = (timeDifference.hour! * 60) + timeDifference.minute!
            if timeDifference.month! <= 0 {
                if timeDifference.day! <= 0 {
                    if timeDifference.hour! <= 0 {
                        if timeDifference.minute! <= 15 {
                            SocketConnectionsclass.matchStatusEmit(match_id:"\(upcomingMatchinfo["MATCH_ID"] as! Int)")
                        } else {
                            _ = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(sendUpcomminingMatchEmit), userInfo: nil, repeats: false)
                        }
                    } else {
                        //                        _ = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(sendUpcomminingMatchEmit), userInfo: nil, repeats: false)
                    }
                }
            }
        } else {
            if timeDifference.month! <= 0 {
                if timeDifference.day! <= 0 {
                    if timeDifference.hour! <= 0 {
                        if timeDifference.minute! <= 30 {
                            SocketConnectionsclass.matchStatusEmit(match_id:"\(upcomingMatchinfo["MATCH_ID"] as! Int)")
                        }
                    }
                }
            }
        }
    }
    
    @objc func sendUpcomminingMatchEmit() {
        NSLog("SENDING upcoming match update")
        SocketConnectionsclass.getMyUpcomingMatches()
    }
    
    @objc func matchAdminDetail(_ notification: NSNotification) {
        if CommonUtil.NewLiveMatchFlow {
            let adminDetail = (notification.userInfo![AnyHashable("matchAdminDetail")] as! NSArray)[0] as! NSDictionary
            print("Match Admin Detail", adminDetail)
            if String(adminDetail["ADMIN"] as! Int) == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String || String(adminDetail["SUPER_ADMIN"] as! Int) == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String{
                
                switch (adminDetail["STEP_NAME"] as! String) {
                    
                case "TOSS": let tossVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "GoForTaskViewController") as? GoForTaskViewController
                tossVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                tossVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                tossVC?.prodSeconds = timeLeftForMatch * 60
                tossVC?.matchID = adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)"
                self.present(tossVC!, animated: true, completion: nil)
                    break
                    
                case "toss_won_team":
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissGoForTossView"), object: nil)
                    let showTossVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "TossWonByViewController") as? TossWonByViewController
                    showTossVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    showTossVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    showTossVC?.matchID = adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)"
                    showTossVC?.adminDetail = adminDetail
                    self.present(showTossVC!, animated: true, completion: nil)
                    print("Go to toss result")
                    
                    let matchAdminDetail:[String: Any] = ["matchAdminDetail": adminDetail]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshTossPage"), object: nil, userInfo: matchAdminDetail)
                    break
                    
                case "over_count":
                    break
                    
                case "toss_won_selection":
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissTossWonView"), object: nil)
                    let batOrBowlVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "ChooseBowlingOrBattingViewController") as? ChooseBowlingOrBattingViewController
                    batOrBowlVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    batOrBowlVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    batOrBowlVC?.matchID = adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)"
                    batOrBowlVC?.adminDetail = adminDetail
                    self.present(batOrBowlVC!, animated: true, completion: nil)
                    let matchAdminDetail:[String: Any] = ["matchAdminDetail": adminDetail]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshBatBowlPage"), object: nil, userInfo: matchAdminDetail)
                    
                    break
                    
                case "BAT_USER_ID":
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissBatBowlView"), object: nil)
                    getPlayerListForMatch(adminDetail: adminDetail)
                    break
                    
                case "bowler":
                    break
                    
                case "striker":
                    break
                    
                case "non-striker":
                    break
                    
                case "Scorer_Updated":
                    let teamScoreVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "TeamScorerViewController") as! TeamScorerViewController
                    teamScoreVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    teamScoreVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    teamScoreVC.MATCH_ID =  adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)"
                    self.present(teamScoreVC, animated: true, completion: nil)
                    break
                    
                case "change-scorer":
                    break
                    
                case "SCORE_BOARD":
                    if adminDetail["SCORER"] as? String ?? "\(adminDetail["SCORER"] as! Int)" == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String {
                        let teamScoreVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "TeamScorerViewController") as! TeamScorerViewController
                        teamScoreVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        teamScoreVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        teamScoreVC.MATCH_ID =  adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)"
                        self.present(teamScoreVC, animated: true, completion: nil)
                    }
                    break
                    
                default: print("DEFAULT")
                    break
                }
            }
        } else {
            let adminDetail = (notification.userInfo![AnyHashable("matchAdminDetail")] as! NSArray)[0] as! NSDictionary
            print("Match Admin Detail", adminDetail)
            
            if String(adminDetail["SCORER"] as! Int) == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String {
                
                print("Goto Step ", adminDetail["STEP_NAME"] as! String)
                if adminDetail["STEP_NAME"] as! String == "SCORE_BOARD" {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissNonScorerView"), object: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissMyProfileFunction"), object: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissMatchDetailView"), object: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissTeamProfileFunction"), object: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissPlayerProfileFunction"), object: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissFixedMatchesFunction"), object: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissOpenMatchesFunction"), object: nil)
                    let alert = UIAlertController(title: "Match Alert", message: "You have a match to be played! Do you want to continue match?", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: { action in
                        let scorerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScorerViewController") as? ScorerViewController
                        scorerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        scorerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        scorerVC?.MATCH_ID = adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)"
                        self.stopTimer()
                        self.present(scorerVC!, animated: true, completion: nil)
                    }))
                    alert.addAction(UIAlertAction(title: "Later", style: UIAlertActionStyle.cancel, handler:{ action in
                        print("No")
                        self.startTimer(match_ID: adminDetail["MATCH_ID"] as? String ?? String(adminDetail["MATCH_ID"] as! Int))
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    print("Go to non scorer activity")
                    alertNonScorer = UIAlertController(title: "Match Alert", message: "You have a match to be played! Do you want to go for toss?", preferredStyle: UIAlertControllerStyle.alert)
                    alertNonScorer?.addAction(UIAlertAction(title: "Toss", style: UIAlertActionStyle.default, handler: { action in
                        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\":\"\(adminDetail["MATCH_ID"] as! String)\"}", mod: "Match", actionType: "get-match-teams", callback: { (response) in
                            print(response)
                            if response as? String != "error" {
                                let showTossVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LiveMatchViewController") as? LiveMatchViewController
                                showTossVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                                showTossVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                                showTossVC?.matchInfo = response
                                showTossVC?.MATCH_ID = adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)"
                                showTossVC?.isFromDashBoard = true
                                self.stopTimer()
                                self.present(showTossVC!, animated: true, completion: nil)
                            } else {
                                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                            }
                        })
                        
                    }))
                    alertNonScorer?.addAction(UIAlertAction(title: "Later", style: UIAlertActionStyle.cancel, handler:{ action in
                        print("No")
                        self.startTimer(match_ID: adminDetail["MATCH_ID"] as! String)
                    }))
                    self.present(alertNonScorer!, animated: true, completion: nil)
                }
            }
        }
    }
    
    func getPlayerListForMatch(adminDetail: NSDictionary) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\":\"\(adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)")\"}", mod: "MatchScore", actionType: "match-score-details", callback: { (response) in
            print(response)
            if response as? String != "error" {
                let toss = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["toss"] as! NSDictionary
                let team = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["team"] as! NSDictionary
                let teamA = team["teamA"] as! NSDictionary
                let teamB = team["teamB"] as! NSDictionary
                var battingOrder: NSArray = []
                var bowlingOrder: NSArray = []
                
                if teamA["match_team_id"] as? String ?? "\(teamA["match_team_id"] as! Int)" == toss["won"] as? String ?? "\(toss["won"] as! Int)" {
                    if toss["decide"] as! String == "BAT" {
                        battingOrder = teamA["players"] as! NSArray
                        bowlingOrder = teamB["players"] as! NSArray
                    } else {
                        battingOrder = teamB["players"] as! NSArray
                        bowlingOrder = teamA["players"] as! NSArray
                    }
                    
                } else if teamB["match_team_id"] as? String ?? "\(teamB["match_team_id"] as! Int)" == toss["won"] as? String ?? "\(toss["won"] as! Int)" {
                    if toss["decide"] as! String == "BAT" {
                        battingOrder = teamB["players"] as! NSArray
                        bowlingOrder = teamA["players"] as! NSArray
                    } else {
                        battingOrder = teamA["players"] as! NSArray
                        bowlingOrder = teamB["players"] as! NSArray
                    }
                }
                
                if adminDetail["BAT_USER_ID"] as? String ?? "\(adminDetail["BAT_USER_ID"] as! Int)" == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as? String
                {
                    // Go to batting flow
                    let strikerVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "NewChooseStrikerViewController") as? NewChooseStrikerViewController
                    strikerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    strikerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    strikerVC?.matchID = adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)"
                    strikerVC?.adminDetail = adminDetail
                    strikerVC?.playerList = battingOrder
                    self.present(strikerVC!, animated: true, completion: nil)
                } else {
                    // Go to bowling flow
                    let bowlerVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "ChooseABowlerViewController") as? ChooseABowlerViewController
                    bowlerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    bowlerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    bowlerVC?.matchID = adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)"
                    bowlerVC?.adminDetail = adminDetail
                    bowlerVC?.playerList = bowlingOrder
                    self.present(bowlerVC!, animated: true, completion: nil)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
        })
    }
    
    func startTimer(match_ID: String) {
        timer?.invalidate()   // just in case you had existing `Timer`, `invalidate` it before we lose our reference to it
        timer = Timer.scheduledTimer(withTimeInterval: 60.0, repeats: true) { [weak self] _ in
            SocketConnectionsclass.matchStatusEmit(match_id:"\(match_ID)")
        }
    }
    
    func stopTimer() {
        print("Stoping Timer")
        timer?.invalidate()
    }
    
    deinit {
        stopTimer()
    }
    
    @objc func openPlayerVCFromTeamProfile() {
        let selectedPlayer = UserDefaults.standard.object(forKey: CommonUtil.SELECTEDPLAYERFROMTEAMPROFILE) as! Data
        let decodedselectedPlayer = NSKeyedUnarchiver.unarchiveObject(with: selectedPlayer) as! NSDictionary
        getPlayerProfile(addressID: decodedselectedPlayer["ADDRESS_ID"] as! String, address: decodedselectedPlayer["ADDRESS"] as! String, isCalledFromTeamProfile: true)
    }
    
    func getPlayerProfile(addressID: String, address: String, isCalledFromTeamProfile: Bool) {
        var isCached = false
        currentAdressID = addressID
        if let response = UserDefaults.standard.object(forKey: "PLAYERPROFILE_\(currentAdressID!)") {
            print("Player profile loaded from cache")
            isCached = true
            self.showPlayerProfile(response: response, address: address, isCalledFromTeamProfile: isCalledFromTeamProfile)
        }
        if isCalledFromTeamProfile {
            let selectedPlayer = UserDefaults.standard.object(forKey: CommonUtil.SELECTEDPLAYERFROMTEAMPROFILE) as! Data
            let decodedselectedPlayer = NSKeyedUnarchiver.unarchiveObject(with: selectedPlayer) as! NSDictionary
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"PLAYER\",\"USER_ID\":\"\(decodedselectedPlayer["USER_ID"] as! String)\"}", mod: "Player", actionType: "dashborad-player-profile") { (response: Any) in
                print("Player Response", response)
                if response as? String != "error" {
                    print("Player profile updated")
                    UserDefaults.standard.set(response, forKey: "PLAYERPROFILE_\(self.currentAdressID!)")
                    if isCached {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshPlayerProfile"), object: nil)
                    } else {
                        self.showPlayerProfile(response: response, address: address, isCalledFromTeamProfile: isCalledFromTeamProfile)
                    }
                    
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
            }
        } else {
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"PLAYER\",\"ADDRESS_ID\":\"\(addressID)\"}", mod: "Map", actionType: "venue-items") { (response: Any) in
                if response as? String != "error" {
                    print("Player profile updated")
                    UserDefaults.standard.set(response, forKey: "PLAYERPROFILE_\(self.currentAdressID!)")
                    if isCached {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshPlayerProfile"), object: nil)
                    } else {
                        self.showPlayerProfile(response: response, address: address, isCalledFromTeamProfile: isCalledFromTeamProfile)
                    }
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
            }
        }
    }
    
    private func showPlayerProfile(response: Any, address: String, isCalledFromTeamProfile: Bool) {
        backgroundView.isHidden = false
        guard
            let playerVC = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "PlayerProfileViewController") as? PlayerProfileViewController
            else { return }
        playerVC.playerDetails = response
        playerVC.address = address
        playerVC.currentAddressID = currentAdressID
        playerVC.isCalledFromTeamProfile = isCalledFromTeamProfile
        addPullUpController(playerVC)
    }
    
    @objc func openMatchesDetailVC(_ notification: NSNotification) {
        backgroundView.isHidden = false
        guard
            let matchDetailVC = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "OpenMatchesDetailsViewController") as? OpenMatchesDetailsViewController
            else { return }
        
        matchDetailVC.matchInfo = notification.userInfo![AnyHashable("data")] as? NSDictionary
        addPullUpController(matchDetailVC)
    }
    
    @objc func sendMessage(_ notification: NSNotification) {
        print("Received notification", notification.userInfo![AnyHashable("data")] as! NSDictionary)
        let data = notification.userInfo![AnyHashable("data")] as! NSDictionary
        let msgbody = data["MSG"] as! String
        let JIDPrefix = data["JID"] as! String
        var clientJid: XMPPJID!
        clientJid = XMPPJID.init(string: "\(JIDPrefix)\(CommonUtil.cricdost_chat_domain_name)")
        let senderJID = clientJid
        let msg = XMPPMessage(type: "chat", to: senderJID)
        msg.addBody(msgbody)
        msg.addSubject(UserDefaults.standard.object(forKey: CommonUtil.IMAGE_URL) as! String)
        stream?.send(msg)
    }
    
    @objc func sendComposing(_ notification: NSNotification) {
        fetchChatHistory()
        print("Received Composing", notification.userInfo![AnyHashable("data")] as! NSDictionary)
        let data = notification.userInfo![AnyHashable("data")] as! NSDictionary
        //        let msgbody = data["MSG"] as! String
        let JIDPrefix = data["JID"] as! String
        let message = XMLElement.element(withName: "message") as? XMLElement
        message?.addAttribute(withName: "type", stringValue: "chat")
        message?.addAttribute(withName: "to", stringValue: "\(JIDPrefix)\(CommonUtil.cricdost_chat_domain_name)")
        let xmppMessage = XMPPMessage(from: message!)
        xmppMessage.addComposingChatState()
        stream?.send(xmppMessage)
    }
    
    @objc func joinGroupChat(_ notification: NSNotification) {
        
        let data = notification.userInfo![AnyHashable("data")] as! NSDictionary
        let rosterstorage = XMPPRoomMemoryStorage()
        xmppRoom = XMPPRoom(roomStorage: rosterstorage!, jid: XMPPJID(string: "\(data["groupJID"] as! String)\(CommonUtil.cricdost_group_chat_domain_name)")!, dispatchQueue: DispatchQueue.main)
        xmppRoom?.configureRoom(usingOptions: nil)
        xmppRoom?.activate(stream!)
        
        xmppRoom?.addDelegate(self.xmppController!, delegateQueue: DispatchQueue.main)
        xmppRoom?.join(usingNickname: "\((stream?.myJID?.user!)!)-\(UserDefaults.standard.object(forKey: CommonUtil.FULL_NAME) as! String)", history: nil, password: nil)
        xmppRoom?.fetchConfigurationForm()
    }
    
    func fetchChatHistory() {
        let query = try? XMLElement(xmlString: "<query xmlns='jabber:iq:roster'/>")
        let iq = XMLElement.element(withName: "iq") as? XMLElement
        iq?.addAttribute(withName: "type", stringValue: "get")
        iq?.addAttribute(withName: "id", stringValue: "u_2")
        iq?.addAttribute(withName: "from", stringValue: "u_2\(CommonUtil.cricdost_chat_domain_name)")
        if let aQuery = query {
            iq?.addChild(aQuery)
        }
        stream?.send(iq!)
    }
    
    @objc func sendMessageGroup(_ notification: NSNotification) {
        let data = notification.userInfo![AnyHashable("data")] as! NSDictionary
        let xmppMessage = XMPPMessage(type: "groupchat", to: XMPPJID(string: "\(data["groupJID"] as! String)\(CommonUtil.cricdost_group_chat_domain_name)"))
        xmppMessage.addSubject(data["SUBJECT"] as! String)
        xmppMessage.addBody(data["MSG"] as! String)
        xmppRoom?.send(xmppMessage)
    }
    
    @objc func connectXMPP() {
        self.xmppController?.connect()
    }
    
    @objc func disconnectXMPP() {
        stream?.disconnect()
    }
    
    @objc func refreshDrawerMsgCount() {
        rootNav?.refreshDrawerMessageCount()
    }
    
    @objc func openMatchOverViewController(_ notification: NSNotification) {
        let tossDetail =  notification.userInfo![AnyHashable("tossDetail")] as? NSDictionary
        let overVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "HowManyOversViewController") as! HowManyOversViewController
        overVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        overVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        overVC.tossDetail = tossDetail
        self.present(overVC, animated: true, completion: nil)
    }
    
    @objc func openNonStrikerViewController(_ notification: NSNotification) {
        let playerlist =  notification.userInfo![AnyHashable("playerList")] as? NSArray
        let nonScorerVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "ChooseNonStrikerViewController") as! ChooseNonStrikerViewController
        nonScorerVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nonScorerVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        nonScorerVC.playerlist = playerlist
        nonScorerVC.matchID = notification.userInfo![AnyHashable("match_id")] as? String
        nonScorerVC.striker = notification.userInfo![AnyHashable("striker")] as? String
        self.present(nonScorerVC, animated: true, completion: nil)
    }
    
    @objc func openChooseScorerViewController(_ notification: NSNotification) {
        let playerlist =  notification.userInfo![AnyHashable("playerList")] as? NSArray
        let ScorerVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "ChooseScorerViewController") as! ChooseScorerViewController
        ScorerVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        ScorerVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        ScorerVC.playerlist = playerlist
        ScorerVC.matchID = notification.userInfo![AnyHashable("match_id")] as? String
        ScorerVC.striker = notification.userInfo![AnyHashable("striker")] as? String
        ScorerVC.nonStriker = notification.userInfo![AnyHashable("non-striker")] as? String
        self.present(ScorerVC, animated: true, completion: nil)
    }
    
    @objc func openTeamScoreViewController(_ notification: NSNotification) {
        let teamScoreVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "TeamScorerViewController") as! TeamScorerViewController
        teamScoreVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        teamScoreVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        teamScoreVC.MATCH_ID = notification.userInfo![AnyHashable("match_id")] as? String
        self.present(teamScoreVC, animated: true, completion: nil)
    }
    
    @objc func openScorerViewController(_ notification: NSNotification) {
        let scorerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScorerViewController") as? ScorerViewController
        scorerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        scorerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        scorerVC?.MATCH_ID = notification.userInfo![AnyHashable("match_id")] as? String
        self.present(scorerVC!, animated: true, completion: nil)
    }
    
    @objc func openNotificationPage() {
        print("Opening NotificationPage")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier:
            "NotificationViewController") as! NotificationViewController
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
    
    func deleteComment(commentID: String ,postID: String) {
        
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"POST_ID\":\"\(postID)\",\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"COMMENT_ID\":\"\(commentID)\"}", mod: "CricSpace", actionType: "delete-comment") { (response) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.editPostDetail(postID: postID)
                } else {
                    self.showAlert(title: "comment", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops", message: "Something went wrong! Please try again")
            }
            
        }
    }
    
    func reportComment(commentID: String , postID: String) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let reportVC = storyBoard.instantiateViewController(withIdentifier: "ReportPageViewController") as! ReportPageViewController
        reportVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        reportVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        reportVC.postID = postID
        reportVC.commentID = commentID
        reportVC.reportCommentFlag = true
        self.present(reportVC, animated: true, completion: nil)
    }
    
    func blockComment(UserID: String, postID: String) {
        blockPost(USER_ID: UserID, postID: postID)
    }
    
    func createNotification() {
        print("notification")
        let center = UNUserNotificationCenter.current()
        let content = UNMutableNotificationContent()
        content.title = "CricDost"
        content.subtitle = "Greetings from CricDost..!!"
        content.body = "Hey Dost..."
        content.sound = UNNotificationSound.default()
        let triger = UNTimeIntervalNotificationTrigger(timeInterval: 5.0, repeats: false)
        let request = UNNotificationRequest(identifier: "NewIdentifier", content: content, trigger: triger)
        center.add(request) { (error) in
            print(error as Any)
        }
    }
    
    func setWelcomeNotification() {
        let label = UILabel(frame: CGRect(x: 10, y: -5, width: 16, height: 16))
        label.layer.borderColor = UIColor.clear.cgColor
        label.layer.borderWidth = 2
        label.layer.cornerRadius = label.bounds.size.height / 2
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.font = UIFont.systemFont(ofSize: 10)
        label.textColor = .white
        label.backgroundColor = .black
        label.text = "1"
        rightButton?.addSubview(label)
    }
    
}
