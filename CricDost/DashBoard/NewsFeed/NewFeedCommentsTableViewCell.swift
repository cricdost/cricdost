//
//  NewFeedCommentsTableViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 10/17/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class NewFeedCommentsTableViewCell: UITableViewCell {

    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var commentsProfileImage: UIImageView!
    @IBOutlet weak var commentsProfileName: UILabel!
    @IBOutlet weak var commentsOptionButton: ShareButtonCustom!
    @IBOutlet weak var commentsaddressTimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        CommonUtil.imageRoundedCorners(imageviews: [commentsProfileImage])
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
