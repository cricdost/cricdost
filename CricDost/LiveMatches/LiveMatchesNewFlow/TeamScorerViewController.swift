//
//  MatchGoingNowViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 9/18/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import GlitchLabel
import LTMorphingLabel
import Lottie
import GoogleMobileAds

class TeamScorerViewController: UIViewController, LTMorphingLabelDelegate, ShowsAlert, GADBannerViewDelegate{
    
    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet weak var viewHolder2: UIView!
    @IBOutlet weak var matchTimerLabel: UILabel!
    @IBOutlet weak var matchStatus2: LTMorphingLabel!
    @IBOutlet weak var moreOptionButton: UIButton!
    @IBOutlet weak var moreOptiionButton2: UIButton!
    @IBOutlet weak var battingTeamImageView: UIImageView!
    @IBOutlet weak var battingTeamNameLabel: UILabel!
    @IBOutlet weak var onStrikePlayerImage: UIImageView!
    @IBOutlet weak var onStrikePlayerName: UILabel!
    @IBOutlet weak var onStrikePlayerScore: LTMorphingLabel!
    @IBOutlet weak var offStrikePlayerImage: UIImageView!
    @IBOutlet weak var offStrikePlayerScore: LTMorphingLabel!
    @IBOutlet weak var offStrikePlayerName: UILabel!
    @IBOutlet weak var bowlerPlayerImage: UIImageView!
    @IBOutlet weak var bowlerPlayerScore: LTMorphingLabel!
    @IBOutlet weak var bowlerPlayerName: UILabel!
    @IBOutlet weak var battingTeamScoreLabel: LTMorphingLabel!
    @IBOutlet weak var bowlingTeamImageView: UIImageView!
    @IBOutlet weak var bowlingTeamNameLabel: UILabel!
    @IBOutlet weak var bowlingTeamScoreLabel: LTMorphingLabel!
    @IBOutlet weak var matchStatus: LTMorphingLabel!
    @IBOutlet weak var cheersDostView: UIView!
    @IBOutlet weak var winnerScoreLabel: LTMorphingLabel!
    @IBOutlet weak var wonLabel: LTMorphingLabel!
    @IBOutlet weak var heyWooLabel: LTMorphingLabel!
    @IBOutlet weak var winnerTeamLabel: LTMorphingLabel!
    @IBOutlet weak var wellPlayerLabel: LTMorphingLabel!
    @IBOutlet weak var runnerTeamLabel: LTMorphingLabel!
    @IBOutlet weak var runnerLabel: LTMorphingLabel!
    @IBOutlet weak var runnerScoreLabel: LTMorphingLabel!
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var winnerView: UIView!
    @IBOutlet weak var runnerCupImage: UIImageView!
    @IBOutlet weak var clapThemButton: UIButton!
    @IBOutlet weak var winnerCupImage: UIImageView!
    @IBOutlet weak var clapThemButton2: UIButton!
    @IBOutlet weak var runnerView: UIView!
    @IBOutlet weak var celebrationImageView: UIImageView!
    var MATCH_ID: String?
    var tempLastBall = ""
    var liveScoreTimer = Timer()
    var logo = 0
    var tempExtra = ""
    var teama = ""
    var teamb = ""
    @IBOutlet weak var cheersDost: LTMorphingLabel!
    
    @IBOutlet weak var adMobView: GADBannerView!
    
    @IBOutlet weak var refreshButton2: UIButton!
    @IBOutlet weak var refreshButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [viewHolder, viewHolder2, winnerView, runnerView])
        CommonUtil.imageRoundedCorners(imageviews: [battingTeamImageView,bowlingTeamImageView, onStrikePlayerImage, offStrikePlayerImage, bowlerPlayerImage])
        CommonUtil.buttonRoundedCorners(buttons: [clapThemButton, clapThemButton2])
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissViewController))
        dismissView.addGestureRecognizer(tap)
        dismissView.isUserInteractionEnabled = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateLiveScoreNew), name: NSNotification.Name(rawValue: "updateLiveScoreNew"), object: nil)
        SocketConnectionsclass.joinMatchScoreBoard(match_id: MATCH_ID!)
        
        liveScoreTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.livescoreOnNav(theTimer:)),userInfo: 0,repeats: true)
        
        setLabelMorph(labels: [cheersDost], effect: .evaporate)
        setLabelMorph(labels: [matchStatus2,matchStatus, onStrikePlayerScore, offStrikePlayerScore,bowlerPlayerScore,battingTeamScoreLabel,bowlingTeamScoreLabel,wellPlayerLabel,runnerTeamLabel,runnerScoreLabel,runnerLabel,winnerScoreLabel,winnerTeamLabel,wonLabel,heyWooLabel], effect: .evaporate)

        if CommonUtil.AdMobIsOn == true {
            cheersDost.isEnabled = false
            cheersDostView.backgroundColor = UIColor.init(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.0)
            adMobView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
//             adMobView.adUnitID = "ca-app-pub-2009641953881873/1885241901"
            adMobView.rootViewController = self
            adMobView.load(GADRequest())
        } else {
            cheersDostView.backgroundColor = UIColor.init(red: 0.98, green: 0.66, blue: 0.10, alpha: 1.0)
            cheersDost.isEnabled = true
            cheersDost.text = "CHEERS DOST!"
        }
       
        refreshButton.setImage(#imageLiteral(resourceName: "baseline_refresh_white_24pt_1x.png").withRenderingMode(.alwaysTemplate), for: .normal)
        refreshButton.tintColor = CommonUtil.themeRed
        
        refreshButton2.setImage(#imageLiteral(resourceName: "baseline_refresh_white_24pt_1x.png").withRenderingMode(.alwaysTemplate), for: .normal)
        refreshButton2.tintColor = CommonUtil.themeRed
        
    }
    
    func setLabelMorph(labels: [LTMorphingLabel], effect: LTMorphingEffect) {
        for label in labels {
            label.delegate = self
            label.morphingEffect = effect
            label.start()
        }
    }
    
    @objc func livescoreOnNav(theTimer: Timer) {
        if logo == 0 {
            UIView.transition(with: self.viewHolder, duration: 1.0, options: [.transitionFlipFromLeft], animations:{
                self.viewHolder.isHidden = true
            }, completion: nil)
            UIView.transition(with: self.viewHolder2, duration: 1.0, options: [.transitionFlipFromLeft], animations:{
                self.viewHolder2.isHidden = false
            }, completion: nil)
            logo = 1
            cheersDost.text = "........."
        } else {
            UIView.transition(with: self.viewHolder2, duration: 1.0, options: [.transitionFlipFromRight], animations:{
                self.viewHolder2.isHidden = true
            }, completion: nil)
            UIView.transition(with: viewHolder, duration: 1.0, options: [.transitionFlipFromRight], animations:{
                self.viewHolder.isHidden = false
            }, completion: nil)
            logo = 0
            cheersDost.text = "CHEERS DOST!"
        }
        viewHolder2.layoutIfNeeded()
        viewHolder2.layoutSubviews()
        viewHolder.layoutIfNeeded()
        viewHolder.layoutSubviews()
    }
    
    @IBAction func refreshButton2Click(_ sender: Any) {
        print("flip flip flip====2222")
        SocketConnectionsclass.matchGetStatus(match_id: MATCH_ID!)
    }
    
    @IBAction func refreshButton1Click(_ sender: Any) {
        print("flip flip flip====1111")
        SocketConnectionsclass.matchGetStatus(match_id: MATCH_ID!)
    }
    
    @IBAction func optionButton1Click(_ sender: Any) {
        optionMenu()
    }
    
    @IBAction func optionButton2Click(_ sender: Any) {
        optionMenu()
    }
    
    @IBAction func optionButton3Click(_ sender: Any) {
        optionMenu()
    }
    
    @IBAction func optionButton4Click(_ sender: Any) {
        optionMenu()
    }
    
    @objc func dismissViewController(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        CommonUtil.updateGATracker(screenName: "Live Scores")
        adMobView.roundCornersView([.bottomLeft, .bottomRight], radius: 5)
        cheersDostView.roundCornersView([.bottomLeft , .bottomRight], radius: 5)
         cheersDost.text = "CHEERS DOST!"
//        if CommonUtil.AdMobIsOn == true {
////            cheersDost.isEnabled = false
////            adMobView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
////            adMobView.adUnitID = "ca-app-pub-3940256099942544/2934735744"
////            adMobView.rootViewController = self
////            adMobView.load(GADRequest())
//        } else {
//            cheersDost.isEnabled = true
//            cheersDost.text = "CHEERS DOST!"
//        }
       
//        showFourSix()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "newFlowLiveNonScorer_Fullscore" {
            UserDefaults.standard.set(MATCH_ID, forKey: CommonUtil.CURRENTSCOREMATCHID)
        }
    }
    
    func optionMenu() {
        let alert = UIAlertController(title: "Options", message: "View full score card of match", preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .popover
        alert.addAction(UIAlertAction(title: "Full Score Card", style: .default, handler: { _ in
            print("FULL SCORE")
            self.performSegue(withIdentifier: "newFlowLiveNonScorer_Fullscore", sender: self)
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            print("IPAD")
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            alert.popoverPresentationController?.permittedArrowDirections = .down
            
        default:
            break
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func showFourSixNew(image: String) {
        if let animationView = LOTAnimationView(name: image) {
            animationView.frame = CGRect(x: 0, y: 0, width: view.frame.width/2, height: view.frame.height/2)
            animationView.center = self.view.center
            animationView.contentMode = .scaleAspectFit
            view.addSubview(animationView)
            animationView.bringSubview(toFront: viewHolder2)
            animationView.play()
            DispatchQueue.main.asyncAfter(deadline: .now() + 6.0, execute: {
                animationView.removeFromSuperview()
            })
        }
    }
    
    
    func showFourSix(image: UIImage) {
        celebrationImageView.isHidden = false
        celebrationImageView.image = image
        CommonUtil.animateImage(image: celebrationImageView)
        viewHolder.shake()
        viewHolder2.shake()
        if let animationView = LOTAnimationView(name: "confetti") {
            animationView.frame = CGRect(x: 0, y: 0, width: view.frame.width/2, height: view.frame.height/2)
            animationView.center = self.view.center
            animationView.contentMode = .scaleAspectFit
            view.addSubview(animationView)
            animationView.play()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                UIView.transition(with: self.celebrationImageView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                    self.celebrationImageView.isHidden = true

                }, completion: nil)
                animationView.removeFromSuperview()
            })
        }
    }
    
    
    @IBAction func winnerClapButtonClick(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MATCHID_CLAP_\(MATCH_ID!)") as? Bool ?? true {
            var x = ""
            if teama.components(separatedBy: ",").contains(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String) {
                x = teamb
            } else {
                x = teama
            }
            let clap:[String: Any] = ["MatchID": MATCH_ID!,
                                      "TeamName": winnerTeamLabel.text!,
                                      "UserId": UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String,
                                      "OtherPlayerIDS": x]
            print(clap)
            
            if let animationView = LOTAnimationView(name: "confetti") {
                animationView.frame = CGRect(x: 0, y: 0, width: view.frame.width/2, height: view.frame.height/2)
                animationView.center = self.view.center
                animationView.contentMode = .scaleAspectFit
                view.addSubview(animationView)
                animationView.play()
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                    animationView.removeFromSuperview()
                    self.dismiss(animated: true, completion: nil)
                })
            }
            SocketConnectionsclass.clapThemEmit(param: clap)
            UserDefaults.standard.set(false, forKey: "MATCHID_CLAP_\(MATCH_ID!)")
        } else {
            self.showAlert(title: "Clap Them", message: "Your appreciation was already notified dost")
        }
        
        
    }
    
    @IBAction func runnerClapButtonClick(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "MATCHID_CLAP_\(MATCH_ID!)") as? Bool ?? true {
            var x = ""
            if teama.components(separatedBy: ",").contains(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String) {
                x = teamb
            } else {
                x = teama
            }
            let clap:[String: Any] = ["MatchID": MATCH_ID!,
                                      "TeamName": runnerTeamLabel.text!,
                                      "UserId": UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String,
                                      "OtherPlayerIDS": x]
            print(clap)
            if let animationView = LOTAnimationView(name: "confetti") {
                animationView.frame = CGRect(x: 0, y: 0, width: view.frame.width/2, height: view.frame.height/2)
                animationView.center = self.view.center
                animationView.contentMode = .scaleAspectFit
                view.addSubview(animationView)
                animationView.play()
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                    animationView.removeFromSuperview()
                    self.dismiss(animated: true, completion: nil)
                })
            }
            SocketConnectionsclass.clapThemEmit(param: clap)
            UserDefaults.standard.set(false, forKey: "MATCHID_CLAP_\(MATCH_ID!)")
        } else {
            self.showAlert(title: "Clap Them", message: "Your appreciation was already notified dost")
        }
    }
    
    @objc func updateLiveScoreNew(_ notification: NSNotification) {
        
        let data = (notification.userInfo![AnyHashable("data")] as! NSArray)[0] as! NSDictionary
        matchStatus.text = "......."
        matchStatus2.text = "......."
        matchStatus2.text = data["RESULT"] as? String
        matchStatus.text = data["RESULT"] as? String
        var teamA: NSDictionary = [:]
        var teamB: NSDictionary = [:]
        
        teamA = data["TEAM_A"] as! NSDictionary
        teamB = data["TEAM_B"] as! NSDictionary
        
        UserDefaults.standard.set(teamA["TEAM_A_NAME"] as? String, forKey: CommonUtil.CURRENT_MATCH_TEAM_A_NAME)
        UserDefaults.standard.set(teamB["TEAM_B_NAME"] as? String, forKey: CommonUtil.CURRENT_MATCH_TEAM_B_NAME)
        
        battingTeamImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamA["IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "no_team_image_icon"))
        battingTeamNameLabel.text = teamA["TEAM_A_NAME"] as? String
        battingTeamScoreLabel.text = "...."
        if teamA["RUN_STR"] as? String == "0/0 (0)" {
            battingTeamScoreLabel.text = "0/0 (0)"
        } else {
            battingTeamScoreLabel.text = teamA["RUN_STR"] as? String
        }
        
        bowlingTeamImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamB["IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "no_team_image_icon"))
        bowlingTeamNameLabel.text = teamB["TEAM_B_NAME"] as? String
        bowlingTeamScoreLabel.text = "...."
        if teamB["RUN_STR"] as? String == "0/0 (0)" {
            bowlingTeamScoreLabel.text = "Yet to bat"
        } else {
            bowlingTeamScoreLabel.text = teamB["RUN_STR"] as? String
        }
        
        let battingOrder = data["BATTING"] as! NSArray
        let bowlingOrder = data["BOWLING"] as! NSArray
        
        onStrikePlayerImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\("")"), placeholderImage: #imageLiteral(resourceName: "profile_pic_default.png"))
        onStrikePlayerName.text = "Yet to choose"
        onStrikePlayerScore.text = ".."
        onStrikePlayerScore.text = "-"
        offStrikePlayerImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\("")"), placeholderImage: #imageLiteral(resourceName: "profile_pic_default.png"))
        offStrikePlayerName.text = "Yet to choose"
        offStrikePlayerScore.text = ".."
        offStrikePlayerScore.text = "-"
        bowlerPlayerImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\("")"), placeholderImage: #imageLiteral(resourceName: "profile_pic_default.png"))
        bowlerPlayerName.text = "Yet to choose"
        bowlerPlayerScore.text = ".."
        bowlerPlayerScore.text = "-"
        
        if battingOrder.count > 0 {
            for batsman in battingOrder {
                let bat = batsman as! NSDictionary
                if bat["STRIKER"] as? String ?? "\(bat["STRIKER"] as! Int)" == "1" {
                    onStrikePlayerImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(bat["IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "no_team_image_icon"))
                    onStrikePlayerName.text = bat["FULL_NAME"] as? String
                    onStrikePlayerScore.text = "...."
                    onStrikePlayerScore.text = "\(bat["RUNS"] as? String ?? "\(bat["RUNS"] as! Int)")/\(bat["BALLS"] as? String ?? "\(bat["BALLS"] as! Int)")"
                } else {
                    offStrikePlayerImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(bat["IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "no_team_image_icon"))
                    offStrikePlayerName.text = bat["FULL_NAME"] as? String
                    offStrikePlayerScore.text = "...."
                    offStrikePlayerScore.text = "\(bat["RUNS"] as? String ?? "\(bat["RUNS"] as! Int)")/\(bat["BALLS"] as? String ?? "\(bat["BALLS"] as! Int)")"
                }
            }
        }
        
        if bowlingOrder.count > 0 {
            for bowler in bowlingOrder {
                let bowl = bowler as! NSDictionary
                bowlerPlayerImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(bowl["IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "no_team_image_icon"))
                bowlerPlayerName.text = bowl["FULL_NAME"] as? String
                bowlerPlayerScore.text = "...."
                bowlerPlayerScore.text = "\(bowl["RUNS"] as? String ?? "\(bowl["RUNS"] as! Int)")/\(bowl["WICKETS"] as? String ?? "\(bowl["WICKETS"] as! Int)") (\(bowl["BALLS"] as? String ?? "\(bowl["BALLS"] as! Int)"))"
            }
        } else {
            bowlerPlayerImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\("")"), placeholderImage: #imageLiteral(resourceName: "no_team_image_icon"))
            bowlerPlayerName.text = "Yet to choose"
            bowlerPlayerScore.text = ".."
            bowlerPlayerScore.text = "-"
        }
        
        if data["STATUS_STR"] as? String == "Completed" {
            print("completed")
            viewHolder.isHidden = true
            viewHolder2.isHidden = true
            let teamAPlayers = (teamA["TEAM_PLAYERS"] as? String)?.components(separatedBy: ",")
            let teamBPlayers = (teamB["TEAM_PLAYERS"] as? String)?.components(separatedBy: ",")
            
            print(teamAPlayers, teamBPlayers)
            teama = teamA["TEAM_PLAYERS"] as! String
            teamb = teamB["TEAM_PLAYERS"] as! String
            if teamA["TEAM_ADMIN_USER_ID"] as? String ?? "\(teamA["TEAM_ADMIN_USER_ID"] as! Int)" == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String || (teamAPlayers?.contains(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String))! {
                print("TEAM A is my team")
                if teamA["RUNS"] as? Int ?? Int(teamA["RUNS"] as! String)! > teamB["RUNS"] as? Int ?? Int(teamB["RUNS"] as! String)! {
                    winnerView.isHidden = false
                    runnerView.isHidden = true
                    winnerTeamLabel.text = teamA["TEAM_A_NAME"] as? String
                    wonLabel.text = "won the match"
                    heyWooLabel.text = "HeY! WoO!"
                    winnerScoreLabel.text = teamA["RUN_STR"] as? String
                } else {
                    winnerView.isHidden = true
                    runnerView.isHidden = false
                    runnerTeamLabel.text = teamA["TEAM_A_NAME"] as? String
                    runnerLabel.text = "runner-uo of the match"
                    wellPlayerLabel.text = "Well Played!"
                    runnerScoreLabel.text = teamA["RUN_STR"] as? String
                }
                
            } else if teamB["TEAM_ADMIN_USER_ID"] as? String ?? "\(teamB["TEAM_ADMIN_USER_ID"] as! Int)" == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String || (teamBPlayers?.contains(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String))! {
                print("TEAM B is my team")
                if teamB["RUNS"] as? Int ?? Int(teamB["RUNS"] as! String)! > teamA["RUNS"] as? Int ?? Int(teamA["RUNS"] as! String)! {
                    winnerView.isHidden = false
                    runnerView.isHidden = true
                    winnerTeamLabel.text = teamB["TEAM_B_NAME"] as? String
                    wonLabel.text = "won the match"
                    heyWooLabel.text = "HeY! WoO!"
                    winnerScoreLabel.text = teamB["RUN_STR"] as? String
                } else {
                    winnerView.isHidden = true
                    runnerView.isHidden = false
                    runnerTeamLabel.text = teamB["TEAM_B_NAME"] as? String
                    runnerLabel.text = "runner-up of the match"
                    wellPlayerLabel.text = "Well Played!"
                    runnerScoreLabel.text = teamB["RUN_STR"] as? String
                }
            } else {
                matchStatus2.text = data["STATUS_STR"] as? String
                matchStatus.text = data["STATUS_STR"] as? String
                viewHolder.isHidden = false
                viewHolder2.isHidden = true
                liveScoreTimer.invalidate()
            }
            
            CommonUtil.animateImage(image: self.winnerCupImage)
            CommonUtil.animateImage(image: self.runnerCupImage)
            
            UIView.animate(withDuration: 0.5,
                           delay: 0.1,
                           options: [ .curveEaseInOut, .repeat],
                           animations: {
                            UIView.setAnimationRepeatCount(5)
                            self.winnerCupImage.alpha=1.0
                            self.winnerCupImage.alpha=0.3
                            self.winnerCupImage.alpha=1.0
                            self.winnerCupImage.alpha=0.3
                            self.winnerCupImage.alpha=1.0
            }, completion: { finished in
                self.winnerCupImage.isHidden = false
            })
            
            UIView.animate(withDuration: 0.5,
                           delay: 0.1,
                           options: [ .curveEaseInOut, .repeat],
                           animations: {
                            UIView.setAnimationRepeatCount(5)
                            self.runnerCupImage.alpha = 1.0
                            self.runnerCupImage.alpha = 0.3
                            self.runnerCupImage.alpha = 1.0
                            self.runnerCupImage.alpha = 0.3
                            self.runnerCupImage.alpha = 1.0
            }, completion: { finished in
                self.runnerCupImage.isHidden = false
            })
        } else {
            if data["SCORER"] as? String ?? "\(data["SCORER"] as! Int)" == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as? String {
                self.dismiss(animated: true) {
                    let playerList:[String: Any] = ["match_id":self.MATCH_ID!]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openScorerViewController"), object: nil, userInfo: playerList)
                }
            }
        }
        //        cheersDostLabel.blendMode = .lighten
        
        if (data["CURRENT_OVER"] as! NSArray).count > 0 {
            let currentOver = (data["CURRENT_OVER"] as! NSArray)[0] as! NSDictionary
            let lastballRuns = currentOver["RUNS"] as? String ?? "\(currentOver["RUNS"] as! Int)"
            let lastball = currentOver["OVER"] as? String ?? "\(currentOver["OVER"] as! Double)"
            let extra = currentOver["EXTRAS_TYPE"] as? String ?? "\(currentOver["EXTRAS_TYPE"] as! Int)"
            
            if tempLastBall != lastball {
                if lastballRuns == "4" {
                    showFourSix(image: #imageLiteral(resourceName: "four"))
//                    showFourSixNew(image: "four")
                } else if lastballRuns == "6" {
                    showFourSix(image: #imageLiteral(resourceName: "six"))
//                    showFourSixNew(image: "six")
                } else if currentOver["WICKET"] as? String ?? "\(currentOver["WICKET"] as! Int)" == "1" {
                    showFourSix(image: #imageLiteral(resourceName: "out-1"))
                }
            } else if tempExtra == "nb" || tempExtra == "wd"  {
                if lastballRuns == "4" {
                    showFourSix(image: #imageLiteral(resourceName: "four"))
//                    showFourSixNew(image: "four")
                } else if lastballRuns == "6" {
                    showFourSix(image: #imageLiteral(resourceName: "six"))
//                    showFourSixNew(image: "six")
                } else if currentOver["WICKET"] as? String ?? "\(currentOver["WICKET"] as! Int)" == "1" {
                    showFourSix(image: #imageLiteral(resourceName: "out-1"))
                }
            }
            tempExtra = extra
            tempLastBall = lastball
        }
    }
}







