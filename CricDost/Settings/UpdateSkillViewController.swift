//
//  UpdateSkillViewController.swift
//  CricDost
//
//  Created by Jit Goel on 7/3/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class UpdateSkillViewController: UIViewController, ShowsAlert {

    @IBOutlet weak var batsmanButton: UIButton!
    @IBOutlet weak var bowlerButton: UIButton!
    @IBOutlet weak var keeperButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var isBatsman: String?
    var isBowler: String?
    var isKeeper: String?
    var batsman = false
    var bowler = false
    var keeper = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonBorder(buttons: [batsmanButton, bowlerButton, keeperButton])
        if isBatsman == "1" {
            batsman = true
        } else {
            batsman = false
        }
        
        if isBowler == "1" {
            bowler = true
        } else {
            bowler = false
        }
        
        if isKeeper == "1" {
            keeper = true
        } else {
            keeper = false
        }
        setSelected()
    }
    
    @IBAction func batsmanButtonClick(_ sender: Any) {
        batsman = !batsman
        if batsman {
            isBatsman = "1"
        } else {
            isBatsman = "0"
        }
        setSelected()
    }
    
    @IBAction func bowlerButtonClicked(_ sender: Any) {
        bowler = !bowler
        if bowler {
            isBowler = "1"
        } else {
            isBowler = "0"
        }
        setSelected()
    }
    
    @IBAction func keeperButtonClick(_ sender: Any) {
        keeper = !keeper
        if keeper {
            isKeeper = "1"
        } else {
            isKeeper = "0"
        }
        setSelected()
    }
    
    @IBAction func cancelButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updateButtonClick(_ sender: Any) {
        if isBatsman == "1" || isBowler == "1" || isKeeper == "1" {
            updateSkill()
        } else {
            self.showAlert(title: "Alert", message: "Please select atleast one skill")
        }
    }
    
    func setSelected(){
        switch isBatsman! {
        case "1": batsmanButton.setImage(#imageLiteral(resourceName: "baseline_done_white_24pt_1x"), for: .normal)
                batsmanButton.backgroundColor = CommonUtil.themeRed
            break
        case "0": batsmanButton.setImage(nil, for: .normal)
                batsmanButton.backgroundColor = UIColor.white
            break
        default:
            break
        }
        
        switch isBowler! {
        case "1": bowlerButton.setImage(#imageLiteral(resourceName: "baseline_done_white_24pt_1x"), for: .normal)
                bowlerButton.backgroundColor = CommonUtil.themeRed
            break
        case "0": bowlerButton.setImage(nil, for: .normal)
                bowlerButton.backgroundColor = UIColor.white
            break
        default:
            break
        }
        
        switch isKeeper! {
        case "1": keeperButton.setImage(#imageLiteral(resourceName: "baseline_done_white_24pt_1x"), for: .normal)
                keeperButton.backgroundColor = CommonUtil.themeRed
            break
        case "0": keeperButton.setImage(nil, for: .normal)
                keeperButton.backgroundColor = UIColor.white
            break
        default:
            break
        }
    }
    
    func buttonBorder(buttons: [UIButton]) {
        for button in buttons {
            button.layer.borderWidth = 1.0
            button.layer.borderColor = CommonUtil.themeRed.cgColor
            button.layer.cornerRadius = 1.0
        }
    }
    
    func getSkillSet() -> String {
        var dataSkill = ""
        if isBowler == "1" {
            dataSkill = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_INFO\":{\"IS_BOWLER\":\(true)}}"
        }
        if isBatsman == "1" {
            dataSkill = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_INFO\":{\"IS_BATSMAN\":\(true)}}"
        }
        if isKeeper == "1" {
            dataSkill = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_INFO\":{\"IS_KEEPER\":\(true)}}"
        }
        
        if isBatsman == "1" && isBowler == "1" {
            dataSkill = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_INFO\":{\"IS_BATSMAN\":\(true),\"IS_BOWLER\":\(true)}}"
        }
        
        if isBatsman == "1" && isKeeper == "1" {
            dataSkill = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_INFO\":{\"IS_BATSMAN\":\(true),\"IS_KEEPER\":\(true)}}"
        }
        
        if isBowler == "1" && isKeeper == "1" {
            dataSkill = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_INFO\":{\"IS_BOWLER\":\(true),\"IS_KEEPER\":\(true)}}"
        }
        
        if isBatsman == "1" && isBowler == "1" && isKeeper == "1" {
            dataSkill = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_INFO\":{\"IS_BATSMAN\":\(true),\"IS_BOWLER\":\(true),\"IS_KEEPER\":\(true)}}"
        }
        
        return dataSkill
    }
    
    func updateSkill() {
        activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: getSkillSet(), mod: "Player", actionType: "update-player-details") { (response) in
            if response as? String != "error" {
                print(response)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getPlayerProfileEdit"), object: nil)
                let alert = UIAlertController(title: "Message", message: (response as! NSDictionary)["XSCMessage"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
        }
    }
}
