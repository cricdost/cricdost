//
//  PrivacyViewController.swift
//  CricDost
//
//  Created by Jit Goel on 7/2/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class PrivacyViewController: UIViewController, ShowsAlert {

    @IBOutlet weak var profileVisibilitySwitch: UISwitch!
    @IBOutlet weak var pictureVisibitySwitch: UISwitch!
    @IBOutlet weak var allowCallSwitch: UISwitch!
    @IBOutlet weak var allowChatSwitch: UISwitch!
    var IS_ALLOW_USER_TO_CALL: Int?
    var IS_ALLOW_USER_TO_CHAT: Int?
    var IS_VISIBLE: Int?
    var IS_VISIBLE_PROFILE_PICTURE: Int?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var preventUserInteraction: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Privacy"
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        profileVisibilitySwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        pictureVisibitySwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        allowCallSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        allowChatSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        
        setOnOFF(switchBut: profileVisibilitySwitch, state: IS_VISIBLE!)
        setOnOFF(switchBut: pictureVisibitySwitch, state: IS_VISIBLE_PROFILE_PICTURE!)
        setOnOFF(switchBut: allowCallSwitch, state: IS_ALLOW_USER_TO_CALL!)
        setOnOFF(switchBut: allowChatSwitch, state: IS_ALLOW_USER_TO_CHAT!)
    }
    
    func setOnOFF(switchBut: UISwitch, state: Int) {
        if state == 1 {
            switchBut.isOn = true
        } else {
            switchBut.isOn = false
        }
        self.view.layoutSubviews()
        self.view.layoutIfNeeded()
    }
    
    @IBAction func profileVisiblityToggleSwitch(_ sender: Any) {
        
        if profileVisibilitySwitch.isOn {
            let alert = UIAlertController(title: "Profile Visibility", message: "Are you sure you want to show your profile?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                self.updatePrivacySettings(actionType: "set-user-visible", flag: "1")
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
                self.setOnOFF(switchBut: self.profileVisibilitySwitch, state: self.IS_VISIBLE!)
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Profile Visibility", message: "Are you sure you want to hide your profile?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                self.updatePrivacySettings(actionType: "set-user-visible", flag: "0")
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
                self.setOnOFF(switchBut: self.profileVisibilitySwitch, state: self.IS_VISIBLE!)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func pictureVisibilityToggleSwitch(_ sender: Any) {
        if pictureVisibitySwitch.isOn {
            let alert = UIAlertController(title: "Profile Picture Visibility", message: "Are you sure you want to show your profile picture?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                self.updatePrivacySettings(actionType: "set-profile-picture-visible", flag: "1")
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
                self.setOnOFF(switchBut: self.pictureVisibitySwitch, state: self.IS_VISIBLE_PROFILE_PICTURE!)
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Profile Picture Visibility", message: "Are you sure you want to hide your profile picture?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                self.updatePrivacySettings(actionType: "set-profile-picture-visible", flag: "0")
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
                self.setOnOFF(switchBut: self.pictureVisibitySwitch, state: self.IS_VISIBLE_PROFILE_PICTURE!)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func allowCallToggleSwitch(_ sender: Any) {
        if allowCallSwitch.isOn {
            let alert = UIAlertController(title: "Call Privacy", message: "Are you sure you want to allow calls from other?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                self.updatePrivacySettings(actionType: "set-user-allow-to-call", flag: "1")
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
                self.setOnOFF(switchBut: self.allowCallSwitch, state: self.IS_ALLOW_USER_TO_CALL!)
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Call Privacy", message: "Are you sure you want to block calls from other?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                self.updatePrivacySettings(actionType: "set-user-allow-to-call", flag: "0")
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
                self.setOnOFF(switchBut: self.allowCallSwitch, state: self.IS_ALLOW_USER_TO_CALL!)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func allowChatToggleSwitch(_ sender: Any) {
        if allowChatSwitch.isOn {
            let alert = UIAlertController(title: "Chat Privacy", message: "Are you sure you want to allow calls from other?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                self.updatePrivacySettings(actionType: "set-user-allow-to-chat", flag: "1")
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
                self.setOnOFF(switchBut: self.allowChatSwitch, state: self.IS_ALLOW_USER_TO_CHAT!)
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Chat Privacy", message: "Are you sure you want to block chats from other?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                self.updatePrivacySettings(actionType: "set-user-allow-to-chat", flag: "0")
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
                self.setOnOFF(switchBut: self.allowChatSwitch, state: self.IS_ALLOW_USER_TO_CHAT!)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func updatePrivacySettings(actionType: String, flag: String) {
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"FLAG\":\"\(flag)\"}", mod: "Settings", actionType: actionType) { (response) in
            if response as? String != "error" {
                print(response)
                self.showAlert(title: "Message", message: (response as! NSDictionary)["XSCMessage"] as! String)
                self.getUserSettingsDetail()
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
        }
    }
    
    func getUserSettingsDetail() {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Settings", actionType: "get-settings") { (response) in
            if response as? String != "error" {
                let settings = (response as! NSDictionary)["XSCData"] as! NSDictionary
                self.IS_ALLOW_USER_TO_CALL = settings["IS_ALLOW_USER_TO_CALL"] as? Int
                self.IS_ALLOW_USER_TO_CHAT = settings["IS_ALLOW_USER_TO_CHAT"] as? Int
                self.IS_VISIBLE = settings["IS_VISIBLE"] as? Int
                self.IS_VISIBLE_PROFILE_PICTURE = settings["IS_VISIBLE_PROFILE_PICTURE"] as? Int
                
                self.setOnOFF(switchBut: self.profileVisibilitySwitch, state: self.IS_VISIBLE!)
                self.setOnOFF(switchBut: self.pictureVisibitySwitch, state: self.IS_VISIBLE_PROFILE_PICTURE!)
                self.setOnOFF(switchBut: self.allowCallSwitch, state: self.IS_ALLOW_USER_TO_CALL!)
                self.setOnOFF(switchBut: self.allowChatSwitch, state: self.IS_ALLOW_USER_TO_CHAT!)
            
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        }
    }
}















