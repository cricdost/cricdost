//
//  GenderViewController.swift
//  CricDost
//
//  Created by Jit Goel on 5/25/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import LTMorphingLabel

class GenderViewController: UIViewController, ShowsAlert, LTMorphingLabelDelegate {

    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var maleImageView: UIImageView!
    @IBOutlet weak var femaleImageView: UIImageView!
    @IBOutlet weak var transImageView: UIImageView!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    @IBOutlet weak var transButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadingViewPreventUserInteraction: UIView!
    var GENDER: String?
    @IBOutlet weak var heyDostLabel: LTMorphingLabel!
    @IBOutlet weak var areYouLabel: LTMorphingLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonUtil.buttonRoundedCorners(buttons: [doneButton,maleButton,femaleButton,transButton])
        
        let male = UITapGestureRecognizer(target: self, action: #selector(maleFunction))
        maleImageView.addGestureRecognizer(male)
        
        let female = UITapGestureRecognizer(target: self, action: #selector(femaleFunction))
        femaleImageView.addGestureRecognizer(female)
        
        let trans = UITapGestureRecognizer(target: self, action: #selector(transFunction))
        transImageView.addGestureRecognizer(trans)
        
        setLabelMorph(labels: [heyDostLabel], effect: .evaporate)
        setLabelMorph(labels: [areYouLabel], effect: .evaporate)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        heyDostLabel.text = "Hey Dost!"
        areYouLabel.text = "Are You?"
    }
    
    func setLabelMorph(labels: [LTMorphingLabel], effect: LTMorphingEffect) {
        for label in labels {
            label.delegate = self
            label.morphingEffect = effect
            label.start()
        }
    }
    
    @objc func maleFunction(sender:UITapGestureRecognizer) {
        setSelectedButton(maleIsHidden: false, femaleIsHidden: true, transIsHidden: true)
        GENDER = "Male"
    }
    
    @objc func femaleFunction(sender:UITapGestureRecognizer) {
        setSelectedButton(maleIsHidden: true, femaleIsHidden: false, transIsHidden: true)
        GENDER = "Female"
    }
    
    @objc func transFunction(sender:UITapGestureRecognizer) {
        setSelectedButton(maleIsHidden: true, femaleIsHidden: true, transIsHidden: false)
        GENDER = "Other"
    }
    
    func setSelectedButton(maleIsHidden:Bool,femaleIsHidden:Bool,transIsHidden:Bool) {
        maleButton.isHidden = maleIsHidden
        femaleButton.isHidden = femaleIsHidden
        transButton.isHidden = transIsHidden
        UIView.transition(with: self.doneButton, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.doneButton.isHidden = false
        }, completion: nil)
    }
    
    @IBAction func doneButtonClick(_ sender: Any) {
        self.activityIndicator.startAnimating()
        self.loadingViewPreventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"GENDER\":\"\(GENDER!)\"}", mod: "Users", actionType: "update-user-gender", callback: {(response : Any) -> Void in
            print(response)
            if response as? String != "error" {
                if((response as! NSDictionary)["XSCStatus"] as! Int == 0) {
                    self.performSegue(withIdentifier: "gender_dob", sender: self)
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.loadingViewPreventUserInteraction.isHidden = true
        })
    }
}











