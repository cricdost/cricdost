//
//  LiveScoreNavigationCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 8/14/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import MarqueeLabel

class LiveScoreNavigationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var teamAImageView: UIImageView!
    @IBOutlet weak var teamBImageView: UIImageView!
    @IBOutlet weak var teamAName: UILabel!
    @IBOutlet weak var teamBName: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var resultLabel: MarqueeLabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 3.0
        layer.shadowRadius = 3
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 0, height: 0)
        self.clipsToBounds = false
    }
    
}
