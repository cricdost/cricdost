//
//  NameImageViewController.swift
//  CricDost
//
//  Created by Jit Goel on 5/24/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import AVFoundation

class NameImageViewController: UIViewController, UITextFieldDelegate, AVCapturePhotoCaptureDelegate, ShowsAlert {
    
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var nameFrameView: UIView!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var selfieButton: UIButton!
    @IBOutlet weak var cameraView: UIView!
    var captureSession = AVCaptureSession()
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentCamera: AVCaptureDevice?
    var photoOutput: AVCapturePhotoOutput?
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    @IBOutlet weak var cameraFrameView: UIView!
    @IBOutlet weak var switchCameraButton: UIButton!
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var loadingViewPreventUserInteraction: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var image: UIImage?
    var imageData1: Data? = nil
    var goto: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //button rounded edge
        CommonUtil.buttonRoundedCorners(buttons: [nextButton, skipButton, selfieButton])
        
        // Keyboard Config
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)
        
        if goto == "IMAGE" {
            nameTextfield.text = UserDefaults.standard.object(forKey: CommonUtil.FULL_NAME) as? String
            UIView.transition(with: self.nameFrameView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.nameFrameView.isHidden = true
            })
            UIView.transition(with: self.cameraFrameView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.cameraFrameView.isHidden = false
                self.switchCameraButton.isHidden = false
            })
            if ProcessInfo.processInfo.environment["SIMULATOR_DEVICE_NAME"] != nil {
                print("Simulator")
            } else {
                print("Not Simulator")
                if self.captureSession.isRunning {
                    self.captureSession.beginConfiguration()
                    
                    let currentInput : AVCaptureInput = self.captureSession.inputs[0]
                    self.captureSession.removeInput(currentInput)
                    self.captureSession.removeOutput(self.photoOutput!)
                    
                    self.setupInputOut()
                    
                    self.captureSession.commitConfiguration()
                } else {
                    self.setupCaptureSession()
                    self.setupDevice()
                    self.setupInputOut()
                    //                    self.setupPreviewLayer()
                    //                    self.startRunningCaptureSession()
                }
            }
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if goto == "IMAGE" {
            self.setupPreviewLayer()
            self.startRunningCaptureSession()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        CommonUtil.updateGATracker(screenName: "New User")
    }
    
    @IBAction func skipButtonClick(_ sender: Any) {
        activityIndicator.startAnimating()
        loadingViewPreventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Users", actionType: "update-user-profile", callback: {(response: Any) -> Void in
            if response as? String != "error" {
                if ((response as! NSDictionary)["XSCStatus"] as! Int) == 0 {
                    let profile = (response as! NSDictionary)["XSCData"] as! NSDictionary
                    print(profile)
                    
                    switch(profile["STEP_NAME"] as! String) {
                        
                    case "GENDER":  self.performSegue(withIdentifier: "name_gender", sender: self)
                        break
                    case "DOB": self.performSegue(withIdentifier: "name_dob", sender: self)
                        break
                    case "PLAYER_STATUS": self.performSegue(withIdentifier: "name_skill", sender: self)
                        break
                    case "LOCATION": self.performSegue(withIdentifier: "name_location", sender: self)
                        break
                    case "DASHBOARD":
                        if CommonUtil.LandingPageCricSpace {
                            self.performSegue(withIdentifier: "name_cricspace", sender: self)
                        } else {
                            self.performSegue(withIdentifier: "name_dashboard", sender: self)
                        }
                        break
                    default:
                        print("DEFAULT CASE")
                    }
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCStatus"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.loadingViewPreventUserInteraction.isHidden = true
        })
        
    }
    
    @IBAction func nextButtonClick(_ sender: Any) {
        nameTextfield.endEditing(true)
        if (nameTextfield.text?.isEmpty)! || nameTextfield.isReallyEmpty {
            // textfield is empty
            self.nameTextfield.shakeTextField()
            showAlert(message: "Please enter a name")
        } else {
            // text field is not empty
            activityIndicator.startAnimating()
            loadingViewPreventUserInteraction.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"FULL_NAME\": \"\(nameTextfield.text!)\"}", mod: "Users", actionType: "update-user-name", callback: {(response : Any) -> Void in
                
                print(response)
                if response as? String != "error" {
                    let nextStep = (response as! NSDictionary)["XSCData"] as! NSDictionary
                    
                    UserDefaults.standard.set(nextStep["FULL_NAME"] as? String ?? "", forKey: CommonUtil.FULL_NAME)
                    
                    switch(nextStep["STEP_NAME"] as! String) {
                        
                    case "IMAGE": UIView.transition(with: self.nameFrameView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.nameFrameView.isHidden = true
                    })
                    UIView.transition(with: self.cameraFrameView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.cameraFrameView.isHidden = false
                        self.switchCameraButton.isHidden = false
                    })
                    if ProcessInfo.processInfo.environment["SIMULATOR_DEVICE_NAME"] != nil {
                        print("Simulator")
                    } else {
                        print("Not Simulator")
                        if self.captureSession.isRunning {
                            self.captureSession.beginConfiguration()
                            
                            let currentInput : AVCaptureInput = self.captureSession.inputs[0]
                            self.captureSession.removeInput(currentInput)
                            self.captureSession.removeOutput(self.photoOutput!)
                            
                            self.setupInputOut()
                            
                            self.captureSession.commitConfiguration()
                        } else {
                            self.setupCaptureSession()
                            self.setupDevice()
                            self.setupInputOut()
                            self.setupPreviewLayer()
                            self.startRunningCaptureSession()
                        }
                    }
                        break
                    case "GENDER":  self.performSegue(withIdentifier: "name_gender", sender: self)
                        break
                    case "DOB": self.performSegue(withIdentifier: "name_dob", sender: self)
                        break
                    case "PLAYER_STATUS": self.performSegue(withIdentifier: "name_skill", sender: self)
                        break
                    case "LOCATION": self.performSegue(withIdentifier: "name_location", sender: self)
                        break
                    case "DASHBOARD": self.performSegue(withIdentifier: "name_dashboard", sender: self)
                        break
                    default:
                        print("DEFAULT CASE")
                    }
                    
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
                self.loadingViewPreventUserInteraction.isHidden = true
            })
        }
        
    }
    
    @IBAction func selfieButtonClick(_ sender: Any) {
        if #available(iOS 11.0, *) {
            let settings = AVCapturePhotoSettings()
            photoOutput?.capturePhoto(with: settings, delegate: self)
        } else {
            let settings = AVCapturePhotoSettings()
            let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
            let previewFormat = [
                kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
                kCVPixelBufferWidthKey as String: 160,
                kCVPixelBufferHeightKey as String: 160
            ]
            settings.previewPhotoFormat = previewFormat
            photoOutput?.capturePhoto(with: settings, delegate: self)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "imagepreview_segue" {
            let imagePreview = segue.destination as! ImagePreviewViewController
            imagePreview.image = self.image
            imagePreview.imageData = self.imageData1
            imagePreview.name = self.nameTextfield.text
        }
    }
    
    func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        print("Capturing")
        if let sampleBuffer = photoSampleBuffer, let previewBuffer = previewPhotoSampleBuffer, let ImageData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewBuffer) {
            print(ImageData)
            image = UIImage(data: ImageData)?.updateImageOrientionUpSide()
            imageData1 = UIImageJPEGRepresentation(image!, 0.025)
            performSegue(withIdentifier: "imagepreview_segue", sender: self)
        }
    }
    
    @available(iOS 11.0, *)
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let ImageData = photo.fileDataRepresentation() {
            print(ImageData)
            image = UIImage(data: ImageData)?.updateImageOrientionUpSide()
            imageData1 = UIImageJPEGRepresentation(image!, 0.025)
            performSegue(withIdentifier: "imagepreview_segue", sender: self)
        }
    }
    
    @IBAction func switchCameraButtonClick(_ sender: Any) {
        if currentCamera == backCamera {
            currentCamera = frontCamera
        } else {
            currentCamera = backCamera
        }
        
        if captureSession.isRunning {
            captureSession.beginConfiguration()
            
            let currentInput : AVCaptureInput = captureSession.inputs[0]
            captureSession.removeInput(currentInput)
            captureSession.removeOutput(photoOutput!)
            
            setupInputOut()
            
            captureSession.commitConfiguration()
        }
    }
    
    // Camera Config Methods
    func setupCaptureSession() {
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
    }
    
    func setupDevice() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        
        let devices = deviceDiscoverySession.devices
        for device in devices {
            if device.position == AVCaptureDevice.Position.front {
                frontCamera = device
            }
            if device.position == AVCaptureDevice.Position.back {
                backCamera = device
            }
        }
        currentCamera = frontCamera
    }
    
    func setupInputOut() {
        do {
            let captureDeviceInput = try AVCaptureDeviceInput(device: currentCamera!)
            captureSession.addInput(captureDeviceInput)
            photoOutput = AVCapturePhotoOutput()
            if #available(iOS 11.0, *) {
                photoOutput?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey:AVVideoCodecType.jpeg])], completionHandler: nil)
            } else {
                photoOutput?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey:AVVideoCodecJPEG])], completionHandler: nil)
            }
            captureSession.addOutput(photoOutput!)
            
        } catch {
            
        }
    }
    
    func setupPreviewLayer() {
        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        cameraPreviewLayer?.frame = self.cameraView.layer.bounds
        self.cameraView.layer.addSublayer(cameraPreviewLayer!)
        
        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        let rootLayer :CALayer = self.cameraView.layer
        rootLayer.masksToBounds=true
        cameraPreviewLayer?.frame = rootLayer.bounds
        rootLayer.addSublayer(self.cameraPreviewLayer!)
    }
    
    func startRunningCaptureSession() {
        captureSession.startRunning()
    }
    
    //Deinitialize
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //TextField Done Button Click event
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    //Keyboard Methods
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant = 0.0
            } else {
                self.keyboardHeightLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
}
