//
//  CricNewsFeedTableViewCell.swift
//  CricDost
//
//  Created by JIT GOEL on 10/26/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class CricNewsFeedTableViewCell: UITableViewCell {

    @IBOutlet weak var cricNewsImageView: UIImageView!
    @IBOutlet weak var cricNewsTitleLabel: UILabel!
    @IBOutlet weak var cricNewsContentLabel: UILabel!
    @IBOutlet weak var cricNewsShareButton: ShareButtonCustom!
    @IBOutlet weak var shareButton: ShareButtonCustom!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var spaceHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var newsSourceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }

}
