//
//  ScoreTableViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 6/27/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ScoreTableViewCell: UITableViewCell {

    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var C1: UILabel!
    @IBOutlet weak var C2: UILabel!
    @IBOutlet weak var C3: UILabel!
    @IBOutlet weak var C4: UILabel!
    @IBOutlet weak var C5: UILabel!
    @IBOutlet weak var howOutLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
