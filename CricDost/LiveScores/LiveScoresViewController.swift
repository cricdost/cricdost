//
//  LiveScoresViewController.swift
//  CricDost
//
//  Created by Jit Goel on 7/11/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import FirebaseDatabase

class LiveScoresViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var ref: DatabaseReference!
    var databaseHandleAdded: DatabaseHandle!
    var databaseHandleRemoved: DatabaseHandle!
    var databaseHandleChanged: DatabaseHandle!
    @IBOutlet weak var liveMatchButton: UIButton!
    @IBOutlet weak var completedMatchButton: UIButton!
    @IBOutlet weak var activityIndicator1: UIActivityIndicatorView!
    @IBOutlet weak var activityIndicator2: UIActivityIndicatorView!
    var matchesInfo: [Any] = []
    var liveMatches: [Any] = []
    var completedMatches: [Any] = []
    var removeElements: [Int] = []
    @IBOutlet weak var liveMatchCollectionView: UICollectionView!
    @IBOutlet weak var completedMatchCollectionView: UICollectionView!
    @IBOutlet weak var pagecontrolLive: UIPageControl!
    @IBOutlet weak var pagecontrolCompleted: UIPageControl!
    var scrollingTimer = Timer()
    var scrollingTimer1 = Timer()
    var rowIndex = 0
    var rowIndex1 = 0
    var matchTemp: NSDictionary?
    var teamATemp: NSDictionary?
    var teamBTemp: NSDictionary?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        
        activityIndicator1.startAnimating()
        activityIndicator2.startAnimating()
        
        databaseHandleAdded = ref.child("live_scores").observe(.childAdded, with: { (snapshot) in
            let response = snapshot.value
            
            if let data = response {
                self.matchesInfo.append(data)
            }
            self.activityIndicator1.stopAnimating()
            self.activityIndicator2.stopAnimating()
            
            self.refreshData()
        })
        
        databaseHandleChanged = ref.child("live_scores").observe(.childChanged, with: { (snapshot) in
            let response = snapshot.value
            
            if let data = response {

                print("RECEIVED CHANGES IN SCORE", data)
                for i in 0...(self.matchesInfo.count - 1) {
                    print(i)
                    let item = self.matchesInfo[i]
                    if (item as! NSDictionary)["id"] as! Int == (data as! NSDictionary)["id"] as! Int {
                        self.matchesInfo[i] = data
                    }
                }
                
                self.refreshData()
            }
        })
        
        databaseHandleRemoved = ref.child("live_scores").observe(.childRemoved, with: { (snapshot) in
            let response = snapshot.value
            
            if let data = response {
//                print("DATA Removed", data)
                print("RECEIVED REMOVED IN SCORE")
                self.removeElements = []
                for i in 0...(self.matchesInfo.count - 1) {
                    let item = self.matchesInfo[i]
                    if (item as! NSDictionary)["id"] as! Int == (data as! NSDictionary)["id"] as! Int {
                        print("ID Removed", (item as! NSDictionary)["id"] as! Int)
                        self.removeElements.append(i)
                    }
                }
                
                for index in self.removeElements {
                    self.matchesInfo.remove(at: index)
                }
                self.refreshData()
            }
        })
        
        liveMatchButton.setImage(#imageLiteral(resourceName: "baseline_fiber_manual_record_white_18pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        liveMatchButton.tintColor = UIColor(red:0.56, green:0.65, blue:0.27, alpha:1.0)
        
        completedMatchButton.setImage(#imageLiteral(resourceName: "baseline_fiber_manual_record_white_18pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        completedMatchButton.tintColor = CommonUtil.themeRed
        
        scrollingTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.startTimer(theTimer:)), userInfo: rowIndex, repeats: true)
        scrollingTimer1 = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.startTimer1(theTimer:)), userInfo: rowIndex1, repeats: true)
        
        setCollectionViewLayout(collectionViews: [liveMatchCollectionView, completedMatchCollectionView])
        CommonUtil.userActivityDetected(viewController: self, activityType: "VIEW LIVESCORE", status: "1")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        CommonUtil.updateGATracker(screenName: "Live International Scores")
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print(Int(scrollView.contentOffset.x) / Int(scrollView.frame.width))
        let section = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        if scrollView == liveMatchCollectionView {
            pagecontrolLive.currentPage = section
            rowIndex = section
        } else {
            pagecontrolCompleted.currentPage = section
            rowIndex1 = section
        }
    }
    
    func setCollectionViewLayout(collectionViews: [UICollectionView]) {
        for collectionView in collectionViews {
            let screensize1 = view.bounds.size
            let cellwidth1 = floor(screensize1.width)
            let cellheight1 = floor(collectionView.bounds.height)
            
            let layout1 = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout1.itemSize = CGSize(width: cellwidth1, height: cellheight1)
            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }
    }
    
    func refreshData() {
        liveMatches.removeAll()
        completedMatches.removeAll()
        for item in matchesInfo {
            if (item as! NSDictionary)["status"] as! Int == 3 {
                completedMatches.append(item)
            } else {
                liveMatches.append(item)
            }
        }
        if liveMatches.count == 0 {
            liveMatchButton.setTitle("NO LIVE MATCHES", for: .normal)
        } else {
            liveMatchButton.setTitle("LIVE MATCHES", for: .normal)
        }
        if completedMatches.count == 0 {
            completedMatchButton.setTitle("NO COMPLETED MATCHES", for: .normal)
        } else {
            completedMatchButton.setTitle("COMPLETED MATCHES", for: .normal)
        }
        pagecontrolCompleted.numberOfPages = completedMatches.count
        pagecontrolLive.numberOfPages = liveMatches.count
        liveMatchCollectionView.reloadData()
        completedMatchCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == liveMatchCollectionView {
            return liveMatches.count
        } else {
            return completedMatches.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == liveMatchCollectionView {
            let match = liveMatches[indexPath.row] as! NSDictionary
            let teamA = (match["team"] as! NSDictionary)["teamA"] as! NSDictionary
            let teamB = (match["team"] as! NSDictionary)["teamB"] as! NSDictionary
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "livescoresCell", for: indexPath) as! LiveScoresCollectionViewCell
            
            cell.venueLabel.text = match["type_str"] as? String
            cell.matchDetails.text = "\(match["trophy"] as! String), \(match["title"] as! String)"
            
            cell.teamAImageView.sd_setImage(with: URL(string: teamA["logo"] as! String), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            cell.teamBImageView.sd_setImage(with: URL(string: teamB["logo"] as! String), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            
            cell.teamAName.text = teamA["name"] as? String
            cell.teamBName.text = teamB["name"] as? String
            
            cell.liveButton.setImage(#imageLiteral(resourceName: "baseline_fiber_manual_record_white_18pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
            cell.liveButton.tintColor = UIColor(red:0.56, green:0.65, blue:0.27, alpha:1.0)
            if teamA["innings1"] as? NSDictionary != nil {
                if (teamA["innings1"] as! NSDictionary)["status"] as! Int == 1 || (teamA["innings1"] as! NSDictionary)["status"] as! Int == 2 {
                    var score = "\((teamA["innings1"] as! NSDictionary)["run_str"] as! String)"
                    if (teamA["innings2"] as! NSDictionary)["status"] as! Int == 1 || (teamA["innings2"] as! NSDictionary)["status"] as! Int == 2 {
                        score = "\(score) & \((teamA["innings2"] as! NSDictionary)["run_str"] as! String)"
                    }
                    cell.teamAscore.text = score
                } else {
                    cell.teamAscore.text = "-"
                }
            }
            
            if teamB["innings1"] as? NSDictionary != nil {
                if (teamB["innings1"] as! NSDictionary)["status"] as! Int == 1 {
                    var score = "\((teamB["innings1"] as! NSDictionary)["run_str"] as! String)"
                    if (teamB["innings2"] as! NSDictionary)["status"] as! Int == 1 {
                        score = "\(score) & \((teamB["innings2"] as! NSDictionary)["run_str"] as! String)"
                    }
                    cell.teamBscore.text = score
                } else {
                    cell.teamBscore.text = "-"
                }
            }
            cell.resultLabel.text = match["result"] as? String
            
            return cell
        } else {
            let match = completedMatches[indexPath.row] as! NSDictionary
            let teamA = (match["team"] as! NSDictionary)["teamA"] as! NSDictionary
            let teamB = (match["team"] as! NSDictionary)["teamB"] as! NSDictionary
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "livescoresCell", for: indexPath) as! LiveScoresCollectionViewCell
            
            cell.venueLabel.text = match["type_str"] as? String
            cell.matchDetails.text = "\(match["trophy"] as! String), \(match["title"] as! String)"
            
            cell.teamAImageView.sd_setImage(with: URL(string: teamA["logo"] as! String), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            cell.teamBImageView.sd_setImage(with: URL(string: teamB["logo"] as! String), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            
            cell.teamAName.text = teamA["name"] as? String
            cell.teamBName.text = teamB["name"] as? String
            
            cell.liveButton.setImage(#imageLiteral(resourceName: "baseline_fiber_manual_record_white_18pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
            cell.liveButton.tintColor = CommonUtil.themeRed
            
            if (teamA["innings1"] as? NSDictionary) != nil {
                if (teamA["innings1"] as! NSDictionary)["status"] as! Int == 3 {
                    var score = "\((teamA["innings1"] as! NSDictionary)["run_str"] as! String)"
                    if (teamA["innings2"] as! NSDictionary)["status"] as! Int == 3 {
                        score = "\(score) & \((teamA["innings2"] as! NSDictionary)["run_str"] as! String)"
                    }
                    cell.teamAscore.text = score
                } else {
                    cell.teamAscore.text = "-"
                }
            }
            if (teamB["innings1"] as? NSDictionary) != nil {
            if (teamB["innings1"] as! NSDictionary)["status"] as! Int == 3 {
                var score = "\((teamB["innings1"] as! NSDictionary)["run_str"] as! String)"
                if (teamB["innings2"] as! NSDictionary)["status"] as! Int == 3 {
                    score = "\(score) & \((teamB["innings2"] as! NSDictionary)["run_str"] as! String)"
                }
                cell.teamBscore.text = score
            } else {
                cell.teamBscore.text = "-"
            }
            }
            cell.resultLabel.text = match["result"] as? String
            
            return cell
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "live_fullscore" {
            let fullScore = segue.destination as! UINavigationController
            let fullScoreVC = fullScore.topViewController as? FullScoreCardViewController
            fullScoreVC?.isCalledFromInternational = true
            fullScoreVC?.matchTemp = matchTemp
            fullScoreVC?.teamATemp = teamATemp
            fullScoreVC?.teamBTemp = teamBTemp
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == liveMatchCollectionView {
            let match = liveMatches[indexPath.row] as! NSDictionary
            let teamA = (match["team"] as! NSDictionary)["teamA"] as! NSDictionary
            let teamB = (match["team"] as! NSDictionary)["teamB"] as! NSDictionary
            
            matchTemp = match
            teamATemp = teamA
            teamBTemp = teamB
            
            UserDefaults.standard.set(teamA["name"] as! String, forKey: CommonUtil.CURRENT_MATCH_TEAM_A_NAME)
            UserDefaults.standard.set(teamB["name"] as! String, forKey: CommonUtil.CURRENT_MATCH_TEAM_B_NAME)
            
        } else if collectionView == completedMatchCollectionView {
            let match = completedMatches[indexPath.row] as! NSDictionary
            let teamA = (match["team"] as! NSDictionary)["teamA"] as! NSDictionary
            let teamB = (match["team"] as! NSDictionary)["teamB"] as! NSDictionary
            
            matchTemp = match
            teamATemp = teamA
            teamBTemp = teamB
            
            UserDefaults.standard.set(teamA["name"] as! String, forKey: CommonUtil.CURRENT_MATCH_TEAM_A_NAME)
            UserDefaults.standard.set(teamB["name"] as! String, forKey: CommonUtil.CURRENT_MATCH_TEAM_B_NAME)
        }
        self.performSegue(withIdentifier: "live_fullscore", sender: self)
    }
    
    @objc func startTimer(theTimer: Timer) {
        
        let numberOfRecords = self.liveMatches.count - 1
        if rowIndex < numberOfRecords {
            rowIndex = rowIndex + 1
        } else {
            rowIndex = 0
        }
        
        if self.liveMatches.count != 0 {
            self.liveMatchCollectionView.scrollToItem(at: IndexPath(row: rowIndex, section: 0), at: .centeredHorizontally, animated: true)
            pagecontrolLive.currentPage = rowIndex
        }
    }
    
    @objc func startTimer1(theTimer: Timer) {
        
        let numberOfRecords = self.completedMatches.count - 1
        if rowIndex1 < numberOfRecords {
            rowIndex1 = rowIndex1 + 1
        } else {
            rowIndex1 = 0
        }
        if self.completedMatches.count != 0 {
            self.completedMatchCollectionView.scrollToItem(at: IndexPath(row: rowIndex1, section: 0), at: .centeredHorizontally, animated: true)
            pagecontrolCompleted.currentPage = rowIndex1
        }
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        self.scrollingTimer.invalidate()
        self.scrollingTimer1.invalidate()
        self.dismiss(animated: true, completion: nil)
    }
}







