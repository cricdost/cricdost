//
//  ChooseBowlingOrBattingViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 9/17/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ChooseBowlingOrBattingViewController: UIViewController, ShowsAlert {

    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet weak var teamNameLabel: UILabel!
    @IBOutlet weak var battingImageView: UIImageView!
    @IBOutlet weak var bowlingImageView: UIImageView!
    @IBOutlet weak var battingTickButton: UIButton!
    @IBOutlet weak var bowlingTickButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    var matchID: String?
    var adminDetail: NSDictionary?
    var bat_Bowl = ""
    var TOSS_WON: String?
    var TOSS_WON_NAME: String?
    var OVERS: String?
    var TOSS_LOSS: String?
    @IBOutlet weak var chooseLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [viewHolder])
        CommonUtil.imageRoundedCorners(imageviews: [battingImageView,bowlingImageView])
        CommonUtil.buttonRoundedCorners(buttons: [battingTickButton,bowlingTickButton,nextButton])
        
        let teamAtapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTappedTeamA))
        battingImageView.isUserInteractionEnabled = true
        battingImageView.addGestureRecognizer(teamAtapGestureRecognizer)
        
        let teamBtapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTappedTeamB))
        bowlingImageView.isUserInteractionEnabled = true
        bowlingImageView.addGestureRecognizer(teamBtapGestureRecognizer)
        
        if adminDetail!["ACCESS_ID"] as? String ?? "\(adminDetail!["ACCESS_ID"] as? Int ?? 0)" != UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String {
            nextButton.isHidden = true
            battingImageView.isUserInteractionEnabled = false
            bowlingImageView.isUserInteractionEnabled = false
            chooseLabel.text = "elected to"
        }
        
        
        let wonTeamObj = adminDetail!["WON_TEAM_OBJECT"] as? String ?? ""
        var dictonary:NSDictionary?
        
        if let data = wonTeamObj.data(using: String.Encoding.utf8) {
            
            do {
                dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
                
                if let myDictionary = dictonary
                {
                    print(" First name is: \(myDictionary)")
                    TOSS_LOSS = myDictionary["mLossId"] as? String ?? "\(myDictionary["mLossId"] as! Int)"
                }
            } catch let error as NSError {
                print(error)
            }
        }
        
        teamNameLabel.text = adminDetail!["WON_TEAM_NAME"] as? String
        TOSS_WON = adminDetail!["TOSS_WON_TEAM_ID"] as? String ?? "\(adminDetail!["TOSS_WON_TEAM_ID"] as! Int)"
        TOSS_WON_NAME = adminDetail!["WON_TEAM_NAME"] as? String
        OVERS = adminDetail!["MATCH_OVER_COUNT"] as? String ?? "\(adminDetail!["MATCH_OVER_COUNT"] as! Int)"
        TOSS_LOSS = adminDetail!["TOSS_LOSS"] as? String ?? TOSS_LOSS
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshBatBowlPage), name: NSNotification.Name(rawValue: "refreshBatBowlPage"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissBatBowlView), name: NSNotification.Name(rawValue: "dismissBatBowlView"), object: nil)
    }
    
    @objc func dismissBatBowlView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func imageTappedTeamA() {
        battingTickButton.isHidden = false
        bowlingTickButton.isHidden = true
        bat_Bowl = "BAT"
        send_Emit(stepName: "over_count", batorBowl: 1)
    }
    
    @objc func imageTappedTeamB() {
        battingTickButton.isHidden = true
        bowlingTickButton.isHidden = false
        bat_Bowl = "BOWL"
        send_Emit(stepName: "over_count",batorBowl: 2)
    }
    
    func send_Emit(stepName: String,batorBowl: Int) {
//        let data: [String: Any] = ["match_id": matchID!,
//                                   "user_id": UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String,
//                                   "step_name":stepName,
//                                   "won_team_id":batorBowl,
//            "won_team_name": adminDetail!["WON_TEAM_NAME"] as! String,
//            "bat_or_bowl":bat_Bowl,
//            "won_team_object":""]
        let data: [String: Any] = ["match_id": matchID!,
                                   "user_id": UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String,
                                   "step_name":stepName,
                                   "won_team_id":batorBowl,
                                   "won_team_name": "",
                                   "bat_or_bowl":bat_Bowl,
                                   "won_team_object":""]
        SocketConnectionsclass.matchStatusUpdate_NewFlow(param: data)
    }

    @IBAction func nextButtonClick(_ sender: Any) {
        print("Choose bowling or batting next button clicked")
        if bat_Bowl != "" {
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\":\"\(matchID!)\",\"TOSS_WON\":\"\(TOSS_WON!)\",\"TOSS_LOSS\":\"\(TOSS_LOSS!)\", \"TOSS_WON_NAME\":\"\(TOSS_WON_NAME!)\",\"OVERS\":\"\(OVERS!)\",\"TOSS_DECISION\":\"\(bat_Bowl)\"}", mod: "MatchScore", actionType: "toss") { (response) in
                if response as? String != "error" {
                    print(response)
                    self.send_Emit(stepName: "toss_won_selection",batorBowl: Int(self.TOSS_WON!)!)
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
            }
        } else {
            self.viewHolder.shake()
            self.showAlert(title: "Choose bat or bowl", message: "Please select an option ")
        }
        
    }

    @objc func refreshBatBowlPage(_ notification: NSNotification) {
        let adminDetail = notification.userInfo![AnyHashable("matchAdminDetail")] as! NSDictionary
        print("Refreshing BAT BOWL UI", adminDetail)
        
        if adminDetail["TOSS_WON_TEAM_ID"] as? String ?? "\(adminDetail["TOSS_WON_TEAM_ID"] as! Int)" == "1" {
            battingTickButton.isHidden = false
            bowlingTickButton.isHidden = true
            bat_Bowl = "BAT"
        } else {
            battingTickButton.isHidden = true
            bowlingTickButton.isHidden = false
            bat_Bowl = "BOWL"
        }
    }
}



