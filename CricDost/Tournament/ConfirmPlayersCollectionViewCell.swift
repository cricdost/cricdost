//
//  ConfirmPlayersCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 9/1/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ConfirmPlayersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var playerProfileImage: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        CommonUtil.imageRoundedCorners(imageviews: [playerProfileImage])
    }
}
