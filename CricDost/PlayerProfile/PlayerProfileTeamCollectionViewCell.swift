//
//  PlayerProfileTeamCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 6/4/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class PlayerProfileTeamCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var teamImageView: UIImageView!
    @IBOutlet weak var adminButton: UIButton!
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var teamNameLabel: UILabel!
    @IBOutlet weak var teamAddressLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        CommonUtil.buttonRoundedCorners(buttons: [adminButton, joinButton])
        CommonUtil.imageRoundedCorners(imageviews: [teamImageView])
        
//        self.layer.cornerRadius = 3.0
//        layer.shadowRadius = 2
//        layer.shadowOpacity = 0.3
//        layer.shadowOffset = CGSize(width: 2, height: 2)
//        self.clipsToBounds = false
    }
    
}
