//
//  BreakViewController.swift
//  CricDost
//
//  Created by Jit Goel on 6/28/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class BreakViewController: UIViewController, ShowsAlert, UITextFieldDelegate {

    @IBOutlet weak var breakView: UIView!
    @IBOutlet weak var infoTextField: UITextField!
    @IBOutlet weak var oversTextField: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    @IBOutlet weak var lineView: UIView!
    var abandon = false
    var editOver = false
    var nextBall = 0.0
    var over = "0"
    var MATCH_ID: String?
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonUtil.buttonRoundedCorners(buttons: [cancelButton, infoButton])
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [breakView])
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)
        
        self.addDoneButtonOnKeyboard()
        
        if editOver {
            titleLabel.text = "Edit Over"
            lineView.isHidden = true
            infoTextField.isHidden = true
            oversTextField.isHidden = false
            oversTextField.text = over
            infoButton.setTitle("Edit Over", for: .normal)
        } else {
            infoTextField.isHidden = false
            oversTextField.isHidden = true
            lineView.isHidden = false
            infoButton.setTitle("Add Info", for: .normal)
            if abandon {
                titleLabel.text = "Reason to Abandon Match"
            } else {
                titleLabel.text = "Reason for Break"
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == oversTextField {
            let maxLength = 2
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.oversTextField.inputAccessoryView = doneToolbar
        self.infoTextField.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction() {
        self.oversTextField.resignFirstResponder()
        self.infoTextField.resignFirstResponder()
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant = 0.0
            } else {
                self.keyboardHeightLayoutConstraint?.constant = -(endFrame?.size.height ?? 0.0)/2
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    @IBAction func cancelButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func infoButtonClick(_ sender: Any) {
        if editOver {
            print("Edit")
            if !oversTextField.isReallyEmpty {
                print(Int(Double(oversTextField.text!)!))
                if Int(Double(oversTextField.text!)!) > Int(nextBall) {
                    editOvers(overs: oversTextField.text!)
                } else {
                    showAlert(title: "Overs", message: "Overs cant be edited lesser than current over")
                }
            } else {
                showAlert(title: "Overs", message: "Please enter number of overs")
            }
        } else {
            if abandon {
                print("Abandon")
                if !infoTextField.isReallyEmpty {
                    breakAbandonMatch(status: "ABANDONED", info: infoTextField.text!)
                } else {
                    showAlert(title: "Abandon Reason", message: "Please give a reason for abandon match")
                }
            } else {
                print("Break")
                if !infoTextField.isReallyEmpty {
                    breakAbandonMatch(status: "BREAK", info: infoTextField.text!)
                } else {
                    showAlert(title: "Break Reason", message: "Please give a reason for break")
                }
            }
        }
    }
    
    func editOvers(overs: String) {
        activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\":\"\(MATCH_ID!)\", \"OVERS\":\"\(overs)\"}", mod: "MatchScore", actionType: "update-match-overs") { (response) in
            if response as? String != "error" {
                print(response)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshScorerPage"), object: nil)
                SocketConnectionsclass.matchScoreUpdateEmit(match_id: self.MATCH_ID!)
                self.dismiss(animated: true, completion: nil)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
        }
    }
    
    func breakAbandonMatch(status: String, info: String) {
        activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\":\"\(MATCH_ID!)\", \"STATUS\":\"\(status)\",\"ADDITIONAL_INFO\":\"\(info)\"}", mod: "MatchScore", actionType: "update-match-status") { (response) in
            if response as? String != "error" {
                print(response)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshScorerPage"), object: nil)
                SocketConnectionsclass.matchScoreUpdateEmit(match_id: self.MATCH_ID!)
                self.dismiss(animated: true, completion: nil)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
        }
    }
}
