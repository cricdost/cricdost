//
//  TossWonByViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 9/17/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class TossWonByViewController: UIViewController, ShowsAlert {

    @IBOutlet weak var tossWonByViewHolder: UIView!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var teamAImageView: UIImageView!
    @IBOutlet weak var teamBImageView: UIImageView!
    @IBOutlet weak var teamATickButton: UIButton!
    @IBOutlet weak var teamBTickButton: UIButton!
    @IBOutlet weak var teamALabel: UILabel!
    @IBOutlet weak var teamBLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var coinSideOne: UIImageView!
    @IBOutlet weak var coinSideTwo: UIImageView!
    var matchID: String?
    var adminDetail: NSDictionary?
    var TOSS_WON_NAME: String?
    var TOSS_WON: String?
    var TOSS_LOSS: String?
    var teamA: NSDictionary = [:]
    var teamB: NSDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        CommonUtil.roundedUIViewCornersWithShade(uiviews: [tossWonByViewHolder])
        CommonUtil.imageRoundedCorners(imageviews: [teamAImageView,teamBImageView])
        CommonUtil.buttonRoundedCorners(buttons: [nextButton,teamATickButton,teamBTickButton])
        refreshButton.setImage(#imageLiteral(resourceName: "baseline_refresh_white_24pt_1x").withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        refreshButton.tintColor = CommonUtil.themeRed
        
        let teamAtapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTappedTeamA))
        teamAImageView.isUserInteractionEnabled = true
        teamAImageView.addGestureRecognizer(teamAtapGestureRecognizer)
        
        let teamBtapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTappedTeamB))
        teamBImageView.isUserInteractionEnabled = true
        teamBImageView.addGestureRecognizer(teamBtapGestureRecognizer)
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshTossPage), name: NSNotification.Name(rawValue: "refreshTossPage"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissTossWonView), name: NSNotification.Name(rawValue: "dismissTossWonView"), object: nil)
        
        getMatchTeams()
        teamATickButton.isHidden = true
        teamBTickButton.isHidden = true
    }
    
    @objc func imageTappedTeamA() {
        teamATickButton.isHidden = false
        teamBTickButton.isHidden = true
//        TOSS_WON = teamA["MATCH_TEAM_ID"] as? String
        TOSS_WON = "1"
        TOSS_LOSS = teamB["MATCH_TEAM_ID"] as? String
        TOSS_WON_NAME = teamA["TEAM_NAME"] as? String
        send_Emit()
    }
    
    @objc func imageTappedTeamB() {
        teamATickButton.isHidden = true
        teamBTickButton.isHidden = false
        TOSS_LOSS = teamA["MATCH_TEAM_ID"] as? String
//        TOSS_WON = teamB["MATCH_TEAM_ID"] as? String
        TOSS_WON = "2"
        TOSS_WON_NAME = teamB["TEAM_NAME"] as? String
        send_Emit()
    }
    
    @objc func dismissTossWonView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        flipTransition(with: coinSideOne, view2: coinSideTwo)
        if adminDetail!["ACCESS_ID"] as? String ?? "\(adminDetail!["ACCESS_ID"] as! Int)" != UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String {
            refreshButton.isHidden = true
            nextButton.isHidden = true
            teamAImageView.isUserInteractionEnabled = false
            teamBImageView.isUserInteractionEnabled = false
        } else {
//            flipTransition(with: coinSideOne, view2: coinSideTwo)
            tossCoin()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.8, execute: {
            UIView.transition(with: self.tossWonByViewHolder, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.coinSideOne.isHidden = true
                self.coinSideTwo.isHidden = true
                self.tossWonByViewHolder.isHidden = false
            }, completion: nil)
        })
    }
    
    @IBAction func refreshButtonClick(_ sender: Any) {
        UIView.transition(with: self.tossWonByViewHolder, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.coinSideOne.isHidden = false
            self.coinSideTwo.isHidden = false
            self.tossWonByViewHolder.isHidden = true
        }, completion: nil)
        tossCoin()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            UIView.transition(with: self.tossWonByViewHolder, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.coinSideOne.isHidden = true
                self.coinSideTwo.isHidden = true
                self.tossWonByViewHolder.isHidden = false
            }, completion: nil)
        })
    }
    
    @IBAction func nextButtonClick(_ sender: Any) {
        print("toss won by team clicked")
        if TOSS_WON == "1" {
            TOSS_WON = teamA["MATCH_TEAM_ID"] as? String
            TOSS_WON_NAME = teamA["TEAM_NAME"] as? String
            TOSS_LOSS = teamB["MATCH_TEAM_ID"] as? String
        } else if TOSS_WON == "2" {
            TOSS_LOSS = teamA["MATCH_TEAM_ID"] as? String
            TOSS_WON = teamB["MATCH_TEAM_ID"] as? String
            TOSS_WON_NAME = teamB["TEAM_NAME"] as? String
        }
        print(TOSS_WON,TOSS_WON_NAME)
        let data: [String : Any] = ["TOSS_LOSS":TOSS_LOSS,"TOSS_WON":TOSS_WON, "TOSS_WON_NAME":TOSS_WON_NAME, "MATCH_ID":"\(matchID!)","TeamA":teamA,"TeamB":teamA]
        print(data)
        self.dismiss(animated: true) {
            let tossDetail:[String: Any] = ["tossDetail": data]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openMatchOverViewController"), object: nil, userInfo: tossDetail)
        }
    }
    
    func flipTransition (with view1: UIImageView, view2: UIImageView, isReverse: Bool = false) {
        var transitionOptions = UIViewAnimationOptions()
        transitionOptions = isReverse ? [.transitionFlipFromLeft,.repeat] : [.transitionFlipFromRight, .repeat] // options for transition
        
        // animation durations are equal so while first will finish, second will start
        // below example could be done also using completion block.
        
        UIView.transition(with: view1, duration: 0.5, options: transitionOptions, animations: {
            view1.isHidden = true
            view2.isHidden = false
        })
        
        UIView.transition(with: view2, duration: 0.5, options: transitionOptions, animations: {
            view1.isHidden = false
            view2.isHidden = true
        })
    }
    
    func getMatchTeams() {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\":\"\(matchID!)\"}", mod: "Match", actionType: "get-match-teams", callback: { (response) in
            print(response)
            if response as? String != "error" {
                self.teamA = (((response as! NSDictionary)["XSCData"] as! NSDictionary)["TEAM_A"] as! NSDictionary)
                self.teamB = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["TEAM_B"] as! NSDictionary
                self.teamALabel.text = self.teamA["TEAM_NAME"] as? String
                self.teamBLabel.text = self.teamB["TEAM_NAME"] as? String
                self.teamAImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(self.teamA["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
                self.teamBImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(self.teamB["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
        })
    }
    
    func send_Emit() {
        let data: [String: Any] = ["match_id": matchID!,
                                   "user_id": UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String,
                                   "step_name":"toss_match",
                                   "won_team_id":"\(TOSS_WON!)",
            "won_team_name":"\(TOSS_WON_NAME!)",
            "bat_or_bowl":"",
            "won_team_object":""]
        
        SocketConnectionsclass.matchStatusUpdate_NewFlow(param: data)
    }
    
    func tossCoin() {
        let randomNumber = arc4random_uniform(1000) + 10
        print(randomNumber)
        if randomNumber % 2 == 0 {
            print("Team A Won toss")
//            TOSS_WON = teamA["MATCH_TEAM_ID"] as? String ?? "\(teamA["MATCH_TEAM_ID"] as? Int ?? 0)"
            TOSS_WON = "1"
            TOSS_LOSS = teamB["MATCH_TEAM_ID"] as? String ?? "\(teamB["MATCH_TEAM_ID"] as? Int ?? 0)"
            TOSS_WON_NAME = teamA["TEAM_NAME"] as? String ?? "\(teamA["TEAM_NAME"] as? Int ?? 0)"
            teamATickButton.isHidden = false
            teamBTickButton.isHidden = true
            send_Emit()
        } else {
            print("Team B Won toss")
            TOSS_LOSS = teamA["MATCH_TEAM_ID"] as? String ?? "\(teamA["MATCH_TEAM_ID"] as? Int ?? 0)"
//            TOSS_WON = teamB["MATCH_TEAM_ID"] as? String ?? "\(teamB["MATCH_TEAM_ID"] as? Int ?? 0)"
            TOSS_WON = "2"
            TOSS_WON_NAME = teamB["TEAM_NAME"] as? String ?? "\(teamB["TEAM_NAME"] as? Int ?? 0)"
            teamATickButton.isHidden = true
            teamBTickButton.isHidden = false
            send_Emit()
        }
    }
    
    @objc func refreshTossPage(_ notification: NSNotification) {
        let adminDetail = notification.userInfo![AnyHashable("matchAdminDetail")] as! NSDictionary
        print("Refreshing UI", adminDetail)
        if adminDetail["TOSS_WON_TEAM_ID"] as? String ?? "\(adminDetail["TOSS_WON_TEAM_ID"] as! Int)"  == "1" {
            teamATickButton.isHidden = false
            teamBTickButton.isHidden = true
//            TOSS_WON = teamA["MATCH_TEAM_ID"] as? String ?? "\(teamA["MATCH_TEAM_ID"] as? Int ?? 0)"
            TOSS_WON = "1"
            TOSS_LOSS = teamB["MATCH_TEAM_ID"] as? String ?? "\(teamB["MATCH_TEAM_ID"] as? Int ?? 0)"
            TOSS_WON_NAME = teamA["TEAM_NAME"] as? String ?? "\(teamA["TEAM_NAME"] as? Int ?? 0)"
        } else if adminDetail["TOSS_WON_TEAM_ID"] as? String ?? "\(adminDetail["TOSS_WON_TEAM_ID"] as! Int)"  == "2" {
            teamATickButton.isHidden = true
            teamBTickButton.isHidden = false
            TOSS_LOSS = teamA["MATCH_TEAM_ID"] as? String ?? "\(teamA["MATCH_TEAM_ID"] as? Int ?? 0)"
//            TOSS_WON = teamB["MATCH_TEAM_ID"] as? String ?? "\(teamB["MATCH_TEAM_ID"] as? Int ?? 0)"
            TOSS_WON = "2"
            TOSS_WON_NAME = teamB["TEAM_NAME"] as? String ?? "\(teamB["TEAM_NAME"] as? Int ?? 0)"
        } else {
            teamATickButton.isHidden = true
            teamBTickButton.isHidden = true
        }
    }
}
