//
//  JoinMatchViewController.swift
//  CricDost
//
//  Created by Jit Goel on 5/28/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import CoreLocation
import Lottie
import LTMorphingLabel
import AnimatedCollectionViewLayout

class JoinMatchViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, CLLocationManagerDelegate, ShowsAlert,LTMorphingLabelDelegate {
    
    @IBOutlet weak var matchViewCard: UIView!
    @IBOutlet weak var matchCollectionVIew: UICollectionView!
    @IBOutlet weak var createMatchButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var selectTeamColectionVIew: UICollectionView!
    @IBOutlet weak var chooseTeamCardView: UIView!
    @IBOutlet weak var selectPlayerCardView: UIView!
    @IBOutlet weak var selectPlayerCollectionView: UICollectionView!
    @IBOutlet weak var playerSelectedChallengeButton: UIButton!
    @IBOutlet weak var numOfSelectedPlayersLabel: UILabel!
    @IBOutlet weak var clearSelectedPlayersList: UILabel!
    @IBOutlet weak var requestSendCardView: UIView!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var noMatchFoundLabel: UILabel!
    @IBOutlet weak var getStartedButton: UIButton!
    @IBOutlet weak var shakeView: UIView!
    @IBOutlet weak var matchesNearYouLabel: LTMorphingLabel!
    @IBOutlet weak var selectYourTeamLabel: LTMorphingLabel!
    @IBOutlet weak var choosePlayerForThisChallengeLabel: LTMorphingLabel!
    
    let layout = AnimatedCollectionViewLayout()
    var manager: CLLocationManager?
    var latitude = 0.00
    var longitude = 0.00
    var locationSet = true
    var matchList: NSArray = []
    var myTeamList: NSArray = []
    var myPlayerList: NSArray = []
    var currentMatchID: String?
    var currentTeamID: String?
    var _selectedCells : NSMutableArray = []
    var _selectedPlayers : [String] = []
    @IBOutlet weak var loadingViewPreventUserInteraction: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        matchesNearYouLabel.text = "..."
//        selectYourTeamLabel.text = "..."
//        choosePlayerForThisChallengeLabel.text = "..."
        CommonUtil.buttonRoundedCorners(buttons: [createMatchButton,playerSelectedChallengeButton, okButton, getStartedButton])
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [matchViewCard, chooseTeamCardView, selectPlayerCardView, requestSendCardView, shakeView])
        
        layout.animator = LinearCardAttributesAnimator(minAlpha: 1.0, itemSpacing: 0.19, scaleRate: 0.88)
        layout.scrollDirection = .horizontal
        matchCollectionVIew.collectionViewLayout = layout
        matchCollectionVIew.isPagingEnabled = true
        
//        setCollectionViewLayout(card: matchViewCard, collectionView: matchCollectionVIew, center: true)
//        setCollectionViewLayout(card: chooseTeamCardView, collectionView: selectTeamColectionVIew, center: false)
//        setCollectionViewLayout(card: selectPlayerCardView, collectionView: selectPlayerCollectionView, center: false)
        selectTeamColectionVIew.isPagingEnabled = true
        selectPlayerCollectionView.isPagingEnabled = true
        
        OperationQueue.main.addOperation{
            self.manager = CLLocationManager()
            self.manager?.delegate = self
            self.manager?.desiredAccuracy = kCLLocationAccuracyBest
            self.manager?.requestWhenInUseAuthorization()
            self.manager?.startUpdatingLocation()
        }
        
        selectPlayerCollectionView.allowsMultipleSelection = true
        
        let clearAll = UITapGestureRecognizer(target: self, action: #selector(clearAllTapFunction))
        clearSelectedPlayersList.addGestureRecognizer(clearAll)
        
        setLabelMorph(labels: [matchesNearYouLabel], effect: .fall)
        setLabelMorph(labels: [selectYourTeamLabel], effect: .fall)
        setLabelMorph(labels: [choosePlayerForThisChallengeLabel], effect: .fall)
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        matchesNearYouLabel.text = "Matches Near You"
//        selectYourTeamLabel.text = "Select Your Team"
//        choosePlayerForThisChallengeLabel.text = "Choose players for this challenge"
    }
    
    func setLabelMorph(labels: [LTMorphingLabel], effect: LTMorphingEffect) {
        for label in labels {
            label.delegate = self
            label.morphingEffect = effect
            label.start()
        }
    }
    
    @IBAction func clearAllTapFunction(_ sender: Any) {
        print("Clear All")
        _selectedPlayers.removeAll()
        _selectedCells.removeAllObjects()
        selectPlayerCollectionView.reloadData()
        numOfSelectedPlayersLabel.text = "\(_selectedCells.count) Selected"
    }
    
    @IBAction func getStartedButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func skipButtonClick(_ sender: Any) {
        if UserDefaults.standard.object(forKey: CommonUtil.SHAKE) as? Bool ?? true {
            shakeView.isHidden = false
            matchViewCard.isHidden = true
            chooseTeamCardView.isHidden = true
            selectPlayerCardView.isHidden = true
            requestSendCardView.isHidden = true
            self.addShakeView()
           UserDefaults.standard.set(false, forKey: CommonUtil.SHAKE)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func okButtonClicked(_ sender: Any) {
        if UserDefaults.standard.object(forKey: CommonUtil.SHAKE) as? Bool ?? true {
            shakeView.isHidden = false
            matchViewCard.isHidden = true
            chooseTeamCardView.isHidden = true
            selectPlayerCardView.isHidden = true
            requestSendCardView.isHidden = true
            self.addShakeView()
            UserDefaults.standard.set(false, forKey: CommonUtil.SHAKE)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func createMatchButtonClick(_ sender: Any) {
//        if UserDefaults.standard.object(forKey: CommonUtil.SHAKE) as? Bool ?? true {
//            shakeView.isHidden = false
//            matchViewCard.isHidden = true
//            chooseTeamCardView.isHidden = true
//            selectPlayerCardView.isHidden = true
//            requestSendCardView.isHidden = true
//            addShakeView()
//            UserDefaults.standard.set(false, forKey: CommonUtil.SHAKE)
//        } else {
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CreateMatchVCFunction"), object: nil)
        })
//        }
    }
    
    @IBAction func challengeAfterPlayerSelectedButtonClick(_ sender: Any) {
        if _selectedPlayers.count != 0 {
            activityIndicator.startAnimating()
            loadingViewPreventUserInteraction.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\": \"\(currentMatchID!)\",\"TEAM_ID\":\"\(currentTeamID!)\",\"PLAYERS\":\(_selectedPlayers)}", mod: "Match", actionType: "interest-match") { (response: Any) in
                print(response)
                if response as? String != "error" {
                    
                    if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                        UIView.transition(with: self.requestSendCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                            self.requestSendCardView.isHidden = false
                        }, completion: nil)
                        UIView.transition(with: self.selectPlayerCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                            self.selectPlayerCardView.isHidden = true
                        }, completion: nil)
                    } else {
                        self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String )
                    }
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
                self.loadingViewPreventUserInteraction.isHidden = true
            }
        } else {
            self.showAlert(title: "No Player Selected", message: "Please at least one player")
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == selectTeamColectionVIew {
            return myTeamList.count
        } else if collectionView == selectPlayerCollectionView {
            return myPlayerList.count
        } else {
            return matchList.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == selectTeamColectionVIew {
            let team = myTeamList[indexPath.section] as! NSDictionary
            
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "selectTeam", for: indexPath) as! SelectTeamForChallengeCollectionViewCell
            CommonUtil.imageRoundedCorners(imageviews: [cell1.teamImageView])
            cell1.teamImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(team["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            cell1.teamNameLabel.text = team["TEAM_NAME"] as? String
            
            return cell1
            
        } else if collectionView == selectPlayerCollectionView {
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "selectPlayers", for: indexPath) as! SelectPlayerCollectionViewCell
            let player = myPlayerList[indexPath.section] as! NSDictionary
            CommonUtil.imageRoundedCorners(imageviews: [cell2.playerImageView])
            CommonUtil.buttonRoundedCorners(buttons: [cell2.tickButton])
            cell2.playerName.text = player["FULL_NAME"] as? String
            cell2.playerImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(player["IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "profile_pic_default"))
            if _selectedCells.contains(indexPath) {
                cell2.isSelected=true
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
                cell2.tickButton.isHidden=false
            } else{
                cell2.isSelected=false
                cell2.tickButton.isHidden=true
            }
            return cell2
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "matchCell", for: indexPath) as! MatchCollectionViewCell
            
            let match = (matchList[indexPath.section] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
            let teamA = match["TEAM_A"] as! NSDictionary
            let teamB = match["TEAM_B"] as! NSDictionary
            
            CommonUtil.imageRoundedCorners(imageviews: [cell.team1ImageView, cell.team2ImageView])
            CommonUtil.buttonRoundedCorners(buttons: [cell.joinMatchBUtton, cell.challengeButton])
            
            cell.dateLabel.text = match["DATE"] as? String
            cell.timeLabel.text = match["TIME"] as? String
            cell.numOfPlayersLabel.text = "\(match["AVAILABLE_PLAYERS"] as? String ?? String(match["AVAILABLE_PLAYERS"] as! Int) )/\(match["TOTAL_PLAYERS"] as? String ?? String(match["TOTAL_PLAYERS"] as! Int)) Players"
            
            cell.team1NameLabel.text = teamA["TEAM_NAME"] as? String
            cell.team1ImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamA["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            cell.addressLabel.text = match["ADDRESS"] as? String
            if  teamB["POSITION_STATUS"] as! Int == 0 {
                cell.team2ImageView.image = nil
                cell.team2NameLabel.text = "Challengers"
                cell.numOfChallengers.isHidden = false
                cell.numOfChallengers.text = match["INTEREST_COUNT"] as? String
            } else {
                cell.numOfChallengers.isHidden = true
                cell.team2ImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamB["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
                cell.team2NameLabel.text = teamB["TEAM_NAME"] as? String
            }
            if match["CAN_CHALLENGE"] as! Int == 1 {
                cell.challengeButton.isHidden = false
            } else {
                cell.challengeButton.isHidden = true
            }
            
            if match["CAN_JOIN"] as! Int == 1 {
                cell.joinMatchBUtton.isHidden = false
            } else {
                cell.joinMatchBUtton.isHidden = true
            }
            
            cell.joinMatchBUtton.addTarget(self, action: #selector(joinMatchBUttonTapped), for: UIControlEvents.touchUpInside)
            cell.challengeButton.addTarget(self, action: #selector(challengeButtonTapped), for: UIControlEvents.touchUpInside)
            cell.joinMatchBUtton.tag = indexPath.section
            cell.challengeButton.tag = indexPath.section
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == selectTeamColectionVIew {
            let team = myTeamList[indexPath.section] as! NSDictionary
            currentTeamID = team["TEAM_ID"] as? String
            print(team)
            self.activityIndicator.startAnimating()
            self.loadingViewPreventUserInteraction.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\": \"\(currentMatchID!)\",\"TEAM_ID\":\"\(currentTeamID!)\"}", mod: "Team", actionType: "challenge-match-team-players") { (response: Any) in
                print(response)
                if response as? String != "error" {
                    let players = (response as! NSDictionary)["XSCData"] as! NSArray
                    self.myPlayerList = players
                    if self.myPlayerList.count == 1 {
                        self.setCollectionViewLayout(card: self.selectPlayerCardView, collectionView: self.selectPlayerCollectionView, center: true)
                    }
                    self.selectPlayerCollectionView.reloadData()
                    UIView.transition(with: self.chooseTeamCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.chooseTeamCardView.isHidden = true
                    }, completion: nil)
                    UIView.transition(with: self.selectPlayerCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.selectPlayerCardView.isHidden = false
                    }, completion: nil)
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
                self.loadingViewPreventUserInteraction.isHidden = true
            }
        } else if collectionView == selectPlayerCollectionView {
            let player = myPlayerList[indexPath.section] as! NSDictionary
            print(player)
            if _selectedCells.count < 25 {
                if player["CAN_PLAYER_JOIN"] as! Int == 1 {
                    _selectedCells.add(indexPath)
                    _selectedPlayers.append(player["PLAYER_ID"] as! String)
                    collectionView.reloadItems(at: [indexPath])
                    numOfSelectedPlayersLabel.text = "\(_selectedCells.count) Selected"
                } else {
                    self.showAlert(message: player["CAN_PLAYER_JOIN_REASON"] as! String)
                }
            } else {
                self.showAlert(message: "Maximum number of players selected")
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == selectPlayerCollectionView {
            let player = myPlayerList[indexPath.section] as! NSDictionary
            print(player)
            _selectedCells.remove(indexPath)
            //            _selectedPlayers.remove(player["PLAYER_ID"] as! String)
            _selectedPlayers = _selectedPlayers.filter{$0 != player["PLAYER_ID"] as! String}
            collectionView.reloadItems(at: [indexPath])
            numOfSelectedPlayersLabel.text = "\(_selectedCells.count) Selected"
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == matchCollectionVIew {
            return CGSize(width: collectionView.bounds.width / CGFloat(1), height: collectionView.bounds.height / CGFloat(1))
        } else {
            return CGSize(width: collectionView.bounds.width / 3, height: collectionView.bounds.height )

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    @IBAction func joinMatchBUttonTapped(_ sender: UIButton) {
        let match = (matchList[sender.tag] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        currentMatchID = match["MATCH_ID"] as? String
        activityIndicator.startAnimating()
        loadingViewPreventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\": \"\(currentMatchID!)\"}", mod: "Match", actionType: "join-match", callback: { (response: Any) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    if UserDefaults.standard.object(forKey: CommonUtil.SHAKE) as? Bool ?? true {
                        self.shakeView.isHidden = false
                        self.matchViewCard.isHidden = true
                        self.chooseTeamCardView.isHidden = true
                        self.selectPlayerCardView.isHidden = true
                        self.requestSendCardView.isHidden = true
                        self.addShakeView()
                        UserDefaults.standard.set(false, forKey: CommonUtil.SHAKE)
                    } else {
                    self.dismiss(animated: true, completion: nil)
                    }
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.loadingViewPreventUserInteraction.isHidden = true
        })
        
    }
    
    @IBAction func challengeButtonTapped(_ sender: UIButton) {
        let match = (matchList[sender.tag] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        currentMatchID = match["MATCH_ID"] as? String
        activityIndicator.startAnimating()
        loadingViewPreventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Match", actionType: "create-match-team-list") { (response: Any) in
            if response as? String != "error" {
                let teams = (response as! NSDictionary)["XSCData"] as! NSArray
                print(teams)
                self.myTeamList = teams
                if self.myTeamList.count == 1 {
                    self.setCollectionViewLayout(card: self.chooseTeamCardView, collectionView: self.selectTeamColectionVIew, center: false)
                }
                self.selectTeamColectionVIew.reloadData()
                UIView.transition(with: self.chooseTeamCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                    self.chooseTeamCardView.isHidden = false
                }, completion: nil)
                UIView.transition(with: self.matchViewCard, duration: 0.4, options: .transitionCrossDissolve, animations: {
                    self.matchViewCard.isHidden = true
                }, completion: nil)
                if self.myTeamList.count == 0 {
                    let alert = UIAlertController(title: "No Teams", message: "You don't own a team. Create a team now?", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Create", style: UIAlertActionStyle.default, handler: { action in
                        
                        if UserDefaults.standard.object(forKey: CommonUtil.SHAKE) as? Bool ?? true {
                            self.shakeView.isHidden = false
                            self.matchViewCard.isHidden = true
                            self.chooseTeamCardView.isHidden = true
                            self.selectPlayerCardView.isHidden = true
                            self.requestSendCardView.isHidden = true
                            self.addShakeView()
                            UserDefaults.standard.set(false, forKey: CommonUtil.SHAKE)
                        } else {
                        self.dismiss(animated: true, completion: {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "createTeamVCFunction"), object: nil)
                        })
                        }
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.loadingViewPreventUserInteraction.isHidden = true
        }
    }
    
    func addShakeView() {
        if let animationView = LOTAnimationView(name: "data") {
            animationView.frame = CGRect(x: 0, y: 0, width: shakeView.frame.width/2, height: shakeView.frame.height/2)
            animationView.contentMode = .scaleAspectFill
            animationView.center = self.view.center
            
            view.addSubview(animationView)
            
            animationView.play()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.manager?.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            latitude = location.coordinate.latitude
            longitude = location.coordinate.longitude
            if (locationSet) {
                activityIndicator.startAnimating()
                loadingViewPreventUserInteraction.isHidden = false
                CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"LATITUDE\": \"\(latitude)\",\"LONGITUDE\":\"\(longitude)\",\"REQUEST_TYPE\":\"Sent\"}", mod: "Match", actionType: "onboarding-match-list", callback:{ (response: Any) -> Void in
                    if response as? String != "error" {
                        let matches = (response as! NSDictionary)["XSCData"] as! NSArray
                        print(matches)
                        self.matchList = matches
                        self.matchCollectionVIew.reloadData()
                        if self.matchList.count == 0 {
                            self.noMatchFoundLabel.isHidden = false
                        } else {
                            self.noMatchFoundLabel.isHidden = true
                        }
                    } else {
                        self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                        self.locationSet = true
                        self.manager?.requestLocation()
                    }
                    self.activityIndicator.stopAnimating()
                    self.loadingViewPreventUserInteraction.isHidden = true
                })
                self.locationSet = false
            }
        }
        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func setCollectionViewLayout(card: UIView, collectionView: UICollectionView, center: Bool) {
        
        if center {
            let screensize = card.bounds.size
            let cellwidth = floor(screensize.width * 0.8)
            let cellheight = floor(collectionView.bounds.height * 0.9)
            
            let insetX = (card.bounds.width - cellwidth) / 2.0
            let insetY = (collectionView.bounds.height - cellheight) / 2.0
            
            let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.itemSize = CGSize(width: cellwidth, height: cellheight)
            collectionView.contentInset = UIEdgeInsetsMake(insetY, insetX, insetY, insetX)
        } else {
            let screensize1 = card.bounds.size
            let cellwidth1 = floor(screensize1.width/3)
            let cellheight1 = floor(collectionView.bounds.height)
            
            let layout1 = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout1.itemSize = CGSize(width: cellwidth1, height: cellheight1)
            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }
    }
}














