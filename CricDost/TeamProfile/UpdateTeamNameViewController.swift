//
//  UpdateTeamNameViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 8/2/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class UpdateTeamNameViewController: UIViewController , ShowsAlert, UITextFieldDelegate {
    
    
    @IBOutlet weak var teamNameLabel: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var updateButton: UIButton!
    var curTeamName: String?
    @IBOutlet weak var keyboardHeightLayoutConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var EditView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        CommonUtil.roundedUIViewCornersWithShade(uiviews: [EditView])
        self.teamNameLabel.text = curTeamName
        
        // Keyboard Config
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)
    }
    
    @IBAction func cancelTNButton(_ sender: Any) {
        print("Cancel button clicked")
        self.dismiss(animated: true, completion: nil)
    }


    @IBAction func updateTNButton(_ sender: Any) {
        print("Update Button clicked")
        if !teamNameLabel.isReallyEmpty {
            UserDefaults.standard.set(teamNameLabel.text, forKey: "teamNameText")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setTeamName"), object: nil)
            self.dismiss(animated: true, completion: nil)
            }
        else {
            self.showAlert(title: "Field is Blank", message: "Please enter a name")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //Keyboard Methods
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant = 0.0
            } else {
                self.keyboardHeightLayoutConstraint?.constant = -(endFrame?.size.height ?? 0.0)/2
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    
}
