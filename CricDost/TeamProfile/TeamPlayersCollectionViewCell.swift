//
//  TeamPlayersCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 6/8/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class TeamPlayersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var messageBUtton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var skillLabel: UILabel!
    @IBOutlet weak var adminLabel: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var optionButton: UIButton!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = 3.0
        layer.shadowRadius = 3
        layer.shadowOpacity = 0.3
        layer.shadowOffset = CGSize(width: 0, height: 0)
        self.clipsToBounds = false
        
        CommonUtil.addLeftBorder(color: UIColor(red:0.80, green:0.80, blue:0.80, alpha:1.0), width: 1.0, buttons: [messageBUtton])
        optionButton.setImage(#imageLiteral(resourceName: "baseline_more_vert_white_24pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        optionButton.tintColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        CommonUtil.imageRoundedCorners(imageviews: [profileImageView])
    }
    
    
}
