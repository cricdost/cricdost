//
//  JoinTourCreateTeamsToSelectPlayersCollectionViewCell.swift
//  CricDost
//
//  Created by JIT GOEL on 8/31/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class JoinTourCreateTeamsToSelectPlayersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var joinTourSelectPlayersImageView: UIImageView!
    @IBOutlet weak var joinTourSelectPlayersTickButton: UIButton!
    @IBOutlet weak var joinTourSelectTeamNamesLabel: UILabel!
  
    override func layoutSubviews() {
        super.layoutSubviews()
        CommonUtil.imageRoundedCorners(imageviews: [joinTourSelectPlayersImageView])
        CommonUtil.buttonRoundedCorners(buttons: [joinTourSelectPlayersTickButton])
    }

}
