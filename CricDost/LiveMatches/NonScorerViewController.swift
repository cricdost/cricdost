//
//  NonScorerViewController.swift
//  CricDost
//
//  Created by Jit Goel on 6/25/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class NonScorerViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ShowsAlert {
    
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var scoreBoardView: UIView!
    @IBOutlet weak var optionButton: UIButton!
    @IBOutlet weak var teamBImageView: UIImageView!
    @IBOutlet weak var teamAImageView: UIImageView!
    @IBOutlet weak var teamAName: UILabel!
    @IBOutlet weak var teamBName: UILabel!
    @IBOutlet weak var teamAScore: UILabel!
    @IBOutlet weak var teamBScore: UILabel!
    @IBOutlet weak var matchStatus: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var ballCollectionView: UICollectionView!
    var MATCH_ID: String?
    var currentOvers: NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let outerViewTap = UITapGestureRecognizer(target: self, action: #selector(outerViewFunction))
        dismissView.addGestureRecognizer(outerViewTap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateLiveScore), name: NSNotification.Name(rawValue: "updateLiveScore"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(dismissNonScorerView), name: NSNotification.Name(rawValue: "dismissNonScorerView"), object: nil)
        
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [scoreBoardView])
        CommonUtil.imageRoundedCorners(imageviews: [teamAImageView, teamBImageView])
        optionButton.setImage(#imageLiteral(resourceName: "baseline_more_vert_white_24pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        optionButton.tintColor = UIColor.black
        
        print("Match ID", MATCH_ID)
        SocketConnectionsclass.joinMatchScoreBoard(match_id: MATCH_ID!)
        activityIndicator.startAnimating()
        
        CommonUtil.userActivityDetected(viewController: self, activityType: "VIEW LIVESCORE", status: "1")
    }
    
    @objc func dismissNonScorerView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "nonscorer_fullscore" {
            UserDefaults.standard.set(MATCH_ID, forKey: CommonUtil.CURRENTSCOREMATCHID)
        }
    }
    
    @IBAction func optionButtonClick(_ sender: Any) {
        let alert = UIAlertController(title: "Options", message: "View full score card of match", preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .popover
        alert.addAction(UIAlertAction(title: "Full Score Card", style: .default, handler: { _ in
            print("FULL SCORE")
            self.performSegue(withIdentifier: "nonscorer_fullscore", sender: self)
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            print("IPAD")
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            alert.popoverPresentationController?.permittedArrowDirections = .down
            
        default:
            break
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func updateLiveScore(_ notification: NSNotification) {
        let matchinfo = (notification.userInfo![AnyHashable("data")] as! NSArray)[0] as! NSDictionary
        print("Received Match Score Update", matchinfo)
        matchStatus.text = matchinfo["RESULT"] as? String
        teamAName.text = (matchinfo["TEAM_A"] as! NSDictionary)["TEAM_A_NAME"] as? String
        teamBName.text = (matchinfo["TEAM_B"] as! NSDictionary)["TEAM_B_NAME"] as? String
        
        UserDefaults.standard.set((matchinfo["TEAM_A"] as! NSDictionary)["TEAM_A_NAME"] as! String, forKey: CommonUtil.CURRENT_MATCH_TEAM_A_NAME)
        UserDefaults.standard.set((matchinfo["TEAM_B"] as! NSDictionary)["TEAM_B_NAME"] as! String, forKey: CommonUtil.CURRENT_MATCH_TEAM_B_NAME)
        
        teamAImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\((matchinfo["TEAM_A"] as! NSDictionary)["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
        teamBImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\((matchinfo["TEAM_B"] as! NSDictionary)["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
        teamAScore.text = (matchinfo["TEAM_A"] as! NSDictionary)["RUN_STR"] as? String
        teamBScore.text = (matchinfo["TEAM_B"] as! NSDictionary)["RUN_STR"] as? String
        currentOvers = matchinfo["CURRENT_OVER"] as! NSArray
        
        if matchinfo["STATUS_STR"] as! String == "Live" {
            ballCollectionView.isHidden = false
        } else {
            ballCollectionView.isHidden = true
        }
        
        if currentOvers.count == 0 {
            ballCollectionView.isHidden = true
        }
        ballCollectionView.reloadData()
        activityIndicator.stopAnimating()
        self.view.layoutSubviews()
        self.view.layoutIfNeeded()
        
        if matchinfo["STATUS_STR"] as! String == "Completed" {
            let resultVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WinnerCupResultViewController") as? WinnerCupResultViewController
            resultVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            resultVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            resultVC?.result = matchinfo["RESULT"] as? String
            self.present(resultVC!, animated: true, completion: nil)
        }
    }
    
    @objc func outerViewFunction(sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return currentOvers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let over = currentOvers[indexPath.section] as! NSDictionary
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ballsCell", for: indexPath) as! BallsCollectionViewCell
        cell.oversLabel.text = "\(over["OVER"] as! Double)"
        
        if over["WICKET"] as! Int == 1 {
            if over["RUNS"] as! Int > 0 {
                cell.runsButton.setTitle("\(over["RUNS"] as! Int)+w", for: .normal)
            } else {
                cell.runsButton.setTitle("w", for: .normal)
            }
        } else {
            cell.runsButton.setTitle("\(over["RUNS"] as! Int)", for: .normal)
        }
        
        var extraRuns = ""
        if over["EXTRAS_TYPE"] as! String != "" {
            
            switch over["EXTRAS_TYPE"] as! String {
            case "nb": extraRuns = "nb"
                break
            case "lb": if over["EXTRAS"] as! Int > 1 {
                    extraRuns = "lb\(over["EXTRAS"] as! Int)"
                    } else {
                        extraRuns = "lb"
                    }
                break
            case "b": if over["EXTRAS"] as! Int > 1 {
                    extraRuns = "b\(over["EXTRAS"] as! Int)"
                    }  else {
                        extraRuns = "b"
                    }
                break
            case "wd":  if (over["EXTRAS"] as! Int - 1) > 0 {
                    extraRuns = "wd + \(over["EXTRAS"] as! Int - 1)"
                    }  else {
                        extraRuns = "wd"
                    }
                break
            default:
                break
            }
        }
        cell.extraLabel.text = extraRuns
        print("extra run", extraRuns)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/6, height: collectionView.frame.height)
    }
    
}
