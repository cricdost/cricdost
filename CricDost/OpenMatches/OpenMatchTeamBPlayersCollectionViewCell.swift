//
//  OpenMatchTeamBPlayersCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 6/20/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class OpenMatchTeamBPlayersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        CommonUtil.imageRoundedCorners(imageviews: [profileImageView])
    }
}
