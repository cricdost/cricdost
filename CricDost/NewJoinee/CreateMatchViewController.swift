//
//  CreateMatchViewController.swift
//  CricDost
//
//  Created by Jit Goel on 5/30/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import CoreLocation
import LTMorphingLabel

class CreateMatchViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, ShowsAlert, UITextFieldDelegate,LTMorphingLabelDelegate{
    
    @IBOutlet weak var selectTeamCardView: UIView!
    var myTeamList: NSArray = []
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var selectTeamColectionView: UICollectionView!
    @IBOutlet weak var dateTimeCardView: UIView!
    var selectedTeam: String?
    var ballType: String?
    var numberOfPlayers: String?
    var place:String?
    var date: String?
    var time: String?
    var lat: Double?
    var lon: Double?
    @IBOutlet weak var numberofPlayerTextField: UITextField!
    @IBOutlet weak var dateTimeNextButton: UIButton!
    @IBOutlet weak var matchPlaceCardView: UIView!
    @IBOutlet weak var ballTypeCardView: UIView!
    @IBOutlet weak var numberOfPlayersCardView: UIView!
    @IBOutlet weak var matchCreatedCardView: UIView!
    @IBOutlet weak var locationNextButton: UIButton!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var locationHolderView: UIView!
    @IBOutlet weak var softTennisButton: RadioButton!
    @IBOutlet weak var hardTennisButton: RadioButton!
    @IBOutlet weak var rubberButton: RadioButton!
    @IBOutlet weak var corkButton: RadioButton!
    @IBOutlet weak var otherButton: RadioButton!
    @IBOutlet weak var ballTypeNextButton: UIButton!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    @IBOutlet weak var loadingViewPreventUserInteraction: UIView!
    @IBOutlet weak var chooseYourTeamLabel: LTMorphingLabel!
    @IBOutlet weak var createMatchesWhenLabel: LTMorphingLabel!
    @IBOutlet weak var createMatchesWhereLabel: LTMorphingLabel!
    @IBOutlet weak var createMatchesBallTypeLabel: LTMorphingLabel!
    @IBOutlet weak var createMatchesPlayersLabel: LTMorphingLabel!
    @IBOutlet weak var createMatchesImageView: UIImageView!
    @IBOutlet weak var yourMatchCreatedSuccessfullyLabel: LTMorphingLabel!
    @IBOutlet weak var inviteSentForYourTeamPlayersLabel: LTMorphingLabel!
    
    var time_picker:UIDatePicker?
    @IBOutlet weak var timeTextField: UITextField!
    
    var isOnBoardUser = true
    @IBOutlet weak var outerView: UIView!
    
    @IBOutlet weak var okButton: UIButton!
    var date_picker:UIDatePicker?
    @IBOutlet weak var dateTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        chooseYourTeamLabel.text = "..."
//        createMatchesWhenLabel.text = "..."
//        createMatchesWhereLabel.text = "..."
//        createMatchesBallTypeLabel.text = "..."
//        createMatchesPlayersLabel.text = "..."
//        yourMatchCreatedSuccessfullyLabel.text = "..."
//        inviteSentForYourTeamPlayersLabel.text = "..."
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [selectTeamCardView, dateTimeCardView, matchPlaceCardView, ballTypeCardView, numberOfPlayersCardView, matchCreatedCardView])
        CommonUtil.buttonRoundedCorners(buttons: [dateTimeNextButton, locationNextButton, ballTypeNextButton, createButton, okButton])
        self.setCollectionViewLayout(card: self.selectTeamCardView, collectionView: self.selectTeamColectionView, center: false)
        let now = Date()
        date_picker = UIDatePicker()
        date_picker?.minimumDate = now
        dateTextField.inputView = date_picker
        dateTextField.tintColor = .clear
        date_picker?.addTarget(self, action: #selector(handleDatePicker), for: UIControlEvents.valueChanged)
        date_picker?.datePickerMode = .date
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        dateTextField.text = formatter.string(from: now)
        
        time_picker = UIDatePicker()
        time_picker?.minimumDate = now.addingTimeInterval(30.0 * 60.0)
        timeTextField.inputView = time_picker
        timeTextField.tintColor = .clear
        time_picker?.addTarget(self, action: #selector(handleTimePicker), for: UIControlEvents.valueChanged)
        time_picker?.datePickerMode = .time
        
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "hh:mm a"
        timeTextField.text = formatter1.string(from: now.addingTimeInterval(30.0 * 60.0))
        
        locationHolderView.layer.cornerRadius = locationHolderView.frame.height/2
        locationHolderView.clipsToBounds = true
        
        let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
        dateTextField.inputAccessoryView = toolBar
        timeTextField.inputAccessoryView = toolBar
        
        let addressTap = UITapGestureRecognizer(target: self, action: #selector(addressTapFunction))
        locationLabel.addGestureRecognizer(addressTap)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(setLocationFunction), name: NSNotification.Name(rawValue: "setLocationFunction"), object: nil)
        
        let outerViewTap = UITapGestureRecognizer(target: self, action: #selector(outerViewFunction))
        outerView.addGestureRecognizer(outerViewTap)
        
        outerView.isHidden = isOnBoardUser
        
        getMyTeams()
        setLabelMorph(labels: [chooseYourTeamLabel,createMatchesWhereLabel,createMatchesPlayersLabel,createMatchesBallTypeLabel,createMatchesWhenLabel,yourMatchCreatedSuccessfullyLabel,inviteSentForYourTeamPlayersLabel], effect: .evaporate)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        chooseYourTeamLabel.text = "Choose your Team?"
    }
    
    func setLabelMorph(labels: [LTMorphingLabel], effect: LTMorphingEffect) {
        for label in labels {
            label.delegate = self
            label.morphingEffect = effect
            label.start()
        }
    }
    
    @objc func outerViewFunction(sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func setLocationFunction() {
        lat = UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as? Double
        lon = UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as? Double
        getAddressFromLatLon(pdblLatitude: lat!, withLongitude: lon!)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 2
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant = 0.0
            } else {
                self.keyboardHeightLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    @objc func addressTapFunction(sender:UITapGestureRecognizer) {
        UserDefaults.standard.set(true, forKey: CommonUtil.CREATEMATCHSEGUE)
        self.performSegue(withIdentifier: "createMatch_Location", sender: self)
    }
    
    @objc func dismissPicker() {
        view.endEditing(true)
    }
    
    @IBAction func softTennisBallButtonClick(_ sender: Any) {
        if (sender as! RadioButton) == softTennisButton {
            ballType = "Soft Tennis Ball"
            setRadioButtonSelected(radio: softTennisButton)
            setRadioButtonDeSelected(radioButtons: [hardTennisButton,rubberButton,corkButton,otherButton])
        }
    }
    
    @IBAction func hardTennisBallButtonClick(_ sender: Any) {
        if (sender as! RadioButton) == hardTennisButton {
            ballType = "Hard Tennis Ball"
            setRadioButtonSelected(radio: hardTennisButton)
            setRadioButtonDeSelected(radioButtons: [softTennisButton,rubberButton,corkButton,otherButton])
        } else {
            
        }
    }
    @IBAction func rubberBallButtonClick(_ sender: Any) {
        if (sender as! RadioButton) == rubberButton {
            ballType = "Rubber Ball"
            setRadioButtonSelected(radio: rubberButton)
            setRadioButtonDeSelected(radioButtons: [hardTennisButton,softTennisButton,corkButton,otherButton])
        }
    }
    @IBAction func corkBallButtonClick(_ sender: Any) {
        if (sender as! RadioButton) == corkButton {
            ballType = "Cork Ball"
            setRadioButtonSelected(radio: corkButton)
            setRadioButtonDeSelected(radioButtons: [hardTennisButton,rubberButton,softTennisButton,otherButton])
        }
    }
    @IBAction func otherBallButtonClick(_ sender: Any) {
        if (sender as! RadioButton) == otherButton {
            ballType = "Other"
            setRadioButtonSelected(radio: otherButton)
            setRadioButtonDeSelected(radioButtons: [hardTennisButton,rubberButton,corkButton,softTennisButton])
        }
    }
    
    @IBAction func dateTimeNextButtonClick(_ sender: Any) {
        view.endEditing(true)
        if dateTextField.text == "DD/MM/YY" || timeTextField.text == "00:00 am" {
            self.dateTimeCardView.shake()
            self.showAlert(title: "Invalid info", message: "Please set a date and time")
        } else {
            date = dateTextField.text
            time = timeTextField.text
            UIView.transition(with: self.matchPlaceCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.matchPlaceCardView.isHidden = false
            })
            UIView.transition(with: self.dateTimeCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.dateTimeCardView.isHidden = true
                self.createMatchesWhereLabel.text = "Where?"
            })
            
        }
        
        
    }
    
    @IBAction func ballTypeNextButtonClick(_ sender: Any) {
        if ballType != nil {
            UIView.transition(with: self.ballTypeCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.ballTypeCardView.isHidden = true
            })
            UIView.transition(with: self.numberOfPlayersCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.numberOfPlayersCardView.isHidden = false
                self.createMatchesPlayersLabel.text = "How many players match?"
            })
        } else {
            self.ballTypeCardView.shake()
            self.showAlert(title: "Invalid info", message: "Please select a ball type")
        }
        
    }
    
    func setRadioButtonSelected(radio: RadioButton) {
        radio.isSelected = true
        if radio.isSelected {
            radio.outerCircleColor = UIColor(red:0.00, green:0.56, blue:0.00, alpha:1.0)
            radio.innerCircleCircleColor = UIColor(red:0.00, green:0.56, blue:0.00, alpha:1.0)
        } else{
            radio.outerCircleColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.5)
        }
    }
    
    func setRadioButtonDeSelected(radioButtons: [RadioButton]) {
        for radio in radioButtons {
            radio.isSelected = false
            if radio.isSelected {
                radio.outerCircleColor = UIColor(red:0.00, green:0.56, blue:0.00, alpha:1.0)
                radio.innerCircleCircleColor = UIColor(red:0.00, green:0.56, blue:0.00, alpha:1.0)
            } else {
                radio.outerCircleColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.5)
            }
        }
    }
    
    @IBAction func locationNextButtonClicked(_ sender: Any) {
        if locationLabel.text != "Enter the place" {
            UIView.transition(with: self.matchPlaceCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.matchPlaceCardView.isHidden = true
            })
            UIView.transition(with: self.ballTypeCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.ballTypeCardView.isHidden = false
                self.createMatchesBallTypeLabel.text = "Ball Type?"
            })
        } else {
            self.matchPlaceCardView.shake()
            self.showAlert(title: "Location", message: "Please select the match location")
        }
       
    }
    
    @IBAction func createButtonClick(_ sender: Any) {
        if numberofPlayerTextField.isReallyEmpty {
            self.numberOfPlayersCardView.shake()
            self.showAlert(title: "Number of players", message: "Please enter number of players")
        } else {
            if Int(numberofPlayerTextField.text!)! > 2 && Int(numberofPlayerTextField.text!)! < 16  {
                numberOfPlayers = numberofPlayerTextField.text
                createMatch()
            } else {
                self.numberOfPlayersCardView.shake()
                self.showAlert(title: "Number of players", message: "Minimum of 3 players is required & maximum of 15 players is allowed")
            }
        }
        
        
    }
    
    @objc func handleDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateTextField.text = dateFormatter.string(from: (date_picker?.date)!)
    }
    
    @IBAction func okBUttonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshMapView"), object: nil)
        })
    }
    
    @objc func handleTimePicker() {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        timeTextField.text = formatter.string(from: (time_picker?.date)!)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return myTeamList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "createMatchTeamCell", for: indexPath) as! CreateMatchSelectTeamCollectionViewCell
        let team = myTeamList[indexPath.section] as! NSDictionary
        
        CommonUtil.imageRoundedCorners(imageviews: [cell.teamImageView])
        cell.teamName.text = team["TEAM_NAME"] as? String
        cell.teamImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(team["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedTeam = (myTeamList[indexPath.section] as! NSDictionary)["TEAM_ID"] as? String
        UIView.transition(with: self.selectTeamCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.selectTeamCardView.isHidden = true
        })
        UIView.transition(with: self.dateTimeCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.dateTimeCardView.isHidden = false
        })
        self.createMatchesWhenLabel.text = "When?"
    }
    
    func getMyTeams() {
        activityIndicator.startAnimating()
        loadingViewPreventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Match", actionType: "create-match-team-list") { (response: Any) in
            if response as? String != "error" {
                let teams = (response as! NSDictionary)["XSCData"] as! NSArray
                print(teams)
                self.myTeamList = teams
                if self.myTeamList.count == 0 {
                    self.selectTeamCardView.shake()
                    let alert = UIAlertController(title: "No Teams", message: "You don't own a team. Create a team now?", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Create", style: UIAlertActionStyle.default, handler: { action in
                        self.dismiss(animated: true, completion: {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "createTeamVCFunction"), object: nil)
                        })
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                if self.myTeamList.count == 1 {
                    self.setCollectionViewLayout(card: self.selectTeamCardView, collectionView: self.selectTeamColectionView, center: true)
                }
                self.selectTeamColectionView.reloadData()
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                self.getMyTeams()
            }
            self.activityIndicator.stopAnimating()
            self.loadingViewPreventUserInteraction.isHidden = true
        }
    }
    
    func setCollectionViewLayout(card: UIView, collectionView: UICollectionView, center: Bool) {
        
        if center {
            let screensize = card.bounds.size
            let cellwidth = floor(screensize.width * 0.8)
            let cellheight = floor(collectionView.bounds.height * 0.9)
            
            let insetX = (card.bounds.width - cellwidth) / 2.0
            let insetY = (collectionView.bounds.height - cellheight) / 2.0
            
            let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.itemSize = CGSize(width: cellwidth, height: cellheight)
            collectionView.contentInset = UIEdgeInsetsMake(insetY, insetX, insetY, insetX)
        } else {
            let screensize1 = card.bounds.size
            let cellwidth1 = floor(screensize1.width/3)
            let cellheight1 = floor(collectionView.bounds.height)
            
            
            let layout1 = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout1.itemSize = CGSize(width: cellwidth1, height: cellheight1)
            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
//        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude
        //21.228124
        let lon: Double = pdblLongitude
        //72.833770
        
        CommonUtil.getAddressForLatLng(viewcontroller: self, latitude: String(lat), longitude: String(lon), callback: {
            (address: String) -> Void in
            print("GEOCODING \(address)")
            self.locationLabel.text = address
        })
//        let ceo: CLGeocoder = CLGeocoder()
//        center.latitude = lat
//        center.longitude = lon
//        
//        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
//        
//        
//        ceo.reverseGeocodeLocation(loc, completionHandler:
//            {(placemarks, error) in
//                if (error != nil)
//                {
//                    print("reverse geodcode fail: \(error!.localizedDescription)")
//                    self.showAlert(message: "The network was unavailable or a network error occurred")
//                    CommonUtil.getAddressForLatLng(latitude: String(lat), longitude: String(lon), callback: {
//                        (address: String) -> Void in
//                        print("GEOCODING \(address)")
//                        self.locationLabel.text = address
//                    })
//                }
//                if let pm = placemarks {
//                    
//                    if (pm.count) > 0 {
//                        let pm = placemarks![0]
//                        var addressString : String = ""
//                        
//                        if pm.subThoroughfare != nil {
//                            addressString = addressString + pm.subThoroughfare! + ", "
//                        }
//                        
//                        if pm.thoroughfare != nil {
//                            addressString = addressString + pm.thoroughfare! + ", "
//                        }
//                        if pm.subLocality != nil {
//                            addressString = addressString + pm.subLocality! + ", "
//                        }
//                        if pm.locality != nil {
//                            addressString = addressString + pm.locality! + ", "
//                        }
//                        if pm.country != nil {
//                            addressString = addressString + pm.country! + ", "
//                        }
//                        if pm.postalCode != nil {
//                            addressString = addressString + pm.postalCode! + " "
//                        }
//                        
//                        print(addressString)
//                        self.locationLabel.text = addressString
//                    }
//                }
//        })
    }
    
    func createMatch() {
        activityIndicator.startAnimating()
        loadingViewPreventUserInteraction.isHidden = false
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let myDate = dateFormatter.date(from: date!)!
        dateFormatter.dateFormat = "yyyy/MM/dd"
        date = dateFormatter.string(from: myDate)
        
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_ID\":\"\(selectedTeam!)\",\"MATCH_DATE\":\"\(date!)\",\"MATCH_TIME\":\"\(time!)\",\"LATITUDE\":\"\(lat!)\",\"LONGITUDE\":\"\(lon!)\",\"ADDRESS\":\"\(removeSpecialCharsFromString(text: locationLabel.text!))\",\"TOTAL_PLAYERS\":\"\(numberOfPlayers!)\",\"BALL_TYPE\":\"\(ballType!)\"}", mod: "Match", actionType: "create-match") { (response) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    UIView.transition(with: self.numberOfPlayersCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.numberOfPlayersCardView.isHidden = true
                    })
                    UIView.transition(with: self.matchCreatedCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        CommonUtil.updateGATracker(screenName: "Match Created")
                        self.matchCreatedCardView.isHidden = false
                        CommonUtil.animateImage(image: self.createMatchesImageView)
                        self.yourMatchCreatedSuccessfullyLabel.text = "Your match created successfully"
                        self.inviteSentForYourTeamPlayersLabel.text = "Invite sent for your team players"
                    })
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String )
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.loadingViewPreventUserInteraction.isHidden = true
        }
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_")
        return String(text.filter {okayChars.contains($0) })
    }
}











