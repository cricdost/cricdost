//
//  LiveMatchViewController.swift
//  CricDost
//
//  Created by Jit Goel on 6/22/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class LiveMatchViewController: UIViewController, UITextFieldDelegate, ShowsAlert {

    @IBOutlet weak var tossView: UIView!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var teamAImageView: UIImageView!
    @IBOutlet weak var teamBImageView: UIImageView!
    @IBOutlet weak var teamATickButton: UIImageView!
    @IBOutlet weak var teamBTickButton: UIImageView!
    @IBOutlet weak var teamAName: UILabel!
    @IBOutlet weak var teamBName: UILabel!
    @IBOutlet weak var tossNextButton: UIButton!
    @IBOutlet weak var chooseBatBowlView: UIView!
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var tossWonTeamNameLabel: UILabel!
    @IBOutlet weak var batChooseImageView: UIImageView!
    @IBOutlet weak var bowlChooseImageView: UIImageView!
    @IBOutlet weak var batGreenTick: UIImageView!
    @IBOutlet weak var bowlGreenTick: UIImageView!
    @IBOutlet weak var chooseBatBowlNextButton: UIButton!
    @IBOutlet weak var numberOfOversView: UIView!
    @IBOutlet weak var overTextField: UITextField!
    @IBOutlet weak var oversNextButton: UIButton!
    @IBOutlet weak var coinSideOne: UIImageView!
    @IBOutlet weak var coinSideTwo: UIImageView!
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var matchInfo: Any?
    var TOSS_WON: String?
    var TOSS_LOSS: String?
    var OVERS: String?
    var MATCH_ID: String?
    var TOSS_WON_NAME: String?
    var TOSS_DECISION: String?
    var match_detials: NSDictionary?
    
    var isFromDashBoard = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [tossView,chooseBatBowlView, numberOfOversView])
        CommonUtil.imageRoundedCorners(imageviews: [teamAImageView, teamBImageView, teamATickButton, teamBTickButton, batGreenTick, bowlGreenTick])
        CommonUtil.buttonRoundedCorners(buttons: [tossNextButton,chooseBatBowlNextButton, oversNextButton])
        
        refreshButton.setImage(#imageLiteral(resourceName: "baseline_refresh_white_24pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        refreshButton.tintColor = CommonUtil.themeRed
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)

        self.addDoneButtonOnKeyboard()
        
        let outerViewTap = UITapGestureRecognizer(target: self, action: #selector(outerViewFunction))
        dismissView.addGestureRecognizer(outerViewTap)
        
        let batTap = UITapGestureRecognizer(target: self, action: #selector(batTapFunction))
        batChooseImageView.addGestureRecognizer(batTap)
        
        let bowlTap = UITapGestureRecognizer(target: self, action: #selector(bowlTapFunction))
        bowlChooseImageView.addGestureRecognizer(bowlTap)
        print("XXXXXX",matchInfo)
        if isFromDashBoard {
            match_detials = (matchInfo as! NSDictionary)["XSCData"] as? NSDictionary
        } else {
            match_detials = (matchInfo as! NSDictionary)["MATCH_DETAILS"] as? NSDictionary
            MATCH_ID = match_detials!["MATCH_ID"] as? String
        }
        
        teamAImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\((match_detials!["TEAM_A"] as! NSDictionary)["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
        teamBImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\((match_detials!["TEAM_B"] as! NSDictionary)["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
        teamAName.text = (match_detials!["TEAM_A"] as! NSDictionary)["TEAM_NAME"] as? String
        teamBName.text = (match_detials!["TEAM_B"] as! NSDictionary)["TEAM_NAME"] as? String
    }
    
    override func viewDidAppear(_ animated: Bool) {
        flipTransition(with: coinSideOne, view2: coinSideTwo)
        tossCoin()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            UIView.transition(with: self.tossView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tossView.isHidden = false
            }, completion: nil)
        })
    }
    
    @IBAction func reTossButtonClick(_ sender: Any) {
        UIView.transition(with: self.tossView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tossView.isHidden = true
        }, completion: nil)
        tossCoin()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            UIView.transition(with: self.tossView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tossView.isHidden = false
            }, completion: nil)
        })
    }
    
    @objc func batTapFunction(sender: UITapGestureRecognizer) {
        TOSS_DECISION = "BAT"
        batGreenTick.isHidden = false
        bowlGreenTick.isHidden = true
    }
    
    @objc func bowlTapFunction(sender: UITapGestureRecognizer) {
        TOSS_DECISION = "BOWL"
        batGreenTick.isHidden = true
        bowlGreenTick.isHidden = false
    }
    
    @objc func outerViewFunction(sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 2
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    @IBAction func tossNextButtonClick(_ sender: Any) {
        
        if CommonUtil.NewLiveMatchFlow {
            
        } else {
            UIView.transition(with: self.tossView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tossView.isHidden = true
            }, completion: nil)
            
            UIView.transition(with: self.chooseBatBowlView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.chooseBatBowlView.isHidden = false
            }, completion: nil)
            
            self.coinSideTwo.removeFromSuperview()
            self.coinSideOne.removeFromSuperview()
        }
    }
    
    @IBAction func chooseBatBowlNextButtonClick(_ sender: Any) {
        UIView.transition(with: self.chooseBatBowlView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.chooseBatBowlView.isHidden = true
        }, completion: nil)
        
        UIView.transition(with: self.numberOfOversView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.numberOfOversView.isHidden = false
        }, completion: nil)
    }
    
    @IBAction func overNextButtonClick(_ sender: Any) {
        
        if !overTextField.isReallyEmpty {
            if Int(overTextField.text!)! > 2 {
                OVERS = overTextField.text!
                activityIndicator.startAnimating()
                CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\":\"\(MATCH_ID!)\",\"TOSS_WON\":\"\(TOSS_WON!)\",\"TOSS_LOSS\":\"\(TOSS_LOSS!)\", \"TOSS_WON_NAME\":\"\(TOSS_WON_NAME!)\",\"OVERS\":\"\(OVERS!)\",\"TOSS_DECISION\":\"\(TOSS_DECISION!)\"}", mod: "MatchScore", actionType: "toss") { (response) in
                    if response as? String != "error" {
                        print(response)
                        let scorerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScorerViewController") as? ScorerViewController
                        scorerVC?.MATCH_ID = self.MATCH_ID
                        scorerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        scorerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        self.present(scorerVC!, animated: true, completion: nil)
                    } else {
                        self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                    }
                    self.activityIndicator.stopAnimating()
                }
            } else {
                showAlert(title: "Overs", message: "Minimum over is 3")
            }
        } else {
            showAlert(title: "Overs", message: "Please enter the number of overs")
        }
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.overTextField.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction() {
        self.overTextField.resignFirstResponder()
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant = 0.0
            } else {
                self.keyboardHeightLayoutConstraint?.constant = -(endFrame?.size.height ?? 0.0)/2
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    func flipTransition (with view1: UIImageView, view2: UIImageView, isReverse: Bool = false) {
        var transitionOptions = UIViewAnimationOptions()
        transitionOptions = isReverse ? [.transitionFlipFromLeft,.repeat] : [.transitionFlipFromRight, .repeat] // options for transition
        
        // animation durations are equal so while first will finish, second will start
        // below example could be done also using completion block.
        
        UIView.transition(with: view1, duration: 0.5, options: transitionOptions, animations: {
            view1.isHidden = true
            view2.isHidden = false
        })
        
        UIView.transition(with: view2, duration: 0.5, options: transitionOptions, animations: {
            view1.isHidden = false
            view2.isHidden = true
        })
    }
    
    func tossCoin() {
        let randomNumber = arc4random_uniform(1000) + 10
        print(randomNumber)
        if randomNumber % 2 == 0 {
            print("Team A Won toss")
            TOSS_WON = (match_detials!["TEAM_A"] as! NSDictionary)["MATCH_TEAM_ID"] as? String
            TOSS_LOSS = (match_detials!["TEAM_B"] as! NSDictionary)["MATCH_TEAM_ID"] as? String
            TOSS_WON_NAME = (match_detials!["TEAM_A"] as! NSDictionary)["TEAM_NAME"] as? String
            teamATickButton.isHidden = false
            teamBTickButton.isHidden = true
        } else {
            print("Team B Won toss")
            TOSS_LOSS = (match_detials!["TEAM_A"] as! NSDictionary)["MATCH_TEAM_ID"] as? String
            TOSS_WON = (match_detials!["TEAM_B"] as! NSDictionary)["MATCH_TEAM_ID"] as? String
            TOSS_WON_NAME = (match_detials!["TEAM_B"] as! NSDictionary)["TEAM_NAME"] as? String
            teamATickButton.isHidden = true
            teamBTickButton.isHidden = false
        }
    }
}








