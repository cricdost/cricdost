//
//  LiveScoresCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 7/12/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import SkeletonView

class LiveScoresCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var venueLabel: UILabel!
    @IBOutlet weak var matchDetails: UILabel!
    @IBOutlet weak var liveButton: UIButton!
    @IBOutlet weak var teamAImageView: UIImageView!
    @IBOutlet weak var teamBImageView: UIImageView!
    @IBOutlet weak var teamAName: UILabel!
    @IBOutlet weak var teamBName: UILabel!
    @IBOutlet weak var teamAscore: UILabel!
    @IBOutlet weak var teamBscore: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        containerView.layer.cornerRadius = 3.0
        containerView.clipsToBounds = true
        
        self.layer.cornerRadius = 3.0
        layer.shadowRadius = 3
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 1, height: 1)
        self.clipsToBounds = false
    }
}
