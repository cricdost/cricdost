//
//  CustomSlider.swift
//  CricDost
//
//  Created by Jit Goel on 6/14/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class CustomSlide: UISlider {
    
    @IBInspectable var trackHeight: CGFloat = 5
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        //set your bounds here
        return CGRect(origin: bounds.origin, size: CGSize(width: bounds.width, height: trackHeight))
    }
}
