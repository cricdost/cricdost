//
//  ScoreHeaderTableViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 6/27/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ScoreHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var C1Label: UILabel!
    @IBOutlet weak var C2Label: UILabel!
    @IBOutlet weak var C3Label: UILabel!
    @IBOutlet weak var C4Label: UILabel!
    @IBOutlet weak var C5Label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
