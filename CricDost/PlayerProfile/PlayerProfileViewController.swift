//
//  PlayerProfileViewController.swift
//  CricDost
//
//  Created by Jit Goel on 6/1/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import AnimatedCollectionViewLayout

class PlayerProfileViewController: PullUpController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ShowsAlert {
    
    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet weak var playerCollectionView: UICollectionView!
    @IBOutlet weak var teamsCollectionView: UICollectionView!
    @IBOutlet weak var matchesCollectionView: UICollectionView!
    @IBOutlet weak var icon: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var noTeamsLabel: UILabel!
    @IBOutlet weak var noMatchesLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var preventUserInteractionLoadingView: UIView!
    @IBOutlet weak var mainLoaderActivityIndicator: UIActivityIndicatorView!
    var playerList: NSArray = []
    var teamList: NSArray = []
    var matchesList: NSArray = []
    var currentMatchID: String?
    var currentPlayerID: String?
    var currentAddressID: String?
    var playerDetails: Any?
    var address: String?
    var isCalledFromTeamProfile = false
    var isCalledFromSearch = false
    var searchPlayerID = ""
    let layout = AnimatedCollectionViewLayout()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(dismissPlayerProfileFunction), name: NSNotification.Name(rawValue: "dismissPlayerProfileFunction"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openPlayerProfileFunction), name: NSNotification.Name(rawValue: "openPlayerProfileFunction"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadMacthListFunction), name: NSNotification.Name(rawValue: "reloadMacthListFunction"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshPlayerProfile), name: NSNotification.Name(rawValue: "refreshPlayerProfile"), object: nil)
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [viewHolder])
        print("SEARCHVIEW",playerDetails!)
        print(isCalledFromTeamProfile)
        
        if isCalledFromSearch {
            searchPlayerID = ((((playerDetails as! NSDictionary)["XSCData"] as! NSArray)[0]) as! NSDictionary)["USER_ID"] as? String ?? ""
        }
        
        didMoveToStickyPoint = { [weak self] point in
            if point == -100.0 {
                NotificationCenter.default.post(name: NSNotification.Name("hideShowNavBar"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name("dismissBlackScreen"), object: nil)
            }
        }
        
        layout.animator = LinearCardAttributesAnimator(minAlpha: 1.0, itemSpacing: 0.1, scaleRate: 0.95)
        layout.scrollDirection = .horizontal
        matchesCollectionView.collectionViewLayout = layout
        matchesCollectionView.isPagingEnabled = true
        
    }
    
    @objc func refreshPlayerProfile() {
        print("playerlist updated from server")
        playerDetails = UserDefaults.standard.object(forKey: "PLAYERPROFILE_\(currentAddressID!)")
        playerList = (playerDetails as! NSDictionary)["XSCData"] as! NSArray
        playerCollectionView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        CommonUtil.updateGATracker(screenName: "Player Profile")
        
        playerList = (playerDetails as! NSDictionary)["XSCData"] as! NSArray
        if (playerList.count > 0) {
            getPlayerTeamList(userId: (playerList[0] as! NSDictionary)["USER_ID"] as! String)
        }
        playerCollectionView.reloadData()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == playerCollectionView {
            return playerList.count
        } else if collectionView == teamsCollectionView {
            return teamList.count
        } else if collectionView == matchesCollectionView {
            return matchesList.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == playerCollectionView {
            let player = playerList[indexPath.section] as! NSDictionary
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "playerProfile", for: indexPath) as! PlayerProfileCollectionViewCell
            cell.nameAgeLabel.text = "\(player["FULL_NAME"] as! String), \(player["AGE"] as! String)"
            cell.addressLabel.text = self.address
            cell.ratingLabel.text = "\(player["RATTING"] as! String)"
            cell.skillsLabel.text = "\((player["SKILLS"] as! String).uppercased()) \u{2022} FOLLOWERS \(player["FOLLOWERS"] as! Int) \u{2022} FOLLOWING \(player["FOLLOWING"] as! Int)"
            cell.rankButton.setTitle("CD_RANK \(player["CD_RANK"] as! Int)", for: .normal)
            cell.profileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(player["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
            cell.matchesLabel.text = "\(player["MATCHES"] as! String)"
            cell.runsLabel.text = "\(player["RUNS"] as! String)"
            if player["HIGH_SCORE_STATUS"] as! String == "1" {
                cell.wicketsLabel.text = "\(player["HIGH_SCORE"] as! String)*"
            } else {
                cell.wicketsLabel.text = "\(player["HIGH_SCORE"] as! String)"
            }
            
            
            let runsLabelTapped = UITapGestureRecognizer(target: self, action: #selector(playercareerViewFunction))
            cell.runsLabel.addGestureRecognizer(runsLabelTapped)
            cell.runsLabel.isUserInteractionEnabled = true
            cell.runsLabel.tag = indexPath.section
            
            let runsNameLabelTapped = UITapGestureRecognizer(target: self, action: #selector(playercareerViewFunction))
            cell.runsNameLabel.addGestureRecognizer(runsNameLabelTapped)
            cell.runsNameLabel.isUserInteractionEnabled = true
            cell.runsNameLabel.tag = indexPath.section
            
            let matchesLabelTapped = UITapGestureRecognizer(target: self, action: #selector(playercareerViewFunction))
            cell.matchesLabel.addGestureRecognizer(matchesLabelTapped)
            cell.matchesLabel.isUserInteractionEnabled = true
            cell.matchesLabel.tag = indexPath.section
            
            let matchesNameLabelTapped = UITapGestureRecognizer(target: self, action: #selector(playercareerViewFunction))
            cell.matchesNameLabel.addGestureRecognizer(matchesNameLabelTapped)
            cell.matchesNameLabel.isUserInteractionEnabled = true
            cell.matchesNameLabel.tag = indexPath.section
            
            let wicketLabelTapped = UITapGestureRecognizer(target: self, action: #selector(playercareerViewFunction))
            cell.wicketsLabel.addGestureRecognizer(wicketLabelTapped)
            cell.wicketsLabel.isUserInteractionEnabled = true
            cell.wicketsLabel.tag = indexPath.section
            
            let wicketNameLabelTapped = UITapGestureRecognizer(target: self, action: #selector(playercareerViewFunction))
            cell.wicketsNameLabel.addGestureRecognizer(wicketNameLabelTapped)
            cell.wicketsNameLabel.isUserInteractionEnabled = true
            cell.wicketsNameLabel.tag = indexPath.section
            
            
            let tapped = UITapGestureRecognizer(target: self, action: #selector(ImageTappedFunction))
            cell.profileImageView.addGestureRecognizer(tapped)
            cell.profileImageView.tag = indexPath.section
            
            cell.matchesButton.setImage(#imageLiteral(resourceName: "match_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            cell.matchesButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            cell.matchesButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
            cell.requestButton.setImage(#imageLiteral(resourceName: "request_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            cell.requestButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            cell.requestButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
            
            cell.followButton.addTarget(self, action: #selector(followButtonTapped), for: UIControlEvents.touchUpInside)
            cell.followButton.tag = indexPath.section
            
            cell.teamsButton.addTarget(self, action: #selector(teamButtonTapped), for: UIControlEvents.touchUpInside)
            cell.teamsButton.tag = indexPath.section
            
            cell.matchesButton.addTarget(self, action: #selector(matchesButtonTapped), for: UIControlEvents.touchUpInside)
            cell.matchesButton.tag = indexPath.section
            
            cell.oneStar.addTarget(self, action: #selector(oneStarButtonTapped), for: UIControlEvents.touchUpInside)
            cell.oneStar.tag = indexPath.section
            
            cell.twoStar.addTarget(self, action: #selector(twoStarButtonTapped), for: UIControlEvents.touchUpInside)
            cell.twoStar.tag = indexPath.section
            
            cell.threeStar.addTarget(self, action: #selector(threeStarButtonTapped), for: UIControlEvents.touchUpInside)
            cell.threeStar.tag = indexPath.section
            
            cell.fourStar.addTarget(self, action: #selector(fourStarButtonTapped), for: UIControlEvents.touchUpInside)
            cell.fourStar.tag = indexPath.section
            
            cell.fiveStar.addTarget(self, action: #selector(fiveStarButtonTapped), for: UIControlEvents.touchUpInside)
            cell.fiveStar.tag = indexPath.section
            
            cell.optionMenuButton.addTarget(self, action: #selector(optionMenuButtonTapped), for: UIControlEvents.touchUpInside)
            cell.optionMenuButton.tag = indexPath.section
            
            cell.callButton.addTarget(self, action: #selector(callButtonTapped), for: UIControlEvents.touchUpInside)
            cell.callButton.tag = indexPath.section
            
            cell.messageButton.addTarget(self, action: #selector(messageButtonTapped), for: UIControlEvents.touchUpInside)
            cell.messageButton.tag = indexPath.section
            
            cell.requestButton.addTarget(self, action: #selector(requestButtonTapped), for: UIControlEvents.touchUpInside)
            cell.requestButton.tag = indexPath.section
            
            if player["CAN_FOLLOW"] as! Int == 1 {
                cell.followButton.setTitle("Follow", for: .normal)
            } else {
                cell.followButton.setTitle("Unfollow", for: .normal)
            }
            
            if player["CAN_RATE"] as! Int == 1 {
                cell.oneStar.isEnabled = true
                cell.twoStar.isEnabled = true
                cell.threeStar.isEnabled = true
                cell.fourStar.isEnabled = true
                cell.fiveStar.isEnabled = true
            } else {
                cell.oneStar.isEnabled = false
                cell.twoStar.isEnabled = false
                cell.threeStar.isEnabled = false
                cell.fourStar.isEnabled = false
                cell.fiveStar.isEnabled = false
                cell.setRating(rating: player["MY_RATING"] as! String)
            }
            
            return cell
        } else if collectionView == teamsCollectionView {
            let team = teamList[indexPath.section] as! NSDictionary
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "teamCell", for: indexPath) as! PlayerProfileTeamCollectionViewCell
            cell1.teamNameLabel.text = team["TEAM_NAME"] as? String
            cell1.teamAddressLabel.text = team["ADDRESS"] as? String
            cell1.teamImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(team["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            if team["IS_TEAM_ADMIN"] as! String == "true" {
                cell1.adminButton.isHidden = false
            } else {
                cell1.adminButton.isHidden = true
            }
            cell1.joinButton.addTarget(self, action: #selector(joinButtonTapped), for: UIControlEvents.touchUpInside)
            cell1.joinButton.tag = indexPath.section
            
            if (team["IS_JOINED"] as! Bool) {
                cell1.joinButton.setTitle("Exit", for: .normal)
                cell1.joinButton.backgroundColor = CommonUtil.themeRed
                if (team["OTHER_USER_IS_ADMIN"] as! Bool) {
                    cell1.joinButton.isHidden = true
                } else {
                    cell1.joinButton.isHidden = false
                }
            } else if (team["IS_REQUEST_SENT"] as! Bool) {
                cell1.joinButton.setTitle("Respond", for: .normal)
                cell1.joinButton.backgroundColor = CommonUtil.themeRed
            } else if (team["IS_REQUEST_RECEIVED"] as! Bool) {
                cell1.joinButton.setTitle("Cancel", for: .normal)
                cell1.joinButton.backgroundColor = UIColor(red:0.22, green:0.63, blue:0.81, alpha:1.0)
            } else {
                cell1.joinButton.setTitle("Join", for: .normal)
                cell1.joinButton.backgroundColor = UIColor(red:0.22, green:0.63, blue:0.81, alpha:1.0)
            }
            return cell1
        } else {
            let match = (matchesList[indexPath.section] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
            let score = (matchesList[indexPath.section] as! NSDictionary)["SCORE_DETAILS"] as! NSDictionary
            let teamA = match["TEAM_A"] as! NSDictionary
            let teamB = match["TEAM_B"] as! NSDictionary
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "matchesCell", for: indexPath) as! PlayerProfileMatchesCollectionViewCell
            cell2.dateLabel.text = match["DATE"] as? String
            cell2.timeLabel.text = match["TIME"] as? String
            cell2.teamAName.text = teamA["TEAM_NAME"] as? String
            cell2.numOfPlayers.text = "\(match["AVAILABLE_PLAYERS"] as? String ?? String(match["AVAILABLE_PLAYERS"] as! Int) )/\(match["TOTAL_PLAYERS"] as? String ?? String(match["TOTAL_PLAYERS"] as! Int)) Players"
            
            cell2.teamAImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamA["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            cell2.addressLabel.text = match["ADDRESS"] as? String
            if  teamB["POSITION_STATUS"] as! Int == 0 {
                cell2.teamBImageView.image = nil
                cell2.teamBName.text = "Challengers"
                cell2.numOfChallengers.isHidden = false
                cell2.numOfChallengers.text = match["INTEREST_COUNT"] as? String
            } else {
                cell2.numOfChallengers.isHidden = true
                cell2.teamBImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamB["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
                cell2.teamBName.text = teamB["TEAM_NAME"] as? String
            }
            if match["CAN_CHALLENGE"] as! Int == 1 {
                cell2.challengeMatchButton.isHidden = false
            } else {
                cell2.challengeMatchButton.isHidden = true
            }
            
            if match["IS_ADMIN"] as! Int == 0 {
                cell2.adminButton.isHidden = true
            } else {
                cell2.adminButton.isHidden = false
            }
            
            if match["CAN_JOIN"] as! Int == 1 {
                cell2.joinMatchButton.isHidden = false
            } else {
                cell2.joinMatchButton.isHidden = true
            }
            
            cell2.joinMatchButton.addTarget(self, action: #selector(joinMatchBUttonTapped), for: UIControlEvents.touchUpInside)
            cell2.challengeMatchButton.addTarget(self, action: #selector(challengeButtonTapped), for: UIControlEvents.touchUpInside)
            cell2.joinMatchButton.tag = indexPath.section
            cell2.challengeMatchButton.tag = indexPath.section
            
            if score["STATUS"] as! Int == 1 {
                cell2.joinMatchButton.isHidden = true
                cell2.challengeMatchButton.isHidden = true
                cell2.livescoreView.isHidden = false
                cell2.teamAScore.text = (score["TEAM_A"] as! NSDictionary)["RUN_STR"] as? String
                cell2.teamBScore.text = (score["TEAM_B"] as! NSDictionary)["RUN_STR"] as? String
                cell2.numOfPlayers.isHidden = true
                cell2.addressLabel.isHidden = true
            } else {
                cell2.livescoreView.isHidden = true
            }
            return cell2
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == playerCollectionView {
//            let player = playerList[indexPath.section] as! NSDictionary
//            let career = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PlayerCareerViewController") as? PlayerCareerViewController
//            career?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//            career?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
//            career?.currentPlayerID = player["PLAYER_ID"] as? String
//            career?.currentAddress = address
//            career?.followers = "\((player["SKILLS"] as! String).uppercased()) \u{2022} FOLLOWERS \(player["FOLLOWERS"] as! Int) \u{2022} \nFOLLOWING \(player["FOLLOWING"] as! Int)"
//            career?.playerImageURL = player["IMAGE_URL"] as? String
//            career?.cdrank = "CD_RANK \(player["CD_RANK"] as! Int)"
//            career?.playerName = "\(player["FULL_NAME"] as! String), \(player["AGE"] as! String)"
//            self.present(career!, animated: true, completion: nil)
        } else if collectionView == teamsCollectionView {
            if isCalledFromSearch {
                print("Open team from search")
                let team = teamList[indexPath.section] as! NSDictionary
                print(team)
                let encodedTeamData = NSKeyedArchiver.archivedData(withRootObject: team)
                UserDefaults.standard.set(encodedTeamData, forKey: CommonUtil.SELECTEDTEAMFROMPLAYERPROFILE)
                pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[1], completion: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getTeamProfileFromSearch"), object: nil)
            } else {
                let team = teamList[indexPath.section] as! NSDictionary
                print(team)
                let encodedTeamData = NSKeyedArchiver.archivedData(withRootObject: team)
                UserDefaults.standard.set(encodedTeamData, forKey: CommonUtil.SELECTEDTEAMFROMPLAYERPROFILE)
                pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[1], completion: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openTeamVCFromPlayerProfile"), object: nil)
            }
        } else if collectionView == matchesCollectionView {
            let score = (matchesList[indexPath.section] as! NSDictionary)["SCORE_DETAILS"] as! NSDictionary
            let match = (matchesList[indexPath.section] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
            currentMatchID = match["MATCH_ID"] as? String
            if score["STATUS"] as! Int == 1 {
                if score["IS_SCORER"] as! Int == 1 {
                    let scorerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScorerViewController") as? ScorerViewController
                    scorerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    scorerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    scorerVC?.MATCH_ID = match["MATCH_ID"] as? String
                    self.present(scorerVC!, animated: true, completion: nil)
                } else {
                    if CommonUtil.NewLiveMatchFlow {
                        print("Go to non scorer activity")
                        let non_scorerVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "TeamScorerViewController") as? TeamScorerViewController
                        non_scorerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        non_scorerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        non_scorerVC?.MATCH_ID = match["MATCH_ID"] as? String
                        self.present(non_scorerVC!, animated: true, completion: nil)
                    } else {
                        let non_scorerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NonScorerViewController") as? NonScorerViewController
                        non_scorerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        non_scorerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        non_scorerVC?.MATCH_ID = match["MATCH_ID"] as? String
                        self.present(non_scorerVC!, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    @objc func ImageTappedFunction(gesture: UITapGestureRecognizer) {
        let player = playerList[(gesture.view?.tag)!] as! NSDictionary
        if player["IMAGE_URL"] as! String != "" {
            let imageVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewProfileImageViewController") as? ViewProfileImageViewController
            imageVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            imageVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            imageVC?.imageUrl = player["IMAGE_URL"] as? String
            self.present(imageVC!, animated: true, completion: nil)
        }
    }
    
    @objc func playercareerViewFunction(gesture: UITapGestureRecognizer) {
        let player = playerList[((gesture.view?.tag)!)] as! NSDictionary
        let career = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PlayerCareerViewController") as? PlayerCareerViewController
        career?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        career?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        career?.currentPlayerID = player["PLAYER_ID"] as? String
        career?.currentAddress = address
        career?.followers = "\((player["SKILLS"] as! String).uppercased()) \u{2022} FOLLOWERS \(player["FOLLOWERS"] as! Int) \u{2022} \nFOLLOWING \(player["FOLLOWING"] as! Int)"
        career?.playerImageURL = player["IMAGE_URL"] as? String
        career?.cdrank = "CD_RANK \(player["CD_RANK"] as! Int)"
        career?.playerName = "\(player["FULL_NAME"] as! String), \(player["AGE"] as! String)"
        self.present(career!, animated: true, completion: nil)
    }
    
    
    @IBAction func callButtonTapped(_ sender: UIButton) {
        let player = playerList[sender.tag] as! NSDictionary
        print("Call")
        if (player["CAN_BLOCK"] as! Int == 1) {
            print("Call", player["MOBILE_NUMBER"] as! String)
            guard let number = URL(string: "telprompt://\(player["MOBILE_NUMBER"] as! String)") else { return }
            UIApplication.shared.open(number)
        } else {
            showAlert(title: "Privacy", message: "You are not allowed to call this person")
        }
    }
    
    @IBAction func messageButtonTapped(_ sender: UIButton) {
        let player = playerList[sender.tag] as! NSDictionary
        print("Message", player)
        let chatVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
        chatVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        chatVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        chatVC.playerUserName = player["OPENFIRE_USERNAME"] as? String
        self.present(chatVC, animated: true, completion: nil)
    }
    
    @IBAction func optionMenuButtonTapped(_ sender: UIButton) {
        let player = playerList[sender.tag] as! NSDictionary
        print("Option Menu")
        if player["CAN_BLOCK"] as! Int == 1 {
            let alert = UIAlertController(title: "Options", message: "By blocking, You wont be able to call or message these player and the player wont be able to see your profile", preferredStyle: .actionSheet)
            alert.modalPresentationStyle = .popover
            alert.addAction(UIAlertAction(title: "Block", style: .default, handler: { _ in
                self.blockUnblockPlayer(userId: player["USER_ID"] as! String, block: 1)
            }))
            
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            
            /*If you want work actionsheet on ipad
             then you have to use popoverPresentationController to present the actionsheet,
             otherwise app will crash on iPad */
            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                print("IPAD")
                alert.popoverPresentationController?.sourceView = self.view
                alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                alert.popoverPresentationController?.permittedArrowDirections = .down
                
            default:
                break
            }
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Options", message: "By unblocking, You can again call and message the player. Also the player will be able to contact you.", preferredStyle: .actionSheet)
            alert.modalPresentationStyle = .popover
            alert.addAction(UIAlertAction(title: "Unblock", style: .default, handler: { _ in
                self.blockUnblockPlayer(userId: player["USER_ID"] as! String, block: 0)
            }))
            
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            
            /*If you want work actionsheet on ipad
             then you have to use popoverPresentationController to present the actionsheet,
             otherwise app will crash on iPad */
            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                print("IPAD")
                alert.popoverPresentationController?.sourceView = self.view
                alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                alert.popoverPresentationController?.permittedArrowDirections = .down
                
            default:
                break
            }
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func oneStarButtonTapped(_ sender: UIButton) {
        let player = playerList[sender.tag] as! NSDictionary
        print("oneStar")
        let alert = UIAlertController(title: "Rate Player", message: "Rate 1 star to player. Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            self.preventUserInteractionLoadingView.isHidden = false
            self.mainLoaderActivityIndicator.startAnimating()
            self.inputRating(userId: player["USER_ID"] as! String, rating: 1)
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ action in
            print("No")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func twoStarButtonTapped(_ sender: UIButton) {
        let player = playerList[sender.tag] as! NSDictionary
        print("twoStar")
        let alert = UIAlertController(title: "Rate Player", message: "Rate 2 star to player. Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            self.preventUserInteractionLoadingView.isHidden = false
            self.mainLoaderActivityIndicator.startAnimating()
            self.inputRating(userId: player["USER_ID"] as! String, rating: 2)
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ action in
            print("No")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func threeStarButtonTapped(_ sender: UIButton) {
        let player = playerList[sender.tag] as! NSDictionary
        print("threeStar")
        let alert = UIAlertController(title: "Rate Player", message: "Rate 3 star to player. Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            self.preventUserInteractionLoadingView.isHidden = false
            self.mainLoaderActivityIndicator.startAnimating()
            self.inputRating(userId: player["USER_ID"] as! String, rating: 3)
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ action in
            print("No")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func fourStarButtonTapped(_ sender: UIButton) {
        let player = playerList[sender.tag] as! NSDictionary
        print("fourStar")
        let alert = UIAlertController(title: "Rate Player", message: "Rate 4 star to player. Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            self.preventUserInteractionLoadingView.isHidden = false
            self.mainLoaderActivityIndicator.startAnimating()
            self.inputRating(userId: player["USER_ID"] as! String, rating: 4)
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ action in
            print("No")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func fiveStarButtonTapped(_ sender: UIButton) {
        let player = playerList[sender.tag] as! NSDictionary
        print("fiveStar")
        let alert = UIAlertController(title: "Rate Player", message: "Rate 5 star to player. Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            self.preventUserInteractionLoadingView.isHidden = false
            self.mainLoaderActivityIndicator.startAnimating()
            self.inputRating(userId: player["USER_ID"] as! String, rating: 5)
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ action in
            print("No")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func joinMatchBUttonTapped(_ sender: UIButton) {
        let match = (matchesList[sender.tag] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        currentMatchID = match["MATCH_ID"] as? String
        activityIndicator.startAnimating()
        preventUserInteractionLoadingView.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\": \"\(currentMatchID!)\"}", mod: "Match", actionType: "join-match", callback: { (response: Any) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.getPlayerMatchesList(userId: self.currentPlayerID!)
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        })
    }
    
    @IBAction func challengeButtonTapped(_ sender: UIButton) {
        let match = (matchesList[sender.tag] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        currentMatchID = match["MATCH_ID"] as? String
        activityIndicator.startAnimating()
        preventUserInteractionLoadingView.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Match", actionType: "create-match-team-list") { (response: Any) in
            if response as? String != "error" {
                print(response)
                
                let teams = (response as! NSDictionary)["XSCData"] as! NSArray
                
                if teams.count == 0 {
                    let alert = UIAlertController(title: "No Teams", message: "You don't own a team. Create a team now?", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Create", style: UIAlertActionStyle.default, handler: { action in
                        self.pullUpControllerMoveToVisiblePoint(self.pullUpControllerMiddleStickyPoints[1], completion: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "createTeamVCFunction"), object: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let myTeamVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyTeamsViewController") as? MyTeamsViewController
                    myTeamVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    myTeamVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    myTeamVC?.teamList = teams
                    myTeamVC?.reloadPlayerMatch = true
                    myTeamVC?.currentMatchID = self.currentMatchID
                    self.present(myTeamVC!, animated: true, completion: nil)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
        
    }
    
    @IBAction func joinButtonTapped(_ sender: UIButton) {
        let team = teamList[sender.tag] as! NSDictionary
        
        switch sender.titleLabel?.text {
        case "Join":
            let alert = UIAlertController(title: "Join Team Request", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                self.preventUserInteractionLoadingView.isHidden = false
                self.mainLoaderActivityIndicator.startAnimating()
                self.joinTeamRequest(teamID: team["TEAM_ID"] as! String, sender)
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ action in
                print("No")
            }))
            self.present(alert, animated: true, completion: nil)
            break
        case "Cancel":
            let alert = UIAlertController(title: "Cancel Request", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                self.preventUserInteractionLoadingView.isHidden = false
                self.mainLoaderActivityIndicator.startAnimating()
                self.cancelTeamRequest(teamPlayerID: "\(team["TEAM_PLAYER_ID"] as! Int)", sender)
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ action in
                print("No")
            }))
            self.present(alert, animated: true, completion: nil)
            break
        case "Exit":
            let alert = UIAlertController(title: "Exit Team", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                self.preventUserInteractionLoadingView.isHidden = false
                self.mainLoaderActivityIndicator.startAnimating()
                self.exitTeam(teamID: team["TEAM_ID"] as! String, sender)
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ action in
                print("No")
            }))
            self.present(alert, animated: true, completion: nil)
            break
        case "Respond":
            let alert = UIAlertController(title: "Respond Team Request", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Accept", style: UIAlertActionStyle.default, handler: { action in
                self.preventUserInteractionLoadingView.isHidden = false
                self.mainLoaderActivityIndicator.startAnimating()
                self.replyTeamRequest(teamPlayerID: team["TEAM_PLAYER_ID"] as? String ?? "\(team["TEAM_PLAYER_ID"] as! Int)", sender, accept: 1)
            }))
            alert.addAction(UIAlertAction(title: "Decline", style: UIAlertActionStyle.cancel, handler:{ action in
                self.preventUserInteractionLoadingView.isHidden = false
                self.mainLoaderActivityIndicator.startAnimating()
                self.replyTeamRequest(teamPlayerID: team["TEAM_PLAYER_ID"] as? String  ?? "\(team["TEAM_PLAYER_ID"] as! Int)", sender, accept: 0)
            }))
            self.present(alert, animated: true, completion: nil)
            break
        default: print("Invalid Input")
            break
        }
    }
    
    @IBAction func requestButtonTapped(_ sender: UIButton) {
        if let collectionView = self.playerCollectionView {
            let indexPath = IndexPath(row: 0, section: sender.tag)
            let cell = collectionView.cellForItem(at: indexPath) as? PlayerProfileCollectionViewCell
            cell?.teamsButton.setImage(#imageLiteral(resourceName: "teams-24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            cell?.teamsButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            cell?.teamsButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
            cell?.matchesButton.setImage(#imageLiteral(resourceName: "match_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            cell?.matchesButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            cell?.matchesButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
            cell?.requestButton.setImage(#imageLiteral(resourceName: "request_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            cell?.requestButton.tintColor = UIColor.black
            cell?.requestButton.setTitleColor(UIColor.black, for: .normal)
            cell?.layoutSubviews()
            cell?.layoutIfNeeded()
        }
        print("Request")
        let player = playerList[sender.tag] as! NSDictionary
        currentPlayerID = player["PLAYER_ID"] as? String
        mainLoaderActivityIndicator.startAnimating()
        preventUserInteractionLoadingView.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_ID\":\"\(currentPlayerID!)\"}", mod: "Team", actionType: "player-team-list") { (response: Any) in
            if response as? String != "error" {
                print(response)
                let teams = (response as! NSDictionary)["XSCData"] as! NSArray
                if teams.count == 0 {
                    let alert = UIAlertController(title: "No Teams", message: "You don't own a team. Create a team now?", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Create", style: UIAlertActionStyle.default, handler: { action in
                            self.pullUpControllerMoveToVisiblePoint(self.pullUpControllerMiddleStickyPoints[1], completion: nil)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "createTeamVCFunction"), object: nil)
                            
                        }))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let myTeamVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyTeamsViewController") as? MyTeamsViewController
                    myTeamVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    myTeamVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    myTeamVC?.teamList = teams
                    myTeamVC?.showTeamsOnly = true
                    myTeamVC?.reloadPlayerMatch = true
                    myTeamVC?.currentPlayerID = self.currentPlayerID
                    myTeamVC?.currentMatchID = self.currentMatchID
                    self.present(myTeamVC!, animated: true, completion: nil)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.mainLoaderActivityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    @IBAction func matchesButtonTapped(_ sender: UIButton) {
        if let collectionView = self.playerCollectionView {
            let indexPath = IndexPath(row: 0, section: sender.tag)
            let cell = collectionView.cellForItem(at: indexPath) as? PlayerProfileCollectionViewCell
            cell?.matchesButton.setImage(#imageLiteral(resourceName: "match_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            cell?.matchesButton.tintColor = CommonUtil.themeRed
            cell?.matchesButton.setTitleColor(CommonUtil.themeRed, for: .normal)
            cell?.teamsButton.setImage(#imageLiteral(resourceName: "teams-24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            cell?.teamsButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            cell?.teamsButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
            cell?.requestButton.setImage(#imageLiteral(resourceName: "request_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            cell?.requestButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            cell?.requestButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
            cell?.layoutSubviews()
            cell?.layoutIfNeeded()
        }
        print("Matches Tapped")
        icon.setImage(#imageLiteral(resourceName: "match_icon_map"), for: .normal)
        titleLabel.text = "Matches"
        noTeamsLabel.isHidden = true
        self.teamsCollectionView.isHidden = true
        self.matchesCollectionView.isHidden = false
        let player = playerList[sender.tag] as! NSDictionary
        pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[2], completion: nil)
        currentPlayerID = player["USER_ID"] as? String
        getPlayerMatchesList(userId: player["USER_ID"] as! String)
    }
    
    @IBAction func teamButtonTapped(_ sender: UIButton, secondParams: Any) {
        if let collectionView = self.playerCollectionView {
            let indexPath = IndexPath(row: 0, section: sender.tag)
            let cell = collectionView.cellForItem(at: indexPath) as? PlayerProfileCollectionViewCell
            cell?.teamsButton.setImage(#imageLiteral(resourceName: "teams-24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            cell?.teamsButton.tintColor = UIColor(red:0.22, green:0.63, blue:0.81, alpha:1.0)
            cell?.teamsButton.setTitleColor(UIColor(red:0.22, green:0.63, blue:0.81, alpha:1.0), for: .normal)
            cell?.matchesButton.setImage(#imageLiteral(resourceName: "match_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            cell?.matchesButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            cell?.matchesButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
            cell?.requestButton.setImage(#imageLiteral(resourceName: "request_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
            cell?.requestButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            cell?.requestButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
            cell?.layoutSubviews()
            cell?.layoutIfNeeded()
        }
        print("teams")
        icon.setImage(#imageLiteral(resourceName: "teamIcon"), for: .normal)
        titleLabel.text = "Playing with teams"
        noMatchesLabel.isHidden = true
        self.teamsCollectionView.isHidden = false
        self.matchesCollectionView.isHidden = true
        let player = playerList[sender.tag] as! NSDictionary
        pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[2], completion: nil)
        getPlayerTeamList(userId: player["USER_ID"] as! String)
    }
    
    @IBAction func followButtonTapped(_ sender: UIButton) {
        print("follow")
        let player = playerList[sender.tag] as! NSDictionary
        let indexPath = IndexPath(row: 0, section: sender.tag)
        let cell = playerCollectionView.cellForItem(at: indexPath) as? PlayerProfileCollectionViewCell
        let fol1 : [String] = ((cell?.skillsLabel.text)?.components(separatedBy: " \u{2022} "))!
        let fol = (fol1[1]).split{$0 == " "}.map(String.init)
        print(fol1)
        print(fol)
        if sender.titleLabel?.text == "Follow" {
            followUnfollowPlayer(userId: player["USER_ID"] as! String, follow: 1, sender)
            cell?.skillsLabel.text = "\((player["SKILLS"] as! String).uppercased()) \u{2022} FOLLOWERS \(Int(fol[1])! + 1) \u{2022} FOLLOWING \(player["FOLLOWING"] as! Int)"
            cell?.followButton.setTitle("Unfollow", for: .normal)
        } else {
            followUnfollowPlayer(userId: player["USER_ID"] as! String, follow: 0, sender)
            cell?.followButton.setTitle("Follow", for: .normal)
            cell?.skillsLabel.text = "\((player["SKILLS"] as! String).uppercased()) \u{2022} FOLLOWERS \(Int(fol[1])! - 1) \u{2022} FOLLOWING \(player["FOLLOWING"] as! Int)"
        }
        cell?.layoutSubviews()
        cell?.layoutIfNeeded()
        sender.isEnabled = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print(Int(scrollView.contentOffset.x) / Int(scrollView.frame.width))
        let section = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        if scrollView == playerCollectionView {
            icon.setImage(#imageLiteral(resourceName: "teamIcon"), for: .normal)
            titleLabel.text = "Playing with teams"
            noMatchesLabel.isHidden = true
            self.teamsCollectionView.isHidden = false
            self.matchesCollectionView.isHidden = true
            let player = playerList[section] as! NSDictionary
            pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[0], completion: nil)
            getPlayerTeamList(userId: player["USER_ID"] as! String)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == playerCollectionView {
            return CGSize(width: self.playerCollectionView.frame.width, height: self.playerCollectionView.frame.height)
        } else if collectionView == matchesCollectionView {
            return CGSize(width: collectionView.bounds.width / CGFloat(1), height: collectionView.bounds.height / CGFloat(1))
//            let screensize = viewHolder.bounds.size
//
//            var cellwidth = floor(screensize.width * 0.8)
//            let cellheight = floor(matchesCollectionView.bounds.height * 0.95)
//
//            if UIDevice.current.userInterfaceIdiom == .pad {
//                cellwidth = floor(screensize.width * 0.6)
//            }
//
//            let insetX = (viewHolder.bounds.width - cellwidth) / 2.0
//            let insetY = (matchesCollectionView.bounds.height - cellheight) / 2.0
//
//            let layout = matchesCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
//            layout.itemSize = CGSize(width: cellwidth, height: cellheight)
//            matchesCollectionView.contentInset = UIEdgeInsetsMake(insetY, insetX, insetY, insetX)
//            return layout.itemSize
        } else {
            let a = teamList.count
            if a == 1 {
                return CGSize(width: self.teamsCollectionView.frame.width, height: self.teamsCollectionView.frame.height)
            } else if  a == 2 {
                return CGSize(width: self.teamsCollectionView.frame.width/2, height: self.teamsCollectionView.frame.height)
            } else {
                if UIDevice.current.userInterfaceIdiom == .pad {
                    return CGSize(width: self.teamsCollectionView.frame.width/4, height: self.teamsCollectionView.frame.height)
                } else {
                    return CGSize(width: self.teamsCollectionView.frame.width/2, height: self.teamsCollectionView.frame.height)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    @objc func dismissPlayerProfileFunction() {
        pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[1], completion: nil)
    }
    
    @objc func reloadMacthListFunction() {
        self.getPlayerMatchesList(userId: self.currentPlayerID!)
    }
    
    @objc func openPlayerProfileFunction() {
        //        pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[0], completion: nil)
    }
    
    override var pullUpControllerPreferredSize: CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 630)
    }
    
    override var pullUpControllerPreviewOffset: CGFloat {
        return pullUpControllerMiddleStickyPoints[0]
    }
    
    override var pullUpControllerMiddleStickyPoints: [CGFloat] {
        return [335,-100,630]
    }
    
    override var pullUpControllerIsBouncingEnabled: Bool {
        return true
    }
    
    override var pullUpControllerPreferredLandscapeFrame: CGRect {
        return CGRect(x: 5, y: 5, width: 280, height: UIScreen.main.bounds.height - 10)
    }
    
    func followUnfollowPlayer(userId: String, follow: Int, _ sender: UIButton) {
//        mainLoaderActivityIndicator.startAnimating()
//        preventUserInteractionLoadingView.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"PLAYER\",\"REFERENCE_ID\":\"\(userId)\",\"IS_FOLLOW\":\(follow)}", mod: "Connections", actionType: "update-following") { (response: Any) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
//                    if sender.titleLabel?.text == "Follow" {
//                        sender.setTitle("Unfollow", for: .normal)
//                    } else {
//                        sender.setTitle("Follow", for: .normal)
//                    }
                } else {
                    if sender.titleLabel?.text == "Follow" {
                        sender.setTitle("Unfollow", for: .normal)
                    } else {
                        sender.setTitle("Follow", for: .normal)
                    }
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            sender.isEnabled = true
//            self.mainLoaderActivityIndicator.stopAnimating()
//            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    func joinTeamRequest(teamID: String,_ sender: UIButton) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"REQUEST_TYPE\":\"Received\",\"TEAM_ID\":\"\(teamID)\"}", mod: "Team", actionType: "send-request") { (response: Any) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    sender.setTitle("Cancel", for: .normal)
                    sender.backgroundColor = UIColor(red:0.22, green:0.63, blue:0.81, alpha:1.0)
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.mainLoaderActivityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    func cancelTeamRequest(teamPlayerID: String,_ sender: UIButton) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_PLAYER_ID\":\"\(teamPlayerID)\"}", mod: "Team", actionType: "cancel-request") { (response: Any) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    sender.setTitle("Join", for: .normal)
                    sender.backgroundColor = UIColor(red:0.22, green:0.63, blue:0.81, alpha:1.0)
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.mainLoaderActivityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    func replyTeamRequest(teamPlayerID: String,_ sender: UIButton, accept: Int) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_PLAYER_ID\":\"\(teamPlayerID)\",\"STATUS\":\"\(accept)\",\"REQUEST_TYPE\": \"Received\"}", mod: "Team", actionType: "request-reply") { (response: Any) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    if accept == 1 {
                        sender.setTitle("Exit", for: .normal)
                        sender.backgroundColor = CommonUtil.themeRed
                    } else {
                        sender.setTitle("Join", for: .normal)
                        sender.backgroundColor = UIColor(red:0.22, green:0.63, blue:0.81, alpha:1.0)
                    }
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.mainLoaderActivityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    func exitTeam(teamID: String,_ sender: UIButton) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_ID\":\"\(teamID)\"}", mod: "Team", actionType: "exit-team") { (response: Any) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    sender.setTitle("Join", for: .normal)
                    sender.backgroundColor = UIColor(red:0.22, green:0.63, blue:0.81, alpha:1.0)
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.mainLoaderActivityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    func getPlayerTeamList(userId: String) {
        activityIndicator.startAnimating()
        self.preventUserInteractionLoadingView.isHidden = true
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"USER_ID\":\"\(userId)\"}", mod: "Player", actionType: "get-player-teams") { (response: Any) in
            print(response)
            if response as? String != "error" {
                self.teamList = (response as! NSDictionary)["XSCData"] as! NSArray
                self.teamsCollectionView.reloadData()
                if self.teamList.count == 0 {
                    if self.matchesCollectionView.isHidden {
                        self.noTeamsLabel.isHidden = false
                    }
                } else {
                    self.noTeamsLabel.isHidden = true
                }
                self.noMatchesLabel.isHidden = true
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    func getPlayerMatchesList(userId: String) {
        activityIndicator.startAnimating()
        preventUserInteractionLoadingView.isHidden = true
        
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"USER_ID\":\"\(userId)\"}", mod: "Match", actionType: "player-match-list") { (response: Any) in
            print(response)
            if response as? String != "error" {
                self.matchesList = ((response as! NSDictionary)["XSCData"] as! NSArray)
//                print("Next Page",((response as! NSDictionary)["XSCData"] as! NSDictionary)["NEXT_PAGE"])
                self.matchesCollectionView.reloadData()
                if self.matchesList.count == 0 {
                    if self.teamsCollectionView.isHidden {
                        self.noMatchesLabel.isHidden = false
                    }
                } else {
                    self.noMatchesLabel.isHidden = true
                }
                self.noTeamsLabel.isHidden = true
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    func inputRating(userId: String, rating: Int) {
        activityIndicator.startAnimating()
        preventUserInteractionLoadingView.isHidden = true
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"REFERENCE_ID\":\"\(userId)\",\"TYPE\":\"PLAYER\",\"RATING\":\"\(rating)\"}", mod: "Connections", actionType: "update-rating") { (response: Any) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.getPlayerProfile(addressID: self.currentAddressID!, address: self.address!, isCalledFromTeamProfile: self.isCalledFromTeamProfile)
                } else {
                    self.showAlert(title: "Oops!", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
    
    func getPlayerProfile(addressID: String, address: String, isCalledFromTeamProfile: Bool) {
        if isCalledFromTeamProfile {
            
            let selectedPlayer = UserDefaults.standard.object(forKey: CommonUtil.SELECTEDPLAYERFROMTEAMPROFILE) as! Data
            let decodedselectedPlayer = NSKeyedUnarchiver.unarchiveObject(with: selectedPlayer) as! NSDictionary
            currentAddressID = addressID
            activityIndicator.startAnimating()
            preventUserInteractionLoadingView.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"PLAYER\",\"USER_ID\":\"\(decodedselectedPlayer["USER_ID"] as! String)\"}", mod: "Player", actionType: "dashborad-player-profile") { (response: Any) in
                //            print(response)
                if response as? String != "error" {
                    self.playerDetails = response
                    self.playerList = (self.playerDetails as! NSDictionary)["XSCData"] as! NSArray
                    if (self.playerList.count > 0) {
                        self.getPlayerTeamList(userId: (self.playerList[0] as! NSDictionary)["USER_ID"] as! String)
                    }
                    self.playerCollectionView.reloadData()
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.mainLoaderActivityIndicator.stopAnimating()
                self.preventUserInteractionLoadingView.isHidden = true
            }
        } else {
            currentAddressID = addressID
            activityIndicator.startAnimating()
            preventUserInteractionLoadingView.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"PLAYER\",\"ADDRESS_ID\":\"\(addressID)\"}", mod: "Map", actionType: "venue-items") { (response: Any) in
                //            print(response)
                if response as? String != "error" {
                    self.playerDetails = response
                    
                    self.playerList = (self.playerDetails as! NSDictionary)["XSCData"] as! NSArray
                    if self.isCalledFromSearch {
                        let mutablePlayerList = self.playerList.mutableCopy() as! NSMutableArray
                        var x = 0
                        repeat {
                            if (mutablePlayerList[x] as! NSDictionary)["USER_ID"] as! String == self.searchPlayerID {
                                self.playerList = []
                                let tempMutableArray: NSMutableArray = []
                                tempMutableArray.add(mutablePlayerList[x] as! NSDictionary)
                                self.playerList = tempMutableArray.copy() as! NSArray
                                print("MYPLAYER",self.playerList)
                            }
                            x = x + 1
                        } while(x < mutablePlayerList.count)
                    }
                    if (self.playerList.count > 0) {
                        self.getPlayerTeamList(userId: (self.playerList[0] as! NSDictionary)["USER_ID"] as! String)
                    }
                    self.playerCollectionView.reloadData()
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.mainLoaderActivityIndicator.stopAnimating()
                self.preventUserInteractionLoadingView.isHidden = true
            }
        }
        
    }
    
    func blockUnblockPlayer(userId: String, block: Int) {
        mainLoaderActivityIndicator.startAnimating()
        preventUserInteractionLoadingView.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"IS_BLOCK\":\"\(block)\",\"REFERENCE_ID\":\"\(userId)\"}", mod: "Connections", actionType: "update-blocking") { (response: Any) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.getPlayerProfile(addressID: self.currentAddressID!, address: self.address!, isCalledFromTeamProfile: self.isCalledFromTeamProfile)
                } else {
                    self.showAlert(title: "Oops!", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.mainLoaderActivityIndicator.stopAnimating()
            self.preventUserInteractionLoadingView.isHidden = true
        }
    }
}




