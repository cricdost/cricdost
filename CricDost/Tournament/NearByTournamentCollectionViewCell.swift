//
//  NearByTournamentCollectionViewCell.swift
//  CricDost
//
//  Created by JIT GOEL on 8/20/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class NearByTournamentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var tourDate: UILabel!
    @IBOutlet weak var tourPrize: UIButton!
    @IBOutlet weak var tourImage: UIImageView!
    @IBOutlet weak var tourCall: UIButton!
    @IBOutlet weak var tourMessage: UIButton!
    @IBOutlet weak var tourName: UILabel!
    @IBOutlet weak var tourTeams: UIButton!
    @IBOutlet weak var tourOvers: UIButton!
    @IBOutlet weak var tourLocation: UILabel!
    @IBOutlet weak var joinTournament: UIButton!
    @IBOutlet weak var viewHolder: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 3.0
        layer.shadowRadius = 3
        layer.shadowOpacity = 0.3
        layer.shadowOffset = CGSize(width: 2, height: 2)
        self.clipsToBounds = false
        
        CommonUtil.buttonRoundedCorners(buttons: [joinTournament])
        CommonUtil.imageRoundedCorners(imageviews: [tourImage])
        CommonUtil.addLeftBorder(color: UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.3), width: 1, buttons: [tourOvers])
        
    }
    
}
