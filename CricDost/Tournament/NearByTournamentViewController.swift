//
//  NearByTournamentViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 8/20/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import CoreLocation
import LTMorphingLabel

class NearByTournamentViewController: UIViewController ,UICollectionViewDelegate , UICollectionViewDataSource , ShowsAlert,CLLocationManagerDelegate,LTMorphingLabelDelegate {
    
    @IBOutlet weak var dissmissViewHolder: UIView!
    @IBOutlet weak var myTournamnet: UIButton!
    @IBOutlet weak var hostTournament: UIButton!
    @IBOutlet weak var tourCollectionView: UICollectionView!
    @IBOutlet weak var viewHolderTour: UIView!
    @IBOutlet weak var myTourName: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var preventUserInteraction: UIView!
    var manager: CLLocationManager?
    var gotlocation: Bool = true
    var tournamentList: NSArray = []
    @IBOutlet weak var tournamentNearByYouLabel: LTMorphingLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tournamentNearByYouLabel.text = "Hey! Dost!"
        CommonUtil.buttonRoundedCorners(buttons: [hostTournament,myTournamnet])
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [viewHolderTour])
        CommonUtil.newButtonRoundedCorners(buttons: [myTourName])
        
        let locationGesture = UITapGestureRecognizer(target: self, action: #selector(dissmissViewHolderTapped))
        dissmissViewHolder.isUserInteractionEnabled = true
        dissmissViewHolder.addGestureRecognizer(locationGesture)
        
        myTournamnet.layer.shadowRadius = 3
        myTournamnet.layer.shadowOpacity = 0.5
        myTournamnet.clipsToBounds = false
        myTournamnet.layer.shadowOffset = CGSize(width: 0, height: 0)
        tourCollectionView.isHidden = true
        
        setLabelMorph(labels: [tournamentNearByYouLabel], effect: .fall)
        
        OperationQueue.main.addOperation{
            self.manager = CLLocationManager()
            self.manager?.delegate = self
            self.manager?.desiredAccuracy = kCLLocationAccuracyBest
            self.manager?.requestWhenInUseAuthorization()
            self.manager?.startUpdatingLocation()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        tournamentNearByYouLabel.text = "Tournament Near By You"
        tourCollectionView.isHidden = false
        setCollectionViewLayout(card: viewHolderTour, collectionView: tourCollectionView, center: true)
    }
    func setLabelMorph(labels: [LTMorphingLabel], effect: LTMorphingEffect) {
        for label in labels {
            label.delegate = self
            label.morphingEffect = effect
            label.start()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        var latitude = location.coordinate.latitude
        var longitude = location.coordinate.longitude
        if ProcessInfo.processInfo.environment["SIMULATOR_DEVICE_NAME"] != nil {
            print("Simulator")
            latitude = 13.095276
            longitude = 80.169093
        }
        
        if gotlocation {
            UserDefaults.standard.set(latitude, forKey: CommonUtil.TEMPLATITUDE)
            UserDefaults.standard.set(longitude, forKey: CommonUtil.TEMPLONGITUDE)
            getNearByTournaments(lat: latitude, lon: longitude)
            gotlocation = false
        }
        manager.stopUpdatingLocation()
    }
    
    @IBAction func myTournamentTapped(_ sender: Any) {
        
    }
    
    @IBAction func hostTournamentTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "HostTournament"), object: nil)
        })
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return tournamentList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let tournament = tournamentList[indexPath.section] as! NSDictionary
        
        let nearByTour = collectionView.dequeueReusableCell(withReuseIdentifier: "nearByTournamentCollectionViewCell", for: indexPath) as! NearByTournamentCollectionViewCell
        
        //print("Near by tournament last date entry", tournament["START_DATE"] as? String)
        var lastDateEntry = tournament["START_DATE"] as? String
        lastDateEntry = CommonUtil.convertStringDateFormater(lastDateEntry!)
    
        nearByTour.tourDate.text = lastDateEntry
        nearByTour.tourPrize.setTitle("Prize \u{20B9} \(tournament["PRIZE"] as? String ?? "0.00")", for: .normal)
        if tournament["PRIZE"] as? String ?? "0.00" == "" {
            nearByTour.tourPrize.isHidden = true
        } else if Int(tournament["PRIZE"] as? String ?? "0.00") == 0 {
            nearByTour.tourPrize.isHidden = true
        } else {
            nearByTour.tourPrize.isHidden = false
        }
        
        nearByTour.tourImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(tournament["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
        nearByTour.tourName.text = tournament["NAME"] as? String
        
        nearByTour.joinTournament.addTarget(self, action: #selector(joinTournamentButtonTapped), for: UIControlEvents.touchUpInside)
        nearByTour.joinTournament.tag = indexPath.section
        nearByTour.tourTeams.setTitle("\(tournament["NO_OF_TEAMS"] as? String ?? "0 Teams") Teams", for: UIControlState.normal)
        nearByTour.tourOvers.setTitle("\(tournament["NO_OF_OVERS"] as? String ?? "0 Overs") Overs", for: UIControlState.normal)
        nearByTour.tourLocation.text = tournament["ADDRESS"] as? String
        nearByTour.tourCall.addTarget(self, action: #selector(tournamentCallTapped), for: UIControlEvents.touchUpInside)
        nearByTour.tourCall.tag = indexPath.section
        nearByTour.tourMessage.addTarget(self, action: #selector(tournamentMessage), for: UIControlEvents.touchUpInside)
        nearByTour.tourMessage.tag = indexPath.section
        
        return nearByTour
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let tournamentDetails = tournamentList[indexPath.section] as! NSDictionary
        print("tornament details", tournamentDetails)
        let tournamentId = tournamentDetails["TOURNAMENT_ID"] as! String
        let tournamentDetailsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TournamentDetailsViewController") as! TournamentDetailsViewController
        tournamentDetailsVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        tournamentDetailsVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        tournamentDetailsVC.tournamentId = tournamentId
        tournamentDetailsVC.openFire_UserID = tournamentDetails["OPENFIRE_USERNAME"] as! String
        self.present(tournamentDetailsVC, animated: true, completion: nil)
    }
    
    func setCollectionViewLayout(card: UIView, collectionView: UICollectionView, center: Bool) {
        
        if center {
            let screensize = card.bounds.size
            let cellwidth = floor(screensize.width)
            let cellheight = floor(collectionView.bounds.height)
            
            let insetX = (card.bounds.width - cellwidth) / 1
            let insetY = (collectionView.bounds.height - cellheight) / 1
            
            let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            //            layout.sectionInset = UIEdgeInsets(top: 5, left: 7, bottom: 5, right: 7)
            layout.itemSize = CGSize(width: cellwidth, height: cellheight)
            //            layout.minimumInteritemSpacing = 10
            //            layout.minimumLineSpacing = 10
            collectionView.contentInset = UIEdgeInsetsMake(insetY, insetX, insetY, insetX)
        } else {
            let screensize1 = card.bounds.size
            let cellwidth1 = floor(screensize1.width/3)
            let cellheight1 = floor(collectionView.bounds.height)
            
            
            let layout1 = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout1.itemSize = CGSize(width: cellwidth1, height: cellheight1)
            layout1.minimumInteritemSpacing = 0
            layout1.minimumLineSpacing = 0
            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }
    }
    
    
    @objc func dissmissViewHolderTapped(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func getNearByTournaments(lat: Double, lon: Double) {
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"LATITUDE\":\"\(lat)\",\"LONGITUDE\":\"\(lon)\"}", mod: "Tournament", actionType: "get-nearby-tournaments") { (response) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    print(response)
                    self.tournamentList = (response as! NSDictionary)["XSCData"] as! NSArray
                    self.tourCollectionView.reloadData()
                } else {
                    self.showAlert(title: "Message", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        }
    }
    
    @IBAction func joinTournamentButtonTapped(_ sender: UIButton) {
        print("join tournament cliked")
        let tournament = tournamentList[sender.tag] as! NSDictionary
        
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TOURNAMENT_ID\":\"\(tournament["TOURNAMENT_ID"] as! String)\",\"NO_OF_TEAMS\":\"\(tournament["NO_OF_TEAMS"] as! String)\"}", mod: "Tournament", actionType: "tournament-verification") { (response) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    let tourDetail = (response as! NSDictionary)["XSCData"] as! NSDictionary
                    
                    if tourDetail["TEAM_EXCEED"] as! String == "true" {
                        self.viewHolderTour.shake()
                        self.showAlert(title: "Tournament is full", message: "Maximum number of teams reached")
                    } else if tourDetail["TEAM_JOINED"] as! String == "true" {
                        self.viewHolderTour.shake()
                        self.showAlert(title: "Already Joined", message: "You are already in this tournament")
                    } else {
                        let tourPinKey: [String: NSDictionary] = ["data": tourDetail, "tournament":tournament]
                        
                        self.dismiss(animated: true, completion: {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "JoinTournament"), object: nil, userInfo: tourPinKey)
                        })
                    }
                } else {
                    self.showAlert(title: "Message", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        }
    }
    
    @IBAction func tournamentCallTapped(_ sender: UIButton){
        print("Tournament Call button tapped")
        let callTapped = tournamentList[sender.tag] as! NSDictionary
        guard let number = URL(string: "telprompt://\(callTapped["CONTACT_NUMBER"] as! String)") else { return }
        UIApplication.shared.open(number)
    }
    
    @IBAction func tournamentMessage(_ sender: UIButton) {
        print("Tournament Message button Tapped")
        let meassageTapped = tournamentList[sender.tag] as! NSDictionary
        
        let chatVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
        chatVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        chatVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        chatVC.playerUserName = meassageTapped["OPENFIRE_USERNAME"] as? String
        self.present(chatVC, animated: true, completion: nil)
    }
    
}
