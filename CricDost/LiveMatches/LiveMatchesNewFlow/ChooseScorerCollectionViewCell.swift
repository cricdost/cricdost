//
//  ChooseScorerCollectionViewCell.swift
//  CricDost
//
//  Created by JIT GOEL on 9/17/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ChooseScorerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var scorerImageView: UIImageView!
    @IBOutlet weak var scorerNameLabel: UILabel!
    @IBOutlet weak var scorerTickButton: UIButton!
    @IBOutlet weak var strikerNonStrikerImageView: UIButton!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        CommonUtil.imageRoundedCorners(imageviews: [scorerImageView])
        CommonUtil.buttonRoundedCorners(buttons: [scorerTickButton])
    }
    
    }
