//
//  PlayerProfileMatchesCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 6/5/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class PlayerProfileMatchesCollectionViewCell: UICollectionViewCell {
    
   
    @IBOutlet weak var teamAImageView: UIImageView!
    @IBOutlet weak var teamBImageView: UIImageView!
    @IBOutlet weak var teamAName: UILabel!
    @IBOutlet weak var teamBName: UILabel!
    @IBOutlet weak var adminButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var joinMatchButton: UIButton!
    @IBOutlet weak var challengeMatchButton: UIButton!
    @IBOutlet weak var numOfChallengers: UILabel!
    @IBOutlet weak var numOfPlayers: UILabel!
    @IBOutlet weak var livescoreView: UIView!
    @IBOutlet weak var teamAScore: UILabel!
    @IBOutlet weak var teamBScore: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        CommonUtil.buttonRoundedCorners(buttons: [adminButton, joinMatchButton, challengeMatchButton])
        CommonUtil.imageRoundedCorners(imageviews: [teamAImageView,teamBImageView])
        
        self.containerView.layer.cornerRadius = 3.0
        self.containerView.clipsToBounds = false
        
        self.layer.cornerRadius = 3.0
        layer.shadowRadius = 3
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 0, height: 0)
        self.clipsToBounds = false
    }
}
