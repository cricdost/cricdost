//
//  ViewProfileImageViewController.swift
//  CricDost
//
//  Created by Jit Goel on 8/21/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ViewProfileImageViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    var imageUrl: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = UIColor.black
        }
//        UIApplication.shared.statusBarStyle = .lightContent
        imageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(imageUrl!)"))
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
}
