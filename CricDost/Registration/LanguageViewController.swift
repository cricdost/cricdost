//
//  LanguageViewController.swift
//  CricDost
//
//  Created by Jit Goel on 5/23/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class LanguageViewController: UIViewController {

    @IBOutlet weak var englishButton: UIButton!
    @IBOutlet weak var tamilButton: UIButton!
    @IBOutlet weak var hindiButton: UIButton!
    @IBOutlet weak var teluguButton: UIButton!
    @IBOutlet weak var kannadaButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonUtil.buttonRoundedCorners(buttons: [englishButton,tamilButton, hindiButton, teluguButton, kannadaButton])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        CommonUtil.buttonRoundedCorners(buttons: [englishButton,tamilButton, hindiButton, teluguButton, kannadaButton])
    }
    
    @IBAction func englishButtonclick(_ sender: Any) {
        UserDefaults.standard.set("English", forKey: CommonUtil.LANGUAGE)
        performSegue(withIdentifier: "language_splash", sender: self)
    }
    
    @IBAction func tamilButtonClick(_ sender: Any) {
        UserDefaults.standard.set("Tamil", forKey: CommonUtil.LANGUAGE)
        performSegue(withIdentifier: "language_splash", sender: self)
    }
    
    @IBAction func hindiButtonClick(_ sender: Any) {
        UserDefaults.standard.set("Hindi", forKey: CommonUtil.LANGUAGE)
        performSegue(withIdentifier: "language_splash", sender: self)
    }
    
    @IBAction func teluguButtonClick(_ sender: Any) {
        UserDefaults.standard.set("Telugu", forKey: CommonUtil.LANGUAGE)
        performSegue(withIdentifier: "language_splash", sender: self)
    }
    
    @IBAction func kannadaButtonClick(_ sender: Any) {
        UserDefaults.standard.set("Kannada", forKey: CommonUtil.LANGUAGE)
        performSegue(withIdentifier: "language_splash", sender: self)
    }
}








