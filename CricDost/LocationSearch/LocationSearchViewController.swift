//
//  LocationSearchViewController.swift
//  CricDost
//
//  Created by Jit Goel on 5/26/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import MapKit

class LocationSearchViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, ShowsAlert {
    
    var resultSearchController:UISearchController? = nil
    let locationManager = CLLocationManager()
    @IBOutlet weak var mapView: MKMapView!
    var selectedPin:MKPlacemark? = nil
    var searchBar: UISearchBar?
    @IBOutlet weak var chooseLocationButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        locationManager.startUpdatingLocation()
        
        CommonUtil.buttonRoundedCorners(buttons: [chooseLocationButton])
        chooseLocationButton.isEnabled = false
        let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "LocationSearchTable") as! LocationSearchTable
        resultSearchController = UISearchController(searchResultsController: locationSearchTable)
        resultSearchController?.searchResultsUpdater = locationSearchTable
        
        searchBar = resultSearchController!.searchBar
        searchBar?.sizeToFit()
        searchBar?.placeholder = "Search for places"
        let cancelButtonAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        navigationItem.titleView = resultSearchController?.searchBar
        
        self.addDoneButtonOnKeyboard()
        
        resultSearchController?.hidesNavigationBarDuringPresentation = false
        resultSearchController?.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        
        locationSearchTable.mapView = mapView
        
        locationSearchTable.handleMapSearchDelegate = self
        
        self.mapView.delegate = self
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
        done.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.blue] , for: .normal)
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.searchBar?.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.searchBar?.resignFirstResponder()
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let center = mapView.centerCoordinate
        print("AK",center)
        DispatchQueue.global(qos: .background).async {
            print("This is run on the background queue")
            self.getAddressFromLatLon(pdblLatitude: center.latitude, withLongitude: center.longitude)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            let span = MKCoordinateSpanMake(0.05, 0.05)
            let region = MKCoordinateRegion(center: location.coordinate, span: span)
            mapView.setRegion(region, animated: true)
        }
        locationManager.stopUpdatingLocation()
    }
    
    @IBAction func chooseLocationButtonClick(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: CommonUtil.USERSELECTEDLOCATION)
        
        if UserDefaults.standard.object(forKey: CommonUtil.CREATEMATCHSEGUE) as? Bool ?? false {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setLocationFunction"), object: nil)
                UserDefaults.standard.set(false, forKey: CommonUtil.CREATEMATCHSEGUE)
            }
        } else if UserDefaults.standard.object(forKey: CommonUtil.EDITPROFILESEGUE) as? Bool ?? false {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setLocationEditProfileFunction"), object: nil)
                UserDefaults.standard.set(false, forKey: CommonUtil.EDITPROFILESEGUE)
            }
        } else if UserDefaults.standard.object(forKey: CommonUtil.EDITTEAMLOCATIONACTIVATE) as? Bool ?? false {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setLocationForEditTeam"), object: nil)
                UserDefaults.standard.set(false, forKey: CommonUtil.EDITTEAMLOCATIONACTIVATE)
            }
        }else if UserDefaults.standard.object(forKey: CommonUtil.PRODUCT_USERLOCATION) as? Bool ?? false {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setLocationForUserLocation"), object: nil)
                UserDefaults.standard.set(false, forKey: CommonUtil.PRODUCT_USERLOCATION)
            }
        }else if UserDefaults.standard.object(forKey: CommonUtil.GROUNDSELECTEDLOCATION) as? Bool ?? false {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setLocationForTournamentHost"), object: nil)
                UserDefaults.standard.set(false, forKey: CommonUtil.GROUNDSELECTEDLOCATION)
            }
        } else if UserDefaults.standard.object(forKey: CommonUtil.MATCHDETAILUPDATELOCATION) as? Bool ?? false {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateMatchLocation"), object: nil)
                UserDefaults.standard.set(false, forKey: CommonUtil.MATCHDETAILUPDATELOCATION)
            }
        }
        else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
//        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude
        //21.228124
        let lon: Double = pdblLongitude
        //72.833770
        CommonUtil.getAddressForLatLng(viewcontroller: self, latitude: String(lat), longitude: String(lon), callback: {
            (address: String) -> Void in
            print("GEOCODING \(address)")
            DispatchQueue.main.async {
                UIView.transition(with: self.searchBar!, duration: 0.4, options: .curveEaseOut, animations: {
                    UserDefaults.standard.set(lat, forKey: CommonUtil.TEMPLATITUDE)
                    UserDefaults.standard.set(lon, forKey: CommonUtil.TEMPLONGITUDE)
                    self.chooseLocationButton.isEnabled = true
                    self.searchBar?.text = address
                }, completion: nil)
            }
        })
    }
    
    
}

extension LocationSearchViewController: HandleMapSearch {
    func dropPinZoomIn(placemark:MKPlacemark){
        // cache the pin
        selectedPin = placemark
        
        UserDefaults.standard.set(true, forKey: CommonUtil.USERSELECTEDLOCATION)
        UserDefaults.standard.set(placemark.coordinate.latitude, forKey: CommonUtil.TEMPLATITUDE)
        UserDefaults.standard.set(placemark.coordinate.longitude, forKey: CommonUtil.TEMPLONGITUDE)
        
        if UserDefaults.standard.object(forKey: CommonUtil.CREATEMATCHSEGUE) as? Bool ?? false {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setLocationFunction"), object: nil)
                UserDefaults.standard.set(false, forKey: CommonUtil.CREATEMATCHSEGUE)
            }
        } else if UserDefaults.standard.object(forKey: CommonUtil.EDITPROFILESEGUE) as? Bool ?? false {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setLocationEditProfileFunction"), object: nil)
                UserDefaults.standard.set(false, forKey: CommonUtil.EDITPROFILESEGUE)
            }
        } else if UserDefaults.standard.object(forKey: CommonUtil.EDITTEAMLOCATIONACTIVATE) as? Bool ?? false {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setLocationForEditTeam"), object: nil)
                UserDefaults.standard.set(false, forKey: CommonUtil.EDITTEAMLOCATIONACTIVATE)
            }
        } else if UserDefaults.standard.object(forKey: CommonUtil.PRODUCT_USERLOCATION) as? Bool ?? false {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setLocationForUserLocation"), object: nil)
                UserDefaults.standard.set(false, forKey: CommonUtil.PRODUCT_USERLOCATION)
            }
        } else if UserDefaults.standard.object(forKey: CommonUtil.GROUNDSELECTEDLOCATION) as? Bool ?? false {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setLocationForTournamentHost"), object: nil)
                UserDefaults.standard.set(false, forKey: CommonUtil.GROUNDSELECTEDLOCATION)
            }
        } else if UserDefaults.standard.object(forKey: CommonUtil.MATCHDETAILUPDATELOCATION) as? Bool ?? false {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateMatchLocation"), object: nil)
                UserDefaults.standard.set(false, forKey: CommonUtil.MATCHDETAILUPDATELOCATION)
            }
        }
        else {
                self.dismiss(animated: true, completion: nil)
        }
        
        print("CLOSING")
        
        // clear existing pins
        //        mapView.removeAnnotations(mapView.annotations)
        //        let annotation = MKPointAnnotation()
        //        annotation.coordinate = placemark.coordinate
        //        annotation.title = placemark.name
        //        if let city = placemark.locality,
        //            let state = placemark.administrativeArea {
        //            annotation.subtitle = "(city) (state)"
        //        }
        //        mapView.addAnnotation(annotation)
        //        let span = MKCoordinateSpanMake(0.05, 0.05)
        //        let region = MKCoordinateRegionMake(placemark.coordinate, span)
        //        mapView.setRegion(region, animated: true)
    }
}
