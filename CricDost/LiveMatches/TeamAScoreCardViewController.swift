//
//  TeamAScoreCardViewController.swift
//  CricDost
//
//  Created by Jit Goel on 6/27/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class TeamAScoreCardViewController: UIViewController, IndicatorInfoProvider, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var teamATableView: UITableView!
    @IBOutlet weak var runsLabel: UILabel!
    @IBOutlet weak var wicketOversLabel: UILabel!
    @IBOutlet weak var extrasLabel: UILabel!
    var batsmanList: NSArray = []
    var bowlerList: NSArray = []
    var isInternational = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(refreshTeamAScore), name: NSNotification.Name(rawValue: "refreshTeamAScore"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshTeamAScore1), name: NSNotification.Name(rawValue: "refreshTeamAScore1"), object: nil)
        
        teamATableView.estimatedRowHeight = 80.0
        teamATableView.rowHeight = UITableViewAutomaticDimension
    }
    
    @objc func refreshTeamAScore1(_ notification: NSNotification) {
        let data = (notification.userInfo![AnyHashable("teamAinfo")]) as! NSDictionary
        print("teamAinfo", data)
        DispatchQueue.main.async {
            if (data["innings1"] as! NSDictionary)["status"] as! Int != 0 {
                var score = data["innings1"] as! NSDictionary
                self.isInternational = true
                self.batsmanList = score["batsmen"] as! NSArray
                self.bowlerList = score["bowler"] as! NSArray
                self.runsLabel.text = "\(score["runs"] as! Int)"
                self.wicketOversLabel.text = "/\(score["wickets"] as! Int) \(score["over_str"] as! String)"
                self.extrasLabel.text = " \((score["extras"] as! NSDictionary)["total"] as! Int)"
                if (data["innings2"] as! NSDictionary)["status"] as! Int != 0 {
                    score = data["innings2"] as! NSDictionary
                    self.batsmanList = score["batsmen"] as! NSArray
                    self.bowlerList = score["bowler"] as! NSArray
                    self.runsLabel.text = "\(score["runs"] as! Int)"
                    self.wicketOversLabel.text = "/\(score["wickets"] as! Int) \(score["over_str"] as! String)"
                    self.extrasLabel.text = " \((score["extras"] as! NSDictionary)["total"] as! Int)"
                }
            }
            self.teamATableView.reloadData()
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
        }
    }
    
    @objc func refreshTeamAScore(_ notification: NSNotification) {
        let data = (notification.userInfo![AnyHashable("teamAinfo")]) as! NSDictionary
        print("teamAinfo", data)
        DispatchQueue.main.async {
            self.batsmanList = data["batsman"] as! NSArray
            self.bowlerList = data["bowler"] as! NSArray
            self.runsLabel.text = data["runs"] as? String
            self.wicketOversLabel.text = "/\(data["wickets"] as! String) \(data["over_str"] as! String)"
            self.extrasLabel.text = " \(data["extras"] as! String)"
            
            self.teamATableView.reloadData()
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
        }
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: (UserDefaults.standard.object(forKey: CommonUtil.CURRENT_MATCH_TEAM_A_NAME) as! String).uppercased())
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(50)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat(0)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionCell = tableView.dequeueReusableCell(withIdentifier: "scoreHeaderCell") as! ScoreHeaderTableViewCell

        if section == 0 {
            sectionCell.titleLabel.text = "Batting"
            sectionCell.C1Label.text = "R"
            sectionCell.C2Label.text = "B"
            sectionCell.C3Label.text = "4s"
            sectionCell.C4Label.text = "6s"
            sectionCell.C5Label.text = "SR"
        } else {
            sectionCell.titleLabel.text = "Bowling"
            sectionCell.C1Label.text = "O"
            sectionCell.C2Label.text = "M"
            sectionCell.C3Label.text = "R"
            sectionCell.C4Label.text = "W"
            sectionCell.C5Label.text = "Econ"
        }
        return sectionCell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return batsmanList.count
        } else {
            return bowlerList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "scoreCell", for: indexPath) as! ScoreTableViewCell
        if indexPath.section == 0 {
            let batsman = batsmanList[indexPath.row] as! NSDictionary
            if isInternational {
                cell.playerName.text = batsman["name"] as? String
                cell.C1.text = batsman["runs"] as? String ?? "\(batsman["runs"] as! Int)"
                cell.C2.text = batsman["balls"] as? String ?? "\(batsman["balls"] as! Int)"
                cell.C3.text = batsman["4s"] as? String ?? "\(batsman["4s"] as! Int)"
                cell.C4.text = batsman["6s"] as? String ?? "\(batsman["6s"] as! Int)"
                cell.C5.text = batsman["sr"] as? String ?? "\(batsman["sr"] as! Double)"
                cell.howOutLabel.text = batsman["how_out"] as? String
                cell.howOutLabel.isHidden = false
            } else {
                cell.playerName.text = batsman["FULL_NAME"] as? String
                cell.C1.text = batsman["RUNS"] as? String
                cell.C2.text = batsman["BALLS"] as? String
                cell.C3.text = batsman["4S"] as? String
                cell.C4.text = batsman["6S"] as? String
                cell.C5.text = batsman["STRIKE_RATE"] as? String
                cell.howOutLabel.text = batsman["OUT_TEXT"] as? String
                cell.howOutLabel.isHidden = false
            }
        } else {
            let bowler = bowlerList[indexPath.row] as! NSDictionary
            if isInternational  {
                cell.playerName.text = bowler["name"] as? String
                cell.C1.text = bowler["overs"] as? String ?? "\(bowler["overs"] as! Double)"
                cell.C2.text = bowler["maiden"] as? String ?? "\(bowler["maiden"] as! Int)"
                cell.C3.text = bowler["runs"] as? String ?? "\(bowler["runs"] as! Int)"
                cell.C4.text = bowler["wickets"] as? String ?? "\(bowler["wickets"] as! Int)"
                cell.C5.text = bowler["econ"] as? String ?? "\(bowler["econ"] as! Double)"
                cell.howOutLabel.isHidden = true
            } else {
                cell.playerName.text = bowler["FULL_NAME"] as? String
                cell.C1.text = bowler["OVERS"] as? String
                cell.C2.text = bowler["MAIDEN"] as? String
                cell.C3.text = bowler["RUNS"] as? String
                cell.C4.text = bowler["WICKETS"] as? String
                cell.C5.text = bowler["ECONOMY"] as? String
                cell.howOutLabel.isHidden = true
            }
        }
        return cell
    }
}
