//
//  RunnerTeamViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 9/18/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class RunnerTeamViewController: UIViewController {

    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet weak var runnerTeamNameLabel: UILabel!
    @IBOutlet weak var runnerTeamScoreLabel: UILabel!
    @IBOutlet weak var runnerClapThemButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        CommonUtil.roundedUIViewCornersWithShade(uiviews: [viewHolder])
        CommonUtil.buttonRoundedCorners(buttons: [runnerClapThemButton])
    }
    @IBAction func runnerClapThemButtonClick(_ sender: Any) {
        print("Ruuner clap them button clicked")
    }
    
    @IBAction func moreOptionButtonClick(_ sender: Any) {
        print("Runner more option button clicked")
    }
    
}
