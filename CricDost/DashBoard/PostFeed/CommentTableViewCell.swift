//
//  CommentTableViewCell.swift
//  CricDost
//
//  Created by JIT GOEL on 10/16/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressTimeLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var optionMenuButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        CommonUtil.imageRoundedCorners(imageviews: [profileImageView])
        
    }

}
