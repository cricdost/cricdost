//
//  NotificationTableViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 7/16/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var declineButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var notificationImage: UIImageView!
    @IBOutlet weak var cellView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        CommonUtil.buttonRoundedCorners(buttons: [declineButton, acceptButton])
        CommonUtil.imageRoundedCorners(imageviews: [notificationImage])
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
