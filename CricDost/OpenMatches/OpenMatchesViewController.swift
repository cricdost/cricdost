//
//  OpenMatchesViewController.swift
//  CricDost
//
//  Created by Jit Goel on 6/18/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import AnimatedCollectionViewLayout

class OpenMatchesViewController: PullUpController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ShowsAlert {
    
    var currentAddressID: String?
    var currentMatchID: String?
    var openMatchesDetails: Any?
    var address: String?
    @IBOutlet weak var numberOfMatchesButton: UIButton!
    var matchList: NSArray = []
    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet weak var matchesCollectionView: UICollectionView!
    @IBOutlet weak var preventUserInteraction: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    let layout = AnimatedCollectionViewLayout()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(dismissOpenMatchesFunction), name: NSNotification.Name(rawValue: "dismissOpenMatchesFunction"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadOpenMacthListFunction), name: NSNotification.Name(rawValue: "reloadOpenMacthListFunction"), object: nil)
        
       
        
        print(openMatchesDetails)
        matchList = (openMatchesDetails as! NSDictionary)["XSCData"] as! NSArray
        numberOfMatchesButton.setTitle("\(matchList.count) Open Matches on this location", for: .normal)
        numberOfMatchesButton.layer.cornerRadius = 5
        numberOfMatchesButton.clipsToBounds = true
        
        layout.animator = LinearCardAttributesAnimator(minAlpha: 1.0, itemSpacing: 0.18, scaleRate: 0.9)
        layout.scrollDirection = .horizontal
        matchesCollectionView.collectionViewLayout = layout
        matchesCollectionView.isPagingEnabled = true
        
        didMoveToStickyPoint = { [weak self] point in
            if point == -100.0 {
                NotificationCenter.default.post(name: NSNotification.Name("dismissBlackScreen"), object: nil)
            }
        }
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return matchList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let match = (matchList[indexPath.section] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        let teamA = match["TEAM_A"] as! NSDictionary
        let teamB = match["TEAM_B"] as! NSDictionary
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OpenMatchesCell", for: indexPath) as! OpenMatchesCollectionViewCell
        cell.dateLabel.text = match["DATE"] as? String
        cell.timeLabel.text = match["TIME"] as? String
        cell.addressLabel.text = match["ADDRESS"] as? String
        cell.numberOfPlayers.text = "\((match["AVAILABLE_PLAYERS"] as? Int ?? Int(match["AVAILABLE_PLAYERS"] as! String))!)/\((match["TOTAL_PLAYERS"] as? Int ?? Int(match["TOTAL_PLAYERS"] as! String))!) Players"
        cell.teamAName.text = teamA["TEAM_NAME"] as? String
        cell.teamAImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamA["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
        
        if teamA["IS_TEAM_PLAYER"] as! Int == 1 {
            cell.teamAPlayerImage.isHidden = false
            cell.teamAPlayerImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(UserDefaults.standard.object(forKey: CommonUtil.IMAGE_URL) as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
        } else {
            cell.teamAPlayerImage.isHidden = true
        }
        
        if teamB["IS_TEAM_PLAYER"] as! Int == 1 {
            cell.teamBPlayerImage.isHidden = false
            cell.teamBPlayerImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(UserDefaults.standard.object(forKey: CommonUtil.IMAGE_URL) as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
        } else {
            cell.teamBPlayerImage.isHidden = true
        }
        
        if  teamB["POSITION_STATUS"] as! Int == 0 {
            cell.teamBImageView.image = nil
            cell.teamBName.text = "Challengers"
            cell.challengersCount.isHidden = false
            cell.challengersCount.text = match["INTEREST_COUNT"] as? String
        } else {
            cell.challengersCount.isHidden = true
            cell.teamBImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamB["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            cell.teamBName.text = teamB["TEAM_NAME"] as? String
        }
        if match["CAN_CHALLENGE"] as! Int == 1 {
            cell.challengeBUtton.isHidden = false
        } else {
            cell.challengeBUtton.isHidden = true
        }
        
        if match["IS_ADMIN"] as! Int == 0 {
            cell.adminButton.isHidden = true
        } else {
            cell.adminButton.isHidden = false
        }
        
        if match["CAN_CHAT"] as! Int == 0 {
            cell.chatButton.isHidden = true
        } else {
            cell.chatButton.isHidden = false
        }
        
        if match["CAN_EXIT"] as! Int == 0 {
            cell.exitButton.isHidden = true
        } else {
            cell.exitButton.isHidden = false
        }
        
        if match["CAN_JOIN"] as! Int == 1 {
            cell.joinButton.isHidden = false
        } else {
            cell.joinButton.isHidden = true
        }
        
        cell.joinButton.addTarget(self, action: #selector(joinButtonTapped), for: UIControlEvents.touchUpInside)
        cell.challengeBUtton.addTarget(self, action: #selector(challengeButtonTapped), for: UIControlEvents.touchUpInside)
        cell.chatButton.addTarget(self, action: #selector(chatButtonTapped), for: UIControlEvents.touchUpInside)
        cell.exitButton.addTarget(self, action: #selector(exitButtonTapped), for: UIControlEvents.touchUpInside)
        cell.joinButton.tag = indexPath.section
        cell.challengeBUtton.tag = indexPath.section
        cell.chatButton.tag = indexPath.section
        cell.exitButton.tag = indexPath.section
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let match = (matchList[indexPath.section] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        preventUserInteraction.isHidden = false
        activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\": \"\(match["MATCH_ID"] as! String)\"}", mod: "Match", actionType: "view-match-detail-dashboard", callback: { (response: Any) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    print(response)
                    print("open match details")
                    let matchInfo:[String: NSDictionary] = ["data": (response as! NSDictionary)]
                    
                    if UserDefaults.standard.object(forKey: CommonUtil.SearchOpenMatchesDetails) as? Bool ?? false {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openMatchesDetailnSearchViewVC"), object: nil, userInfo: matchInfo)
                    } else {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openMatchesDetailVC"), object: nil, userInfo: matchInfo)
                    }

                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.preventUserInteraction.isHidden = true
            self.activityIndicator.stopAnimating()
        })
    }
    
    @IBAction func chatButtonTapped(_ sender: UIButton) {
        print("Chat")
    }
    
    @IBAction func exitButtonTapped(_ sender: UIButton) {
        let match = (matchList[sender.tag] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        currentMatchID = match["MATCH_ID"] as? String
        preventUserInteraction.isHidden = false
        activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_TEAM_PLAYER_ID\": \"\(match["MATCH_TEAM_PLAYER_ID"] as! String)\"}", mod: "Match", actionType: "exit-match-team", callback: { (response: Any) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    print(response)
                    self.getOpenMatches(addressId: self.currentAddressID!, address: self.address!)
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.preventUserInteraction.isHidden = true
            self.activityIndicator.stopAnimating()
        })
    }
    
    @IBAction func joinButtonTapped(_ sender: UIButton) {
        let match = (matchList[sender.tag] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        currentMatchID = match["MATCH_ID"] as? String
        preventUserInteraction.isHidden = false
        activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\": \"\(currentMatchID!)\"}", mod: "Match", actionType: "join-match", callback: { (response: Any) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    print(response)
                    self.getOpenMatches(addressId: self.currentAddressID!, address: self.address!)
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.preventUserInteraction.isHidden = true
            self.activityIndicator.stopAnimating()
        })
    }
    
    @IBAction func challengeButtonTapped(_ sender: UIButton) {
        let match = (matchList[sender.tag] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        currentMatchID = match["MATCH_ID"] as? String
        preventUserInteraction.isHidden = false
        activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Match", actionType: "create-match-team-list") { (response: Any) in
            if response as? String != "error" {
                print(response)
                
                let teams = (response as! NSDictionary)["XSCData"] as! NSArray
                
                if teams.count == 0 {
                    let alert = UIAlertController(title: "No Teams", message: "You don't own a team. Create a team now?", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Create", style: UIAlertActionStyle.default, handler: { action in
                        self.pullUpControllerMoveToVisiblePoint(self.pullUpControllerMiddleStickyPoints[1], completion: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "createTeamVCFunction"), object: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let myTeamVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyTeamsViewController") as? MyTeamsViewController
                    myTeamVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    myTeamVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    myTeamVC?.teamList = teams
                    myTeamVC?.currentMatchID = self.currentMatchID
                    myTeamVC?.reloadOpenMatch = true
                    self.present(myTeamVC!, animated: true, completion: nil)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.preventUserInteraction.isHidden = true
            self.activityIndicator.stopAnimating()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.width / CGFloat(1), height: collectionView.bounds.height / CGFloat(1))

//        let screensize = viewHolder.bounds.size
//        var cellwidth = floor(screensize.width * 0.8)
//        let cellheight = floor(matchesCollectionView.bounds.height * 0.95)
//
//        if UIDevice.current.userInterfaceIdiom == .pad {
//            cellwidth = floor(screensize.width * 0.6)
//        }
//
//        let insetX = (viewHolder.bounds.width - cellwidth) / 2.0
//        let insetY = (matchesCollectionView.bounds.height - cellheight) / 2.0
//
//        let layout = matchesCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
//        layout.itemSize = CGSize(width: cellwidth, height: cellheight)
//        matchesCollectionView.contentInset = UIEdgeInsetsMake(insetY, insetX, insetY, insetX)
//        return layout.itemSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        CommonUtil.updateGATracker(screenName: "Open Matches")
    }
    
    @objc func dismissOpenMatchesFunction() {
        pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[1], completion: nil)
    }
    
    @objc func reloadOpenMacthListFunction() {
        getOpenMatches(addressId: currentAddressID!, address: address!)
    }
    
    override var pullUpControllerPreferredSize: CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 300)
    }
    
    override var pullUpControllerPreviewOffset: CGFloat {
        return pullUpControllerMiddleStickyPoints[0]
    }
    
    override var pullUpControllerMiddleStickyPoints: [CGFloat] {
        return [300,-100]
    }
    
    override var pullUpControllerIsBouncingEnabled: Bool {
        return true
    }
    
    override var pullUpControllerPreferredLandscapeFrame: CGRect {
        return CGRect(x: 5, y: 5, width: 280, height: UIScreen.main.bounds.height - 10)
    }
    
    func getOpenMatches(addressId: String, address: String) {
        self.preventUserInteraction.isHidden = false
        self.activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"OPEN-MATCH\",\"ADDRESS_ID\":\"\(addressId)\"}", mod: "Map", actionType: "venue-items") { (response) in
            print(response)
            if response as? String != "error" {
                self.openMatchesDetails = response
                self.address = address
                self.currentAddressID = addressId
                self.matchList = (self.openMatchesDetails as! NSDictionary)["XSCData"] as! NSArray
                self.numberOfMatchesButton.setTitle("\(self.matchList.count) Open Matches on this location", for: .normal)
                if self.matchList.count == 0 {
                    self.pullUpControllerMoveToVisiblePoint(self.pullUpControllerMiddleStickyPoints[1], completion: nil)
                }
                self.matchesCollectionView.reloadData()
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.preventUserInteraction.isHidden = true
            self.activityIndicator.stopAnimating()
        }
    }
}
