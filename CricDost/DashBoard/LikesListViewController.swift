//
//  LikesListViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 10/29/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class LikesListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,ShowsAlert {
    
    

    @IBOutlet weak var likesListTableView: UITableView!
    @IBOutlet weak var likesListTVHeightConstraint: NSLayoutConstraint!
    
    var postID: String?
    var post: NSDictionary?
    var currentPage = 1
    var likeType: String?
    var likeDetails: NSDictionary?
    var likePlayerList: NSArray = []
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "baseline_arrow_back_white_24pt_1x.png")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "baseline_arrow_back_white_24pt_1x.png")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        print("post ===",post!)
        getPostLikeDetails()
//        self.navigationBar.topItem?.title = likeType
        self.navigationItem.title = likeType

        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("count",likePlayerList.count)
        return likePlayerList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LikesDetailsTableViewCell", for: indexPath) as! LikesDetailsTableViewCell
        let userDetail = likePlayerList[indexPath.row] as! NSDictionary
        cell.likesProfileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(userDetail["IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "profile_pic_default.png"))
        cell.likesProfileNameLabel.text = (userDetail["FULL_NAME"] as? String)?.capitalized
        
        if userDetail["FOLLOW_STATUS"] as! String == "true" {
            cell.followAndUnfollowButton.setTitle("Unfollow", for: UIControlState.normal)
            cell.followAndUnfollowButton.titleLabel?.textColor = CommonUtil.themeRed
        } else {
            cell.followAndUnfollowButton.setTitle("Follow", for: UIControlState.normal)
            cell.followAndUnfollowButton.titleLabel?.textColor = UIColor.init(red: 0.18, green: 0.76, blue: 0.44, alpha: 1.0)
        }
        
        cell.followAndUnfollowButton.addTarget(self, action: #selector(followButtonTapped), for: UIControlEvents.touchUpInside)
        cell.followAndUnfollowButton.tag = indexPath.row
        
        if let _ = userDetail["ADDRESS"] as? String {
            let addressList = (userDetail["ADDRESS"] as! String).split(separator: ",")
            if addressList.count > 3 && addressList.count < 9 {
                cell.likesAddressLabel.text = "\(addressList[addressList.endIndex - 4].trimmingCharacters(in: .whitespaces))"
            } else {
                cell.likesAddressLabel.text = "\(addressList[addressList.endIndex - 2].trimmingCharacters(in: .whitespaces))"
            }
        }
        
        return cell
    }
    
    
    @IBAction func followButtonTapped(_ sender: UIButton) {
        print("follow")
        let player = likePlayerList[sender.tag] as! NSDictionary
        if sender.titleLabel?.text == "Follow" {
            followUnfollowPlayer(userId: player["USER_ID"] as! String, follow: 1, sender)
        } else {
            followUnfollowPlayer(userId: player["USER_ID"] as! String, follow: 0, sender)
        }
        sender.isEnabled = false
    }
    
    func getPostLikeDetails() {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"LIKE_TYPE\":\"\(likeType!)\",\"CURRENT_PAGE\":\"\(currentPage)\",\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"POST_ID\":\"\(postID!)\"}", mod: "CricSpace", actionType: "get-post-players-likes-list") { (response) in
           
            print("like details",response )
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.likeDetails = (response as! NSDictionary)["XSCData"] as? NSDictionary
                    self.likePlayerList = self.likeDetails!["PLAYERS_LIST"] as! NSArray
                    print("like type details",self.likePlayerList)
                    self.likesListTableView.reloadData()
                } else {
                        print((response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops", message: "Something went wrong! Please try again")
            }
        }
    }
    
    func followUnfollowPlayer(userId: String, follow: Int, _ sender: UIButton) {
     
        
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"PLAYER\",\"REFERENCE_ID\":\"\(userId)\",\"IS_FOLLOW\":\(follow)}", mod: "Connections", actionType: "update-following") { (response: Any) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    
                    if sender.titleLabel?.text == "Follow" {
                        sender.titleLabel?.text = "Unfollow"
                        sender.titleLabel?.textColor = CommonUtil.themeRed
                    } else {
                        sender.titleLabel?.text = "Follow"
                        sender.titleLabel?.textColor = UIColor.init(red: 0.18, green: 0.76, blue: 0.44, alpha: 1.0)
                    }
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            sender.isEnabled = true
        }
    }

}
