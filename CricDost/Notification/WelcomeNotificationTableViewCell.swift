//
//  WelcomeNotificationTableViewCell.swift
//  CricDost
//
//  Created by JIT GOEL on 9/21/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class WelcomeNotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var massageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        print("hey you yeah you!!!!!")
        CommonUtil.imageRoundedCorners(imageviews: [logoImageView])
    }
}
