//
//  SelectPlayerCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 5/29/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class SelectPlayerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var playerImageView: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var tickButton: UIButton!
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        self.layer.cornerRadius = 3.0
//        layer.shadowRadius = 2
//        layer.shadowOpacity = 0.3
//        layer.shadowOffset = CGSize(width: 2, height: 2)
//        self.clipsToBounds = false
    }
    
}
