//
//  SelectedRulesTableViewCell.swift
//  CricDost
//
//  Created by JIT GOEL on 8/23/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class SelectedRulesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ruleNameLabel: UILabel!
    @IBOutlet weak var ruleTypeButton: UIButton!
    @IBOutlet weak var ruleRemoveButton: UIButton!
    @IBOutlet weak var viewholder: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ruleTypeButton.setImage(#imageLiteral(resourceName: "baseline_keyboard_arrow_down_black_24pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        ruleTypeButton.tintColor = CommonUtil.themeRed
        ruleTypeButton.setTitleColor(CommonUtil.themeRed, for: .normal)
        CommonUtil.buttonRoundedCorners(buttons: [ruleRemoveButton])
        viewholder.layer.cornerRadius = 3
        viewholder.clipsToBounds = true
    }

}
