//
//  MyProfileViewController.swift
//  CricDost
//
//  Created by Jit Goel on 6/13/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import AnimatedCollectionViewLayout

class MyProfileViewController: PullUpController, ShowsAlert, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var myProfileViewHolderDetails: UIView!
    @IBOutlet weak var mainActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var teamsCollectionView: UICollectionView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var myRatingLabel: UILabel!
    @IBOutlet weak var cdRankButton: UIButton!
    @IBOutlet weak var tabTitleLabel: UILabel!
    @IBOutlet weak var iconButton: UIButton!
    @IBOutlet weak var nameAgeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var skillsLabel: UILabel!
    @IBOutlet weak var matchesCountLabel: UILabel!
    @IBOutlet weak var runsCountLabel: UILabel!
    @IBOutlet weak var highscoreLabel: UILabel!
    @IBOutlet weak var teamButton: UIButton!
    @IBOutlet weak var matchButton: UIButton!
    @IBOutlet weak var requestButton: UIButton!
    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet weak var matchesCollectionView: UICollectionView!
    var myTeamList: NSArray = []
    var myMatchesList: NSArray = []
    var receivedList: NSArray = []
    var sentList: NSArray = []
    var myProfile: NSDictionary?
    var currentAddressID = ""
    var playerID: String?
    var imageUrl = ""
    @IBOutlet weak var noTeamLabel: UILabel!
    @IBOutlet weak var noMatchesLabel: UILabel!
    @IBOutlet weak var optionMenuButton: UIButton!
    @IBOutlet weak var requestTableView: UITableView!
    let layout = AnimatedCollectionViewLayout()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.bringSubview(toFront: myProfileViewHolderDetails)
        CommonUtil.addLeftBorder(color: UIColor(red:0.80, green:0.80, blue:0.80, alpha:1.0), width: 1.0, buttons: [matchButton, requestButton])
        CommonUtil.buttonRoundedCorners(buttons: [cdRankButton])
        CommonUtil.imageRoundedCorners(imageviews: [profileImageView])
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [viewHolder])
        optionMenuButton.setImage(#imageLiteral(resourceName: "baseline_more_vert_white_24pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        optionMenuButton.tintColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissMyProfileFunction), name: NSNotification.Name(rawValue: "dismissMyProfileFunction"), object: nil)
        matchButton.setImage(#imageLiteral(resourceName: "match_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
        matchButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        matchButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
        requestButton.setImage(#imageLiteral(resourceName: "request_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
        requestButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        requestButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
        
        let imageTap = UITapGestureRecognizer(target: self, action: #selector(imageTapFunction))
        profileImageView.addGestureRecognizer(imageTap)
        
        initialUI()
        
        layout.animator = LinearCardAttributesAnimator(minAlpha: 1.0, itemSpacing: 0.13, scaleRate: 0.93)
        layout.scrollDirection = .horizontal
        matchesCollectionView.collectionViewLayout = layout
        matchesCollectionView.isPagingEnabled = true
        
        didMoveToStickyPoint = { [weak self] point in
            if point == -100.0 {
                print("dismissing pull controller")
                NotificationCenter.default.post(name: NSNotification.Name("dismissBlackScreen"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name("hideShowNavBar"), object: nil)
            }
        }
        
    }
    
    
    @objc func imageTapFunction(sender:UITapGestureRecognizer) {
        print("Image Tapped")
//        let imageVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewProfileImageViewController") as? ViewProfileImageViewController
//        imageVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        imageVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
//        imageVC?.imageUrl = imageUrl
//        self.present(imageVC!, animated: true, completion: nil)
        
        let imageVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewPostImageViewController") as? ViewPostImageViewController
        imageVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        imageVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        imageVC?.imageUrl = imageUrl
        self.present(imageVC!, animated: true, completion: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        CommonUtil.updateGATracker(screenName: "My Profile")
        getMyProfileDetails()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(60)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat(0)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionCell = tableView.dequeueReusableCell(withIdentifier: "myProfileSection") as! MyProfileRequestSectionTableViewCell
        
        if section == 0 {
            sectionCell.sectionNameLabel.text = "\(receivedList.count) Received Requests"
//            if receivedList.count == 0 {
//                sectionCell.sectionNameLabel.text = "0 Received Requests"
//            }
        } else {
            sectionCell.sectionNameLabel.text = "\(sentList.count) Sent Requests"
//            if sentList.count == 0 {
//                sectionCell.sectionNameLabel.text = "0 Sent Requests"
//            }
        }
        return sectionCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return receivedList.count
        } else {
            return sentList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myProfileRequest", for: indexPath) as! MyProfileRequestTableViewCell
        
        CommonUtil.imageRoundedCorners(imageviews: [cell.profileImageView])
        CommonUtil.buttonRoundedCorners(buttons: [cell.declineButton, cell.acceptButton])
        
        if indexPath.section == 0 {
            let request = receivedList[indexPath.row] as! NSDictionary
            
            cell.nameLabel.text = request["REQUEST_NAME"] as? String
            cell.detailLabel.text = request["REQUEST_SUB_DATA"] as? String
            cell.profileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(request["IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "profile_pic_default"))
            if indexPath.row == receivedList.count - 1 {
                cell.lineView.isHidden = true
            } else {
                cell.lineView.isHidden = false
            }
            cell.acceptButton.isHidden = false
            cell.declineButton.setTitle("Decline", for: .normal)
            
            cell.declineButton.addTarget(self, action: #selector(declineButtonRequestTapped), for: UIControlEvents.touchUpInside)
            cell.declineButton.tag = indexPath.row

            cell.acceptButton.addTarget(self, action: #selector(acceptButtonRequestTapped), for: UIControlEvents.touchUpInside)
            cell.acceptButton.tag = indexPath.row
            
        } else {
            let request = sentList[indexPath.row] as! NSDictionary
            
            cell.nameLabel.text = request["TEAM_NAME"] as? String
            cell.detailLabel.text = request["ADDRESS"] as? String
            cell.profileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(request["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            
            if indexPath.row == sentList.count - 1 {
                cell.lineView.isHidden = true
            } else {
                cell.lineView.isHidden = false
            }
            
            cell.acceptButton.isHidden = true
            cell.declineButton.setTitle("Cancel", for: .normal)
            
            cell.declineButton.addTarget(self, action: #selector(declineButtonRequestTapped), for: UIControlEvents.touchUpInside)
            cell.declineButton.tag = indexPath.row
            
        }
        
        return cell
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == teamsCollectionView {
            return myTeamList.count
        } else {
            return myMatchesList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == teamsCollectionView {
            let team = myTeamList[indexPath.section] as! NSDictionary
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myProfileTeams", for: indexPath) as! MyProfileTeamsCollectionViewCell
            cell.teamNameButton.text = team["TEAM_NAME"] as? String
            cell.addressLabel.text = team["ADDRESS"] as? String
            cell.teamImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(team["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            
            if team["IS_TEAM_ADMIN"] as! String == "true" {
                cell.adminButton.isHidden = false
            } else {
                cell.adminButton.isHidden = true
            }
            
            if team["IS_JOINED"] as! Int == 1 {
                cell.chatButton.isHidden = false
            } else {
                cell.chatButton.isHidden = true
            }
            
            cell.chatButton.addTarget(self, action: #selector(chatButtonTapped), for: UIControlEvents.touchUpInside)
            cell.chatButton.tag = indexPath.section
            
            return cell
        } else {
            let match = (myMatchesList[indexPath.section] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
            let score = (myMatchesList[indexPath.section] as! NSDictionary)["SCORE_DETAILS"] as! NSDictionary
            let teamA = match["TEAM_A"] as! NSDictionary
            let teamB = match["TEAM_B"] as! NSDictionary
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "myProfileMatches", for: indexPath) as! MyProfileMatchesCollectionViewCell
            cell1.dateLabel.text = match["DATE"] as? String
            cell1.timeLabel.text = match["TIME"] as? String
            cell1.teamAImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamA["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            cell1.teamANameLabel.text = teamA["TEAM_NAME"] as? String
            cell1.numberOfPlayersLabel.text = "\(match["AVAILABLE_PLAYERS"] as? String ?? String(match["AVAILABLE_PLAYERS"] as! Int) )/\(match["TOTAL_PLAYERS"] as? String ?? String(match["TOTAL_PLAYERS"] as! Int)) Players"
            cell1.addressLabel.text = match["ADDRESS"] as? String
            if  teamB["POSITION_STATUS"] as! Int == 0 {
                cell1.teamBImageVIew.image = nil
                cell1.teamBNameLabel.text = "Challengers"
                cell1.challengerCountLabel.isHidden = false
                cell1.challengerCountLabel.text = match["INTEREST_COUNT"] as? String
            } else {
                cell1.challengerCountLabel.isHidden = true
                cell1.teamBImageVIew.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamB["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
                cell1.teamBNameLabel.text = teamB["TEAM_NAME"] as? String
            }
            
            if match["IS_ADMIN"] as! Int == 0 {
                cell1.adminButton.isHidden = true
            } else {
                cell1.adminButton.isHidden = false
            }
            
            if match["CAN_CHAT"] as! Int == 0 {
                cell1.chatButton.isHidden = true
            } else {
                cell1.chatButton.isHidden = false
            }
            
            if match["CAN_TOSS"] as! Int == 1 && match["IS_ADMIN"] as! Int == 1 {
                cell1.tossButton.setTitle("Go for toss", for: .normal)
                cell1.tossButton.isHidden = false
            } else if match["CAN_TOSS"] as! Int == 1 && match["IS_OTHER_ADMIN"] as! Int == 1 {
                cell1.tossButton.setTitle("Go for toss", for: .normal)
                cell1.tossButton.isHidden = false
            } else if match["CAN_TOSS"] as! Int == 1 && match["IS_ADMIN"] as! Int == 0 {
                cell1.tossButton.setTitle("Admin can toss now", for: .normal)
                cell1.tossButton.isHidden = false
            }
            cell1.tossButton.addTarget(self, action: #selector(tossButtonTapped), for: UIControlEvents.touchUpInside)
            cell1.tossButton.tag = indexPath.section
            
            if score["STATUS"] as! Int == 1 {
                cell1.chatButton.isHidden = true
                cell1.scoreView.isHidden = false
                cell1.teamAScore.text = (score["TEAM_A"] as! NSDictionary)["RUN_STR"] as? String
                cell1.teamBScore.text = (score["TEAM_B"] as! NSDictionary)["RUN_STR"] as? String
                cell1.numberOfPlayersLabel.isHidden = true
                cell1.addressLabel.isHidden = true
            } else {
                cell1.scoreView.isHidden = true
            }
            return cell1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == teamsCollectionView {
            let a = myTeamList.count
            if a == 1 {
                return CGSize(width: self.teamsCollectionView.frame.width, height: self.teamsCollectionView.frame.height)
            } else if  a == 2 {
                return CGSize(width: self.teamsCollectionView.frame.width/2, height: self.teamsCollectionView.frame.height)
            } else {
                if UIDevice.current.userInterfaceIdiom == .pad {
                    return CGSize(width: self.teamsCollectionView.frame.width/4, height: self.teamsCollectionView.frame.height)
                } else {
                    return CGSize(width: self.teamsCollectionView.frame.width/2, height: self.teamsCollectionView.frame.height)
                }
            }
        } else {
            
            return CGSize(width: collectionView.bounds.width / CGFloat(1), height: collectionView.bounds.height / CGFloat(1))
//            let screensize = matchesCollectionView.bounds.size
//            var cellwidth = floor(screensize.width * 0.8)
//            let cellheight = floor(matchesCollectionView.bounds.height * 0.95)
//
//            if UIDevice.current.userInterfaceIdiom == .pad {
//                cellwidth = floor(screensize.width * 0.6)
//            }
//
//            let insetX = (matchesCollectionView.bounds.width - cellwidth) / 2.0
//            let insetY = (matchesCollectionView.bounds.height - cellheight) / 2.0
//
//            let layout = matchesCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
//            layout.itemSize = CGSize(width: cellwidth, height: cellheight)
//            matchesCollectionView.contentInset = UIEdgeInsetsMake(insetY, insetX, insetY, insetX)
//            return layout.itemSize
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == teamsCollectionView {
            let team = myTeamList[indexPath.section] as! NSDictionary
            print(team)
            let encodedTeamData = NSKeyedArchiver.archivedData(withRootObject: team)
            UserDefaults.standard.set(encodedTeamData, forKey: CommonUtil.SELECTEDTEAMFROMPLAYERPROFILE)
            pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[1], completion: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openTeamVCFromPlayerProfile"), object: nil)
        } else if collectionView == matchesCollectionView {
            print("my profile matches")
            let score = (myMatchesList[indexPath.section] as! NSDictionary)["SCORE_DETAILS"] as! NSDictionary
            let match = (myMatchesList[indexPath.section] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
            if score["STATUS"] as! Int == 1 {
                if score["IS_SCORER"] as! Int == 1 {
                    let scorerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScorerViewController") as? ScorerViewController
                    scorerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    scorerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    scorerVC?.MATCH_ID = match["MATCH_ID"] as? String
                    self.present(scorerVC!, animated: true, completion: nil)
                } else {
                    print("Go to non scorer activity")
                    if CommonUtil.NewLiveMatchFlow {
                        let non_scorerVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "TeamScorerViewController") as? TeamScorerViewController
                        non_scorerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        non_scorerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        non_scorerVC?.MATCH_ID = match["MATCH_ID"] as? String
                        self.present(non_scorerVC!, animated: true, completion: nil)
                    } else {
                        let non_scorerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NonScorerViewController") as? NonScorerViewController
                        non_scorerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        non_scorerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        non_scorerVC?.MATCH_ID = match["MATCH_ID"] as? String
                        self.present(non_scorerVC!, animated: true, completion: nil)
                    }
                }
            } else {
                    CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\": \"\(match["MATCH_ID"] as! String)\"}", mod: "Match", actionType: "view-match-detail-dashboard", callback: { (response: Any) in
                        if response as? String != "error" {
                            if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                                print(response)
                                print("open match details")
                                let matchInfo:[String: NSDictionary] = ["data": (response as! NSDictionary)]
                                UserDefaults.standard.set(false, forKey: CommonUtil.SearchOpenMatchesDetails)
                                if UserDefaults.standard.object(forKey: CommonUtil.SearchOpenMatchesDetails) as? Bool ?? false {
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openMatchesDetailnSearchViewVC"), object: nil, userInfo: matchInfo)
                                } else {
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openMatchesDetailVC"), object: nil, userInfo: matchInfo)
                                }
                                
                            } else {
                                self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                            }
                        } else {
                            self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                        }
                    })
            }
        }
    }
    
    @IBAction func tossButtonTapped(_ sender: UIButton) {
        if sender.titleLabel?.text == "Go for toss" {
            dismissMyProfileFunction()
            SocketConnectionsclass.getMyUpcomingMatches()
//            let matchinfo:[String: Any] = ["matchInfo": myMatchesList[sender.tag]]
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowTossVCFunction"), object: nil, userInfo: matchinfo)
        } else {
            self.showAlert(title: "Toss", message: "Please request match admin to toss")
        }
    }
    
    @IBAction func chatButtonTapped(_sender: UIButton) {
        let team = myTeamList[_sender.tag] as! NSDictionary
        print("CHAT", team)
//        pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[1], completion: nil)
        let groupChatVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GroupChatViewController") as? GroupChatViewController
        groupChatVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        groupChatVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        groupChatVC?.groupJIDPrefix = team["OPENFIRE_USERNAME"] as? String
        groupChatVC?.teamName = team["TEAM_NAME"] as? String
        self.present(groupChatVC!, animated: true, completion: nil)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    @objc func dismissMyProfileFunction() {
        pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[1], completion: nil)
    }
    
    override var pullUpControllerPreferredSize: CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 550)
    }
    
    override var pullUpControllerPreviewOffset: CGFloat {
        return pullUpControllerMiddleStickyPoints[0]
    }
    
    override var pullUpControllerMiddleStickyPoints: [CGFloat] {
        return [300,-100,550]
    }
    
    override var pullUpControllerIsBouncingEnabled: Bool {
        return true
    }
    
    override var pullUpControllerPreferredLandscapeFrame: CGRect {
        return CGRect(x: 5, y: 5, width: 280, height: UIScreen.main.bounds.height - 10)
    }
    
    @IBAction func teamButtonClick(_ sender: Any) {
        teamButton.setImage(#imageLiteral(resourceName: "teams-24x24").withRenderingMode(.alwaysTemplate), for: .normal)
        teamButton.tintColor = UIColor(red:0.22, green:0.63, blue:0.81, alpha:1.0)
        teamButton.setTitleColor(UIColor(red:0.22, green:0.63, blue:0.81, alpha:1.0), for: .normal)
        matchButton.setImage(#imageLiteral(resourceName: "match_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
        matchButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        matchButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
        requestButton.setImage(#imageLiteral(resourceName: "request_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
        requestButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        requestButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
        self.view.layoutSubviews()
        self.view.layoutIfNeeded()
        
        matchesCollectionView.isHidden = true
        teamsCollectionView.isHidden = false
        noMatchesLabel.isHidden = true
        requestTableView.isHidden = true
        noTeamLabel.isHidden = false
        iconButton.setImage(#imageLiteral(resourceName: "teamIcon"), for: .normal)
        tabTitleLabel.text = "Playing with teams"
        pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[2], completion: nil)
        getMyTeams()
    }
    
    @IBAction func optionButtonClicked(_ sender: Any) {
        
        print("profile option button")
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .popover
        alert.addAction(UIAlertAction(title: "View Profile", style: .default, handler: { _ in
            
            let career = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PlayerCareerViewController") as? PlayerCareerViewController
            career?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            career?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            career?.currentPlayerID = self.playerID!
            career?.currentAddress = UserDefaults.standard.object(forKey: CommonUtil.ADDRESS) as? String
            career?.followers = self.skillsLabel.text
            career?.playerImageURL = UserDefaults.standard.object(forKey: CommonUtil.IMAGE_URL) as? String
            career?.cdrank = self.cdRankButton.titleLabel?.text
            career?.playerName = self.nameAgeLabel.text!
            self.present(career!, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Edit Profile", style: .default, handler: { _ in
            NotificationCenter.default.post(name: NSNotification.Name("dismissBlackScreen"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name("hideShowNavBar"), object: nil)
            self.pullUpControllerMoveToVisiblePoint(self.pullUpControllerMiddleStickyPoints[1], completion: nil)
            let changeNumberVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
            
            changeNumberVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            changeNumberVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(changeNumberVC, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            print("IPAD")
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            alert.popoverPresentationController?.permittedArrowDirections = .down
            
        default:
            break
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func matchButtonClick(_ sender: Any) {
        teamButton.setImage(#imageLiteral(resourceName: "teams-24x24").withRenderingMode(.alwaysTemplate), for: .normal)
        teamButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        teamButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
        matchButton.setImage(#imageLiteral(resourceName: "match_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
        matchButton.tintColor = CommonUtil.themeRed
        matchButton.setTitleColor(CommonUtil.themeRed, for: .normal)
        requestButton.setImage(#imageLiteral(resourceName: "request_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
        requestButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        requestButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
        self.view.layoutSubviews()
        self.view.layoutIfNeeded()
        teamsCollectionView.isHidden = true
        matchesCollectionView.isHidden = false
        requestTableView.isHidden = true
        noMatchesLabel.isHidden = false
        noTeamLabel.isHidden = true
        iconButton.setImage(#imageLiteral(resourceName: "match_icon_map"), for: .normal)
        tabTitleLabel.text = "Matches"
        pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[2], completion: nil)
        getMyMatches()
    }
    
    @IBAction func requestButtonClick(_ sender: Any) {
        teamButton.setImage(#imageLiteral(resourceName: "teams-24x24").withRenderingMode(.alwaysTemplate), for: .normal)
        teamButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        teamButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
        matchButton.setImage(#imageLiteral(resourceName: "match_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
        matchButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        matchButton.setTitleColor(UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5), for: .normal)
        requestButton.setImage(#imageLiteral(resourceName: "request_24x24").withRenderingMode(.alwaysTemplate), for: .normal)
        requestButton.tintColor = UIColor.black
        requestButton.setTitleColor(UIColor.black, for: .normal)
        self.view.layoutSubviews()
        self.view.layoutIfNeeded()
        teamsCollectionView.isHidden = true
        matchesCollectionView.isHidden = true
        requestTableView.isHidden = false
        noMatchesLabel.isHidden = true
        noTeamLabel.isHidden = true
        pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[2], completion: nil)
        fetchMyRequests()
    }
    
    func getMyProfileDetails() {
        mainActivityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Player", actionType: "my-profile-details") { (response) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.refreshUI(profile: (response as! NSDictionary)["XSCData"] as! NSDictionary)
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.mainActivityIndicator.stopAnimating()
        }
    }
    
    func refreshUI(profile: NSDictionary) {
        print("Refreshing")
        profileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(profile["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
        imageUrl = profile["IMAGE_URL"] as! String
        myRatingLabel.text = profile["RATTING"] as? String
        cdRankButton.setTitle("CD RANK: \(profile["CD_RANK"] as! Int)", for: .normal)
        nameAgeLabel.text = "\(profile["FULL_NAME"] as! String), \(profile["AGE"] as! String)"
        addressLabel.text = profile["ADDRESS"] as? String
        runsCountLabel.text = profile["RUNS"] as? String
        matchesCountLabel.text = profile["MATCHES"] as? String
        highscoreLabel.text = profile["HIGH_SCORE"] as? String
        playerID = profile["PLAYER_ID"] as? String
        if profile["HIGH_SCORE_STATUS"] as! String == "1" {
            highscoreLabel.text = "\(profile["HIGH_SCORE"] as! String)*"
        }
        skillsLabel.text = "\((profile["SKILLS"] as! String).uppercased()) \u{2022} FOLLOWERS \(profile["FOLLOWERS"] as! Int) \u{2022} \nFOLLOWING \(profile["FOLLOWING"] as! Int)"
        optionMenuButton.isEnabled = true
        getMyTeams()
    }
    
    func initialUI() {
        myRatingLabel.text = "0.0"
        cdRankButton.setTitle("...", for: .normal)
        nameAgeLabel.text = "..."
        addressLabel.text = "..."
        runsCountLabel.text = "0"
        matchesCountLabel.text = "0"
        highscoreLabel.text = "0"
        skillsLabel.text = "FOLLOWERS 0 \u{2022} FOLLOWING 0"
    }
    
    func getMyTeams() {
        tableActivityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"USER_ID\":\"\(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String)\"}", mod: "Player", actionType: "get-player-teams") { (response) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.myTeamList = (response as! NSDictionary)["XSCData"] as! NSArray
                    self.teamsCollectionView.reloadData()
                    if !self.teamsCollectionView.isHidden {
                        if self.myTeamList.count == 0 {
                            self.noTeamLabel.isHidden = false
                        } else {
                            self.noTeamLabel.isHidden = true
                        }
                    }
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.tableActivityIndicator.stopAnimating()
        }
    }
    
    func getMyMatches() {
        tableActivityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"USER_ID\":\"\(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String)\"}", mod: "Match", actionType: "player-match-list") { (response) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.myMatchesList = (response as! NSDictionary)["XSCData"] as! NSArray
                    self.matchesCollectionView.reloadData()
                    if self.myMatchesList.count == 0 {
                            self.noMatchesLabel.isHidden = false
                    } else {
                            self.noMatchesLabel.isHidden = true
                    }
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.tableActivityIndicator.stopAnimating()
        }
    }
    
    func fetchMyRequests() {
        tableActivityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Player", actionType: "my-requests") { (response) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    let requests = (response as! NSDictionary)["XSCData"] as! NSDictionary
                    
                    self.receivedList = requests["RECEIVED"] as! NSArray
                    self.sentList = requests["SENT"] as! NSArray
                    
                    self.requestTableView.reloadData()
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.tableActivityIndicator.stopAnimating()
        }
    }
    
    @IBAction func declineButtonRequestTapped(_ sender: UIButton) {
        
        if sender.titleLabel?.text == "Decline" {
            let request = receivedList[sender.tag] as! NSDictionary
            print("Decline")
            let alert = UIAlertController(title: "Decline Request", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Decline", style: UIAlertActionStyle.default, handler: { action in
                self.replyToTeamRequest(requestSupportId: request["REQUEST_SUPPORT_ID"] as! String, accept: 0)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:{ action in
                
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            let request = sentList[sender.tag] as! NSDictionary
            print("Cancel")
            let alert = UIAlertController(title: "Cancel Request", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                self.cancelTeamRequestSentToPlayer(teamPlayerId: request["TEAM_PLAYER_ID"] as! String)
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler:{ action in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func acceptButtonRequestTapped(_ sender: UIButton) {
        print("ACCEPT")
        let request = receivedList[sender.tag] as! NSDictionary
        let alert = UIAlertController(title: "Accept Request", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Accept", style: UIAlertActionStyle.default, handler: { action in
            self.replyToTeamRequest(requestSupportId: request["REQUEST_SUPPORT_ID"] as! String, accept: 1)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:{ action in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func replyToTeamRequest(requestSupportId: String, accept: Int) {
        tableActivityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_PLAYER_ID\":\"\(requestSupportId)\",\"STATUS\":\"\(accept)\",\"REQUEST_TYPE\": \"Received\"}", mod: "Team", actionType: "request-reply") { (response: Any) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.fetchMyRequests()
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.tableActivityIndicator.stopAnimating()
        }
    }
    
    func cancelTeamRequestSentToPlayer(teamPlayerId: String) {
        tableActivityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TEAM_PLAYER_ID\":\"\(teamPlayerId)\"}", mod: "Team", actionType: "cancel-request") { (response: Any) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.fetchMyRequests()
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.tableActivityIndicator.stopAnimating()
        }
    }
}













