//
//  AwardsPrizesTableViewCell.swift
//  CricDost
//
//  Created by JIT GOEL on 8/24/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class AwardsPrizesTableViewCell: UITableViewCell {

   @IBOutlet weak var tourAwardsNameLabel: UILabel!
    @IBOutlet weak var tourPrizeAmount: UITextField!
    @IBOutlet weak var tourPrizeButton: UIButton!
    @IBOutlet weak var tourAwardsRemoveButton: UIButton!
    @IBOutlet weak var viewHolder: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        CommonUtil.buttonRoundedCorners(buttons: [tourAwardsRemoveButton])
        viewHolder.layer.cornerRadius = 3
        viewHolder.clipsToBounds = true
        
        
    }

 

}
