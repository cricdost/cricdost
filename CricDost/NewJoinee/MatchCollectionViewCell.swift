//
//  MatchCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 5/28/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class MatchCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var team1ImageView: UIImageView!
    @IBOutlet weak var team2ImageView: UIImageView!
    @IBOutlet weak var team1NameLabel: UILabel!
    @IBOutlet weak var team2NameLabel: UILabel!
    @IBOutlet weak var numOfPlayersLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var joinMatchBUtton: UIButton!
    @IBOutlet weak var challengeButton: UIButton!
    @IBOutlet weak var numOfChallengers: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 3.0
        layer.shadowRadius = 3
        layer.shadowOpacity = 0.3
        layer.shadowOffset = CGSize(width: 2, height: 2)
        self.clipsToBounds = false
        containerView.layer.cornerRadius = 5.0
        containerView.clipsToBounds = false
    }
}
