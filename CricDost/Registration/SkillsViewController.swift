//
//  SkillsViewController.swift
//  CricDost
//
//  Created by Jit Goel on 5/25/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import LTMorphingLabel

class SkillsViewController: UIViewController, ShowsAlert,LTMorphingLabelDelegate {

    @IBOutlet weak var keepingTickButton: UIButton!
    @IBOutlet weak var batTickButton: UIButton!
    @IBOutlet weak var bowlTickButton: UIButton!
    @IBOutlet weak var bowlImageView: UIImageView!
    @IBOutlet weak var keepingImageView: UIImageView!
    @IBOutlet weak var batImageView: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var loadingViewPreventUserInteraction: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var bat: Bool = true
    var bowl: Bool = true
    var keeping: Bool = true
    @IBOutlet weak var heySuperLabel: LTMorphingLabel!
    @IBOutlet weak var iLoveLabel: LTMorphingLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonUtil.buttonRoundedCorners(buttons: [keepingTickButton,batTickButton,bowlTickButton])
        
        let bat = UITapGestureRecognizer(target: self, action: #selector(batFunction))
        batImageView.addGestureRecognizer(bat)
        
        let bowl = UITapGestureRecognizer(target: self, action: #selector(bowlFunction))
        bowlImageView.addGestureRecognizer(bowl)
        
        let keeping = UITapGestureRecognizer(target: self, action: #selector(keepingFunction))
        keepingImageView.addGestureRecognizer(keeping)
        
        setLabelMorph(labels: [heySuperLabel], effect: .evaporate)
        setLabelMorph(labels: [iLoveLabel], effect: .evaporate)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        heySuperLabel.text = "Hey! Super!"
        iLoveLabel.text = "I Love?"
    }
    
    func setLabelMorph(labels: [LTMorphingLabel], effect: LTMorphingEffect) {
        for label in labels {
            label.delegate = self
            label.morphingEffect = effect
            label.start()
        }
    }
    
    @objc func batFunction(sender:UITapGestureRecognizer) {
        batTickButton.isHidden = !bat
        bat = !bat
        showNextButton()
    }
    
    @objc func bowlFunction(sender:UITapGestureRecognizer) {
        bowlTickButton.isHidden = !bowl
        bowl = !bowl
        showNextButton()
    }
    
    @objc func keepingFunction(sender:UITapGestureRecognizer) {
        keepingTickButton.isHidden = !keeping
        keeping = !keeping
        showNextButton()
    }
    
    func showNextButton() {
        if !bat || !bowl || !keeping {
            UIView.transition(with: nextButton, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.nextButton.isHidden = false
            }, completion: nil)
        } else {
            UIView.transition(with: nextButton, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.nextButton.isHidden = true
            }, completion: nil)
        }
    }
    
    func getSkillSet() -> String {
        var dataSkill = ""
        if !bowl {
            dataSkill = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_INFO\":{\"IS_BOWLER\":\(!bowl)}}"
        }
        if !bat {
            dataSkill = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_INFO\":{\"IS_BATSMAN\":\(!bat)}}"
        }
        if !keeping {
            dataSkill = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_INFO\":{\"IS_KEEPER\":\(!keeping)}}"
        }
        
        if !bat && !bowl {
            dataSkill = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_INFO\":{\"IS_BATSMAN\":\(!bat),\"IS_BOWLER\":\(!bowl)}}"
        }
        
        if !bat && !keeping {
            dataSkill = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_INFO\":{\"IS_BATSMAN\":\(!bat),\"IS_KEEPER\":\(!keeping)}}"
        }
        
        if !bowl && !keeping {
            dataSkill = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_INFO\":{\"IS_BOWLER\":\(!bowl),\"IS_KEEPER\":\(!keeping)}}"
        }
        
        if !bat && !bowl && !keeping {
            dataSkill = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_INFO\":{\"IS_BATSMAN\":\(!bat),\"IS_BOWLER\":\(!bowl),\"IS_KEEPER\":\(!keeping)}}"
        }
        return dataSkill
    }
    
    @IBAction func nextButtonClick(_ sender: Any) {
        activityIndicator.startAnimating()
        loadingViewPreventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: getSkillSet(), mod: "Player", actionType: "update-player-details", callback: {(response : Any) -> Void in
            print(response)
            if response as? String != "error" {
                if((response as! NSDictionary)["XSCStatus"] as! Int == 0) {
                    self.performSegue(withIdentifier: "skill_location", sender: self)
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.loadingViewPreventUserInteraction.isHidden = true
        })
    }
}











