//
//  SplashViewController.swift
//  CricDost
//
//  Created by Jit Goel on 5/23/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import Lottie
import NVActivityIndicatorView
import Firebase

class SplashViewController: UIViewController, ShowsAlert {
    
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    var deviceId:String?
    var nameVC_goto: String?
    var remoteConfig: RemoteConfig?
    let Xs = ["1","2","3"]
    let As = ["a","b","c"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Load Splash Logo Anime
        if let animationView = LOTAnimationView(name: "appLogo") {
            animationView.frame = CGRect(x: 0, y: 0, width: 200, height: 150)
            animationView.center = self.view.center
            animationView.contentMode = .scaleAspectFill

            view.addSubview(animationView)

            animationView.play()
        }
       
        //Loading Indicator at bottom
        loadingIndicator.type = NVActivityIndicatorType(rawValue: 1)!
        loadingIndicator.startAnimating()
        
        //get device id
        deviceId = UIDevice.current.identifierForVendor!.uuidString
        
        
        remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig?.configSettings = RemoteConfigSettings(developerModeEnabled: false)
        
        for x in Xs {
            print("Xs", x)
            for a in As {
                if a == "a" {
                    print("As", a)
                    break
                } else if a == "b" {
                    print("As", a)
                    break
                } else if a == "c" {
                    print("As", a)
                    break
                }
            }
        }
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "splash_name" {
            let nameVC = segue.destination as! NameImageViewController
            nameVC.goto = nameVC_goto
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //        if UserDefaults.standard.object(forKey: CommonUtil.LANGUAGE) as? String == nil {
        //            performSegue(withIdentifier: "splash_language", sender: self)
        //        } else {
        if (currentReachabilityStatus != .notReachable) {
            remoteConfig?.fetch(withExpirationDuration: TimeInterval(5)) { (status, error) -> Void in
                print("SPLASH")
                if status == .success {
                    print("Config fetched!")
                    self.remoteConfig?.activateFetched()
                    
                    print("OTP ENABLE", self.remoteConfig!["otp_enabled"].stringValue!)
                    
                    if self.remoteConfig!["otp_enabled"].stringValue! == "false" {
                        UserDefaults.standard.set(true, forKey: CommonUtil.OTP_ENABLED)
                    } else {
                        UserDefaults.standard.set(false, forKey: CommonUtil.OTP_ENABLED)
                    }
                    
                    if CommonUtil.DeveloperMode {
                        CommonUtil.CHAT_AUTH_KEY = self.remoteConfig!["chat_auth_key_test"].stringValue!
                        CommonUtil.CHAT_CLIENT_API = self.remoteConfig!["chat_client_api_test"].stringValue!
                        CommonUtil.CHAT_CLIENT_PORT = self.remoteConfig!["chat_client_port_test"].stringValue!
                        CommonUtil.cricdost_group_chat_domain_name = self.remoteConfig!["cricdost_group_chat_domain_name_test"].stringValue!
                        CommonUtil.cricdost_chat_domain_name = self.remoteConfig!["cricdost_chat_domain_name_test"].stringValue!
                        CommonUtil.cricdost_chat_domain = self.remoteConfig!["cricdost_chat_domain_test"].stringValue!
                        CommonUtil.SOCKET_URL = self.remoteConfig!["cricdost_socket_api_test"].stringValue!
                        CommonUtil.BASE_URL = self.remoteConfig!["cricdost_api_key_ios_test"].stringValue!
                    } else {
                        CommonUtil.CHAT_AUTH_KEY = self.remoteConfig!["chat_auth_key"].stringValue!
                        CommonUtil.CHAT_CLIENT_API = self.remoteConfig!["chat_client_api"].stringValue!
                        CommonUtil.CHAT_CLIENT_PORT = self.remoteConfig!["chat_client_port"].stringValue!
                        CommonUtil.cricdost_group_chat_domain_name = self.remoteConfig!["cricdost_group_chat_domain_name"].stringValue!
                        CommonUtil.cricdost_chat_domain_name = self.remoteConfig!["cricdost_chat_domain_name"].stringValue!
                        CommonUtil.cricdost_chat_domain = self.remoteConfig!["cricdost_chat_domain"].stringValue!
                        CommonUtil.SOCKET_URL = self.remoteConfig!["cricdost_socket_api"].stringValue!
                        CommonUtil.BASE_URL = self.remoteConfig!["cricdost_api_key_ios"].stringValue!
                    }
                    print("flip_interval",self.remoteConfig!["flip_interval"].stringValue!)
                    
                    print("chat_auth_key",CommonUtil.CHAT_AUTH_KEY)
                    UserDefaults.standard.set(CommonUtil.CHAT_AUTH_KEY, forKey: CommonUtil.CHAT_AUTH_KEY_SHARED)
                    
                    print("chat_client_api",CommonUtil.CHAT_CLIENT_API)
                    UserDefaults.standard.set(CommonUtil.CHAT_CLIENT_API, forKey: CommonUtil.CHAT_CLIENT_API_SHARED)
                    
                    print("chat_client_port",CommonUtil.CHAT_CLIENT_PORT)
                    UserDefaults.standard.set(CommonUtil.CHAT_CLIENT_PORT, forKey: CommonUtil.CHAT_CLIENT_PORT_SHARED)
                    
                    print("cricdost_group_chat_domain_name",CommonUtil.cricdost_group_chat_domain_name)
                    UserDefaults.standard.set(CommonUtil.cricdost_group_chat_domain_name, forKey: CommonUtil.cricdost_group_chat_domain_name_SHARED)
                    
                    print("cricdost_chat_domain_name",CommonUtil.cricdost_chat_domain_name)
                    UserDefaults.standard.set(CommonUtil.cricdost_chat_domain_name, forKey: CommonUtil.cricdost_chat_domain_name_SHARED)
                    
                    print("cricdost_chat_domain",CommonUtil.cricdost_chat_domain)
                    UserDefaults.standard.set(CommonUtil.cricdost_chat_domain, forKey: CommonUtil.cricdost_chat_domain_SHARED)
                    
                    print("cricdost_socket_api",CommonUtil.SOCKET_URL)
                    UserDefaults.standard.set(CommonUtil.SOCKET_URL, forKey: CommonUtil.SOCKET_URL_SHARED)
                    
                    print("cricdost_api_key_ios",CommonUtil.BASE_URL)
                    UserDefaults.standard.set(CommonUtil.BASE_URL, forKey: CommonUtil.BASE_URL_SHARED)
                } else {
                    print("Config not fetched")
                    print("Error: \(error?.localizedDescription ?? "No error available.")")
                }
                if UserDefaults.standard.object(forKey: CommonUtil.BASE_URL_SHARED) as? String ?? "" != "" {
                    CommonUtil.CHAT_AUTH_KEY = UserDefaults.standard.object(forKey: CommonUtil.CHAT_AUTH_KEY_SHARED) as! String
                    CommonUtil.CHAT_CLIENT_API = UserDefaults.standard.object(forKey: CommonUtil.CHAT_CLIENT_API_SHARED) as! String
                    CommonUtil.CHAT_CLIENT_PORT = UserDefaults.standard.object(forKey: CommonUtil.CHAT_CLIENT_PORT_SHARED) as! String
                    CommonUtil.cricdost_group_chat_domain_name = UserDefaults.standard.object(forKey: CommonUtil.cricdost_group_chat_domain_name_SHARED) as! String
                    CommonUtil.cricdost_chat_domain_name = UserDefaults.standard.object(forKey: CommonUtil.cricdost_chat_domain_name_SHARED) as! String
                    CommonUtil.cricdost_chat_domain = UserDefaults.standard.object(forKey: CommonUtil.cricdost_chat_domain_SHARED) as! String
                    CommonUtil.SOCKET_URL = UserDefaults.standard.object(forKey: CommonUtil.SOCKET_URL_SHARED) as! String
                    CommonUtil.BASE_URL = UserDefaults.standard.object(forKey: CommonUtil.BASE_URL_SHARED) as! String
                    
                    let _ : Timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.myPerformeCode), userInfo: nil, repeats: false)
//                    self.myPerformeCode()
                } else {
                    self.showAlert(title: "Server Busy", message: "Please try after sometime")
                }
            }
        } else {
            showAlert(title: "Network Issue", message: "Please check you network connection")
        }
        //        }
    }
    
    @objc func myPerformeCode() {
        //Verify User Already registered or not
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_ID\":\"\(deviceId!)\"}", mod: "AA", actionType: "user-verification", callback: {(response : Any) -> Void in
            
            if response as? String != "error" {
                let profile = (response as! NSDictionary)["XSCData"] as! NSDictionary
                print("SPLASH RESPONSE", response)
                if profile["USER_STATUS"] as! Int == 0 && ((response as! NSDictionary)["XSCStatus"] as! Int) == 0 {
                    UserDefaults.standard.set(profile["DEVICE_KEY"] as? String ?? "", forKey: CommonUtil.DEVICE_KEY)
                    UserDefaults.standard.set(profile["USER_ID"] as? String ?? "", forKey: CommonUtil.USER_ID)
                    UserDefaults.standard.set(profile["ADDRESS"] as? String ?? "", forKey: CommonUtil.ADDRESS)
                    UserDefaults.standard.set(profile["SKILLS"] as? String ?? "", forKey: CommonUtil.SKILLS)
                    UserDefaults.standard.set(profile["IMAGE_URL"] as? String ?? "", forKey: CommonUtil.IMAGE_URL)
                    UserDefaults.standard.set(profile["FULL_NAME"] as? String ?? "", forKey: CommonUtil.FULL_NAME)
                    UserDefaults.standard.set(profile["MOBILE_NUMBER"] as? String ?? "", forKey: CommonUtil.MOBILE_NUMBER)
                    UserDefaults.standard.set(profile["COUNTRY_CODE"] as? String ?? "+91", forKey: CommonUtil.COUNTRY_CODE)
                    UserDefaults.standard.set(profile["TEAM_COUNT"] as? Int ?? "", forKey: CommonUtil.TEAM_COUNT)
                    UserDefaults.standard.set(profile["STEP_NAME"] as? String ?? "", forKey: CommonUtil.STEPNAME)
//                    UserDefaults.standard.set(profile["FB_TOKEN"] as? String ?? "", forKey: CommonUtil.FB_TOKEN)
                    UserDefaults.standard.set(profile["OPENFIRE_PASSWORD"] as? String ?? "", forKey: CommonUtil.OPENFIRE_PASSWORD)
                    UserDefaults.standard.set(profile["OPENFIRE_USERNAME"] as? String ?? "", forKey: CommonUtil.OPENFIRE_USERNAME)
                    
                    print("Country Code", profile["COUNTRY_CODE"] as? String ?? "+91")
                    
                    switch(profile["STEP_NAME"] as! String) {
                        
                    case "NAME":    self.nameVC_goto = "NAME"
                    self.performSegue(withIdentifier: "splash_name", sender: self)
                        break
                    case "IMAGE":   self.nameVC_goto = "IMAGE"
                    self.performSegue(withIdentifier: "splash_name", sender: self)
                        break
                    case "GENDER":  self.performSegue(withIdentifier: "splash_gender", sender: self)
                        break
                    case "DOB":     self.performSegue(withIdentifier: "splash_dob", sender: self)
                        break
                    case "PLAYER_STATUS": self.performSegue(withIdentifier: "splash_skills", sender: self)
                        break
                    case "LOCATION": self.performSegue(withIdentifier: "splash_location", sender: self)
                        break
                    case "DASHBOARD":
                        if CommonUtil.LandingPageCricSpace {
                            self.performSegue(withIdentifier: "splash_cricspace", sender: self)
                        } else {
                            self.performSegue(withIdentifier: "splash_dashboard", sender: self)
                        }
                        break
                    default:
                        print("DEFAULT CASE")
                    }
                    
                } else {
                    print("New User")
                    self.performSegue(withIdentifier: "splash_mobile", sender: self)
                }
            }
        })
        
    }
}








