//
//  MessageViewController.swift
//  CricDost
//
//  Created by Jit Goel on 7/17/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import XMPPFramework
import IQKeyboardManager
import UserNotifications

class MessageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate,UNUserNotificationCenterDelegate {
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var typingLabel: UILabel!
    var xmppController: XMPPController?
    var stream: XMPPStream?
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var messageTableView: UITableView!
    var password: String?
    var userName: String?
    var playerUserName: String?
    var JIDPrefix: String?
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var navBarImage: UIImageView!
    @IBOutlet weak var navBarTitle: UILabel!
    var imageUrl: String = ""
    
    var userChatHistoryBackup: [String]?
    var messageList: NSMutableArray?
    var messageList1: NSMutableArray? = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().layoutIfNeededOnUpdate = true
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = CommonUtil.themeRed
        }
//        UIApplication.shared.statusBarStyle = .lightContent
        
        sendButton.setImage(#imageLiteral(resourceName: "baseline_send_white_24pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        sendButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(self.keyboardNotification(notification:)),
//                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
//                                               object: nil)
        
        messageTextView.textContainer.heightTracksTextView = true
        messageTextView.textContainer.widthTracksTextView = false
        messageTextView.isScrollEnabled = false
        messageTextView.layer.cornerRadius = 5.0
        
        userName = UserDefaults.standard.object(forKey: CommonUtil.OPENFIRE_USERNAME) as? String
        password = UserDefaults.standard.object(forKey: CommonUtil.OPENFIRE_PASSWORD) as? String
        
        userChatHistoryBackup = UserDefaults.standard.object(forKey: userName!) as? [String] ?? []
        
        print("userChatHistoryBackup", userChatHistoryBackup)
        
        stream = self.xmppController?.xmppStream
        JIDPrefix = playerUserName
        
        let userlist = UserDefaults.standard.object(forKey: CommonUtil.USERLIST) as! Data
        let decodedData = NSKeyedUnarchiver.unarchiveObject(with: userlist) as! NSDictionary
        
        print(JIDPrefix!, decodedData["users"] as? NSArray ?? [])
        
        UserDefaults.standard.set(JIDPrefix!, forKey: CommonUtil.CURRENTCHAT)
        let users = decodedData["users"] as? NSArray ?? []
        
        for user in users {
            if JIDPrefix! == (user as! NSDictionary)["username"] as! String {
                navBarTitle.text = (user as! NSDictionary)["name"] as? String
            }
        }
        
        if let tempNames: NSArray = UserDefaults.standard.object(forKey: JIDPrefix!) as? NSArray {
            messageList = tempNames.mutableCopy() as? NSMutableArray
        } else {
            messageList = []
        }
        
        CommonUtil.imageRoundedCorners(imageviews: [navBarImage])
        messageTableView.transform = CGAffineTransform(rotationAngle: -(.pi))
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshChatList), name: NSNotification.Name(rawValue: "refreshChatList"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showTyping), name: NSNotification.Name(rawValue: "showTyping"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showOnlinePresence), name: NSNotification.Name(rawValue: "showOnlinePresence"), object: nil)
        let url = UserDefaults.standard.object(forKey: "\(JIDPrefix!)_IMAGEURL") as? String ?? ""
        if url != "" {
            print("\(CommonUtil.BASE_URL)\(url)")
            navBarImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(url)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
        }
        
        if JIDPrefix == "cricdost" {
            navBarImage.image = #imageLiteral(resourceName: "cd_logo_version")
            navBarImage.contentMode = .scaleAspectFit
            navBarImage.layer.masksToBounds = false
            navBarImage.layer.cornerRadius = 0
            navBarImage.clipsToBounds = true
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageList!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let msg = messageList![indexPath.row] as! NSDictionary
        
        if msg["type"] as! String == "SEND" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sentmessageCell", for: indexPath) as! SentMessageTableViewCell
            cell.transform = CGAffineTransform(rotationAngle: .pi)
            cell.messageLabel.text = msg["message"] as? String
            cell.timeLabel.text = msg["time"] as? String ?? "0:00"
            cell.profileImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(UserDefaults.standard.object(forKey: CommonUtil.IMAGE_URL) as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "receivedMessageCell", for: indexPath) as! ReceivedMessageTableViewCell
            cell.messageLabel.text = msg["message"] as? String
            cell.timeLabel.text = msg["time"] as? String ?? "0:00"
            cell.transform = CGAffineTransform(rotationAngle: .pi)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        messageTextView.resignFirstResponder()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        CommonUtil.updateGATracker(screenName: "Chat")
        if UserDefaults.standard.object(forKey: CommonUtil.NewNotification) as? Bool ?? false {
            UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: ["NewIdentifier"])
            print("recieve message from cricdost")
            let formatter1 = DateFormatter()
            formatter1.dateFormat = "dd, MMM hh:mm a"
            let dateString1 = formatter1.string(from: Date())
            let recievedMessage: NSDictionary = ["Name":"cricdost", "message": "Hey Dost,\nDo you want to be the next Dhoni? \nCome let’s play Cricket! Get to know new cricket players and teams nearby you and make your profile familiar among star players using CricDost App.\nFeeling excited? Reach us \n@ 7550290888 or email us \n@ info@cricdost.com.", "time": dateString1,"type":"RECEIVED", "nickName":"","image":""]
            messageList1?.insert(recievedMessage, at: 0)
            //            messageList1?.add(recievedMessage!)
            if !(userChatHistoryBackup!.contains("cricdost")) {
                userChatHistoryBackup?.append("cricdost")
            }
            UserDefaults.standard.set(userChatHistoryBackup, forKey: userName!)
            UserDefaults.standard.set(messageList1, forKey: "cricdost")
        }
        
        if let tempNames: NSArray = UserDefaults.standard.object(forKey: JIDPrefix!) as? NSArray {
            messageList = tempNames.mutableCopy() as? NSMutableArray
        } else {
            messageList = []
        }
        print("MESSAGE HIST",UserDefaults.standard.object(forKey: JIDPrefix!) as? NSArray)
        
        UserDefaults.standard.set(0, forKey: "\(JIDPrefix!)_\(userName!)")
        messageTableView.reloadData()
        
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        print("back back back", UserDefaults.standard.object(forKey: CommonUtil.NewNotification) as? Bool)
        if UserDefaults.standard.object(forKey: CommonUtil.NewNotification) as? Bool ?? true {
            print("back from message to home")
            if CommonUtil.LandingPageCricSpace {
                self.performSegue(withIdentifier: "message_cricspace", sender: self)
            } else {
                self.performSegue(withIdentifier: "message_home", sender: self)
            }
             UserDefaults.standard.set(false, forKey: CommonUtil.NewNotification)
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateNotificationCount"), object: nil)
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshChatHome"), object: nil)
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        print("typing")
        let matchInfo:[String: NSDictionary] = ["data": ["JID" : JIDPrefix!,"MSG": text]]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "sendComposing"), object: nil, userInfo: matchInfo)
        return true
    }
    
    @IBAction func sendButtonClick(_ sender: Any) {
        if (messageTextView.text).trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            if messageList?.count == 0 {
                userChatHistoryBackup?.append(JIDPrefix!)
                UserDefaults.standard.set(userChatHistoryBackup, forKey: userName!)
            }
            let matchInfo:[String: NSDictionary] = ["data": ["JID" : JIDPrefix!,"MSG":  (messageTextView.text).trimmingCharacters(in: .whitespacesAndNewlines)]]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "sendMessage"), object: nil, userInfo: matchInfo)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd, MMM hh:mm a"
            let dateString = formatter.string(from: Date())
            let sendMessage: NSDictionary = ["Name":JIDPrefix!, "message": (messageTextView.text).trimmingCharacters(in: .whitespacesAndNewlines), "time": dateString,"type":"SEND"]
            messageTextView.text = ""
            //            messageTextView.resignFirstResponder()
            messageList?.insert(sendMessage, at: 0)
            UserDefaults.standard.set(messageList, forKey: JIDPrefix!)
            UserDefaults.standard.set(0, forKey: "\(JIDPrefix!)_\(userName!)")
            messageTableView.reloadData()
        }
    }
    
    @objc func refreshChatList(_ notification: NSNotification) {
        typingLabel.isHidden = true
        print("Received notification", notification.userInfo![AnyHashable("data")] as! NSDictionary)
        if let tempNames: NSArray = UserDefaults.standard.object(forKey: JIDPrefix!) as? NSArray {
            messageList = tempNames.mutableCopy() as? NSMutableArray
        } else {
            messageList = []
        }
        UserDefaults.standard.set(0, forKey: "\(JIDPrefix!)_\(userName!)")
        messageTableView.reloadData()
        print("ReFreshing Chat")
    }
    
    @objc func showTyping() {
        typingLabel.isHidden = false
        typingLabel.text = "typing..."
        Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(hideTypeLabel), userInfo: nil, repeats: false)
    }
    
    @objc func showOnlinePresence() {
        let presence = UserDefaults.standard.object(forKey: "\(JIDPrefix!)_\(CommonUtil.PRESENCE)") as? String ?? "unavailable"
        if presence == "available" {
            typingLabel.isHidden = false
            typingLabel.text = "Online"
        } else {
            typingLabel.isHidden = true
        }
    }
    
    @objc func hideTypeLabel(){
        showOnlinePresence()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant = 0.0
            } else {
                self.keyboardHeightLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
}









