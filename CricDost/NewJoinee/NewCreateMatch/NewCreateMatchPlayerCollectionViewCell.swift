//
//  NewCreateMatchPlayerCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 10/8/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class NewCreateMatchPlayerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var playerImageView: UIImageView!
    @IBOutlet weak var playerNameLabel: UILabel!
    @IBOutlet weak var playerSkillLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        CommonUtil.imageRoundedCorners(imageviews: [playerImageView])
        
//        self.layer.cornerRadius = 3.0
//        layer.shadowRadius = 3
//        layer.shadowOpacity = 0.3
//        layer.shadowOffset = CGSize(width: 2, height: 2)
//        self.clipsToBounds = false
    }
}
