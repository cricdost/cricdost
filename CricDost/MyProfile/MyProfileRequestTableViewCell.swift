//
//  MyProfileRequestTableViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 6/13/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class MyProfileRequestTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var declineButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var lineView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
