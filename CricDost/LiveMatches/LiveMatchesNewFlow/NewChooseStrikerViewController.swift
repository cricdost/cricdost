//
//  ChooseStrikerViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 9/18/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class NewChooseStrikerViewController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegate, ShowsAlert {
   
    @IBOutlet weak var strikerViewHolder: UIView!
    @IBOutlet weak var strikerCollectionView: UICollectionView!
    @IBOutlet weak var strikerNextButton: UIButton!
    var matchID: String?
    var adminDetail: NSDictionary?
    var playerList: NSArray?
    var selected_Player: String = ""
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var preventUserInteraction: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        CommonUtil.roundedUIViewCornersWithShade(uiviews: [strikerViewHolder])
        CommonUtil.buttonRoundedCorners(buttons: [strikerNextButton])
        print(adminDetail)
        print(playerList)
    }

    @IBAction func strikerNextButtonClick(_ sender: Any) {
        print("striker next button clicked")
        if selected_Player != "" {
            activityIndicator.startAnimating()
            preventUserInteraction.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"BATSMAN\":\"\(selected_Player)\",\"STRIKER\":\"1\",\"MATCH_ID\":\"\(matchID!)\"}", mod: "MatchScore", actionType: "create-batsman") { (response) in
                if response as? String != "error" {
                    print(response)
                    let data: [String: Any] = ["match_id": self.matchID!,
                                               "user_id": UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String,
                                               "step_name":"BAT_USER_ID",
                                               "won_team_id":"",
                                               "won_team_name": "",
                                               "bat_or_bowl":"",
                                               "won_team_object":""]
                    SocketConnectionsclass.matchStatusUpdate_NewFlow(param: data)
                    self.dismiss(animated: true, completion: {
                        let playerList:[String: Any] = ["playerList": self.playerList!, "match_id":self.matchID!,"striker":self.selected_Player]
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openNonStrikerViewController"), object: nil, userInfo: playerList)
                    })
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
                self.preventUserInteraction.isHidden = true
            }
        } else {
            self.strikerViewHolder.shake()
            self.showAlert(title: "Striker", message: "Please select a striker")
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return playerList!.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let strikerPlayer = playerList![indexPath.row] as! NSDictionary
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewChooseStrikerCollectionViewCell", for: indexPath) as! NewChooseStrikerCollectionViewCell
        cell.strikerImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(strikerPlayer["IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "profile_pic_default"))
        cell.strikerNameLabel.text = strikerPlayer["FULL_NAME"] as? String
        
        if selected_Player == strikerPlayer["MATCH_TEAM_PLAYER_ID"] as? String ?? "\(strikerPlayer["MATCH_TEAM_PLAYER_ID"] as! Int)" {
            cell.strikerTickButton.isHidden = false
        } else {
            cell.strikerTickButton.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let strikerPlayer = playerList![indexPath.row] as! NSDictionary
        selected_Player = strikerPlayer["MATCH_TEAM_PLAYER_ID"] as? String ?? "\(strikerPlayer["MATCH_TEAM_PLAYER_ID"] as! Int)"
        collectionView.reloadData()
    }

}
