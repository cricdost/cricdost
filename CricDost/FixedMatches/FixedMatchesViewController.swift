//
//  FixedMatchesViewController.swift
//  CricDost
//
//  Created by Jit Goel on 6/21/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import AnimatedCollectionViewLayout

class FixedMatchesViewController: PullUpController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ShowsAlert {
    
    var currentAddressID: String?
    var currentMatchID: String?
    var fixedMatchesDetails: Any?
    var address: String?
    @IBOutlet weak var numberOfMatchesButton: UIButton!
    var matchList: NSArray = []
    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet weak var matchesCollectionView: UICollectionView!
    @IBOutlet weak var preventUserInteraction: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    let layout = AnimatedCollectionViewLayout()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(dismissFixedMatchesFunction), name: NSNotification.Name(rawValue: "dismissFixedMatchesFunction"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadFixedMacthListFunction), name: NSNotification.Name(rawValue: "reloadFixedMacthListFunction"), object: nil)
        
        print(fixedMatchesDetails)
        matchList = (fixedMatchesDetails as! NSDictionary)["XSCData"] as! NSArray
        numberOfMatchesButton.setTitle("\(matchList.count) Fixed Matches on this location", for: .normal)
        numberOfMatchesButton.layer.cornerRadius = 5
        numberOfMatchesButton.clipsToBounds = true
        
        layout.animator = LinearCardAttributesAnimator(minAlpha: 1.0, itemSpacing: 0.18, scaleRate: 0.9)
        layout.scrollDirection = .horizontal
        matchesCollectionView.collectionViewLayout = layout
        matchesCollectionView.isPagingEnabled = true
        
        didMoveToStickyPoint = { [weak self] point in
            if point == -100.0 {
                NotificationCenter.default.post(name: NSNotification.Name("dismissBlackScreen"), object: nil)
            }
        }
    
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return matchList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let match = (matchList[indexPath.section] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        let teamA = match["TEAM_A"] as! NSDictionary
        let teamB = match["TEAM_B"] as! NSDictionary
        
        let score = (matchList[indexPath.section] as! NSDictionary)["SCORE_DETAILS"] as! NSDictionary
        
        if score["STATUS"] as! Int == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "liveMatchCell", for: indexPath) as! FixedMatchLiveCollectionViewCell
            
            cell.teamAImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamA["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            
            cell.teamBImageVIew.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamB["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            cell.teamAName.text = teamA["TEAM_NAME"] as? String
            cell.teamBName.text = teamB["TEAM_NAME"] as? String
            cell.addressLabel.text = match["ADDRESS"] as? String
            cell.teamAScore.text = (score["TEAM_A"] as! NSDictionary)["RUN_STR"] as? String
            cell.teamBScore.text = (score["TEAM_B"] as! NSDictionary)["RUN_STR"] as? String
            
            if match["IS_ADMIN"] as! Int == 0 {
                cell.adminButton.isHidden = true
            } else {
                cell.adminButton.isHidden = false
            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fixedMatchesCell", for: indexPath) as! FixedMatchesCollectionViewCell
            cell.dateLabel.text = match["DATE"] as? String
            cell.timeLabel.text = match["TIME"] as? String
            cell.addressLabel.text = match["ADDRESS"] as? String
            cell.numberOfPlayers.text = "\(match["AVAILABLE_PLAYERS"] as? String ?? String(match["AVAILABLE_PLAYERS"] as! Int) )/\(match["TOTAL_PLAYERS"] as? String ?? String(match["TOTAL_PLAYERS"] as! Int)) Players"
            cell.teamAName.text = teamA["TEAM_NAME"] as? String
            cell.teamAImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamA["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            
            if teamA["IS_TEAM_PLAYER"] as! Int == 1 {
                cell.teamAPlayerImage.isHidden = false
                cell.teamAPlayerImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(UserDefaults.standard.object(forKey: CommonUtil.IMAGE_URL) as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
            } else {
                cell.teamAPlayerImage.isHidden = true
            }
            
            if teamB["IS_TEAM_PLAYER"] as! Int == 1 {
                cell.teamBPlayerImage.isHidden = false
                cell.teamBPlayerImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(UserDefaults.standard.object(forKey: CommonUtil.IMAGE_URL) as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
            } else {
                cell.teamBPlayerImage.isHidden = true
            }
            
            if  teamB["POSITION_STATUS"] as! Int == 0 {
                cell.teamBImageView.image = nil
                cell.teamBName.text = "Challengers"
                cell.challengersCount.isHidden = false
                cell.challengersCount.text = match["INTEREST_COUNT"] as? String
            } else {
                cell.challengersCount.isHidden = true
                cell.teamBImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamB["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
                cell.teamBName.text = teamB["TEAM_NAME"] as? String
            }
            if match["CAN_CHALLENGE"] as! Int == 1 {
                cell.challengeBUtton.isHidden = true
            } else {
                cell.challengeBUtton.isHidden = true
            }
            
            if match["IS_ADMIN"] as! Int == 0 {
                cell.adminButton.isHidden = true
            } else {
                cell.adminButton.isHidden = false
            }
            
            if match["CAN_CHAT"] as! Int == 0 {
                cell.chatButton.isHidden = true
                cell.chatButton1.isHidden = true
            } else {
                cell.chatButton.isHidden = false
                cell.chatButton1.isHidden = false
            }
            
            if match["CAN_EXIT"] as! Int == 0 {
                cell.exitButton.isHidden = true
            } else {
                cell.exitButton.isHidden = false
            }
            
            if match["CAN_JOIN"] as! Int == 1 {
                cell.joinButton.isHidden = true
            } else {
                cell.joinButton.isHidden = true
            }
            
            if match["CAN_TOSS"] as! Int == 1 && match["IS_ADMIN"] as! Int == 1 {
                cell.tossButton.setTitle("Go for toss", for: .normal)
                cell.tossButton.isHidden = false
            } else if match["CAN_TOSS"] as! Int == 1 && match["IS_OTHER_ADMIN"] as! Int == 1 {
                cell.tossButton.setTitle("Go for toss", for: .normal)
                cell.tossButton.isHidden = false
            } else if match["CAN_TOSS"] as! Int == 1 && match["IS_ADMIN"] as! Int == 0 {
                cell.tossButton.setTitle("Admin can toss now", for: .normal)
                cell.tossButton.isHidden = false
            }
            
            //        cell.joinButton.addTarget(self, action: #selector(joinButtonTapped), for: UIControlEvents.touchUpInside)
            //        cell.challengeBUtton.addTarget(self, action: #selector(challengeButtonTapped), for: UIControlEvents.touchUpInside)
            cell.chatButton.addTarget(self, action: #selector(chatButtonTapped), for: UIControlEvents.touchUpInside)
            cell.chatButton1.addTarget(self, action: #selector(chatButton1Tapped), for: UIControlEvents.touchUpInside)
            cell.exitButton.addTarget(self, action: #selector(exitButtonTapped), for: UIControlEvents.touchUpInside)
            //        cell.joinButton.tag = indexPath.section
            //        cell.challengeBUtton.tag = indexPath.section
            cell.tossButton.addTarget(self, action: #selector(tossButtonTapped), for: UIControlEvents.touchUpInside)
            cell.tossButton.tag = indexPath.section
            cell.chatButton.tag = indexPath.section
            cell.chatButton1.tag = indexPath.section
            cell.exitButton.tag = indexPath.section
            
            return cell
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        CommonUtil.updateGATracker(screenName: "Fixed matches")
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let score = (matchList[indexPath.section] as! NSDictionary)["SCORE_DETAILS"] as! NSDictionary
        let match = (matchList[indexPath.section] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        currentMatchID = match["MATCH_ID"] as? String
        if score["STATUS"] as! Int == 1 {
            if score["IS_SCORER"] as! Int == 1 {
                print("Go to non scorer activity")
                let scorerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScorerViewController") as? ScorerViewController
                scorerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                scorerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                scorerVC?.MATCH_ID = match["MATCH_ID"] as? String
                self.present(scorerVC!, animated: true, completion: nil)
            } else {
                print("Go to non scorer activity")
                if CommonUtil.NewLiveMatchFlow {
                    let non_scorerVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "TeamScorerViewController") as? TeamScorerViewController
                    non_scorerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    non_scorerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    non_scorerVC?.MATCH_ID = match["MATCH_ID"] as? String
                    self.present(non_scorerVC!, animated: true, completion: nil)
                } else {
                    let non_scorerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NonScorerViewController") as? NonScorerViewController
                    non_scorerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    non_scorerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    non_scorerVC?.MATCH_ID = match["MATCH_ID"] as? String
                    self.present(non_scorerVC!, animated: true, completion: nil)
                }
            }
        } else {
            let match = (matchList[indexPath.section] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\": \"\(match["MATCH_ID"] as! String)\"}", mod: "Match", actionType: "view-match-detail-dashboard") { (response) in
                if response as? String != "error" {
                    if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                        print(response)
                        print("open match details")
                        let matchInfo:[String: NSDictionary] = ["data": (response as! NSDictionary)]
                        
                        if UserDefaults.standard.object(forKey: CommonUtil.SearchOpenMatchesDetails) as? Bool ?? false {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openMatchesDetailnSearchViewVC"), object: nil, userInfo: matchInfo)
                        } else {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openMatchesDetailVC"), object: nil, userInfo: matchInfo)
                        }
                        
                    } else {
                        self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                    }
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
            }
        }
        
        
    }
    
    @IBAction func chatButtonTapped(_ sender: UIButton) {
        print("Chat")
        let match = (matchList[sender.tag] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
    
        let groupChatVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GroupChatViewController") as? GroupChatViewController
        groupChatVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        groupChatVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        groupChatVC?.groupJIDPrefix = match["OPENFIRE_USERNAME"] as? String
        groupChatVC?.teamName = "Match date:\(match["DATE"] as! String) \(match["TIME"] as! String)"
        self.present(groupChatVC!, animated: true, completion: nil)
    }
    @IBAction func chatButton1Tapped(_ sender: UIButton) {
        print("Chat")
        let match = (matchList[sender.tag] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        
        let groupChatVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GroupChatViewController") as? GroupChatViewController
        groupChatVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        groupChatVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        groupChatVC?.groupJIDPrefix = match["OPENFIRE_USERNAME"] as? String
        groupChatVC?.teamName = "Match date:\(match["DATE"] as! String) \(match["TIME"] as! String)"
        self.present(groupChatVC!, animated: true, completion: nil)
    }
    
    @IBAction func tossButtonTapped(_ sender: UIButton) {
        if sender.titleLabel?.text == "Go for toss" {
            let matchinfo:[String: Any] = ["matchInfo": matchList[sender.tag]]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowTossVCFunction"), object: nil, userInfo: matchinfo)
        } else {
            self.showAlert(title: "Toss", message: "Please request match admin to toss")
        }
    }
    
    @IBAction func exitButtonTapped(_ sender: UIButton) {
        let match = (matchList[sender.tag] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        currentMatchID = match["MATCH_ID"] as? String
        preventUserInteraction.isHidden = false
        activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_TEAM_PLAYER_ID\": \"\(match["MATCH_TEAM_PLAYER_ID"] as! String)\"}", mod: "Match", actionType: "exit-match-team", callback: { (response: Any) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    print(response)
                    self.getFixedMatches(addressId: self.currentAddressID!, address: self.address!)
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.preventUserInteraction.isHidden = true
            self.activityIndicator.stopAnimating()
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.width / CGFloat(1), height: collectionView.bounds.height / CGFloat(1))

//        let screensize = viewHolder.bounds.size
//        var cellwidth = floor(screensize.width * 0.8)
//        let cellheight = floor(matchesCollectionView.bounds.height * 0.95)
//
//        if UIDevice.current.userInterfaceIdiom == .pad {
//            cellwidth = floor(screensize.width * 0.6)
//        }
//
//        let insetX = (viewHolder.bounds.width - cellwidth) / 2.0
//        let insetY = (matchesCollectionView.bounds.height - cellheight) / 2.0
//
//        let layout = matchesCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
//        layout.itemSize = CGSize(width: cellwidth, height: cellheight)
//        matchesCollectionView.contentInset = UIEdgeInsetsMake(insetY, insetX, insetY, insetX)
//        return layout.itemSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    @objc func dismissFixedMatchesFunction() {
        pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[1], completion: nil)
    }
    
    @objc func reloadFixedMacthListFunction() {
        getFixedMatches(addressId: currentAddressID!, address: address!)
    }
    
    override var pullUpControllerPreferredSize: CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 300)
    }
    
    override var pullUpControllerPreviewOffset: CGFloat {
        return pullUpControllerMiddleStickyPoints[0]
    }
    
    override var pullUpControllerMiddleStickyPoints: [CGFloat] {
        return [300,-100]
    }
    
    override var pullUpControllerIsBouncingEnabled: Bool {
        return true
    }
    
    override var pullUpControllerPreferredLandscapeFrame: CGRect {
        return CGRect(x: 5, y: 5, width: 280, height: UIScreen.main.bounds.height - 10)
    }
    
    func getFixedMatches(addressId: String, address: String) {
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        currentAddressID = addressId
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"FIXED-MATCH\",\"ADDRESS_ID\":\"\(addressId)\"}", mod: "Map", actionType: "venue-items") { (response) in
            //            print(response)
            if response as? String != "error" {
                self.fixedMatchesDetails = response
                self.address = address
                self.currentAddressID = addressId
                self.matchList = (self.fixedMatchesDetails as! NSDictionary)["XSCData"] as! NSArray
                self.numberOfMatchesButton.setTitle("\(self.matchList.count) Open Matches on this location", for: .normal)
                if self.matchList.count == 0 {
                    self.pullUpControllerMoveToVisiblePoint(self.pullUpControllerMiddleStickyPoints[1], completion: nil)
                }
                self.matchesCollectionView.reloadData()
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        }
    }

}














