//
//  PlayerCareerViewController.swift
//  CricDost
//
//  Created by Jit Goel on 6/6/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class PlayerCareerViewController: UIViewController, ShowsAlert {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var cdRankButton: UIButton!
    @IBOutlet weak var cardView: UIView!
    var currentPlayerID: String?
    var currentAddress: String?
    var followers: String?
    var playerImageURL: String?
    @IBOutlet weak var outView1: UIView!
    @IBOutlet var outView2: UIView!
    var cdrank: String?
    var playerName: String?
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var batMatches: UILabel!
    @IBOutlet weak var batInnings: UILabel!
    @IBOutlet weak var batNotOut: UILabel!
    @IBOutlet weak var batRuns: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var batHighest: UILabel!
    @IBOutlet weak var batHundreds: UILabel!
    @IBOutlet weak var batFifty: UILabel!
    @IBOutlet weak var batAverage: UILabel!
    @IBOutlet weak var bowlMatches: UILabel!
    @IBOutlet weak var bowlInnings: UILabel!
    @IBOutlet weak var bowlOvers: UILabel!
    @IBOutlet weak var bowlRuns: UILabel!
    @IBOutlet weak var bowlWickets: UILabel!
    @IBOutlet weak var bowlBest: UILabel!
    @IBOutlet weak var bowlTenWic: UILabel!
    @IBOutlet weak var bowlFiveWic: UILabel!
    @IBOutlet weak var bowlEconomy: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonUtil.buttonRoundedCorners(buttons: [cdRankButton])
        profileImageView.layer.masksToBounds=true
        profileImageView.layer.borderWidth=1.5
        profileImageView.layer.borderColor = UIColor.white.cgColor
        profileImageView.layer.cornerRadius=profileImageView.bounds.width/2
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [cardView])
        addressLabel.text = currentAddress
        followersLabel.text = followers
        nameLabel.text = playerName
        cdRankButton.setTitle(cdrank, for: .normal)
        profileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(playerImageURL!)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
        getPlayerCareerDetails()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        outView2.addGestureRecognizer(tap)
        
        let imageTap = UITapGestureRecognizer(target: self, action: #selector(imageTapFunction))
        profileImageView.addGestureRecognizer(imageTap)
    }
    
    @objc func imageTapFunction(sender:UITapGestureRecognizer) {
        print("Image Tapped")
        let imageVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewProfileImageViewController") as? ViewProfileImageViewController
        imageVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        imageVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        imageVC?.imageUrl = playerImageURL
        self.present(imageVC!, animated: true, completion: nil)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        print("SHARE")
        let bounds = UIScreen.main.bounds
        UIGraphicsBeginImageContextWithOptions(bounds.size, true, 0.0)
        self.view.drawHierarchy(in: bounds, afterScreenUpdates: false)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let activityViewController = UIActivityViewController(activityItems: [img!], applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func getPlayerCareerDetails() {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_ID\": \"\(currentPlayerID!)\"}", mod: "PlayerCareer", actionType: "player-career") { (response) in
            print(response)
            if response as? String != "error" {
                let detail = (response as! NSDictionary)["XSCData"] as! NSDictionary
                let battingCareer = detail["CAREER_BATTING"] as! NSDictionary
                let bowlingCareer = detail["CAREER_BOWLING"] as! NSDictionary
                
                self.batMatches.text = "\(battingCareer["MATCHES"] as! String)"
                self.batInnings.text = "\(battingCareer["INNINGS"] as! String)"
                self.batNotOut.text = "\(battingCareer["NOT_OUT"] as! String)"
                self.batRuns.text = "\(battingCareer["RUNS"] as! String)"
                self.batHighest.text = "\(battingCareer["HIGH_SCORE"] as! String)"
                if battingCareer["HIGH_SCORE_STATUS"] as! String == "1" {
                        self.batHighest.text = "\(battingCareer["HIGH_SCORE"] as! String)*"
                }
                self.batHundreds.text = "\(battingCareer["100S"] as! String)"
                self.batFifty.text = "\(battingCareer["50S"] as! String)"
                self.batAverage.text = "\(battingCareer["AVERAGE"] as! String)"
                
                self.bowlMatches.text = "\(bowlingCareer["MATCHES"] as! String)"
                self.bowlInnings.text = "\(bowlingCareer["INNINGS"] as! String)"
                self.bowlOvers.text = "\(bowlingCareer["OVERS"] as! String)"
                self.bowlRuns.text = "\(bowlingCareer["RUNS"] as! String)"
                self.bowlEconomy.text = "\(bowlingCareer["ECONOMY"] as! String)"
                self.bowlWickets.text = "\(bowlingCareer["WICKETS"] as! String)"
                self.bowlTenWic.text = "\(bowlingCareer["10W"] as! String)"
                self.bowlBest.text = "\(bowlingCareer["BEST_WICKETS_TAKEN"] as! String)"
                self.bowlFiveWic.text = "\(bowlingCareer["5W"] as! String)"
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
        }
    }
}


