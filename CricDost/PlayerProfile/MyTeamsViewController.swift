//
//  MyTeamsViewController.swift
//  CricDost
//
//  Created by Jit Goel on 6/5/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class MyTeamsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, ShowsAlert {
    
    @IBOutlet weak var chooseTeamCardView: UIView!
    @IBOutlet weak var selectTeamColectionView: UICollectionView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var selectPlayerCardView: UIView!
    @IBOutlet weak var selectPlayerCollectionView: UICollectionView!
    @IBOutlet weak var playerSelectedChallengeButton: UIButton!
    @IBOutlet weak var numOfSelectedPlayersLabel: UILabel!
    @IBOutlet weak var clearSelectedPlayersList: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadingViewPreventUserInteraction: UIView!
    var reloadOpenMatch = false
    var reloadTeamMatch = false
    var reloadPlayerMatch = false
    var reloadOpenMatchDetail = false
    var reloadSearchMatch = false
    var currentMatchID: String?
    var showTeamsOnly = false
    var currentTeamID: String?
    var currentPlayerID: String?
    var teamList: NSArray = []
    var _selectedCells : NSMutableArray = []
    var _selectedPlayers : [String] = []
    var myPlayerList: NSArray = []
    @IBOutlet weak var dismissView: UIView!
    var totalPlayer: Int = 25
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [chooseTeamCardView, selectPlayerCardView])
        CommonUtil.buttonRoundedCorners(buttons: [cancelButton, playerSelectedChallengeButton])
        
        setCollectionViewLayout(card: selectPlayerCardView, collectionView: selectPlayerCollectionView, center: false)
        
        selectPlayerCollectionView.allowsMultipleSelection = true
        
        if teamList.count == 1 {
            print("MyTeams - TEST")
            setCollectionViewLayout(card: chooseTeamCardView, collectionView: selectTeamColectionView, center: false)
        } else {
            setCollectionViewLayout(card: chooseTeamCardView, collectionView: selectTeamColectionView, center: false)
        }
        
        let clearAll = UITapGestureRecognizer(target: self, action: #selector(clearAllTapFunction))
        clearSelectedPlayersList.addGestureRecognizer(clearAll)
        
        let outerViewTap = UITapGestureRecognizer(target: self, action: #selector(outerViewFunction))
        dismissView.addGestureRecognizer(outerViewTap)
        
        if !showTeamsOnly {
            getMatchDetail()
        }
       
    }
    
    @objc func outerViewFunction(sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearAllTapFunction(_ sender: Any) {
        print("Clear All")
        _selectedPlayers.removeAll()
        _selectedCells.removeAllObjects()
        selectPlayerCollectionView.reloadData()
        numOfSelectedPlayersLabel.text = "\(1) Selected"
        for player in myPlayerList {
            if (player as! NSDictionary)["IS_TEAM_OWNER"] as! String == "true" {
                _selectedPlayers.append((player as! NSDictionary)["PLAYER_ID"] as! String)
                _selectedPlayers = Array(Set(_selectedPlayers))
                numOfSelectedPlayersLabel.text = "\(1) Selected"
            }
        }
    }
    
    @IBAction func challengeAfterPlayerSelectedButtonClick(_ sender: Any) {
        if _selectedPlayers.count != 0 {
            activityIndicator.startAnimating()
            loadingViewPreventUserInteraction.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\": \"\(currentMatchID!)\",\"TEAM_ID\":\"\(currentTeamID!)\",\"PLAYERS\":\(_selectedPlayers)}", mod: "Match", actionType: "interest-match") { (response: Any) in
                print(response)
                if response as? String != "error" {
                    if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                        self.dismiss(animated: true, completion: {
                            if self.reloadPlayerMatch {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadMacthListFunction"), object: nil)
                            }
                            if self.reloadTeamMatch {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadTeamMacthListFunction"), object: nil)
                            }
                            
                            if self.reloadOpenMatch {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadOpenMacthListFunction"), object: nil)
                            }
                            
                            if self.reloadOpenMatchDetail {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadOpenMacthDetailListFunction"), object: nil)
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadOpenMacthListFunction"), object: nil)
                            }
                            
                            if self.reloadSearchMatch {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getDefaultSearchResult"), object: nil)
                            }
                            
                        })
                    } else {
                        self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String )
                    }
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
                self.loadingViewPreventUserInteraction.isHidden = true
            }
        } else {
            self.showAlert(title: "No Player Selected", message: "Please at least one player")
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == selectTeamColectionView {
            return teamList.count
        } else {
            return myPlayerList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == selectTeamColectionView {
            let team = teamList[indexPath.section] as! NSDictionary
            
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "selectTeam", for: indexPath) as! SelectTeamForChallengeCollectionViewCell
            CommonUtil.imageRoundedCorners(imageviews: [cell1.teamImageView])
            cell1.teamImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(team["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            cell1.teamNameLabel.text = team["TEAM_NAME"] as? String
            
            return cell1
            
        } else {
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "selectPlayers", for: indexPath) as! SelectPlayerCollectionViewCell
            let player = myPlayerList[indexPath.section] as! NSDictionary
            CommonUtil.imageRoundedCorners(imageviews: [cell2.playerImageView])
            CommonUtil.buttonRoundedCorners(buttons: [cell2.tickButton])
            cell2.playerName.text = player["FULL_NAME"] as? String
            cell2.playerImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(player["IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "profile_pic_default"))
            if _selectedCells.contains(indexPath) {
                cell2.isSelected=true
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
                cell2.tickButton.isHidden=false
            } else{
                cell2.isSelected=false
                cell2.tickButton.isHidden=true
            }
            
            if player["IS_TEAM_OWNER"] as! String == "true" {
                cell2.isSelected=true
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
                cell2.tickButton.isHidden=false
                _selectedCells.add(indexPath)
                _selectedPlayers.append(player["PLAYER_ID"] as! String)
                _selectedPlayers = Array(Set(_selectedPlayers))
                numOfSelectedPlayersLabel.text = "\(_selectedPlayers.count) Selected"
            }
            return cell2
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == selectTeamColectionView {
            let team = teamList[indexPath.section] as! NSDictionary
            currentTeamID = team["TEAM_ID"] as? String
            print(team)
            if showTeamsOnly {
                print("Sending request to team")
                if team["IS_REQUEST_SENT"] as! String == "true" {
                    self.showAlert(title: "Request", message: "Request already sent")
                } else if team["IS_PLAYER_JOINED"] as! String == "true" {
                    self.showAlert(title: "Request", message: "Already a member")
                } else {
                    self.activityIndicator.startAnimating()
                    self.loadingViewPreventUserInteraction.isHidden = false
                    CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"PLAYER_ID\": \"\(currentPlayerID!)\",\"TEAM_ID\":\"\(currentTeamID!)\",\"REQUEST_TYPE\":\"Sent\"}", mod: "Team", actionType: "send-request") { (response) in
                        print(response)
                        if response as? String != "error" {
                            let alert = UIAlertController(title: "Request", message: (response as! NSDictionary)["XSCMessage"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                                self.dismiss(animated: true, completion: nil)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        } else {
                            self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                        }
                        self.activityIndicator.stopAnimating()
                        self.loadingViewPreventUserInteraction.isHidden = true
                    }
                }
                
            } else {
                self.activityIndicator.startAnimating()
                self.loadingViewPreventUserInteraction.isHidden = false
                CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\": \"\(currentMatchID!)\",\"TEAM_ID\":\"\(currentTeamID!)\"}", mod: "Team", actionType: "challenge-match-team-players") { (response: Any) in
                    print(response)
                    if response as? String != "error" {
                        let players = (response as! NSDictionary)["XSCData"] as! NSArray
                        self.myPlayerList = players
                        
                        for player in self.myPlayerList {
                            if (player as! NSDictionary)["IS_TEAM_OWNER"] as! String == "true" {
                                self._selectedPlayers.append((player as! NSDictionary)["PLAYER_ID"] as! String)
                                self._selectedPlayers = Array(Set(self._selectedPlayers))
                                self.numOfSelectedPlayersLabel.text = "\(1) Selected"
                            }
                        }
                        
                        if self.myPlayerList.count == 1 {
                            self.setCollectionViewLayout(card: self.selectPlayerCardView, collectionView: self.selectPlayerCollectionView, center: false)
                        }
                        self.selectPlayerCollectionView.reloadData()
                        UIView.transition(with: self.chooseTeamCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                            self.chooseTeamCardView.isHidden = true
                        }, completion: nil)
                        UIView.transition(with: self.selectPlayerCardView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                            self.selectPlayerCardView.isHidden = false
                        }, completion: nil)
                    } else {
                        self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                    }
                    self.activityIndicator.stopAnimating()
                    self.loadingViewPreventUserInteraction.isHidden = true
                }
            }
            
        } else if collectionView == selectPlayerCollectionView {
            let player = myPlayerList[indexPath.section] as! NSDictionary
            print("numberof player====",player)
            var maxPlayer = 25
            if showTeamsOnly {
                maxPlayer = 25
            } else {
                maxPlayer = totalPlayer
            }
            
            if player["IS_TEAM_OWNER"] as! String != "true" {
                print(_selectedPlayers.count + 1, "<=" ,maxPlayer)
                if (_selectedPlayers.count + 1) <= maxPlayer {
                    if player["CAN_PLAYER_JOIN"] as! Int == 1 {
                        _selectedCells.add(indexPath)
                        _selectedPlayers.append(player["PLAYER_ID"] as! String)
                        _selectedPlayers = Array(Set(_selectedPlayers))
                        collectionView.reloadItems(at: [indexPath])
                        numOfSelectedPlayersLabel.text = "\(_selectedPlayers.count) Selected"
                    } else {
                        self.showAlert(message: player["CAN_PLAYER_JOIN_REASON"] as! String)
                    }
                } else {
                    self.showAlert(message: "Maximum number of players selected")
                }
            }
            
            print("Remaining player", totalPlayer)
            print("Selected player", _selectedPlayers)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == selectPlayerCollectionView {
            let player = myPlayerList[indexPath.section] as! NSDictionary
            print(player)
            _selectedCells.remove(indexPath)
            _selectedPlayers = _selectedPlayers.filter { $0 != player["PLAYER_ID"] as! String }
            _selectedPlayers = Array(Set(_selectedPlayers))
            numOfSelectedPlayersLabel.text = "\(_selectedPlayers.count) Selected"
            collectionView.reloadItems(at: [indexPath])
        }
    }
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setCollectionViewLayout(card: UIView, collectionView: UICollectionView, center: Bool) {
        
        if center {
            let screensize = card.bounds.size
            let cellwidth = floor(screensize.width * 0.8)
            let cellheight = floor(collectionView.bounds.height * 0.9)
            
            let insetX = (card.bounds.width - cellwidth) / 2.0
            let insetY = (collectionView.bounds.height - cellheight) / 2.0
            
            let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.itemSize = CGSize(width: cellwidth, height: cellheight)
            collectionView.contentInset = UIEdgeInsetsMake(insetY, insetX, insetY, insetX)
        } else {
            let screensize1 = card.bounds.size
            let cellwidth1 = floor(screensize1.width/3)
            let cellheight1 = floor(collectionView.bounds.height)
            
            let layout1 = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout1.itemSize = CGSize(width: cellwidth1, height: cellheight1)
            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }
    }
    
    func getMatchDetail() {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\": \"\(currentMatchID!)\"}", mod: "Match", actionType: "view-match-detail-dashboard") { (response) in
            print("TOTAL PLAYERS", response)
            
            let match = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
            self.totalPlayer = match["REQUIRED_PLAYERS"] as? Int ?? Int(match["REQUIRED_PLAYERS"] as! String)!
        }
    }
}
