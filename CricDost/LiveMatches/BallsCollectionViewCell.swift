//
//  BallsCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 6/26/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class BallsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var oversLabel: UILabel!
    @IBOutlet weak var runsButton: UIButton!
    @IBOutlet weak var extraLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        CommonUtil.buttonRoundedCorners(buttons: [runsButton])
    }
    
}
