//
//  GroupChatViewController.swift
//  CricDost
//
//  Created by Jit Goel on 7/25/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import XMPPFramework
import IQKeyboardManager

class GroupChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
    var groupID: String?
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var groupNameLabel: UILabel!
    @IBOutlet weak var messageTableView: UITableView!
    @IBOutlet weak var messageTextView: UITextView!
    var groupJIDPrefix: String?
    var password: String?
    var teamName: String?
    var userName: String?
    var messageList: NSMutableArray?
    var userChatHistoryBackup: [String]?
    @IBOutlet weak var keyboardHeightLayoutConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = CommonUtil.themeRed
        }
//        UIApplication.shared.statusBarStyle = .lightContent
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().layoutIfNeededOnUpdate = true
        
        sendButton.setImage(#imageLiteral(resourceName: "baseline_send_white_24pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        sendButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        messageTextView.textContainer.heightTracksTextView = true
        messageTextView.textContainer.widthTracksTextView = false
        messageTextView.isScrollEnabled = false
        messageTextView.layer.cornerRadius = 5.0
        
        messageTableView.transform = CGAffineTransform(rotationAngle: -(.pi))
        
        userName = UserDefaults.standard.object(forKey: CommonUtil.OPENFIRE_USERNAME) as? String
        password = UserDefaults.standard.object(forKey: CommonUtil.OPENFIRE_PASSWORD) as? String
        
        userChatHistoryBackup = UserDefaults.standard.object(forKey: userName!) as? [String] ?? []
        
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(self.keyboardNotification(notification:)),
//                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
//                                               object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshGroupChatList), name: NSNotification.Name(rawValue: "refreshGroupChatList"), object: nil)
        groupNameLabel.text = teamName
        CommonUtil.imageRoundedCorners(imageviews: [profileImageView])
        let matchInfo:[String: NSDictionary] = ["data": ["groupJID" : groupJIDPrefix!,"MSG": "HIIIIIIIIIIIIIIIIII"]]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "joinGroupChat"), object: nil, userInfo: matchInfo)

        if let tempNames: NSArray = UserDefaults.standard.object(forKey: groupJIDPrefix!) as? NSArray {
            messageList = tempNames.mutableCopy() as? NSMutableArray
        } else {
            messageList = []
        }
        UserDefaults.standard.set(0, forKey: "\(groupJIDPrefix!)_\(userName!)")
        print("Message History", messageList!)
        let url = UserDefaults.standard.object(forKey: "\(groupJIDPrefix!)_IMAGEURL") as? String ?? ""
        if url != "" {
            print("\(CommonUtil.BASE_URL)\(url)")
            profileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(url)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("Hello")
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendButtonClick(_ sender: Any) {
        if (messageTextView.text).trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            if messageList?.count == 0 {
                userChatHistoryBackup?.append(groupJIDPrefix!)
                UserDefaults.standard.set(userChatHistoryBackup, forKey: userName!)
            }
            let matchInfo:[String: NSDictionary] = ["data": ["SUBJECT":"", "groupJID" : groupJIDPrefix!,"MSG":  (messageTextView.text).trimmingCharacters(in: .whitespacesAndNewlines)]]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "sendMessageGroup"), object: nil, userInfo: matchInfo)
            
            messageTextView.text = ""
//            messageTextView.resignFirstResponder()
            UserDefaults.standard.set(0, forKey: "\(groupJIDPrefix!)_\(userName!)")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (messageList?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let msg = messageList![indexPath.row] as! NSDictionary
        print(msg)
        if (msg["Name"] as! String) != userName {
            let cell = tableView.dequeueReusableCell(withIdentifier: "receivedGroupCell", for: indexPath) as! GroupReceivedTableViewCell
            cell.receivedNameLabel.text = msg["nickName"] as? String
            cell.messageLabel.text = msg["message"] as? String
            cell.timeLabel.text = msg["time"] as? String
            cell.transform = CGAffineTransform(rotationAngle: .pi)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sendGroupCell", for: indexPath) as! GroupSendTableViewCell
            cell.transform = CGAffineTransform(rotationAngle: .pi)
            cell.timeLabel.text = msg["time"] as? String
            cell.messageLabel.text = msg["message"] as? String
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        messageTextView.resignFirstResponder()
    }
    
    @objc func refreshGroupChatList(_ notification: NSNotification) {
        print("Refreshing group chat")
        print("Received notification", notification.userInfo![AnyHashable("data")] as! NSDictionary)
        if let tempNames: NSArray = UserDefaults.standard.object(forKey: groupJIDPrefix!) as? NSArray {
            messageList = tempNames.mutableCopy() as? NSMutableArray
        } else {
            messageList = []
        }
        UserDefaults.standard.set(0, forKey: "\(groupJIDPrefix!)_\(userName!)")
        messageTableView.reloadData()
        print("ReFreshing Chat")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant = 0.0
            } else {
                self.keyboardHeightLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
}
