//
//  WinnerTeamViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 9/18/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class WinnerTeamViewController: UIViewController {

    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet weak var matchTimerLabel: UILabel!
    @IBOutlet weak var moreOptionButton: UIButton!
    @IBOutlet weak var winnerTeamNameLabel: UILabel!
    @IBOutlet weak var winnerTeamScoreLabel: UILabel!
    @IBOutlet weak var clapThemButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        CommonUtil.roundedUIViewCornersWithShade(uiviews: [viewHolder])
        CommonUtil.buttonRoundedCorners(buttons: [clapThemButton])
    }
    @IBAction func clapThemButtonClick(_ sender: Any) {
        print("winner clap them button clicked")
    }
    
   
    @IBAction func moreOptionButtonClick(_ sender: Any) {
        print("winner option button clicked")
    }
    
}
