//
//  PlayersScoreViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 9/18/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class PlayersScoreViewController: UIViewController {

    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet weak var matchTimerLabel: UILabel!
    @IBOutlet weak var moreOptionButton: UIButton!
    @IBOutlet weak var onStrikerImageView: UIImageView!
    @IBOutlet weak var onStrikerPlayerNameLabel: UILabel!
    @IBOutlet weak var onStrikerScoreLabel: UILabel!
    @IBOutlet weak var offStrikerImageView: UIImageView!
    @IBOutlet weak var offStrikerPlayerNameLabel: UILabel!
    @IBOutlet weak var offStrikerScoreLabel: UILabel!
    @IBOutlet weak var bowlerImageView: UIImageView!
    @IBOutlet weak var bowlerPlayerNameLabel: UILabel!
    @IBOutlet weak var bowlerScoreLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        CommonUtil.roundedUIViewCornersWithShade(uiviews: [viewHolder])
        CommonUtil.imageRoundedCorners(imageviews: [onStrikerImageView,offStrikerImageView,bowlerImageView])
    }

   
    @IBAction func moreOptionButtonClick(_ sender: Any) {
        print("Player score more option button clicked")
    }
}
