//
//  WinnerCupResultViewController.swift
//  CricDost
//
//  Created by Jit Goel on 8/14/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class WinnerCupResultViewController: UIViewController {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var dismissView: UIView!
    var result: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [cardView])
        let outerViewTap = UITapGestureRecognizer(target: self, action: #selector(outerViewFunction))
        dismissView.addGestureRecognizer(outerViewTap)
        
        resultLabel.text = result
    }
    
    @objc func outerViewFunction(sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
}
