//
//  ReceivedMessageTableViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 7/18/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ReceivedMessageTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        CommonUtil.imageRoundedCorners(imageviews: [profileImage])
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [viewHolder])
//        viewHolder.layer.cornerRadius = 5
//        viewHolder.layer.masksToBounds = true
        
        profileImage.layer.borderWidth = 1.5
        profileImage.layer.borderColor = UIColor.white.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }

}
