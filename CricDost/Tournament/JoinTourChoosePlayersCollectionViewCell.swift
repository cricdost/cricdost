//
//  JoinTourChoosePlayersCollectionViewCell.swift
//  CricDost
//
//  Created by JIT GOEL on 9/1/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class JoinTourChoosePlayersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var joinTourChoosePlayerProfileImageView: UIImageView!
    @IBOutlet weak var joinTourPlayerNameLabel: UILabel!
    @IBOutlet weak var joinTourPlayerSkillsLabel: UILabel!
    @IBOutlet weak var joinTourPlayersTeamNameLabel: UILabel!
    @IBOutlet weak var joinTourGreenTickImage: UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        CommonUtil.imageRoundedCorners(imageviews: [joinTourChoosePlayerProfileImageView, joinTourGreenTickImage])
    }
    
}
