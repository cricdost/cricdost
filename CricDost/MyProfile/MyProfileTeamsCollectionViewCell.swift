//
//  MyProfileTeamsCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 6/13/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class MyProfileTeamsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var teamImageView: UIImageView!
    @IBOutlet weak var adminButton: UIButton!
    @IBOutlet weak var teamNameButton: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var chatButton: UIButton!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        CommonUtil.buttonRoundedCorners(buttons: [adminButton, chatButton])
        CommonUtil.imageRoundedCorners(imageviews: [teamImageView])
    }
}
