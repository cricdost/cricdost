//
//  WicketCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 7/4/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class WicketCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var playerImage: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var greenTick: UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        CommonUtil.imageRoundedCorners(imageviews: [playerImage, greenTick])
    }
}
