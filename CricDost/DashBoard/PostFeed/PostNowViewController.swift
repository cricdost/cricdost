//
//  PostNowViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 10/15/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class PostNowViewController: UIViewController, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ShowsAlert{

    @IBOutlet weak var profileImgeView: UIImageView!
    @IBOutlet weak var postTextView: UITextView!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var postNowButton: UIButton!
    var rightButton  : UIButton?
    var imagePicker = UIImagePickerController()
    var imageData:Data?
    var image: UIImage?
    var openCam = false
    var editPost = false
    var postID = "0"
    var commentPage = false
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        postTextView.delegate = self
        imagePicker.delegate = self

        postTextView.text = "Post something about cricket"
        postTextView.textColor = UIColor.lightGray
        postTextView.layer.cornerRadius = 5.0
        CommonUtil.imageRoundedCorners(imageviews: [profileImgeView])
        CommonUtil.buttonRoundedCorners(buttons: [cancelButton,postNowButton])
        
        self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "baseline_arrow_back_white_24pt_1x.png")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "baseline_arrow_back_white_24pt_1x.png")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        rightButton?.setBackgroundImage(#imageLiteral(resourceName: "baseline_more_vert_white_24pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        rightButton?.tintColor = UIColor.white
        rightButton?.addTarget(self, action: #selector(moreOption), for: .touchUpInside)
        let rightBarButtomItem = UIBarButtonItem(customView: rightButton!)
        navigationItem.rightBarButtonItem = rightBarButtomItem
        
        profileImgeView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(UserDefaults.standard.object(forKey: CommonUtil.IMAGE_URL) as? String ?? "")"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default.png"))
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.postImageTapped))
        postImageView.isUserInteractionEnabled = true
        postImageView.addGestureRecognizer(tapGesture)
        
        if editPost {
            print("edit post" ,editPost)
            postNowButton.setTitle("Update Now", for: .normal)
            getPostDetails(postID: postID)
        } else {
            postNowButton.setTitle("Post Now", for: .normal)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if openCam {
            if image != nil {
                self.postImageView.image = image
                self.postImageView.clipsToBounds = true
                self.postImageView.contentMode = .scaleAspectFit
            }
        }
    }
    
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        if(text == "\n") {
//            textView.resignFirstResponder()
//            return false
//        }
//        return true
//    }
    
    func textViewDidChange(_ textView: UITextView) {
        let maxHeight: CGFloat = 100.0
        let minHeight: CGFloat = 50.0
        textViewHeightConstraint.constant = min(maxHeight, max(minHeight, textView.contentSize.height))
        textView.isScrollEnabled = true
        self.view.layoutIfNeeded()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Post something about cricket"
            textView.textColor = UIColor.lightGray
        }
    }
    
    
    
    @objc func moreOption() {
        print("more option tapped")
    }
    
    @objc func postImageTapped() {
        print("Image Tapped")
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .popover
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            print("IPAD")
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            alert.popoverPresentationController?.permittedArrowDirections = .down
            
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        openCam = false
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image_data = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageData = UIImageJPEGRepresentation(image_data.updateImageOrientionUpSide()!, 0.025)
            self.dismiss(animated: true, completion: nil)
            self.postImageView.image = image_data
//            self.postImageView.layer.cornerRadius = self.postImageView.frame.height/2
            self.postImageView.clipsToBounds = true
            self.postImageView.contentMode = .scaleAspectFit
        }
    }
    
    @IBAction func postNowButtonClick(_ sender: Any) {
        print("post button click")
        
        if postTextView.text == "Post something about cricket" && imageData == nil {
            
        } else {
            if postTextView.text == "Post something about cricket" {
                postTextView.text = ""
            }
            activityIndicator.startAnimating()
            postNowButton.isEnabled = false
            if imageData != nil {
                print("image data selected" , imageData!)
                myImageUploadRequest(image: imageData!)
            } else if image != nil {
                print("image sent from previous page")
                myImageUploadRequest(image: UIImageJPEGRepresentation(self.image!.updateImageOrientionUpSide()!, 0.025)!)
            } else {
                print("no image")
                noImageSelectedUpload()
            }
        }
        
    }
    
    @IBAction func cancelButtonClick(_ sender: Any) {
        print("post button click")
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func noImageSelectedUpload() {
        var subaction = ""
        
        if editPost {
            subaction = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"POST\":\"\(postTextView.text!)\",\"IMAGE\":\"\",\"POST_ID\":\"\(postID)\"}"
        } else {
            subaction = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"POST\":\"\(postTextView.text!)\",\"IMAGE\":\"\"}"
        }
        
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: subaction, mod: "CricSpace", actionType: "add-post", callback: {(response: Any) -> Void in
            if response as? String != "error" {
                print(response)
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
//                    let alert = UIAlertController(title: "Message", message: (response as! NSDictionary)["XSCMessage"] as? String, preferredStyle: UIAlertControllerStyle.alert)
//                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
//
//                    }))
//
//                    self.present(alert, animated: true, completion: nil)
                    
                    if self.editPost {
                        
                        if self.commentPage {
                            let post:[String: Any] = ["commentPageEdit":self.postID]
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "editPostRefreshData"), object: nil, userInfo: post)
                            
                        } else {
                            let post:[String: Any] = ["post":self.postID]
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "editPostDetailRefreshData"), object: nil, userInfo: post)
                        }
                        self.editPost = false
                    } else {
                        let post:[String: Any] = ["post":(response as! NSDictionary)["XSCData"]!]
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newPostedAddedRefreshData"), object: nil, userInfo: post)
                    }
                    
                    if self.commentPage {
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                    
                } else {
                    self.showAlert(title: "OOPS", message: (response as! NSDictionary)["XSCMessage"] as! String)
                    print((response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "OOPS", message: "Something went wrong")
            }
            self.activityIndicator.stopAnimating()
            self.postNowButton.isEnabled = true
        })
    }
    
    func myImageUploadRequest(image: Data)
    {
        let myUrl = NSURL(string: CommonUtil.BASE_URL);
        //let myUrl = NSURL(string: "http://www.boredwear.com/utils/postImage.php");
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        
        request.httpMethod = "POST";
        
        var subaction = ""
        
        if editPost {
            subaction = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"POST\":\"\(postTextView.text!)\",\"POST_ID\":\"\(postID)\"}"
        } else {
            subaction = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"POST\":\"\(postTextView.text!)\"}"
        }
        
        let param = [
            "app" : "CRICDOST",
            "mod": "CricSpace",
            "actionType" : "add-post",
            "subAction": subaction
            ] as [String : Any]
        print(param)
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        if(imageData==nil)  { return; }
        
        request.httpBody = createBodyWithParameters(parameters: param , filePathKey: "IMAGE", imageDataKey: image as NSData, boundary: boundary) as Data
        
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                self.showAlert(title: "Error", message: "Request TimeOut")
                print("error=\(String(describing: error))")
                return
            }
            
            // You can print out response object
            print("******* response = \(String(describing: response))")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                print(json as Any)
                DispatchQueue.main.async {
                    if json!["XSCStatus"] as! Int == 0 {
//                        let alert = UIAlertController(title: "Message", message: json!["XSCMessage"] as? String, preferredStyle: UIAlertControllerStyle.alert)
//                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
//
//                        }))
//
//                        self.present(alert, animated: true, completion: nil)
                        
                        if self.editPost {
                            if self.commentPage {
                                let post:[String: Any] = ["commentPageEdit":self.postID]
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "editPostRefreshData"), object: nil, userInfo: post)
                            } else {
                                let post:[String: Any] = ["post":self.postID]
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "editPostDetailRefreshData"), object: nil, userInfo: post)
                            }
                            self.editPost = false
                        } else {
                            let post:[String: Any] = ["post":json!["XSCData"]!]
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newPostedAddedRefreshData"), object: nil, userInfo: post)
                        }
                        if self.commentPage {
                            self.navigationController?.popViewController(animated: true)
                        } else {
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                        
                    } else {
                        self.showAlert(title: "Oops", message: json!["XSCMessage"] as! String)
                    }
                    self.activityIndicator.stopAnimating()
                    self.postNowButton.isEnabled = true
                }
            }catch
            {
                print(error)
            }
        }
        task.resume()
    }
    
    func createBodyWithParameters(parameters: [String: Any]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func getPostDetails(postID: String) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"POST_ID\":\"\(postID)\",\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "CricSpace", actionType: "get-post-details") { (response) in
            print("post details" ,response)
            
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    let postDetails = ((response as! NSDictionary)["XSCData"] as! NSDictionary)
                    self.postImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(postDetails["POST_IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "upload-pic-25"))
                    self.postTextView.text = postDetails["POST"] as? String
                    self.postTextView.textColor = UIColor.black
                } else {
                    print((response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                print("Something went wrong! Please try again")
            }
        }
    }
    
    
}
