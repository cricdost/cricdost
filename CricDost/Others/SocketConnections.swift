//
//  SocketConnections.swift
//  CricDost
//
//  Created by Jit Goel on 6/25/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import SocketIO

class SocketConnectionsclass {
    
    
    
    static var socket: SocketIOClient?
    static var socketManager: SocketManager?

    
    static func connectSocket(userId: String, deviceKey: String) {
        print(deviceKey,userId)
        socketManager = SocketManager(socketURL:URL(string: CommonUtil.SOCKET_URL)! ,
                                      config: [ .log(false),
                                                .connectParams(["user_id": userId, "device_key": deviceKey])
            ])
        
        socket = socketManager?.defaultSocket
        
        socket?.on(clientEvent: .connect) { data, ack in
            print("socket connected", data)
            self.socket?.emit("get_my_upcoming_match", "")
        }
        
        socket?.on("match_fixed") { data, ack in
            print("match_fixed", data)
            self.socket?.emit("get_my_upcoming_match", "")
        }
        
        socket?.on(clientEvent: .disconnect) { data, ack in
            print("Socket disconnected")
        }
        
        socket?.on("my_upcoming_match") { data, ack in
            print("my_upcoming_match", data)
            let upcoming:[String: Any] = ["upcoming": data]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpcomingMatch"), object: nil, userInfo: upcoming)
        }
        
        socket?.on("match_step") { data, ack in
            print("match_step", data)
            let matchAdminDetail:[String: Any] = ["matchAdminDetail": data]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "matchAdminDetail"), object: nil, userInfo: matchAdminDetail)
        }
        
        socket?.on("full_scorecard") { data, ack in
            print("full_scorecard", data)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshScore"), object: nil)
        }
        
        socket?.on("Live_score") { data, ack in
            print("LIVE_Score bowler", data)
//            self.socket?.emit("get_score", match_id)
        }
        
        socket?.on("live_score") { data, ack in
            print("live_score", data)
            let matchInfo:[String: Any] = ["data": data]
            
            if CommonUtil.NewLiveMatchFlow {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateLiveScoreNew"), object: nil, userInfo: matchInfo)
                NotificationCenter.default.post(name: NSNotification.Name("refreshScorerPage"), object: nil)
            } else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateLiveScore"), object: nil, userInfo: matchInfo)
            }
        }
        
        socket?.on("match_status_update") { data, ack in
            print("match_status_update", data)
            let matchAdminDetail:[String: Any] = ["matchAdminDetail": data]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "matchAdminDetail"), object: nil, userInfo: matchAdminDetail)
        }
        
        self.socket?.connect()
    }
    
    static func getMyUpcomingMatches() {
        print("get my upcoming matches")
        self.socket?.emit("get_my_upcoming_match", "")
    }
    
    static func matchStatusEmit(match_id: String) {
        let x = ["match_id": match_id] as [String : Any]
        self.socket?.emit("get_match_status", x)
    }
    
    static func matchGetStatus(match_id: String) {
        print("Getting match status")
        self.socket?.emit("get_score", match_id)
    }
    
    static func matchStatusUpdate_NewFlow(param: [String: Any]) {
//        let x = ["match_id": match_id] as [String : Any]
        self.socket?.emit("match_status_update", param)
    }
    
    static func clapThemEmit(param: [String: Any]) {
        //        let x = ["match_id": match_id] as [String : Any]
        self.socket?.emit("clap_them", param)
    }
    
    static func matchScoreUpdateEmit(match_id: String) {
        self.socket?.emit("score_updated", match_id)
        self.socket?.emit("scorer_changed", match_id)
    }
    
    static func joinMatchScoreBoard(match_id: String) {
        print("match_id_\(match_id)")
        self.socket?.emit("join", "match_id_\(match_id)")
        self.socket?.emit("score_updated", match_id )
    }
    
    static func disconnectSocket() {
        self.socket?.disconnect()
    }

}

