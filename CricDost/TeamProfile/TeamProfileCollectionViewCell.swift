//
//  TeamProfileCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 6/7/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class TeamProfileCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var rankButton: UIButton!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var nameAgeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var follwerLabel: UILabel!
    @IBOutlet weak var matchesLabel: UILabel!
    @IBOutlet weak var winnerLabel: UILabel!
    @IBOutlet weak var runnerLabel: UILabel!
    @IBOutlet weak var oneStar: UIButton!
    @IBOutlet weak var twoStar: UIButton!
    @IBOutlet weak var threeStar: UIButton!
    @IBOutlet weak var fourStar: UIButton!
    @IBOutlet weak var fiveStar: UIButton!
    @IBOutlet weak var playersButton: UIButton!
    @IBOutlet weak var matchesButton: UIButton!
    @IBOutlet weak var optionMenuButton: UIButton!
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var requestButton: UIButton!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        CommonUtil.buttonRoundedCorners(buttons: [rankButton,followButton])
        CommonUtil.imageRoundedCorners(imageviews: [profileImageView])
        CommonUtil.addLeftBorder(color: UIColor(red:0.80, green:0.80, blue:0.80, alpha:1.0), width: 1.0, buttons: [matchesButton, requestButton])
        optionMenuButton.setImage(#imageLiteral(resourceName: "baseline_more_vert_white_24pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        optionMenuButton.tintColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
    }
    
    func setRating(rating: String) {
        
        switch rating {
        case "1": for star in [oneStar] {
            star?.setImage(#imageLiteral(resourceName: "ratingstar"), for: .disabled)
        }
            break
        case "2": for star in [oneStar,twoStar] {
            star?.setImage(#imageLiteral(resourceName: "ratingstar"), for: .disabled)
        }
            break
        case "3": for star in [oneStar,twoStar,threeStar] {
            star?.setImage(#imageLiteral(resourceName: "ratingstar"), for: .disabled)
        }
            break
        case "4": for star in [oneStar,twoStar,threeStar,fourStar] {
            star?.setImage(#imageLiteral(resourceName: "ratingstar"), for: .disabled)
        }
            break
        case "5": for star in [oneStar,twoStar,threeStar,fourStar,fiveStar] {
            star?.setImage(#imageLiteral(resourceName: "ratingstar"), for: .disabled)
        }
            break
        default:
            break
        }
    }
    
}
