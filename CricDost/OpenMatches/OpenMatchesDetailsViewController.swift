//
//  OpenMatchesDetailsViewController.swift
//  CricDost
//
//  Created by Jit Goel on 6/20/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class OpenMatchesDetailsViewController: PullUpController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ShowsAlert {

    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet weak var matchInfoLabel: UILabel!
    @IBOutlet weak var teamAImageView: UIImageView!
    @IBOutlet weak var teamBImageView: UIImageView!
    @IBOutlet weak var teamANameLabel: UILabel!
    @IBOutlet weak var teamBNameLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var groundNameButton: UIButton!
    @IBOutlet weak var numberOfPalyersButton: UIButton!
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var teamAPlayerNameLabel: UILabel!
    @IBOutlet weak var teamBPlayerNameLabel: UILabel!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var optionButton: UIButton!
    @IBOutlet weak var dateTimeEditButton: UIButton!
    @IBOutlet weak var addressEditButton: UIButton!
    @IBOutlet weak var numberofPlayerEditButton: UIButton!
    @IBOutlet weak var titleView: UIView!
    var matchInfo: NSDictionary?
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var challengersCount: UILabel!
    @IBOutlet weak var challengeButton: UIButton!
    @IBOutlet weak var vsLabel: UILabel!
    var teamAPlayerList: NSArray = []
    var teamBPlayerList: NSArray = []
    var currentMatchID: String?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var preventUserInteraction: UIView!
    @IBOutlet weak var noPlayerTeamALabel: UILabel!
    @IBOutlet weak var noPlayerTeamBLabel: UILabel!
    @IBOutlet weak var teamACollectionView: UICollectionView!
    @IBOutlet weak var teamBCollectionView: UICollectionView!
    var matchName: String?
    var matchJID: String?
    var matchTime: String?
    var matchDate: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonUtil.buttonRoundedCorners(buttons: [joinButton,chatButton, challengeButton])
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [viewHolder])
        CommonUtil.imageRoundedCorners(imageviews: [teamAImageView, teamBImageView])
        titleView.clipsToBounds = true
        titleView.layer.cornerRadius = 5
        if #available(iOS 11.0, *) {
            titleView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            let path = UIBezierPath(roundedRect: titleView.bounds,
                                    byRoundingCorners: [.topLeft,.topRight],
                                    cornerRadii: CGSize(width: 5, height: 5))
            let maskLayer = CAShapeLayer()
            maskLayer.path = path.cgPath
            titleView.layer.mask = maskLayer
        }
        NotificationCenter.default.addObserver(self, selector: #selector(reloadOpenMacthDetailListFunction), name: NSNotification.Name(rawValue: "reloadOpenMacthDetailListFunction"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(dismissMatchDetailView), name: NSNotification.Name(rawValue: "dismissMatchDetailView"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateMatchLocation), name: NSNotification.Name(rawValue: "updateMatchLocation"), object: nil)
        
        
        dateTimeEditButton.setImage(#imageLiteral(resourceName: "baseline_edit_black_24pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        dateTimeEditButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        addressEditButton.setImage(#imageLiteral(resourceName: "baseline_edit_black_24pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        addressEditButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        numberofPlayerEditButton.setImage(#imageLiteral(resourceName: "baseline_edit_black_24pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        numberofPlayerEditButton.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        loadData()
    }
    
    @objc func dismissMatchDetailView() {
        print("dismissMatchDetailView")
        self.pullUpControllerMoveToVisiblePoint(self.pullUpControllerMiddleStickyPoints[1], completion: nil)
    }
    
    @IBAction func optionButtonClick(_ sender: Any) {
        let alert = UIAlertController(title: "Cancel Match", message: nil, preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .popover
        alert.addAction(UIAlertAction.init(title: "No", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in
            self.cancelMatch()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func cancelMatch() {
        self.activityIndicator.startAnimating()
        self.preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\":\"\(currentMatchID!)\"}", mod: "Match", actionType: "cancel-match") { (response) in
            print(response)
            if response as? String != "error" {
                let message = (response as! NSDictionary)["XSCMessage"] as! String
                self.showAlert(title: "Match", message: message)
                self.pullUpControllerMoveToVisiblePoint(self.pullUpControllerMiddleStickyPoints[1], completion: nil)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        }
    }
    
    @IBAction func editDateTimeButtonClick(_ sender: Any) {
     
        print("Edit date time")
        let editDateTimeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditMatchTimeViewController") as? EditMatchTimeViewController
        editDateTimeVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        editDateTimeVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        editDateTimeVC?.dateString = matchDate
        editDateTimeVC?.timeString = matchTime
        editDateTimeVC?.matchID = currentMatchID
        self.present(editDateTimeVC!, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        UserDefaults.standard.set(true, forKey: CommonUtil.MATCHDETAILUPDATELOCATION)
    }
    
    @IBAction func addressEditButtonClick(_ sender: Any) {
        self.performSegue(withIdentifier: "matchDetail_locationSelect", sender: self)
    }
    
    @objc func updateMatchLocation() {
        print("Setting location on view")
        getAddressFromLatLon(pdblLatitude: UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double, withLongitude: UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double)
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
        let lat: Double = pdblLatitude
        
        let lon: Double = pdblLongitude
        print(lat,lon)
        CommonUtil.getAddressForLatLng(viewcontroller: self, latitude: String(lat), longitude: String(lon), callback: {
            (address: String) -> Void in
            print("GEOCODING \(address)")
            self.activityIndicator.startAnimating()
            self.preventUserInteraction.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"ADDRESS\":\"\(self.removeSpecialCharsFromString(text: address))\",\"LATITUDE\":\"\(lat)\",\"LONGITUDE\":\"\(lon)\",\"MATCH_ID\":\"\(self.currentMatchID!)\"}", mod: "Match", actionType: "update-match-location", callback: { (response) in
                if response as? String != "error" {
                    if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                        print(response)
                        self.showAlert(title: "Match", message: (response as! NSDictionary)["XSCMessage"] as! String)
                        self.refreshMatchDetail()
                    } else {
                        self.showAlert(title: "Oops", message: (response as! NSDictionary)["XSCMessage"] as! String)
                    }
                } else {
                    self.showAlert(title: "Oops", message: "Something went wrong")
                }
                self.activityIndicator.stopAnimating()
                self.preventUserInteraction.isHidden = true
            })
            
        })
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_")
        return String(text.filter {okayChars.contains($0) })
    }
    
    @IBAction func numberOfPlayerEditButtonClick(_ sender: Any) {
        print("Edit location")
        let editPlayersVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditMatchTeamPlayerViewController") as? EditMatchTeamPlayerViewController
        editPlayersVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        editPlayersVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        editPlayersVC?.currentMatchID = self.currentMatchID
        self.present(editPlayersVC!, animated: true, completion: nil)
    }
    
    @IBAction func chatButtonClick(_ sender: Any) {
        print("Chat")
        let groupChatVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GroupChatViewController") as? GroupChatViewController
        groupChatVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        groupChatVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        groupChatVC?.groupJIDPrefix = matchJID
        groupChatVC?.teamName = "Match \(matchName!)"
        self.present(groupChatVC!, animated: true, completion: nil)
    }
    
    @objc func reloadOpenMacthDetailListFunction() {
        refreshMatchDetail()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == teamACollectionView {
            return teamAPlayerList.count
        } else {
            return teamBPlayerList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == teamACollectionView {
            let player = (teamAPlayerList[indexPath.section] as! NSDictionary)
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "teamPlayers", for: indexPath) as! OpenMatchTeamPlayersCollectionViewCell
            cell.profileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(player["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
            cell.playerName.text = player["FULL_NAME"] as? String
            return cell
        } else {
            let player = teamBPlayerList[indexPath.section] as! NSDictionary
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "teamBPlayerCell", for: indexPath) as! OpenMatchTeamBPlayersCollectionViewCell
            cell.profileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(player["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
            cell.playerName.text = player["FULL_NAME"] as? String
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10)
        return CGSize(width: collectionView.frame.width/4, height: collectionView.frame.height)
    }
    
    func loadData(){
        let match = (matchInfo!["XSCData"] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        let teamA = match["TEAM_A"] as! NSDictionary
        let teamB = match["TEAM_B"] as! NSDictionary
        
        matchName = match["DATE"] as? String
        matchJID = match["OPENFIRE_USERNAME"] as? String
        
        currentMatchID = match["MATCH_ID"] as? String
        matchInfoLabel.text = "\(match["TITLE"] as! String)"
        matchDate = match["DATE"] as? String
        matchTime = match["TIME"] as? String
        dateTimeLabel.text = "\(match["DATE"] as! String) - \(match["TIME"] as! String)"
        groundNameButton.setImage(#imageLiteral(resourceName: "baseline_location_on_black_24pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        groundNameButton.tintColor = CommonUtil.themeRed
        addressLabel.text = match["ADDRESS"] as? String
        numberOfPalyersButton.setTitle("\(match["AVAILABLE_PLAYERS"] as? String ?? String(match["AVAILABLE_PLAYERS"] as! Int) )/\(match["TOTAL_PLAYERS"] as? String ?? String(match["TOTAL_PLAYERS"] as! Int)) Players", for: .normal)
        teamANameLabel.text = teamA["TEAM_NAME"] as? String
        teamAPlayerNameLabel.text = teamA["TEAM_NAME"] as? String
        teamAImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamA["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
        
        teamAPlayerList = teamA["PLAYERS"] as! NSArray
        
        if match["CAN_CHALLENGE"] as! Int == 1 {
            challengeButton.isHidden = false
            vsLabel.isHidden = true
            teamBNameLabel.isHidden = true
            teamBImageView.isHidden = true
            challengersCount.isHidden = true
        } else {
            challengeButton.isHidden = true
            vsLabel.isHidden = false
            teamBNameLabel.isHidden = false
            teamBImageView.isHidden = false
            challengersCount.isHidden = false
        }
        
        if  teamB["POSITION_STATUS"] as! Int == 0 {
            teamBImageView.image = nil
            teamBNameLabel.text = "Challengers"
            teamBPlayerNameLabel.text = "No Opponent"
            noPlayerTeamBLabel.isHidden = false
            challengersCount.isHidden = false
            challengersCount.text = match["INTEREST_COUNT"] as? String
        } else {
            challengersCount.isHidden = true
            teamBImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamB["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            teamBNameLabel.text = teamB["TEAM_NAME"] as? String
            noPlayerTeamBLabel.isHidden = true
            teamBPlayerNameLabel.text = teamB["TEAM_NAME"] as? String
            teamBPlayerList = teamB["PLAYERS"] as! NSArray
        }
        
        if match["CAN_JOIN"] as! Int == 1 {
            joinButton.isHidden = false
        } else {
            joinButton.isHidden = true
        }
        
        if teamAPlayerList.count == 0 {
            noPlayerTeamALabel.isHidden = false
        } else {
            noPlayerTeamALabel.isHidden = true
        }
        
        if teamBPlayerList.count == 0 {
            noPlayerTeamBLabel.isHidden = false
        } else {
            noPlayerTeamBLabel.isHidden = true
        }
        
        if match["CAN_CHAT"] as! Int == 0 {
            chatButton.isHidden = true
        } else {
            chatButton.isHidden = false
        }
        
        if match["IS_ADMIN"] as! Int == 0 {
            optionButton.isHidden = true
            dateTimeEditButton.isHidden = true
            addressEditButton.isHidden = true
            numberofPlayerEditButton.isHidden = true
        } else {
            optionButton.isHidden = false
            dateTimeEditButton.isHidden = false
            addressEditButton.isHidden = false
            numberofPlayerEditButton.isHidden = false
        }
    }
    
    
    @IBAction func challengeButtonTapped(_ sender: UIButton) {
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Match", actionType: "create-match-team-list") { (response: Any) in
            if response as? String != "error" {
                print(response)
                
                let teams = (response as! NSDictionary)["XSCData"] as! NSArray
                
                if teams.count == 0 {
                    let alert = UIAlertController(title: "No Teams", message: "You don't own a team. Create a team now?", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Create", style: UIAlertActionStyle.default, handler: { action in
                        self.pullUpControllerMoveToVisiblePoint(self.pullUpControllerMiddleStickyPoints[1], completion: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "createTeamVCFunction"), object: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let myTeamVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyTeamsViewController") as? MyTeamsViewController
                    myTeamVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    myTeamVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    myTeamVC?.teamList = teams
                    myTeamVC?.currentMatchID = self.currentMatchID
                    myTeamVC?.reloadOpenMatchDetail = true
                    self.present(myTeamVC!, animated: true, completion: nil)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        }
        
    }
    
    @IBAction func joinButtonTapped(_ sender: UIButton) {
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\": \"\(currentMatchID!)\"}", mod: "Match", actionType: "join-match", callback: { (response: Any) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.refreshMatchDetail()
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        })
    }
    
    override var pullUpControllerPreferredSize: CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 570)
    }
    
    override var pullUpControllerPreviewOffset: CGFloat {
        return pullUpControllerMiddleStickyPoints[2]
    }
    
    override var pullUpControllerMiddleStickyPoints: [CGFloat] {
        return [330 ,-300, 570]
    }
    
    override var pullUpControllerIsBouncingEnabled: Bool {
        return true
    }
    
    override var pullUpControllerPreferredLandscapeFrame: CGRect {
        return CGRect(x: 5, y: 5, width: 280, height: UIScreen.main.bounds.height - 10)
    }
    
    @objc func refreshMatchDetail() {
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\": \"\(currentMatchID!)\"}", mod: "Match", actionType: "view-match-detail-dashboard", callback: { (response: Any) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    print(response)
                    self.matchInfo = response as? NSDictionary
                    self.loadData()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadOpenMacthListFunction"), object: nil)
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        })
    }
}
