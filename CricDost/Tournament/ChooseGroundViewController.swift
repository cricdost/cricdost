//
//  ChooseGroundViewController.swift
//  CricDost
//
//  Created by Jit Goel on 8/29/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import IQKeyboardManager
import CoreLocation

class ChooseGroundViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate,ShowsAlert, UISearchBarDelegate {
    
    @IBOutlet weak var groundTableView: UITableView!
    var manager: CLLocationManager?
    var latitude = 0.00
    var longitude = 0.00
    @IBOutlet weak var searchbar: UISearchBar!
    var gotlocation = true
    var groundList: NSArray = []
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = CommonUtil.themeRed
        }
//        UIApplication.shared.statusBarStyle = .lightContent
        
        IQKeyboardManager.shared().isEnabled = true
        
        searchbar.layer.borderWidth = 1
        searchbar.layer.borderColor = CommonUtil.themeRed.cgColor
            
        OperationQueue.main.addOperation{
            self.manager = CLLocationManager()
            self.manager?.delegate = self
            self.manager?.desiredAccuracy = kCLLocationAccuracyBest
            self.manager?.requestWhenInUseAuthorization()
            self.manager?.startUpdatingLocation()
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        getNearByGround(searchKey: searchBar.text!, lat: latitude, lon: longitude)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        getNearByGround(searchKey: searchText, lat: latitude, lon: longitude)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        latitude = location.coordinate.latitude
        longitude = location.coordinate.longitude
        if ProcessInfo.processInfo.environment["SIMULATOR_DEVICE_NAME"] != nil {
            print("Simulator")
            latitude = 13.095276
            longitude = 80.169093
        }
        if gotlocation {
            UserDefaults.standard.set(latitude, forKey: CommonUtil.TEMPLATITUDE)
            UserDefaults.standard.set(longitude, forKey: CommonUtil.TEMPLONGITUDE)
            print("GETTING MY LOCATION")
            getNearByGround(searchKey: "", lat: latitude, lon: longitude)
            gotlocation = false
        }
        manager.stopUpdatingLocation()
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (groundList.count + 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == (tableView.numberOfRows(inSection: indexPath.section) - 1)  {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectFromMapCell", for: indexPath) as! SelectFromMapTableViewCell
            return cell
        } else {
            let ground = groundList[indexPath.row] as! NSDictionary
            let cell = tableView.dequeueReusableCell(withIdentifier: "groundCell", for: indexPath) as! GroundTableViewCell
            cell.groundLabel.text = "\(ground["NAME"] as? String ?? "")\(ground["ADDRESS"] as? String ?? "")"
            cell.distanceLabel.text = "\(String(format: "%.2f", Double(ground["DISTANCE"] as! String)!)) km"
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == (tableView.numberOfRows(inSection: indexPath.section) - 1)  {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "gotoSelectFromMap"), object: nil)
            }
        } else {
            let ground = groundList[indexPath.row] as! NSDictionary
            let grd = ["GROUND_ID":"\(ground["LOCATION"] as! String)","ADDRESS":"\(ground["ADDRESS"] as! String)"]
            UserDefaults.standard.set(grd, forKey: CommonUtil.TEMPGROUNDSELECTED)
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setLocationForTournamentHostFromExistingGround"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "setLocationForCreateMatchFromExistingGround"), object: nil)
            }
        }
    }
    
    func getNearByGround(searchKey: String, lat: Double, lon: Double) {
        activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"SEARCH_KEY\":\"\(searchKey)\",\"LATITUDE\":\"\(lat)\",\"LONGITUDE\":\"\(lon)\",\"ITEMS_PER_PAGE\":\"5\",\"USER_ID\":\"\(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String)\"}", mod: "Ground", actionType: "ground-list") { (response) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    print(response)
                    self.groundList = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["GROUNDS"] as! NSArray
                    
                    UIView.transition(with: self.groundTableView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.groundTableView.reloadData()
                    })
                    
                } else {
                    self.showAlert(title: "Oops", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops", message: "Something went wrong")
            }
            self.activityIndicator.stopAnimating()
        }
    }
}





