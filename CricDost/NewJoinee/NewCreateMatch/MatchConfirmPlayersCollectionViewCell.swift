//
//  MatchConfirmPlayersCollectionViewCell.swift
//  CricDost
//
//  Created by JIT GOEL on 10/9/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class MatchConfirmPlayersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var playersProfileImageView: UIImageView!
    @IBOutlet weak var playersNameLabel: UILabel!
    override func layoutSubviews() {
        super.layoutSubviews()
        CommonUtil.imageRoundedCorners(imageviews: [playersProfileImageView])
        
//        self.layer.cornerRadius = 3.0
//        layer.shadowRadius = 3
//        layer.shadowOpacity = 0.3
//        layer.shadowOffset = CGSize(width: 2, height: 2)
//        self.clipsToBounds = false
    }
}
