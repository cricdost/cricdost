//
//  NewsFeedTableViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 10/15/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import SkeletonView
class NewsFeedTableViewCell: UITableViewCell {

    @IBOutlet weak var spaceViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var oneImageView: UIImageView!
    @IBOutlet weak var addressTimeLabel: UILabel!
    @IBOutlet weak var postContentLabel: UILabel!
    @IBOutlet weak var optionButton: UIButton!
    @IBOutlet weak var sixButton: UIButton!
    @IBOutlet weak var sixCountLabel: UILabel!
    @IBOutlet weak var fourButton: UIButton!
    @IBOutlet weak var fourCountLabel: UILabel!
    @IBOutlet weak var outButton: UIButton!
    @IBOutlet weak var outCountLabel: UILabel!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var commentsCountLabel: UILabel!
    @IBOutlet weak var shareButton: ShareButtonCustom!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var comment1View: UIView!
    @IBOutlet weak var comment2View: UIView!
    @IBOutlet weak var showMoreButton: UIButton!
    @IBOutlet weak var c1ProfileImage: UIImageView!
    @IBOutlet weak var c1ProfileName: UILabel!
    @IBOutlet weak var c1OptionButton: UIButton!
    @IBOutlet weak var c1CommentLabel: UILabel!
    @IBOutlet weak var c1addressTimeLabel: UILabel!
    @IBOutlet weak var c2ProfileImage: UIImageView!
    @IBOutlet weak var c2ProfileName: UILabel!
    @IBOutlet weak var c2OptionButton: UIButton!
    @IBOutlet weak var c2CommentLabel: UILabel!
    @IBOutlet weak var c2addressTimeLabel: UILabel!
    @IBOutlet weak var bottomLineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        CommonUtil.imageRoundedCorners(imageviews: [profileImageView])
        
        [profileImageView,profileNameLabel,oneImageView,addressTimeLabel,postContentLabel].forEach { (object) in
            object?.showAnimatedGradientSkeleton()
        }
    }
    
    func hideAnimation() {
        [profileImageView,profileNameLabel,oneImageView,addressTimeLabel,postContentLabel].forEach { (object) in
            object?.hideSkeleton()
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }

}
