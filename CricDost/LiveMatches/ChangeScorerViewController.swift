//
//  ChangeScorerViewController.swift
//  CricDost
//
//  Created by Jit Goel on 6/29/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ChangeScorerViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ShowsAlert {

    @IBOutlet weak var scorerCollectionView: UICollectionView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var chooseScorerView: UIView!
    var playerList: NSMutableArray = []
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var currentScorer: String?
    var MATCH_ID: String?
    @IBOutlet weak var dismissView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonUtil.buttonRoundedCorners(buttons: [nextButton])
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [chooseScorerView])
        
        setCollectionViewLayout(card: chooseScorerView, collectionView: scorerCollectionView, center: false)
        currentScorer = UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as? String
        print(playerList)
        
        let outerViewTap = UITapGestureRecognizer(target: self, action: #selector(outerViewFunction))
        dismissView.addGestureRecognizer(outerViewTap)
    }
    
    @objc func outerViewFunction(sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return playerList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let player = playerList[indexPath.section] as! NSDictionary
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "changeScorer", for: indexPath) as! ChangeScorerCollectionViewCell
        cell.playerName.text = player["FULL_NAME"] as? String
        cell.profileImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(player["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
        
        if player["USER_ID"] as? String == currentScorer {
            cell.greenTick.isHidden = false
        } else {
            cell.greenTick.isHidden = true
        }
        return cell
    }
    
    @IBAction func nextButtonClick(_ sender: Any) {
        if currentScorer == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as? String {
            self.showAlert(title: "Unable to Change", message: "You are the scorer currently, please select a different user")
        } else {
            changeScorer(scorer: currentScorer!)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let player = playerList[indexPath.section] as! NSDictionary
        currentScorer = player["USER_ID"] as? String
        collectionView.reloadData()
    }
    
    func setCollectionViewLayout(card: UIView, collectionView: UICollectionView, center: Bool) {
        if center {
            let screensize = card.bounds.size
            let cellwidth = floor(screensize.width * 0.8)
            let cellheight = floor(collectionView.bounds.height * 1.0)
            
            let insetX = (card.bounds.width - cellwidth) / 2.0
            let insetY = (collectionView.bounds.height - cellheight) / 2.0
            
            let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.itemSize = CGSize(width: cellwidth, height: cellheight)
            collectionView.contentInset = UIEdgeInsetsMake(insetY, insetX, insetY, insetX)
        } else {
            let screensize1 = card.bounds.size
            let cellwidth1 = floor(screensize1.width/3)
            let cellheight1 = floor(collectionView.bounds.height)
            
            let layout1 = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout1.itemSize = CGSize(width: cellwidth1, height: cellheight1)
            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }
    }
    
    func changeScorer(scorer: String) {
        activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\":\"\(MATCH_ID!)\",\"SCORER\":\"\(scorer)\"}", mod: "MatchScore", actionType: "change-scorer") { (response) in
            if response as? String != "error" {
                print(response)
                self.dismiss(animated: true, completion: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissScorerView"), object: nil)
                SocketConnectionsclass.matchScoreUpdateEmit(match_id: self.MATCH_ID!)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
        }
    }
}
