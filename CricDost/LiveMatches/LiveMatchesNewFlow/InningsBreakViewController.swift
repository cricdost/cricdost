//
//  InningsBreakViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 9/18/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class InningsBreakViewController: UIViewController {

    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet weak var matchTimerLabel: UILabel!
    @IBOutlet weak var moreOptionButton: UIButton!
    @IBOutlet weak var teamAImageView: UIImageView!
    @IBOutlet weak var teamANameLabel: UILabel!
    @IBOutlet weak var teamAScoreLabel: UILabel!
    @IBOutlet weak var teamBImageView: UIImageView!
    @IBOutlet weak var teamBNameLabel: UILabel!
    @IBOutlet weak var teamBScoreLabel: UILabel!
    @IBOutlet weak var startMatchButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        CommonUtil.roundedUIViewCornersWithShade(uiviews: [viewHolder])
        CommonUtil.buttonRoundedCorners(buttons: [startMatchButton])
        CommonUtil.imageRoundedCorners(imageviews: [teamAImageView,teamBImageView])
    }

    @IBAction func startMatchButtonClick(_ sender: Any) {
        print("Start match button clicked")
    }
    
    @IBAction func moreOptionButtonClick(_ sender: Any) {
        print("Inning break more option button clicked")
    }
    
}
