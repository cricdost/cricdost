//
//  PlayersSearchTableViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 7/9/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class PlayersSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var starOne: UIButton!
    @IBOutlet weak var starTwo: UIButton!
    @IBOutlet weak var starFour: UIButton!
    @IBOutlet weak var starThree: UIButton!
    @IBOutlet weak var starFive: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var skillsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        CommonUtil.imageRoundedCorners(imageviews: [profileImage])
    }
    
    func setRating(rating: String) {
        switch rating {
        case "1": for star in [starOne] {
            star?.setImage(#imageLiteral(resourceName: "ratingstar"), for: .normal)
        }
            break
        case "2": for star in [starOne,starTwo] {
            star?.setImage(#imageLiteral(resourceName: "ratingstar"), for: .normal)
        }
            break
        case "3": for star in [starOne,starTwo,starThree] {
            star?.setImage(#imageLiteral(resourceName: "ratingstar"), for: .normal)
        }
            break
        case "4": for star in [starOne,starTwo,starThree,starFour] {
            star?.setImage(#imageLiteral(resourceName: "ratingstar"), for: .normal)
        }
            break
        case "5": for star in [starOne,starTwo,starThree,starFour,starFive] {
            star?.setImage(#imageLiteral(resourceName: "ratingstar"), for: .normal)
        }
            break
        default:
            break
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
