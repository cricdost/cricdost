//
//  NotificationViewController.swift
//  CricDost
//
//  Created by Jit Goel on 7/16/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import UserNotifications

class NotificationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource ,ShowsAlert ,UIScrollViewDelegate{
    
   
    var notificationList: NSMutableArray = []
    
    var selectedNotifications : [String] = []
    var readNotificationTemp: [String] = []
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var noNotification: UILabel!
    @IBOutlet weak var notificationTableView: UITableView!
    var rigthBarButtonItem : UIBarButtonItem!
    
    var isSelected = false
    var isNotificationUpdated = false
    
    var nextPage = 0
    var readNoticationArray: NSMutableArray?
    @IBOutlet weak var welcomeNotiView: UIView!
    @IBOutlet weak var welcomeNotiProfileImageView: UIImageView!
    @IBOutlet weak var welcomeNotiTitleLabel: UILabel!
    @IBOutlet weak var welcomeNotiTimerLabel: UILabel!
     @IBOutlet weak var welcomeNotiViewHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = CommonUtil.themeRed
        }
        
        self.navBar.topItem?.title = "Notifications"
        
//        UIApplication.shared.statusBarStyle = .lightContent
        //remove delete button
        removeDeleteButton()
        //get notification list
        getNotifications()
        
        NotificationCenter.default.addObserver(self, selector: #selector(getNotifications), name: NSNotification.Name(rawValue: "getNotifications"), object: nil)
        
       if UserDefaults.standard.object(forKey: CommonUtil.NewNotification) as? Bool ?? false {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let currentDate = formatter.string(from: date)
        
        welcomeNotiProfileImageView.image = #imageLiteral(resourceName: "cd_logo_otp")
        welcomeNotiTitleLabel.text = "Hey Dost"
        welcomeNotiTimerLabel.text = currentDate
        
        let notiView = UITapGestureRecognizer(target: self, action: #selector(welcomeNotiViewTapFunction))
        welcomeNotiView.addGestureRecognizer(notiView)
        welcomeNotiView.backgroundColor = UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.0)
       } else {
        welcomeNotiViewHeight.constant = 0
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func welcomeNotiViewTapFunction() {
        let chatVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
        chatVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        chatVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        chatVC.playerUserName = "cricdost"
        self.present(chatVC, animated: true, completion: nil)
    }
    
    func createDeleteButton()
    {
        self.rigthBarButtonItem = UIBarButtonItem(title: "Delete", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        self.rigthBarButtonItem.tintColor = UIColor.white
        self.rigthBarButtonItem.target = self
        self.rigthBarButtonItem.action = #selector(deleteSelectedNotifications)
        self.navBar.topItem?.rightBarButtonItem = self.rigthBarButtonItem
    }
    
    func removeDeleteButton() {
        self.navBar.topItem?.rightBarButtonItem = nil
    }
    
    @objc func longPress(_ longPressGestureRecognizer: UILongPressGestureRecognizer) {
        
        if longPressGestureRecognizer.state == UIGestureRecognizerState.began {
            let touchPoint = longPressGestureRecognizer.location(in: self.view)
            if let indexPath = notificationTableView.indexPathForRow(at: touchPoint) {
                
                self.isSelected = true
                selectItem(indexPath: indexPath)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        notificationTableView.reloadData()
    }
    
    func selectItem(indexPath : IndexPath)
    {
        let notification = notificationList[indexPath.row] as! NSDictionary
        
        let selectedCell:NotificationTableViewCell = notificationTableView.cellForRow(at: indexPath) as! NotificationTableViewCell
        selectedCell.contentView.backgroundColor = UIColor(red:1.00, green:0.70, blue:0.85, alpha:1.0)
        
        if selectedNotifications.contains(notification["NOTIFICATION_LIST_ID"] as! String) {
            //remove item
            removeItem(indexPath: indexPath)
        }
        else
        {
            //add notification id into array
            selectedNotifications.append(notification["NOTIFICATION_LIST_ID"] as! String)
            //create delete button
            createDeleteButton()
            
            self.navBar.topItem?.title = "\(selectedNotifications.count) selected item"
            self.navBar?.barTintColor = UIColor.black
        }
        
        print(selectedNotifications)
    }
    
    func removeItem(indexPath : IndexPath)
    {
        let notification = notificationList[indexPath.row] as! NSDictionary
        
        let selectedCell: NotificationTableViewCell = notificationTableView.cellForRow(at: indexPath) as! NotificationTableViewCell
        selectedCell.contentView.backgroundColor = UIColor.white
        
        //remove notification id from array list
        if selectedNotifications.contains(notification["NOTIFICATION_LIST_ID"] as! String) {
            
            selectedNotifications = selectedNotifications.filter{$0 != notification["NOTIFICATION_LIST_ID"] as! String }
            
            //remove delete button if it has no notification to delete
            if(selectedNotifications.count == 0)
            {
                removeDeleteButton()
                self.navBar.topItem?.title = "Notifications"
                self.navBar?.barTintColor = CommonUtil.themeRed
                self.isSelected = false
            }
            else
            {
                self.navBar.topItem?.title = "\(selectedNotifications.count) selected item"
                self.navBar?.barTintColor = UIColor.black
            }
        }
        
        print(selectedNotifications)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if UserDefaults.standard.object(forKey: CommonUtil.NewNotification) as? Bool ?? false {
//            return 1
//        } else {
            return notificationList.count
//        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        if UserDefaults.standard.object(forKey: CommonUtil.NewNotification) as? Bool ?? false {
//            print("welcome notification")
//            let notiCell = tableView.dequeueReusableCell(withIdentifier: "WelcomeNotificationTableViewCell", for: indexPath) as! WelcomeNotificationTableViewCell
//            let date = Date()
//            let formatter = DateFormatter()
//            formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
//            let currentDate = formatter.string(from: date)
//
//            notiCell.logoImageView.image = #imageLiteral(resourceName: "cd_logo_otp")
//            notiCell.massageLabel.text = "Hey!.. Welcome to CricDost!!"
//            notiCell.timeLabel.text = currentDate
//            return notiCell
//        } else {
            print("Notification")
            let notification = notificationList[indexPath.row] as! NSDictionary
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotificationTableViewCell
            
            cell.declineButton.addTarget(self, action: #selector(declineButtonTapped), for: UIControlEvents.touchUpInside)
            cell.declineButton.tag = indexPath.row
            
            cell.acceptButton.addTarget(self, action: #selector(acceptButtonTapped), for: UIControlEvents.touchUpInside)
            cell.acceptButton.tag = indexPath.row
            
            let longtap = UILongPressGestureRecognizer(target: self, action: #selector(longPress))
            
            //        let singletap = UITapGestureRecognizer(target: self, action: #selector(singlePress))
            
            cell.cellView.addGestureRecognizer(longtap)
            //        cell.cellView.addGestureRecognizer(singletap)
            cell.cellView.tag = indexPath.row
            
            cell.messageLabel.text = (notification["MESSAGE"] as! String).htmlToString
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let myDate = dateFormatter.date(from: notification["UPDATED_DATE"] as! String)!
            
            cell.timeLabel.text = CommonUtil.timeAgoSinceDate(date: myDate as NSDate, numericDates: false)
            cell.notificationImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(notification["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
            
            cell.contentView.backgroundColor = UIColor.white
            
            if indexPath.row == self.notificationList.count - 1 {
                print("ready to load next page")
                ///getNotifications()
            }
            
            if notification["IS_READ"] as! Bool || readNotificationTemp.contains(notification["NOTIFICATION_ID"] as! String) {
                cell.contentView.backgroundColor = UIColor.white
            } else {
                cell.contentView.backgroundColor = UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.0)
            }
            
            if selectedNotifications.contains(notification["NOTIFICATION_LIST_ID"] as! String) {
                cell.contentView.backgroundColor = UIColor(red:1.00, green:0.70, blue:0.85, alpha:1.0)
            }
//            else {
//                cell.contentView.backgroundColor = UIColor.white
//            }
            
            if notification["TYPE"] as? String == "MATCH_JOIN_TEAM" || notification["TYPE"] as? String == "TEAM_PLAYER_REQUEST" || notification["TYPE"] as? String == "PLAYER_TEAM_REQUEST" {
                cell.acceptButton.isHidden = false
                cell.declineButton.isHidden = false
            } else {
                cell.acceptButton.isHidden = true
                cell.declineButton.isHidden = true
            }
            return cell
//        }
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        if UserDefaults.standard.object(forKey: CommonUtil.NewNotification) as? Bool ?? false {
//            print("selected welcome notification")
//            let chatVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
//            chatVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//            chatVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
//            chatVC.playerUserName = "cricdost"
//            self.present(chatVC, animated: true, completion: nil)
//        } else {
            if(isSelected)
            {
                selectItem(indexPath: indexPath)
            }
            else
            {
                removeItem(indexPath: indexPath)
            }
            let notification = notificationList[indexPath.row] as! NSDictionary
            readNotificationTemp.append(notification["NOTIFICATION_ID"] as! String)
            tableView.reloadData()
//        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @IBAction func declineButtonTapped(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Decline", message: "Are you sure you want to decline this request?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            //self.updateNotificationSettings(actionType: "set-push-notification", flag: "0")
            self.declineRequest(indexPath: sender.tag)
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func acceptButtonTapped(_ sender: UIButton) {

        let alert = UIAlertController(title: "Accept", message: "Are you sure you want to accept this request?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            //self.updateNotificationSettings(actionType: "set-push-notification", flag: "0")
            self.acceptRequest(indexPath: sender.tag)
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func acceptRequest(indexPath : Int)
    {
        let notification = notificationList[indexPath] as! NSDictionary
        
        var type = notification["TYPE"] as? String
        var mod : String?
        var actionType : String?
        var subAction : String?
        
        switch type {
        case "MATCH_JOIN_TEAM":
            mod = "Match"
            actionType = "join-match-team"
            subAction = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"LIST_ID\":\"\(notification["NOTIFICATION_ID"] as? String ?? "\(notification["NOTIFICATION_ID"] as! Int)")\",\"NOTIFICATION_LIST_ID\":\"\(notification["NOTIFICATION_LIST_ID"] as? String ?? "\(notification["NOTIFICATION_LIST_ID"] as! Int)")\",\"MATCH_ID\":\"\(notification["MATCH_ID"] as? String ?? "\(notification["MATCH_ID"] as! Int)")\",\"MATCH_TEAM_ID\":\"\(notification["MATCH_TEAM_ID"] as? String ?? "\(notification["MATCH_TEAM_ID"] as! Int)")\",\"TEAM_ID\":\"\(notification["TEAM_ID"] as? String ?? "\(notification["TEAM_ID"] as! Int)")\",\"USER_ID\":\"\(notification["USER_ID"] as? String ?? "\(notification["USER_ID"] as! Int)")\"}"
            
            break
        case "TEAM_PLAYER_REQUEST":
            mod = "Team"
            actionType = "request-reply"
            subAction = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"NOTIFICATION_LIST_ID\":\"\(notification["NOTIFICATION_LIST_ID"] as? String ?? "\(notification["NOTIFICATION_LIST_ID"] as! Int)")\",\"TEAM_PLAYER_ID\":\"\(notification["TEAM_PLAYER_ID"] as? String ?? "\(notification["TEAM_PLAYER_ID"] as! Int)")\",\"REQUEST_TYPE\":\"Sent\",\"STATUS\":\"1\"}"
            
            break
        case "PLAYER_TEAM_REQUEST":
            mod = "Team"
            actionType = "request-reply"
            subAction = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"NOTIFICATION_LIST_ID\":\"\(notification["NOTIFICATION_LIST_ID"] as? String ?? "\(notification["NOTIFICATION_LIST_ID"] as! Int)")\",\"TEAM_PLAYER_ID\":\"\(notification["TEAM_PLAYER_ID"] as? String ?? "\(notification["TEAM_PLAYER_ID"] as! Int)")\",\"REQUEST_TYPE\":\"Received\",\"STATUS\":\"1\"}"
            
            break
        default:
            break;
        }
        
        updateNotificationStatus(mod: mod!,actionType: actionType!,subAction: subAction!,indexPath : IndexPath(row: indexPath, section: 0))
    }
    
    func declineRequest(indexPath : Int)
    {
        let notification = notificationList[indexPath] as! NSDictionary
        
        var type = notification["TYPE"] as? String
        var mod : String?
        var actionType : String?
        var subAction : String?
        
        switch type {
        case "MATCH_JOIN_TEAM":
            mod = "Match"
            actionType = "delete-notification-list"
            subAction = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"LIST_ID\":\"\(notification["NOTIFICATION_ID"] as? String ?? "\(notification["NOTIFICATION_ID"] as! Int)")\",\"NOTIFICATION_LIST_ID\":\"\(notification["NOTIFICATION_LIST_ID"] as? String ?? "\(notification["NOTIFICATION_LIST_ID"] as! Int)")\",\"MATCH_ID\":\"\(notification["MATCH_ID"] as? String ?? "\(notification["MATCH_ID"] as! Int)")\",\"MATCH_TEAM_ID\":\"\(notification["MATCH_TEAM_ID"] as? String ?? "\(notification["MATCH_TEAM_ID"] as! Int)")\",\"TEAM_ID\":\"\(notification["TEAM_ID"] as? String ?? "\(notification["TEAM_ID"] as! Int)")\",\"USER_ID\":\"\(notification["USER_ID"] as? String ?? "\(notification["USER_ID"] as! Int)")\"}"
            
            break
        case "TEAM_PLAYER_REQUEST":
            print("decline request")
            mod = "Team"
            actionType = "request-reply"
            subAction = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"NOTIFICATION_LIST_ID\":\"\(notification["NOTIFICATION_LIST_ID"] as? String ?? "\(notification["NOTIFICATION_LIST_ID"] as! Int)")\",\"TEAM_PLAYER_ID\":\"\(notification["TEAM_PLAYER_ID"] as? String ?? "\(notification["TEAM_PLAYER_ID"] as! Int)")\",\"REQUEST_TYPE\":\"Sent\",\"STATUS\":\"0\"}"
            break
        case "PLAYER_TEAM_REQUEST":
            mod = "Team"
            actionType = "request-reply"
            subAction = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"NOTIFICATION_LIST_ID\":\"\(notification["NOTIFICATION_LIST_ID"] as? String ?? "\(notification["NOTIFICATION_LIST_ID"] as! Int)")\",\"TEAM_PLAYER_ID\":\"\(notification["TEAM_PLAYER_ID"] as? String ?? "\(notification["TEAM_PLAYER_ID"] as! Int)")\",\"REQUEST_TYPE\":\"Received\",\"STATUS\":\"0\"}"
            
            break
        default:
            break;
        }
        
        updateNotificationStatus(mod: mod!,actionType: actionType!,subAction: subAction!,indexPath : IndexPath(row: indexPath, section: 0))
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        print("checknoti")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateNotificationCount"), object: nil)

        if UserDefaults.standard.object(forKey: CommonUtil.NewNotification) as? Bool ?? false {
            if CommonUtil.LandingPageCricSpace {
                self.dismiss(animated: true, completion: nil)
            } else {
                self.performSegue(withIdentifier: "notication_dashboard", sender: self)
            }
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func getNotifications() {
        
        let currentPage = self.nextPage != 0 ? -self.nextPage : 0
        print("current page is \(currentPage)")
        if(currentPage == 0)
        {
            self.activityIndicator.startAnimating()
            self.noNotification.isHidden = true
            
            if UserDefaults.standard.object(forKey: CommonUtil.NewNotification) as? Bool ?? false {
                self.notificationTableView.isHidden = false
            } else {
                self.notificationTableView.isHidden = true
            }
            
        }
        
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Notification", actionType: "list-notification") { (response) in
            if response as? String != "error" {
                print(response)
                
                self.nextPage = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["NEXT_PAGE"] as! Int
                
                if(currentPage == 0)
                {
                    self.activityIndicator.stopAnimating()
                    self.notificationList = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["NOTIFICAITONS"] as! NSArray as! NSMutableArray
                    
                    if(self.notificationList.count > 0)
                    {
                        self.noNotification.isHidden = true
                        self.notificationTableView.isHidden = false
                    }
                    else
                    {
                        self.noNotification.isHidden = false
                        
                        if UserDefaults.standard.object(forKey: CommonUtil.NewNotification) as? Bool ?? false {
                            UserDefaults.standard.set(true, forKey: CommonUtil.NewNotification)
                            self.notificationTableView.isHidden = false
                            self.noNotification.isHidden = true
                        } else {
                            self.notificationTableView.isHidden = true
                        }
                    }
                    //update notification if it is not yet updated
                    if(self.notificationList.count > 0 && !self.isNotificationUpdated)
                    {
                        self.updateNotificationStatus()
                    }
                }
                else
                {
                    //add array to existing array
                    //self.notificationList.addObjectsFromArray()
                }
                
                self.notificationTableView.reloadData()
                
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
        }
    }
    
    func updateNotificationStatus(mod : String , actionType : String , subAction : String, indexPath: IndexPath) {
        
        self.activityIndicator.startAnimating()
        self.noNotification.isHidden = true
        //        self.notificationTableView.isHidden = true
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: subAction, mod: mod, actionType: actionType) { (response) in
            if response as? String != "error" {
                print(response)
                self.activityIndicator.stopAnimating()
//                self.showAlert(title : "Success", message : (response as! NSDictionary)["XSCMessage"] as! String)
                self.getNotifications()
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
        }
    }
    
    @objc func deleteSelectedNotifications(){
        
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to delete these notifications?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            self.deleteNotificationFun()
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteNotificationFun()
    {
        self.activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"NOTIFICATION_LIST_IDS\":\(selectedNotifications)}", mod: "Notification", actionType: "delete-multiple-notifications") { (response) in
            if response as? String != "error" {
                print(response)
                self.activityIndicator.stopAnimating()
                self.removeDeleteButton()
                self.navBar.topItem?.title = "Notifications"
                self.navBar?.barTintColor = CommonUtil.themeRed
                self.isSelected = false
//                self.showAlert(title : "Success", message : (response as! NSDictionary)["XSCMessage"] as! String)
                self.selectedNotifications.removeAll()
                self.getNotifications()
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
        }
    }
    
    func updateNotificationStatus()
    {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Notification", actionType: "update-all-read-status") { (response) in
            if response as? String != "error" {
                print(response)
                self.isNotificationUpdated = true
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
        }
    }
}
