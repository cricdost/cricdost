//
//  MobileViewController.swift
//  CricDost
//
//  Created by Jit Goel on 5/23/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import MessageUI
import FRHyperLabel

class MobileViewController: UIViewController, MFMailComposeViewControllerDelegate, ShowsAlert, UITextFieldDelegate,UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var verifyNowButton: UIButton!
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var countryCodeTextField: UITextField!
    @IBOutlet weak var mobileNumberLineView: UIView!
    @IBOutlet weak var countryCodeLineView: UIView!
    @IBOutlet weak var problemSigningLabel: UnderlinedLabel!
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var privacyLabel: UILabel!
    var countryPicker = UIPickerView()
    var deviceId:String?
    @IBOutlet weak var loadingViewPreventUserInteraction: UIView!
    @IBOutlet weak var verifyActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var otpStackView: UIStackView!
    @IBOutlet weak var otpTextField1: UITextField!
    @IBOutlet weak var otpTextField2: UITextField!
    @IBOutlet weak var otpTextField3: UITextField!
    @IBOutlet weak var imDoneButton: UIButton!
    @IBOutlet weak var sitbackLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var didrecieveLabel: FRHyperLabel!
    @IBOutlet weak var logoImageView: UIImageView!
    
    var count = 60
    var timer:Timer?
    
    var nameVC_goto: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        CommonUtil.animateImage(image: self.logoImageView)
        
        
        
        //Device ID
        deviceId = UIDevice.current.identifierForVendor!.uuidString
        
        //Rounded Button
        CommonUtil.buttonRoundedCorners(buttons: [verifyNowButton, imDoneButton])
        
        //HyperLabel Config
        didrecieveLabel.numberOfLines = 0
        
        let re_send = "Didn't receive code yet? Resend"
        
        let attributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        didrecieveLabel.attributedText = NSAttributedString(string: re_send, attributes: attributes)
        
        let handler = {
            (hyperLabel: FRHyperLabel?, substring: String?) -> Void in
        }
        didrecieveLabel.setLinksForSubstrings(["Resend"], withLinkHandler: handler)
        
        // Configuring Keyboard
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)
        self.addDoneButtonOnKeyboard()
        
        // bottom label configuration
        problemSigningLabel.text = "Problems with signing in?"
        
        let didrecieveLabelTaped = UITapGestureRecognizer(target: self, action: #selector(didrecieveLabelTapFunction))
        didrecieveLabel.addGestureRecognizer(didrecieveLabelTaped)
        
        let tapTerms = UITapGestureRecognizer(target: self, action: #selector(tapTermsFunction))
        termsLabel.addGestureRecognizer(tapTerms)
        
        let tapPrivacy = UITapGestureRecognizer(target: self, action: #selector(tapPrivacyFunction))
        privacyLabel.addGestureRecognizer(tapPrivacy)
        
        let tapSignInProb = UITapGestureRecognizer(target: self, action: #selector(tapSignInProbFunction))
        problemSigningLabel.addGestureRecognizer(tapSignInProb)
        
        countryPicker.delegate = self
        countryPicker.dataSource = self
        
        countryCodeTextField.inputView = countryPicker
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        CommonUtil.updateGATracker(screenName: "Login")
    }
    
   
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == countryPicker {
            print(CountryCodes.countryCodes.count)
            return CountryCodes.countryCodes.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == countryPicker {
            let text = "\(CountryCodes.countryCodes[row].first!.value)"
            return text
        }
        return "error"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == countryPicker {
            let country = CountryCodes.countryCodes[row].first!.key
            countryCodeTextField.text = country
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mobile_name" {
            let nameVC = segue.destination as! NameImageViewController
            nameVC.goto = nameVC_goto
        }
    }
    
    
    // Back Button Click
    @IBAction func backButtonClick(_ sender: Any) {
        UIView.transition(with: self.mobileNumberTextField, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.mobileNumberTextField.isHidden = false
            self.countryCodeTextField.isHidden = false
        })
        UIView.transition(with: self.mobileNumberLineView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.mobileNumberLineView.isHidden = false
            self.countryCodeLineView.isHidden = false
        })
        UIView.transition(with: self.verifyNowButton, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.verifyNowButton.isHidden = false
        })
        UIView.transition(with: self.backButton, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.backButton.isHidden = true
        })
        UIView.transition(with: self.otpStackView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.otpStackView.isHidden = true
        })
        UIView.transition(with: self.imDoneButton, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.imDoneButton.isHidden = true
        })
        otpTextField1.text = ""
        otpTextField2.text = ""
        otpTextField3.text = ""
        UIView.transition(with: self.sitbackLabel, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.sitbackLabel.isHidden = true
        })
        UIView.transition(with: self.timerLabel, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.timerLabel.isHidden = true
        })
        UIView.transition(with: self.didrecieveLabel, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.didrecieveLabel.isHidden = true
        })
        if self.timer != nil {
            self.timer!.invalidate()
            self.timer = nil
        }
    }
    
    // Timer Method
    @objc func update() {
        if(count > 0) {
            if count < 11 {
                timerLabel.text = "00:0\(String(count - 1))"
            } else {
                timerLabel.text = "00:\(String(count - 1))"
            }
            count = count - 1
        } else {
            if timer != nil {
                timer!.invalidate()
                timer = nil
            }
            UIView.transition(with: self.sitbackLabel, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.sitbackLabel.text = "Enter OTP code send to your mobile number"
                
                if self.otpTextField1.text != "" && self.otpTextField2.text != "" && self.otpTextField3.text != "" {
                    self.imDoneButton.isHidden = false
                    self.timerLabel.isHidden = true
                    self.sitbackLabel.isHidden = true
                    self.timerLabel.isHidden = true
                    self.didrecieveLabel.isHidden = true
                } else {
                    self.imDoneButton.isHidden = true
                    self.sitbackLabel.isHidden = false
                    self.didrecieveLabel.isHidden = false
                    self.timerLabel.isHidden = true
                }
            })
            print("Timer Stopped")
        }
    }
    
    //Verify button click action
    @IBAction func verifyButtonClick(_ sender: Any) {
        if mobileNumberTextField.isReallyEmpty {
            self.mobileNumberTextField.shakeTextField()
            self.showAlert(title: "Missing info", message: "Please enter mobile number")
        } else {
            if (mobileNumberTextField.text?.count)! < 5 || (mobileNumberTextField.text?.count)! > 15 {
                self.mobileNumberTextField.shakeTextField()
                print("invalid")
                self.showAlert(title: "Invalid Phone Number", message: "Please enter a valid mobile number")
            } else {
                print("valid")
                verifyActivityIndicator.startAnimating()
                loadingViewPreventUserInteraction.isHidden = false
                mobileNumberTextField.isEnabled = false
                countryCodeTextField.isEnabled = false
                verifyNowButton.isEnabled = false
                getOTP()
            }
        }
       
    }
    
    @IBAction func checkOTPButtonClick(_ sender: Any) {
        if otpTextField1.text != "" && otpTextField2.text != "" && otpTextField3.text != "" {
            let otp = "\(otpTextField1.text!)\(otpTextField2.text!)\(otpTextField3.text!)"
            verifyActivityIndicator.startAnimating()
            loadingViewPreventUserInteraction.isHidden = false
            checkOTP(otp: otp)
        }
    }
    
    //Textfield Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == mobileNumberTextField {
            if textField.text?.count == 9 {
                UIView.transition(with: self.verifyNowButton, duration: 0.4, options: .transitionCrossDissolve, animations: {
                    self.verifyNowButton.isHidden = false
                })
            }
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
//                return false
                return true
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 15
//            return true
        } else {
            if ((textField.text?.count)! < 1  && string.count > 0){
                if(textField == otpTextField1){
                    otpTextField2.becomeFirstResponder()
                    UIView.transition(with: self.imDoneButton, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.imDoneButton.isHidden = true
                    })
                    UIView.transition(with: self.sitbackLabel, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.sitbackLabel.isHidden = false
                    })
                    UIView.transition(with: self.timerLabel, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.timerLabel.isHidden = false
                        if self.count == 0 {
                            self.timerLabel.isHidden = true
                            self.didrecieveLabel.isHidden = false
                        }
                    })
                }
                if(textField == otpTextField2){
                    otpTextField3.becomeFirstResponder()
                    UIView.transition(with: self.imDoneButton, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.imDoneButton.isHidden = true
                    })
                    UIView.transition(with: self.sitbackLabel, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.sitbackLabel.isHidden = false
                    })
                    UIView.transition(with: self.timerLabel, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.timerLabel.isHidden = false
                        if self.count == 0 {
                            self.timerLabel.isHidden = true
                            self.didrecieveLabel.isHidden = false
                        }
                    })
                    
                }
                if(textField == otpTextField3){
                    otpTextField3.resignFirstResponder()
                    UIView.transition(with: self.imDoneButton, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.imDoneButton.isHidden = false
                    })
                    UIView.transition(with: self.didrecieveLabel, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.didrecieveLabel.isHidden = true
                    })
                    UIView.transition(with: self.sitbackLabel, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.sitbackLabel.isHidden = true
                    })
                    UIView.transition(with: self.timerLabel, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.timerLabel.isHidden = true
                    })
                }
                textField.text = string
                return false
                
            }else if ((textField.text?.count)! >= 1  && string.count == 0){
                // on deleting value from Textfield
                if(textField == otpTextField2){
                    otpTextField1.becomeFirstResponder()
                    UIView.transition(with: self.imDoneButton, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.imDoneButton.isHidden = true
                    })
                    UIView.transition(with: self.sitbackLabel, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.sitbackLabel.isHidden = false
                    })
                    UIView.transition(with: self.timerLabel, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.timerLabel.isHidden = false
                        if self.count == 0 {
                            self.timerLabel.isHidden = true
                            self.didrecieveLabel.isHidden = false
                        }
                    })
                }
                if(textField == otpTextField3){
                    otpTextField2.becomeFirstResponder()
                    UIView.transition(with: self.imDoneButton, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.imDoneButton.isHidden = true
                    })
                    UIView.transition(with: self.sitbackLabel, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.sitbackLabel.isHidden = false
                    })
                    UIView.transition(with: self.timerLabel, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.timerLabel.isHidden = false
                        if self.count == 0 {
                            self.timerLabel.isHidden = true
                            self.didrecieveLabel.isHidden = false
                        }
                    })
                }
                textField.text = ""
                return false
            }else if ((textField.text?.count)! >= 1  ){
                print("number text field register page")
                textField.text = string
                return false
            }
        }
        return true
    }
    
    
    
    
    // bottom labels methods
    @objc func tapTermsFunction(sender:UITapGestureRecognizer) {
        print("tap terms working")
        UIApplication.shared.open(NSURL(string:"http://www.cricdost.com/terms.html")! as URL)
    }
    
    @objc func didrecieveLabelTapFunction(sender:UITapGestureRecognizer) {
        if self.timer != nil {
            self.timer!.invalidate()
            self.timer = nil
        }
        print("Resend")
        verifyActivityIndicator.startAnimating()
        loadingViewPreventUserInteraction.isHidden = false
        self.getOTP()
        UIView.transition(with: self.sitbackLabel, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.sitbackLabel.isHidden = true
            self.didrecieveLabel.isHidden = true
        })
    }
    
    @objc func tapPrivacyFunction(sender:UITapGestureRecognizer) {
        print("tap Privacy working")
        UIApplication.shared.open(NSURL(string:"http://www.cricdost.com/privacy.html")! as URL)
    }
    
    @objc func tapSignInProbFunction(sender:UITapGestureRecognizer) {
        print("Problem Signing in")
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.setToRecipients(["ios@cricdost.com"])
        composeVC.setSubject("Problem with signing in")
        composeVC.setMessageBody("Hey CRICDOST! Here's my feedback.", isHTML: false)
        
        // Present the view controller modally.
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            return
        } else {
            self.present(composeVC, animated: true, completion: nil)
        }
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    // KeyBoard Methods
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant = 0.0
            } else {
                self.keyboardHeightLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.mobileNumberTextField.inputAccessoryView = doneToolbar
        self.otpTextField1.inputAccessoryView = doneToolbar
        self.otpTextField2.inputAccessoryView = doneToolbar
        self.otpTextField3.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.mobileNumberTextField.resignFirstResponder()
        self.otpTextField1.resignFirstResponder()
        self.otpTextField2.resignFirstResponder()
        self.otpTextField3.resignFirstResponder()
    }
    
    func getOTP() {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_ID\":\"\(deviceId!)\",\"REFERRAL_CODE\":\"\",\"MOBILE_NUMBER\":\"\(mobileNumberTextField.text!)\",\"COUNTRY_CODE\":\"\(countryCodeTextField.text!)\"}", mod: "AA", actionType: "send-otp", callback: {(response : Any) -> Void in
            
            if response as? String != "error" {
                let success = (response as! NSDictionary)["XSCStatus"] as! Int
                if success == 0 {
                    
                    let otpRecived = (response as! NSDictionary)["XSCData"] as? String ?? "\((response as! NSDictionary)["XSCData"] as! Int)"
                    let otp = otpRecived.compactMap{Int(String($0))}
                    print(otp)
                    
                    UIView.transition(with: self.mobileNumberTextField, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.mobileNumberTextField.isHidden = true
                        self.countryCodeTextField.isHidden = true
                    })
                    UIView.transition(with: self.mobileNumberLineView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.mobileNumberLineView.isHidden = true
                        self.countryCodeLineView.isHidden = true
                    })
                    UIView.transition(with: self.verifyNowButton, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.verifyNowButton.isHidden = true
                    })
                    UIView.transition(with: self.backButton, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.backButton.isHidden = false
                    })
                    UIView.transition(with: self.otpStackView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.otpStackView.isHidden = false
                    })
                    UIView.transition(with: self.sitbackLabel, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.sitbackLabel.isHidden = false
                        self.sitbackLabel.text = "Sit back and relax while we verify your mobile number"
                    })
                    UIView.transition(with: self.timerLabel, duration: 0.4, options: .transitionCrossDissolve, animations: {
                        self.timerLabel.isHidden = false
                    })
                    self.count = 60
                    self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
                    
                    if UserDefaults.standard.object(forKey: CommonUtil.OTP_ENABLED) as? Bool ?? false {
                        self.otpTextField1.text = "\(otp[0])"
                        self.otpTextField2.text = "\(otp[1])"
                        self.otpTextField3.text = "\(otp[2])"
                        self.imDoneButton.isHidden = false
                        self.timerLabel.isHidden = true
                        self.sitbackLabel.isHidden = true
                        self.timerLabel.isHidden = true
                        self.didrecieveLabel.isHidden = true
                    }
                } else {
                    
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.verifyActivityIndicator.stopAnimating()
            self.loadingViewPreventUserInteraction.isHidden = true
            self.mobileNumberTextField.isEnabled = true
            self.countryCodeTextField.isEnabled = true
            self.verifyNowButton.isEnabled = true
        })
    }
    
    func checkOTP(otp: String) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_ID\":\"\(deviceId!)\",\"CODE\":\"\(otp)\",\"MOBILE_NUMBER\":\"\(mobileNumberTextField.text!)\"}", mod: "AA", actionType: "check-otp", callback: {(response : Any) -> Void in
            print(response)
            if response as? String != "error" {
                let success = (response as! NSDictionary)["XSCStatus"] as! Int
                if success == 0 {
                    let profile = (response as! NSDictionary)["XSCData"] as! NSDictionary
                    print(profile)
                    UserDefaults.standard.set(profile["DEVICE_KEY"] as? String ?? "", forKey: CommonUtil.DEVICE_KEY)
                    UserDefaults.standard.set(profile["USER_ID"] as? String ?? "", forKey: CommonUtil.USER_ID)
                    UserDefaults.standard.set(profile["ADDRESS"] as? String ?? "", forKey: CommonUtil.ADDRESS)
                    UserDefaults.standard.set(profile["SKILLS"] as? String ?? "", forKey: CommonUtil.SKILLS)
                    UserDefaults.standard.set(profile["IMAGE_URL"] as? String ?? "", forKey: CommonUtil.IMAGE_URL)
                    UserDefaults.standard.set(profile["FULL_NAME"] as? String ?? "", forKey: CommonUtil.FULL_NAME)
                    UserDefaults.standard.set(profile["MOBILE_NUMBER"] as? String ?? "", forKey: CommonUtil.MOBILE_NUMBER)
                    UserDefaults.standard.set(profile["COUNTRY_CODE"] as? String ?? "+91", forKey: CommonUtil.COUNTRY_CODE)
                    UserDefaults.standard.set(profile["TEAM_COUNT"] as? Int ?? "", forKey: CommonUtil.TEAM_COUNT)
                    UserDefaults.standard.set(profile["STEP_NAME"] as? String ?? "", forKey: CommonUtil.STEPNAME)
                    UserDefaults.standard.set(profile["OPENFIRE_PASSWORD"] as? String ?? "", forKey: CommonUtil.OPENFIRE_PASSWORD)
                    UserDefaults.standard.set(profile["OPENFIRE_USERNAME"] as? String ?? "", forKey: CommonUtil.OPENFIRE_USERNAME)
                    
                    switch(profile["STEP_NAME"] as! String) {
                    
                    case "NAME":    self.nameVC_goto = "NAME"
                                    self.performSegue(withIdentifier: "mobile_name", sender: self)
                        break
                    case "IMAGE":   self.nameVC_goto = "IMAGE"
                                    self.performSegue(withIdentifier: "mobile_name", sender: self)
                        break
                    case "GENDER":  self.performSegue(withIdentifier: "mobile_gender", sender: self)
                        break
                    case "DOB":     self.performSegue(withIdentifier: "mobile_dob", sender: self)
                        break
                    case "PLAYER_STATUS": self.performSegue(withIdentifier: "mobile_skill", sender: self)
                        break
                    case "LOCATION":    self.performSegue(withIdentifier: "mobile_location", sender: self)
                        break
                    case "DASHBOARD":
                        CommonUtil.updateGATracker(screenName: "Old User")
                        if CommonUtil.LandingPageCricSpace {
                            self.performSegue(withIdentifier: "mobile_cricspace", sender: self)
                        } else {
                            self.performSegue(withIdentifier: "mobile_dashboard", sender: self)
                        }
                        
                        break
                    default:
                        print("DEFAULT CASE")
                    }
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.verifyActivityIndicator.stopAnimating()
            self.loadingViewPreventUserInteraction.isHidden = true
        })
    }
}




