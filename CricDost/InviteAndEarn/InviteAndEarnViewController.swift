//
//  InviteAndEarnViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 11/14/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//
import UIKit
import Branch

class InviteAndEarnViewController: UIViewController, ShowsAlert {

    var rightButton  : UIButton?
    @IBOutlet weak var giftBoxView: UIView!
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var inviteFriendButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "baseline_arrow_back_white_24pt_1x.png")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "baseline_arrow_back_white_24pt_1x.png")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.pointLabel.text = "\(UserDefaults.standard.object(forKey: CommonUtil.MYPOINTS) as? Int ?? 50)"
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        CommonUtil.viewRoundedCorners(uiviews: [giftBoxView])
        CommonUtil.buttonRoundedCorners(buttons: [inviteFriendButton])
        
        Branch.getInstance().loadRewards { (changed, error) in
            if (error == nil) {
                let credits = Branch.getInstance().getCredits()
                print("credit: \(credits)")
                self.pointLabel.text = "\(credits)"
                UserDefaults.standard.set(credits, forKey: CommonUtil.MYPOINTS)
                self.updateMyRewardPoints(reward_point: credits)
                if credits == 0 {
                    Branch.getInstance()?.userCompletedAction("get_rewards")
                    Branch.getInstance().loadRewards { (changed, error) in
                        if (error == nil) {
                            let credits = Branch.getInstance().getCredits()
                            print("credit: \(credits)")
                            self.pointLabel.text = "\(credits)"
                            self.updateMyRewardPoints(reward_point: credits)
                            UserDefaults.standard.set(credits, forKey: CommonUtil.MYPOINTS)
                            if credits >= 1000 {
                                print("Unlock Rewards")
                            } else {
                                
                            }
                        }
                    }
                }
                if credits >= 1000 {
                    print("Unlock Rewards")
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        
        //        Branch.getInstance().redeemRewards(1000, callback: {(_ success: Bool, _ err: Error?) -> Void in
        //            print("redeem",err ?? "")
        //            if err == nil {
        //
        //                let newBalance: Int = Branch.getInstance().getCredits()
        //                let successMsg = "You redeemed 5 credits! You have \(Int(newBalance)) remaining."
        //
        //                let alertController = UIAlertController(title: "Success", message: successMsg, preferredStyle: UIAlertControllerStyle.alert) //Replace UIAlertControllerStyle.Alert by UIAlertControllerStyle.alert
        //
        //                let DestructiveAction = UIAlertAction(title: "cancel", style: UIAlertActionStyle.destructive) {
        //                    (result : UIAlertAction) -> Void in
        //                    print("Destructive")
        //                }
        //
        //                // Replace UIAlertActionStyle.Default by UIAlertActionStyle.default
        //
        //                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
        //                    (result : UIAlertAction) -> Void in
        //                    print("OK")
        //                }
        //
        //                alertController.addAction(DestructiveAction)
        //                alertController.addAction(okAction)
        //                self.present(alertController, animated: true, completion: nil)
        //            }
        //        })
    }
    
    @IBAction func inviteFriendButtonClick(_ sender: Any) {
        
    }
    
    func updateMyRewardPoints(reward_point: Int) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as? String ?? "abc")\",\"REWARD_POINTS\":\"\(reward_point)\"}", mod: "Users", actionType: "update-user-reward-points") { (response) in
            if response as? String != "error" {
                print("Player profile updated",response)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
        }
    }
    
}
