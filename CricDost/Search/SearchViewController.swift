//
//  SearchViewController.swift
//  CricDost
//
//  Created by Jit Goel on 7/9/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import SDWebImage

class SearchViewController: UIViewController, UISearchBarDelegate, ShowsAlert, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var scroll: UIScrollView!
    lazy var searchBar = UISearchBar(frame: CGRect.zero)
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var playerTableView: UITableView!
    @IBOutlet weak var teamTableView: UITableView!
    @IBOutlet weak var playerTitleLabel: UILabel!
    @IBOutlet weak var teamsTitleLabel: UILabel!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var filterViewheight: NSLayoutConstraint!
    @IBOutlet weak var matchesTitleLabel: UILabel!
    @IBOutlet weak var matchTableView: UITableView!
    @IBOutlet weak var playerTableHeightConstaint: NSLayoutConstraint!
    @IBOutlet weak var teamTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var matchTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var playerViewAllButton: UIButton!
    @IBOutlet weak var teamViewAllButton: UIButton!
    @IBOutlet weak var matchesViewAllButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var teamView: UIView!
    @IBOutlet weak var matchView: UIView!
    @IBOutlet weak var playerButton: UIButton!
    @IBOutlet weak var teamsButton: UIButton!
    @IBOutlet weak var matchesButton: UIButton!
    @IBOutlet weak var preventUserInteraction: UIView!
    @IBOutlet weak var filterButton: UIBarButtonItem!
    var isFilterOpen = false
    var playersList: NSArray = []
    var teamList: NSArray = []
    var matchesList: NSArray = []
    var currentMatchID = ""
    var searchKey = ""
    var currentPlayerPage = 1
    var currentTeamPage = 1
    var currentMatchPage = 1
    
    var currentAddressID: String?
    
    var viewAll = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
//            statusBar.backgroundColor = CommonUtil.themeRed
//        }
//        UIApplication.shared.statusBarStyle = .lightContent
        
        searchBar.placeholder = "Search"
        searchBar.sizeToFit()
        searchBar.delegate = self
        let cancelButtonAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        navBar.topItem?.titleView = searchBar
        
        navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navBar.shadowImage = UIImage()
        
        roundedCorner(buttons: [playerButton, teamsButton, matchesButton])
        
        NotificationCenter.default.addObserver(self, selector: #selector(getDefaultSearchResult), name: NSNotification.Name(rawValue: "getDefaultSearchResult"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(getPlayerProfileFromSearch), name: NSNotification.Name(rawValue: "getPlayerProfileFromSearch"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(getTeamProfileFromSearch), name: NSNotification.Name(rawValue: "getTeamProfileFromSearch"), object: nil)
        
            NotificationCenter.default.addObserver(self, selector: #selector(openMatchesDetailnSearchViewVC), name: NSNotification.Name(rawValue: "openMatchesDetailnSearchViewVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(dismissBlackScreen), name: NSNotification.Name(rawValue: "dismissBlackScreen"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(createTeamVCFunction), name: NSNotification.Name(rawValue: "createTeamVCFunction"), object: nil)
        
        let outerViewTap = UITapGestureRecognizer(target: self, action: #selector(outerViewFunction))
        backgroundView.addGestureRecognizer(outerViewTap)
        
        playerView.isHidden = true
        teamView.isHidden = true
        matchView.isHidden = true
        
        getDefaultSearchResult()
    }
    
    @objc func createTeamVCFunction() {
        backgroundView.isHidden = true
        let createTeamVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateTeamViewController") as? CreateTeamViewController
        createTeamVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        createTeamVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        createTeamVC?.isOnBoardUser = false
        self.present(createTeamVC!, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        CommonUtil.updateGATracker(screenName: "Search")
    }
    
    @objc func dismissBlackScreen() {
        backgroundView.isHidden = true
    }
    
    @IBAction func filterButtonClick(_ sender: Any) {
        if isFilterOpen {
            UIView.transition(with: filterView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.filterView.isHidden = true
                self.filterViewheight.constant = 0
            }, completion: nil)
        } else {
            UIView.transition(with: filterView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.filterView.isHidden = false
                self.filterViewheight.constant = 65
            }, completion: nil)
        }
        isFilterOpen = !isFilterOpen
    }
    
    @objc func outerViewFunction() {
        backgroundView.isHidden = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissMatchDetailView"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissMyProfileFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissTeamProfileFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissPlayerProfileFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissFixedMatchesFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissOpenMatchesFunction"), object: nil)
    }
    
    @objc func getTeamProfileFromSearch() {
        let selectedTeam = UserDefaults.standard.object(forKey: CommonUtil.SELECTEDTEAMFROMPLAYERPROFILE) as! Data
        let decodedselectedTeam = NSKeyedUnarchiver.unarchiveObject(with: selectedTeam) as! NSDictionary
        print(decodedselectedTeam)
        getTeamProfile(addressID: decodedselectedTeam["ADDRESS_ID"] as! String, address: decodedselectedTeam["ADDRESS"] as! String, teamId: decodedselectedTeam["TEAM_ID"] as! String)
    }
    
    @objc func getPlayerProfileFromSearch() {
        let selectedPlayer = UserDefaults.standard.object(forKey: CommonUtil.SELECTEDPLAYERFROMTEAMPROFILE) as! Data
        let decodedselectedPlayer = NSKeyedUnarchiver.unarchiveObject(with: selectedPlayer) as! NSDictionary
        print(decodedselectedPlayer)
        getPlayerProfile(addressID: decodedselectedPlayer["ADDRESS_ID"] as! String, address: decodedselectedPlayer["ADDRESS"] as! String, userId: decodedselectedPlayer["USER_ID"] as! String)
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.matchTableHeightConstraint.constant = self.matchTableView.contentSize.height
        self.teamTableHeightConstraint.constant = self.teamTableView.contentSize.height
        self.playerTableHeightConstaint.constant = self.playerTableView.contentSize.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == playerTableView {
            if viewAll == "Player" {
                return playersList.count
            } else {
                if playersList.count > 0 {
                    return 1
                } else {
                    return playersList.count
                }
            }
        } else if tableView == teamTableView {
            if viewAll == "Team" {
                return teamList.count
            } else {
                if teamList.count > 0 {
                    return 1
                } else {
                    return teamList.count
                }
            }
        } else {
            if viewAll == "Match" {
                return matchesList.count
            } else {
                if matchesList.count > 0 {
                    return 1
                } else {
                    return matchesList.count
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == playerTableView {
            let player = playersList[indexPath.row] as! NSDictionary
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "playerSearchCell", for: indexPath) as! PlayersSearchTableViewCell
            cell.nameLabel.text = player["FULL_NAME"] as? String
            cell.profileImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(player["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
            cell.setRating(rating: player["RATING"] as! String)
            cell.skillsLabel.text = player["SKILLS"] as? String
            cell.distanceLabel.text = "\(player["DISTANCE"] as! String)"
            return cell
        } else if tableView == teamTableView {
            let team = teamList[indexPath.row] as! NSDictionary
            let cell = tableView.dequeueReusableCell(withIdentifier: "teamsearchCell", for: indexPath) as! TeamsSearchTableViewCell
            cell.nameLabel.text = team["TEAM_NAME"] as? String
            cell.profileImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(team["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            cell.setRating(rating: team["RATING"] as! String)
            cell.distance.text = "\(team["DISTANCE"] as! String)"
            return cell
        } else {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "matchesSearchCell", for: indexPath) as! MatchesTableViewCell
            
            let match = (matchesList[indexPath.row] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
            let teamA = match["TEAM_A"] as! NSDictionary
            let teamB = match["TEAM_B"] as! NSDictionary
            
            cell.teamAName.text = teamA["TEAM_NAME"] as? String
            
            cell.teamAImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamA["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
            cell.locationLabel.text = match["ADDRESS"] as? String
            
            if match["TYPE"] as? String == "OPEN-MATCH" {
                cell.teamBImage.isHidden = true
                cell.teamBName.isHidden = true
                cell.joinMatchButton.isHidden = false
                cell.challengeButton.isHidden = false
                cell.numOfChallengers.isHidden = true
                
                if match["CAN_JOIN"] as! Int == 1 {
                    cell.joinMatchButton.isHidden = false
                } else {
                    cell.joinMatchButton.isHidden = true
                }
                
                if match["CAN_CHALLENGE"] as! Int == 1 {
                    cell.challengeButton.isHidden = false
                } else {
                    cell.challengeButton.isHidden = true
                }
                
                if match["CAN_EXIT"] as! Int == 1 || match["CANCEL_CHALLENGE"] as! Int == 1{
                    cell.exitButton.isHidden = false
                } else {
                    cell.exitButton.isHidden = true
                }
                
                if match["CAN_EXIT"] as! Int == 1 {
                    cell.exitButton.setTitle("Exit", for: .normal)
                }
                
                if match["CANCEL_CHALLENGE"] as! Int == 1 {
                    cell.exitButton.setTitle("Revoke Challenge", for: .normal)
                }
            } else {
                cell.teamBImage.isHidden = false
                cell.teamBName.isHidden = false
                cell.joinMatchButton.isHidden = true
                cell.challengeButton.isHidden = true
                cell.exitButton.isHidden = true
                if  teamB["POSITION_STATUS"] as! Int == 0 {
                    cell.teamBImage.image = nil
                    cell.teamBName.text = "Challengers"
                    cell.numOfChallengers.isHidden = false
                    cell.numOfChallengers.text = match["INTEREST_COUNT"] as? String
                } else {
                    cell.numOfChallengers.isHidden = true
                    cell.teamBImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(teamB["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
                    cell.teamBName.text = teamB["TEAM_NAME"] as? String
                }
            }
            
            cell.joinMatchButton.addTarget(self, action: #selector(joinMatchBUttonTapped), for: UIControlEvents.touchUpInside)
            cell.challengeButton.addTarget(self, action: #selector(challengeButtonTapped), for: UIControlEvents.touchUpInside)
            cell.exitButton.addTarget(self, action: #selector(exitButtonTapped), for: UIControlEvents.touchUpInside)
            cell.joinMatchButton.tag = indexPath.row
            cell.challengeButton.tag = indexPath.row
            cell.exitButton.tag = indexPath.row
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == playerTableView {
            let player = playersList[indexPath.row] as! NSDictionary
            if player["USER_ID"] as! String == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String {
                showMyProfile()
            } else {
                getPlayerProfile(addressID: player["ADDRESS_ID"] as! String, address: player["ADDRESS"] as! String, userId: player["USER_ID"] as! String)
            }
        } else if tableView == teamTableView {
            let team = teamList[indexPath.row] as! NSDictionary
            getTeamProfile(addressID: team["ADDRESS_ID"] as! String, address: team["ADDRESS"] as! String, teamId: team["TEAM_ID"] as! String)
        } else if tableView == matchTableView {
            let match = matchesList[indexPath.row] as! NSDictionary
            print(match)
            if (match["MATCH_DETAILS"] as! NSDictionary)["TYPE"] as! String == "OPEN-MATCH" {
                getOpenMatches(addressId: (match["MATCH_DETAILS"] as! NSDictionary)["ADDRESS_ID"] as! String, address: (match["MATCH_DETAILS"] as! NSDictionary)["ADDRESS"] as! String)
            } else {
                getFixedMatches(addressId: (match["MATCH_DETAILS"] as! NSDictionary)["ADDRESS_ID"] as! String, address: (match["MATCH_DETAILS"] as! NSDictionary)["ADDRESS"] as! String)
            }
        }
        searchBar.resignFirstResponder()
    }
    
    @IBAction func exitButtonTapped(_ sender: UIButton) {
        let match = (matchesList[sender.tag] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        currentMatchID = match["MATCH_ID"] as! String
        
        if sender.titleLabel?.text == "Exit" {
            activityIndicator.startAnimating()
            preventUserInteraction.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_TEAM_PLAYER_ID\": \"\(match["MATCH_TEAM_PLAYER_ID"] as! String)\"}", mod: "Match", actionType: "exit-match-team", callback: { (response: Any) in
                if response as? String != "error" {
                    if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                        self.getDefaultSearchResult()
                    } else {
                        self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                    }
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
            })
        } else {
            activityIndicator.startAnimating()
            preventUserInteraction.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_TEAM_ID\": \"\(match["MATCH_TEAM_ID"] as! String)\"}", mod: "Match", actionType: "revoke-match-team-challenge", callback: { (response: Any) in
                if response as? String != "error" {
                    if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                        self.getDefaultSearchResult()
                    } else {
                        self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                    }
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
            })
        }
    }
    
    @IBAction func joinMatchBUttonTapped(_ sender: UIButton) {
        let match = (matchesList[sender.tag] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        currentMatchID = match["MATCH_ID"] as! String
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\": \"\(currentMatchID)\"}", mod: "Match", actionType: "join-match", callback: { (response: Any) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    self.getDefaultSearchResult()
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        })
    }
    
    @IBAction func challengeButtonTapped(_ sender: UIButton) {
        let match = (matchesList[sender.tag] as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary
        currentMatchID = match["MATCH_ID"] as! String
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Match", actionType: "create-match-team-list") { (response: Any) in
            if response as? String != "error" {
                print(response)
                let teams = (response as! NSDictionary)["XSCData"] as! NSArray
                if teams.count == 0 {
                    let alert = UIAlertController(title: "No Teams", message: "You don't own a team. Create a team now?", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Create", style: UIAlertActionStyle.default, handler: { action in
                        let createTeamVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateTeamViewController") as? CreateTeamViewController
                        createTeamVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        createTeamVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        createTeamVC?.isOnBoardUser = false
                        self.present(createTeamVC!, animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let myTeamVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyTeamsViewController") as? MyTeamsViewController
                    myTeamVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    myTeamVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    myTeamVC?.teamList = teams
                    myTeamVC?.reloadSearchMatch = true
                    myTeamVC?.currentMatchID = self.currentMatchID
                    self.present(myTeamVC!, animated: true, completion: nil)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)  {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Do some search stuff
        print(searchText)
        searchBar.setShowsCancelButton(true, animated: true)
        searchKey = searchText
        getDefaultSearchResult()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchKey = ""
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        if playerView.isHidden || teamView.isHidden || matchView.isHidden {
            viewAll = ""
            if playersList.count != 0 {
                playerView.isHidden = false
                playerViewAllButton.isHidden = false
            }
            
            if teamList.count != 0 {
                teamView.isHidden = false
                teamViewAllButton.isHidden = false
            }
            
            if matchesList.count != 0 {
                matchView.isHidden = false
                matchesViewAllButton.isHidden = false
            }
            
            if playerViewAllButton.titleLabel?.text == "View All" && teamViewAllButton.titleLabel?.text == "View All" && matchesViewAllButton.titleLabel?.text == "View All" {
                self.dismiss(animated: true, completion: nil)
                UserDefaults.standard.set(false, forKey: CommonUtil.SearchOpenMatchesDetails)
            }
            
            filterButton.isEnabled = true
            filterButton.tintColor = UIColor.white
            
            playerViewAllButton.setTitle("View All", for: .normal)
            teamViewAllButton.setTitle("View All", for: .normal)
            matchesViewAllButton.setTitle("View All", for: .normal)
            playerButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.3)
            teamsButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.3)
            matchesButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.3)
            self.playerTableView.reloadData()
            self.teamTableView.reloadData()
            self.matchTableView.reloadData()
            self.matchTableHeightConstraint.constant = self.matchTableView.contentSize.height
            self.teamTableHeightConstraint.constant = self.teamTableView.contentSize.height
            self.playerTableHeightConstaint.constant = self.playerTableView.contentSize.height
        } else {
            self.dismiss(animated: true, completion: nil)
            UserDefaults.standard.set(false, forKey: CommonUtil.SearchOpenMatchesDetails)
        }
    }
    
    @IBAction func playerButtonClick(_ sender: Any) {
        viewAll = "Player"
        playerView.isHidden = false
        teamView.isHidden = true
        matchView.isHidden = true
        playerButton.backgroundColor = UIColor.black
        teamsButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.3)
        matchesButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.3)
    }
    
    @IBAction func playerViewAllButtonClick(_ sender: Any) {
        if playerViewAllButton.titleLabel?.text == "View All" {
            viewAll = "Player"
            playerView.isHidden = false
            teamView.isHidden = true
            matchView.isHidden = true
            filterButton.tintColor = UIColor.clear
            filterButton.isEnabled = false
            UIView.transition(with: filterView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.filterView.isHidden = true
                self.filterViewheight.constant = 0
            }, completion: nil)
            isFilterOpen = false
            playerViewAllButton.setTitle("View More", for: .normal)
            if self.currentPlayerPage == 0 {
                self.playerViewAllButton.isHidden = true
            }
        } else {
            searchPlayers()
        }
        
        playerTableView.reloadData()
        playerTableHeightConstaint.constant = playerTableView.contentSize.height
    }
    
    @IBAction func teamsButtonClick(_ sender: Any) {
        viewAll = "Team"
        playerView.isHidden = true
        teamView.isHidden = false
        matchView.isHidden = true
        teamsButton.backgroundColor = UIColor.black
        playerButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.3)
        matchesButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.3)
    }
    
    @IBAction func teamViewAllButtonClick(_ sender: Any) {
        if teamViewAllButton.titleLabel?.text == "View All" {
            viewAll = "Team"
            playerView.isHidden = true
            teamView.isHidden = false
            matchView.isHidden = true
            filterButton.tintColor = UIColor.clear
            filterButton.isEnabled = false
            UIView.transition(with: filterView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.filterView.isHidden = true
                self.filterViewheight.constant = 0
            }, completion: nil)
            isFilterOpen = false
            teamViewAllButton.setTitle("View More", for: .normal)
            if self.currentTeamPage == 0 {
                self.teamViewAllButton.isHidden = true
            }
        } else {
            searchTeams()
        }
        teamTableView.reloadData()
        teamTableHeightConstraint.constant = teamTableView.contentSize.height
    }
    
    @IBAction func matchButtonClick(_ sender: Any) {
        viewAll = "Match"
        playerView.isHidden = true
        teamView.isHidden = true
        matchView.isHidden = false
        matchesButton.backgroundColor = UIColor.black
        playerButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.3)
        teamsButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.3)
    }
    
    @IBAction func matchViewAllButtonCLick(_ sender: Any) {
        if matchesViewAllButton.titleLabel?.text == "View All" {
            viewAll = "Match"
            playerView.isHidden = true
            teamView.isHidden = true
            matchView.isHidden = false
            filterButton.tintColor = UIColor.clear
            filterButton.isEnabled = false
            UIView.transition(with: filterView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.filterView.isHidden = true
                self.filterViewheight.constant = 0
            }, completion: nil)
            isFilterOpen = false
            matchesViewAllButton.setTitle("View More", for: .normal)
            if self.currentMatchPage == 0 {
                self.matchesViewAllButton.isHidden = true
            }
        } else {
            searchMatches()
        }
        
        matchTableView.reloadData()
        matchTableHeightConstraint.constant = matchTableView.contentSize.height
    }
    
    @objc func getDefaultSearchResult() {
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        playersList = []
        teamList = []
        matchesList = []
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"SEARCH_KEY\":\"\(searchKey)\"}", mod: "Search", actionType: "search-all") { (response) in
            if response as? String != "error" {
               print(response)
                if ((response as! NSDictionary)["XSCStatus"] as! Int) == 0 {
                self.playersList = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["PLAYERS"] as! NSArray
                self.matchesList = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["MATCHES"] as! NSArray
                self.teamList = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["TEAMS"] as! NSArray
                self.currentPlayerPage = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["NEXT_PAGE"] as! Int
                self.currentTeamPage = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["NEXT_PAGE"] as! Int
                self.currentMatchPage = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["NEXT_PAGE"] as! Int
                
                if self.playersList.count == 1 || self.playersList.count == 0 {
                    self.playerViewAllButton.isHidden = true
                } else {
                    self.playerViewAllButton.isHidden = false
                }
                if self.matchesList.count == 1 || self.matchesList.count == 0 {
                    self.matchesViewAllButton.isHidden = true
                } else {
                    self.matchesViewAllButton.isHidden = false
                }
                if self.teamList.count == 1 || self.teamList.count == 0 {
                    self.teamViewAllButton.isHidden = true
                } else {
                    self.teamViewAllButton.isHidden = false
                }
                
                if self.playersList.count == 0 {
                    self.playerTitleLabel.text = "No Players Available"
                    self.playerView.isHidden = true
                    self.playerButton.isHidden = true
                } else {
                    self.playerTitleLabel.text = "Players"
                    self.playerView.isHidden = false
                    self.playerButton.isHidden = false
                }
                if self.matchesList.count == 0 {
                    self.matchesTitleLabel.text = "No Matches Available"
                    self.matchView.isHidden = true
                    self.matchesButton.isHidden = true
                } else {
                    self.matchesTitleLabel.text = "Matches"
                    self.matchView.isHidden = false
                    self.matchesButton.isHidden = false
                }
                if self.teamList.count == 0 {
                    self.teamsTitleLabel.text = "No Teams Available"
                    self.teamView.isHidden = true
                    self.teamsButton.isHidden = true
                } else {
                    self.teamsTitleLabel.text = "Teams"
                    self.teamView.isHidden = false
                    self.teamsButton.isHidden = false
                }
                
                self.playerTableView.reloadData()
                self.teamTableView.reloadData()
                self.matchTableView.reloadData()
                self.matchesButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.3)
                self.playerButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.3)
                self.teamsButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.3)
                self.playerViewAllButton.setTitle("View All", for: .normal)
                self.teamViewAllButton.setTitle("View All", for: .normal)
                self.matchesViewAllButton.setTitle("View All", for: .normal)
                self.matchTableHeightConstraint.constant = self.matchTableView.contentSize.height
                self.teamTableHeightConstraint.constant = self.teamTableView.contentSize.height
                self.playerTableHeightConstaint.constant = self.playerTableView.contentSize.height
                } else {
                    self.showAlert(title: "Oops!", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        }
    }
    
    func searchPlayers() {
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"SEARCH_KEY\":\"\(searchKey)\",\"CURRENT_PAGE\":\"\(currentPlayerPage)\"}", mod: "Search", actionType: "players") { (response) in
            if response as? String != "error" {
                print(response)
                let playersList1 = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["PLAYERS"] as! NSArray
                self.currentPlayerPage = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["NEXT_PAGE"] as! Int

                print("Player count before", self.playersList.count)
                let list: NSMutableArray = []
                for player in self.playersList {
                    list.add(player)
                }
                for player in playersList1 {
                    list.add(player)
                }
                self.playersList = list
                print("Player count after", self.playersList.count)
                if self.playersList.count == 1 || self.playersList.count == 0 {
                    self.playerViewAllButton.isHidden = true
                } else {
                    self.playerViewAllButton.isHidden = false
                }
                if self.matchesList.count == 1 || self.matchesList.count == 0 {
                    self.matchesViewAllButton.isHidden = true
                } else {
                    self.matchesViewAllButton.isHidden = false
                }
                if self.teamList.count == 1 || self.teamList.count == 0 {
                    self.teamViewAllButton.isHidden = true
                } else {
                    self.teamViewAllButton.isHidden = false
                }
                
                if self.playersList.count == 0 {
                    self.playerTitleLabel.text = "No Players Available"
                    self.playerView.isHidden = true
                    self.playerButton.isHidden = true
                } else {
                    self.playerTitleLabel.text = "Players"
                    self.playerView.isHidden = false
                    self.playerButton.isHidden = false
                }
                if self.matchesList.count == 0 {
                    self.matchesTitleLabel.text = "No Matches Available"
                    self.matchView.isHidden = true
                    self.matchesButton.isHidden = true
                } else {
                    self.matchesTitleLabel.text = "Matches"
                    self.matchesButton.isHidden = false
                }
                if self.teamList.count == 0 {
                    self.teamsTitleLabel.text = "No Teams Available"
                    self.teamView.isHidden = true
                    self.teamsButton.isHidden = true
                } else {
                    self.teamsTitleLabel.text = "Teams"
                    self.teamsButton.isHidden = false
                }
                
                if self.currentPlayerPage == 0 {
                    self.playerViewAllButton.isHidden = true
                }
                
                self.playerTableView.reloadData()
                self.teamTableView.reloadData()
                self.matchTableView.reloadData()
                self.matchTableHeightConstraint.constant = self.matchTableView.contentSize.height
                self.teamTableHeightConstraint.constant = self.teamTableView.contentSize.height
                self.playerTableHeightConstaint.constant = self.playerTableView.contentSize.height
                
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        }
    }
    
    func searchTeams() {
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"SEARCH_KEY\":\"\(searchKey)\",\"CURRENT_PAGE\":\"\(currentTeamPage)\"}", mod: "Search", actionType: "teams") { (response) in
            if response as? String != "error" {
                print(response)
                let teamList1 = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["TEAMS"] as! NSArray
                self.currentTeamPage = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["NEXT_PAGE"] as! Int
                
                print("Player count before", self.teamList.count)
                let list: NSMutableArray = []
                for team in self.teamList {
                    list.add(team)
                }
                for team in teamList1 {
                    list.add(team)
                }
                self.teamList = list
                print("Player count after", self.teamList.count)
                if self.playersList.count == 1 || self.playersList.count == 0 {
                    self.playerViewAllButton.isHidden = true
                } else {
                    self.playerViewAllButton.isHidden = false
                }
                if self.matchesList.count == 1 || self.matchesList.count == 0 {
                    self.matchesViewAllButton.isHidden = true
                } else {
                    self.matchesViewAllButton.isHidden = false
                }
                if self.teamList.count == 1 || self.teamList.count == 0 {
                    self.teamViewAllButton.isHidden = true
                } else {
                    self.teamViewAllButton.isHidden = false
                }
                
                if self.playersList.count == 0 {
                    self.playerTitleLabel.text = "No Players Available"
                    self.playerView.isHidden = true
                    self.playerButton.isHidden = true
                } else {
                    self.playerTitleLabel.text = "Players"
                    self.playerButton.isHidden = false
                }
                if self.matchesList.count == 0 {
                    self.matchesTitleLabel.text = "No Matches Available"
                    self.matchView.isHidden = true
                    self.matchesButton.isHidden = true
                } else {
                    self.matchesTitleLabel.text = "Matches"
                    self.matchesButton.isHidden = false
                    
                }
                if self.teamList.count == 0 {
                    self.teamsTitleLabel.text = "No Teams Available"
                    self.teamView.isHidden = true
                    self.teamsButton.isHidden = true
                } else {
                    self.teamsTitleLabel.text = "Teams"
                    self.teamView.isHidden = false
                    self.teamsButton.isHidden = false
                }
                
                if self.currentTeamPage == 0 {
                    self.teamViewAllButton.isHidden = true
                }
                
                self.playerTableView.reloadData()
                self.teamTableView.reloadData()
                self.matchTableView.reloadData()
                self.matchTableHeightConstraint.constant = self.matchTableView.contentSize.height
                self.teamTableHeightConstraint.constant = self.teamTableView.contentSize.height
                self.playerTableHeightConstaint.constant = self.playerTableView.contentSize.height
                
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        }
    }
    
    func searchMatches() {
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"SEARCH_KEY\":\"\(searchKey)\",\"CURRENT_PAGE\":\"\(currentTeamPage)\"}", mod: "Search", actionType: "matches") { (response) in
            if response as? String != "error" {
                print(response)
                let matchList1 = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["MATCHES"] as! NSArray
                self.currentMatchPage = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["NEXT_PAGE"] as! Int
                
                print("Player count before", self.matchesList.count)
                let list: NSMutableArray = []
                for team in self.matchesList {
                    list.add(team)
                }
                for team in matchList1 {
                    list.add(team)
                }
                self.matchesList = list
                print("Player count after", self.matchesList.count)
                if self.playersList.count == 1 || self.playersList.count == 0 {
                    self.playerViewAllButton.isHidden = true
                } else {
                    self.playerViewAllButton.isHidden = false
                }
                if self.matchesList.count == 1 || self.matchesList.count == 0 {
                    self.matchesViewAllButton.isHidden = true
                } else {
                    self.matchesViewAllButton.isHidden = false
                }
                if self.teamList.count == 1 || self.teamList.count == 0 {
                    self.teamViewAllButton.isHidden = true
                } else {
                    self.teamViewAllButton.isHidden = false
                }
                
                if self.playersList.count == 0 {
                    self.playerTitleLabel.text = "No Players Available"
                    self.playerView.isHidden = true
                    self.playerButton.isHidden = true
                } else {
                    self.playerTitleLabel.text = "Players"
                    self.playerButton.isHidden = false
                }
                if self.matchesList.count == 0 {
                    self.matchesTitleLabel.text = "No Matches Available"
                    self.matchView.isHidden = true
                    self.matchesButton.isHidden = true
                } else {
                    self.matchesTitleLabel.text = "Matches"
                    self.matchView.isHidden = false
                    self.matchesButton.isHidden = false
                }
                if self.teamList.count == 0 {
                    self.teamsTitleLabel.text = "No Teams Available"
                    self.teamView.isHidden = true
                    self.teamsButton.isHidden = true
                } else {
                    self.teamsTitleLabel.text = "Teams"
                    self.teamsButton.isHidden = false
                }
                
                if self.currentMatchPage == 0 {
                    self.matchesViewAllButton.isHidden = true
                }
                
                self.playerTableView.reloadData()
                self.teamTableView.reloadData()
                self.matchTableView.reloadData()
                self.matchTableHeightConstraint.constant = self.matchTableView.contentSize.height
                self.teamTableHeightConstraint.constant = self.teamTableView.contentSize.height
                self.playerTableHeightConstaint.constant = self.playerTableView.contentSize.height
                
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        }
    }
    
    func getPlayerProfile(addressID: String, address: String, userId: String) {
        currentAddressID = addressID
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"PLAYER\",\"USER_ID\":\"\(userId)\"}", mod: "Player", actionType: "dashborad-player-profile") { (response: Any) in
                //            print(response)
                if response as? String != "error" {
                    self.showPlayerProfile(response: response, address: address, isCalledFromSearch: true)
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
                self.preventUserInteraction.isHidden = true
            }
    }
    
    private func showPlayerProfile(response: Any, address: String, isCalledFromSearch: Bool) {
        backgroundView.isHidden = false
        guard
            let playerVC = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "PlayerProfileViewController") as? PlayerProfileViewController
            else { return }
        playerVC.playerDetails = response
        playerVC.address = address
        playerVC.currentAddressID = currentAddressID
        playerVC.isCalledFromSearch = isCalledFromSearch
        addPullUpController(playerVC)
        navigationController?.setNavigationBarHidden(navigationController?.isNavigationBarHidden == true, animated: true)
    }
    
    func getTeamProfile(addressID: String, address: String,teamId: String) {
        currentAddressID = addressID
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"TEAM\",\"TEAM_ID\":\"\(teamId)\"}", mod: "Team", actionType: "dashborad-team-profile") { (response: Any) in
                print(response)
                if response as? String != "error" {
                    self.showTeamProfile(response: response, address: address, isCalledFromSearch: true)
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
                self.preventUserInteraction.isHidden = true
            }
    }
    
    private func showTeamProfile(response: Any, address: String, isCalledFromSearch: Bool) {
        backgroundView.isHidden = false
        guard
            let teamVC = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "TeamProfileViewController") as? TeamProfileViewController
            else { return }
        teamVC.teamDetails = response
        teamVC.address = address
        teamVC.currentAddressID = currentAddressID
        teamVC.isCalledFromSearch = isCalledFromSearch
        addPullUpController(teamVC)
        navigationController?.setNavigationBarHidden(navigationController?.isNavigationBarHidden == true, animated: true)
    }
    
    func getOpenMatches(addressId: String, address: String) {
        currentAddressID = addressId
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"OPEN-MATCH\",\"ADDRESS_ID\":\"\(addressId)\"}", mod: "Map", actionType: "venue-items") { (response) in
            //            print(response)
            if response as? String != "error" {
                self.showOpenMatches(response: response, address: address)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        }
    }
    
    private func showOpenMatches(response: Any, address: String) {
        backgroundView.isHidden = false
        guard
            let openMatchesVC = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "OpenMatchesViewController") as? OpenMatchesViewController
            else { return }
        openMatchesVC.openMatchesDetails = response
        openMatchesVC.address = address
        openMatchesVC.currentAddressID = currentAddressID
        //        playerVC.isCalledFromTeamProfile = isCalledFromTeamProfile
        addPullUpController(openMatchesVC)
    }
    
    func getFixedMatches(addressId: String, address: String) {
        currentAddressID = addressId
        activityIndicator.startAnimating()
        preventUserInteraction.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"FIXED-MATCH\",\"ADDRESS_ID\":\"\(addressId)\"}", mod: "Map", actionType: "venue-items") { (response) in
            //            print(response)
            if response as? String != "error" {
                self.showFixedMatches(response: response, address: address)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.preventUserInteraction.isHidden = true
        }
    }
    
    private func showFixedMatches(response: Any, address: String) {
        backgroundView.isHidden = false
        guard
            let fixedMatchesVC = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "FixedMatchesViewController") as? FixedMatchesViewController
            else { return }
        fixedMatchesVC.fixedMatchesDetails = response
        fixedMatchesVC.address = address
        fixedMatchesVC.currentAddressID = currentAddressID
        //        playerVC.isCalledFromTeamProfile = isCalledFromTeamProfile
        addPullUpController(fixedMatchesVC)
    }
    
    @objc func showMyProfile(){
        guard
            let myProfileVC = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "MyProfileViewController") as? MyProfileViewController
            else { return }
        addPullUpController(myProfileVC)
    }
    
    func roundedCorner(buttons: [UIButton]) {
        for button in buttons {
            button.layer.cornerRadius = 3
            button.clipsToBounds = true
        }
    }
    
    @objc func openMatchesDetailnSearchViewVC(_ notification: NSNotification) {
        
        guard
            let matchDetailVC = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "OpenMatchesDetailsViewController") as? OpenMatchesDetailsViewController
            else { return }
        
        matchDetailVC.matchInfo = notification.userInfo![AnyHashable("data")] as? NSDictionary
        addPullUpController(matchDetailVC)
    }
    
}






