//
//  TournamentPaymentDetailsViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 8/27/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import IQKeyboardManager

class TournamentPaymentDetailsViewController: UIViewController , ShowsAlert, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource {
    
    
    
    @IBOutlet weak var dismissViewHolder: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var preventUserInteractionView: UIView!
    @IBOutlet weak var confirmAccountNumber: UITextField!
    var maxiEntryDate: Date?
    
    @IBOutlet weak var tourEnterPasswordView: UIView!
    @IBOutlet weak var tourEnterPasswordTextField: UITextField!
    @IBOutlet weak var tourEnterPasswordNextButton: UIButton!
    @IBOutlet weak var tourEnterPassAgreeButton: UIButton!
    @IBOutlet weak var tourEnterPassTermsAndConditionLabel: UILabel!
    @IBOutlet weak var tourEnterPinForgotPassword: UILabel!
    
    @IBOutlet weak var tourCreatePasswordView: UIView!
    @IBOutlet weak var tourNewPasswordTextField: UITextField!
    @IBOutlet weak var tourConfirmPasswordTextField: UITextField!
    @IBOutlet weak var tourCreatePasswordNextButton: UIButton!
    @IBOutlet weak var tourCreatePassAgreeButton: UIButton!
    @IBOutlet weak var tourCreatePassTermsAndConditionLabel: UILabel!
    
    @IBOutlet weak var tourTeamEntryFeesView: UIView!
    @IBOutlet weak var tourTeamEntryFeesTextField: UITextField!
    @IBOutlet weak var tourTeamEntryLastDateTextField: UITextField!
    @IBOutlet weak var tourTeamEntryNextButton: UIButton!
    
    @IBOutlet weak var tourAwardsPrizeView: UIView!
    @IBOutlet weak var tourAwardsPrizeTitleView: UIView!
    @IBOutlet weak var tourSelectAwardsView: UIView!
    @IBOutlet weak var tourSelectAwardsTextField: UITextField!
    @IBOutlet weak var tourselectAwardsButton: UIButton!
    @IBOutlet weak var tourSelectedAwardsTableView: UITableView!
    @IBOutlet weak var tourAwardsPrizeNextButton: UIButton!
    
    var countryPicker = UIPickerView()
    @IBOutlet weak var countryCodeTextField: UITextField!
    
    @IBOutlet weak var tourAccountDetailsView: UIView!
    @IBOutlet weak var tourAccountTitleView: UIView!
    @IBOutlet weak var tourAccountNumberTextField: UITextField!
    @IBOutlet weak var tourAccountHolderName: UITextField!
    @IBOutlet weak var tourIFSCCodeTextField: UITextField!
    @IBOutlet weak var tourMobileNumberTextField: UITextField!
    @IBOutlet weak var tourSubmitButton: UIButton!
    
    @IBOutlet weak var keyboardHeightEnterPasswordConstraint: NSLayoutConstraint!
    @IBOutlet weak var keyboardHeightCreatePasswordConstraint: NSLayoutConstraint!
    @IBOutlet weak var keyboardHeightTeamEntryFeesConstraint: NSLayoutConstraint!
    @IBOutlet weak var keyboardHeightAccountDetailsConstraint: NSLayoutConstraint!
    
    var tourAwardsPickerView: UIPickerView?
    let tourAwardsPickerNumberValues = ["Best BatsMan", "Best Bowler", "best wicket keeper", "Man of the Match"]
    var awardsReceived: NSArray = []
    var UnSelectedAwards: NSMutableArray = []
    var selectedAwards: NSMutableArray = []
    var tempCurrentAwardsID: String?
    var rupeesTF: String?
    
    var tourTeamEntryLastDatepicker: UIDatePicker?
    var tourTeamEntryLasttDate: String?
    var tourTeamEntryFee: String?
    
    
    var hasPin: Bool?
    var tournament_ID: String?
    var createTourVC: CreateTournamentViewController?
    
    let awardsChoosed: NSMutableArray = []
    
    @IBOutlet weak var fixOneTitleView: UIView!
    @IBOutlet weak var fixture1View: UIView!
    @IBOutlet weak var fixture2View: UIView!
    @IBOutlet weak var fixture1ContinueButton: UIButton!
    @IBOutlet weak var fixture2ContinueButton: UIButton!
    @IBOutlet weak var f1View1: UIView!
    @IBOutlet weak var f2View1: UIView!
    @IBOutlet weak var f2View2: UIView!
    @IBOutlet weak var f2View3: UIView!
    @IBOutlet weak var group1Label: UILabel!
    @IBOutlet weak var group2Label: UILabel!
    @IBOutlet weak var group3Label: UILabel!
    @IBOutlet weak var group4Label: UILabel!
    
    var fixtureViews: [UIView] = []
    @IBOutlet weak var fixTwoTitleView: UIView!
    
    @IBOutlet weak var f1Match1ImageView: UIImageView!
    @IBOutlet weak var f1Match2ImageView: UIImageView!
    @IBOutlet weak var f1Match3ImageView: UIImageView!
    
    var numberOfTeams: String?
    var isFixtureCreated = false
    
    @IBOutlet weak var f2Match1ImageView: UIImageView!
    @IBOutlet weak var f2Match2ImageView: UIImageView!
    @IBOutlet weak var f2Match3ImageView: UIImageView!
    @IBOutlet weak var f2Match4ImageView: UIImageView!
    @IBOutlet weak var f2Match5ImageView: UIImageView!
    @IBOutlet weak var f2Match6ImageView: UIImageView!
    @IBOutlet weak var f2Match7ImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fixtureViews = [f1View1, f2View1, f2View2, f2View3]
        
        CommonUtil.buttonRoundedCorners(buttons: [tourEnterPasswordNextButton,fixture1ContinueButton,fixture2ContinueButton,tourCreatePasswordNextButton,tourTeamEntryNextButton,tourAwardsPrizeNextButton,tourSubmitButton])
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [tourEnterPasswordView,tourCreatePasswordView,tourTeamEntryFeesView,tourAwardsPrizeView,tourAccountDetailsView,fixture1View,fixture2View])
        
        CommonUtil.imageRoundedCorners(imageviews: [f1Match1ImageView,f1Match2ImageView,f1Match3ImageView,f2Match1ImageView,f2Match2ImageView,f2Match2ImageView,f2Match3ImageView,f2Match4ImageView,f2Match5ImageView,f2Match6ImageView,f2Match7ImageView])
        
        let dismissGesture = UITapGestureRecognizer(target: self, action: #selector(dissmissViewHolderTapped))
        dismissViewHolder.isUserInteractionEnabled = true
        dismissViewHolder.addGestureRecognizer(dismissGesture)
        
        let tourEnterPinForgotPasswordGesture = UITapGestureRecognizer(target: self, action: #selector(tourEnterPinForgotPasswordTapped))
        tourEnterPinForgotPassword.isUserInteractionEnabled = true
        tourEnterPinForgotPassword.addGestureRecognizer(tourEnterPinForgotPasswordGesture)
        
        IQKeyboardManager.shared().isEnabled = true
        
        tourCreatePassAgreeButton.backgroundColor = .clear
        tourCreatePassAgreeButton.layer.cornerRadius = 0
        tourCreatePassAgreeButton.layer.borderWidth = 1
        tourCreatePassAgreeButton.layer.borderColor = UIColor.black.cgColor
        
        tourEnterPassAgreeButton.backgroundColor = .clear
        tourEnterPassAgreeButton.layer.cornerRadius = 0
        tourEnterPassAgreeButton.layer.borderWidth = 1
        tourEnterPassAgreeButton.layer.borderColor = UIColor.black.cgColor
        
        let tourTermsAndConditionGesture = UITapGestureRecognizer(target: self, action: #selector(tourTermsAndConditionTapped))
        tourEnterPassTermsAndConditionLabel.isUserInteractionEnabled = true
        tourEnterPassTermsAndConditionLabel.addGestureRecognizer(tourTermsAndConditionGesture)
        
        let tourCreatePinTermsAndConditionGesture = UITapGestureRecognizer(target: self, action: #selector(tourCreatePinTermsAndConditionTapped))
        tourCreatePassTermsAndConditionLabel.isUserInteractionEnabled = true
        tourCreatePassTermsAndConditionLabel.addGestureRecognizer(tourCreatePinTermsAndConditionGesture)
        
        tourEnterPasswordNextButton.isEnabled = false
        tourEnterPasswordNextButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        tourCreatePasswordNextButton.isEnabled = false
        tourCreatePasswordNextButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        checkPinCreatedOrNot()
        
        countryPicker.delegate = self
        countryPicker.dataSource = self
        
        countryCodeTextField.inputView = countryPicker
        
        //        NotificationCenter.default.addObserver(self,
        //                                               selector: #selector(self.keyboardNotification(notification:)),
        //                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
        //                                               object: nil)
        //
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let curentDate = Date()
        tourTeamEntryLastDatepicker = UIDatePicker()
        tourTeamEntryLastDatepicker?.minimumDate = curentDate
        tourTeamEntryLastDatepicker?.maximumDate = maxiEntryDate
        tourTeamEntryLastDateTextField.inputView = tourTeamEntryLastDatepicker
        tourTeamEntryLastDateTextField.text = dateFormatter.string(from: Date())
        tourTeamEntryLastDateTextField.tintColor = .clear
        tourTeamEntryLastDatepicker?.addTarget(self, action: #selector(handleTourTeamEntryLastDatePicker), for: UIControlEvents.valueChanged)
        tourTeamEntryLastDatepicker?.datePickerMode = .date
        tourTeamEntryLastDateTextField.text = "DD/MM/YYYY"
        
        let tourLastDateToolBar = UIToolbar().ToolbarPiker(mySelect: #selector(tourLastDateDismissPicker))
        tourTeamEntryLastDateTextField.inputAccessoryView = tourLastDateToolBar
        
        tourAwardsPickerView = UIPickerView()
        tourAwardsPickerView?.dataSource = self
        tourAwardsPickerView?.delegate = self
        
        tourSelectAwardsTextField.inputView = tourAwardsPickerView
        tourSelectAwardsTextField.text = "Select Awards"
        
        let tourAwardsToolBar = UIToolbar().ToolbarPiker(mySelect: #selector(tourSelectRulesDismissPicker))
        tourSelectAwardsTextField.inputAccessoryView = tourAwardsToolBar
        
        
        
        getAwardList()
        
        switch numberOfTeams {
        case "8":   group1Label.text = "2"
                    group2Label.text = "2"
                    group3Label.text = "2"
                    group4Label.text = "2"
            break
        case "16":  group1Label.text = "4"
                    group2Label.text = "4"
                    group3Label.text = "4"
                    group4Label.text = "4"
            break
        case "32":  group1Label.text = "8"
                    group2Label.text = "8"
                    group3Label.text = "8"
                    group4Label.text = "8"
            break
        case "64":  group1Label.text = "16"
                    group2Label.text = "16"
                    group3Label.text = "16"
                    group4Label.text = "16"
            break
        default:    group1Label.isHidden = true
                    group2Label.isHidden = true
                    group3Label.isHidden = true
                    group4Label.isHidden = true
            break
        }
        
        countryCodeTextField.text = UserDefaults.standard.object(forKey: CommonUtil.COUNTRY_CODE) as? String
        tourMobileNumberTextField.text = UserDefaults.standard.object(forKey: CommonUtil.MOBILE_NUMBER) as? String
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if !isFixtureCreated {
            for view in fixtureViews {
                view.addLeftBorderWithColor(color: UIColor(red:0.18, green:0.62, blue:0.82, alpha:1.0), width: 2.0)
                view.addRightBorderWithColor(color: UIColor(red:0.18, green:0.62, blue:0.82, alpha:1.0), width: 2.0)
                view.addBottomBorderWithColor(color: UIColor(red:0.18, green:0.62, blue:0.82, alpha:1.0), width: 2.0)
            }
            isFixtureCreated = true
        }
        tourAwardsPrizeTitleView.roundCornersView([.topLeft,.topRight], radius: 5)
        tourAccountTitleView.roundCornersView([.topLeft,.topRight], radius: 5)
        
        fixOneTitleView.roundCornersView([.topLeft,.topRight], radius: 5)
        fixTwoTitleView.roundCornersView([.topRight,.topLeft], radius: 5)
    }
    
    @objc func dissmissViewHolderTapped(){
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openNearByTournament"), object: nil)
        })
    }
    
    @IBAction func tourCreatePasswordNextButtonClick(_ sender: Any) {
        view.endEditing(true)
        print("create password button clicked")
        if tourNewPasswordTextField.isReallyEmpty && tourConfirmPasswordTextField.isReallyEmpty {
            self.tourCreatePasswordView.shake()
            self.showAlert(title: "Missing Info", message: "Please enter CDT Pin")
        } else if (tourNewPasswordTextField.text != tourConfirmPasswordTextField.text) {
            self.tourCreatePasswordView.shake()
            self.showAlert(title: "Mismatch Password", message: "Please enter correct Password ")
        } else {
            activityIndicator.startAnimating()
            preventUserInteractionView.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"USER_ID\":\"\(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String)\",\"PIN\":\"\(tourNewPasswordTextField.text!)\"}", mod: "Tournament", actionType: "create-password") { (response) in
                if response as? String != "error" {
                    if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                        print(response)
                        UIView.transition(with: self.tourCreatePasswordView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                            self.tourCreatePasswordView.isHidden = true
                        })
                        
                        if self.numberOfTeams == "4" {
                            UIView.transition(with: self.tourTeamEntryFeesView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                                self.fixture1View.isHidden = false
                            })
                        } else {
                            UIView.transition(with: self.tourTeamEntryFeesView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                                self.fixture2View.isHidden = false
                            })
                        }
                    } else {
                        print((response as! NSDictionary)["XSCMessage"] as! String)
                    }
                } else {
                    print("Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
                self.preventUserInteractionView.isHidden = true
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tourNewPasswordTextField || textField == tourConfirmPasswordTextField || textField == tourEnterPasswordTextField {
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else if textField == tourAccountNumberTextField {
            let maxLength = 18
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else if textField == confirmAccountNumber {
            let maxLength = 18
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else if textField == tourIFSCCodeTextField {
            let maxLength = 11
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else if textField == tourMobileNumberTextField {
            let maxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else if textField == tourTeamEntryFeesTextField {
            let maxLength = 6
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else if textField.tag != 0 {
            let maxLength = 6
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else {
            return true
        }
    }
    
    @IBAction func closeButtonClick(_ sender: Any) {
        self.tourAccountDetailsView.shake()
        let alert = UIAlertController(title: "Close", message: "Are you sure you want to close? All your entered info will be lost", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            self.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openNearByTournament"), object: nil)
            })
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func tourFixtureOneBackButton(_ sender: Any) {
        print("fixture one")
        self.fixture1View.shake()
        let alert = UIAlertController(title: "Close", message: "Are you sure you want to close? All your entered info will be lost", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            self.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openNearByTournament"), object: nil)
            })
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func tourFixtureTwoBackButton(_ sender: Any) {
        print("fixture two")
        self.fixture2View.shake()
        let alert = UIAlertController(title: "Close", message: "Are you sure you want to close? All your entered info will be lost", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
            self.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openNearByTournament"), object: nil)
            })
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func tourEntryFeesBackButton(_ sender: Any) {
        print("tournament entry fees")
        tourTeamEntryFeesTextField.text = ""
        tourTeamEntryLastDateTextField.text = "DD/MM/YYYY"
        UIView.transition(with: self.tourTeamEntryFeesView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tourTeamEntryFeesView.isHidden = true
        })
        
        if self.numberOfTeams == "4" {
            UIView.transition(with: self.fixture1View, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.fixture1View.isHidden = false
            })
        } else {
            UIView.transition(with: self.fixture2View, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.fixture2View.isHidden = false
            })
        }
//        let alert = UIAlertController(title: "Close", message: "Are you sure you want to close? All your entered info will be lost", preferredStyle: UIAlertControllerStyle.alert)
//        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
//            self.dismiss(animated: true, completion: {
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openNearByTournament"), object: nil)
//            })
//        }))
//        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
//
//        }))
//
//        self.present(alert, animated: true, completion: nil)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag != 0 {
            print("didEnd Editing", textField.text,textField.tag)
            UserDefaults.standard.set(textField.text, forKey: "AWARDSID_\(textField.tag)")
            print(UserDefaults.standard.object(forKey: "AWARDSID_\(textField.tag)") as! String)
        }
    }
    
    @IBAction func tourTeamEntryNextButtonClick(_ sender: Any) {
        view.endEditing(true)
        if tourTeamEntryFeesTextField.isReallyEmpty  {
            self.tourTeamEntryFeesView.shake()
            self.showAlert(title: "Missing Info", message: "Please enter team entry fees")
        } else if tourTeamEntryLastDateTextField.text == "DD/MM/YYYY"{
            self.tourTeamEntryFeesView.shake()
            self.showAlert(title: "Missing Info", message: "Please enter team entry last date")
        } else {
            tourTeamEntryLasttDate = tourTeamEntryLastDateTextField.text
            tourTeamEntryFee = tourTeamEntryFeesTextField.text
            UIView.transition(with: self.tourTeamEntryFeesView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tourTeamEntryFeesView.isHidden = true
            })
            UIView.transition(with: self.tourAwardsPrizeView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tourAwardsPrizeView.isHidden = false
            })
        }
    }
    
    @IBAction func tourSubmitButtonClick(_ sender: Any) {
        view.endEditing(true)
        if tourAccountNumberTextField.isReallyEmpty || tourAccountHolderName.isReallyEmpty || tourIFSCCodeTextField.isReallyEmpty || tourMobileNumberTextField.isReallyEmpty {
            self.tourAccountDetailsView.shake()
            self.showAlert(title: "Missing Info", message: "Please enter missing details to create tournament")
        } else if ((tourAccountNumberTextField.text)?.count)! < 6 || ((tourAccountNumberTextField.text)?.count)! > 18 {
            self.tourAccountDetailsView.shake()
            self.showAlert(title: "Invalid Account Number", message: "Please enter a valid account number")
        } else if confirmAccountNumber.text != tourAccountNumberTextField.text {
            self.tourAccountDetailsView.shake()
            self.showAlert(title: "Mismatch", message: "Account number doesn't match")
        } else if ((tourIFSCCodeTextField.text)?.count)! != 11 {
            self.tourAccountDetailsView.shake()
            self.showAlert(title: "Invalid IFSC Code", message: "Please enter a valid ifsc code")
        } else if ((tourMobileNumberTextField.text)?.count)! < 6 || ((tourMobileNumberTextField.text)?.count)! > 15 {
            self.tourAccountDetailsView.shake()
            self.showAlert(title: "Invalid Mobile Number", message: "Please enter a valid Mobile Number")
        } else if countryCodeTextField.isReallyEmpty {
            self.tourAccountDetailsView.shake()
            self.showAlert(title: "Country Code", message: "Please select your country code")
        } else {
            activityIndicator.startAnimating()
            preventUserInteractionView.isHidden = false
            do {
                let data = try JSONSerialization.data(withJSONObject: awardsChoosed)
                let dataString = String(data: data, encoding: .utf8)!
                print(dataString)
                tourTeamEntryLasttDate = CommonUtil.convertDateFormater(tourTeamEntryLasttDate!)
                CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TOURNAMENT_ID\":\"\(tournament_ID!)\",\"ENTRANCE_FEE\":\"\(tourTeamEntryFee!)\",\"LAST_DATE_FOR_ENTRY\":\"\(tourTeamEntryLasttDate!)\",\"ACCOUNT_NUMBER\":\"\(tourAccountNumberTextField.text!)\",\"ACCOUNT_HOLDER_NAME\":\"\(tourAccountHolderName.text!)\",\"IFSC_CODE\":\"\(tourIFSCCodeTextField.text!)\",\"CONTACT_NUMBER\":\"\(tourMobileNumberTextField.text!)\",\"TOURNAMENT_AWARDS\":\(dataString),\"COUNTRY_CODE\":\"\(countryCodeTextField.text!)\"}", mod: "Tournament", actionType: "update-awards") { (response) in
                    
                    print(response)
                    if response as? String != "error" {
                        if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                            for award in self.awardsReceived {
                                UserDefaults.standard.removeObject(forKey: "AWARDSID_\((award as! NSDictionary)["AWARD_ID"] as! String)")
                                UserDefaults.standard.removeObject(forKey: "AWARDSPRIZE_\((award as! NSDictionary)["AWARD_ID"] as! String)")
                                self.UnSelectedAwards.add(award)
                            }
                            let alert = UIAlertController(title: "Message", message: "Tournament has been created successfully!", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                                self.dismiss(animated: true, completion: {
                                    self.dismiss(animated: true, completion: {
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openNearByTournament"), object: nil)
                                    })
                                })
                            }))
                            self.present(alert, animated: true, completion: nil)

                        } else {
                            self.showAlert(title: "Oops", message: (response as! NSDictionary)["XSCMessage"] as! String)
                        }
                    } else {
                        print("Something went wrong! Please try again")
                    }
                    self.activityIndicator.stopAnimating()
                    self.preventUserInteractionView.isHidden = true
                }
            } catch {
                print("JSON serialization failed: ", error)
            }
        }
    }
    
    @IBAction func tourEnterPasswordNextButtonClick(_ sender: Any) {
        view.endEditing(true)
        if tourEnterPasswordTextField.isReallyEmpty {
            self.tourEnterPasswordView.shake()
            self.showAlert(title: "Missing Info", message: "Please enter CDT Pin")
        } else {
            activityIndicator.startAnimating()
            preventUserInteractionView.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"USER_ID\":\"\(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String)\",\"PIN\":\"\(tourEnterPasswordTextField.text!)\"}", mod: "Tournament", actionType: "password-verification") { (response) in
                if response as? String != "error" {
                    if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                        print(response)
                        UIView.transition(with: self.tourEnterPasswordView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                            self.tourEnterPasswordView.isHidden = true
                        })
                        if self.numberOfTeams == "4" {
                            UIView.transition(with: self.tourTeamEntryFeesView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                                self.fixture1View.isHidden = false
                            })
                        } else {
                            UIView.transition(with: self.tourTeamEntryFeesView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                                self.fixture2View.isHidden = false
                            })
                        }
                    } else {
                        self.showAlert(title: "Oops", message: (response as! NSDictionary)["XSCMessage"] as! String)
                    }
                } else {
                    print("Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
                self.preventUserInteractionView.isHidden = true
            }
        }
    }
    
    @IBAction func fixture1NextButtonClick(_ sender: Any) {
        UIView.transition(with: self.tourEnterPasswordView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.fixture1View.isHidden = true
        })
        UIView.transition(with: self.tourTeamEntryFeesView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tourTeamEntryFeesView.isHidden = false
        })
    }
    
    @IBAction func fixture2NextButtonClick(_ sender: Any) {
        UIView.transition(with: self.tourEnterPasswordView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.fixture2View.isHidden = true
        })
        UIView.transition(with: self.tourTeamEntryFeesView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tourTeamEntryFeesView.isHidden = false
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    //Keyboard Methods
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.keyboardHeightEnterPasswordConstraint?.constant = 0.0
                self.keyboardHeightCreatePasswordConstraint?.constant = 0.0
                self.keyboardHeightTeamEntryFeesConstraint?.constant = 0.0
                self.keyboardHeightAccountDetailsConstraint?.constant = 0.0
            } else {
                self.keyboardHeightEnterPasswordConstraint?.constant = -(endFrame?.size.height ?? 0.0) / 2
                self.keyboardHeightCreatePasswordConstraint?.constant = -(endFrame?.size.height ?? 0.0) / 2
                self.keyboardHeightTeamEntryFeesConstraint?.constant = -(endFrame?.size.height ?? 0.0) / 2
                self.keyboardHeightAccountDetailsConstraint?.constant = -(endFrame?.size.height ?? 0.0) / 2
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == countryPicker {
            print(CountryCodes.countryCodes.count)
            return CountryCodes.countryCodes.count
        } else {
            return UnSelectedAwards.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == countryPicker {
            let text = "\(CountryCodes.countryCodes[row].first!.value)"
            return text
            
        } else {
            return (UnSelectedAwards[row] as! NSDictionary)["AWARD_NAME"] as? String
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == countryPicker {
            let country = CountryCodes.countryCodes[row].first!.key
            
            countryCodeTextField.text = country
        } else {
            if UnSelectedAwards.count > 0 {
                tempCurrentAwardsID = (UnSelectedAwards[row] as! NSDictionary)["AWARD_ID"] as? String
                print(tempCurrentAwardsID)
                tourSelectAwardsTextField.text = "Select Awards"
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedAwards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let award = selectedAwards[indexPath.row] as! NSDictionary
        let selectedAwardsCell = tableView.dequeueReusableCell(withIdentifier: "awardsAndPrizeTableViewCell", for: indexPath) as! AwardsPrizesTableViewCell
        
        selectedAwardsCell.tourPrizeAmount.tag = Int(award["AWARD_ID"] as! String)!
        selectedAwardsCell.tourPrizeAmount.delegate = self
        selectedAwardsCell.tourAwardsNameLabel.text = award["AWARD_NAME"] as? String
        selectedAwardsCell.tourAwardsRemoveButton.addTarget(self, action: #selector(awardRemoveButtonTapped), for: UIControlEvents.touchUpInside)
        selectedAwardsCell.tourAwardsRemoveButton.tag = indexPath.row
        selectedAwardsCell.tourPrizeAmount.text = UserDefaults.standard.object(forKey: "AWARDSID_\(award["AWARD_ID"] as! String)") as? String ?? ""
        selectedAwardsCell.tourPrizeAmount.textColor = CommonUtil.themeRed
        selectedAwardsCell.tourPrizeButton.addTarget(self, action: #selector(tourPrizeButtonTapped), for: UIControlEvents.touchUpInside)
        selectedAwardsCell.tourPrizeButton.tag = indexPath.row
        if UserDefaults.standard.object(forKey: "AWARDSPRIZE_\(award["AWARD_ID"] as! String)") as? Int ?? 0 == 1 {
            selectedAwardsCell.tourPrizeButton.setImage(#imageLiteral(resourceName: "prize_image"), for: .normal)
        } else {
            selectedAwardsCell.tourPrizeButton.setImage(#imageLiteral(resourceName: "prize_image").withRenderingMode(.alwaysTemplate), for: .normal)
            selectedAwardsCell.tourPrizeButton.tintColor = UIColor.darkGray
        }
        
        return selectedAwardsCell
    }
    
    @objc func tourSelectRulesDismissPicker(){
        if UnSelectedAwards.count > 0 {
            
            
            print("Chrash Test", UnSelectedAwards)
            
            print("for loop started")
            var removable: NSDictionary?
            for award in self.UnSelectedAwards {
                if (award as! NSDictionary)["AWARD_ID"] as? String == self.tempCurrentAwardsID {
                    self.selectedAwards.add(award)
                    removable = award as? NSDictionary
                }
                print("for loops endeed")
            }
            
            if removable != nil {
                self.UnSelectedAwards.remove(removable!)
            }
            
            
            UIView.transition(with: self.tourSelectedAwardsTableView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tourSelectedAwardsTableView.reloadData()
                self.tourAwardsPickerView?.reloadAllComponents()
                print("picker reloaded")
            })
            
        }
        view.endEditing(true)
    }
    
    @IBAction func tourPrizeButtonTapped(_ sender: UIButton) {
        let award = selectedAwards[sender.tag] as! NSDictionary
        if UserDefaults.standard.object(forKey: "AWARDSPRIZE_\(award["AWARD_ID"] as! String)") as? Int ?? 0 == 1 {
            UserDefaults.standard.set(0, forKey: "AWARDSPRIZE_\(award["AWARD_ID"] as! String)")
            sender.setImage(#imageLiteral(resourceName: "prize_image").withRenderingMode(.alwaysTemplate), for: .normal)
            sender.tintColor = UIColor.darkGray
        } else {
            UserDefaults.standard.set(1, forKey: "AWARDSPRIZE_\(award["AWARD_ID"] as! String)")
            sender.setImage(#imageLiteral(resourceName: "prize_image"), for: .normal)
        }
    }
    
    @IBAction func awardRemoveButtonTapped(_ sender: UIButton) {
        let awardRemove = selectedAwards[sender.tag] as! NSDictionary
        
        for awards in selectedAwards {
            if awardRemove["AWARD_ID"] as? String == (awards as! NSDictionary)["AWARD_ID"] as? String {
                UnSelectedAwards.add(awards)
                selectedAwards.remove(awards)
                UserDefaults.standard.removeObject(forKey: "AWARDSID_\((awards as! NSDictionary)["AWARD_ID"] as! String)")
                UserDefaults.standard.removeObject(forKey: "AWARDSPRIZE_\((awards as! NSDictionary)["AWARD_ID"] as! String)")
                break
            }
        }
        self.tourSelectedAwardsTableView.reloadData()
        tourAwardsPickerView?.reloadAllComponents()
        UIView.transition(with: self.tourSelectedAwardsTableView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tourSelectedAwardsTableView.reloadData()
        })
        
        
    }
    
    @IBAction func tourAwardDropDownButtonClick(_ sender: Any) {
        tourSelectAwardsTextField.becomeFirstResponder()
    }
    
    @IBAction func tourAwardsPrizeNextButtonClick(_ sender: Any) {
        print("Select Awards Button clicked")
        view.endEditing(true)
        for award in selectedAwards {
            let keyValue = ["AWARD_ID":"\((award as! NSDictionary)["AWARD_ID"] as! String)","IS_TROPHY":"\(UserDefaults.standard.object(forKey: "AWARDSPRIZE_\((award as! NSDictionary)["AWARD_ID"] as! String)") as? Int ?? 0)","AMOUNT":"\((UserDefaults.standard.object(forKey: "AWARDSID_\((award as! NSDictionary)["AWARD_ID"] as! String)") as? String ?? "0"))","ADDL_INFO":""]
            awardsChoosed.add(keyValue)
        }
        print(awardsChoosed)
        if awardsChoosed.count > 0 {
            UIView.transition(with: self.tourAwardsPrizeView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tourAwardsPrizeView.isHidden = true
            })
            UIView.transition(with: self.tourAccountDetailsView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tourAccountDetailsView.isHidden = false
            })
        } else {
            self.tourAwardsPrizeView.shake()
            self.showAlert(title: "Missing info", message: "Please select atleast one award")
        }
        
    }
    
    //date format for from date
    @objc func handleTourTeamEntryLastDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        tourTeamEntryLastDateTextField.text = dateFormatter.string(from: (tourTeamEntryLastDatepicker?.date)!)
    }
    @objc func tourLastDateDismissPicker() {
        view.endEditing(true)
    }
    
    func checkPinCreatedOrNot() {
        if hasPin == true {
            UIView.transition(with: self.tourCreatePasswordView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tourCreatePasswordView.isHidden = true
            })
            UIView.transition(with: self.tourEnterPasswordView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tourEnterPasswordView.isHidden = false
            })
        } else {
            UIView.transition(with: self.tourEnterPasswordView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tourEnterPasswordView.isHidden = true
            })
            UIView.transition(with: self.tourCreatePasswordView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.tourCreatePasswordView.isHidden = false
            })
        }
    }
    
    func getAwardList(){
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "", mod: "Tournament", actionType: "get-award-list") { (response) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    print(response)
                    self.awardsReceived = (response as! NSDictionary)["XSCData"] as! NSArray
                    self.UnSelectedAwards = []
                    self.selectedAwards = []
                    
                    for rule in self.awardsReceived {
                        self.UnSelectedAwards.add(rule)
                    }
                    self.tourAwardsPickerView?.reloadAllComponents()
                } else {
                    print()
                    self.showAlert(title: "Oops", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                print("Something went wrong! Please try again")
            }
        }
    }
    
    @IBAction func tourCreatePassAgreeButtonClick(_ sender: Any) {
        print("Agree button clicked")
        if tourCreatePassAgreeButton.backgroundColor == .clear  {
            tourCreatePasswordNextButton.isEnabled = true
            tourCreatePasswordNextButton.backgroundColor = .black
            tourCreatePassAgreeButton.backgroundColor = CommonUtil.themeRed
            tourCreatePassAgreeButton.layer.cornerRadius = 0
            tourCreatePassAgreeButton.layer.borderWidth = 1
            tourCreatePassAgreeButton.layer.borderColor = UIColor.red.cgColor
            tourCreatePassAgreeButton.setImage(#imageLiteral(resourceName: "baseline_done_white_24pt_1x"), for: UIControlState.normal)
        } else if tourCreatePassAgreeButton.backgroundColor == CommonUtil.themeRed {
            tourCreatePasswordNextButton.isEnabled = false
            tourCreatePasswordNextButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            tourCreatePassAgreeButton.backgroundColor = .clear
            tourCreatePassAgreeButton.layer.cornerRadius = 0
            tourCreatePassAgreeButton.layer.borderWidth = 1
            tourCreatePassAgreeButton.layer.borderColor = UIColor.black.cgColor
        }
    }
    @IBAction func tourEnterPassAgreeButtonClick(_ sender: Any) {
        print("Agree button clicked")
        if tourEnterPassAgreeButton.backgroundColor == .clear  {
            tourEnterPasswordNextButton.isEnabled = true
            tourEnterPasswordNextButton.backgroundColor = .black
            tourEnterPassAgreeButton.backgroundColor = CommonUtil.themeRed
            tourEnterPassAgreeButton.layer.cornerRadius = 0
            tourEnterPassAgreeButton.layer.borderWidth = 1
            tourEnterPassAgreeButton.layer.borderColor = UIColor.red.cgColor
            tourEnterPassAgreeButton.setImage(#imageLiteral(resourceName: "baseline_done_white_24pt_1x"), for: UIControlState.normal)
        } else if tourEnterPassAgreeButton.backgroundColor == CommonUtil.themeRed {
            tourEnterPasswordNextButton.isEnabled = false
            tourEnterPasswordNextButton.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
            tourEnterPassAgreeButton.backgroundColor = .clear
            tourEnterPassAgreeButton.layer.cornerRadius = 0
            tourEnterPassAgreeButton.layer.borderWidth = 1
            tourEnterPassAgreeButton.layer.borderColor = UIColor.black.cgColor
        }
    }
    
    @objc func tourTermsAndConditionTapped(){
        print("Terms and condition Clicked")
        UIApplication.shared.open(NSURL(string:"http://www.cricdost.com/terms.html")! as URL)
    }
    
    @objc func tourCreatePinTermsAndConditionTapped() {
        print("Terms and condition Clicked")
        UIApplication.shared.open(NSURL(string:"http://www.cricdost.com/terms.html")! as URL)
    }
    
    @IBAction func tourAwardsPrizeBackButton(_ sender: Any) {
        //self.tourTeamEntryFeesTextField.text = ""
        self.selectedAwards.removeAllObjects()
        self.UnSelectedAwards.removeAllObjects()
        //        self.awardsChoosed.removeAllObjects()
        
        for award in self.awardsReceived {
            UserDefaults.standard.removeObject(forKey: "AWARDSID_\((award as! NSDictionary)["AWARD_ID"] as! String)")
            UserDefaults.standard.removeObject(forKey: "AWARDSPRIZE_\((award as! NSDictionary)["AWARD_ID"] as! String)")
            self.UnSelectedAwards.add(award)
        }
        
        //        self.tourSelectedAwardsTableView.reloadData()
        
        self.tourSelectedAwardsTableView.reloadData()
        self.tourAwardsPickerView?.reloadAllComponents()
        UIView.transition(with: self.tourTeamEntryFeesView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tourTeamEntryFeesView.isHidden = false
        })
        UIView.transition(with: self.tourAwardsPrizeView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tourAwardsPrizeView.isHidden = true
        })
    }
    
    @IBAction func tourAccountDetailsBackButton(_ sender: Any) {
        
        //self.tourTeamEntryFeesTextField.text = ""
        //        self.selectedAwards.removeAllObjects()
        //        self.UnSelectedAwards.removeAllObjects()
        //
        //        for rule in self.awardsReceived {
        //            UserDefaults.standard.removeObject(forKey: "AWARDSID_\((rule as! NSDictionary)["AWARD_ID"] as! String)")
        //            self.UnSelectedAwards.add(rule)
        //        }
        //
        //        self.tourSelectedAwardsTableView.reloadData()
        //        self.tourAwardsPickerView?.reloadAllComponents()
        //        self.selectedAwards.removeAllObjects()
        tourAccountNumberTextField.text = ""
        confirmAccountNumber.text = ""
        tourAccountHolderName.text = ""
        tourIFSCCodeTextField.text = ""
        tourMobileNumberTextField.text = ""
        UIView.transition(with: self.tourAwardsPrizeView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tourAwardsPrizeView.isHidden = false
        })
        UIView.transition(with: self.tourAccountDetailsView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.tourAccountDetailsView.isHidden = true
        })
    }
    
    @objc func tourEnterPinForgotPasswordTapped(){
        print("Forgot password click")
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"USER_ID\":\"\(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String)\"}", mod: "Tournament", actionType: "forget-password") { (response) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    print(response)
                    self.showAlert(title: "Message", message: "Tournament PIN has been sent to your register mobile number")
                } else {
                    print((response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                print("Something went wrong! Please try again")
            }
        }
    }
}
