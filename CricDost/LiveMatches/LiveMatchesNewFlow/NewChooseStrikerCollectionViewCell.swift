//
//  NewChooseStrikerCollectionViewCell.swift
//  CricDost
//
//  Created by JIT GOEL on 9/18/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class NewChooseStrikerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var strikerImageView: UIImageView!
    @IBOutlet weak var strikerNameLabel: UILabel!
    @IBOutlet weak var strikerTickButton: UIButton!
    override func layoutSubviews() {
        super.layoutSubviews()
        CommonUtil.imageRoundedCorners(imageviews: [strikerImageView])
        CommonUtil.buttonRoundedCorners(buttons: [strikerTickButton])
    }
}
