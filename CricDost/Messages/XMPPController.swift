//
//  XMPPController.swift
//  CricDost
//
//  Created by Jit Goel on 7/17/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import Foundation
import XMPPFramework

enum XMPPControllerError: Error {
    case wrongUserJID
}

class XMPPController: NSObject {
    var xmppStream: XMPPStream
//    var xmppRoster: XMPPRoster
//    let xmppRosterStorage: XMPPRosterStorage
    var xmppStreamManagement: XMPPStreamManagement
    var xmppMessageDeliveryRecipts: XMPPMessageDeliveryReceipts
//    var xmppRoom: XMPPRoom
    
    let hostName: String
    let userJID: XMPPJID
    let hostPort: UInt16
    let password: String
    
    var messageList: NSMutableArray?
    var userChatHistoryBackup: [String]?
    var groupChatReadIDList: [String] = []
    
    init(hostName: String, userJIDString: String, hostPort: UInt16 = 5222, password: String) throws {
        guard let userJID = XMPPJID(string: userJIDString) else {
            throw XMPPControllerError.wrongUserJID
        }
        
        self.hostName = hostName
        self.userJID = userJID
        self.hostPort = hostPort
        self.password = password
        
        // Stream Configuration
        self.xmppStream = XMPPStream()
//        xmppRosterStorage = XMPPRosterCoreDataStorage()
//        xmppRoster = XMPPRoster(rosterStorage: xmppRosterStorage)
        self.xmppStream.hostName = hostName
        self.xmppStream.hostPort = hostPort
        self.xmppStream.startTLSPolicy = XMPPStreamStartTLSPolicy.allowed
        self.xmppStream.myJID = userJID
//        self.xmppRoster.autoFetchRoster = true
//        self.xmppRoster.autoAcceptKnownPresenceSubscriptionRequests = true
//        self.xmppRoster.activate(xmppStream)
        let xmppSMMS = XMPPStreamManagementMemoryStorage()
        xmppStreamManagement = XMPPStreamManagement(storage: xmppSMMS, dispatchQueue: DispatchQueue.main)
        xmppStreamManagement.activate(xmppStream)
        xmppStreamManagement.autoResume = true
        xmppStreamManagement.ackResponseDelay = 0.2
        xmppStreamManagement.requestAck()
        xmppStreamManagement.automaticallyRequestAcks(afterStanzaCount: 1, orTimeout: 10)
        xmppStreamManagement.automaticallySendAcks(afterStanzaCount: 1, orTimeout: 10)
        xmppStreamManagement.enable(withResumption: true, maxTimeout: 0)
        xmppStreamManagement.sendAck()
        xmppMessageDeliveryRecipts = XMPPMessageDeliveryReceipts(dispatchQueue: DispatchQueue.main)
        xmppMessageDeliveryRecipts.autoSendMessageDeliveryReceipts = true
        xmppMessageDeliveryRecipts.autoSendMessageDeliveryRequests = true
        xmppMessageDeliveryRecipts.activate(xmppStream)
        xmppStream.register(xmppStreamManagement)
        
        super.init()
        self.xmppStreamManagement.addDelegate(self, delegateQueue: DispatchQueue.main)
//        self.xmppRoster.addDelegate(self, delegateQueue: DispatchQueue.main)
        self.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
    }
    
    func connect() {
        if !self.xmppStream.isDisconnected {
            return
        }
        try! self.xmppStream.connect(withTimeout: XMPPStreamTimeoutNone)
    }
}

extension XMPPController: XMPPRosterDelegate, XMPPRoomDelegate {
    
    func xmppRoomDidJoin(_ sender: XMPPRoom) {
        print("HIIIIIIIIIIIIIII")
    }
//    func xmppRosterDidEndPopulating(_ sender: XMPPRoster) {
//        let jidList = xmppRosterStorage.jids(for: xmppStream)
//        print("RosterList=\(jidList)")
//    }
}

extension XMPPController: XMPPStreamDelegate, XMPPStreamManagementDelegate {
    
    func xmppStreamDidConnect(_ stream: XMPPStream) {
        print("Stream: Connected")
        try! stream.authenticate(withPassword: self.password)
    }
    
    func xmppStreamDidAuthenticate(_ sender: XMPPStream) {
        self.xmppStream.send(XMPPPresence())
        print("Stream: Authenticated")
        CommonUtil.getJID() { (response) in
            let encodedTeamData = NSKeyedArchiver.archivedData(withRootObject: response)
            UserDefaults.standard.set(encodedTeamData, forKey: CommonUtil.USERLIST)
        }
        CommonUtil.getChatRooomsJID { (response) in
            let encodedTeamData = NSKeyedArchiver.archivedData(withRootObject: response)
            UserDefaults.standard.set(encodedTeamData, forKey: CommonUtil.CHATROOMLIST)
        }
    }
    
    func xmppStreamDidDisconnect(_ sender: XMPPStream, withError error: Error?) {
        print("Stream: Dis-Connected", error)
    }
    
    func xmppStreamManagement(_ sender: XMPPStreamManagement, didReceiveAckForStanzaIds stanzaIds: [Any]) {
        if let messageIds = stanzaIds as? [String] {
            for id in messageIds {
                print("Message is delivered to xmpp server: \(id)")
            }
        }
    }
    
    func xmppStream(_ sender: XMPPStream, didReceive message: XMPPMessage) {
        
        if message.type == "chat" || message.type == "groupchat" {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd, MMM hh:mm a"
            let dateString = formatter.string(from: Date())
            let JIDPrefix = (message.fromStr?.components(separatedBy: "@")[0])
            
            let userName = UserDefaults.standard.object(forKey: CommonUtil.OPENFIRE_USERNAME) as? String
            userChatHistoryBackup = UserDefaults.standard.object(forKey: userName!) as? [String] ?? []
            if let msg = message.body {
                
                print(message.subject)
                
                let imageUrl = message.subject ?? ""
                
                var recievedMessage: NSDictionary?
                
                var JIDPrefixSender = ""
                
                if message.type == "groupchat" {
                    let userlist = UserDefaults.standard.object(forKey: CommonUtil.USERLIST) as! Data
                    let decodedData = NSKeyedUnarchiver.unarchiveObject(with: userlist) as! NSDictionary
                    
                    let users = decodedData["users"] as? NSArray ?? []
                    
                    JIDPrefixSender = (message.fromStr?.components(separatedBy: "/")[1])!
                    let nickName = JIDPrefixSender.components(separatedBy: "-")[1]
                    JIDPrefixSender = JIDPrefixSender.components(separatedBy: "-")[0]

                    if imageUrl != "" {
                        UserDefaults.standard.set(imageUrl, forKey: "\(JIDPrefix!)_IMAGEURL")
                    }
                    
                    let formatter1 = DateFormatter()
                    formatter1.dateFormat = "dd, MMM hh:mm a"
                    let dateString1 = formatter1.string(from: message.delayedDeliveryDate ?? Date())
                    
                    for user in users {
                        if JIDPrefixSender == (user as! NSDictionary)["username"] as! String {
                            recievedMessage = ["Name":JIDPrefixSender, "message": msg, "time": dateString1,"type":"RECEIVED", "nickName":nickName,"image":imageUrl]
                        }
                    }
                } else if message.type == "chat" {
                    
                    if imageUrl != "" {
                        UserDefaults.standard.set(imageUrl, forKey: "\(JIDPrefix!)_IMAGEURL")
                    }
                    JIDPrefixSender = (message.fromStr?.components(separatedBy: "/")[1])!
                    recievedMessage = ["Name":JIDPrefix!, "message": msg, "time": dateString,"type":"RECEIVED", "nickName":JIDPrefixSender,"image":imageUrl]
                    
                    print("recievedMessageXXX",recievedMessage)
                }
                if let tempNames: NSArray = UserDefaults.standard.object(forKey: JIDPrefix!) as? NSArray {
                    messageList = tempNames.mutableCopy() as? NSMutableArray
                } else {
                    messageList = []
                }
                if messageList?.count == 0 {
                    if message.type == "groupchat" {
                        if !(userChatHistoryBackup?.contains(JIDPrefix!))! {
                            userChatHistoryBackup?.append(JIDPrefix!)
                            UserDefaults.standard.set(userChatHistoryBackup, forKey: userName!)
                        }
                    } else {
                        userChatHistoryBackup?.append(JIDPrefix!)
                        UserDefaults.standard.set(userChatHistoryBackup, forKey: userName!)
                    }
                }
                messageList?.insert(recievedMessage!, at: 0)
                
                UserDefaults.standard.set(messageList, forKey: JIDPrefix!)
                var unread = UserDefaults.standard.object(forKey: "\(JIDPrefix!)_\(userName!)") as? Int ?? 0
                    unread = unread + 1
                UserDefaults.standard.set(unread, forKey: "\(JIDPrefix!)_\(userName!)")
                let matchInfo:[String: NSDictionary] = ["data": recievedMessage!]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshChatHome"), object: nil, userInfo: matchInfo)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshChatList"), object: nil, userInfo: matchInfo)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshGroupChatList"), object: nil, userInfo: matchInfo)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshDrawerMsgCount"), object: nil)
            } else {
                let JIDPrefix = (message.fromStr?.components(separatedBy: "@")[0])
                if JIDPrefix == UserDefaults.standard.object(forKey: CommonUtil.CURRENTCHAT) as? String ?? "" {
                    print("Showing typing for \(JIDPrefix!)")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showTyping"), object: nil)
                }
            }
        }
    }
    
    func xmppStream(_ sender: XMPPStream, didReceive presence: XMPPPresence) {
        print("Receiving Presence \(presence.type)", presence)
        if let online = presence.type {
            if online != "error" {
                var JIDPrefix = (presence.fromStr?.components(separatedBy: "@")[0])
                if (JIDPrefix?.contains(find: "t"))! || (JIDPrefix?.contains(find: "m"))! {
                    let x = presence.fromStr?.components(separatedBy: "/")
                    if (x?.count)! > 1 {
                        JIDPrefix = x?[1]
                        UserDefaults.standard.set(online, forKey: "\(JIDPrefix!)_\(CommonUtil.PRESENCE)")
                    }
                } else {
                    UserDefaults.standard.set(online, forKey: "\(JIDPrefix!)_\(CommonUtil.PRESENCE)")
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showOnlinePresence"), object: nil)
            }
        }
    }
    
    func xmppStream(_ sender: XMPPStream, didReceive iq: XMPPIQ) -> Bool {
        if iq.isResultIQ {
           if iq.lastActivitySeconds() == 0 {
                print("user is online")
            } else {
                print("user is offline")
            }
        }
        return true
    }
}

