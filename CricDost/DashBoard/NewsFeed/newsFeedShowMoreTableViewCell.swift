//
//  newsFeedShowMoreTableViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 10/17/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class newsFeedShowMoreTableViewCell: UITableViewCell {

    @IBOutlet weak var showMoreButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
