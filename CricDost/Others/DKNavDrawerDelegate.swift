//
//  DKNavDrawerDelegate.swift
//  DKNavigationDrawer
//
//  Created by Darshan on 5/17/17.
//  Copyright © 2017 Darshan. All rights reserved.
//

import UIKit


protocol DKNavDrawerDelegate: NSObjectProtocol {
    //add methods as per requirements
    func dkNavDrawerSelection(_ selectionIndex: Int)
}


class DKNavDrawer: UINavigationController, UIGestureRecognizerDelegate, UITableViewDataSource, UITableViewDelegate {
//    var pan_gr: UIPanGestureRecognizer?
    var pan_gr: UIScreenEdgePanGestureRecognizer?
    weak var dkNavDrawerDelegate: DKNavDrawerDelegate?
    
    var SHAWDOW_ALPHA:Float = 0.5
    var MENU_DURATION:Float = 0.3
    let MENU_TRIGGER_VELOCITY = 350
    var isOpen: Bool = false
    var statusBarHidden = false
    var meunHeight: Float = 0.0
    var menuWidth: Float = 0.0
    var outFrame = CGRect.zero
    var inFrame = CGRect.zero
    var shawdowView: UIView?
    var drawerView: DrawerView?
    @IBOutlet weak var navBar: UINavigationBar!
    
    private var menuItems = [Any]()
    private var menuImages = [Any]()
    
    // MARK: - VC lifecycle
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        // Custom initialization
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        menuItems = ["My Profile", "Messages", "Buy/Sell", "Live Scores", "Settings", "Invite Friends", "Rate this app","About us"]
//        menuImages = ["My-Profile", "message", "buy_sell","live-score", "Settings", "invite-team","rate-the-app", "about-us"]
//        menuItems = ["My Profile","Messages","Live Scores","Settings","Rate this app","About us","CricSpace"]
//        menuImages = ["My-Profile","message","live-score","Settings","rate-the-app", "about-us","buy_sell"]
        menuItems = ["My Profile","Messages","Live Scores","Settings","Rate this app","About us","Invite Friends"]
        menuImages = ["My-Profile","message","live-score","Settings","rate-the-app", "about-us","invite-team"]
        setUpDrawer()
    }
    
    override var prefersStatusBarHidden: Bool {
        return statusBarHidden
    }
    
    func setUpDrawer() {
        isOpen = false
        // load drawer view
        drawerView = Bundle.main.loadNibNamed("DrawerView", owner: self, options: nil)?[0] as? DrawerView
        let wind = UIWindow(frame: UIScreen.main.bounds)
        var width: Float = (Float(wind.frame.size.width))
        width = width * 0.8
        // Adjust width here
        meunHeight = Float(wind.frame.size.height)
        menuWidth = width
        outFrame = CGRect(x: CGFloat(-menuWidth), y: CGFloat(0), width: CGFloat(menuWidth), height: CGFloat(meunHeight))
        inFrame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(menuWidth), height: CGFloat(meunHeight))
        print("inframe=\(meunHeight)")
        // drawer shawdow and assign its gesture
        shawdowView = UIView(frame: view.frame)
        shawdowView?.backgroundColor = UIColor(red: CGFloat(0.0), green: CGFloat(0.0), blue: CGFloat(0.0), alpha: CGFloat(0.0))
        shawdowView?.isHidden = true
        let tapIt = UITapGestureRecognizer(target: self, action: #selector(self.taponShawdow))
        shawdowView?.addGestureRecognizer(tapIt)
        shawdowView?.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(shawdowView!)
        // add drawer view
        drawerView?.frame = outFrame
        view.addSubview(drawerView!)
        // drawer list
        drawerView?.drawerTableView.contentInset = UIEdgeInsetsMake(0, 0, 30, 0)
        // statuesBarHeight+navBarHeight
        drawerView?.drawerTableView.dataSource = self
        drawerView?.drawerTableView.delegate = self
        //seprator cleanup
        drawerView?.drawerTableView.tableFooterView = UIView(frame: CGRect.zero)
        // gesture on self.view
//        pan_gr = UIPanGestureRecognizer(target: self, action: #selector(self.moveDrawer))
//        pan_gr?.maximumNumberOfTouches = 1
//        pan_gr?.minimumNumberOfTouches = 1
//        view.addGestureRecognizer(pan_gr!)
        pan_gr = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.moveDrawer))
        pan_gr?.maximumNumberOfTouches = 1
        pan_gr?.minimumNumberOfTouches = 1
        pan_gr?.edges = .left
        view.addGestureRecognizer(pan_gr!)
//        self.pan_gr.delegate = self;


        //    for (id x in self.view.subviews){
        //        NSLog(@"%@",NSStringFromClass([x class]));
        
        drawerView?.profileNameLabel.text = UserDefaults.standard.object(forKey: CommonUtil.FULL_NAME) as? String
        drawerView?.profileImageVIew.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(UserDefaults.standard.object(forKey: CommonUtil.IMAGE_URL) as! String)"), placeholderImage: #imageLiteral(resourceName: "profile_pic_default"))
        drawerView?.profileImageVIew.layer.cornerRadius = (drawerView?.profileImageVIew.frame.height)!/2
        drawerView?.profileImageVIew.clipsToBounds = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageTapFunction))
        
        // add it to the image view;
        drawerView?.profileImageVIew.addGestureRecognizer(tapGesture)
        // make sure imageView can be interacted with by user
        drawerView?.profileImageVIew.isUserInteractionEnabled = true
    }
    
    func getUnreadMsgCount() -> Int {
        let chatList = UserDefaults.standard.object(forKey: UserDefaults.standard.object(forKey: CommonUtil.OPENFIRE_USERNAME) as! String) as? [String] ?? []
        let userName = UserDefaults.standard.object(forKey: CommonUtil.OPENFIRE_USERNAME) as? String
        var total_unreadmsg = 0
        for chat in chatList {
            total_unreadmsg = total_unreadmsg + (UserDefaults.standard.object(forKey: "\(chat)_\(userName!)") as? Int ?? 0)
        }
        print("Total unread message", total_unreadmsg)
        return total_unreadmsg
    }
    
    @objc func imageTapFunction(sender:UITapGestureRecognizer) {
        print("Show my profile")
        drawerToggle()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissMatchDetailView"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissMyProfileFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissTeamProfileFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissPlayerProfileFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissFixedMatchesFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissOpenMatchesFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showMyProfile"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - push & pop
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
        // disable gesture in next vc
        pan_gr?.isEnabled = false
    }
    
    override func popViewController(animated: Bool) -> UIViewController {
        let vc: UIViewController? = super.popViewController(animated: animated)
        // enable gesture in root vc
        if viewControllers.count == 1 {
            pan_gr?.isEnabled = true
        }
        return vc!
    }
    
    // MARK: - drawer
    func drawerToggle() {
        if !isOpen {
            openNavigationDrawer()
        }
        else {
            closeNavigationDrawer()
        }
        drawerView?.profileImageVIew.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(UserDefaults.standard.object(forKey: CommonUtil.IMAGE_URL) as! String)"), placeholderImage: #imageLiteral(resourceName: "profile_pic_default"))
        drawerView?.profileNameLabel.text = UserDefaults.standard.object(forKey: CommonUtil.FULL_NAME) as? String
        view.bringSubview(toFront: drawerView!)
        refreshDrawerMessageCount()
    }
    
    func refreshDrawerMessageCount() {
        drawerView?.drawerTableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .fade)
    }
    
    //open and close action
    func openNavigationDrawer() {
        //    NSLog(@"open x=%f",self.menuView.center.x);
        
        let duration: Float = MENU_DURATION / menuWidth * fabs(Float(drawerView!.center.x)) + MENU_DURATION / 2
        // y=mx+c
        // shawdow
        shawdowView?.isHidden = false
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: .curveEaseInOut, animations: {() -> Void in
            self.shawdowView?.backgroundColor = UIColor(red: CGFloat(0.0), green: CGFloat(0.0), blue: CGFloat(0.0), alpha: CGFloat(self.SHAWDOW_ALPHA))
        }, completion: { _ in })
        // drawer
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: .beginFromCurrentState, animations: {() -> Void in
            self.drawerView?.frame = self.inFrame
            UIApplication.shared.isStatusBarHidden = true
            self.statusBarHidden = true
            self.setNeedsStatusBarAppearanceUpdate()
        }, completion: { _ in
            print("opening")
            })
        isOpen = true

    }
    
    func closeNavigationDrawer() {
        //    NSLog(@"close x=%f",self.menuView.center.x);
        let duration: Float = MENU_DURATION / menuWidth * fabs(Float(drawerView!.center.x)) + MENU_DURATION / 2
        // y=mx+c
        // shawdow
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: .curveEaseInOut, animations: {() -> Void in
            self.shawdowView?.backgroundColor = UIColor(red: CGFloat(0.0), green: CGFloat(0.0), blue: CGFloat(0.0), alpha: CGFloat(0.0))
        }, completion: {(_ finished: Bool) -> Void in
            self.shawdowView?.isHidden = true
        })
        // drawer
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: .beginFromCurrentState, animations: {() -> Void in
            self.drawerView?.frame = self.outFrame
            UIApplication.shared.isStatusBarHidden = false
            self.statusBarHidden = false
            self.setNeedsStatusBarAppearanceUpdate()
        }, completion: { _ in
            print("closing")
            if UserDefaults.standard.object(forKey: CommonUtil.OPENINVITEPAGE) as? Bool ?? false {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "gotoInvitePage"), object: nil)
                UserDefaults.standard.set(false, forKey: CommonUtil.OPENINVITEPAGE)
            }
        })
        isOpen = false
    }
    
    
    @objc func taponShawdow(recognizer: UITapGestureRecognizer) {
        closeNavigationDrawer()
    }
    
    @objc func moveDrawer(_ recognizer: UIPanGestureRecognizer) {
        let translation: CGPoint = recognizer.translation(in: view)
        let velocity: CGPoint? = (recognizer as UIPanGestureRecognizer).velocity(in: view)
        //    NSLog(@"velocity x=%f",velocity.x);
        if (recognizer as UIPanGestureRecognizer).state == .began {
            //        NSLog(@"start");
            
            let x:Int = Int((velocity?.x)!)
            
            
            if  x > MENU_TRIGGER_VELOCITY && !isOpen {
                openNavigationDrawer()
            }
            else if x < -MENU_TRIGGER_VELOCITY && isOpen {
                closeNavigationDrawer()
            }
        }
        if (recognizer as UIPanGestureRecognizer).state == .changed {
            //        NSLog(@"changing");
            let movingx: Float = Float(drawerView!.center.x + translation.x)
            print("menuWidth== \(menuWidth / 2)")
            if movingx > -menuWidth / 2 && movingx < menuWidth / 2 {
                drawerView?.center = CGPoint(x: CGFloat(movingx), y: CGFloat((drawerView?.center.y)!))
                recognizer.setTranslation(CGPoint(x: CGFloat(0), y: CGFloat(0)), in: view)
                let changingAlpha: Float = SHAWDOW_ALPHA / menuWidth * movingx + SHAWDOW_ALPHA / 2
                // y=mx+c
                shawdowView?.isHidden = false
                shawdowView?.backgroundColor = UIColor(red: CGFloat(0.0), green: CGFloat(0.0), blue: CGFloat(0.0), alpha: CGFloat(changingAlpha))
            }
        }
        if (recognizer as UIPanGestureRecognizer).state == .ended {
            //        NSLog(@"end");
            if Int((drawerView?.center.x)!) > 0 {
                openNavigationDrawer()
            }
            else if Int((drawerView?.center.x)!) < 0 {
                closeNavigationDrawer()
            }
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CellIdentifier: String = "Cell"
        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier)
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: CellIdentifier)
        }
        // Configure the cell...
        cell?.textLabel?.font = UIFont(name: "HelveticaNeue", size: 13)
        if (menuItems[indexPath.row] as! String) == "Messages" {
            let unreadmsg = getUnreadMsgCount()
            if unreadmsg == 0 {
                cell?.textLabel?.text = "\(menuItems[indexPath.row])"
                cell?.textLabel?.textColor = UIColor.black
            } else {
                cell?.textLabel?.text = "\(menuItems[indexPath.row]) (\(unreadmsg))"
//                cell?.textLabel?.textColor = CommonUtil.themeRed
                cell?.textLabel?.textColor = UIColor.black
                let text = "\(menuItems[indexPath.row])  (\(unreadmsg) Unread)"
                let range = (text as NSString).range(of: "(\(unreadmsg) Unread)")
                let attributedString = NSMutableAttributedString(string:text)
                attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: CommonUtil.themeRed , range: range)
                attributedString.addAttributes([NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 13)], range: range)
                
                //Apply to the label
                cell?.textLabel?.attributedText = attributedString;
            }
        } else {
            cell?.textLabel?.text = "\(menuItems[indexPath.row])"
            cell?.textLabel?.textColor = UIColor.black
        }
        cell?.imageView?.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        // cell.imageView.backgroundColor=[UIColor redColor];
        cell?.imageView?.image = UIImage(named: "\(menuImages[indexPath.row])")
        cell?.imageView?.image = cell?.imageView?.image?.withRenderingMode(.alwaysTemplate)
        cell?.imageView?.tintColor = UIColor.red
        cell?.imageView?.contentMode = .scaleAspectFit
//        cell?.textLabel?.text = "\(menuItems[indexPath.row])"
        // if (indexPath.row==1 || indexPath.row==0 ||indexPath.row==2 || indexPath.row==3 || indexPath.row==5 ||indexPath.row==9) {
        //cell.layoutMargins = UIEdgeInsetsZero;
        cell?.separatorInset = UIEdgeInsetsMake(1.0, 1.0, 0.0, (cell?.bounds.size.width)!)
        //}
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    // MARK: - Table view delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dkNavDrawerDelegate?.dkNavDrawerSelection(indexPath.row)
        closeNavigationDrawer()
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
