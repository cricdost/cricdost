//
//  ChooseBowlerViewController.swift
//  CricDost
//
//  Created by Jit Goel on 6/28/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ChooseBowlerViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ShowsAlert {
    
    @IBOutlet weak var chooseBowlerChoolectionView: UICollectionView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var chooseBowlerView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var bowlerList: NSArray = []
    var selectedPlayer: String = ""
    @IBOutlet weak var outerView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonUtil.buttonRoundedCorners(buttons: [nextButton])
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [chooseBowlerView])
        print(bowlerList)
        
        let outerViewTap = UITapGestureRecognizer(target: self, action: #selector(outerViewFunction))
        outerView.addGestureRecognizer(outerViewTap)
        
        setCollectionViewLayout(card: chooseBowlerView, collectionView: chooseBowlerChoolectionView, center: false)
    }
    
    @objc func outerViewFunction(sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return bowlerList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let player = bowlerList[indexPath.section] as! NSDictionary
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "chooseStriker", for: indexPath) as! ChooseStrikerCollectionViewCell
        cell.playerImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(player["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
        cell.playerName.text = player["FULL_NAME"] as? String
        cell.score.text = "\(player["RUNS"] as! String)(\(player["BALLS"] as! String))"
        cell.status.isHidden = true
        
        if selectedPlayer == player["MATCH_TEAM_PLAYER_ID"] as! String {
            cell.greenTick.isHidden = false
        } else {
            cell.greenTick.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let player = bowlerList[indexPath.section] as! NSDictionary
        selectedPlayer = player["MATCH_TEAM_PLAYER_ID"] as! String
        collectionView.reloadData()
    }
    
    @IBAction func nextButtonClick(_ sender: Any) {
        if selectedPlayer != "" {
            sendSelectedPlayer()
        } else {
            self.showAlert(title: "Bowler", message: "Please select a bowler")
        }
        
    }
    
    func sendSelectedPlayer() {
        activityIndicator.startAnimating()
        nextButton.isEnabled = false
        outerView.isUserInteractionEnabled = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"BOWLER\":\"\(selectedPlayer)\"}", mod: "MatchScore", actionType: "create-bowler") { (response) in
            if response as? String != "error" {
                print(response)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshScorerPage"), object: nil)
                self.dismiss(animated: true, completion: nil)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.nextButton.isEnabled = true
            self.outerView.isUserInteractionEnabled = true
        }
    }
    
    func setCollectionViewLayout(card: UIView, collectionView: UICollectionView, center: Bool) {
        
        if center {
            let screensize = card.bounds.size
            let cellwidth = floor(screensize.width * 0.8)
            let cellheight = floor(collectionView.bounds.height * 1.0)
            
            let insetX = (card.bounds.width - cellwidth) / 2.0
            let insetY = (collectionView.bounds.height - cellheight) / 2.0
            
            let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.itemSize = CGSize(width: cellwidth, height: cellheight)
            collectionView.contentInset = UIEdgeInsetsMake(insetY, insetX, insetY, insetX)
        } else {
            let screensize1 = card.bounds.size
            let cellwidth1 = floor(screensize1.width/3)
            let cellheight1 = floor(collectionView.bounds.height)
            
            let layout1 = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout1.itemSize = CGSize(width: cellwidth1, height: cellheight1)
            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }
    }

}
