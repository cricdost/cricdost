//
//  EditMatchTeamPlayerViewController.swift
//  CricDost
//
//  Created by Jit Goel on 11/15/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import LTMorphingLabel
import IQKeyboardManager

class EditMatchTeamPlayerViewController: UIViewController, LTMorphingLabelDelegate, ShowsAlert, UITextFieldDelegate {

    //Number of Players View
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var numberOfPlayersView: UIView!
    @IBOutlet weak var numberOfPlayersTextField: UITextField!
    @IBOutlet weak var numberOfPlayerUpdateButton: UIButton!
    @IBOutlet weak var numberOfPlayerLabel: LTMorphingLabel!
    var currentMatchID: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [numberOfPlayersView])
        CommonUtil.buttonRoundedCorners(buttons: [numberOfPlayerUpdateButton])
        
        let dismissViewGesture = UITapGestureRecognizer(target: self, action: #selector(dismissViewTapped))
        dismissView.addGestureRecognizer(dismissViewGesture)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        numberOfPlayerLabel.text = "Number of players"
    }
    
    @objc func dismissViewTapped(sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == numberOfPlayersTextField {
            let maxLength = 2
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else {
            return true
        }
    }
    
    
    @IBAction func updateButtonClick(_ sender: Any) {
        if numberOfPlayersTextField.text == "" {
            self.numberOfPlayersView.shake()
            self.showAlert(title: "Invalid info", message: "Please enter number of player")
        } else {
            if Int(numberOfPlayersTextField.text!)! >= 3 && Int(numberOfPlayersTextField.text!)! <= 16 {
                updateTeamPlayerCount()
            } else {
                self.numberOfPlayersView.shake()
                self.showAlert(title: "Number of players", message: "Maximum number of player is 16 and minimum is 3 players for a match")
            }
        }
    }
  
    func updateTeamPlayerCount() {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\":\"\(currentMatchID!)\",\"TOTAL_PLAYERS\":\"\(numberOfPlayersTextField.text!)\"}", mod: "Match", actionType: "update-match-total-players") { (response) in
            print(response)
            if response as? String != "error" {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadOpenMacthDetailListFunction"), object: nil)
                let alertView = UIAlertController(title: "Match", message: (response as! NSDictionary)["XSCMessage"] as? String , preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    self.dismiss(animated: true, completion: nil)
                })
                alertView.addAction(action)
                self.present(alertView, animated: true, completion: nil)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
        }
    }
    
}
