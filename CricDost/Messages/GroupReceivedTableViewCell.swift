//
//  GroupReceivedTableViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 7/30/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class GroupReceivedTableViewCell: UITableViewCell {

    @IBOutlet weak var receivedNameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var viewHolder: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [viewHolder])
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
