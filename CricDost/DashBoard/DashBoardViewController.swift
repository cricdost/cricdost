//
//  DashBoardViewController.swift
//  CricDost
//
//  Created by Jit Goel on 5/26/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Floaty
import XMPPFramework
import FirebaseDatabase
import PlugNPlay
import UserNotifications

class DashBoardViewController: UIViewController, DKNavDrawerDelegate, CLLocationManagerDelegate, MKMapViewDelegate, ShowsAlert, FloatyDelegate, UICollectionViewDelegate, UICollectionViewDataSource,UNUserNotificationCenterDelegate, UIGestureRecognizerDelegate {
    
    var floaty = Floaty()
    weak var timer: Timer?
    var alertNonScorer: UIAlertController?
    var rootNav: DKNavDrawer?
    var joinTeamVC: JoinTeamViewController?
    var joinMatchVC: JoinMatchViewController?
    var createTeamVC: CreateTeamViewController?
    var createMatchVC: CreateMatchViewController?
    var showTossVC: LiveMatchViewController?
    var timeLeftForMatch = 0
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var filterPlayerImageView: UIImageView!
    @IBOutlet weak var filterTeamImageView: UIImageView!
    @IBOutlet weak var filterOpenMatchImageVIew: UIImageView!
    @IBOutlet weak var filterPlayerTickButton: UIButton!
    @IBOutlet weak var filterMatchImageView: UIImageView!
    @IBOutlet weak var filterTeamTickButton: UIButton!
    @IBOutlet weak var liveScoreCollectionView: UICollectionView!
    @IBOutlet weak var filterOpenMatchTickButton: UIButton!
    @IBOutlet weak var filterMatchTickButton: UIButton!
    @IBOutlet weak var fromDateTextField: UITextField!
    @IBOutlet weak var toDateTextField: UITextField!
    @IBOutlet weak var fromDateTextFieldWidth: NSLayoutConstraint!
    var isPlayerFilterSelected = true
    var isTeamFilterSelected = true
    var isOpenMatchFilterSelected = true
    var isMatchFilterSelected = true
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    var manager: CLLocationManager?
    var notilabel: UILabel = UILabel(frame: CGRect(x: 10, y: -5, width: 16, height: 16))
    @IBOutlet weak var preventUserInteractionLoader: UIView!
    
    @IBOutlet weak var locationLabel: UILabel!
    var playerAnnotationList: [CustomAnnotation] = []
    var teamAnnotationList: [CustomAnnotation] = []
    var openMatchAnnotationList: [CustomAnnotation] = []
    var fixedMatchAnnotationList: [CustomAnnotation] = []
    @IBOutlet weak var myLocationButton: UIButton!
    @IBOutlet weak var mainActivityIndicator: UIActivityIndicatorView!
    
    var xmppController: XMPPController?
    var stream: XMPPStream?
    var xmppRoom: XMPPRoom?
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var liveScoreNavTitleView: UIView!
    @IBOutlet weak var profileloadingView: UIView!
    @IBOutlet weak var profileloadingactivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var filterImageView: UIImageView!
    @IBOutlet weak var tournamentBtn: UIButton!
    @IBOutlet weak var tournamentTitle: UIButton!
    var filterIsOpen = false
    var currentDistance = 5.0
    var logo = 0
    
    var from_date_picker:UIDatePicker?
    var to_date_picker:UIDatePicker?
    
    var latitude = 0.00
    var longitude = 0.00
    var currentAdressID: String?
    var currentAddressOfAnnotaion: String?
    
    var gotlocation = true
    var shake = 0
    var rowIndex = 0
    
    var notificationCount : Int = 0
    var rightButton  : UIButton?
    
    var password: String?
    var userName: String?
    
    
    var liveScoreTimer = Timer()
    var imageView: UIImageView?
    var ref: DatabaseReference!
    var databaseHandleAdded: DatabaseHandle!
    var databaseHandleRemoved: DatabaseHandle!
    var databaseHandleChanged: DatabaseHandle!
    var matchesInfo: [Any] = []
    var removeElements: [Int] = []
    var liveMatches: [Any] = []
    
    var scrollingTimer = Timer()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rootNav = (navigationController as? DKNavDrawer)
        rootNav?.dkNavDrawerDelegate = self
        
        //join team popup after login new user
        joinTeamVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JoinTeamViewController") as? JoinTeamViewController
        joinTeamVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        joinTeamVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        initializeNoficationCentreMethods()
        initializeTapGestures()
        
        //Map & Location Configuration
        map.delegate = self
        
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [profileloadingView, searchView, filterView])
        CommonUtil.buttonRoundedCorners(buttons: [myLocationButton,filterPlayerTickButton,filterTeamTickButton,filterOpenMatchTickButton,filterMatchTickButton,tournamentBtn])
        CommonUtil.roundedTextFieldCorners(textFields: [fromDateTextField,toDateTextField])
        CommonUtil.newButtonRoundedCorners(buttons: [tournamentTitle])
        tournamentTitle.blink(enabled: true, duration: 0.5, stopAfter: 0.0)
        
        myLocationButton.layer.shadowRadius = 3
        myLocationButton.layer.shadowOpacity = 0.5
        myLocationButton.layer.shadowOffset = CGSize(width: 0, height: 0)
        myLocationButton.clipsToBounds = false
        tournamentBtn.layer.shadowRadius = 3
        tournamentBtn.layer.shadowOpacity = 0.5
        tournamentBtn.clipsToBounds = false
        tournamentBtn.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        OperationQueue.main.addOperation{
            self.manager = CLLocationManager()
            self.manager?.delegate = self
            self.manager?.desiredAccuracy = kCLLocationAccuracyBest
            self.manager?.requestWhenInUseAuthorization()
            self.manager?.startUpdatingLocation()
        }
        
        
        let now = Date()
        from_date_picker = UIDatePicker()
        from_date_picker?.minimumDate = now
        fromDateTextField.inputView = from_date_picker
        fromDateTextField.tintColor = .clear
        
        from_date_picker?.addTarget(self, action: #selector(handleFromDatePicker), for: UIControlEvents.valueChanged)
        from_date_picker?.datePickerMode = .date
        
        to_date_picker = UIDatePicker()
        toDateTextField.inputView = to_date_picker
        toDateTextField.tintColor = .clear
        
        to_date_picker?.addTarget(self, action: #selector(handleToDatePicker), for: UIControlEvents.valueChanged)
        to_date_picker?.datePickerMode = .date
        
        let toolBar1 = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker1))
        fromDateTextField.inputAccessoryView = toolBar1
        
        let toolBar2 = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker2))
        toDateTextField.inputAccessoryView = toolBar2
        
        UserDefaults.standard.set(false, forKey: CommonUtil.USERSELECTEDLOCATION)
        setFilterTick()
        
        self.navigationController?.navigationBar.layer.zPosition = -1;
        
        layoutFAB()
        
        SocketConnectionsclass.connectSocket(userId: UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String, deviceKey: UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)
        
        self.becomeFirstResponder()
        
        userName = UserDefaults.standard.object(forKey: CommonUtil.OPENFIRE_USERNAME) as? String
        password = UserDefaults.standard.object(forKey: CommonUtil.OPENFIRE_PASSWORD) as? String
        
        //        try! self.xmppController = XMPPController(hostName: "192.168.13.22:15222",
        //                                                  userJIDString: "\(userName!)@svr-1004/\(UserDefaults.standard.object(forKey: CommonUtil.FULL_NAME) as! String)",
        //            password: password!)
        try! self.xmppController = XMPPController(hostName: CommonUtil.cricdost_chat_domain, userJIDString: "\(userName!)\(CommonUtil.cricdost_chat_domain_name)/\(UserDefaults.standard.object(forKey: CommonUtil.FULL_NAME) as! String)", hostPort: UInt16(CommonUtil.CHAT_CLIENT_PORT)!, password: password!)
        self.xmppController?.connect()
        
        stream = self.xmppController?.xmppStream
        
        // create notification button in bar button item
        rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        rightButton?.setBackgroundImage(#imageLiteral(resourceName: "baseline_notifications_white_24pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        rightButton?.tintColor = UIColor.white
        
        rightButton?.addTarget(self, action: #selector(navigateToNotification), for: .touchUpInside)
        
        let rightBarButtomItem = UIBarButtonItem(customView: rightButton!)
        
        navigationItem.rightBarButtonItem = rightBarButtomItem
//        self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80.0)
        liveScoreInitialize()
        
        UserDefaults.standard.set(false, forKey: CommonUtil.SearchOpenMatchesDetails)
    }
    
    @IBAction func tournamentButton(_ sender: Any) {
        print("tournament clicked")
        let nearByTourVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NearByTournamentViewController") as! NearByTournamentViewController
        nearByTourVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nearByTourVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(nearByTourVC, animated: true, completion: nil)
    }
    
    func liveScoreInitialize() {
        liveScoreTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.livescoreOnNav(theTimer:)),userInfo: 0,repeats: true)
        
        ref = Database.database().reference()
        
        //        CommonUtil.roundedUIViewCornersWithShade(uiviews: [liveScoreNavTitleView])
        liveScoreNavTitleView.layer.cornerRadius = 5
        liveScoreNavTitleView.layer.masksToBounds = true
        liveScoreNavTitleView.clipsToBounds = false
        liveScoreCollectionView.layer.cornerRadius = 5
        liveScoreCollectionView.layer.masksToBounds = true
        let screensize1 = liveScoreNavTitleView.bounds.size
        let cellwidth1 = floor(screensize1.width)
        let cellheight1 = floor(liveScoreCollectionView.bounds.height)
        
        let layout1 = liveScoreCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout1.itemSize = CGSize(width: cellwidth1, height: cellheight1)
        liveScoreCollectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        navigationItem.titleView = liveScoreNavTitleView
        
        databaseHandleAdded = ref.child("live_scores").observe(.childAdded, with: { (snapshot) in
            let response = snapshot.value
            
            if let data = response {
                self.matchesInfo.append(data)
            }
            
            self.refreshData()
        })
        
        databaseHandleChanged = ref.child("live_scores").observe(.childChanged, with: { (snapshot) in
            let response = snapshot.value
            
            if let data = response {
                for i in 0...(self.matchesInfo.count - 1) {
                    print(i)
                    let item = self.matchesInfo[i]
                    if (item as! NSDictionary)["id"] as! Int == (data as! NSDictionary)["id"] as! Int {
                        self.matchesInfo[i] = data
                    }
                }
                
                self.refreshData()
            }
        })
        
        databaseHandleRemoved = ref.child("live_scores").observe(.childRemoved, with: { (snapshot) in
            let response = snapshot.value
            
            if let data = response {
                //                print("DATA Removed", data)
                self.removeElements = []
                for i in 0...(self.matchesInfo.count - 1) {
                    let item = self.matchesInfo[i]
                    if (item as! NSDictionary)["id"] as! Int == (data as! NSDictionary)["id"] as! Int {
                        print("ID Removed", (item as! NSDictionary)["id"] as! Int)
                        self.removeElements.append(i)
                    }
                }
                
                for index in self.removeElements {
                    self.matchesInfo.remove(at: index)
                }
                self.refreshData()
            }
        })
        scrollingTimer = Timer.scheduledTimer(timeInterval: 20.0, target: self, selector: #selector(self.startTimer(theTimer:)), userInfo: rowIndex, repeats: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print(Int(scrollView.contentOffset.x) / Int(scrollView.frame.width))
        let section = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        rowIndex = section
    }
    
    @objc func startTimer(theTimer: Timer) {
        
        let numberOfRecords = self.liveMatches.count - 1
        if rowIndex < numberOfRecords {
            rowIndex = rowIndex + 1
        } else {
            rowIndex = 0
        }
        if self.liveMatches.count != 0 {
            self.liveScoreCollectionView.scrollToItem(at: IndexPath(row: rowIndex, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    
    func refreshData() {
        liveMatches.removeAll()
        for item in matchesInfo {
            if (item as! NSDictionary)["status"] as! Int == 1 && ((item as! NSDictionary)["scorecard"] as! Bool) {
                liveMatches.append(item)
            }
        }
        liveScoreCollectionView.reloadData()
    }
    
    @objc func livescoreOnNav(theTimer: Timer) {
        if logo == 1 {
            UIView.transition(with: self.liveScoreCollectionView, duration: 1.0, options: [.transitionFlipFromBottom], animations:{
                self.liveScoreCollectionView.isHidden = true
            }, completion: nil)
            UIView.transition(with: self.logoImageView, duration: 1.0, options: [.transitionFlipFromBottom], animations:{
                self.logoImageView.isHidden = false
            }, completion: nil)
            logo = 0
        } else {
            if liveMatches.count > 0 {
                UIView.transition(with: liveScoreCollectionView, duration: 1.0, options: [.transitionFlipFromTop], animations:{
                    self.liveScoreCollectionView.isHidden = false
                }, completion: nil)
                UIView.transition(with: self.logoImageView, duration: 1.0, options: [.transitionFlipFromTop], animations:{
                    self.logoImageView.isHidden = true
                }, completion: nil)
                logo = 1
            }
        }
        liveScoreNavTitleView.layoutIfNeeded()
        liveScoreNavTitleView.layoutSubviews()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return liveMatches.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "dashboard_livescores", sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "navLiveScoreCell", for: indexPath) as! LiveScoreNavigationCollectionViewCell
        cell.resultLabel.restart()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let match = liveMatches[indexPath.row] as! NSDictionary
        let teamA = (match["team"] as! NSDictionary)["teamA"] as! NSDictionary
        let teamB = (match["team"] as! NSDictionary)["teamB"] as! NSDictionary
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "navLiveScoreCell", for: indexPath) as! LiveScoreNavigationCollectionViewCell
        cell.teamAName.text = teamA["sh_name"] as? String
        cell.teamBName.text = teamB["sh_name"] as? String
        
        cell.teamAImageView.sd_setImage(with: URL(string: teamA["logo"] as! String), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
        cell.teamBImageView.sd_setImage(with: URL(string: teamB["logo"] as! String), placeholderImage:#imageLiteral(resourceName: "no_team_image_icon"))
        
        if teamA["innings1"] as? NSDictionary != nil {
            if (teamA["innings1"] as! NSDictionary)["status"] as! Int == 1 || (teamA["innings1"] as! NSDictionary)["status"] as! Int == 2 {
                var score = "\((teamA["innings1"] as! NSDictionary)["run_str"] as! String)"
                if (teamA["innings2"] as! NSDictionary)["status"] as! Int == 1 || (teamA["innings2"] as! NSDictionary)["status"] as! Int == 2 {
                    score = "\(score) & \((teamA["innings2"] as! NSDictionary)["run_str"] as! String)"
                }
                cell.scoreLabel.text = "\(teamA["sh_name"] as! String) \(score)"
            } else {
                cell.scoreLabel.text = "-"
            }
        }
        
        if teamB["innings1"] as? NSDictionary != nil {
            if (teamB["innings1"] as! NSDictionary)["status"] as! Int == 1 {
                var score = "\((teamB["innings1"] as! NSDictionary)["run_str"] as! String)"
                if (teamB["innings2"] as! NSDictionary)["status"] as! Int == 1 {
                    score = "\(score) & \((teamB["innings2"] as! NSDictionary)["run_str"] as! String)"
                }
                cell.scoreLabel.text = "\(teamA["sh_name"] as! String) \(score)"
            }
        }
        cell.resultLabel.text = match["result"] as? String
        cell.resultLabel.restart()
        
        return cell
    }
    
    
    func createBadgeNotification()
    {
        notilabel.layer.borderColor = UIColor.clear.cgColor
        notilabel.layer.borderWidth = 2
        notilabel.layer.cornerRadius = notilabel.bounds.size.height / 2
        notilabel.textAlignment = .center
        notilabel.layer.masksToBounds = true
        notilabel.font = UIFont.systemFont(ofSize: 10)
        notilabel.textColor = .white
        notilabel.backgroundColor = .clear
        notilabel.text = ""
        
        // create subview for notification bar button item
        
        if(notificationCount > 0)
        {
            notilabel.text = String(notificationCount)
            if UserDefaults.standard.object(forKey: CommonUtil.NewNotification) as? Bool ?? false {
                notilabel.text = String(notificationCount + 1)
            }
            if(notificationCount > 99)
            {
                notilabel.text = "99+"
            }
            notilabel.backgroundColor = .black
            
        }
        if notificationCount == 0 {
            notilabel.text = ""
            notilabel.backgroundColor = .clear
        }
        rightButton?.addSubview(notilabel)
        UIApplication.shared.applicationIconBadgeNumber = notificationCount
        if UserDefaults.standard.object(forKey: CommonUtil.NewNotification) as? Bool ?? false {
            UIApplication.shared.applicationIconBadgeNumber = notificationCount + 1
        }
        
    }
    
    func setWelcomeNotification() {
        let label = UILabel(frame: CGRect(x: 10, y: -5, width: 16, height: 16))
        label.layer.borderColor = UIColor.clear.cgColor
        label.layer.borderWidth = 2
        label.layer.cornerRadius = label.bounds.size.height / 2
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.font = UIFont.systemFont(ofSize: 10)
        label.textColor = .white
        label.backgroundColor = .black
        label.text = "1"
        rightButton?.addSubview(label)
    }
    
    @objc func navigateToNotification() {
        setNeedsStatusBarAppearanceUpdate()
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let notificationViewController = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        notificationViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        notificationViewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
        self.present(notificationViewController, animated: true, completion: nil)
    }
    
    override var canBecomeFirstResponder: Bool {
        get {
            return true
        }
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            shake = shake + 1
            switch shake {
            case 1: isPlayerFilterSelected = true
            isTeamFilterSelected = false
            isOpenMatchFilterSelected = false
            isMatchFilterSelected = false
                break
            case 2: isPlayerFilterSelected = false
            isTeamFilterSelected = true
            isOpenMatchFilterSelected = false
            isMatchFilterSelected = false
                break
            case 3: isPlayerFilterSelected = false
            isTeamFilterSelected = false
            isOpenMatchFilterSelected = true
            isMatchFilterSelected = false
                break
            case 4: isPlayerFilterSelected = false
            isTeamFilterSelected = false
            isOpenMatchFilterSelected = false
            isMatchFilterSelected = true
                break
            case 5: isPlayerFilterSelected = true
            isTeamFilterSelected = true
            isOpenMatchFilterSelected = true
            isMatchFilterSelected = true
            shake = 0
                break
            default:
                break
            }
        }
        setFilterTick()
        refreshMarkers()
    }
    
    override var prefersStatusBarHidden: Bool {
        return navigationController?.isNavigationBarHidden == true
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return UIStatusBarAnimation.slide
    }
    
    //floating button for create team ,create match and search
    func layoutFAB() {
        
        let item = FloatyItem()
        item.buttonColor = CommonUtil.themeRed
        item.circleShadowColor = UIColor.black
        item.title = "Create Team"
        item.iconImageView.image = #imageLiteral(resourceName: "create_team_image")
        item.handler = { item in
            CommonUtil.userActivityDetected(viewController: self, activityType: "CREATE TEAM", status: "1")
            self.createTeamVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateTeamViewController") as? CreateTeamViewController
            self.createTeamVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.createTeamVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.createTeamVC?.isOnBoardUser = false
            self.present(self.createTeamVC!, animated: true, completion: nil)
        }
        floaty.addItem(item: item)
        
        let item1 = FloatyItem()
        item1.buttonColor = CommonUtil.themeRed
        item1.circleShadowColor = UIColor.black
        item1.iconImageView.image = #imageLiteral(resourceName: "icon_create_match")
        item1.title = "Create Match"
        item1.handler = { item1 in
            CommonUtil.userActivityDetected(viewController: self, activityType: "CREATE MATCH", status: "1")
            if CommonUtil.NewCreateMatchFlow {
                let newCreateMatchVC = UIStoryboard(name: "NewCreateMatch", bundle: nil).instantiateViewController(withIdentifier: "NewCreateMatchViewController") as? NewCreateMatchViewController
                    newCreateMatchVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    newCreateMatchVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    newCreateMatchVC?.isOnBoardUser = false
                self.present(newCreateMatchVC!, animated: true, completion: nil)
            } else {
                self.createMatchVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateMatchViewController") as? CreateMatchViewController
                self.createMatchVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                self.createMatchVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.createMatchVC?.isOnBoardUser = false
                self.present(self.createMatchVC!, animated: true, completion: nil)
            }
        }
        floaty.addItem(item: item1)
        
        let item2 = FloatyItem()
        item2.buttonColor = CommonUtil.themeRed
        item2.circleShadowColor = UIColor.black
        item2.iconImageView.image = #imageLiteral(resourceName: "ic_search_white_24dp")
        item2.title = "Search"
        item2.handler = { item2 in
            UserDefaults.standard.set(true, forKey: CommonUtil.SearchOpenMatchesDetails)
            CommonUtil.userActivityDetected(viewController: self, activityType: "SEARCH", status: "1")
            let searchVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController
            searchVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            searchVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        
            self.present(searchVC!, animated: true, completion: nil)
        }
        floaty.addItem(item: item2)
        
        floaty.plusColor = UIColor.white
        floaty.buttonColor = CommonUtil.themeRed
        floaty.paddingX = 10
        floaty.fabDelegate = self
        
        self.view.addSubview(floaty)
    }
    
    //From date set and get latitude & longitude
    @objc func dismissPicker1() {
        view.endEditing(true)
        print("Getting Items for date3")
        if fromDateTextField.text != "From date" && toDateTextField.text != "To date" {
            print("Getting Items for date4")
            getNearByItems(lat: String(UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double), lon: String(UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double))
        }
    }
    
    //To date set and get latitude & longitude
    @objc func dismissPicker2() {
        view.endEditing(true)
        print("Getting Items for date1")
        if fromDateTextField.text != "From date" && toDateTextField.text != "To date" {
            print("Getting Items for date2")
            getNearByItems(lat: String(UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double), lon: String(UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double))
        }
    }
    
    //date format for from date
    @objc func handleFromDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        fromDateTextField.text = dateFormatter.string(from: (from_date_picker?.date)!)
        to_date_picker?.minimumDate = from_date_picker?.date
    }
    
    
    func startTimer(match_ID: String) {
        timer?.invalidate()   // just in case you had existing `Timer`, `invalidate` it before we lose our reference to it
        timer = Timer.scheduledTimer(withTimeInterval: 60.0, repeats: true) { [weak self] _ in
            SocketConnectionsclass.matchStatusEmit(match_id:"\(match_ID)")
        }
    }
    
    func stopTimer() {
        print("Stoping Timer")
        timer?.invalidate()
    }
    
    deinit {
        stopTimer()
    }
    
    @objc func matchAdminDetail(_ notification: NSNotification) {
        if CommonUtil.NewLiveMatchFlow {
            let adminDetail = (notification.userInfo![AnyHashable("matchAdminDetail")] as! NSArray)[0] as! NSDictionary
            print("Match Admin Detail", adminDetail)
            if String(adminDetail["ADMIN"] as! Int) == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String || String(adminDetail["SUPER_ADMIN"] as! Int) == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String {
                
                switch (adminDetail["STEP_NAME"] as! String) {
                    
                case "TOSS": let tossVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "GoForTaskViewController") as? GoForTaskViewController
                        tossVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        tossVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        tossVC?.prodSeconds = timeLeftForMatch * 60
                        tossVC?.matchID = adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)"
                        self.present(tossVC!, animated: true, completion: nil)
                    break
                    
                case "toss_won_team":
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissGoForTossView"), object: nil)
                    let showTossVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "TossWonByViewController") as? TossWonByViewController
                            showTossVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                            showTossVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            showTossVC?.matchID = adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)"
                            showTossVC?.adminDetail = adminDetail
                            self.present(showTossVC!, animated: true, completion: nil)
                            print("Go to toss result")
                
                            let matchAdminDetail:[String: Any] = ["matchAdminDetail": adminDetail]
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshTossPage"), object: nil, userInfo: matchAdminDetail)
                    break
                    
                case "over_count":
                    break
                    
                case "toss_won_selection":
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissTossWonView"), object: nil)
                            let batOrBowlVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "ChooseBowlingOrBattingViewController") as? ChooseBowlingOrBattingViewController
                            batOrBowlVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                            batOrBowlVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            batOrBowlVC?.matchID = adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)"
                            batOrBowlVC?.adminDetail = adminDetail
                            self.present(batOrBowlVC!, animated: true, completion: nil)
                            let matchAdminDetail:[String: Any] = ["matchAdminDetail": adminDetail]
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshBatBowlPage"), object: nil, userInfo: matchAdminDetail)
                            
                    break
                
                case "BAT_USER_ID":
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissBatBowlView"), object: nil)
                    getPlayerListForMatch(adminDetail: adminDetail)
                    break
                    
                case "bowler":
                    break
                    
                case "striker":
                    break
                    
                case "non-striker":
                    break
                    
                case "change-scorer":
                    break
                    
                case "SCORE_BOARD":
                    if adminDetail["SCORER"] as? String ?? "\(adminDetail["SCORER"] as! Int)" == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String {
                        let teamScoreVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "TeamScorerViewController") as! TeamScorerViewController
                        teamScoreVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        teamScoreVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        teamScoreVC.MATCH_ID =  adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)"
                        self.present(teamScoreVC, animated: true, completion: nil)
                    }
                    break
                    
                default: print("DEFAULT")
                    break
                }
            }
        } else {
            let adminDetail = (notification.userInfo![AnyHashable("matchAdminDetail")] as! NSArray)[0] as! NSDictionary
            print("Match Admin Detail", adminDetail)
            
            if String(adminDetail["SCORER"] as! Int) == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String {
                
                print("Goto Step ", adminDetail["STEP_NAME"] as! String)
                if adminDetail["STEP_NAME"] as! String == "SCORE_BOARD" {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissNonScorerView"), object: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissMyProfileFunction"), object: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissTeamProfileFunction"), object: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissPlayerProfileFunction"), object: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissFixedMatchesFunction"), object: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissOpenMatchesFunction"), object: nil)
                    let alert = UIAlertController(title: "Match Alert", message: "You have a match to be played! Do you want to continue match?", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: { action in
                        let scorerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScorerViewController") as? ScorerViewController
                        scorerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        scorerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        scorerVC?.MATCH_ID = adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)"
                        self.stopTimer()
                        self.present(scorerVC!, animated: true, completion: nil)
                    }))
                    alert.addAction(UIAlertAction(title: "Later", style: UIAlertActionStyle.cancel, handler:{ action in
                        print("No")
                        self.startTimer(match_ID: adminDetail["MATCH_ID"] as? String ?? String(adminDetail["MATCH_ID"] as! Int))
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    print("Go to non scorer activity")
                    alertNonScorer = UIAlertController(title: "Match Alert", message: "You have a match to be played! Do you want to go for toss?", preferredStyle: UIAlertControllerStyle.alert)
                    alertNonScorer?.addAction(UIAlertAction(title: "Toss", style: UIAlertActionStyle.default, handler: { action in
                        self.mainActivityIndicator.startAnimating()
                        self.preventUserInteractionLoader.isHidden = false
                        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\":\"\(adminDetail["MATCH_ID"] as! String)\"}", mod: "Match", actionType: "get-match-teams", callback: { (response) in
                            print(response)
                            if response as? String != "error" {
                                let showTossVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LiveMatchViewController") as? LiveMatchViewController
                                showTossVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                                showTossVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                                showTossVC?.matchInfo = response
                                showTossVC?.MATCH_ID = adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)"
                                showTossVC?.isFromDashBoard = true
                                self.stopTimer()
                                self.present(showTossVC!, animated: true, completion: nil)
                            } else {
                                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                            }
                            self.mainActivityIndicator.stopAnimating()
                            self.preventUserInteractionLoader.isHidden = true
                            
                        })
                        
                    }))
                    alertNonScorer?.addAction(UIAlertAction(title: "Later", style: UIAlertActionStyle.cancel, handler:{ action in
                        print("No")
                        self.startTimer(match_ID: adminDetail["MATCH_ID"] as! String)
                    }))
                    self.present(alertNonScorer!, animated: true, completion: nil)
                }
            }
        }
    }
    
    func getPlayerListForMatch(adminDetail: NSDictionary) {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"MATCH_ID\":\"\(adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)")\"}", mod: "MatchScore", actionType: "match-score-details", callback: { (response) in
            print(response)
            if response as? String != "error" {
                let toss = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["toss"] as! NSDictionary
                let team = ((response as! NSDictionary)["XSCData"] as! NSDictionary)["team"] as! NSDictionary
                let teamA = team["teamA"] as! NSDictionary
                let teamB = team["teamB"] as! NSDictionary
                var battingOrder: NSArray = []
                var bowlingOrder: NSArray = []
                
                if teamA["match_team_id"] as? String ?? "\(teamA["match_team_id"] as! Int)" == toss["won"] as? String ?? "\(toss["won"] as! Int)" {
                    if toss["decide"] as! String == "BAT" {
                        battingOrder = teamA["players"] as! NSArray
                        bowlingOrder = teamB["players"] as! NSArray
                    } else {
                        battingOrder = teamB["players"] as! NSArray
                        bowlingOrder = teamA["players"] as! NSArray
                    }
                    
                } else if teamB["match_team_id"] as? String ?? "\(teamB["match_team_id"] as! Int)" == toss["won"] as? String ?? "\(toss["won"] as! Int)" {
                    if toss["decide"] as! String == "BAT" {
                        battingOrder = teamB["players"] as! NSArray
                        bowlingOrder = teamA["players"] as! NSArray
                    } else {
                        battingOrder = teamA["players"] as! NSArray
                        bowlingOrder = teamB["players"] as! NSArray
                    }
                }
                
                if adminDetail["BAT_USER_ID"] as? String ?? "\(adminDetail["BAT_USER_ID"] as! Int)" == UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as? String
                {
                    // Go to batting flow
                    let strikerVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "NewChooseStrikerViewController") as? NewChooseStrikerViewController
                    strikerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    strikerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    strikerVC?.matchID = adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)"
                    strikerVC?.adminDetail = adminDetail
                    strikerVC?.playerList = battingOrder
                    self.present(strikerVC!, animated: true, completion: nil)
                } else {
                    // Go to bowling flow
                    let bowlerVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "ChooseABowlerViewController") as? ChooseABowlerViewController
                    bowlerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    bowlerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    bowlerVC?.matchID = adminDetail["MATCH_ID"] as? String ?? "\(adminDetail["MATCH_ID"] as! Int)"
                    bowlerVC?.adminDetail = adminDetail
                    bowlerVC?.playerList = bowlingOrder
                    self.present(bowlerVC!, animated: true, completion: nil)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
        })
    }
    
    @objc func UpcomingMatch(_ notification: NSNotification) {
        let upcomingMatchinfo = (notification.userInfo![AnyHashable("upcoming")] as! NSArray)[0] as! NSDictionary
        print("Upcoming Match info", upcomingMatchinfo)
        
        let dateFormatter = DateFormatter()
        let userCalendar = Calendar.current
        let requestedComponent: Set<Calendar.Component> = [.month,.day,.hour,.minute,.second]
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //        dateFormatter.dateFormat = "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'"
        //        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let startTime = Date()
        let endTime = dateFormatter.date(from: "\(upcomingMatchinfo["DATE"] as! String) \(upcomingMatchinfo["TIME"] as! String)")
        //        let endTime = dateFormatter.date(from: "\(upcomingMatchinfo["isodate"] as! String)")
        let timeDifference = userCalendar.dateComponents(requestedComponent, from: startTime, to: endTime!)
        
        print("time difference", timeDifference.month,timeDifference.day,timeDifference.hour,timeDifference.minute)
        if CommonUtil.NewLiveMatchFlow {
            let dateFormatter =  DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let date1 = dateFormatter.date(from: upcomingMatchinfo["CURRENT_TIME"] as! String)
            // return the timeZone of your device i.e. America/Los_angeles
            let timeZone = TimeZone.autoupdatingCurrent.identifier as String
            dateFormatter.timeZone = TimeZone(identifier: timeZone)
            let date2 = dateFormatter.string(from: date1!)
            print("LOCAL TIME", date2)
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSzzz"
            let date = dateFormatter1.date(from: date2)
            
            let startTime = date
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let endTime = dateFormatter2.date(from: "\(upcomingMatchinfo["DATE"] as! String) \(upcomingMatchinfo["TIME"] as! String)")
            //        let endTime = dateFormatter.date(from: "\(upcomingMatchinfo["isodate"] as! String)")
            
            let timeDifference = userCalendar.dateComponents(requestedComponent, from: startTime!, to: endTime!)
            
            print("time difference", timeDifference.month,timeDifference.day,timeDifference.hour,timeDifference.minute)
            timeLeftForMatch = (timeDifference.hour! * 60) + timeDifference.minute!
            if timeDifference.month! <= 0 {
                if timeDifference.day! <= 0 {
                    if timeDifference.hour! <= 0 {
                        if timeDifference.minute! < 15 {
                            SocketConnectionsclass.matchStatusEmit(match_id:"\(upcomingMatchinfo["MATCH_ID"] as! Int)")
                        } else {
                            _ = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(sendUpcomminingMatchEmit), userInfo: nil, repeats: false)
                        }
                    } else {
//                        _ = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(sendUpcomminingMatchEmit), userInfo: nil, repeats: false)
                    }
                }
            }
        } else {
            if timeDifference.month! <= 0 {
                if timeDifference.day! <= 0 {
                    if timeDifference.hour! <= 0 {
                        if timeDifference.minute! <= 30 {
                            SocketConnectionsclass.matchStatusEmit(match_id:"\(upcomingMatchinfo["MATCH_ID"] as! Int)")
                        }
                    }
                }
            }
        }
    }
    
    @objc func sendUpcomminingMatchEmit() {
        NSLog("SENDING upcoming match update")
        SocketConnectionsclass.getMyUpcomingMatches()
    }
    
    @objc func handleToDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        toDateTextField.text = dateFormatter.string(from: (to_date_picker?.date)!)
        from_date_picker?.maximumDate = to_date_picker?.date
    }
    
    @objc func filterPlayerTapFunction(sender:UITapGestureRecognizer) {
        isPlayerFilterSelected = !isPlayerFilterSelected
        setFilterTick()
        refreshMarkers()
    }
    
    @objc func filterTeamTapFunction(sender:UITapGestureRecognizer) {
        isTeamFilterSelected = !isTeamFilterSelected
        setFilterTick()
        refreshMarkers()
    }
    
    @objc func filterOpenMatchTapFunction(sender:UITapGestureRecognizer) {
        isOpenMatchFilterSelected = !isOpenMatchFilterSelected
        setFilterTick()
        refreshMarkers()
    }
    
    @objc func filterMatchTapFunction(sender:UITapGestureRecognizer) {
        isMatchFilterSelected = !isMatchFilterSelected
        setFilterTick()
        refreshMarkers()
    }
    
    @objc func addressTapFunction(sender:UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "dashboard_LocationPicker", sender: self)
    }
    
    @objc func filterTapFunction(sender:UITapGestureRecognizer) {
        if filterIsOpen {
            UIView.transition(with: filterView, duration: 0.4, options: .transitionFlipFromBottom, animations: {
                self.filterView.isHidden = true
                self.filterView.layoutIfNeeded()
                UIView.transition(with: self.filterImageView, duration: 0.4, options: .transitionFlipFromBottom, animations: {
                    self.filterImageView.image = #imageLiteral(resourceName: "baseline_keyboard_arrow_down_black_24pt_1x")
                    self.filterImageView.layoutIfNeeded()
                    //                    self.filterView.frame = CGRect(x: 0, y: self.filterView.frame.height, width: self.filterView.frame.width, height: self.filterView.frame.height)
                }, completion: nil)
            }) { (bool) in
                
            }
        } else {
            UIView.transition(with: filterView, duration: 0.4, options: .transitionFlipFromTop, animations: {
                
                self.filterView.isHidden = false
                self.filterView.layoutIfNeeded()
                UIView.transition(with: self.filterImageView, duration: 0.4, options: .transitionFlipFromTop, animations: {
                    self.filterImageView.image = #imageLiteral(resourceName: "baseline_keyboard_arrow_up_black_24pt_1x")
                    self.filterImageView.layoutIfNeeded()
                    //                    self.filterView.frame = CGRect(x: 0, y: -self.filterView.frame.height, width: self.filterView.frame.width, height: self.filterView.frame.height)
                }, completion: nil)
            }) { (bool) in
                
            }
        }
        filterIsOpen = !filterIsOpen
    }
    
    @IBAction func onSliderValueChanged(_ sender: CustomSlide) {
        distanceLabel.text =  "\(Int(sender.value)) km"
        currentDistance = Double(sender.value)
        refreshMarkers()
    }
    
    @objc func showMyProfile(){
        guard
            let myProfileVC = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "MyProfileViewController") as? MyProfileViewController
            else { return }
        addPullUpController(myProfileVC)
    }
    
    @objc func joinMatchVCFunction() {
        joinMatchVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JoinMatchViewController") as? JoinMatchViewController
        joinMatchVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        joinMatchVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(joinMatchVC!, animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        latitude = location.coordinate.latitude
        longitude = location.coordinate.longitude
        if ProcessInfo.processInfo.environment["SIMULATOR_DEVICE_NAME"] != nil {
            print("Simulator")
            latitude = 13.095276
            longitude = 80.169093
        }
        let span:MKCoordinateSpan = MKCoordinateSpanMake(0.05, 0.05)
        
        let myLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        
        let region:MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        
        if gotlocation {
            UserDefaults.standard.set(latitude, forKey: CommonUtil.TEMPLATITUDE)
            UserDefaults.standard.set(longitude, forKey: CommonUtil.TEMPLONGITUDE)
            getNearByItems(lat: String(latitude), lon: String(longitude))
            print("GETTING MY LOCATION")
            getAddressFromLatLon(pdblLatitude: latitude, withLongitude: longitude,myLocation: true)
            gotlocation = false
            map.setRegion(region, animated: true)
            map.showsUserLocation = true
        }
        manager.stopUpdatingLocation()
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let center = mapView.centerCoordinate
        print(center)
    }
    
    @IBAction func myLocationButtonClick(_ sender: Any) {
        gotlocation = true
        manager?.startUpdatingLocation()
    }
    
    @objc func RefreshMapView() {
        gotlocation = true
        manager?.startUpdatingLocation()
    }
    
    @objc func CreateMatchVCFunction() {
        if CommonUtil.NewCreateMatchFlow {
          let  newCreateMatchVC = UIStoryboard(name: "NewCreateMatch", bundle: nil).instantiateViewController(withIdentifier: "NewCreateMatchViewController") as? NewCreateMatchViewController
            newCreateMatchVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            newCreateMatchVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(newCreateMatchVC!, animated: true, completion: nil)
        } else {
            createMatchVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateMatchViewController") as? CreateMatchViewController
            createMatchVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            createMatchVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(createMatchVC!, animated: true, completion: nil)
        }
    }
    
    @objc func ShowTossVCFunction(_ notification: NSNotification) {
        if CommonUtil.NewLiveMatchFlow {
            let showTossVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LiveMatchViewController") as? LiveMatchViewController
            showTossVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            showTossVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            showTossVC?.matchInfo = notification.userInfo![AnyHashable("matchInfo")]
            showTossVC?.MATCH_ID = notification.userInfo![AnyHashable("Match_ID")] as? String
            showTossVC?.isFromDashBoard = false
            self.present(showTossVC!, animated: true, completion: nil)
        } else {
            showTossVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LiveMatchViewController") as? LiveMatchViewController
            showTossVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            showTossVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            showTossVC?.matchInfo = notification.userInfo![AnyHashable("matchInfo")]
            self.present(showTossVC!, animated: true, completion: nil)
        }
    }
    
    @objc func createTeamVCFunction() {
        createTeamVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateTeamViewController") as? CreateTeamViewController
        createTeamVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        createTeamVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        createTeamVC?.isOnBoardUser = false
        self.present(createTeamVC!, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        UserDefaults.standard.set(false, forKey: CommonUtil.USERSELECTEDLOCATION)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if UserDefaults.standard.object(forKey: "ONBOARD") as? Bool ?? true {
            self.present(joinTeamVC!, animated: true)
            UserDefaults.standard.set(true, forKey: CommonUtil.NewNotification)
            if UserDefaults.standard.object(forKey: CommonUtil.NewNotification) as? Bool ?? false {
                print("true,notification can access")
                createNotification()
                setWelcomeNotification()
            } else {
                print("false, sorry can not access")
                
            }
            UserDefaults.standard.set(false, forKey: "ONBOARD")
        }
        if (UserDefaults.standard.object(forKey: CommonUtil.USERSELECTEDLOCATION) as? Bool ?? false) {
            latitude = UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double
            longitude = UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double
            let span:MKCoordinateSpan = MKCoordinateSpanMake(0.05, 0.05)
            
            let myLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
            
            let region:MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
            
            map.setRegion(region, animated: true)
            map.showsUserLocation = true
            
            let mapCamera = MKMapCamera()
            mapCamera.centerCoordinate = myLocation
            mapCamera.pitch = 25
            mapCamera.altitude = 500 // example altitude
            mapCamera.heading = 45
            
            // set the camera property
            map.camera = mapCamera
            
            getAddressFromLatLon(pdblLatitude: UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double, withLongitude: UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double, myLocation: true)
            getNearByItems(lat: String(latitude), lon: String(longitude))
        }
        
        //get notification count
        updateNotificationCount()
        getMyTeams()
        getMyMatches()
        updateFCMToken()
    }
    
    func dkNavDrawerSelection(_ selectionIndex: Int) {
        print(selectionIndex)
        switch selectionIndex {
        case 0: NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissMyProfileFunction"), object: nil)
        showMyProfile()
            break
        case 1: self.performSegue(withIdentifier: "dashboard_message", sender: self)
            break
        case 2: self.performSegue(withIdentifier: "dashboard_livescores", sender: self)
            break
        case 3: self.performSegue(withIdentifier: "dashboard_settings", sender: self)
            break
        case 4: UIApplication.shared.open(URL(string: "https://itunes.apple.com/app/id1367161611")!)
            break
        case 5: self.performSegue(withIdentifier: "dashBoardtoAboutUs", sender: self)
            break
        case 6: self.performSegue(withIdentifier: "dashBoardToCricSpace", sender: self)
            break
        default:
            break
        }
    }
    
    @IBAction func menuButtonClicked(_ sender: Any) {
        rootNav?.drawerToggle()
    }
    
    func getNearByItems(lat: String, lon: String) {
        var data = ""
        if fromDateTextField.text != "From date" && toDateTextField.text != "To date" {
            data = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"LATITUDE\":\"\(lat)\",\"LONGITUDE\":\"\(lon)\",\"IS_BATSMAN\":\"\(true)\",\"IS_BOWLER\":\"\(true)\",\"IS_KEEPER\":\"\(true)\",\"DISTANCE\":\"\(25)\",\"DATE_FROM\":\"\(fromDateTextField.text!)\",\"DATE_TO\":\"\(toDateTextField.text!)\"}"
        } else {
            data = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"LATITUDE\":\"\(lat)\",\"LONGITUDE\":\"\(lon)\",\"IS_BATSMAN\":\"\(true)\",\"IS_BOWLER\":\"\(true)\",\"IS_KEEPER\":\"\(true)\",\"DISTANCE\":\"\(25)\"}"
        }
        mainActivityIndicator.startAnimating()
        preventUserInteractionLoader.isHidden = false
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: data , mod: "Map", actionType: "nearest-items") { (response: Any) in
            
            if response as? String != "error" {
                let allDetails = (response as! NSDictionary)["XSCData"] as! NSDictionary
                print(allDetails)
                
                let encodedData = NSKeyedArchiver.archivedData(withRootObject: allDetails)
                UserDefaults.standard.set(encodedData, forKey: CommonUtil.MYMARKERS)
                
                for anno : CustomAnnotation in self.playerAnnotationList {
                    self.map.removeAnnotation(anno)
                }
                self.playerAnnotationList.removeAll()
                if self.isPlayerFilterSelected {
                    for player in (allDetails["NEAREST_PLAYERS"] as! NSArray) {
                        let plyr = player as! NSDictionary
                        if Double(plyr["DISTANCE"] as! String)! <= self.currentDistance {
                            let annotation = CustomAnnotation(coordinate: CLLocationCoordinate2D(latitude: Double(plyr["LATITUDE"] as! String)!, longitude: Double(plyr["LONGITUDE"] as! String)!))
                            
                            annotation.title = plyr["ADDRESS_ID"] as? String
                            annotation.subtitle = "Player"
                            annotation.address = plyr["ADDRESS"] as? String
                            self.map.addAnnotation(annotation)
                            self.playerAnnotationList.append(annotation)
                        }
                    }
                }
                
                for anno : CustomAnnotation in self.teamAnnotationList {
                    self.map.removeAnnotation(anno)
                }
                self.teamAnnotationList.removeAll()
                if self.isTeamFilterSelected {
                    for team in (allDetails["NEAREST_TEAMS"] as! NSArray) {
                        let plyr = team as! NSDictionary
                        if Double(plyr["DISTANCE"] as! String)! <= self.currentDistance {
                            let annotation = CustomAnnotation(coordinate: CLLocationCoordinate2D(latitude: Double(plyr["LATITUDE"] as! String)!, longitude: Double(plyr["LONGITUDE"] as! String)!))
                            
                            annotation.title = plyr["ADDRESS_ID"] as? String
                            annotation.subtitle = "Team"
                            annotation.address = plyr["ADDRESS"] as? String
                            self.map.addAnnotation(annotation)
                            self.teamAnnotationList.append(annotation)
                        }
                    }
                }
                
                
                for anno : CustomAnnotation in self.openMatchAnnotationList {
                    self.map.removeAnnotation(anno)
                }
                self.openMatchAnnotationList.removeAll()
                if self.isOpenMatchFilterSelected {
                    for team in (allDetails["OPEN_MATCHES"] as! NSArray) {
                        let plyr = team as! NSDictionary
                        if Double(plyr["DISTANCE"] as! String)! <= self.currentDistance {
                            let annotation = CustomAnnotation(coordinate: CLLocationCoordinate2D(latitude: Double(plyr["LATITUDE"] as! String)!, longitude: Double(plyr["LONGITUDE"] as! String)!))
                            
                            annotation.title = plyr["ADDRESS_ID"] as? String
                            annotation.subtitle = "Open_Matches"
                            annotation.address = plyr["ADDRESS"] as? String
                            
                            self.map.addAnnotation(annotation)
                            self.openMatchAnnotationList.append(annotation)
                        }
                    }
                }
                
                for anno : CustomAnnotation in self.fixedMatchAnnotationList {
                    self.map.removeAnnotation(anno)
                }
                self.fixedMatchAnnotationList.removeAll()
                if self.isMatchFilterSelected {
                    for team in (allDetails["FIXED_MATCHES"] as! NSArray) {
                        let plyr = team as! NSDictionary
                        if Double(plyr["DISTANCE"] as! String)! <= self.currentDistance {
                            let annotation = CustomAnnotation(coordinate: CLLocationCoordinate2D(latitude: Double(plyr["LATITUDE"] as! String)!, longitude: Double(plyr["LONGITUDE"] as! String)!))
                            
                            annotation.title = plyr["ADDRESS_ID"] as? String
                            annotation.subtitle = "Fixed_Matches"
                            annotation.address = plyr["ADDRESS"] as? String
                            
                            self.map.addAnnotation(annotation)
                            self.fixedMatchAnnotationList.append(annotation)
                        }
                    }
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                self.getNearByItems(lat: String(self.latitude), lon: String(self.longitude))
            }
            self.mainActivityIndicator.stopAnimating()
            self.preventUserInteractionLoader.isHidden = true
        }
    }
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        var i = -1;
        for view in views {
            i += 1;
            if view.annotation is MKUserLocation {
                continue;
            }
            
            // Check if current annotation is inside visible map rect, else go to next one
            let point:MKMapPoint  =  MKMapPointForCoordinate(view.annotation!.coordinate);
            if (!MKMapRectContainsPoint(self.map.visibleMapRect, point)) {
                continue;
            }
            
            let endFrame:CGRect = view.frame;
            
            // Move annotation out of view
            view.frame = CGRect(origin: CGPoint(x: view.frame.origin.x,y :view.frame.origin.y-self.view.frame.size.height), size: CGSize(width: view.frame.size.width, height: view.frame.size.height))
            
            // Animate drop
            let delay = 0 * Double(i)
            UIView.animate(withDuration: 1.0, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations:{() in
                view.frame = endFrame
                view.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                // Animate squash
            }, completion:{(Bool) in
                UIView.animate(withDuration: 0.05, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations:{() in
                    view.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                }, completion: {(Bool) in
                    UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations:{() in
                        view.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                    }, completion: nil)
                })
            })
        }
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "player")
        if annotation.subtitle == "Player" {
            annotationView.image = #imageLiteral(resourceName: "player_3x")
        } else if annotation.subtitle == "Team" {
            annotationView.image = #imageLiteral(resourceName: "team_3x")
        } else if annotation.subtitle == "Open_Matches" {
            annotationView.image = #imageLiteral(resourceName: "open_matches_3x")
        } else if annotation.subtitle == "Fixed_Matches" {
            annotationView.image = #imageLiteral(resourceName: "match_3x")
        }
        annotationView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        return annotationView
    }
    
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
//        UIApplication.shared.isStatusBarHidden = true
        navigationController?.setNavigationBarHidden(navigationController?.isNavigationBarHidden == false, animated: true)
        floaty.isHidden = true
        tournamentBtn.isHidden = true
        tournamentTitle.isHidden = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissFixedMatchesFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissOpenMatchesFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissMyProfileFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissPlayerProfileFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissTeamProfileFunction"), object: nil)
        if filterIsOpen {
            UIView.transition(with: filterView, duration: 0.4, options: .transitionFlipFromBottom, animations: {
                self.filterView.isHidden = true
                self.filterView.layoutIfNeeded()
                UIView.transition(with: self.filterImageView, duration: 0.4, options: .transitionFlipFromBottom, animations: {
                    self.filterImageView.image = #imageLiteral(resourceName: "baseline_keyboard_arrow_down_black_24pt_1x")
                    self.filterImageView.layoutIfNeeded()
                    //                    self.filterView.frame = CGRect(x: 0, y: self.filterView.frame.height, width: self.filterView.frame.width, height: self.filterView.frame.height)
                }, completion: nil)
            }) { (bool) in
                
            }
            filterIsOpen = !filterIsOpen
        }
        if !(view.annotation is MKUserLocation) {
            if view.annotation?.subtitle == "Player" {
                UIView.transition(with: view, duration: 0.1, options: .curveEaseOut, animations: {
                    view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                })
                profileloadingView.isHidden = false
                profileloadingactivityIndicator.startAnimating()
                preventUserInteractionLoader.isHidden = false
                getPlayerProfile(addressID: ((view.annotation?.title)!)!, address: (view.annotation as! CustomAnnotation).address!, isCalledFromTeamProfile: false)
            } else if view.annotation?.subtitle == "Open_Matches" {
                UIView.transition(with: view, duration: 0.1, options: .curveEaseOut, animations: {
                    view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                })
                getOpenMatches(addressId: ((view.annotation?.title)!)!, address: (view.annotation as! CustomAnnotation).address!)
            } else if view.annotation?.subtitle == "Team" {
                UIView.transition(with: view, duration: 0.1, options: .curveEaseOut, animations: {
                    view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                })
                profileloadingView.isHidden = false
                profileloadingactivityIndicator.startAnimating()
                preventUserInteractionLoader.isHidden = false
                getTeamProfile(addressID: ((view.annotation?.title)!)!, address: (view.annotation as! CustomAnnotation).address!, isCalledFromPlayerProfile: false)
            } else if view.annotation?.subtitle == "Fixed_Matches" {
                UIView.transition(with: view, duration: 0.1, options: .curveEaseOut, animations: {
                    view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                })
                getFixedMatches(addressId: ((view.annotation?.title)!)!, address: (view.annotation as! CustomAnnotation).address!)
            }
            UIView.transition(with: map, duration: 0.7, options: .curveLinear, animations: {
                //                self.map.showAnnotations([view.annotation!], animated: true)
                self.map.setCenter(CLLocationCoordinate2D(latitude: ((view.annotation?.coordinate.latitude)! - self.map.region.span.latitudeDelta * 0.2), longitude: (view.annotation?.coordinate.longitude)!), animated: true)
            })
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
//        UIApplication.shared.isStatusBarHidden = false
        navigationController?.setNavigationBarHidden(navigationController?.isNavigationBarHidden == false, animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissFixedMatchesFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissOpenMatchesFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissPlayerProfileFunction"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissTeamProfileFunction"), object: nil)
        floaty.isHidden = false
        tournamentBtn.isHidden = false
        tournamentTitle.isHidden = false
        refreshData()
        if !(view.annotation is MKUserLocation) {
            if view.annotation?.subtitle == "Player" {
                UIView.transition(with: view, duration:0.1, options: .curveEaseIn, animations: {
                    view.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                })
            } else if view.annotation?.subtitle == "Open_Matches" {
                UIView.transition(with: view, duration: 0.1, options: .curveEaseIn, animations: {
                    view.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                })
            } else if view.annotation?.subtitle == "Team" {
                UIView.transition(with: view, duration: 0.1, options: .curveEaseIn, animations: {
                    view.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                })
            }  else if view.annotation?.subtitle == "Fixed_Matches" {
                UIView.transition(with: view, duration: 0.1, options: .curveEaseIn, animations: {
                    view.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                })
            }
        }
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double, myLocation: Bool) {
        //        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude
        //21.228124
        let lon: Double = pdblLongitude
        //72.833770
        
        CommonUtil.getAddressForLatLng(viewcontroller: self, latitude: String(lat), longitude: String(lon), callback: {
            (address: String) -> Void in
            print("GEOCODING \(address)")
            self.locationLabel.text = address
        })

    }
    
    
    
    private func showPlayerProfile(response: Any, address: String, isCalledFromTeamProfile: Bool) {
        guard
            let playerVC = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "PlayerProfileViewController") as? PlayerProfileViewController
            else { return }
        playerVC.playerDetails = response
        playerVC.address = address
        playerVC.currentAddressID = currentAdressID
        playerVC.isCalledFromTeamProfile = isCalledFromTeamProfile
        addPullUpController(playerVC)
        navigationController?.setNavigationBarHidden(navigationController?.isNavigationBarHidden == true, animated: true)
    }
    
    @objc func openMatchesDetailVC(_ notification: NSNotification) {
        guard
            let matchDetailVC = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "OpenMatchesDetailsViewController") as? OpenMatchesDetailsViewController
            else { return }
        
        matchDetailVC.matchInfo = notification.userInfo![AnyHashable("data")] as? NSDictionary
        addPullUpController(matchDetailVC)
    }
    
    private func showTeamProfile(response: Any, address: String, isCalledFromPlayerProfile: Bool) {
        guard
            let teamVC = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "TeamProfileViewController") as? TeamProfileViewController
            else { return }
        teamVC.teamDetails = response
        teamVC.address = address
        teamVC.currentAddressID = currentAdressID
        teamVC.isCalledFromPlayerProfile = isCalledFromPlayerProfile
        addPullUpController(teamVC)
        navigationController?.setNavigationBarHidden(navigationController?.isNavigationBarHidden == true, animated: true)
    }
    
    func getPlayerProfile(addressID: String, address: String, isCalledFromTeamProfile: Bool) {
        var isCached = false
        currentAdressID = addressID
        if let response = UserDefaults.standard.object(forKey: "PLAYERPROFILE_\(currentAdressID!)") {
            print("Player profile loaded from cache")
            isCached = true
            self.showPlayerProfile(response: response, address: address, isCalledFromTeamProfile: isCalledFromTeamProfile)
            self.profileloadingView.isHidden = true
            self.profileloadingactivityIndicator.stopAnimating()
            self.preventUserInteractionLoader.isHidden = true
        }
        if isCalledFromTeamProfile {
            let selectedPlayer = UserDefaults.standard.object(forKey: CommonUtil.SELECTEDPLAYERFROMTEAMPROFILE) as! Data
            let decodedselectedPlayer = NSKeyedUnarchiver.unarchiveObject(with: selectedPlayer) as! NSDictionary
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"PLAYER\",\"USER_ID\":\"\(decodedselectedPlayer["USER_ID"] as! String)\"}", mod: "Player", actionType: "dashborad-player-profile") { (response: Any) in
                print("Player Response", response)
                if response as? String != "error" {
                    print("Player profile updated")
                    UserDefaults.standard.set(response, forKey: "PLAYERPROFILE_\(self.currentAdressID!)")
                    if isCached {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshPlayerProfile"), object: nil)
                    } else {
                        self.showPlayerProfile(response: response, address: address, isCalledFromTeamProfile: isCalledFromTeamProfile)
                    }
                    
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.profileloadingView.isHidden = true
                self.profileloadingactivityIndicator.stopAnimating()
                self.preventUserInteractionLoader.isHidden = true
            }
        } else {
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"PLAYER\",\"ADDRESS_ID\":\"\(addressID)\"}", mod: "Map", actionType: "venue-items") { (response: Any) in
                //            print(response)
                if response as? String != "error" {
                    print("Player profile updated")
                    print("Player profile updated")
                    UserDefaults.standard.set(response, forKey: "PLAYERPROFILE_\(self.currentAdressID!)")
                    if isCached {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshPlayerProfile"), object: nil)
                    } else {
                        self.showPlayerProfile(response: response, address: address, isCalledFromTeamProfile: isCalledFromTeamProfile)
                    }
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.profileloadingView.isHidden = true
                self.profileloadingactivityIndicator.stopAnimating()
                self.preventUserInteractionLoader.isHidden = true
            }
        }
    }
    
    @objc func openTeamVCFromPlayerProfile() {
        profileloadingView.isHidden = false
        profileloadingactivityIndicator.startAnimating()
        preventUserInteractionLoader.isHidden = false
        let selectedTeam = UserDefaults.standard.object(forKey: CommonUtil.SELECTEDTEAMFROMPLAYERPROFILE) as! Data
        let decodedselectedTeam = NSKeyedUnarchiver.unarchiveObject(with: selectedTeam) as! NSDictionary
        getTeamProfile(addressID: decodedselectedTeam["ADDRESS_ID"] as! String, address: decodedselectedTeam["ADDRESS"] as! String, isCalledFromPlayerProfile: true)
    }
    
    private func showOpenMatches(response: Any, address: String) {
        guard
            let openMatchesVC = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "OpenMatchesViewController") as? OpenMatchesViewController
            else { return }
        openMatchesVC.openMatchesDetails = response
        openMatchesVC.address = address
        openMatchesVC.currentAddressID = currentAdressID
        //        playerVC.isCalledFromTeamProfile = isCalledFromTeamProfile
        addPullUpController(openMatchesVC)
    }
    
    private func showFixedMatches(response: Any, address: String) {
        
        guard
            let fixedMatchesVC = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "FixedMatchesViewController") as? FixedMatchesViewController
            else { return }
        fixedMatchesVC.fixedMatchesDetails = response
        fixedMatchesVC.address = address
        fixedMatchesVC.currentAddressID = currentAdressID
        //        playerVC.isCalledFromTeamProfile = isCalledFromTeamProfile
        addPullUpController(fixedMatchesVC)
    }
    
    func getOpenMatches(addressId: String, address: String) {
        profileloadingView.isHidden = false
        profileloadingactivityIndicator.startAnimating()
        preventUserInteractionLoader.isHidden = false
        currentAdressID = addressId
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"OPEN-MATCH\",\"ADDRESS_ID\":\"\(addressId)\"}", mod: "Map", actionType: "venue-items") { (response) in
            //            print(response)
            if response as? String != "error" {
                self.showOpenMatches(response: response, address: address)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.profileloadingView.isHidden = true
            self.profileloadingactivityIndicator.stopAnimating()
            self.preventUserInteractionLoader.isHidden = true
        }
    }
    
    func getFixedMatches(addressId: String, address: String) {
        profileloadingView.isHidden = false
        profileloadingactivityIndicator.startAnimating()
        preventUserInteractionLoader.isHidden = false
        currentAdressID = addressId
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"FIXED-MATCH\",\"ADDRESS_ID\":\"\(addressId)\"}", mod: "Map", actionType: "venue-items") { (response) in
            //            print(response)
            if response as? String != "error" {
                self.showFixedMatches(response: response, address: address)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.profileloadingView.isHidden = true
            self.profileloadingactivityIndicator.stopAnimating()
            self.preventUserInteractionLoader.isHidden = true
        }
    }
    
    @objc func updateNotificationCount() {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Notification", actionType: "notification-count") { (response) in
            print(response)
            if response as? String != "error" {
                //self.showAlert(title: "Oops!", message: (response as! NSDictionary)["XSCData"] as! String )
                print(response)
                self.notificationCount = (response as! NSDictionary)["XSCData"] as! Int
                self.createBadgeNotification()
            } else {
                
            }
            
        }
    }
    
    @objc func openPlayerVCFromTeamProfile() {
        profileloadingView.isHidden = false
        profileloadingactivityIndicator.startAnimating()
        preventUserInteractionLoader.isHidden = false
        let selectedPlayer = UserDefaults.standard.object(forKey: CommonUtil.SELECTEDPLAYERFROMTEAMPROFILE) as! Data
        let decodedselectedPlayer = NSKeyedUnarchiver.unarchiveObject(with: selectedPlayer) as! NSDictionary
        getPlayerProfile(addressID: decodedselectedPlayer["ADDRESS_ID"] as! String, address: decodedselectedPlayer["ADDRESS"] as! String, isCalledFromTeamProfile: true)
    }
    
    func getTeamProfile(addressID: String, address: String, isCalledFromPlayerProfile: Bool) {
        if isCalledFromPlayerProfile {
            let selectedTeam = UserDefaults.standard.object(forKey: CommonUtil.SELECTEDTEAMFROMPLAYERPROFILE) as! Data
            let decodedselectedTeam = NSKeyedUnarchiver.unarchiveObject(with: selectedTeam) as! NSDictionary
            currentAdressID = addressID
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"TEAM\",\"TEAM_ID\":\"\(decodedselectedTeam["TEAM_ID"] as! String)\"}", mod: "Team", actionType: "dashborad-team-profile") { (response: Any) in
                print(response)
                if response as? String != "error" {
                    self.showTeamProfile(response: response, address: address, isCalledFromPlayerProfile: true)
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.profileloadingView.isHidden = true
                self.profileloadingactivityIndicator.stopAnimating()
                self.preventUserInteractionLoader.isHidden = true
            }
        } else {
            currentAdressID = addressID
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TYPE\":\"TEAM\",\"ADDRESS_ID\":\"\(addressID)\"}", mod: "Map", actionType: "venue-items") { (response: Any) in
                print(response)
                if response as? String != "error" {
                    self.showTeamProfile(response: response, address: address, isCalledFromPlayerProfile: false)
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.profileloadingView.isHidden = true
                self.profileloadingactivityIndicator.stopAnimating()
                self.preventUserInteractionLoader.isHidden = true
            }
        }
    }
    
    func setFilterTick() {
        if isPlayerFilterSelected {
            filterPlayerTickButton.isHidden = false
        } else {
            filterPlayerTickButton.isHidden = true
        }
        if isTeamFilterSelected {
            filterTeamTickButton.isHidden = false
        } else {
            filterTeamTickButton.isHidden = true
        }
        if isOpenMatchFilterSelected {
            filterOpenMatchTickButton.isHidden = false
        } else {
            filterOpenMatchTickButton.isHidden = true
        }
        if isMatchFilterSelected {
            filterMatchTickButton.isHidden = false
        } else {
            filterMatchTickButton.isHidden = true
        }
        
        if isMatchFilterSelected || isOpenMatchFilterSelected {
            UIView.transition(with: fromDateTextField, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.fromDateTextFieldWidth.constant = 90
                self.fromDateTextField.isHidden = false
            }, completion: nil)
            UIView.transition(with: toDateTextField, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.toDateTextField.isHidden = false
            }, completion: nil)
        } else {
            UIView.transition(with: fromDateTextField, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.fromDateTextFieldWidth.constant = 0
                self.fromDateTextField.isHidden = true
                self.fromDateTextField.text = "From date"
            }, completion: nil)
            UIView.transition(with: toDateTextField, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.toDateTextField.isHidden = true
                self.toDateTextField.text = "To date"
            }, completion: nil)
        }
        
        UIView.transition(with: filterView, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.filterView.layoutIfNeeded()
        }, completion: nil)
    }
    
    func refreshMarkers() {
        let markerData = UserDefaults.standard.object(forKey: CommonUtil.MYMARKERS) as! Data
        let allDetails = NSKeyedUnarchiver.unarchiveObject(with: markerData) as! NSDictionary
        
        for anno : CustomAnnotation in self.playerAnnotationList {
            self.map.removeAnnotation(anno)
        }
        self.playerAnnotationList.removeAll()
        if isPlayerFilterSelected {
            for player in (allDetails["NEAREST_PLAYERS"] as! NSArray) {
                let plyr = player as! NSDictionary
                if Double(plyr["DISTANCE"] as! String)! <= currentDistance {
                    let annotation = CustomAnnotation(coordinate: CLLocationCoordinate2D(latitude: Double(plyr["LATITUDE"] as! String)!, longitude: Double(plyr["LONGITUDE"] as! String)!))
                    
                    annotation.title = plyr["ADDRESS_ID"] as? String
                    annotation.subtitle = "Player"
                    annotation.address = plyr["ADDRESS"] as? String
                    self.map.addAnnotation(annotation)
                    self.playerAnnotationList.append(annotation)
                }
            }
        }
        for anno : CustomAnnotation in self.teamAnnotationList {
            self.map.removeAnnotation(anno)
        }
        self.teamAnnotationList.removeAll()
        if isTeamFilterSelected {
            for team in (allDetails["NEAREST_TEAMS"] as! NSArray) {
                let plyr = team as! NSDictionary
                if Double(plyr["DISTANCE"] as! String)! <= currentDistance {
                    let annotation = CustomAnnotation(coordinate: CLLocationCoordinate2D(latitude: Double(plyr["LATITUDE"] as! String)!, longitude: Double(plyr["LONGITUDE"] as! String)!))
                    
                    annotation.title = plyr["ADDRESS_ID"] as? String
                    annotation.subtitle = "Team"
                    annotation.address = plyr["ADDRESS"] as? String
                    self.map.addAnnotation(annotation)
                    self.teamAnnotationList.append(annotation)
                }
            }
        }
        
        for anno : CustomAnnotation in self.openMatchAnnotationList {
            self.map.removeAnnotation(anno)
        }
        self.openMatchAnnotationList.removeAll()
        if isOpenMatchFilterSelected {
            for team in (allDetails["OPEN_MATCHES"] as! NSArray) {
                let plyr = team as! NSDictionary
                if Double(plyr["DISTANCE"] as! String)! <= currentDistance {
                    let annotation = CustomAnnotation(coordinate: CLLocationCoordinate2D(latitude: Double(plyr["LATITUDE"] as! String)!, longitude: Double(plyr["LONGITUDE"] as! String)!))
                    
                    annotation.title = plyr["ADDRESS_ID"] as? String
                    annotation.subtitle = "Open_Matches"
                    annotation.address = plyr["ADDRESS"] as? String
                    
                    self.map.addAnnotation(annotation)
                    self.openMatchAnnotationList.append(annotation)
                }
            }
        }
        
        for anno : CustomAnnotation in self.fixedMatchAnnotationList {
            self.map.removeAnnotation(anno)
        }
        self.fixedMatchAnnotationList.removeAll()
        if isMatchFilterSelected {
            for team in (allDetails["FIXED_MATCHES"] as! NSArray) {
                let plyr = team as! NSDictionary
                if Double(plyr["DISTANCE"] as! String)! <= currentDistance {
                    let annotation = CustomAnnotation(coordinate: CLLocationCoordinate2D(latitude: Double(plyr["LATITUDE"] as! String)!, longitude: Double(plyr["LONGITUDE"] as! String)!))
                    
                    annotation.title = plyr["ADDRESS_ID"] as? String
                    annotation.subtitle = "Fixed_Matches"
                    annotation.address = plyr["ADDRESS"] as? String
                    
                    self.map.addAnnotation(annotation)
                    self.fixedMatchAnnotationList.append(annotation)
                }
            }
        }
    }
    
    func initializeNoficationCentreMethods() {
        NotificationCenter.default.addObserver(self, selector: #selector(joinMatchVCFunction), name: NSNotification.Name(rawValue: "joinMatchVCFunction"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(createTeamVCFunction), name: NSNotification.Name(rawValue: "createTeamVCFunction"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateNotificationCount), name: NSNotification.Name(rawValue: "updateNotificationCount"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(UpcomingMatch), name: NSNotification.Name(rawValue: "UpcomingMatch"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(matchAdminDetail), name: NSNotification.Name(rawValue: "matchAdminDetail"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CreateMatchVCFunction), name: NSNotification.Name(rawValue: "CreateMatchVCFunction"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openTeamVCFromPlayerProfile), name: NSNotification.Name(rawValue: "openTeamVCFromPlayerProfile"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openPlayerVCFromTeamProfile), name: NSNotification.Name(rawValue: "openPlayerVCFromTeamProfile"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showMyProfile), name: NSNotification.Name(rawValue: "showMyProfile"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openMatchesDetailVC), name: NSNotification.Name(rawValue: "openMatchesDetailVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ShowTossVCFunction), name: NSNotification.Name(rawValue: "ShowTossVCFunction"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(sendMessage), name: NSNotification.Name(rawValue: "sendMessage"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(sendComposing), name: NSNotification.Name(rawValue: "sendComposing"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(joinGroupChat), name: NSNotification.Name(rawValue: "joinGroupChat"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(sendMessageGroup), name: NSNotification.Name(rawValue: "sendMessageGroup"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(disconnectXMPP), name: NSNotification.Name(rawValue: "disconnectXMPP"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(connectXMPP), name: NSNotification.Name(rawValue: "connectXMPP"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshDrawerMsgCount), name: NSNotification.Name(rawValue: "refreshDrawerMsgCount"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HostTournament), name: NSNotification.Name(rawValue: "HostTournament"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(JoinTournament), name: NSNotification.Name(rawValue: "JoinTournament"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(gotoTournamentPaymentDetails), name: NSNotification.Name(rawValue: "gotoTournamentPaymentDetails"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openNearByTournament), name: NSNotification.Name(rawValue: "openNearByTournament"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openMatchOverViewController), name: NSNotification.Name(rawValue: "openMatchOverViewController"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openNonStrikerViewController), name: NSNotification.Name(rawValue: "openNonStrikerViewController"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openChooseScorerViewController), name: NSNotification.Name(rawValue: "openChooseScorerViewController"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openTeamScoreViewController), name: NSNotification.Name(rawValue: "openTeamScoreViewController"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openScorerViewController), name: NSNotification.Name(rawValue: "openScorerViewController"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(openNotificationPage), name: NSNotification.Name(rawValue: "openNotificationPage"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(RefreshMapView), name: NSNotification.Name(rawValue: "RefreshMapView"), object: nil)
    }
    
    
    
    @objc func openNotificationPage() {
        print("Opening NotificationPage")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier:
            "NotificationViewController") as! NotificationViewController
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func openTeamScoreViewController(_ notification: NSNotification) {
        let teamScoreVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "TeamScorerViewController") as! TeamScorerViewController
        teamScoreVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        teamScoreVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        teamScoreVC.MATCH_ID = notification.userInfo![AnyHashable("match_id")] as? String
        self.present(teamScoreVC, animated: true, completion: nil)
    }
    
    
    @objc func openScorerViewController(_ notification: NSNotification) {
        let scorerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ScorerViewController") as? ScorerViewController
        scorerVC?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        scorerVC?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        scorerVC?.MATCH_ID = notification.userInfo![AnyHashable("match_id")] as? String
        self.present(scorerVC!, animated: true, completion: nil)
    }
    
    @objc func openChooseScorerViewController(_ notification: NSNotification) {
        let playerlist =  notification.userInfo![AnyHashable("playerList")] as? NSArray
        let ScorerVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "ChooseScorerViewController") as! ChooseScorerViewController
        ScorerVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        ScorerVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        ScorerVC.playerlist = playerlist
        ScorerVC.matchID = notification.userInfo![AnyHashable("match_id")] as? String
        ScorerVC.striker = notification.userInfo![AnyHashable("striker")] as? String
        ScorerVC.nonStriker = notification.userInfo![AnyHashable("non-striker")] as? String
        self.present(ScorerVC, animated: true, completion: nil)
    }
    
    @objc func openNonStrikerViewController(_ notification: NSNotification) {
        let playerlist =  notification.userInfo![AnyHashable("playerList")] as? NSArray
        let nonScorerVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "ChooseNonStrikerViewController") as! ChooseNonStrikerViewController
        nonScorerVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nonScorerVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        nonScorerVC.playerlist = playerlist
        nonScorerVC.matchID = notification.userInfo![AnyHashable("match_id")] as? String
        nonScorerVC.striker = notification.userInfo![AnyHashable("striker")] as? String
        self.present(nonScorerVC, animated: true, completion: nil)
    }
    
    @objc func openMatchOverViewController(_ notification: NSNotification) {
        let tossDetail =  notification.userInfo![AnyHashable("tossDetail")] as? NSDictionary
        let overVC = UIStoryboard(name: "LiveScoresLocalMatches", bundle: nil).instantiateViewController(withIdentifier: "HowManyOversViewController") as! HowManyOversViewController
        overVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        overVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        overVC.tossDetail = tossDetail
        self.present(overVC, animated: true, completion: nil)
    }
    
    @objc func openNearByTournament() {
        let nearByTourVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NearByTournamentViewController") as! NearByTournamentViewController
        nearByTourVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nearByTourVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(nearByTourVC, animated: true, completion: nil)
    }
    
    @objc func gotoTournamentPaymentDetails(_ notification: NSNotification) {
        let tourCreate =  notification.userInfo![AnyHashable("tourCreate")] as? NSDictionary
        let maxiEntryDate = notification.userInfo![AnyHashable("maxiEntryDate")] as? Date
        let tourTeams = notification.userInfo![AnyHashable("tourTeams")] as? String
        let tourPay = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TournamentPaymentDetailsViewController") as? TournamentPaymentDetailsViewController
        tourPay?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        tourPay?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        if tourCreate!["IS_PIN_EXIST"] as! Int == 0 {
            tourPay?.hasPin = false
        } else {
            tourPay?.hasPin = true
        }
        tourPay?.tournament_ID = tourCreate!["TOURNAMENT_ID"] as? String ?? "\(tourCreate!["TOURNAMENT_ID"] as! Int)"
        tourPay?.maxiEntryDate = maxiEntryDate
        tourPay?.numberOfTeams = tourTeams
        self.present(tourPay!, animated: true, completion: nil)
    }
    
    @objc func HostTournament() {
        print("hosttournament")
        let hostTourVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateTournamentViewController") as! CreateTournamentViewController
        hostTourVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        hostTourVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(hostTourVC, animated: true, completion: nil)
    }
    
    @objc func JoinTournament(_ notification: NSNotification) {
        print("Join Tournament")
        let joinTournamentCVB = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JoinTournamentViewController") as? JoinTournamentViewController
        joinTournamentCVB?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        joinTournamentCVB?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        //print(notification.userInfo![AnyHashable("data")] as! Int)
        joinTournamentCVB?.tourDetail =  notification.userInfo![AnyHashable("data")] as? NSDictionary
        joinTournamentCVB?.selectedTournament = notification.userInfo![AnyHashable("tournament")] as? NSDictionary
        self.present(joinTournamentCVB!, animated: true, completion: nil)
    }
    
    
    
    @objc func connectXMPP() {
        self.xmppController?.connect()
    }
    
    @objc func disconnectXMPP() {
        stream?.disconnect()
    }
    
    @objc func refreshDrawerMsgCount() {
        rootNav?.refreshDrawerMessageCount()
    }
    
    func fetchChatHistory() {
        
        let query = try? XMLElement(xmlString: "<query xmlns='jabber:iq:roster'/>")
        let iq = XMLElement.element(withName: "iq") as? XMLElement
        iq?.addAttribute(withName: "type", stringValue: "get")
        iq?.addAttribute(withName: "id", stringValue: "u_2")
        iq?.addAttribute(withName: "from", stringValue: "u_2\(CommonUtil.cricdost_chat_domain_name)")
        if let aQuery = query {
            iq?.addChild(aQuery)
        }
        stream?.send(iq!)
    }
    
    @objc func sendComposing(_ notification: NSNotification) {
        fetchChatHistory()
        print("Received Composing", notification.userInfo![AnyHashable("data")] as! NSDictionary)
        let data = notification.userInfo![AnyHashable("data")] as! NSDictionary
        //        let msgbody = data["MSG"] as! String
        let JIDPrefix = data["JID"] as! String
        let message = XMLElement.element(withName: "message") as? XMLElement
        message?.addAttribute(withName: "type", stringValue: "chat")
        message?.addAttribute(withName: "to", stringValue: "\(JIDPrefix)\(CommonUtil.cricdost_chat_domain_name)")
        let xmppMessage = XMPPMessage(from: message!)
        xmppMessage.addComposingChatState()
        stream?.send(xmppMessage)
    }
    
    @objc func sendMessage(_ notification: NSNotification) {
        print("Received notification", notification.userInfo![AnyHashable("data")] as! NSDictionary)
        let data = notification.userInfo![AnyHashable("data")] as! NSDictionary
        let msgbody = data["MSG"] as! String
        let JIDPrefix = data["JID"] as! String
        var clientJid: XMPPJID!
        clientJid = XMPPJID.init(string: "\(JIDPrefix)\(CommonUtil.cricdost_chat_domain_name)")
        let senderJID = clientJid
        let msg = XMPPMessage(type: "chat", to: senderJID)
        msg.addBody(msgbody)
        msg.addSubject(UserDefaults.standard.object(forKey: CommonUtil.IMAGE_URL) as! String)
        stream?.send(msg)
    }
    
    @objc func joinGroupChat(_ notification: NSNotification) {
        
        let data = notification.userInfo![AnyHashable("data")] as! NSDictionary
        let rosterstorage = XMPPRoomMemoryStorage()
        xmppRoom = XMPPRoom(roomStorage: rosterstorage!, jid: XMPPJID(string: "\(data["groupJID"] as! String)\(CommonUtil.cricdost_group_chat_domain_name)")!, dispatchQueue: DispatchQueue.main)
        xmppRoom?.configureRoom(usingOptions: nil)
        xmppRoom?.activate(stream!)
        
        xmppRoom?.addDelegate(self.xmppController!, delegateQueue: DispatchQueue.main)
        xmppRoom?.join(usingNickname: "\((stream?.myJID?.user!)!)-\(UserDefaults.standard.object(forKey: CommonUtil.FULL_NAME) as! String)", history: nil, password: nil)
        xmppRoom?.fetchConfigurationForm()
    }
    
    @objc func sendMessageGroup(_ notification: NSNotification) {
        let data = notification.userInfo![AnyHashable("data")] as! NSDictionary
        let xmppMessage = XMPPMessage(type: "groupchat", to: XMPPJID(string: "\(data["groupJID"] as! String)\(CommonUtil.cricdost_group_chat_domain_name)"))
        xmppMessage.addSubject(data["SUBJECT"] as! String)
        xmppMessage.addBody(data["MSG"] as! String)
        xmppRoom?.send(xmppMessage)
    }
    
    func initializeTapGestures() {
        let addressTap = UITapGestureRecognizer(target: self, action: #selector(addressTapFunction))
        locationLabel.addGestureRecognizer(addressTap)
        
        let filterTap = UITapGestureRecognizer(target: self, action: #selector(filterTapFunction))
        filterImageView.addGestureRecognizer(filterTap)
        
        let filterPlayerTap = UITapGestureRecognizer(target: self, action: #selector(filterPlayerTapFunction))
        filterPlayerImageView.addGestureRecognizer(filterPlayerTap)
        
        let filterTeamTap = UITapGestureRecognizer(target: self, action: #selector(filterTeamTapFunction))
        filterTeamImageView.addGestureRecognizer(filterTeamTap)
        
        let filterOpenMatchTap = UITapGestureRecognizer(target: self, action: #selector(filterOpenMatchTapFunction))
        filterOpenMatchImageVIew.addGestureRecognizer(filterOpenMatchTap)
        
        let filterMatchTap = UITapGestureRecognizer(target: self, action: #selector(filterMatchTapFunction))
        filterMatchImageView.addGestureRecognizer(filterMatchTap)
    }
    
    func getMyTeams() {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"USER_ID\":\"\(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String)\"}", mod: "Player", actionType: "get-player-teams") { (response) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    let myTeamList = (response as! NSDictionary)["XSCData"] as! NSArray
                    
                    for team in myTeamList {
                        let rosterstorage = XMPPRoomMemoryStorage()
                        self.xmppRoom = XMPPRoom(roomStorage: rosterstorage!, jid: XMPPJID(string: "\((team as! NSDictionary)["OPENFIRE_USERNAME"] as! String)\(CommonUtil.cricdost_group_chat_domain_name)")!, dispatchQueue: DispatchQueue.main)
                        self.xmppRoom?.configureRoom(usingOptions: nil)
                        self.xmppRoom?.activate(self.stream!)
                        self.xmppRoom?.addDelegate(self.xmppController!, delegateQueue: DispatchQueue.main)
                        self.xmppRoom?.join(usingNickname: (self.stream?.myJID?.user!)!, history: nil, password: nil)
                        self.xmppRoom?.fetchConfigurationForm()
                    }
                } else {
                    print((response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                print("Something went wrong! Please try again")
            }
        }
    }
    
    func getMyMatches() {
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"USER_ID\":\"\(UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String)\"}", mod: "Match", actionType: "player-match-list") { (response) in
            print(response)
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    let myMatchesList = (response as! NSDictionary)["XSCData"] as! NSArray
                    
                    for team in myMatchesList {
                        let rosterstorage = XMPPRoomMemoryStorage()
                        self.xmppRoom = XMPPRoom(roomStorage: rosterstorage!, jid: XMPPJID(string: "\(((team as! NSDictionary)["MATCH_DETAILS"] as! NSDictionary)["OPENFIRE_USERNAME"] as! String)\(CommonUtil.cricdost_group_chat_domain_name)")!, dispatchQueue: DispatchQueue.main)
                        self.xmppRoom?.configureRoom(usingOptions: nil)
                        self.xmppRoom?.activate(self.stream!)
                        
                        self.xmppRoom?.addDelegate(self.xmppController!, delegateQueue: DispatchQueue.main)
                        
                        self.xmppRoom?.join(usingNickname: (self.stream?.myJID?.user!)!, history: nil, password: nil)
                        self.xmppRoom?.fetchConfigurationForm()
                    }
                    
                } else {
                    print((response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                print("Something went wrong! Please try again")
            }
        }
    }
    
    func updateFCMToken() {
        print("Updating FBTOKEN",UserDefaults.standard.object(forKey: CommonUtil.FB_TOKEN) as? String ?? "")
        let mod = "AA"
        let actionType =  "save-token"
        let subAction = "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as? String ?? "abc")\",\"DEVICE_TOKEN\":\"\(UserDefaults.standard.object(forKey: CommonUtil.FB_TOKEN) as? String ?? "")\"}"
        if (Reachability.isConnectedToNetwork()) {
            let url = URL(string: CommonUtil.BASE_URL)!
            var request = URLRequest(url: url)
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            let postString = "app=CRICDOST&mod=\(mod)&actionType=\(actionType)&subAction=\(subAction)&sessionId=&subMod="
            print(postString)
            request.httpBody = postString.data(using: .utf8)
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {                                                 // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    DispatchQueue.main.async {
                        //callback("error")
                        print("ATTEMPTING RELOADING -- OOPS")
                    }
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(String(describing: response))")
                }
                
                let responseString = String(data: data, encoding: .utf8)
                print("responseString = \(String(describing: responseString))")
                
                do {
                    //create json object from data
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                        //                    print(json)
                        DispatchQueue.main.async {
                            print("FCM send successfully")
                        }
                    }
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            task.resume()
        } else {
            print("NO NETWORK AVAILABLE")
        }
    }
    
    func openPayUMoneyInterface() {
        
        let txnParam = PUMTxnParam()
        //Set the parameters
        txnParam.phone = "9600194295"
        txnParam.email = "karthik@xcelcorp.com"
        txnParam.amount = "1"
        txnParam.environment = .test
        txnParam.firstname = "Umang"
        txnParam.key = "KVA3m4wr" // Live
        txnParam.merchantid = "6389193" // live
        txnParam.txnID = "txnID123"
        txnParam.surl = "http://www.cricdost.com/success.html"
        txnParam.furl = "http://www.cricdost.com/failed.html"
        txnParam.productInfo = "iPhone5C"
        txnParam.udf1 = "udf1"
        txnParam.udf2 = "udf2"
        txnParam.udf3 = "udf3"
        txnParam.udf4 = "udf4"
        txnParam.udf5 = "udf5"
        txnParam.udf6 = "udf6"
        txnParam.udf7 = "udf7"
        txnParam.udf8 = "udf8"
        txnParam.udf9 = "udf9"
        txnParam.udf10 = "udf10"
        let salt = "WvdvqpsyIW" // Live
        
        txnParam.hashValue = generateHash(txnParam, salt: salt)
        
        PlugNPlay.setDisableCompletionScreen(true)
        PlugNPlay.setButtonColor(CommonUtil.themeRed)
        PlugNPlay.setTopBarColor(CommonUtil.themeRed)
        PlugNPlay.setButtonTextColor(UIColor.white)
        PlugNPlay.setReturning(self)
        PlugNPlay.setTopTitleTextColor(UIColor.white)
        
        PlugNPlay.presentPaymentViewController(withTxnParams: txnParam, on: self, withCompletionBlock: { paymentResponse, error, extraParam in
            print("paymentResponse",paymentResponse)
            if error != nil {
                UIUtility.toastMessage(onScreen: error?.localizedDescription)
            } else {
                var message = ""
                if paymentResponse?["result"] != nil && (paymentResponse?["result"] is [AnyHashable : Any]) {
                    message = (paymentResponse?["result"] as! [AnyHashable : Any])["error_Message"] as? String ?? ""
                    if message.isEqual(NSNull()) || message.count == 0 || (message == "No Error") {
                        message = (paymentResponse?["result"] as! [AnyHashable : Any])["status"] as? String ?? ""
                    }
                } else {
                    message = paymentResponse?["status"] as? String ?? ""
                }
                UIUtility.toastMessage(onScreen: message)
            }
        })
    }
    
    
    func generateHash(_ txnParam: PUMTxnParam, salt: String) -> String {
        let hashSequence = "\(txnParam.key!)|\(txnParam.txnID!)|\(txnParam.amount!)|\(txnParam.productInfo!)|\(txnParam.firstname!)|\(txnParam.email!)|\(txnParam.udf1!)|\(txnParam.udf2!)|\(txnParam.udf3!)|\(txnParam.udf4!)|\(txnParam.udf5!)|\(txnParam.udf6!)|\(txnParam.udf7!)|\(txnParam.udf8!)|\(txnParam.udf9!)|\(txnParam.udf10!)|\(salt)"
        
        let hash = createSHA512(hashSequence).replacingOccurrences(of: "<", with: "").replacingOccurrences(of: ">", with: "").replacingOccurrences(of: " ", with: "")
        
        return hash
    }
    
    func createSHA512(_ string: String) -> String{
        var digest = [UInt8](repeating: 0, count: Int(CC_SHA512_DIGEST_LENGTH))
        if let data = string.data(using: String.Encoding.utf8) {
            let value =  data as NSData
            CC_SHA512(value.bytes, CC_LONG(data.count), &digest)
        }
        var digestHex = ""
        for index in 0..<Int(CC_SHA512_DIGEST_LENGTH) {
            digestHex += String(format: "%02x", digest[index])
        }
        
        return digestHex.replacingOccurrences(of: "<", with: "").replacingOccurrences(of: ">", with: "").replacingOccurrences(of: " ", with: "")
    }
    
    func createNotification() {
        print("notification")
        let center = UNUserNotificationCenter.current()
        let content = UNMutableNotificationContent()
        content.title = "CricDost"
        content.subtitle = "Greetings from CricDost..!!"
//        content.body = "Are you crazy to play cricket?...do you have team?... join to the match to play cricket"
        content.sound = UNNotificationSound.default()
        let triger = UNTimeIntervalNotificationTrigger(timeInterval: 5.0, repeats: false)
        let request = UNNotificationRequest(identifier: "NewIdentifier", content: content, trigger: triger)
        center.add(request) { (error) in
            print(error as Any)
        }
    }
}



