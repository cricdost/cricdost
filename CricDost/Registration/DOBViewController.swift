//
//  DOBViewController.swift
//  CricDost
//
//  Created by Jit Goel on 5/25/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import LTMorphingLabel

class DOBViewController: UIViewController, ShowsAlert, LTMorphingLabelDelegate{

    var date_picker:UIDatePicker?
    @IBOutlet weak var dobText: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadingViewPreventUserInteraction: UIView!
    @IBOutlet weak var wowLabel: LTMorphingLabel!
    @IBOutlet weak var yourDOBLabel: LTMorphingLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dobText.layer.masksToBounds = false
        dobText.layer.cornerRadius = dobText.frame.height/2
        dobText.clipsToBounds = true
        
        let now = Date()
        date_picker = UIDatePicker()
        dobText.inputView = date_picker
        dobText.tintColor = .clear
        date_picker?.maximumDate = now
        date_picker?.addTarget(self, action: #selector(handleDatePicker), for: UIControlEvents.valueChanged)
        date_picker?.datePickerMode = .date
        
        let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
        dobText.inputAccessoryView = toolBar
        setLabelMorph(labels: [wowLabel], effect: .evaporate)
        setLabelMorph(labels: [yourDOBLabel], effect: .evaporate)
    }
    override func viewDidAppear(_ animated: Bool) {
        wowLabel.text = "WOW!"
        yourDOBLabel.text = "Your Happy Birthday?"
    }
    
    func setLabelMorph(labels: [LTMorphingLabel], effect: LTMorphingEffect) {
        for label in labels {
            label.delegate = self
            label.morphingEffect = effect
            label.start()
        }
    }
    @IBAction func skipButtonClicked(_ sender: Any) {
        activityIndicator.startAnimating()
        self.loadingViewPreventUserInteraction.isHidden = false
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        let myDateString = formatter.string(from: date)
        sendDOB(myDateString: myDateString)
    }
    
    @IBAction func nextButtonClick(_ sender: Any) {
        if dobText.text != "DD/MM/YYYY" {
            activityIndicator.startAnimating()
            self.loadingViewPreventUserInteraction.isHidden = false
            let myDateString = dobText.text!
            sendDOB(myDateString: myDateString)
        } else {
            self.showAlert(message: "Please select your Date of Birth")
        }
        
    }
    
    func sendDOB(myDateString: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let myDate = dateFormatter.date(from: myDateString)!
        
        dateFormatter.dateFormat = "yyyy/MM/dd"
        let somedateString = dateFormatter.string(from: myDate)
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"DOB\":\"\(somedateString)\"}", mod: "Users", actionType: "update-user-dob", callback: {(response : Any) -> Void in
            print(response)
            if response as? String != "error" {
                if((response as! NSDictionary)["XSCStatus"] as! Int == 0) {
                    self.performSegue(withIdentifier: "dob_skill", sender: self)
                } else {
                    self.showAlert(message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
            self.loadingViewPreventUserInteraction.isHidden = true
        })
    }
    
    @objc func dismissPicker() {
        if dobText.text == "DD/MM/YYYY" {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            dobText.text = formatter.string(from: Date())
        }
        view.endEditing(true)
    }
    
    @objc func handleDatePicker() {
        UIView.transition(with: self.nextButton, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.nextButton.isHidden = false
        })
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dobText.text = dateFormatter.string(from: (date_picker?.date)!)
    }
}
