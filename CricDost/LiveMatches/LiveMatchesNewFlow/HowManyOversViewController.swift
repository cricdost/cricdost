//
//  HowManyOversViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 9/17/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit
import IQKeyboardManager

class HowManyOversViewController: UIViewController,UITextFieldDelegate, ShowsAlert {
    
    @IBOutlet weak var oversViewHolder: UIView!
    @IBOutlet weak var oversTextField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    var tossDetail: NSDictionary?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared().isEnabled = true
        CommonUtil.buttonRoundedCorners(buttons: [nextButton])
        print(tossDetail)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == oversTextField {
            let maxLength = 2
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else {
            return true
        }
    }
    
    @IBAction func nextButtonClick(_ sender: Any) {
        print("match overs next button clicked")
        if oversTextField.isReallyEmpty {
            self.oversViewHolder.shake()
            self.showAlert(title: "Overs", message: "Please enter number of overs")
        } else {
            if Int(oversTextField.text!)! > 50 {
                self.oversViewHolder.shake()
                self.showAlert(title: "Overs", message: "Maximum number of overs is 50")
            } else {
                print("toss details",tossDetail)
                let teamA = tossDetail!["TeamA"] as! NSDictionary
                let teamB = tossDetail!["TeamB"] as! NSDictionary
                let won_team_object = ["teamAName":teamA["TEAM_NAME"] as! String,
                                       "teamAImage":teamA["IMAGE_URL"] as! String,
                                       "teamBName":teamB["TEAM_NAME"] as! String,
                                       "teamBImage":teamB["IMAGE_URL"] as! String,
                                       "teamAId":teamA["TEAM_ID"] as? String ?? "\(teamA["TEAM_ID"] as! Int)",
                                       "teamAMatchId":teamA["MATCH_TEAM_ID"] as? String ?? "\(teamA["MATCH_TEAM_ID"] as! Int)",
                                       "teamBId":teamB["TEAM_ID"] as? String ?? "\(teamB["TEAM_ID"] as! Int)",
                                       "teamBMatchId":teamB["MATCH_TEAM_ID"] as? String ?? "\(teamB["MATCH_TEAM_ID"] as! Int)",
                                       "wonTeamName":tossDetail!["TOSS_WON_NAME"] as? String ?? "\(tossDetail!["TOSS_WON_NAME"] as! Int)",
                                       "mWonId":tossDetail!["TOSS_WON"] as? String ?? "\(tossDetail!["TOSS_WON"] as! Int)",
                                       "mLossId":tossDetail!["TOSS_LOSS"] as? String ?? "\(tossDetail!["TOSS_LOSS"] as! Int)",
                                       ]
                let data: [String: Any] = ["match_id": tossDetail!["MATCH_ID"] as! String,
                                           "user_id": UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String,
                                           "step_name":"over_count",
                                           "won_team_id":tossDetail!["TOSS_WON"] as? String ?? "\(tossDetail!["TOSS_WON"] as! Int)",
                    "won_team_name":tossDetail!["TOSS_WON_NAME"] as? String ?? "\(tossDetail!["TOSS_WON_NAME"] as! Int)",
                    "bat_or_bowl":"",
                    "overs": oversTextField.text!,
                    "toss_loss":tossDetail!["TOSS_LOSS"] as? String ?? "\(tossDetail!["TOSS_LOSS"] as! Int)",
                    "won_team_object":won_team_object]
                print("DATA SEND DURING OVER", data)
                SocketConnectionsclass.matchStatusUpdate_NewFlow(param: data)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
