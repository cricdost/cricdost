//
//  EditProfileViewController.swift
//  CricDost
//
//  Created by Jit Goel on 7/2/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController, ShowsAlert, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var profilewhiteBorderView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var skillsLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var dobTextField: UITextField!
    var isBatsman: String?
    var isBowler: String?
    var isKeeper: String?
    var date_picker:UIDatePicker?
    var imagePicker = UIImagePickerController()
    var imageData:Data?
    @IBOutlet weak var editProfileImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonUtil.imageRoundedCorners(imageviews: [profileImageView])
        profilewhiteBorderView.layer.cornerRadius = profilewhiteBorderView.bounds.height/2
        profilewhiteBorderView.layer.masksToBounds = true
        profilewhiteBorderView.clipsToBounds = false
        getPlayerProfileEdit()
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = CommonUtil.themeRed
        }
//        UIApplication.shared.statusBarStyle = .lightContent
        
        NotificationCenter.default.addObserver(self, selector: #selector(getPlayerProfileEdit), name: NSNotification.Name(rawValue: "getPlayerProfileEdit"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setLocationEditProfileFunction), name: NSNotification.Name(rawValue: "setLocationEditProfileFunction"), object: nil)
        
        let nameLabelTaped = UITapGestureRecognizer(target: self, action: #selector(nameLabelTapFuntion))
        nameLabel.addGestureRecognizer(nameLabelTaped)
        
        let locationLabelTaped = UITapGestureRecognizer(target: self, action: #selector(addressTapFunction))
        locationLabel.addGestureRecognizer(locationLabelTaped)
        
        let genderLabelTaped = UITapGestureRecognizer(target: self, action: #selector(genderLabelTapFunction))
        genderLabel.addGestureRecognizer(genderLabelTaped)
        
        let skillsLabelTaped = UITapGestureRecognizer(target: self, action: #selector(skillsLabelTapFunction))
        skillsLabel.addGestureRecognizer(skillsLabelTaped)
        
        date_picker = UIDatePicker()
        dobTextField.inputView = date_picker
        dobTextField.tintColor = .clear
        date_picker?.maximumDate = Date()
        date_picker?.addTarget(self, action: #selector(handleDatePicker), for: UIControlEvents.valueChanged)
        date_picker?.datePickerMode = .date
        
        let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
        dobTextField.inputAccessoryView = toolBar
        
        imagePicker.delegate = self
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(gesture:)))
        editProfileImageView.addGestureRecognizer(tapGesture)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func imageTapped(gesture: UIGestureRecognizer) {
        print("Image Tapped")
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .popover
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction(title: "Remove Profile Picture", style: .default, handler: { _ in
            self.removeProfilePicture()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            print("IPAD")
            alert.popoverPresentationController?.sourceView = self.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            alert.popoverPresentationController?.permittedArrowDirections = .down
            
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image_data = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageData = UIImageJPEGRepresentation(image_data.updateImageOrientionUpSide()!, 0.025)
            self.dismiss(animated: true, completion: nil)
            UIView.transition(with: self.profileImageView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.profileImageView.image = image_data
                self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height/2
                self.profileImageView.clipsToBounds = true
            })
            if imageData != nil {
                myImageUploadRequest(image: imageData!)
            }
        }
    }
    
    @objc func dismissPicker() {
        updateDob()
        view.endEditing(true)
    }
    
    @objc func skillsLabelTapFunction() {
        let skillUpdateVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UpdateSkillViewController") as! UpdateSkillViewController
        skillUpdateVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        skillUpdateVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        skillUpdateVC.isBatsman = isBatsman
        skillUpdateVC.isBowler = isBowler
        skillUpdateVC.isKeeper = isKeeper
        self.present(skillUpdateVC, animated: true, completion: nil)
    }
    
    @objc func genderLabelTapFunction() {
        let genderUpdateVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UpdateGenderViewController") as! UpdateGenderViewController
        genderUpdateVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        genderUpdateVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        genderUpdateVC.gender = genderLabel.text
        self.present(genderUpdateVC, animated: true, completion: nil)
    }
    
    @objc func setLocationEditProfileFunction() {
        getAddressFromLatLon(pdblLatitude: UserDefaults.standard.object(forKey: CommonUtil.TEMPLATITUDE) as! Double, withLongitude: UserDefaults.standard.object(forKey: CommonUtil.TEMPLONGITUDE) as! Double)
    }
    
    @objc func addressTapFunction(sender:UITapGestureRecognizer) {
        UserDefaults.standard.set(true, forKey: CommonUtil.EDITPROFILESEGUE)
        self.performSegue(withIdentifier: "edit_location", sender: self)
    }
    
    @objc func handleDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dobTextField.text = dateFormatter.string(from: (date_picker?.date)!)
    }
    
    @objc func nameLabelTapFuntion() {
        let updateNameVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UpdateNameViewController") as! UpdateNameViewController
        updateNameVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        updateNameVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        updateNameVC.name = nameLabel.text
        self.present(updateNameVC, animated: true, completion: nil)
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: NSNotification.Name("refreshMyProfileImage"), object: nil)
        })
    }
    
    @objc func getPlayerProfileEdit() {
        activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Player", actionType: "my-profile-details") { (response) in
            if response as? String != "error" {
                print(response)
                let playerInfo = (response as! NSDictionary)["XSCData"] as! NSDictionary
                self.nameLabel.text = playerInfo["FULL_NAME"] as? String
                self.skillsLabel.text = playerInfo["SKILLS"] as? String
                self.locationLabel.text = playerInfo["ADDRESS"] as? String
                self.dobTextField.text = playerInfo["DOB"] as? String
                self.genderLabel.text = playerInfo["GENDER"] as? String
                self.isBatsman = playerInfo["IS_BATSMAN"] as? String
                self.isBowler = playerInfo["IS_BOWLER"] as? String
                self.isKeeper = playerInfo["IS_KEEPER"] as? String
                
                self.profileImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(playerInfo["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
        }
    }
    
    @objc func removeProfilePicture() {
        activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}", mod: "Player", actionType: "remove-player-image") { (response) in
            if response as? String != "error" {
                print(response)
                self.getPlayerProfileEdit()
                let alert = UIAlertController(title: "Message", message: (response as! NSDictionary)["XSCMessage"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                    
                }))
                self.present(alert, animated: true, completion: nil)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
        }
    }
    
    @objc func updateDob() {
        activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"DOB\":\"\(dobTextField.text!)\"}", mod: "Users", actionType: "update-user-dob") { (response) in
            if response as? String != "error" {
                print(response)
                self.getPlayerProfileEdit()
                let alert = UIAlertController(title: "Message", message: (response as! NSDictionary)["XSCMessage"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                    
                }))
                self.present(alert, animated: true, completion: nil)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
        }
    }
    
    func updateLocation(address: String, lat: Double, lon: Double) {
        activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"ADDRESS\":\"\(removeSpecialCharsFromString(text: address))\",\"LATITUDE\":\"\(lat)\",\"LONGITUDE\":\"\(lon)\"}", mod: "Player", actionType: "update-player-location") { (response) in
            if response as? String != "error" {
                print(response)
                self.getPlayerProfileEdit()
                let alert = UIAlertController(title: "Message", message: (response as! NSDictionary)["XSCMessage"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                    
                }))
                self.present(alert, animated: true, completion: nil)
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
            }
            self.activityIndicator.stopAnimating()
        }
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_")
        return String(text.filter {okayChars.contains($0) })
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
        let lat: Double = pdblLatitude
        //21.228124
        let lon: Double = pdblLongitude
        //72.833770
        activityIndicator.startAnimating()
        CommonUtil.getAddressForLatLng(viewcontroller: self, latitude: String(lat), longitude: String(lon), callback: {
            (address: String) -> Void in
            print("GEOCODING \(address)")
            UIView.transition(with: self.locationLabel, duration: 0.4, options: .curveEaseOut, animations: {
                self.locationLabel.text = address
            }, completion: nil)
            self.updateLocation(address: address, lat: lat, lon: lon)
            self.activityIndicator.stopAnimating()
        })
    }
    
    func myImageUploadRequest(image: Data)
    {
        activityIndicator.startAnimating()
        let myUrl = NSURL(string: CommonUtil.BASE_URL);
        //let myUrl = NSURL(string: "http://www.boredwear.com/utils/postImage.php");
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        
        request.httpMethod = "POST";
        
        let param = [
            "app" : "CRICDOST",
            "mod": "Users",
            "actionType" : "update-user-image",
            "subAction": "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\"}"
            ] as [String : Any]
        print(param)
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        if(imageData==nil)  { return; }
        
        request.httpBody = createBodyWithParameters(parameters: param , filePathKey: "IMAGE", imageDataKey: image as NSData, boundary: boundary) as Data
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                self.showAlert(title: "Error", message: "Request TimeOut")
                self.activityIndicator.stopAnimating()
                print("error=\(String(describing: error))")
                return
            }
            
            // You can print out response object
            print("******* response = \(String(describing: response))")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                print(json as Any)
                DispatchQueue.main.async {
                    if json!["XSCStatus"] as! Int == 0 {
                        self.getPlayerProfileEdit()
                        let alert = UIAlertController(title: "Message", message: json!["XSCMessage"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                            
                        }))
                        if let imageUrl = json!["XSCData"] as? NSDictionary {
                            UserDefaults.standard.set(imageUrl["PROFILE_PIC_URL"] as! String, forKey: CommonUtil.IMAGE_URL)
                        }
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        self.showAlert(title: "Oops", message: json!["XSCMessage"] as! String)
                    }
                    self.activityIndicator.stopAnimating()
                }
            }catch
            {
                print(error)
            }
            
        }
        
        task.resume()
    }
    
    func createBodyWithParameters(parameters: [String: Any]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
}
