//
//  FixedMatchLiveCollectionViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 6/21/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class FixedMatchLiveCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var liveButton: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var teamAImageView: UIImageView!
    @IBOutlet weak var teamBImageVIew: UIImageView!
    @IBOutlet weak var teamAName: UILabel!
    @IBOutlet weak var teamBName: UILabel!
    @IBOutlet weak var teamAScore: UILabel!
    @IBOutlet weak var teamBScore: UILabel!
    @IBOutlet weak var adminButton: UIButton!
    @IBOutlet weak var scoreView: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        containerView.layer.cornerRadius = 5.0
        containerView.layer.shadowRadius = 3
        containerView.layer.shadowOpacity = 0.5
        containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        containerView.clipsToBounds = false
        
        liveButton.setImage(#imageLiteral(resourceName: "baseline_fiber_manual_record_white_18pt_1x").withRenderingMode(.alwaysTemplate), for: .normal)
        liveButton.tintColor = CommonUtil.themeRed
        
        if #available(iOS 11.0, *) {
            scoreView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        } else {
            let path = UIBezierPath(roundedRect: scoreView.bounds,
                                    byRoundingCorners: [.topLeft,.topRight],
                                    cornerRadii: CGSize(width: 5, height: 5))
            let maskLayer = CAShapeLayer()
            maskLayer.path = path.cgPath
            scoreView.layer.mask = maskLayer
        }
        
        CommonUtil.buttonRoundedCorners(buttons: [adminButton])
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [containerView])
        CommonUtil.imageRoundedCorners(imageviews: [teamAImageView, teamBImageVIew])
    }
    
}
