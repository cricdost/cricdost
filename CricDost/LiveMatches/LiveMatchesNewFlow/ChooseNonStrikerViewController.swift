//
//  ChooseNonStrikerViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 9/18/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class ChooseNonStrikerViewController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegate, ShowsAlert {
    

    @IBOutlet weak var nonStrikerViewHolder: UIView!
    @IBOutlet weak var nonStrikerCollectionView: UICollectionView!
    @IBOutlet weak var nonStrikerNextButton: UIButton!
    var playerlist: NSArray?
    var selected_Player: String = ""
    var matchID: String?
    var striker: String?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var preventUserInteraction: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        CommonUtil.roundedUIViewCornersWithShade(uiviews: [nonStrikerViewHolder])
        CommonUtil.buttonRoundedCorners(buttons: [nonStrikerNextButton])
    }

    @IBAction func nonStrikerNextButtonClick(_ sender: Any) {
        print("Non Striker next button clicked")
        if selected_Player != "" {
            activityIndicator.startAnimating()
            preventUserInteraction.isHidden = false
            CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"BATSMAN\":\"\(selected_Player)\",\"STRIKER\":\"0\",\"MATCH_ID\":\"\(matchID!)\"}", mod: "MatchScore", actionType: "create-batsman") { (response) in
                if response as? String != "error" {
                    print(response)
                    let data: [String: Any] = ["match_id": self.matchID!,
                                               "user_id": UserDefaults.standard.object(forKey: CommonUtil.USER_ID) as! String,
                                               "step_name":"BAT_USER_ID",
                                               "won_team_id":"",
                                               "won_team_name":"",
                                               "bat_or_bowl":"",
                                               "won_team_object":""]
                    SocketConnectionsclass.matchStatusUpdate_NewFlow(param: data)
                    self.dismiss(animated: true, completion: {
                        let playerList:[String: Any] = ["playerList": self.playerlist!,"match_id":self.matchID!,"striker":self.striker!, "non-striker":self.selected_Player]
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openChooseScorerViewController"), object: nil, userInfo: playerList)
                    })
                } else {
                    self.showAlert(title: "Oops!", message: "Something went wrong! Please try again")
                }
                self.activityIndicator.stopAnimating()
                self.preventUserInteraction.isHidden = true
            }
        } else {
            self.nonStrikerViewHolder.shake()
            self.showAlert(title: "Non striker", message: "Please select a non striker")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return playerlist!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let strikerPlayer = playerlist![indexPath.row] as! NSDictionary
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseNonStrikerCollectionViewCell", for: indexPath) as! ChooseNonStrikerCollectionViewCell
        cell.nonStrikerImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(strikerPlayer["IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "profile_pic_default"))
        cell.nonStrikerNameLabel.text = strikerPlayer["FULL_NAME"] as? String
        
        if selected_Player == strikerPlayer["MATCH_TEAM_PLAYER_ID"] as? String ?? "\(strikerPlayer["MATCH_TEAM_PLAYER_ID"] as! Int)" {
            cell.nonStrikerTickButton.isHidden = false
        } else {
            cell.nonStrikerTickButton.isHidden = true
        }
        
        if strikerPlayer["MATCH_TEAM_PLAYER_ID"] as? String ?? "\(strikerPlayer["MATCH_TEAM_PLAYER_ID"] as! Int)" == striker! {
            cell.isStrikerImageView.isHidden = false
        } else {
            cell.isStrikerImageView.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let strikerPlayer = playerlist![indexPath.row] as! NSDictionary
        
        if strikerPlayer["MATCH_TEAM_PLAYER_ID"] as? String ?? "\(strikerPlayer["MATCH_TEAM_PLAYER_ID"] as! Int)" == striker! {
            self.showAlert(title: "Striker", message: "You cant select this player")
        } else {
            selected_Player = strikerPlayer["MATCH_TEAM_PLAYER_ID"] as? String ?? "\(strikerPlayer["MATCH_TEAM_PLAYER_ID"] as! Int)"
            collectionView.reloadData()
        }
    }
    
}







