//
//  TournamentDetailsViewController.swift
//  CricDost
//
//  Created by JIT GOEL on 9/6/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class TournamentDetailsViewController: UIViewController , ShowsAlert ,UITableViewDelegate , UITableViewDataSource {
    
    var tournamentId: String?
    @IBOutlet weak var backBarButton: UIBarButtonItem!
    @IBOutlet weak var tournamentImageView: UIImageView!
    @IBOutlet weak var tournamentNameLabel: UILabel!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var noOfTeamsLabel: UILabel!
    @IBOutlet weak var joinTournamentButton: UIButton!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var entryFeesLabel: UILabel!
    @IBOutlet weak var lastDateForEntryLabel: UILabel!
    @IBOutlet weak var awardsAndPrizeTableView: UITableView!
    @IBOutlet weak var tournamentRuleTableView: UITableView!
    var tourDetailsView: NSDictionary = [:]
    var awardsPrize: NSArray = []
    var tournamentRules: NSArray = []
    @IBOutlet weak var awardTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var ruleTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var oversLabel: UILabel!
    @IBOutlet weak var playersPerMatchLabel: UILabel!
    @IBOutlet weak var tournamentType: UILabel!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var openFire_UserID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = CommonUtil.themeRed
        }
//        UIApplication.shared.statusBarStyle = .lightContent
        
        CommonUtil.imageRoundedCorners(imageviews: [tournamentImageView])
        CommonUtil.buttonRoundedCorners(buttons: [shareButton,joinTournamentButton])

        getTournamentDetails()
        NotificationCenter.default.addObserver(self, selector: #selector(getTournamentDetails), name: NSNotification.Name(rawValue: "getTournamentDetails"), object: nil)
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func backBarButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func callButtonClik(_ sender: Any) {
        guard let number = URL(string: "telprompt://\(tourDetailsView["CONTACT_NUMBER"] as? String ?? "\(tourDetailsView["CONTACT_NUMBER"] as! Int)")") else { return }
        UIApplication.shared.open(number)
    }
    
    @IBAction func messageButtonClick(_ sender: Any) {
        let chatVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
        chatVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        chatVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        chatVC.playerUserName = openFire_UserID
        self.present(chatVC, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == awardsAndPrizeTableView {
            return awardsPrize.count
        } else {
            return tournamentRules.count
        }
    }
    
    @IBAction func joinTournamentButtonClick(_ sender: Any) {
        activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"DEVICE_KEY\":\"\(UserDefaults.standard.object(forKey: CommonUtil.DEVICE_KEY) as! String)\",\"TOURNAMENT_ID\":\"\(tourDetailsView["TOURNAMENT_ID"] as! String)\",\"NO_OF_TEAMS\":\"\(tourDetailsView["NO_OF_TEAMS"] as! String)\"}", mod: "Tournament", actionType: "tournament-verification") { (response) in
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    let tourDetail = (response as! NSDictionary)["XSCData"] as! NSDictionary
                    
                    if tourDetail["TEAM_EXCEED"] as! String == "true" {
                        self.showAlert(title: "Tournament is full", message: "Maximum number of teams reached")
                    } else if tourDetail["TEAM_JOINED"] as! String == "true" {
                        self.showAlert(title: "Already Joined", message: "You are already in this tournament")
                    } else {
//                        let tourPinKey: [String: NSDictionary] = ["data": tourDetail, "tournament":self.tourDetailsView]
//                        self.dismiss(animated: true, completion: {
//                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "JoinTournament"), object: nil, userInfo: tourPinKey)
//                        })
                        let joinTournamentCVB = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "JoinTournamentViewController") as? JoinTournamentViewController
                        joinTournamentCVB?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        joinTournamentCVB?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        //print(notification.userInfo![AnyHashable("data")] as! Int)
                        joinTournamentCVB?.tourDetail =  tourDetail
                        joinTournamentCVB?.selectedTournament = self.tourDetailsView
                        joinTournamentCVB?.isInTournamentDetailPage = true
                        self.present(joinTournamentCVB!, animated: true, completion: nil)
                    }
                } else {
                    self.showAlert(title: "Message", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong")
            }
            self.activityIndicator.stopAnimating()
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if tableView == awardsAndPrizeTableView {
             let tourAwards = awardsPrize[indexPath.row] as! NSDictionary
            print("XXX")
            let awardsAndPriceCell = tableView.dequeueReusableCell(withIdentifier: "tournamentDetailsAwardsTableViewCell", for: indexPath) as! TournamentDetailsAwardsTableViewCell
            awardsAndPriceCell.awardNameLabel.text = tourAwards["AWARD_NAME"] as? String
            awardsAndPriceCell.prizeAmountLabel.text = tourAwards["AMOUNT"] as? String
            //awardsAndPriceCell.rupeesImageView.image = #imageLiteral(resourceName: "rupee_symbol")
            if tourAwards["IS_TROPHY"] as? Int ?? 0 == 1 {
                awardsAndPriceCell.trophyImageView.image = #imageLiteral(resourceName: "prize_image")
            } else {
                awardsAndPriceCell.trophyImageView.image = nil
                }
            if tourAwards["AMOUNT"] as? String ?? "" == "" {
                awardsAndPriceCell.prizeAmountLabel.text = "00.00"
            } else if Int(tourAwards["AMOUNT"] as! String) == 0 {
                awardsAndPriceCell.prizeAmountLabel.text = "00.00"
            } else {
                awardsAndPriceCell.prizeAmountLabel.isHidden = false
                awardsAndPriceCell.rupeesImageView.isHidden = false
            }
            return awardsAndPriceCell
        } else {
            print("YYYY")
            let tourRules = tournamentRules[indexPath.row] as! NSDictionary
            let tournamentRulesCell = tableView.dequeueReusableCell(withIdentifier: "tournamentDetailsRulesTableViewCell", for: indexPath) as! TournamentDetailsRulesTableViewCell
           // tournamentRulesCell.ruleImageView
            tournamentRulesCell.ruleOptionLabel.text = tourRules["RULE_OPTION"] as? String
            tournamentRulesCell.tournamentRuleLabel.text = tourRules["RULE"] as? String
            return tournamentRulesCell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == awardsAndPrizeTableView {
            awardTableViewHeightConstraint.constant = awardsAndPrizeTableView.contentSize.height
        } else {
            ruleTableViewHeightConstraint.constant = tournamentRuleTableView.contentSize.height
        }
    }
    
    @objc func getTournamentDetails() {
        loadingView.isHidden = false
        activityIndicator.startAnimating()
        CommonUtil.makeRestAPICall(viewcontroller: self, subAction: "{\"TOURNAMENT_ID\":\"\(tournamentId!)\"}", mod: "Tournament", actionType: "get-tournament-details") { (response) in
           
            if response as? String != "error" {
                if (response as! NSDictionary)["XSCStatus"] as! Int == 0 {
                    print(response)
                    self.tourDetailsView = (response as! NSDictionary)["XSCData"] as! NSDictionary
                    self.tournamentNameLabel.text = self.tourDetailsView["NAME"] as? String
                    self.tournamentImageView.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(self.tourDetailsView["IMAGE_URL"] as! String)"), placeholderImage: #imageLiteral(resourceName: "no_team_image_icon"))
                    var startDate = self.tourDetailsView["START_DATE"] as? String
                    startDate = CommonUtil.convertStringDateFormater(startDate!)
                    self.startDateLabel.text = startDate
                    var endDate = self.tourDetailsView["END_DATE"] as? String
                    endDate = CommonUtil.convertStringDateFormater(endDate!)
                    self.endDateLabel.text = endDate
                    self.locationLabel.text = self.tourDetailsView["ADDRESS"] as? String
                    self.entryFeesLabel.text = self.tourDetailsView["ENTRANCE_FEE"] as? String
                    var lastEntryDate = self.tourDetailsView["LAST_DATE_FOR_ENTRY"] as? String
                    lastEntryDate = CommonUtil.convertDateToStringFormater(lastEntryDate!)
                    self.awardsPrize = self.tourDetailsView["TOURNAMENT_AWARDS"] as! NSArray
                    self.tournamentRules = self.tourDetailsView["TOURNAMENT_RULES"] as! NSArray
                    self.lastDateForEntryLabel.text = lastEntryDate
                    self.oversLabel.text = self.tourDetailsView["NO_OF_OVERS"] as? String
                    self.playersPerMatchLabel.text = self.tourDetailsView["PLAYERS_PER_TEAM"] as? String
                    self.tournamentType.text = self.tourDetailsView["MATCH_FIXTURE_TYPE"] as? String
                    self.noOfTeamsLabel.text = "\(String(format: "%02d",self.tourDetailsView["NO_OF_TEAMS_JOINED"] as! Int)) / \(self.tourDetailsView["NO_OF_TEAMS"] as! String)"
                    self.awardsAndPrizeTableView.reloadData()
                    self.tournamentRuleTableView.reloadData()
                } else {
                    self.showAlert(title: "Message", message: (response as! NSDictionary)["XSCMessage"] as! String)
                }
            } else {
                self.showAlert(title: "Oops!", message: "Something went wrong")
            }
            self.activityIndicator.stopAnimating()
            self.loadingView.isHidden = true
        }
    }

}
