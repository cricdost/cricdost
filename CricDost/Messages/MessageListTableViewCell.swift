//
//  MessageListTableViewCell.swift
//  CricDost
//
//  Created by Jit Goel on 7/19/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class MessageListTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var unReadLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        CommonUtil.imageRoundedCorners(imageviews: [profileImage])
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
