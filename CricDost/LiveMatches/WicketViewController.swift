//
//  WicketViewController.swift
//  CricDost
//
//  Created by Jit Goel on 7/3/18.
//  Copyright © 2018 XCEL Solutions Corp. All rights reserved.
//

import UIKit

class WicketViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ShowsAlert {
    
    var howOutPicker = UIPickerView()
    @IBOutlet weak var howoutTextField: UITextField!
    var howOutTypes = ["caught","bowled","hit wicket","hit bat twice","run out","lbw","stumped","handed ball"]
    var fielderList: NSArray = []
    var batsmanList: NSArray = []
    @IBOutlet weak var batsmanProfileImage: UIImageView!
    @IBOutlet weak var strikerProfileImage: UIImageView!
    @IBOutlet weak var batsmanNameLabel: UILabel!
    @IBOutlet weak var strikerNameLabel: UILabel!
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var addWicket: UIButton!
    @IBOutlet weak var batsmanView: UIView!
    @IBOutlet weak var strikerView: UIView!
    var who_out: String = ""
    @IBOutlet weak var batsmanGreenTick: UIImageView!
    @IBOutlet weak var strikerGreenTick: UIImageView!
    var selectedPlayer: String = ""
    @IBOutlet weak var fielderCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        howOutPicker.delegate = self
        howOutPicker.dataSource = self
        
        howoutTextField.inputView = howOutPicker
        
        let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(dismissPicker))
        howoutTextField.inputAccessoryView = toolBar
        
        CommonUtil.roundedUIViewCornersWithShade(uiviews: [holderView])
        CommonUtil.buttonRoundedCorners(buttons: [cancelButton, addWicket])
        CommonUtil.imageRoundedCorners(imageviews: [batsmanProfileImage, strikerProfileImage, batsmanGreenTick, strikerGreenTick])
        
        print("Fielding", fielderList)
        print("Batsman", batsmanList)
        
        batsmanNameLabel.text = (batsmanList[0] as! NSDictionary)["FULL_NAME"] as? String
        strikerNameLabel.text = (batsmanList[1] as! NSDictionary)["FULL_NAME"] as? String
        
        setCollectionViewLayout(card: holderView, collectionView: fielderCollectionView, center: false)
        
        let batsmanViewTaped = UITapGestureRecognizer(target: self, action: #selector(batsmanViewTapFuntion))
        batsmanView.addGestureRecognizer(batsmanViewTaped)
        
        let strikerViewTaped = UITapGestureRecognizer(target: self, action: #selector(strikerViewTapFuntion))
        strikerView.addGestureRecognizer(strikerViewTaped)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        batsmanProfileImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\((batsmanList[0] as! NSDictionary)["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
        strikerProfileImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\((batsmanList[1] as! NSDictionary)["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
    }
    
    @objc func batsmanViewTapFuntion() {
        strikerGreenTick.isHidden = true
        batsmanGreenTick.isHidden = false
        who_out = (batsmanList[0] as! NSDictionary)["BATSMAN"] as! String
    }
    
    @objc func strikerViewTapFuntion() {
        strikerGreenTick.isHidden = false
        batsmanGreenTick.isHidden = true
        who_out = (batsmanList[1] as! NSDictionary)["BATSMAN"] as! String
    }
    
    @IBAction func cancelButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addWicketClick(_ sender: Any) {
        if selectedPlayer != "" {
            if who_out != "" {
                let wicketInfo:[String: Any] = ["who_out": who_out, "how_out": howoutTextField.text!,"fielder": selectedPlayer ]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "wicketAddScore"), object: nil, userInfo: wicketInfo)
                self.dismiss(animated: true, completion: nil)
            } else {
                self.showAlert(title: "Who is out?", message: "Please select the player who was out.")
            }
        } else {
            self.showAlert(title: "Who took the wicket?", message: "Please select a fielder.")
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return fielderList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let player = fielderList[indexPath.section] as! NSDictionary
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wicketsCell", for: indexPath) as! WicketCollectionViewCell
        cell.playerImage.sd_setImage(with: URL(string: "\(CommonUtil.BASE_URL)\(player["IMAGE_URL"] as! String)"), placeholderImage:#imageLiteral(resourceName: "profile_pic_default"))
        cell.playerName.text = player["FULL_NAME"] as? String
        
        if selectedPlayer == player["MATCH_TEAM_PLAYER_ID"] as! String {
            cell.greenTick.isHidden = false
        } else {
            cell.greenTick.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let player = fielderList[indexPath.section] as! NSDictionary
        selectedPlayer = player["MATCH_TEAM_PLAYER_ID"] as! String
        collectionView.reloadData()
    }
    
    @objc func dismissPicker() {
        view.endEditing(true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return howOutTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let text = "\(howOutTypes[row])"
        return text
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let type = howOutTypes[row]
        howoutTextField.text = type
    }
    
    func setCollectionViewLayout(card: UIView, collectionView: UICollectionView, center: Bool) {
        
        if center {
            let screensize = card.bounds.size
            let cellwidth = floor(screensize.width * 0.8)
            let cellheight = floor(collectionView.bounds.height * 1.0)
            
            let insetX = (card.bounds.width - cellwidth) / 2.0
            let insetY = (collectionView.bounds.height - cellheight) / 2.0
            
            let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.itemSize = CGSize(width: cellwidth, height: cellheight)
            collectionView.contentInset = UIEdgeInsetsMake(insetY, insetX, insetY, insetX)
        } else {
            let screensize1 = card.bounds.size
            let cellwidth1 = floor(screensize1.width/3)
            let cellheight1 = floor(collectionView.bounds.height)
            
            let layout1 = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout1.itemSize = CGSize(width: cellwidth1, height: cellheight1)
            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }
    }
    
}
